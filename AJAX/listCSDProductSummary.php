<?php
	include("../INC/connectSFC.php");
	$productType = $_POST['type'];
	$productCatalog = $_POST['catalog'];
	$productVehicleModel = $_POST['vehicleModel'];
	$search = $_POST['search'];
	$results = array();

	$sqlStr = "SELECT PT.Name AS ProductType,PC.Name  AS ProductCatalog,PB.Name AS BoxTemplate,PB.Id,PB.PricePerUnit FROM CSD_BoxTemplates PB
	LEFT JOIN CSD_ProductCatalogs PC ON PB.ProductCatalogId = PC.Id
	LEFT JOIN CSD_VehicleModels VM ON PC.VehicleModelId = VM.Id
	LEFT JOIN CSD_ProductTypes PT ON PC.ProductTypeId = PT.Id WHERE 1=1";

	if($productType != "All"){$sqlStr .= " AND PT.Name = '".$productType."'";}
	if($productCatalog != "All"){$sqlStr .= " AND PC.Name = '".$productCatalog."'";}
	if($productVehicleModel != "All"){$sqlStr .= " AND VM.Name = '".$productVehicleModel."'";}
	if($search != "All"){$sqlStr .= " AND (PT.Name Like N'%".$search."%'  OR PC.Name Like N'%".$search."%'  OR VM.Name Like N'%".$search."%'  OR PB.Name Like N'%".$search."%')";}

	$query = sqlsrv_query($ConnectSaleForecast,$sqlStr);

	$r = 0;
	while ($obj = sqlsrv_fetch_object($query)) {
		$results[$r][] = $obj->ProductType;
		$results[$r][] = $obj->ProductCatalog;
		$results[$r][] = $obj->BoxTemplate;
		$results[$r][] = $obj->Id;
		$results[$r][] = $obj->PricePerUnit;
		$r++;
	}

	header('Content-type: application/json');
	echo json_encode($results);
?>