<?php
	include("../INC/connectSFC.php");
	$results = array();
	$sqlStr = "SELECT  IDForecast,Progress, CONVERT(VARCHAR(10), TimeFrameContractSigndate, 103) as TimeFrameContractSigndate, CONVERT(VARCHAR(10), TimeFrameDeliveryDate, 103) as TimeFrameDeliveryDate FROM  Forecast where StatusSave='1'";
	$query=sqlsrv_query($ConnectSaleForecast,$sqlStr );
	while ($obj = sqlsrv_fetch_object($query)) {
		$results[$r][] = $obj->IDForecast;
		$results[$r][] = $obj->TimeFrameContractSigndate;
		$results[$r][] = $obj->TimeFrameDeliveryDate;
		$results[$r][] = $obj->Progress;
		$r++;
	}
	header('Content-type: application/json');
	echo json_encode($results);
?>