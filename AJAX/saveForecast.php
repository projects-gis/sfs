<?php
function  DeliveryDate($Date,$duration)//M/D/Y=12/31/2552 
{
	$date_parts=explode("/", $Date);
	$myDate=gregoriantojd($date_parts[1], $date_parts[0], $date_parts[2]);
	$Signdate_parts=$myDate+$duration;
	$Signdate=jdtogregorian($Signdate_parts);
	$date_parts2=explode("/", $Signdate);
	$Signdate=$date_parts2[1]."/".$date_parts2[0]."/".$date_parts2[2];
	return $Signdate;
}
function dateDiffDMY($dformat, $endDate, $beginDate)//$endDate="7/7/2003";//D/M/Y
 {
    $date_parts1=explode($dformat, $beginDate);
    $date_parts2=explode($dformat, $endDate);
    $start_date=gregoriantojd($date_parts1[1],$date_parts1[0], $date_parts1[2]);
    $end_date=gregoriantojd($date_parts2[1], $date_parts2[0], $date_parts2[2]);
    return $end_date - $start_date;
}
	function sqlsrv_escape_string($data) {
        if ( !isset($data) or empty($data) ) return '';
        if ( is_numeric($data) ) return $data;

        $non_displayables = array(
            '/%0[0-8bcef]/',            // url encoded 00-08, 11, 12, 14, 15
            '/%1[0-9a-f]/',             // url encoded 16-31
            '/[\x00-\x08]/',            // 00-08
            '/\x0b/',                   // 11
            '/\x0c/',                   // 12
            '/[\x0e-\x1f]/'             // 14-31
        );
        foreach ( $non_displayables as $regex )
            $data = preg_replace( $regex, '', $data );
        $data = str_replace("'", "''", $data );
        return $data;
	}
					
	$IDForecast = $_POST['IDForecast'];
	$P60price3 = $_POST['P60price3'];
	$P60company3 = $_POST['P60company3'];
	$P60price2 = $_POST['P60price2'];
	$P60company2 = $_POST['P60company2'];
	$P60price1 = $_POST['P60price1'];
	$P60company1 = $_POST['P60company1'];
	$P30price3 = $_POST['P30price3'];
	$P30company3 = $_POST['P30company3'];
	$P30price2 = $_POST['P30price2'];
	$P30company2 = $_POST['P30company2'];
	$P30price1 = $_POST['P30price1'];
	$P30company1 = $_POST['P30company1'];
	$Updatedate = $_POST['Updatedate'];
	$flagGenerate = $_POST['flagGenerate'];
	$ProjectStatus = $_POST['ProjectStatus'];
	$ReasonPrice = $_POST['ReasonPrice'];
	$ReasonRelation = $_POST['ReasonRelation'];
	$ReasonTechnic = $_POST['ReasonTechnic'];
	$PayerIndustry = $_POST['PayerIndustry'];
	$Industry = $_POST['Industry'];
	$StatusUpdate = $_POST['StatusUpdate'];
	$BudgetYear = $_POST['BudgetYear'];
	$payer_org = $_POST['payer_org'];
	$payer_code = $_POST['payer_code'];
	$payer_name = sqlsrv_escape_string($_POST['payer_name']);
	$payer_no = $_POST['payer_no'];
	$DP100 = $_POST['DP100'];
	$DP90 = $_POST['DP90'];
	$DP80 = $_POST['DP80'];
	$DP70 = $_POST['DP70'];
	$DP60 = $_POST['DP60'];
	$DP50 = $_POST['DP50'];
	$DP40 = $_POST['DP40'];
	$DP30 = $_POST['DP30'];
	$DP20 = $_POST['DP20'];
	$DP10 = $_POST['DP10'];
	$enduser_org = $_POST['enduser_org'];
	$enduser_code = $_POST['enduser_code'];
	$enduser_name = sqlsrv_escape_string($_POST['enduser_name']);
	$enduser_no = $_POST['enduser_no'];
	$TimeFrameDeliveryDate = $_POST['TimeFrameDeliveryDate'];
	$TimeFrameProjectDuration = $_POST['TimeFrameProjectDuration'];
	$TimeFrameContractSigndate = $_POST['TimeFrameContractSigndate'];
	$TimeFrameBidingDate = $_POST['TimeFrameBidingDate'];
	$TargetCost = $_POST['TargetCost'];
	$TargetBudget = $_POST['TargetBudget'];
	$TargetIncome = $_POST['TargetIncome'];
	$FCYear = $_POST['FCYear'];
	$TargetSpecialProject = $_POST['TargetSpecialProject'];
	$Project = $_POST['Project'];
	$ProjectName = $_POST['ProjectName'];
	$Potential = $_POST['Potential'];
	$PotentialDesc = $_POST['PotentialDesc'];
	$Progress = $_POST['Progress'];
	$ProgressDesc = $_POST['ProgressDesc'];
	$PEContractNo = $_POST['PEContractNo'];
	$EContract = $_POST['EContract'];
	$SaleRepresentative = $_POST['SaleRepresentative'];
	$SaleID = $_POST['SaleID'];
	$CustID = $_POST['CustID'];
	$CustName = $_POST['CustName'];
	$CustOrg = $_POST['CustOrg'];
	$chkDP = $_POST['chkDP'];
	$PercentAmount = $_POST['PercentAmount'];
	$InvoiceAmount = $_POST['InvoiceAmount'];
	$InvDuration = $_POST['InvDuration'];
	$Invdate = $_POST['Invdate'];
	$Book = $_POST['Book'];
	$Forward = $_POST['Forward'];
	$Forward2 = $_POST['Forward2'];
	$Forward3 = $_POST['Forward3'];
	$Remark = $_POST['Remark'];
	$SignificantProjects = $_POST['SignificantProjects'];
	$SIProjects = $_POST['SIProjects'];
	$VerticalMarketGroup = $_POST['VerticalMarketGroup'];
	$UserUpdated = $_POST['UserUpdated'];
	$ProjectBy = $_POST['ProjectBy'];
	$title = $_POST['title'];
	$custsurname = $_POST['custsurname'];
	$division = $_POST['division'];
	$department = $_POST['department'];
	$quotno = $_POST['quotno'];

    $Project=sqlsrv_escape_string($Project);
    $ProjectName=sqlsrv_escape_string($ProjectName);
	include("../INC/connectSFC.php");
	$sqlStr = "UPDATE Forecast SET EContract='$EContract', Project='$Project', Potential='$Potential', Progress='$Progress', TargetSpecialProject='$TargetSpecialProject', TargetIncome=$TargetIncome, TargetBudget=$TargetBudget, TargetCost=$TargetCost, TimeFrameBidingDate=$TimeFrameBidingDate, TimeFrameContractSigndate=$TimeFrameContractSigndate, TimeFrameProjectDuration='$TimeFrameProjectDuration', TimeFrameDeliveryDate=$TimeFrameDeliveryDate, chkDP='$chkDP', PercentAmount='$PercentAmount', InvoiceAmount=$InvoiceAmount, InvDuration='$InvDuration', Invdate=$Invdate, Book=$Book, Forward=$Forward, Forward2=$Forward2, Forward3=$Forward3, Remark='$Remark', SaleID='$SaleID', SaleRepresentative='$SaleRepresentative', SignificantProjects='$SignificantProjects', SIProjects='$SIProjects', VerticalMarketGroup='$VerticalMarketGroup', UserUpdated='$UserUpdated', DateUpdated=GETDATE(), StatusDel='0', StatusSave='1', SendMailBidding='0', SendMailSign='0', SendMailOver30='0', ProjectBy='$ProjectBy', quotno='$quotno', ProjectName='$ProjectName', BudgetYear='$BudgetYear', EnduserNo='$enduser_no', EnduserName='$enduser_name', EnduserCode='$enduser_code', EnduserOrg='$enduser_org', PayerNo='$payer_no', PayerName='$payer_name', PayerCode='$payer_code', PayerOrg='$payer_org',DateProgress70=$TimeFrameBidingDate, DateProgress100=$TimeFrameContractSigndate,DateProgress80=$DP80,DateProgress90=$DP90,sendmailQuick='0',Industry='$Industry',ProjectStatus='$ProjectStatus',ReasonTechnic='$ReasonTechnic',ReasonRelation='$ReasonRelation',ReasonPrice='$ReasonPrice',FlagGenerate='$flagGenerate',LastUpdate=$Updatedate,PayerIndustry='$PayerIndustry', P30company1='$P30company1', P30price1=$P30price1, P30company2='$P30company2', P30price2=$P30price2, P30company3='$P30company3', P30price3=$P30price3, P60company1='$P60company1', P60price1=$P60price1,P60company2='$P60company2', P60price2=$P60price2, P60company3='$P60company3', P60price3=$P60price3 WHERE IDForecast='$IDForecast'";//ESRIisTOR='$ESRIisTOR', Education='$Education', TargetProject='$TargetProject',
	
	$query = sqlsrv_query($ConnectSaleForecast,$sqlStr );
	if ($query) {
			$lstPot = "";
			$lstPro = "";
			$sqlStr = "SELECT TOP 1 Potential FROM HistoryPotential WHERE IDForecast='$IDForecast' ORDER BY ID DESC";
			if ($obj = sqlsrv_fetch_object(sqlsrv_query($ConnectSaleForecast,$sqlStr ))) {
				$lstPot =  $obj->Potential;
			}
			$sqlStr = "SELECT TOP 1 Progress FROM HistoryProgress WHERE IDForecast='$IDForecast' ORDER BY ID DESC";
			if ($obj = sqlsrv_fetch_object(sqlsrv_query($ConnectSaleForecast,$sqlStr ))) {
				$lstPro =  $obj->Progress;
			}
			if ($lstPot != $Potential) {
				$sqlStr = "INSERT INTO HistoryPotential (Potential, PotentialName, DateChange, IDForecast, Userupdated) VALUES ('$Potential', '$PotentialDesc', GETDATE(), '$IDForecast', '$UserUpdated')";
				sqlsrv_query($ConnectSaleForecast,$sqlStr );
			}
			if ($lstPro != $Progress) {
				$sqlStr = "INSERT INTO HistoryProgress (Progress, ProgressName, DateChange, IDForecast, Userupdated) VALUES ('$Progress', '$ProgressDesc', GETDATE(), '$IDForecast', '$UserUpdated')";
				sqlsrv_query($ConnectSaleForecast,$sqlStr );
			}
	}
	$query ? $results = $IDForecast : $results = "failed";
	header('Content-type: application/json');
	echo json_encode($results);
?>