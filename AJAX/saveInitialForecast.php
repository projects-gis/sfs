<?php
	function  DeliveryDate($Date,$duration)//M/D/Y=12/31/2552 
	{
		$date_parts=explode("/", $Date);
		$myDate=gregoriantojd($date_parts[1], $date_parts[0], $date_parts[2]);
		$Signdate_parts=$myDate+$duration;
		$Signdate=jdtogregorian($Signdate_parts);
		$date_parts2=explode("/", $Signdate);
		$Signdate=$date_parts2[1]."/".$date_parts2[0]."/".$date_parts2[2];
		return $Signdate;
	}
	function dateDiffDMY($dformat, $endDate, $beginDate)//$endDate="7/7/2003";//D/M/Y
	 {
	    $date_parts1=explode($dformat, $beginDate);
	    $date_parts2=explode($dformat, $endDate);
	    $start_date=gregoriantojd($date_parts1[1],$date_parts1[0], $date_parts1[2]);
	    $end_date=gregoriantojd($date_parts2[1], $date_parts2[0], $date_parts2[2]);
	    return $end_date - $start_date;
	}
	function sqlsrv_escape_string($data) {
        if ( !isset($data) or empty($data) ) return '';
        if ( is_numeric($data) ) return $data;

        $non_displayables = array(
            '/%0[0-8bcef]/',            // url encoded 00-08, 11, 12, 14, 15
            '/%1[0-9a-f]/',             // url encoded 16-31
            '/[\x00-\x08]/',            // 00-08
            '/\x0b/',                   // 11
            '/\x0c/',                   // 12
            '/[\x0e-\x1f]/'             // 14-31
        );
        foreach ( $non_displayables as $regex )
            $data = preg_replace( $regex, '', $data );
        $data = str_replace("'", "''", $data );
        return $data;
	}
	
	$BudgetYear = $_POST['BudgetYear'];
	$payer_org = $_POST['payer_org'];
	$payer_code = $_POST['payer_code'];
	$payer_name = sqlsrv_escape_string($_POST['payer_name']);
	$payer_no = $_POST['payer_no'];
	$enduser_org = $_POST['enduser_org'];
	$enduser_code = $_POST['enduser_code'];
	$enduser_name = sqlsrv_escape_string($_POST['enduser_name']);
	$enduser_no = $_POST['enduser_no'];
	$DP100 = $_POST['DP100'];
	$DP90 = $_POST['DP90'];
	$DP80 = $_POST['DP80'];
	$DP70 = $_POST['DP70'];
	$DP60 = $_POST['DP60'];
	$DP50 = $_POST['DP50'];
	$DP40 = $_POST['DP40'];
	$DP30 = $_POST['DP30'];
	$DP20 = $_POST['DP20'];
	$DP10 = $_POST['DP10'];
	$UserUpdated = $_POST['UserUpdated'];
	$ProjectBy = $_POST['ProjectBy'];
	$SaleRepresentative = $_POST['SaleRepresentative'];
	$SaleID = $_POST['SaleID'];
	$TimeFrameDeliveryDate = $_POST['TimeFrameDeliveryDate'];
	$TimeFrameProjectDuration = $_POST['TimeFrameProjectDuration'];
	$TimeFrameContractSigndate = $_POST['TimeFrameContractSigndate'];
	$TimeFrameBidingDate = $_POST['TimeFrameBidingDate'];
	$TargetCost = $_POST['TargetCost'];
	$TargetBudget = $_POST['TargetBudget'];
	$TargetIncome = $_POST['TargetIncome'];
	$FCYear = $_POST['FCYear'];
	$PEContractNo = $_POST['PEContractNo'];
	$Project = $_POST['Project'];
	$ProjectName = $_POST['ProjectName'];
	$Potential = $_POST['Potential'];
	$PotentialDesc = $_POST['PotentialDesc'];
	$Progress = $_POST['Progress'];
	$ProgressDesc = $_POST['ProgressDesc'];
	$jsonStat = json_decode($_POST['jsonStat']);
	
    $Project=sqlsrv_escape_string($Project);
    $ProjectName=sqlsrv_escape_string($ProjectName);
	include("../INC/connectSFC.php");
	$sqlStr = "SELECT TOP 1 IDForecast FROM Forecast WHERE IDForecast LIKE 'SF".$FCYear."-%' ORDER BY IDForecast DESC";
	$query = sqlsrv_query($ConnectSaleForecast, $sqlStr );
	if ($obj = sqlsrv_fetch_object($query)) {
		$tempStr = explode("-", $obj->IDForecast);
		$IDForecast = "SF".$FCYear."-".sprintf('%05d', (intval($tempStr[1]) + 1));
	} else {
		$IDForecast = "SF".$FCYear."-00001";
	}
	
	//$custIDd=(int)$enduser_no;
	$sqlStr = "INSERT INTO Forecast (IDForecast, FCYear, PEContractNo,  Project, Potential, Progress, TargetIncome, TargetBudget, TargetCost, TimeFrameBidingDate, TimeFrameContractSigndate, SaleID, SaleRepresentative,UserAdd, DateAdd, UserUpdated, DateUpdated, StatusDel, StatusSave, ProjectBy ,CustID, quotno,DateProgress10,DateProgress20,DateProgress30,DateProgress40,DateProgress50,DateProgress60,DateProgress70,DateProgress80,DateProgress90,DateProgress100,ProjectName,BudgetYear,EnduserNo,EnduserName,EnduserCode,EnduserOrg,PayerNo,PayerName,PayerCode,PayerOrg,chkDP, ESRIisTOR, Education, TargetProject,TargetSpecialProject, SendMailBidding, SendMailSign, SendMailOver30,ProjectStatus) VALUES ('$IDForecast', '$FCYear', '$PEContractNo', '$Project', '$Potential', '$Progress', $TargetIncome, $TargetBudget, $TargetCost, $TimeFrameBidingDate, $TimeFrameContractSigndate,'$SaleID', '$SaleRepresentative', '$UserUpdated', GETDATE(), '$UserUpdated', GETDATE(), '0', '1', '$ProjectBy','$enduser_no', '$quotno',$DP10,$DP20,$DP30,$DP40,$DP50,$DP60,$DP70,$DP80,$DP90,$DP100, '$ProjectName', '$BudgetYear', '$enduser_no', '$enduser_name', '$enduser_code', '$enduser_org', '$payer_no', '$payer_name', '$payer_code', '$payer_org','0','0','0','0','0','0','0','0','Opportunity 10-20-30-40%')";
	$savenew=$sqlStr;

	$query = sqlsrv_query($ConnectSaleForecast, $sqlStr );
	if ($query) {
		/*for ($i = 0; $i < count($jsonStat); $i++) {
			$sqlStr2 = "INSERT INTO StatusDetail (DateStatus, Description, IDForecast, UserUpdated, DateUpdated, IDUpdateStatus) VALUES (".$jsonStat[$i][0].", '".$jsonStat[$i][1]."', '$IDForecast', '$UserUpdated', GETDATE(), NULL)";
			$query = sqlsrv_query($ConnectSaleForecast, $sqlStr2 );
		}*/
		$sqlStr2 = "INSERT INTO StatusDetail (DateStatus, Description, IDForecast, UserUpdated, DateUpdated, IDUpdateStatus) VALUES (GETDATE(), 'New Register', '$IDForecast', '$UserUpdated', GETDATE(), NULL)";
			$query = sqlsrv_query($ConnectSaleForecast, $sqlStr2 );
	}
	$query ? $results = $IDForecast : $results = "failed";
		header('Content-type: application/json');
	echo json_encode($results);
?>