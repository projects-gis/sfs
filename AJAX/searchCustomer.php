 <?php
	include("../INC/connectNCIS.php");
	$keyword = $_POST['keyword'];
	$results = array();
	// $sqlStr = "SELECT a.title, a.name, a.surname, b.thaidivision, b.thaidepartment, b.thaioganization, a.id, b.CustNo FROM Customers a, Organizations b WHERE a.Search LIKE '%".$keyword."%' AND b.organizationid = a.organizationid ORDER BY a.name, b.organization, b.department, b.division";
	$sqlStr = "SELECT a.title, a.name, a.surname, b.thaidivision, b.thaidepartment, b.thaioganization, a.id, b.CustNo, b.organizationinitial,b.division,b.department,b.organization,b.PTS_Code,b.industry FROM Customers a, Organizations b WHERE (a.name LIKE '%".$keyword."%' OR a.nameen LIKE '%".$keyword."%' OR a.surname LIKE '%".$keyword."%' OR a.surnameen LIKE '%".$keyword."%' OR b.department LIKE '%".$keyword."%' OR b.thaidepartment LIKE '%".$keyword."%' OR b.division LIKE '%".$keyword."%' OR b.thaidivision LIKE '%".$keyword."%' OR b.organization LIKE '%".$keyword."%' OR b.thaioganization LIKE '%".$keyword."%' OR b.PTS_Code LIKE '%".$keyword."%') AND b.organizationid = a.organizationid";
	$query = sqlsrv_query($ConnectNCIS,$sqlStr );
	$r = 0;
	while ($obj = sqlsrv_fetch_object($query)) {
		$results[$r][] =  $obj->title;
		$results[$r][] =  $obj->name;
		$results[$r][] =  $obj->surname;
		$results[$r][] = trim( $obj->thaidivision);
		$results[$r][] = trim( $obj->thaidepartment);
		$results[$r][] = trim( $obj->thaioganization);
		$results[$r][] =  $obj->id;
		$results[$r][] =  $obj->CustNo;
		$results[$r][] =  $obj->PTS_Code;
		$results[$r][] = trim( $obj->division);
		$results[$r][] = trim( $obj->department);
		$results[$r][] = trim( $obj->organization);
		$results[$r][] = trim( $obj->industry);
		$r++;
	}
	header('Content-type: application/json');
	echo json_encode($results);
?>