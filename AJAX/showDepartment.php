<?php
	include("../INC/connectSFC.php");
	$results = array();
	$sqlStr = "SELECT id,depName,mailManager,sequence FROM DepartmentDetail where flag <> '1' ORDER BY CAST(sequence AS int)";
	
	$query = sqlsrv_query($ConnectSaleForecast,$sqlStr );
	$r = 0;
	while ($obj = sqlsrv_fetch_object($query)) {
		$results[$r][] = $obj->id;
		$results[$r][] = $obj->sequence;
		$results[$r][] = $obj->depName;
		$results[$r][] = $obj->mailManager;
		$r++;
	}
	header('Content-type: application/json');
	echo json_encode($results);
?>