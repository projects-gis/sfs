<?php
	include("../INC/connectSFC.php");
	$year = $_POST['year'];
	$saleid = $_POST['saleid'];
	$orderby = $_POST['orderby'];
	$results = array();
	$sqlStr = "SELECT ID,PEContractNo,Project,Education,Potential,Progress,TargetIncome,TargetBudget,CONVERT(VARCHAR(10),TimeFrameBidingDate,103) as TimeFrameBidingDate,CONVERT(VARCHAR(10),TimeFrameContractSigndate,103) as TimeFrameContractSigndate,SaleID,SaleRepresentative  FROM Forecast  WHERE YEAR(TimeFrameBidingDate) ='$year' and SaleID IN ('$saleid')  ORDER BY (CASE progress WHEN '100' THEN 1 WHEN '90' THEN 2 WHEN '80' THEN 3 WHEN '70' THEN 4 WHEN '60' THEN 5 WHEN '50' THEN 6 WHEN '40' THEN 7 WHEN '30' THEN 8 WHEN '20' THEN 9 WHEN '10' THEN 10 WHEN '0' THEN 11 ELSE 12 END) , ". $orderby;
	
	$query = sqlsrv_query($ConnectSaleForecast,$sqlStr) ;
	$r = 0;
	while ($obj = sqlsrv_fetch_object($query)) {
		$results[$r][] =  $obj->ID;
		$results[$r][] =  $obj->PEContractNo;
		$results[$r][] =  $obj->Project;
		$results[$r][] =  $obj->Education;
		$results[$r][] =  $obj->Potential;
		$results[$r][] =  $obj->Progress;
		$results[$r][] =  $obj->TargetIncome;
		$results[$r][] =  $obj->TargetBudget;
		$results[$r][] =  $obj->TimeFrameBidingDate;
		$results[$r][] =  $obj->TimeFrameContractSigndate;
		$results[$r][] =  $obj->SaleID;
		$results[$r][] =  $obj->SaleRepresentative;
		$r++;
	}
	header('Content-type: application/json');
	echo json_encode($results);
?>