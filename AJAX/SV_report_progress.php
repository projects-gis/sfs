﻿<?php
function funcSaleIn($id) 
{
	$myid=explode(",",$id);
	$myCount=count($myid);
	$mythiname="";
	for($i=0;$i<$myCount;$i++)
	{
		if($i===0)
		{
			$mythiname="'".$myid[$i]."'";
		}
		else
		{
			$mythiname=$mythiname.",'".$myid[$i]."'";
		}
	}
	return $mythiname;
}
	$results = array();
	include("../INC/connectSFC.php");
	$year = $_POST['year'];
	$type = $_POST['type'];
	$saleid=funcSaleIn($_POST['saleid']);
	$special=funcSaleIn($_POST['option']);
	$sqlStr2="";
	if($type=='bidding')
	{
		$sqlStr2 = " SELECT     YEAR(TimeFrameBidingDate) AS year ,ISNULL(SUM(CASE WHEN Progress = '100' THEN TargetIncome END), 0) AS P100 ,ISNULL(SUM(CASE WHEN Progress = '90' THEN TargetIncome END), 0) AS P90 ,ISNULL(SUM(CASE WHEN Progress = '80' THEN TargetIncome END), 0) AS P80 ,ISNULL(SUM(CASE WHEN Progress = '70' THEN TargetIncome END), 0) AS P70 ,ISNULL(SUM(CASE WHEN Progress = '60' THEN TargetIncome END), 0) AS P60 ,ISNULL(SUM(CASE WHEN Progress = '50' THEN TargetIncome END), 0) AS P50 ,ISNULL(SUM(CASE WHEN Progress = '40' THEN TargetIncome END), 0) AS P40 ,ISNULL(SUM(CASE WHEN Progress = '30' THEN TargetIncome END), 0) AS P30 ,ISNULL(SUM(CASE WHEN Progress = '20' THEN TargetIncome END), 0) AS P20 ,ISNULL(SUM(CASE WHEN Progress = '10' THEN TargetIncome END), 0) AS P10 ,ISNULL(SUM(CASE WHEN Progress = '0' THEN TargetIncome END), 0) AS P0 ,ISNULL(SUM(CASE WHEN Progress = 'v' THEN TargetIncome END), 0) AS Pv ,ISNULL(SUM(CASE WHEN Progress = 'P' THEN TargetIncome END), 0) AS Pp FROM         Forecast WHERE     YEAR(TimeFrameBidingDate) ='$year' and (SaleID IN ($saleid)) and (TargetSpecialProject IN ($special)) GROUP BY year(TimeFrameBidingDate)";
	} 
	else
	{
		$sqlStr2 = "SELECT     YEAR(TimeFrameContractSigndate) AS year,ISNULL(SUM(CASE WHEN Progress = '100' THEN TargetIncome END), 0) AS P100 ,ISNULL(SUM(CASE WHEN Progress = '90' THEN TargetIncome END), 0) AS P90 ,ISNULL(SUM(CASE WHEN Progress = '80' THEN TargetIncome END), 0) AS P80 ,ISNULL(SUM(CASE WHEN Progress = '70' THEN TargetIncome END), 0) AS P70 ,ISNULL(SUM(CASE WHEN Progress = '60' THEN TargetIncome END), 0) AS P60 ,ISNULL(SUM(CASE WHEN Progress = '50' THEN TargetIncome END), 0) AS P50 ,ISNULL(SUM(CASE WHEN Progress = '40' THEN TargetIncome END), 0) AS P40 ,ISNULL(SUM(CASE WHEN Progress = '30' THEN TargetIncome END), 0) AS P30 ,ISNULL(SUM(CASE WHEN Progress = '20' THEN TargetIncome END), 0) AS P20 ,ISNULL(SUM(CASE WHEN Progress = '10' THEN TargetIncome END), 0) AS P10 ,ISNULL(SUM(CASE WHEN Progress = '0' THEN TargetIncome END), 0) AS P0 ,ISNULL(SUM(CASE WHEN Progress = 'v' THEN TargetIncome END), 0) AS Pv ,ISNULL(SUM(CASE WHEN Progress = 'P' THEN TargetIncome END), 0) AS Pp FROM   Forecast WHERE     YEAR(TimeFrameContractSigndate) ='$year' and (SaleID IN ($saleid)) and (TargetSpecialProject IN ($special)) GROUP BY year(TimeFrameContractSigndate)";
	}
	$query2 = sqlsrv_query($ConnectSaleForecast,$sqlStr2 );//CONVERT(VARCHAR(10), GETDATE(), 104) AS [DD.MM.YYYY]
	while ($obj3 = sqlsrv_fetch_object($query2)) {
			$results[$r][] =  trim($obj3->year);
			$results[$r][] =  trim($obj3->P100);
			$results[$r][] =  trim($obj3->P90);
			$results[$r][] =  trim($obj3->P80);
			$results[$r][] =  trim($obj3->P70);
			$results[$r][] =  trim($obj3->P60);
			$results[$r][] =  trim($obj3->P50);
			$results[$r][] =  trim($obj3->P40);
			$results[$r][] =  trim($obj3->P30);
			$results[$r][] =  trim($obj3->P20);
			$results[$r][] =  trim($obj3->P10);
			$results[$r][] =  trim($obj3->P0);
			$results[$r][] =  trim($obj3->Pv);
			$results[$r][] =  trim($obj3->Pp);
			$r++;
		
	}
	//$query2 ? $results = $sqlStr2: $results = $sqlStr2;
	header('Content-type: application/json');
	echo json_encode($results);
?>