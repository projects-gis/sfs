<?php
	include("../INC/connectSFC.php");
	$results = array();
	$sqlStr = "SELECT svt.Name,sv.SubScriptionPlan,sv.StartDate,sv.EndDate,sv.PricePerUnit,sv.Qty FROM CSD_Services sv inner join CSD_ServiceTypes svt on sv.ServiceTypeId = svt.Id";
	$query = sqlsrv_query($ConnectSaleForecast,$sqlStr);
	
	$r = 0;
	while ($obj = sqlsrv_fetch_object($query)) {
		$results[$r][] = $obj->Name;
		$results[$r][] = $obj->SubScriptionPlan;
		$results[$r][] = $obj->StartDate;
		$results[$r][] = $obj->EndDate;
		$results[$r][] = $obj->PricePerUnit;
		$results[$r][] = $obj->Qty;
		$r++;
	}

	header('Content-type: application/json');
	echo json_encode($results);
?>