<?php
	include("../INC/connectSFC.php");
	$results = array();
	$sqlStr = "SELECT [Name] FROM CSD_VehicleModels";
	$query = sqlsrv_query($ConnectSaleForecast,$sqlStr);
	
	$r = 0;
	while ($obj = sqlsrv_fetch_object($query)) {
		$results[$r][] = $obj->Name;
		$r++;
	}

	header('Content-type: application/json');
	echo json_encode($results);
?>