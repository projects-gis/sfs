<?php
	include("../INC/connectSFC.php");
	$results = array();	
	$idforecast = $_POST['idforecast'];
	$sqlStr = "SELECT PlanPhase, DurationDelivery, CONVERT(VARCHAR(10), DeliveryDate, 103) AS DeliveryDate, Amount_Percent, Amount, Remark FROM InvoicingPlan WHERE IDForecast='$idforecast' ORDER BY InvoicingPlanID";
	$query = sqlsrv_query($ConnectSaleForecast, $sqlStr );
	$r = 0;
	while ($obj = sqlsrv_fetch_object($query)) {
		$results[$r][] = $obj->PlanPhase;
		$results[$r][] = $obj->DurationDelivery;
		$results[$r][] = $obj->DeliveryDate;
		$results[$r][] = $obj->Amount_Percent;
		$results[$r][] = $obj->Amount;
		$results[$r][] = $obj->Remark;
		$results[$r][] = $obj->InvoicingPlanID;
		$r++;
	}
	header('Content-type: application/json');
	echo json_encode($results);
?>