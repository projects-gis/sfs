<?php
	require_once("nusoap/nusoap.php");
	//$client = new nusoap_client("http://isdept2008.cdg.co.th/ISWebService/ISService.asmx?wsdl", true);
	$client = new nusoap_client("http://10.254.15.81/ISWebService/ISService.asmx?wsdl", true);
	//$client = new nusoap_client("https://cdgisweb.cdg.co.th/iswebservice/isservice.asmx?wsdl", true);
	$ContType = $_POST['ContType'];
	$ContKind = $_POST['ContKind'];
	$PayerNo = $_POST['PayerNo'];
	$PayerCde = $_POST['PayerCde'];
	$PayerPersonName = $_POST['PayerPersonName'];
	$CustomerNo = $_POST['CustomerNo'];
	$CustomerCde = $_POST['CustomerCde'];
	$CustPersonName = $_POST['CustPersonName'];
	$SignDate = $_POST['SignDate'];
	$EndContDte = $_POST['EndContDte'];
	$Empno = $_POST['Empno'];
	$SaleID = $_POST['SaleID'];
	$PreContractCode = $_POST['PreContractCode'];
	$ProjDesc = $_POST['ProjDesc'];

	$params = array(
				"CompCde" => "GISC",
				"ContType" => $ContType,
				"ContKind" => $ContKind,
				"PayerNo" => $PayerNo,
				"PayerCde" => $PayerCde,
				"PayerPersonName" => $PayerPersonName,
				"CustomerNo" => $CustomerNo,
				"CustomerCde" => $CustomerCde,
				"CustPersonName" => $CustPersonName,
				"SignDate" => $SignDate,
				"EndContDte" => $EndContDte,
				"Empno" => $Empno,
				"SaleID" => $SaleID,
				"PreContractCode" => $PreContractCode,
				"ProjDesc" =>$ProjDesc
	);
	$data = $client->call("EContractService", $params);
	echo $data["EContractServiceResult"];
?>