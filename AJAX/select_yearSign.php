<?php
	include("../INC/connectSFC.php");
	$results = array();
	$sqlStr = "SELECT Year(TimeFrameContractSigndate) as Year FROM Forecast where Year(TimeFrameContractSigndate)is not NULL Group by Year(TimeFrameContractSigndate) order by Year(TimeFrameContractSigndate) DESC";
	
	$query = sqlsrv_query($ConnectSaleForecast, $sqlStr);
	$r = 0;
	while ($obj = sqlsrv_fetch_object($query)) {
		$results[$r][] = $obj->Year;
		$r++;
	}
	header('Content-type: application/json');
	echo json_encode($results);
?>