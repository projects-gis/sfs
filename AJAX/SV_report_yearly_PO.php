﻿<?php
function funcSaleIn($id) 
{
	$myid=explode(",",$id);
	$myCount=count($myid);
	$mythiname="";
	for($i=0;$i<$myCount;$i++)
	{
		if($i===0)
		{
			$mythiname="'".$myid[$i]."'";
		}
		else
		{
			$mythiname=$mythiname.",'".$myid[$i]."'";
		}
	}
	return $mythiname;
}

	//$saleid="0416,0563,4636,5655,5670";
	//$option="0,1";
	//$year="2018";
	//$Ryear="2017";
	
	$year = $_POST['year'];
	$type = $_POST['type'];
	$special=funcSaleIn($_POST['option']);
	$saleid=funcSaleIn($_POST['saleid']);
    include '../INC/connectNCIS.php';
    	
    $arr = array();
    $sqlCIS = "SELECT description FROM OrganizationIndustry where active='1'";
	$query = sqlsrv_query($ConnectNCIS,$sqlCIS);
	while($obj = sqlsrv_fetch_object($query))
	{
		$Q1=0;
		$Q2=0;
		$Q3=0;
		$Q4=0;
		$Total=0;
    	$ProjectIn="";
        $Industry =  $obj->description; 
		include '../INC/connectSFC.php';
		 $sqlSFS = "SELECT ISNULL(SUM(CASE WHEN MONTH(TimeFrameContractSigndate) IN (1, 2, 3) THEN TargetIncome END), 0) AS Q1,ISNULL(SUM(CASE WHEN MONTH(TimeFrameContractSigndate) IN (4, 5, 6) THEN TargetIncome END), 0) AS Q2,ISNULL(SUM(CASE WHEN MONTH(TimeFrameContractSigndate) IN (7, 8, 9) THEN TargetIncome END), 0) AS Q3,ISNULL(SUM(CASE WHEN MONTH(TimeFrameContractSigndate) IN (10, 11, 12) THEN TargetIncome END), 0) AS Q4,ISNULL(SUM(TargetIncome), 0) AS Total FROM Forecast WHERE  year(TimeFrameContractSigndate)='$year' and (TargetSpecialProject IN ($special)) and  SaleID in ($saleid)  and report_Check = '1' and  Progress in ('90','80','70','60','50','40','30','20','10') and Industry='$Industry'";
		 $querySFS = sqlsrv_query($ConnectSaleForecast,$sqlSFS);
		 while($objSFS = sqlsrv_fetch_object($querySFS))
		 {
			$Q1 = $Q1+ $objSFS->Q1; 
	        $Q2 = $Q2+ $objSFS->Q2; 
	        $Q3 = $Q3+ $objSFS->Q3; 
	        $Q4 = $Q4+ $objSFS->Q4; 
	        $Total =  $Total+$objSFS->Total; 
		 }
		 
		$sqlProject = "SELECT PEContractNo,EContract FROM Forecast WHERE  year(TimeFrameContractSigndate)='$year' and (TargetSpecialProject IN ($special)) and  SaleID in ($saleid)  and report_Check = '1' and  Progress ='100' and Industry='$Industry' ";
		$query2 = sqlsrv_query($ConnectSaleForecast,$sqlProject);
		$iLoop =0;
		while($obj2 = sqlsrv_fetch_object($query2))
		{
			 if($iLoop==0)
		    {
		    	$ProjectIn=	"'".$obj2->EContract."'"; 
		    }
		    else
		    {
		    	$ProjectIn=	$ProjectIn.",'".$obj2->EContract."'"; 
		    }
		    $iLoop++;
		}
		if($ProjectIn!="")
	    {
			$Fyear=$year+1;
	    	include '../INC/connectCDGNT.php';
			$sqlPTS100 = "SELECT  ISNULL(SUM(CASE WHEN MONTH(SignDate) IN (1, 2, 3) THEN NETSALE END), 0) AS Q1, ISNULL(SUM(CASE WHEN MONTH(SignDate) IN (4, 5, 6) THEN NETSALE END), 0) AS Q2, ISNULL(SUM(CASE WHEN MONTH(SignDate)  IN (7, 8, 9) THEN NETSALE END), 0) AS Q3, ISNULL(SUM(CASE WHEN MONTH(SignDate) IN (10, 11, 12) THEN NETSALE END), 0) AS Q4, ISNULL(SUM(NETSALE), 0) AS Total FROM Project WHERE  year(SignDate)='$year' and  ContractNo In ($ProjectIn) ";
			$query3 = sqlsrv_query($conCDGNT,$sqlPTS100);
			while($obj3 = sqlsrv_fetch_object($query3))
			{
				$Q1 = $Q1+ $obj3->Q1; 
				$Q2 = $Q2+ $obj3->Q2; 
				$Q3 = $Q3+ $obj3->Q3; 
				$Q4 = $Q4+ $obj3->Q4; 
				$Total =  $Total+$obj3->Total;
			}
		}
		$arr[] = array(
		    "Industry"=>$Industry,
		    "Q1"=>$Q1,
		    "Q2"=>$Q2,
		    "Q3"=>$Q3,
		    "Q4"=>$Q4,
		    "Total"=>$Total
		);
	}
    header('Content-type: application/json');
    echo json_encode($arr);

	//print_r($arr);
?>