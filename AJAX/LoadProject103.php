<?php
	include("../INC/connectSFC.php");
	$results = array();
	$saleid = $_POST['saleid'];
	$sqlStr = "SELECT ID,IDForecast,Project,Potential,Progress, CONVERT(VARCHAR(10), DateProgress10, 103) as DateProgress10, CONVERT(VARCHAR(10), DateProgress20, 103) as DateProgress20,CONVERT(VARCHAR(10), DateProgress30, 103) as DateProgress30,CONVERT(VARCHAR(10), DateProgress40, 103) as  DateProgress40,CONVERT(VARCHAR(10), DateProgress50, 103) as  DateProgress50,CONVERT(VARCHAR(10), DateProgress60, 103) as  DateProgress60,CONVERT(VARCHAR(10), DateProgress70, 103) as  DateProgress70,CONVERT(VARCHAR(10), DateProgress80, 103) as  DateProgress80,CONVERT(VARCHAR(10), DateProgress90, 103) as  DateProgress90,CONVERT(VARCHAR(10), DateProgress100, 103) as  DateProgress100 , (CASE progress WHEN '100' THEN 1 WHEN '90' THEN 2 WHEN '80' THEN 3 WHEN '70' THEN 4 WHEN '60' THEN 5 WHEN '50' THEN 6 WHEN '40' THEN 7 WHEN '30' THEN 8 WHEN '20' THEN 9 WHEN '10' THEN 10 WHEN '0' THEN 11 ELSE 12 END) as ProgressPiority,sendmailQuick,TargetIncome,(SELECT TOP(1) CONVERT(varchar(10), DateStatus, 103) AS DateStatusnew FROM StatusDetail  WHERE (IDForecast = Forecast.IDForecast) ORDER BY DateStatus DESC) AS DateStatus   FROM  Forecast where StatusSave='1' and saleid = '".$saleid."' and Progress not in('100','0','v') order by (CASE potential WHEN '100' THEN 1 WHEN '75' THEN 2 WHEN '50' THEN 3 WHEN '25' THEN 4 WHEN '10' THEN 5 ELSE 6 END), (CASE progress WHEN '100' THEN 1 WHEN '90' THEN 2 WHEN '80' THEN 3 WHEN '70' THEN 4 WHEN '60' THEN 5 WHEN '50' THEN 6 WHEN '40' THEN 7 WHEN '30' THEN 8 WHEN '20' THEN 9 WHEN '10' THEN 10 WHEN '0' THEN 11 ELSE 12 END),year(DateProgress100) desc ,month(DateProgress100) desc ,day(DateProgress100) desc";
	$query = sqlsrv_query($ConnectSaleForecast,$sqlStr);//CONVERT(VARCHAR(10), GETDATE(), 104) AS [DD.MM.YYYY]
	$r = 0;
	while ($obj = sqlsrv_fetch_object($query)) {
		$results[$r][] =  $obj->IDForecast;
		$results[$r][] =  $obj->Project;
		$results[$r][] =  $obj->Potential;
		$results[$r][] =  $obj->Progress;
		$results[$r][] =  $obj->DateProgress10;
		$results[$r][] =  $obj->DateProgress20;
		$results[$r][] =  $obj->DateProgress30;
		$results[$r][] =  $obj->DateProgress40;
		$results[$r][] =  $obj->DateProgress50;
		$results[$r][] =  $obj->DateProgress60;
		$results[$r][] =  $obj->DateProgress70;
		$results[$r][] =  $obj->DateProgress80;
		$results[$r][] =  $obj->DateProgress90;
		$results[$r][] =  $obj->DateProgress100;
		$results[$r][] =  $obj->ID;
		$results[$r][] =  $obj->ProgressPiority;
		$results[$r][] =  $obj->sendmailQuick;//16
		$results[$r][] =  $obj->TargetIncome;//17
		$results[$r][] =  $obj->DateStatus;//18
		$r++;
	}
	header('Content-type: application/json');
	echo json_encode($results); 
?>