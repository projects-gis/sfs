<?php
	include("../INC/connectSFC.php");
	$results = array();
	$sqlStr = "SELECT id,depName FROM DepartmentDetail where flag <> '1' ORDER BY CAST(sequence AS int)";
	
	$query = sqlsrv_query($ConnectSaleForecast,$sqlStr);
	$r = 0;
	while ($obj = sqlsrv_fetch_object($query)) {
		$results[$r][] = $obj->depName;
		$results[$r][] = $obj->id;
		$r++;
	}
	header('Content-type: application/json');
	echo json_encode($results);
?>