﻿<?php
	include("../INC/connectSFC.php");
	$todaydate=Date("Ymd");
		$TotalTargetIncomeP50G=0;
		$TotalTargetIncomeP50E=0;
		$TotalTargetIncomeP50L=0;
		$toMon=Date("m");
		$toyear=Date("Y");
		$mystr="";
		$typeYear = 'TimeFrameBidingDate';
		if($selectYear == 'signdate'){
			$typeYear = 'TimeFrameContractSigndate';
		}
		if($BidingYear>$toyear)
		{
			$mystr="yes";
		}
		elseif($BidingYear==$toyear)
		{
			$mystr="no";
		}
		elseif($BidingYear<$toyear)
		{
			$mystr="";
		}
		$thaiMonth=array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
		$results = array();
		$r = 0;
		if($mystr!="")
		{
			for($i=1;$i<=12;$i++)
			{
				if($i>=$toMon||$mystr=="yes")
				{
					//Potential>50
					$sqlP50G="SELECT TargetIncome FROM Forecast WHERE Potential>50  and year($typeYear)='$BidingYear' and MONTH($typeYear)='$i' and SaleID in ($saleidin) AND ($typeYear >= '$todaydate') and Progress in ('90','80','70','60','50','40','30','20','10') and TargetSpecialProject IN ($option)";
					$queP50G=sqlsrv_query($ConnectSaleForecast,$sqlP50G, array(), array( "Scrollable" => 'static' ));
					$numP50G=sqlsrv_num_rows($queP50G);
					$TargetIncomeP50G=0;
					while($obj=sqlsrv_fetch_object($queP50G))
					{
						$TargetIncomeP50G=$TargetIncomeP50G+$obj->TargetIncome;
					}
					//Potential=50
					$sqlP50E="SELECT TargetIncome  FROM         Forecast WHERE      Potential=50  and year($typeYear)='$BidingYear' and MONTH($typeYear)='$i' and SaleID in ($saleidin) AND ($typeYear >= '$todaydate') and Progress in ('90','80','70','60','50','40','30','20','10') and TargetSpecialProject IN ($option)";
					$queP50E=sqlsrv_query($ConnectSaleForecast,$sqlP50E, array(), array( "Scrollable" => 'static' ));
					$numP50E=sqlsrv_num_rows($queP50E);
					$TargetIncomeP50E=0;
					while($obj=sqlsrv_fetch_object($queP50E))
					{
						$TargetIncomeP50E=$TargetIncomeP50E+$obj->TargetIncome;						
					}
					//Potential<50
					$sqlP50L="SELECT  TargetIncome  FROM         Forecast WHERE      Potential<50  and year($typeYear)='$BidingYear' and MONTH($typeYear)='$i' and SaleID in ($saleidin) AND ($typeYear >= '$todaydate') and Progress in ('90','80','70','60','50','40','30','20','10') and TargetSpecialProject IN ($option)";
					$queP50L=sqlsrv_query($ConnectSaleForecast,$sqlP50L, array(), array( "Scrollable" => 'static' ));
					$numP50L=sqlsrv_num_rows($queP50L);
					$TargetIncomeP50L=0;
					while($obj=sqlsrv_fetch_object($queP50L))
					{
						$TargetIncomeP50L=$TargetIncomeP50L+$obj->TargetIncome;
					}
					
				}	
				$results[$r][] =  $thaiMonth[$i];
				$results[$r][] =  $TargetIncomeP50G;
				$results[$r][] =  $TargetIncomeP50E;
				$results[$r][] =  $TargetIncomeP50L;
				$r++;
			}
		}
	header('Content-type: application/json');
	echo json_encode($results);
		
?>