<?php
	include("../INC/connectSFC.php"); 
	$results = array();
	$saleid = $_POST['saleid'];
	$sqlStr = "SELECT ID,IDForecast,PEContractNo,Project,Potential,Progress, CONVERT(VARCHAR(10), TimeFrameBidingDate, 103) as TimeFrameBidingDate, CONVERT(VARCHAR(10), TimeFrameContractSigndate, 103) as TimeFrameContractSigndate,TargetIncome FROM  Forecast where StatusSave='1' and saleid = '".$saleid."' order by TimeFrameContractSigndate desc";
	$query = sqlsrv_query($ConnectSaleForecast,$sqlStr );//CONVERT(VARCHAR(10), GETDATE(), 104) AS [DD.MM.YYYY]
	$r = 0;
	while ($obj = sqlsrv_fetch_object($query)) {
		$results[$r][] =  $obj->ID;
		$results[$r][] =  $obj->IDForecast;
		$results[$r][] =  $obj->PEContractNo;
		$results[$r][] =  $obj->Project;
		$results[$r][] =  $obj->Potential;
		$results[$r][] =  $obj->Progress;
		$results[$r][] =  $obj->TimeFrameBidingDate;
		$results[$r][] =  $obj->TimeFrameContractSigndate;
		$results[$r][] =  $obj->TargetIncome;
		$r++;
	}
	header('Content-type: application/json');
	echo json_encode($results);
?>