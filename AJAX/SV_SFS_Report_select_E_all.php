<?php
	include("../INC/connectSFC.php");
	$results = array();
	$sqlStr = "SELECT     ID, IDForecast, PEContractNo,EContract, Project, Progress,SaleID,CONVERT(varchar(10), TimeFrameDeliveryDate, 103) as TimeFrameDeliveryDate,FlagSiteRef,CONVERT(varchar(10), TimeFrameContractSigndate, 103) as TimeFrameContractSigndate,SaleRepresentative  FROM Forecast WHERE Progress ='100' and (convert(datetime,TimeFrameDeliveryDate,103) < convert(datetime,Getdate(),103)) AND (EContract <> '') order by year(TimeFrameDeliveryDate) desc,month(TimeFrameDeliveryDate) desc,day(TimeFrameDeliveryDate) desc";
		$query = sqlsrv_query($ConnectSaleForecast,$sqlStr );
	while ($obj = sqlsrv_fetch_object($query)) {
		$results[$r][] = $obj->ID;
		$results[$r][] = $obj->IDForecast;
		$results[$r][] = $obj->EContract;
		$results[$r][] = $obj->PEContractNo;
		$results[$r][] = $obj->Project;
		$results[$r][] = $obj->FlagSiteRef;
		$results[$r][] = $obj->TimeFrameDeliveryDate;
		$results[$r][] = $obj->SaleID;
		$results[$r][] = $obj->SaleRepresentative;
		$results[$r][] = $obj->TimeFrameContractSigndate;
		$r++;
	}
	header('Content-type: application/json');
	echo json_encode($results); 
?>