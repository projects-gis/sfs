<?php
	include("../INC/connectSFC.php");
	$results = array();
	$sqlStr = "SELECT Id,[Name] FROM CSD_ServiceTypes";
	$query = sqlsrv_query($ConnectSaleForecast,$sqlStr);
	
	$r = 0;
	while ($obj = sqlsrv_fetch_object($query)) {
		$results[$r][] = $obj->Id;
		$results[$r][] = $obj->Name;
		$r++;
	}

	header('Content-type: application/json');
	echo json_encode($results);
?>