<?php
	require_once("nusoap/nusoap.php");
	//$client = new nusoap_client("http://isdept2008.cdg.co.th/ISWebService/ISService.asmx?wsdl", true);
	$client = new nusoap_client("http://10.254.15.81/ISWebService/ISService.asmx?wsdl", true);
	//$client = new nusoap_client("https://cdgisweb.cdg.co.th/iswebservice/isservice.asmx?wsdl", true);
	
	$ContYear = $_POST['ContYear'];
	$CustomerNo = $_POST['CustomerNo'];
	$UpdBy = $_POST['UpdBy'];
	$PreContractName = $_POST['PreContractName'];
	$ContDesc = $_POST['ContDesc'];
	$PreContractRemark = $_POST['PreContractRemark'];
	$CustPersonName = $_POST['CustPersonName'];
	$CustPersonEmail = $_POST['CustPersonEmail'];
	$CustPersonTel = $_POST['CustPersonTel'];
	$CustPersonPosition = $_POST['CustPersonPosition'];
	$PreContractStatusCode = $_POST['PreContractStatusCode'];
	$ProdGrpCde = $_POST['ProdGrpCde'];
	$PreContType = $_POST['PreContType'];
	$PreContSaleAmount = $_POST['PreContSaleAmount'];
	$StartDate = $_POST['StartDate'];
	$EndDate = $_POST['EndDate'];
	$params = array(
				"CompCde" => "GISC",
				"ContYear" => $ContYear,
				"CustomerNo" => $CustomerNo,
				"UpdBy" => $UpdBy,
				"PreContractName" => $PreContractName,
				"ContDesc" => $ContDesc,
				"PreContractRemark" => $PreContractRemark,
				"CustPersonName" => $CustPersonName,
				"CustPersonEmail" => $CustPersonEmail,
				"CustPersonTel" => $CustPersonTel,
				"CustPersonPosition" => $CustPersonPosition,
				"PreContractStatusCode" => $PreContractStatusCode,
				"ProdGrpCde" => $ProdGrpCde,
				"PreContType" => $PreContType,
				"PreContSaleAmount" => $PreContSaleAmount,
				"StartDate"=>$StartDate,
				"EndDate"=>$EndDate
	);
	$data = $client->call("PreContractService", $params);
	echo $data["PreContractServiceResult"];
?>