<?php
function  DeliveryDate($Date,$duration)//M/D/Y=12/31/2552 
{
	$date_parts=explode("/", $Date);
	$myDate=gregoriantojd($date_parts[1], $date_parts[0], $date_parts[2]);
	$Signdate_parts=$myDate+$duration;
	$Signdate=jdtogregorian($Signdate_parts);
	$date_parts2=explode("/", $Signdate);
	$Signdate=$date_parts2[1]."/".$date_parts2[0]."/".$date_parts2[2];
	return $Signdate;
}
function dateDiffDMY($dformat, $endDate, $beginDate)//$endDate="7/7/2003";//D/M/Y
 {
    $date_parts1=explode($dformat, $beginDate);
    $date_parts2=explode($dformat, $endDate);
    $start_date=gregoriantojd($date_parts1[1],$date_parts1[0], $date_parts1[2]);
    $end_date=gregoriantojd($date_parts2[1], $date_parts2[0], $date_parts2[2]);
    return $end_date - $start_date;
}
	function sqlsrv_escape_string($data) {
        if ( !isset($data) or empty($data) ) return '';
        if ( is_numeric($data) ) return $data;

        $non_displayables = array(
            '/%0[0-8bcef]/',            // url encoded 00-08, 11, 12, 14, 15
            '/%1[0-9a-f]/',             // url encoded 16-31
            '/[\x00-\x08]/',            // 00-08
            '/\x0b/',                   // 11
            '/\x0c/',                   // 12
            '/[\x0e-\x1f]/'             // 14-31
        );
        foreach ( $non_displayables as $regex )
            $data = preg_replace( $regex, '', $data );
        $data = str_replace("'", "''", $data );
        return $data;
    }
    //$Project=sqlsrv_escape_string($Project);
    //$ProjectName=sqlsrv_escape_string($ProjectName);
    include("../INC/connectSFC.php");
	$sqlStr = "SELECT TOP 1 IDForecast FROM Forecast WHERE IDForecast LIKE 'SF".$FCYear."-%' ORDER BY IDForecast DESC";
	$query = sqlsrv_query($ConnectSaleForecast,iconv("UTF-8", "TIS-620", $sqlStr));
	if ($obj = sqlsrv_fetch_object($query)) {
		$tempStr = split("-", $obj->IDForecast);
		$IDForecast = "SF".$FCYear."-".sprintf('%05d', (intval($tempStr[1]) + 1));
	} else {
		$IDForecast = "SF".$FCYear."-00001";
	}

	$str_Select="select * from Forecast where id='$ID'";
	$query_Select = sqlsrv_query($ConnectSaleForecast,iconv("UTF-8", "TIS-620", $str_Select) );
	if ($obj_Select = sqlsrv_fetch_object($query_Select)) {           
           $Project 	=$obj_Select->Project;
           $CustName 	=$obj_Select->CustName;
           $CustOrg 	=$obj_Select->CustOrg;
           $Potential='10';
           $PotentialDesc='โอกาสน้อยมาก';
           $Progress='10';
           $ProgressDesc='Build solution';
           $ESRIisTOR='0';
           if($obj_Select->Industry =="Education")
           {
              $Education='1';
           }
           else
           {
              $Education='0';
           }
           $TargetProject='0';
           $TargetSpecialProject=$obj_Select->TargetSpecialProject;
           $TargetIncome=$obj_Select->TargetIncome;
           $TargetBudget=$obj_Select->TargetBudget;
           $TargetCost=$obj_Select->TargetCost;
           $TimeFrameBidingDate =$obj_Select->TimeFrameBidingDate;
           $TimeFrameContractSigndate =$obj_Select->TimeFrameContractSigndate;
           $TimeFrameProjectDuration =$obj_Select->TimeFrameProjectDuration;
           $TimeFrameDeliveryDate =$obj_Select->TimeFrameDeliveryDate;
           $chkDP =$obj_Select->chkDP;
           $PercentAmount =$obj_Select->PercentAmount;
           $InvoiceAmount =$obj_Select->InvoiceAmount;
           $InvDuration =$obj_Select->InvDuration;
           $Invdate =$obj_Select->Invdate;
           $Book =$obj_Select->Book;
           $Forward =$obj_Select->Forward;
           $Forward2 =$obj_Select->Forward2;
           $Forward3 =$obj_Select->Forward3;
           $Remark =$obj_Select->Remark;
           //$SaleID =$obj_Select->SaleID;
           //$SaleRepresentative =$obj_Select->SaleRepresentative;
           //$UserAdd =$obj_Select->UserAdd;
           //$UserUpdated =$obj_Select->UserUpdated;
           //$ProjectBy =$obj_Select->ProjectBy;
           $title =$obj_Select->title;
           $custsurname =$obj_Select->custsurname;
           $division =$obj_Select->division;
           $department =$obj_Select->department;
           $CustID =$obj_Select->CustID;
           $quotno =$obj_Select->quotno;
           $DateProgress10 =$obj_Select->DateProgress10;
           $DateProgress20 =$obj_Select->DateProgress20;
           $DateProgress30 =$obj_Select->DateProgress30;
           $DateProgress40 =$obj_Select->DateProgress40;
           $DateProgress50 =$obj_Select->DateProgress50;
           $DateProgress60 =$obj_Select->DateProgress60;
           $DateProgress70 =$obj_Select->DateProgress70;
           $DateProgress80 =$obj_Select->DateProgress80;
           $DateProgress90 =$obj_Select->DateProgress90;
           $DateProgress100 =$obj_Select->DateProgress100;
           $ProjectName =$obj_Select->ProjectName;
           $BudgetYear =$obj_Select->BudgetYear;
           $EnduserNo =$obj_Select->EnduserNo;
           $EnduserName =$obj_Select->EnduserName;
           $EnduserCode =$obj_Select->EnduserCode;
           $EnduserOrg =$obj_Select->EnduserOrg;
           $PayerNo =$obj_Select->PayerNo;
           $PayerName =$obj_Select->PayerName;
           $PayerCode =$obj_Select->PayerCode;
           $PayerOrg =$obj_Select->PayerOrg;
           $Industry =$obj_Select->Industry;
           $ProjectStatus ="Opportunity 10-20-30-40%";
           $PayerIndustry =$obj_Select->PayerIndustry;
	}
	// ID, IDForecast, FCYear, PEContractNo, EContract, Project, CustName, CustOrg, Potential, PotentialDesc, Progress, ProgressDesc, ESRIisTOR, Education, TargetProject, TargetSpecialProject, TargetIncome, TargetBudget, TargetCost, TimeFrameBidingDate, TimeFrameContractSigndate, TimeFrameProjectDuration, TimeFrameDeliveryDate, chkDP, PercentAmount, InvoiceAmount, InvDuration, Invdate,Book, Forward, Forward2, Forward3, Remark, SaleID, SaleRepresentative, SignificantProjects, SIProjects, VerticalMarketGroup, UserAdd, DateAdd, UserUpdated, DateUpdated, StatusDel, StatusSave,SendMailBidding, SendMailSign, SendMailOver30, ProjectBy, title, custsurname, division, department, CustID, quotno, DateProgress10, DateProgress20, DateProgress30, DateProgress40, DateProgress50,  DateProgress60, DateProgress70, DateProgress80, DateProgress90, DateProgress100, sendmailQuick, ProjectName, BudgetYear, EnduserNo, EnduserName, EnduserCode, EnduserOrg, PayerNo, PayerName,PayerCode, PayerOrg, Industry, ReasonTechnic, ReasonRelation, ReasonPrice, ProjectStatus, FlagSiteRef, FlagGenerate, SendmailSiteRefUpdate, SendmailSiteRefCount, LastUpdate, PayerIndustry, P30company1, P30price1, P30company2, P30price2, P30company3, P30price3, P60company1, P60price1, P60company2, P60price2, P60company3, P60price3

	
	/*
INSERT INTO Forecast
           (IDForecast
           ,FCYear
           ,PEContractNo
           ,Project
           ,CustName
           ,CustOrg
           ,Potential
           ,PotentialDesc
           ,Progress
           ,ProgressDesc
           ,ESRIisTOR
           ,Education
           ,TargetProject
           ,TargetSpecialProject
           ,TargetIncome
           ,TargetBudget
           ,TargetCost
           ,TimeFrameBidingDate
           ,TimeFrameContractSigndate
           ,TimeFrameProjectDuration
           ,TimeFrameDeliveryDate
           ,chkDP
           ,PercentAmount
           ,InvoiceAmount
           ,InvDuration
           ,Invdate
           ,Book
           ,Forward
           ,Forward2
           ,Forward3
           ,Remark
           ,SaleID
           ,SaleRepresentative
           ,UserAdd
           ,DateAdd
           ,UserUpdated
           ,DateUpdated
           ,StatusDel
           ,StatusSave
           ,SendMailBidding
           ,SendMailSign
           ,SendMailOver30
           ,ProjectBy
           ,title
           ,custsurname
           ,division
           ,department
           ,CustID
           ,quotno
           ,DateProgress10
           ,DateProgress20
           ,DateProgress30
           ,DateProgress40
           ,DateProgress50
           ,DateProgress60
           ,DateProgress70
           ,DateProgress80
           ,DateProgress90
           ,DateProgress100
           ,ProjectName
           ,BudgetYear
           ,EnduserNo
           ,EnduserName
           ,EnduserCode
           ,EnduserOrg
           ,PayerNo
           ,PayerName
           ,PayerCode
           ,PayerOrg
           ,Industry
           ,ProjectStatus
           ,PayerIndustry)
     VALUES
           ('$IDForecast'
           ,'$FCYear'
           ,'$PEContractNo'
           ,'$Project'
           ,'$CustName'
           ,'$CustOrg'
           ,'$Potential'
           ,'$PotentialDesc'
           ,'$Progress'
           ,'$ProgressDesc'
           ,'$ESRIisTOR'
           ,'$Education'
           ,'$TargetProject'
           ,'$TargetSpecialProject'
           ,$TargetIncome
           ,$TargetBudget
           ,$TargetCost
           ,'$TimeFrameBidingDate'
           ,'$TimeFrameContractSigndate'
           ,'$TimeFrameProjectDuration'
           ,'$TimeFrameDeliveryDate'
           ,'$chkDP
           ,$PercentAmount
           ,$InvoiceAmount
           ,'$InvDuration'
           ,'$Invdate'
           ,$Book
           ,$Forward
           ,$Forward2
           ,$Forward3
           ,'$Remark'
           ,'$SaleID'
           ,'$SaleRepresentative'
           ,'$UserAdd'
           ,getdate()
           ,'$UserUpdated'
           ,getdate()
           ,'0'
           ,'1'
           ,'0'
           ,'0'
           ,'0'
           ,'$ProjectBy'
           ,'$title'
           ,'$custsurname'
           ,'$division'
           ,'$department'
           ,'$CustID'
           ,'$quotno'
           ,'$DateProgress10'
           ,'$DateProgress20'
           ,'$DateProgress30'
           ,'$DateProgress40'
           ,'$DateProgress50'
           ,'$DateProgress60'
           ,'$DateProgress70'
           ,'$DateProgress80'
           ,'$DateProgress90'
           ,'$DateProgress100'
           ,'$ProjectName'
           ,'$BudgetYear'
           ,'$EnduserNo'
           ,'$EnduserName'
           ,'$EnduserCode'
           ,'$EnduserOrg'
           ,'$PayerNo'
           ,'$PayerName'
           ,'$PayerCode'
           ,'$PayerOrg'
           ,'$Industry'
           ,'$ProjectStatus'
           ,'$PayerIndustry')
	*/
	$query = sqlsrv_query($ConnectSaleForecast,iconv("UTF-8", "TIS-620", $sqlStr) );
	$query ? $results = $IDForecast : $results = "failed";
	header('Content-type: application/json');
	echo json_encode($results);
?>