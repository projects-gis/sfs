<?php
	include("../INC/connectNCIS.php");
	$keyword = $_POST['keyword'];
	$results = array();
	// $sqlStr = "SELECT a.title, a.name, a.surname, b.thaidivision, b.thaidepartment, b.thaioganization, a.id, b.CustNo FROM Customers a, Organizations b WHERE a.Search LIKE '%".$keyword."%' AND b.organizationid = a.organizationid ORDER BY a.name, b.organization, b.department, b.division";
	$sqlStr = "SELECT thaidivision, thaidepartment, thaioganization, CustNo, organizationinitial,division,department,organization,PTS_Code,industry FROM Organizations WHERE department LIKE '%".$keyword."%' OR thaidepartment LIKE '%".$keyword."%' OR division LIKE '%".$keyword."%' OR thaidivision LIKE '%".$keyword."%' OR organization LIKE '%".$keyword."%' OR thaioganization LIKE '%".$keyword."%' OR PTS_Code LIKE '%".$keyword."%'";
	$query = sqlsrv_query($ConnectNCIS,$sqlStr);
	$r = 0;
	while ($obj = sqlsrv_fetch_object($query)) {
		$results[$r][] = $obj->PTS_Code;
		$results[$r][] = trim($obj->thaidivision);
		$results[$r][] = trim($obj->thaidepartment);
		$results[$r][] = trim($obj->thaioganization);
		$results[$r][] = trim($obj->division);
		$results[$r][] = trim($obj->department);
		$results[$r][] = trim($obj->organization);
		$results[$r][] = $obj->CustNo;
		$results[$r][] = trim($obj->industry);
		$r++;
	}
	header('Content-type: application/json');
	echo json_encode($results);
?>