<?php
	function replaceSQuote($textinp) {
		return str_replace("'", "''", str_replace('"', '""', $textinp));
	}
	$SM_back=0;
	include("../INC/connectSFC.php");
	$name = $_POST['name'];
	$pk = $_POST['pk'];
	$value = $_POST['value'];
	$saleid = $_POST['saleid'];
	$sqlStrHistory = "SELECT Potential,Progress,CONVERT(VARCHAR(10), TimeFrameBidingDate, 103) as TimeFrameBidingDate,CONVERT(VARCHAR(10), TimeFrameContractSigndate, 103) as TimeFrameContractSigndate FROM  Forecast where IDForecast='$pk'";
	$queryHistory = sqlsrv_query($ConnectSaleForecast,$sqlStrHistory );
	while ($objHistory = sqlsrv_fetch_object($queryHistory)) {
		$historyPotential= $objHistory->Potential;
		$historyProgress= $objHistory->Progress;

		list($day, $month,$year ) = explode('[/.-]', $objHistory->TimeFrameBidingDate);
		$historyTimeFrameBidingDate=$day."/".$month."/".$year;
		list($day2, $month2,$year2 ) = explode('[/.-]', $objHistory->TimeFrameContractSigndate);
		$historyTimeFrameContractSigndate=$day2."/".$month2."/".$year2;
	}

	include("../INC/connectSFC.php");
	if($name=="potential")
	{
		$sqlStr = "Update Forecast set Potential='$value' where IDForecast='$pk'";
		$query = sqlsrv_query($ConnectSaleForecast,$sqlStr );
		//History
		$sqlStr1 = "INSERT INTO HistoryPotential ( Potential, IDForecast, UserUpdated, DateChange
) VALUES ('$value', '$pk', '$salesid',GETDATE())";
		$query1 = sqlsrv_query($ConnectSaleForecast,$sqlStr1 );
		//Update status
		$sqlStr2 = "INSERT INTO StatusDetail ( DateStatus,Description, IDForecast, UserUpdated, DateUpdated, IDUpdateStatus) VALUES (GETDATE(),'ปรับปรุง Potential จาก $historyPotential% เป็น $value%', '$pk', '$salesid',GETDATE(), NULL)";
		$query2 = sqlsrv_query($ConnectSaleForecast,$sqlStr2 );
		$SM_back=1;

	}
	else if($name=="progress")
	{
		$today = date("d/m/Y"); 
		include("../INC/connectSFC.php");
		$sqlStrDate = "SELECT CONVERT(VARCHAR(10), TimeFrameDeliveryDate, 103) as TimeFrameDeliveryDate, CONVERT(VARCHAR(10),TimeFrameContractSigndate, 103) as TimeFrameContractSigndate from Forecast  where IDForecast='$pk'";
		$queryDate = sqlsrv_query($ConnectSaleForecast,$sqlStrDate);
		while ($objDate = sqlsrv_fetch_object($queryDate)) {
			$date1=$objDate->TimeFrameContractSigndate;
			$date2=$objDate->TimeFrameDeliveryDate;
		}
		$statusProject="";
		if($value=='100')
		{
			$Sign = explode('/', $date1);
			$End = explode('/', $date2);
			$d=mktime(0, 0, 0, $End[1], $End[0], $End[2]); 
			//$compare=compareDate($today,$date2);
			if(strtotime($today) > $d)
			{
				if($Sign[2]==$End[2])
        	{
						$statusProject="Project ".$Sign[2];
					}
					else
					{						
						$statusProject="Project ".$Sign[2]."-".$End[2];
					}
			}
			else
			{
				if($Sign[2]==$End[2])
        	{
						$statusProject="Project implement ".$Sign[2];
					}
					else
					{						
						$statusProject="Project implement ".$Sign[2]."-".$End[2];
					}
			}
		}
		else if($value=='80'||$value=='90')
		{
			$statusProject="Opportunity : 80-90%";
		}
		else if($value=='50'||$value=='60'||$value=='70')
		{
			$statusProject="Opportunity : 50-60-70%";
		}
		else if($value=='10'||$value=='20'||$value=='30'||$value=='40')
		{
			$statusProject="Opportunity : 10-40 %";
		}
		else if($value=='0')
		{
			$statusProject="Cancel";
		}
		else if($value=='v')
		{
			$statusProject="Void";
		}
		$sqlStr = "Update Forecast set Progress='$value',ProjectStatus='$statusProject' where IDForecast='$pk'";
		$query = sqlsrv_query($ConnectSaleForecast,$sqlStr );
		//History
		$sqlStr1 = "INSERT INTO HistoryProgress ( Progress, IDForecast, UserUpdated, DateChange
) VALUES ('$value', '$pk', '$salesid',GETDATE())";
		$query1 = sqlsrv_query($ConnectSaleForecast,$sqlStr1 );
		//Update status
		$sqlStr2 = "INSERT INTO StatusDetail ( DateStatus,Description, IDForecast, UserUpdated, DateUpdated, IDUpdateStatus) VALUES (GETDATE(),'ปรับปรุง Progress จาก $historyProgress% เป็น $value%', '$pk', '$salesid',GETDATE(), NULL)";
		$query2 = sqlsrv_query($ConnectSaleForecast,$sqlStr2 );
		$SM_back=1;
	}
	else if($name=="status")
	{
		$sqlStr = "INSERT INTO StatusDetail (DateStatus, Description, IDForecast, UserUpdated, DateUpdated, IDUpdateStatus) VALUES (GETDATE(),'$value', '$pk', '$salesid',GETDATE(), NULL)";
		$query = sqlsrv_query($ConnectSaleForecast,$sqlStr );
		$SM_back=1;
	}
	else if($name=="progress10")
	{		
		$sqlStr = "Update Forecast set DateProgress10='$value' where IDForecast='$pk'";
		$query = sqlsrv_query($ConnectSaleForecast,$sqlStr );
	}
	else if($name=="progress20")
	{
		$sqlStr = "Update Forecast set DateProgress20='$value' where IDForecast='$pk'";
		$query = sqlsrv_query($ConnectSaleForecast,$sqlStr);
	}
	else if($name=="progress30")
	{
		$sqlStr = "Update Forecast set DateProgress30='$value' where IDForecast='$pk'";
		$query = sqlsrv_query($ConnectSaleForecast,$sqlStr);
	}
	else if($name=="progress40")
	{
		$sqlStr = "Update Forecast set DateProgress40='$value' where IDForecast='$pk'";
		$query = sqlsrv_query($ConnectSaleForecast,$sqlStr) ;
	}
	else if($name=="progress50")
	{
		$sqlStr = "Update Forecast set DateProgress50='$value' where IDForecast='$pk'";
		$query = sqlsrv_query($ConnectSaleForecast,$sqlStr );
	}
	else if($name=="progress60")
	{
		$sqlStr = "Update Forecast set DateProgress60='$value' where IDForecast='$pk'";
		$query = sqlsrv_query($ConnectSaleForecast,$sqlStr );
	}
	else if($name=="progress70")
	{
		list($year, $month, $day) = explode('[/.-]', $value);
		$mydate=$day."/".$month."/".$year;
		$date = new DateTime($value);
		$date->modify('+15 day');
		$DP80=$date->format('Y-m-d');
		$date->modify('+15 day');
		$DP90=$date->format('Y-m-d');
		$date->modify('+15 day');
		$DP100=$date->format('Y-m-d');
		$sqlStr = "Update Forecast set DateProgress70='$value',TimeFrameBidingDate='$value',DateProgress80='$DP80',DateProgress90='$DP90',DateProgress100='$DP100',TimeFrameContractSigndate='$DP100' where IDForecast='$pk'";
		$query = sqlsrv_query($ConnectSaleForecast,$sqlStr );
		//Update status
		$sqlStr2 = "INSERT INTO StatusDetail ( DateStatus,Description, IDForecast, UserUpdated, DateUpdated, IDUpdateStatus) VALUES (GETDATE(),'ปรับปรุง Bidding Date จาก $historyTimeFrameBidingDate เป็น $mydate', '$pk', '$salesid',GETDATE(), NULL)";
		$query2 = sqlsrv_query($ConnectSaleForecast,$sqlStr2 );
		$SM_back=1;
	}
	else if($name=="progress80")
	{
		$sqlStr = "Update Forecast set DateProgress80='$value' where IDForecast='$pk'";
		$query = sqlsrv_query($ConnectSaleForecast,$sqlStr );
	}
	else if($name=="progress90")
	{
		$sqlStr = "Update Forecast set DateProgress90='$value' where IDForecast='$pk'";
		$query = sqlsrv_query($ConnectSaleForecast,$sqlStr );
	}
	else if($name=="progress100")
	{
		list($year, $month, $day) = explode('[/.-]', $value);
		$mydate=$day."/".$month."/".$year;
		$sqlStr = "Update Forecast set DateProgress100='$value',TimeFrameContractSigndate='$value' where IDForecast='$pk'";
		$query = sqlsrv_query($ConnectSaleForecast,$sqlStr);
		//Update status
		$sqlStr2 = "INSERT INTO StatusDetail ( DateStatus,Description, IDForecast, UserUpdated, DateUpdated, IDUpdateStatus) VALUES (GETDATE(),'ปรับปรุง Contract Signdate จาก $historyTimeFrameContractSigndate เป็น $mydate', '$pk', '$salesid',GETDATE(), NULL)";
		$query2 =sqlsrv_query($ConnectSaleForecast,$sqlStr2 );
		$SM_back=1;
	}
	$sqlmail = "Update Forecast set sendmailQuick='0' where IDForecast='$pk'";
	$queryMail = sqlsrv_query($ConnectSaleForecast,$sqlmail );
	
	$results = array();
	$r = 0;
	$sqlStrQ = "SELECT ID,IDForecast,Project,Potential,Progress, chkDP, PercentAmount, 
                      InvoiceAmount, InvDuration, CONVERT(VARCHAR(10), Invdate, 103) as Invdate, CONVERT(VARCHAR(10), TimeFrameDeliveryDate, 103) as TimeFrameDeliveryDate,TimeFrameProjectDuration,CONVERT(VARCHAR(10), TimeFrameContractSigndate, 103) as TimeFrameContractSigndate, Book, Forward, Forward2, Forward3 , (CASE progress WHEN '100' THEN 1 WHEN '90' THEN 2 WHEN '80' THEN 3 WHEN '70' THEN 4 WHEN '60' THEN 5 WHEN '50' THEN 6 WHEN '40' THEN 7 WHEN '30' THEN 8 WHEN '20' THEN 9 WHEN '10' THEN 10 WHEN '0' THEN 11 ELSE 12 END) as ProgressPiority,sendmailQuick,TargetIncome  FROM  Forecast where IDForecast = '".$pk."' and Progress not in('100','0','v') order by (CASE potential WHEN '100' THEN 1 WHEN '75' THEN 2 WHEN '50' THEN 3 WHEN '25' THEN 4 WHEN '10' THEN 5 ELSE 6 END), (CASE progress WHEN '100' THEN 1 WHEN '90' THEN 2 WHEN '80' THEN 3 WHEN '70' THEN 4 WHEN '60' THEN 5 WHEN '50' THEN 6 WHEN '40' THEN 7 WHEN '30' THEN 8 WHEN '20' THEN 9 WHEN '10' THEN 10 WHEN '0' THEN 11 ELSE 12 END),year(DateProgress100) desc ,month(DateProgress100) desc ,day(DateProgress100) desc";
	$queryQ = sqlsrv_query($ConnectSaleForecast,$sqlStrQ);
	while ($objQ = sqlsrv_fetch_object($queryQ)) {
		$results[$r][] =  $objQ->IDForecast;
		$results[$r][] =  $objQ->Project;
		$results[$r][] =  $objQ->Potential;
		$results[$r][] =  $objQ->Progress;
		$results[$r][] =  $objQ->chkDP;
		$results[$r][] =  $objQ->PercentAmount;
		$results[$r][] =  $objQ->InvoiceAmount;
		$results[$r][] =  $objQ->InvDuration;
		$results[$r][] =  $objQ->Invdate;
		$results[$r][] =  $objQ->ID;
		$results[$r][] =  $objQ->ProgressPiority;
		$results[$r][] =  $objQ->sendmailQuick;//11
		$results[$r][] =  $objQ->TargetIncome;//12
		$results[$r][] =  $objQ->TimeFrameProjectDuration;//13
		$results[$r][] =  $objQ->TimeFrameContractSigndate;//14
		$results[$r][] =  $objQ->TimeFrameDeliveryDate;//15
		$results[$r][] =  $SM_back;//16
			$r++;
		}

	//$query ? $results = "Success" : $results = "failed";
	header('Content-type: application/json');
	echo json_encode($results); 
?>