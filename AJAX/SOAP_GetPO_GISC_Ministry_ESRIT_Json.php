<?php
	//header('Content-type: text/html; charset=UTF-8');
	require_once("nusoap/nusoap.php");
	//$client = new nusoap_client("http://isdept2008.cdg.co.th/ISWebService/ISService.asmx?wsdl", true);
	//$client = new nusoap_client("http://10.254.15.81/ISWebService/ISService.asmx?wsdl", true);
	$client = new nusoap_client("http://157.179.28.83/wsCommissionLoadDataFromIS/LoadDataFromIS.asmx?wsdl", true);
	$client->soap_defencoding = 'UTF-8';
	$client->decode_utf8 = false;
	$year = $_POST['year'];
	$saleid = $_POST['saleid'];
	$params = array(
				"year" => $year,
				"saleid"=> $saleid
	);
	$arr = array();
	$data = $client->call("testCallSV_PO_Ministry_ESRIT", $params);	
	$datas=$data["testCallSV_PO_Ministry_ESRITResult"]["Item"]["PO2Inc_Ministry"];
	foreach ($datas as $val) {
		$arr[] = array(
		    "ministry"=>$val["ministry"],
		    "Q1"=>$val["Q1"],
		    "Q2"=>$val["Q2"],
		    "Q3"=>$val["Q3"],
		    "Q4"=>$val["Q4"],
		    "Book"=>$val["Book"],
		    "Forward"=>$val["Forward"],
		    "Total"=>$val["Total"]
		);
	}
	header('Content-type: application/json');
	echo json_encode($arr);
?>