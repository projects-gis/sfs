<?php  
function ThaiIToUTF8($in) {
	$out = ""; 
	for ($i = 0; $i < strlen($in); $i++) 
	{
		if (ord($in[$i]) <= 126) 
		$out .= $in[$i];
	else 
		$out .= "&#" . (ord($in[$i]) - 161 + 3585) . ";"; 
	} 
	return $out; 
} 
function UTF8ToThaiI($string) {
	$str = $string;
	$res = "";
	for ($i = 0; $i < strlen($str); $i++) {
		if (ord($str[$i]) == 224) {
			$unicode = ord($str[$i+2]) & 0x3F;
			$unicode |= (ord($str[$i+1]) & 0x3F) << 6;
			$unicode |= (ord($str[$i]) & 0x0F) << 12;
			$res .= chr($unicode-0x0E00+0xA0);
			$i += 2;
		} else {
			$res .= $str[$i];
		}
	}
	return $res;
}
function funcSaleIn($id) 
{
	$myid=explode(",",$id);
	$myCount=count($myid);
	$mythiname="";
	for($i=0;$i<$myCount;$i++)
	{
		if($i===0)
		{
			$mythiname="'".$myid[$i]."'";
		}
		else
		{
			$mythiname=$mythiname.",'".$myid[$i]."'";
		}
	}
	return $mythiname;
}
function funcSpecialIn($id)
	{
		$myid=explode(",",$id);
		$myCount=count($myid);
		$mythiname="";
		for($i=0;$i<$myCount;$i++)
		{
			if($i==0)
			{
				$mythiname="'".$myid[$i]."'";
			}
			else
			{
				$mythiname=$mythiname.",'".$myid[$i]."'";
			}
		}
		return $mythiname;
	}
function funcProgressIn($id)
	{
		$myid=explode(",",$id);
		$myCount=count($myid);
		$mythiname="";
		if($myCount==1)
		{
			$mythiname="'".$id."'";
		}
		else
		{
			for($i=0;$i<$myCount;$i++)
			{
				if($i==0)
				{
					$mythiname="'".$myid[$i]."'";
				}
				else
				{
					$mythiname=$mythiname.",'".$myid[$i]."'";
				}
			}
		}
		return $mythiname;
	}
function myDescription($idforecast) 
{
	$mydesc="";
	include ("INC/connectSFC.php");
	$sql="SELECT     TypeDescription.TypeName, DescriptionDetail.Description,DescriptionDetail.Price FROM         DescriptionDetail INNER JOIN TypeDescription ON DescriptionDetail.IDTypeDescription = TypeDescription.IDTypeDescription WHERE (DescriptionDetail.IDForecast = '$idforecast')ORDER BY TypeDescription.TypeName";
	$result=sqlsrv_query($ConnectSaleForecast,$sql);
	$i=1;
	while ($obj=sqlsrv_fetch_object($result)) 
	{	
		$TypeName=$obj->TypeName;
		$Description=$obj->Description;
		$Price=$obj->Price;
		$Unit=" บาท ";
		if($i==1)
		{
			$mydesc=$TypeName." : ".$Description;
		}
		else
		{
			$mydesc=$mydesc." , ".$TypeName." : ".$Description;
		}
		if(isset($Price))//check null
		{
			if($Price!="0.0000")
			{
				$mydesc=$mydesc." : ".number_format($Price, 2)." ".$Unit;
			}
		}

		$i++;
	}
	return $mydesc;//ThaiIToUTF8($mydesc)
}
function myStatus($idforecast) 
{
	$mystat="";
	include ("INC/connectSFC.php");
	$sql="SELECT  *,convert(varchar,DateStatus,103) as DateStatus from statusDetail WHERE IDForecast = '$idforecast'  order by Year(DateStatus) desc,Month(DateStatus) desc ,day(DateStatus) desc ";
	$result=sqlsrv_query($ConnectSaleForecast,$sql);
	$i=1;
	while ($obj=sqlsrv_fetch_object($result)) 
	{	
		$DateStatus=$obj->DateStatus;
		$Description=$obj->Description;
		if($i==1)
		{
			$mystat=$DateStatus." : ".$Description;
		}
		else
		{
			$mystat=$mystat." , ".$DateStatus." : ".$Description;
		}
		$i++;
	}
	return $mystat;
}
function CheckDepartmentSupportNeeded($IDForecast,$DSNNameID)
{
	include("INC/connectSFC.php");
	$sql="SELECT  * from DepartmentSupportNeeded WHERE IDForecast = '$IDForecast'  and DSNNameID='$DSNNameID' ";
	$result=sqlsrv_query($ConnectSaleForecast,$sql, array(), array( "Scrollable" => 'static' ));
	$num=sqlsrv_num_rows($result);
	if($num==0)
		$num=0;
	else
		$num=1;
	return $num;

}
function CheckLeicaProduct($IDForecast,$LPName)
{
	include("INC/connectSFC.php");
	$sql="SELECT  * from LeicaProduct WHERE IDForecast = '$IDForecast'  and LPName='$LPName' ";
	$result=sqlsrv_query($ConnectSaleForecast,$sql);
	$num=0;
	while($obj=sqlsrv_fetch_object($result))
	{
		$num=$obj->qty;
	}
	return $num;
}
//<!--Query Department Support Needed-->
	include("INC/connectSFC.php");
	$sqlDepSup ="SELECT id,depName FROM DepartmentDetail where flag <> '1' and  export = '1' order by depName";
	$result3=sqlsrv_query($ConnectSaleForecast,$sqlDepSup, array(), array( "Scrollable" => 'static' ));
	$sumDep=sqlsrv_num_rows($result3);
	$countDep = 0;
	while($obj3=sqlsrv_fetch_object($result3))
	{
		$DepIDArr[$countDep]=$obj3->id;
		$DepNameArr[$countDep]=$obj3->depName;
		$countDep= $countDep+1;
	}
	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
	header("Content-Type: application/download");
	header("Content-Transfer-Encoding: binary ");
	header("Content-Type: application/ms-excel");
	header("Content-Disposition: attachment; filename=".basename("Report.xls").";");
?>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="content-type" content="text/html;charset=window-874" />
<title>:Report:</title>
<style type="text/css">
 
td.menubb {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:14px;border-bottom: 1px solid blue;border-left: 1px solid blue;border-top: 1px solid blue;border-right: 1px solid blue; text-align: center;background:#CCFFCC;font-weight: bold;
}
td.menubbn {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;border-bottom: 1px solid blue;border-left: 1px solid blue;border-top: 1px solid blue;border-right: 1px solid blue; text-align: center;background:#CCFFCC;font-weight: bold;
}
td.menubbE {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:14px;border-bottom: 1px solid blue;border-left: 1px solid blue;border-top: 1px solid blue;border-right: 1px solid blue; text-align: center;background:#CDE6FF;font-weight: bold;
}
td.menubbnE {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;border-bottom: 1px solid blue;border-left: 1px solid blue;border-top: 1px solid blue;border-right: 1px solid blue; text-align: center;background:#CDE6FF;font-weight: bold;
}
td.menubbG {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:14px;border-bottom: 1px solid blue;border-left: 1px solid blue;border-top: 1px solid blue;border-right: 1px solid blue; text-align: center;background:#B2FFFF;font-weight: bold;
}
td.menubbnG {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;border-bottom: 1px solid blue;border-left: 1px solid blue;border-top: 1px solid blue;border-right: 1px solid blue; text-align: center;background:#B2FFFF;font-weight: bold;
}
td.menubbT {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:14px;border-bottom: 1px solid blue;border-left: 1px solid blue;border-top: 1px solid blue;border-right: 1px solid blue; text-align: center;background:#FFB3FE;font-weight: bold;
}
td.menubbnT {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;border-bottom: 1px solid blue;border-left: 1px solid blue;border-top: 1px solid blue;border-right: 1px solid blue; text-align: center;background:#FFB3FE;font-weight: bold;
}
td.menubbL {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:14px;border-bottom: 1px solid blue;border-left: 1px solid blue;border-top: 1px solid blue;border-right: 1px solid blue; text-align: center;background:#E6CDFF;font-weight: bold;
}
td.menubbnL {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;border-bottom: 1px solid blue;border-left: 1px solid blue;border-top: 1px solid blue;border-right: 1px solid blue;; text-align: center;background:#E6CDFF;font-weight: bold;
}
td.menubbA {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:14px;border-bottom: 1px solid blue;border-left: 1px solid blue;border-top: 1px solid blue;border-right: 1px solid blue; text-align: center;background:#CCFFCC;font-weight: bold;
}
td.menubbnA {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;border-bottom: 1px solid blue;border-left: 1px solid blue;border-top: 1px solid blue;border-right: 1px solid blue; text-align: center;background:#CCFFCC;font-weight: bold;
}
td.menubbO {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:14px;border-bottom: 1px solid blue;border-left: 1px solid blue;border-top: 1px solid blue;border-right: 1px solid blue; text-align: center;background:#FFF0B3;font-weight: bold;
}
td.menubbnO {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;border-bottom: 1px solid blue;border-left: 1px solid blue;border-top: 1px solid blue;border-right: 1px solid blue; text-align: center;background:#FFF0B3;font-weight: bold;
}
td.menubbY {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:14px;border-bottom: 1px solid blue;border-left: 1px solid blue;border-top: 1px solid blue;border-right: 1px solid blue; text-align: center;background:#FFFF99;font-weight: bold;
}
td.menubbnY {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;border-bottom: 1px solid blue;border-left: 1px solid blue;border-top: 1px solid blue;border-right: 1px solid blue; text-align: center;background:#FFFF99;font-weight: bold;
}
td.menubbY2 {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:14px;border-bottom: 1px solid blue;border-left: 1px solid blue;border-top: 1px solid blue;border-right: 1px solid blue; text-align: center;background:#FFFD4D;font-weight: bold;
}
td.menubbnY2 {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;border-bottom: 1px solid blue;border-left: 1px solid blue;border-top: 1px solid blue;border-right: 1px solid blue; text-align: center;background:#FFFD4D;font-weight: bold;
}
td.menubbD {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:14px;border-bottom: 1px solid blue;border-left: 1px solid blue;border-top: 1px solid blue;border-right: 1px solid blue; text-align: center;background:#00FF00;font-weight: bold;
}
td.menubbnD {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;border-bottom: 1px solid blue;border-left: 1px solid blue;border-top: 1px solid blue;border-right: 1px solid blue; text-align: center;background:#00FF00;font-weight: bold;
}
td.submenubb {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;border-bottom: 1px solid blue;border-left: 1px solid blue;border-top: 1px solid blue;border-right: 1px solid blue;text-align: left;
}
td.submenubbR {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;border-bottom: 1px solid blue;border-left: 1px solid blue;border-top: 1px solid blue;border-right: 1px solid blue;text-align: right;
}
td.submenubbC {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;border-bottom: 1px solid blue;border-left: 1px solid blue;border-top: 1px solid blue;border-right: 1px solid blue;text-align: center;
}
td.txt13 { 
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;border-left: 1px solid blue;border-right: 1px solid blue;
}
</style>
</head>
<?php $strYearStart=$BidingYear;?>
<body >
<table cellpadding="0" cellspacing="0" border="0"width="300%" >
	<tr align="left"><td colspan="13"><FONT SIZE="3" COLOR="#0000FF"><B>Sales Forecast Report Year <?php echo $BidingYear?></B></FONT></td></tr>
	<tr align="center">
	<td  class ="menubbY" rowspan="2" >IDForecast</td>
    <td  class ="menubbY" rowspan="2" >PE Contract</td>
    <td  class ="menubbY" rowspan="2" >E Contract No.</td>
    <td  class ="menubbY" rowspan="2" >Project</td>
    <td  class ="menubbY" rowspan="2" >Description</td>
    <td  class ="menubbY" rowspan="2" >Potential (%)</td>
    <td  class ="menubbY" rowspan="2" >Progress (%)</td>
    <td  class ="menubbY" rowspan="2" >Mega Project</td>
    <td  class ="menubbO" colspan="3"  >Target</td>
    <td  class ="menubbY2" colspan="3" >Time Frame</td>
    <td  class ="menubbY2" rowspan="2" >Proj. Duration (Day)</td>
    <td  class ="menubbY" rowspan="2" >Status</td>
    <td  class ="menubbY" rowspan="2" >Remark</td>
		<td  class ="menubbY" rowspan="2" >Sale Representative</td>
		<td  class ="menubbY" rowspan="2" ><?php echo "Book ".$strYearStart;?></td>
		<?php 
			$yearFWD=($strYearStart+1);
			$yearFWD2=($strYearStart+2);
			$yearFWD3=($strYearStart+3);
		?>
		<td  class ="menubbY" rowspan="2" ><?php echo "Forward ".$yearFWD;?></td>
		<td  class ="menubbY" rowspan="2" ><?php echo "Forward ".$yearFWD2;?></td>
		<td  class ="menubbY" rowspan="2" ><?php echo "Forward ".$yearFWD3;?></td>
		<td  class ="menubbD" colspan="<?php echo $sumDep?>" >Department Support Needed</td>
    <td  class ="menubbY" rowspan="2" >Payer</td>
    <td  class ="menubbY" rowspan="2" >Payer Description</td>
    <td  class ="menubbY" rowspan="2" >Enduser</td>
    <td  class ="menubbY" rowspan="2" >Enduser Description</td>
    <td  class ="menubbY" rowspan="2" >Project Status</td>
    <td  class ="menubbY" rowspan="2" >Industry</td>

  </tr>
  <tr Align="center">
		<!--Target-->
    <td class ="menubbnO" >Income(Baht)</td>
    <td class ="menubbnO" >Budget(Baht)</td>
		<td class ="menubbnO" >Cost (Baht)</td>
	<!--Time Frame-->
    <td class ="menubbnY2" >Biding date</td>
    <td class ="menubbnY2" >Contract Sign date</td>
    <td class ="menubbnY2" >End of Contract</td>
	<!--Department Support Needed-->
	<?php 
	if($sumDep == 0){
	?>
		<td class ="menubbnD" >-</td>
	<?php 
	}else{
	foreach($DepNameArr as $DepName){  
	?>
		<td class ="menubbnD" ><?php echo $DepName?></td>
	<?php 
	}}
	?>
	
  </tr>
  <?php 

	//$saleid="1455";
	//echo $Progress2;
	$where = $_POST['where'];
	$BidingYear = $_POST['BidingYear'];
	$Progress=funcProgressIn($_POST['Progress2']);
	$SpecialProject=funcSpecialIn($_POST['SpecialProject']);
	$saleidin=funcSaleIn($_POST['saleid']);
	include ("INC/connectSFC.php");
		$sql="select IDForecast,Book,Forward,Forward2,Forward3,PEContractNo,Project,Potential,Progress,ESRIisTOR,Education,TargetProject,TargetSpecialProject,TargetIncome,TargetBudget,TargetCost,TimeFrameProjectDuration,Remark,SaleRepresentative,SignificantProjects,SIProjects,VerticalMarketGroup,convert(varchar,TimeFrameBidingDate,103) as TimeFrameBidingDate,convert(varchar,TimeFrameContractSigndate,103) as TimeFrameContractSigndate,convert(varchar,TimeFrameDeliveryDate,103) as TimeFrameDeliveryDate,convert(varchar,DateAdd,103) as DateAdd,Industry,ProjectStatus,EnduserCode,EnduserOrg,PayerCode,PayerOrg,EContract from Forecast where SaleID in ($saleidin) and Progress in ($Progress)  and TargetSpecialProject in ($SpecialProject) and year($where)='$BidingYear' order by Education, (CASE potential WHEN '100' THEN 1 WHEN '75' THEN 2 WHEN '50' THEN 3 WHEN '25' THEN 4 WHEN '10' THEN 5 ELSE 6 END), (CASE progress WHEN '100' THEN 1 WHEN '90' THEN 2 WHEN '80' THEN 3 WHEN '70' THEN 4 WHEN '60' THEN 5 WHEN '50' THEN 6 WHEN '40' THEN 7 WHEN '30' THEN 8 WHEN '20' THEN 9 WHEN '10' THEN 10 WHEN '0' THEN 11 ELSE 12 END),year($where) ,month($where) ,day($where)";

	
	$result=sqlsrv_query($ConnectSaleForecast,$sql);
	while ($obj=sqlsrv_fetch_object($result)) 
	{	
		$PEContractNo=$obj->PEContractNo;
		$EContract=$obj->EContract;
		$Project=$obj->Project;
		$Project=str_replace("\/-_-/","'",$Project);
		$Potential=$obj->Potential;
		$ProjectStatus=$obj->ProjectStatus;
		$EnduserCode=$obj->EnduserCode;
		$EnduserCode=$obj->EnduserOrg;
		$PayerCode=$obj->PayerCode;
		$PayerCode=$obj->PayerOrg;
		$Progress=$obj->Progress;
		$IDForecast=$obj->IDForecast;
		$ESRIisTOR=$obj->ESRIisTOR;
		$Education=$obj->Education;
		$TargetSpecialProject=$obj->TargetSpecialProject;
		$TargetProject=$obj->TargetProject;
		$TargetIncome=$obj->TargetIncome;
		$TargetBudget=$obj->TargetBudget;
		$TargetCost=$obj->TargetCost;
		$TimeFrameBidingDate=$obj->TimeFrameBidingDate;
		$TimeFrameContractSigndate=$obj->TimeFrameContractSigndate;
		$TimeFrameDeliveryDate=$obj->TimeFrameDeliveryDate;
		$TimeFrameProjectDuration=$obj->TimeFrameProjectDuration;
		$Remark=$obj->Remark;
		$SaleRepresentative=$obj->SaleRepresentative;
		$SignificantProjects=$obj->SignificantProjects;
		$SIProjects=$obj->SIProjects;
		$DateAdd=$obj->DateAdd;
		$VerticalMarketGroup=$obj->VerticalMarketGroup;
		$Description=myDescription($IDForecast);
		$Status=myStatus($IDForecast) ;
		$Book=$obj->Book;
		$Forward=$obj->Forward;
		$Forward2=$obj->Forward2;
		$Forward3=$obj->Forward3;
		$Industry=$obj->Industry;
		?>
		<tr>
			<td  class ="submenubbC" valign=top><?php echo $IDForecast?></td>
			<td  class ="submenubbC" valign=top><?php echo $PEContractNo?>&nbsp;</td>
			<td  class ="submenubbC" valign=top><?php echo $EContract?>&nbsp;</td>
			<td  class ="submenubb" valign=top><?php echo $Project?>&nbsp;</td>
		    <td  class ="submenubb" valign=top><?php echo $Description?>&nbsp;</td>
		    <td class ="submenubbC" valign=top><?php echo trim($Potential);?></td>
		    <td class ="submenubbC" valign=top><?php echo trim($Progress);?></td>
			<td class ="submenubbC" valign=top><?php echo trim($TargetSpecialProject);?></td>
		    <td class ="submenubbR" valign=top style="mso-number-format:'\#\,\#\#0\.00'"><?php echo trim($TargetIncome)?></td>
		    <td class ="submenubbR" valign=top style="mso-number-format:'\#\,\#\#0\.00'"><?php echo trim($TargetBudget)?></td>
		    <td class ="submenubbR" valign=top style="mso-number-format:'\#\,\#\#0\.00'"><?php echo trim($TargetCost)?></td>
		    <td class ="submenubbC" valign=top><?php echo trim($TimeFrameBidingDate);?></td>
		    <td class ="submenubbC" valign=top><?php echo trim($TimeFrameContractSigndate);?></td>
		    <td class ="submenubbC" valign=top><?php echo trim($TimeFrameDeliveryDate);?></td>
		    <td class ="submenubbC" valign=top><?php echo trim($TimeFrameProjectDuration);?></td>
		    <td class ="submenubb" valign=top><?php echo trim($Status);?>&nbsp;</td>
		    <td class ="submenubb" valign=top><?php echo trim($Remark);?></td>
		    <td class ="submenubbC" valign=top><?php echo trim($SaleRepresentative);?></td>
		    <td class ="submenubbC" valign=top style="mso-number-format:'\#\,\#\#0\.00'"><?php echo trim($Book)?></td>
		    <td class ="submenubbC" valign=top style="mso-number-format:'\#\,\#\#0\.00'"><?php echo trim($Forward)?></td>
		    <td class ="submenubbC" valign=top style="mso-number-format:'\#\,\#\#0\.00'"><?php echo trim($Forward2)?></td>
		    <td class ="submenubbC" valign=top style="mso-number-format:'\#\,\#\#0\.00'"><?php echo trim($Forward3)?></td>
			<!--Department Support Needed-->
			<?php 
			if($sumDep == 0){
			?>
				<td class ="submenubbC" valign=top></td>
			<?php 
			}else{
			foreach($DepIDArr as $DepID){
			?>
				<td class ="submenubbC" valign=top><?php echo CheckDepartmentSupportNeeded($IDForecast,$DepID);?></td>
			<?php }}
			?>

			<td  class ="submenubb" valign=top><?php echo $PayerCode?>&nbsp;</td>
			<td  class ="submenubb" valign=top><?php echo $PayerOrg?>&nbsp;</td>
			<td  class ="submenubb" valign=top><?php echo $EnduserCode?>&nbsp;</td>
			<td  class ="submenubb" valign=top><?php echo $EnduserOrg?>&nbsp;</td>
			<td  class ="submenubb" valign=top><?php echo $ProjectStatus?>&nbsp;</td>
		  <td class ="submenubbC" valign=top><?php echo trim($Industry);?></td>
		</tr>
	<?php 
	}
	?>
 </table>
</body>
</html>
