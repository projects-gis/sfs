<!DOCTYPE html>
<html lang="en">
	
<!-- Mirrored from bootstrapstyler.com/preview/_/flatdashboard/ui.html by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 18 Oct 2013 07:02:36 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
		<meta charset="utf-8">
		<title>SFS:::Export->Garmin Product</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="Izuddin Helmi">

		<!-- Stylesheets -->
		<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
		<link href="js/library/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
		<link href="css/styler/style.css" rel="stylesheet">
		<link href="js/library/syntaxHighlighter/shCore.css" rel="stylesheet" type="text/css" />
		<link href="js/library/syntaxHighlighter/shThemeDefault.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="css/styler/fontello.css">
	    <link rel="stylesheet" href="css/styler/animation.css"><!--[if IE 7]>
	    <link rel="stylesheet" href="css/styler/fontello-ie7.css"><![endif]-->
		<!-- Custom Fonts -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300,500' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

		<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
		  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	</head>
<?php 
$checkmenu = '5';
$menuExport = '4';
?>
<body cz-shortcut-listen="true" style="margin-top:0px;">
<form name="frmexport"  action="ExportReport_TM.php" >	
<input type="hidden" name="Progress2" value="100,90,80,70,60,50,30,10">
<input type="hidden" name="BidingYear" value="<?php echo date('Y')?>">
<input type="hidden" name="where" value="TimeFrameContractSigndate">
<section class="page-header bg-color white">
	<div class="container">
		<div class="row">
			<div class="span12">
				<div class="section-header clearfix">
					<div class="pull-left">
						<h2><i class="icon-export-4"></i>Export</h2>
					</div>
					<div id="shortcut" class="nav-collapse pull-right"><ul class="shortcuts unstyled">
						<?php include("menu.php");?>
					</ul>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="content">
	<div class="page-tab">
		<div class="page-tab-header">
			<div class="container">
				<ul class="nav nav-tabs clearfix" id="socialTab">
					<?php include("menu_export.php");?>	
				</ul>
			</div>
		</div>
		<div class="tab-content">
			<div class="tab-pane fade active in container" id="tm">
				<div class="widget bg-color turqoise rounded">
					<div class="box-padding">
						<h1>Garmin Product</h1>
						<p class="lead">Project for Garmin Product</p>
					</div>
				</div>
				<div class="row">
					<div class="span3 widget widget-option">
						<div class="profile-head bg-color dark-blue rounded-top">
							<div class="box-padding" style="text-align:center">
									<h3 class="normal"><i class="icon-calendar"></i>Year</h3>
									<span class=" pull-left" style="margin-left:-10px"></span>
							</div>
						</div>
						<div class="bg-color white rounded-bottom">
								<ul class="menu unstyled">
									<li class="current"><p>
									  	<input type="radio"  class="optionsYear1" id="optionsRadios1" name="rdoYear1" value="TimeFrameBidingDate" style="margin-top:-2px">
									  	 Bidding Date <span class=" pull-right" style="margin-top:-8px"><select id="slc_year" name="slc_year" class="span1" style="width:80px;"></select></span></p></li>
									<li><p>
									  	<input type="radio" checked class="optionsYear2" id="optionsRadios1" name="rdoYear1" style="margin-top:-2px" value="TimeFrameContractSigndate">
									  	 Signdate <span class="pull-right"style="margin-top:-8px"><select id="slc_year_sign" name="slc_year_sign" class="span1" style="width:80px;"></select></span></p></li>
								</ul>
						</div>
						<div style="text-align:center"><a class="btn btn-blue btn-large" id="btnExport" style="width:190px">Export</a></div>
						<div id="showWarning" class="alert alert-error hide" style="margin-top:10px;margin-bottom:-10px;">
							<button type="button" class="close" data-dismiss="alert">×</button>
								<i class="icon-attention-3 pull-left" style="font-size:20px;margin-top:-3px;"></i>
								<div class="media-body">
									<strong>กรุณาเลือก Progress ด้วย</strong>
								</div>
						</div>
						<div id="warningGraph" class="alert alert-block hide" style="margin-top:10px;margin-bottom:-10px;">
							<button type="button" class="close" data-dismiss="alert">×</button>
							<i class="icon-info-circle pull-left" style="font-size:25px;margin-top:-7px;"></i>
							<div class="media-body">
								<strong>ไม่พบข้อมูล</strong>
							</div>
						</div>
					</div>	
					<div class="span5">
						<div class="widget widget-profile">
							<div class="profile-head bg-color dark-blue rounded-top">
								<div class="box-padding">
									<h3 class="normal"><i class="icon-progress-3"></i>Progress</h3>
									<span class=" pull-right" style="margin-right:-10px"><input type="checkbox" class="progressAll" title="Select all"></span>
								</div>
							</div>

							<div class="bg-color white rounded-bottom">
								<ul class="menu unstyled">
									<li class="current"><p>100% เซ็นสัญญาแล้ว<span class=" pull-right"><input type="checkbox" name="chkprog" value="100" class="progress" checked></span></p></li>
									<li class="current"><p>90% ประกาศผลแล้วรอเซ็นสัญญา<span class="pull-right"><input type="checkbox" name="chkprog" value="90" class="progress" checked></span></p></li>
									<li class="current"><p>80% ยื่นซองแล้ว<span class="pull-right"><input type="checkbox" name="chkprog" value="80" class="progress" checked ></span></p></li>
									<li class="current"><p>70% ประกาศประกวดราคาแล้วรอยื่นซอง<span class="pull-right"><input type="checkbox" name="chkprog" value="70" class="progress" checked></span></p></li>
									<li class="current"><p>60% จัดส่งของบประมาณให้สำนักงบประมาณแล้ว<span class="pull-right"><input type="checkbox" name="chkprog" value="60" class="progress" checked></span></p></li>
									<li class="current"><p>50% จัดทำ spec. ให้หน่วยงานอยู่<span class="pull-right"><input type="checkbox" name="chkprog" value="50" class="progress" checked></span></p></li>
									<li class="current"><p>30% ได้ requirement, ออกแบบโครงการอยู่<span class="pull-right"><input type="checkbox" name="chkprog" value="30" class="progress" checked></span></p></li>
									<li class="current"><p>10% พิจารณาจัดตั้งโครงการใหม่อยู่<span class="pull-right"><input type="checkbox" name="chkprog" value="10" class="progress"  checked></span></p></li>
									<li><p>0% แพ้<span class="pull-right"><input type="checkbox" name="chkprog" value="0" class="progress" ></span></p></li>
									<li><p>V ยกเลิกโครงการ<span class="pull-right"><input type="checkbox" name="chkprog" value="v" class="progress" ></span></p></li>
									<li><p>X<span class="pull-right"><input type="checkbox" name="chkprog" value="x" class="progress" ></span></p></li>
								</ul>
							</div>

						</div>
					</div>
					<div class="span4 widget widget-option">
						<div class="tab-content" id="Graph" >
								<div id="containerPie" style="min-width: 370px; height: 300px; margin: 0 auto;display:none" ></div>
								<div id="containerBar" style="min-width: 370px; height: 300px; margin: 0 auto"></div>
								<div style="text-align:right" ><a style="margin:10px 0px;" class="btn btn-blue" id="btnSwitch" >Switch Graph</a></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
		<div id="footer" class="bg-color dark-blue">
			<div class="container">
				<div class="box-padding">
					Copyright &copy; 2013 Sales Forecast
				</div>
			</div>
		</div>
		<!-- Javascript -->
		<!-- JS:jquery-->
		<!--<script src="../../../../ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>-->
  		<script src="js/library/jquery/jquery.min.js"></script>
		<script src="js/library/jquery/jquery-ui.min.js"></script>

		<!-- JS:masonry-->
		<script src="js/library/masonry/jquery.masonry.min.js"></script>

		<!-- JS:flexslider-->
		<script src="js/library/flexslider/jquery.flexslider-min.js"></script>

		<!-- JS:tables-->
		<script src="js/library/dataTables/jquery.dataTables.min.js"></script>

		<!-- JS:calendar-->
		<script src="js/library/fullcalendar/fullcalendar.min.js"></script>
		<script src="js/library/fullcalendar/gcal.js"></script>

		<!-- JS:forms-->
		<script src="js/library/forms/jquery.hotkeys.js"></script>
		<script src="js/library/forms/bootstrap-wysiwyg.js"></script>
		<script src="js/library/forms/picker.js"></script>
		<script src="js/library/forms/picker.date.js"></script>
		<script src="js/library/forms/picker.time.js"></script>
		<script src="js/library/forms/jquery.bootstrap.wizard.min.js"></script>

		<!-- JS:charts-->
		<script src="js/library/charts/jquery.flot.js"></script>
		<script src="js/library/charts/jquery.flot.resize.js"></script>
		<script src="js/library/charts/jquery.flot.stack.js"></script>
		<script src="js/library/charts/jquery.flot.pie.min.js"></script>
		<script src="js/library/charts/jquery.sparkline.min.js"></script>
		<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="excanvas.min.js"></script><![endif]-->

		<!-- JS:syntaxHighlighter-->
		<script src="js/library/syntaxHighlighter/shCore.js"></script>
		<script src="js/library/syntaxHighlighter/shBrushCss.js"></script>
		<script src="js/library/syntaxHighlighter/shBrushXml.js"></script>
		<script src="js/library/syntaxHighlighter/shBrushJScript.js"></script>

		<!-- JS:bootstrap-->
		<script src="bootstrap/js/bootstrap.min.js"></script>
		
		<!-- JS:custom js for this template -->
		<script src="js/styler/custom.js"></script>
		<script src="js/highcharts.js"></script>
		<script src="js/exporting.js"></script>
		<script type="text/javascript">
			Number.prototype.nformat = function() {
			    return this.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
			};
		    $(document).ready(function(){
				$("#slc_year").disableSelection();
		    	$('.widget-sales').on('click','#loadsales',function(){
		        	$('#myModal').modal('show');
		    	});
		    	$.ajax({ 
					dataType: "json",
			        url: "http://157.179.28.116/sfs/sv.svc/select_year?callback=?",
			        success: function (json) {
			        	$(json.items).each(function () {
			        		$("#slc_year").append("<option value='" + this.Year + "'>" + this.Year + "</option>");
						});
						$("#slc_year").val((new Date).getFullYear());
			        }
			    });
			    $.ajax({ 
					dataType: "json",
			        url: "http://157.179.28.116/sfs/sv.svc/Select_yearSign?callback=?",
			        success: function (json) {
			        	$(json.items).each(function () {
			        		$("#slc_year_sign").append("<option value='" + this.Year + "'>" + this.Year + "</option>");
						});
						$("#slc_year_sign").val((new Date).getFullYear());
			        }
			    });
				
		    	$('.progressAll').on('click',function(){
					if ($(".progressAll").is(':checked')) {
						$(".progress").prop("checked", true);
						$('.progress').closest('li').attr("class", "current");
						document.frmexport.Progress2.value="100,90,80,70,60,50,30,10,0,v,x";
						showGraph();
					} else {
						$(".progress").prop("checked", false);
						$('.progress').closest('li').attr("class", "");
						showGraph();
					}
		    	});
				$('.progress').on('click',function(){
					if ( $(this).prop('checked')) {
						$(this).closest('li').attr("class", "current");
				    } else {
						$(this).closest('li').attr("class", "");
				    }
					if( $(".progress:checked").length == $(".progress").length){
						$(".progressAll").prop("checked", true);
					}else{
						$(".progressAll").prop("checked", false);
					}
					getProgress();
				});
		    	
				$('.optionsYear1').on('click',function(){
					$('.optionsYear1').closest('li').attr("class", "current");
					$('.optionsYear2').closest('li').attr("class", "");
					$("#slc_year_sign").disableSelection();
					$("#slc_year").enableSelection();
					document.frmexport.BidingYear.value=document.frmexport.slc_year.value;
					document.frmexport.where.value=document.frmexport.rdoYear1[0].value;
					showGraph();
				});
				$('.optionsYear2').on('click',function(){
					$('.optionsYear1').closest('li').attr("class", "");
					$('.optionsYear2').closest('li').attr("class", "current");
					$("#slc_year").disableSelection();
					$("#slc_year_sign").enableSelection();
					document.frmexport.BidingYear.value=document.frmexport.slc_year_sign.value;
					document.frmexport.where.value=document.frmexport.rdoYear1[1].value;
					showGraph();
				});
				$("#slc_year").change(function(){
					document.frmexport.BidingYear.value=document.frmexport.slc_year.value;
					document.frmexport.where.value=document.frmexport.rdoYear1[0].value;
					showGraph();
				});
				$("#slc_year_sign").change(function(){
					document.frmexport.BidingYear.value=document.frmexport.slc_year_sign.value;
					document.frmexport.where.value=document.frmexport.rdoYear1[1].value;
					showGraph();
				});
				function getProgress(){
					var chkidlist = '';
					$("input[name='chkprog']").each(function () {
							if ( $(this).prop('checked')) {
								if(chkidlist=='')
								{
									chkidlist= $(this).val();
								}
								else
								{
									chkidlist = chkidlist+","+$(this).val();
								}
							}
					});
					document.frmexport.Progress2.value=chkidlist;
					showGraph();
				}
				showGraph();//start
				
				$('#btnExport').on('click',function(){
					if($('#warningGraph').is(':hidden')) {
						$('#showWarning').addClass("hide");
						if ($(".progress:checked").length == 0) {
							$('#showWarning').removeClass("hide");
						}else{
							console.log("Export ",document.frmexport.BidingYear.value+' '+document.frmexport.where.value+' '+document.frmexport.Progress2.value);
							document.frmexport.submit();
						}
					}
				});	
				//////////////////////////////show graph
			$('#btnSwitch').on('click',function(){
				if($('#containerBar').is(':hidden')) {
					$("#containerBar").show();
					$("#containerPie").hide();
				}else{
					$("#containerBar").hide();
					$("#containerPie").show();
				}
			});
			
			function showGraph() {
				$('#showWarning').addClass("hide");
				$('#warningGraph').addClass("hide");
					if($(".progress:checked").length == 0){
						$('#showWarning').removeClass("hide");
						$('#Graph').hide();
						
					}else{
						console.log("change ",document.frmexport.BidingYear.value+' '+document.frmexport.where.value+' '+document.frmexport.Progress2.value);
					
						$.ajax({ 
						dataType: "json",
						url: "http://157.179.28.116/sfs/sv.svc/report_potential?callback=?",
						data: {
							year: document.frmexport.BidingYear.value,
							saleid: '3701',
							type: 'bidding'
						},
						success: function (json) {
						if(json.items.length == 0){
							$('#warningGraph').removeClass("hide");
							$('#Graph').hide();
						}else{
							$('#Graph').show();
							var dataBar ='';
							var dataPie ='';
								dataBar = [json.items[0].P100,json.items[0].P75,json.items[0].P50,json.items[0].P25,json.items[0].P10];
								dataPie = [
								  ['100', json.items[0].P100],
								  ['75', json.items[0].P75],
								  ['50', json.items[0].P50],
								  ['25', json.items[0].P25],
								  ['10', json.items[0].P10]
								];
							
							RanderBarChart('containerBar', dataBar);
							RenderPieChart('containerPie', dataPie); 
						}
						}
						});
					}
				}
			function RenderPieChart(elementId, dataList) {
                new Highcharts.Chart({
                    chart: {
                        renderTo: elementId,
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false
                    }, 
                    credits:{
                    	enabled: false
                    },
                    exporting: {
			            enabled: false
			        },
                    title: {
                        text: "Potential "
                    },
                    tooltip: {
                        formatter: function () {
                            return '<b>' + this.point.name + '</b> : ' + this.percentage.nformat() + ' %' ;
                        }
                    },
                    /*plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                color: '#000000',
                                connectorColor: '#000000',
                                formatter: function () {
                                    return '<b>' + this.point.name + '</b> : ' + this.point.y.nformat() ;
                                }
                            }
                        },
                        series: {                        	
							 events: {
			                    click: function(event) {
									//showdetail(event.point.name,event.point.y);
			                    }
			                }
                        }
                    },*/
                    series: [{
                        type: 'pie',
                        name: 'Potential',
                        data: dataList
                    }],
                    
                    colors: [
					   '#749DD1', 
					   '#D67B76', 
					   '#C5DC96', 
					   '#AE9BC8', 
					   '#66BCD5', 
					   '#F19F55', 
					   '#84B5EF', 
					   '#8AB9EF', 
					   '#F89995', 
					   '#D8F4A0', 
					   '#C5B1E6', 
					   '#94EAFB', 
					   '#FEC283', 
					   '#CADDFB', 
					   '#FECCCD', 
					   '#EAFBCE'
					]
                });
            }
			function RanderBarChart(elementId,dataList){
				$('#'+elementId).highcharts({

			        chart: {
			            type: 'column'
			        },			         
                    credits:{
                    	enabled: false
                    },
                    exporting: {
			            enabled: false
			        },
                    title: {
                        text: "Potential "
                    },
			        xAxis: {
			            categories: ['100', '75', '50', '25', '10']  
			        },
			        /*plotOptions: {
			            series: {
			                allowPointSelect: true,
			                cursor: 'pointer',
			                point: {
			                    events: {
			                        click: function() {
										//showdetail(this.category,this.y);				
			                        }
			                    }
			                }
			            }
			        },*/

			        series: [{
			        	data: dataList      
			            ,name:"Income "        
			        }]
			    });
			}
		    });
		</script>
</form>
</body>

<!-- Mirrored from bootstrapstyler.com/preview/_/flatdashboard/ by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 18 Oct 2013 07:00:04 GMT -->
</html>