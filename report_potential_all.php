﻿<!DOCTYPE html>
<html lang="en">
	
<!-- Mirrored from bootstrapstyler.com/preview/_/flatdashboard/ui.html by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 18 Oct 2013 07:02:36 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
		<meta charset="utf-8">
		<title>SFS:::Report->Month</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="Izuddin Helmi">

		<!-- Stylesheets -->
		<link href="css/styleb.css" rel="stylesheet" type="text/css">
		<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
		<link href="js/library/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
		<link href="css/styler/style.css" rel="stylesheet">
		<link href="js/library/syntaxHighlighter/shCore.css" rel="stylesheet" type="text/css" />
		<link href="js/library/syntaxHighlighter/shThemeDefault.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="css/styler/fontello.css">
	    <link rel="stylesheet" href="css/styler/animation.css"><!--[if IE 7]>
	    <link rel="stylesheet" href="css/styler/fontello-ie7.css"><![endif]-->

		<!-- Custom Fonts -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300,500' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

		<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
		  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	</head>
<?php  $checkmenu = '4'; ?>
<body cz-shortcut-listen="true" style="margin-top:0px;">	
<div id="loading" style="height: 100%; width: 100%; position: fixed; left: 0; top: 0; z-index: 1051 !important; background-color: black; filter: alpha(opacity=9); -khtml-opacity: 0.9; -moz-opacity: 0.9; opacity: 0.9;"><img src="img/loading.gif" style="width: 150px; height: 150px; position: fixed; top: 50%; left: 50%; margin: -75px 0 0 -75px;"></div>
<section class="page-header bg-color white">
	<div class="container">
		<div class="row">
			<div class="span12">
				<div class="section-header clearfix">
					<div class="pull-left">
						<h2><i class="icon-clipboard-1"></i>Report</h2>
					</div>
					<div id="shortcut" class="nav-collapse pull-right"><ul class="shortcuts unstyled">
						<?php include("menu.php");?>
					</ul>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="content">
	<div class="page-tab">
		<div class="page-tab-header">
			<div class="container">
				<ul class="nav nav-tabs clearfix" id="socialTab">
					<li><a href="report_PT_all.php"><h5>Potential</h5></a></li>
					<li><a  href="report_all.php"><h5>Progress</h5></a></li>
					
					<li><a href="report_month_all.php"><h5>Month</h5></a></li>
					<li ><a href="report_yearly.php"><h5>Yearly</h5></a></li>
					<li><a href="report_Qur_all.php"><h5>ภาพรวมรายไตรมาส</h5></a></li>
					<li class="active"><a href="#"><h5>ภาพรวมรายเดือน</h5></a></li>
					<li ><a href="report_siteref_all.php"><h5>Site Reference หนังสือรับรอง</h5></a></li>
				</ul>
			</div>
		</div>
		<div class="tab-content">
			<div class="tab-pane fade active in container" id="Month">
				<div class="widget bg-color turqoise rounded" id="focusReport">
					<div class="box-padding">
						<h1>ภาพรวมรายเดือน</h1>
					</div>
				</div>
				<div class="row">
					<div class="span3 widget widget-option">
						<div class="profile-head bg-color dark-blue rounded-top">
							<div class="box-padding" style="text-align:center">
									<h3 class="normal"><i class="icon-calendar"></i>Year</h3>
									<span class=" pull-left" style="margin-left:-10px"></span>
							</div>
						</div>
						<div class="bg-color white rounded-bottom">
								<ul class="menu unstyled">
									<li ><p>
									  	<input type="radio"  class="optionsYear1" id="optionsRadios1" name="rdoYear1" value="bidding" style="margin-top:-2px">
									  	 Bidding Date <span class=" pull-right" style="margin-top:-8px"><select id="slc_year" name="slc_year" class="span1" style="width:80px;"></select></span></p></li>
									<li class="current"><p>
									  	<input type="radio" checked class="optionsYear2" id="optionsRadios2" name="rdoYear1" style="margin-top:-2px" value="signdate">
									  	 Signdate <span class="pull-right"style="margin-top:-8px"><select id="slc_year_sign" name="slc_year_sign" class="span1" style="width:80px;"></select></span></p></li>
								</ul>
						</div>
						<div class="profile-head bg-color dark-blue rounded-top">
							<div class="box-padding" style="text-align:center">
									<h3 class="normal"><i class="icon-th-thumb-empty"></i>Option</h3>
							</div>
						</div>
						<div class="bg-color white rounded-bottom">
								<ul class="menu unstyled">
									<li><p class="sales">
									  	<input type="checkbox" value="1" style="margin-top:-2px" class="chkSpecial"> Mega Project<span class="pull-right"style="margin-top:-8px"></span></p></li>
								</ul>
						</div>
						<div style="text-align:center"><a class="btn btn-blue btn-large" id="btnReport" style="width:190px">Report</a></div>
						<div id="showError" class="alert alert-error hide" style="margin-top:10px;margin-bottom:-10px;">
							<button type="button" class="close" data-dismiss="alert">×</button>
								<i class="icon-attention-3 pull-left" style="font-size:20px;margin-top:-3px;"></i>
								<div class="media-body">
									<strong>กรุณาเลือก Sale ด้วย</strong>
								</div>
						</div>
						<div id="showWarning" class="alert alert-block hide" style="margin-top:10px;margin-bottom:-10px;">
							<button type="button" class="close" data-dismiss="alert">×</button>
							<i class="icon-info-circle pull-left" style="font-size:25px;margin-top:-7px;"></i>
							<div class="media-body">
								<strong>ไม่พบข้อมูล</strong>
							</div>
						</div>
					</div>

					<div class="span3 widget widget-option">

						<div class="profile-head bg-color dark-blue rounded-top">
							<div class="box-padding" style="text-align:center">
									<h3 class="normal"><i class="icon-users-1"></i>Sales</h3>
									<span class=" pull-left" style="margin-left:-10px"></span>
							</div>
						</div>
						<div class="bg-color white rounded-bottom">
							<div class="accordion" id="accordion2">
				                <div class="accordion-group">
				                  <div class="accordion-heading">
				                    <a id="asales13" class="accordion-toggle collapsed clearfix" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
				                      <h4 class="pull-left" style="margin-left:25px;">Sales 1</h4>
				                       <i id="isales1" class="icon-right-dir pull-right" style="color:#43484D; font-size:18px;"></i>
				                    </a>
				                    <span class="pull-left" style="margin-left:20px;margin-top:-36px"><input type="checkbox" class="Sales1All" title="Select all"></span>
				                  </div>
				                  <div id="collapseOne" class="accordion-body collapse in" >
				                    	<ul id="list_sale1" class="menu unstyled"></ul>
				                  </div>
				                </div>
				                <div class="accordion-group">
				                  <div class="accordion-heading">
				                    <a id="asales23" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">				                      
				                      <h4 class="pull-left" style="margin-left:25px;">Sales 2</h4>
				                       <i id="isales2" class="icon-right-dir pull-right" style="color:#43484D; font-size:18px;"></i>
				                    </a>
				                    <span class="pull-left" style="margin-left:20px;margin-top:-36px"><input type="checkbox" class="Sales2All" title="Select all"></span>
				                    </a>
				                  </div>
				                  <div id="collapseTwo" class="accordion-body collapse in">
				                    	<ul id="list_sale2" class="menu unstyled"></ul>
				                  </div>
				                </div>
				                <div class="accordion-group">
				                  <div class="accordion-heading">
				                    <a id="asales43" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
				                      <h4 class="pull-left" style="margin-left:25px;">Sales 4</h4>
				                       <i id="isales4" class="icon-right-dir pull-right" style="color:#43484D; font-size:18px;"></i>
				                    </a>
				                    <span class="pull-left" style="margin-left:20px;margin-top:-36px"><input type="checkbox" class="Sales4All" title="Select all"></span>
				                    </a>
				                  </div>
				                  <div id="collapseThree" class="accordion-body collapse in">
				                    	<ul id="list_sale4" class="menu unstyled"></ul>
				                  </div>
				                </div>

				                <div class="accordion-group">
				                  <div class="accordion-heading">
				                    <a id="asalesTM3" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion2" href="#collapseFour">
				                      <h4 class="pull-left" style="margin-left:25px;">Sales TM</h4>
				                       <i id="isalesTM" class="icon-right-dir pull-right" style="color:#43484D; font-size:18px;"></i>
				                    </a>
				                    <span class="pull-left" style="margin-left:20px;margin-top:-36px"><input type="checkbox" class="SalesTMAll" title="Select all"></span>
				                    </a>
				                  </div>
								  <div id="collapseFour" class="accordion-body collapse in">
				                    	<ul id="list_saleTM" class="menu unstyled">
										</ul>
				                  </div>
				                </div>
								
								<div class="accordion-group">
				                  <div class="accordion-heading">
				                    <a id="asalesCSD4" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion2" href="#collapseFive">
				                      <h4 class="pull-left" style="margin-left:25px;">Sales CSD</h4>
				                       <i id="isalesCSD" class="icon-right-dir pull-right" style="color:#43484D; font-size:18px;"></i>
				                    </a>
				                    <span class="pull-left" style="margin-left:20px;margin-top:-36px"><input type="checkbox" class="SalesCSDAll" title="Select all"></span>
				                    </a>
				                  </div>
								  <div id="collapseFive" class="accordion-body collapse in">
				                    	<ul id="list_saleCSD" class="menu unstyled">
										</ul>
				                  </div>
				                </div>
								<div class="accordion-group">
				                  <div class="accordion-heading">
				                    <a id="asalesGEO2" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion2" href="#collapseSix">
				                      <h4 class="pull-left" style="margin-left:25px;">Sales GEO</h4>
				                       <i id="isalesGEO" class="icon-right-dir pull-right" style="color:#43484D; font-size:18px;"></i>
				                    </a>
				                    <span class="pull-left" style="margin-left:20px;margin-top:-36px"><input type="checkbox" class="SalesGEOAll" title="Select all"></span>
				                    </a>
				                  </div>
								  <div id="collapseSix" class="accordion-body collapse in">
				                    	<ul id="list_saleGEO" class="menu unstyled">
										</ul>
				                  </div>
				                </div>
								<div class="accordion-group">
				                  <div class="accordion-heading">
				                    <a id="asalesCLMV2" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion2" href="#collapseSeven">
				                      <h4 class="pull-left" style="margin-left:25px;">Sales CLMV</h4>
				                       <i id="isalesCLMV" class="icon-right-dir pull-right" style="color:#43484D; font-size:18px;"></i>
				                    </a>
				                    <span class="pull-left" style="margin-left:20px;margin-top:-36px"><input type="checkbox" class="SalesCLMVAll" title="Select all"></span>
				                    </a>
				                  </div>
								  <div id="collapseSeven" class="accordion-body collapse in">
				                    	<ul id="list_saleCLMV" class="menu unstyled">
										</ul>
				                  </div>
				                </div>
				            </div>
						</div>
					</div>
					<div id="showGraph">
					<div class="span6">
						<div class="widget-header clearfix">
							<span class="pull-left" style="text-transform: uppercase;font-size:18px;">Result</span>
						</div>
						<div class="box-padding bg-color white rounded">
							<div style="text-align: center;">
									<table class="table table-striped showdetail"  >
										<thead>
											<tr role="row">
												<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;font-size:16px;" width="100px">เดือน</th>
												<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center;font-size:16px;" width="150px">Potential>50</th>
												<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center;font-size:16px;" width="150px">Potential=50</th>
												<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center;font-size:16px;" width="150px">Potential&lt;50</th>
											</tr>
										</thead>
										<tbody id="detail_report"></tbody>
									</table>
								</table>
							</div>
							

						</div>
					</div>
					</div>
				</div>
				<div class="widget widget-chart bg-color dark-blue rounded">
					<div style="background-color: #F0AD4E;">
						<div id="charts-data" class="monthly clearfix">	
							<div class="box-padding" style="padding:20px;">
							<div class="clearfix" >
								<h2 class="charts-data-quarter pull-left" style="color: #FFFFFF;"></h2>
								<h2 id="sumResult" class="pull-right" style="margin: 10px 0;color: #FFFFFF;" title="Income">0</h2>
							</div>
							<div class="clearfix" >
								<span id="numResult" class="label label-inverse pull-left" style="font-size:20px;padding:10px;margin-right:10px;" title="Number of results">0</span>
								<font class="pull-left" style="color: #FFFFFF;font-size:16px;"><b>รายการ</b></font>
								<div class="box-padding pull-right" style="padding:5px;margin-right:-5px;">
									<label style="background-color: #DFF0D8; display: inline-block; width: 150px; text-align: center; float: right; cursor: default;color: #777777;">Potential>50</label>
									<label style="background-color: #FCF8E3; display: inline-block; width: 150px; text-align: center; float: right; cursor: default;color: #777777;">Potential=50</label>
									<label style="background-color: #F2DEDE; display: inline-block; width: 150px; text-align: center; float: right; cursor: default;color: #777777;">Potential&lt;50</label>	
								</div>
							</div>
							</div>
							<div class="charts-data-table">
								<div class="box-padding" style="padding:0px;" id="focusShow">
									<table class="table table-striped showdetail" id="detail" >
										<thead>
											<tr role="row">
												<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;" width="80px">PE Contract</th>
												<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;"width="480px">Project Name</th>
												<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;" width="120px">SalesName</th>
												<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;" width="80px">BiddingDate</th>
												<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;" width="80px">SignDate</th>
												<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align:right" width="100px">Income</th>
												<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;" width="20px"><i class='icon-newspaper' title='View'></i></th>
											</tr>
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
		<div id="footer" class="bg-color dark-blue">
			<div class="container">
				<div class="box-padding">
					Copyright &copy; 2013 Sales Forecast
				</div>
			</div>
		</div>
		<!-- Javascript -->
		<!-- JS:jquery-->
		<!--<script src="../../../../ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>-->
  		<script src="js/library/jquery/jquery.min.js"></script>
		<script src="js/library/jquery/jquery-ui.min.js"></script>

		<!-- JS:masonry-->
		<script src="js/library/masonry/jquery.masonry.min.js"></script>

		<!-- JS:flexslider-->
		<script src="js/library/flexslider/jquery.flexslider-min.js"></script>

		<!-- JS:tables-->
		<script src="js/library/dataTables/jquery.dataTables.min.js"></script>

		<!-- JS:calendar-->
		<script src="js/library/fullcalendar/fullcalendar.min.js"></script>
		<script src="js/library/fullcalendar/gcal.js"></script>

		<!-- JS:forms-->
		<script src="js/library/forms/jquery.hotkeys.js"></script>
		<script src="js/library/forms/bootstrap-wysiwyg.js"></script>
		<script src="js/library/forms/picker.js"></script>
		<script src="js/library/forms/picker.date.js"></script>
		<script src="js/library/forms/picker.time.js"></script>
		<script src="js/library/forms/jquery.bootstrap.wizard.min.js"></script>

		<!-- JS:charts-->
		<script src="js/library/charts/jquery.flot.js"></script>
		<script src="js/library/charts/jquery.flot.resize.js"></script>
		<script src="js/library/charts/jquery.flot.stack.js"></script>
		<script src="js/library/charts/jquery.flot.pie.min.js"></script>
		<script src="js/library/charts/jquery.sparkline.min.js"></script>
		<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="excanvas.min.js"></script><![endif]-->

		<!-- JS:syntaxHighlighter-->
		<script src="js/library/syntaxHighlighter/shCore.js"></script>
		<script src="js/library/syntaxHighlighter/shBrushCss.js"></script>
		<script src="js/library/syntaxHighlighter/shBrushXml.js"></script>
		<script src="js/library/syntaxHighlighter/shBrushJScript.js"></script>

		<!-- JS:bootstrap-->
		<script src="bootstrap/js/bootstrap.min.js"></script>
		
		<!-- JS:custom js for this template -->
		<script src="js/styler/custom.js"></script>

							<script src="js/highcharts.js"></script>
							<script src="js/exporting.js"></script>
		<script type="text/javascript">
			var get_month= (new Date).getMonth()+1;
			var qur_value= '';
			var qur_title= '';
			var saleid= '<?php echo $_COOKIE['Ses_ID']?>';
			var userid= '<?php echo $_COOKIE['Ses_ID']?>';
			var typeYear= 'signdate';
			var slcYear=(new Date).getFullYear();
			var showQur = get_month;
			var slcOption= '0';
			Number.prototype.nformat = function() {
			    return this.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
			};		
			function numFormat(x) {
						var parts = x.toString().split(".");
						parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
						return parts.join(".");
				}
		    $(document).ready(function(){  
				$("#slc_year_sign").disableSelection();
		    	$.ajax({ 
		    		cache: false,
					type: "POST",
					dataType: "json",
		        	url: "AJAX/select_year.php",
		        	success: function (json) {
		        		$.each(json, function() {
							var item = this;
		        			$("#slc_year").append("<option value='" + item[0] + "'>" + item[0] + "</option>");
						});
						$("#slc_year").val((new Date).getFullYear());
			        }
			    });
			    $.ajax({ 
			    	cache: false,
					type: "POST",
					dataType: "json",
		        	url: "AJAX/select_yearSign.php",
		        	success: function (json) {
		        		$.each(json, function() {
							var item = this;
		        			$("#slc_year_sign").append("<option value='" + item[0] + "'>" + item[0] + "</option>");
						});
						$("#slc_year_sign").val((new Date).getFullYear());
			        }
			    });
				$('.optionsYear1').on('click',function(){
					$('.optionsYear1').closest('li').attr("class", "current");
					$('.optionsYear2').closest('li').attr("class", "");
					$("#slc_year_sign").disableSelection();
					$("#slc_year").enableSelection();
				});
				$('.optionsYear2').on('click',function(){
					$('.optionsYear1').closest('li').attr("class", "");
					$('.optionsYear2').closest('li').attr("class", "current");
					$("#slc_year").disableSelection();
					$("#slc_year_sign").enableSelection();
				});
				///sale1
				$.ajax({ 
					type:"POST",
					dataType: "json",
					data:{
						saledept : "sales 1"
					},
			        url: "AJAX/SV_Select_SaleDept.php",
			        success: function (json) {
						var chkdep = "0";
			        	$(json).each(function () {

							//console.log(this);
							var empno=this[0];
							var name=this[1];
							if(empno == saleid){
								checked = "checked";
								classli = "current";
								chkdep = "1";
							}else{
								checked = "";
								classli = "";
							}
			        		$("#list_sale1").append("<li class='" + classli + "'><p style='padding:5px 20px;'><input type='checkbox' class='chksales1' style='margin-top:-2px' name='chkids' value='" + empno + "' " + checked + " /> " + name + "</p></li>");
						});
						if(chkdep == '1'){
							$('#asales11').removeClass("collapsed");
							$('#isales1').removeClass("icon-right-dir").addClass("icon-down-dir");
							$('#collapseOne').addClass("in");
							$('#collapseOne').css("height","auto");
							$('#isales1').removeClass("icon-right-dir").addClass("icon-down-dir");
							
							$(".Sales1All").prop("checked", true);
							$(".chksales1").prop("checked", true);
							$('.chksales1').closest('li').addClass("current");
							
						}
				$('.Sales1All').on('click',function(){
					if ($(".Sales1All").is(':checked')) {
						$(".chksales1").prop("checked", true);
						$('.chksales1').closest('li').addClass("current");
						$('#asales11').removeClass("collapsed");
						$('#isales1').removeClass("icon-right-dir").addClass("icon-down-dir");
						$('#collapseOne').addClass("in");
						$('#collapseOne').css("height","auto");
					} else {
						$(".chksales1").prop("checked", false);
						$('.chksales1').closest('li').removeClass("current");
						/*$('#asales11').addClass("collapsed");
						$('#isales1').removeClass("icon-down-dir").addClass("icon-right-dir");
						$('#collapseOne').removeClass("in");
						$('#collapseOne').css("height","0px");*/
						
					}
		    	});
				$('.chksales1').on('click',function(){
					if ( $(this).prop('checked')) {
						$(this).closest('li').attr("class", "current");
				    } else {
						$(this).closest('li').attr("class", "");
				    }
					
					if( $(".chksales1:checked").length == $(".chksales1").length){
						$(".Sales1All").prop("checked", true);
					}else{
						$(".Sales1All").prop("checked", false);
					}
				});
				$('#asales11').on('click',function(){
					if($("#collapseOne").css("height") == "0px") { 
						$('#isales1').removeClass("icon-right-dir").addClass("icon-down-dir");
				    } else {
						$('#isales1').removeClass("icon-down-dir").addClass("icon-right-dir");
				    }
				});
			        }
			    });
				//sale2
				$.ajax({ 
					type:"POST",
					dataType: "json",
					data:{
						saledept : "sales 2"
					},
			        url: "AJAX/SV_Select_SaleDept.php",
			        success: function (json) {
			        	var chkdep = "0";
			        	$(json).each(function () {

							//console.log(this);
							var empno=this[0];
							var name=this[1];
							if(empno == saleid){
								checked = "checked";
								classli = "current";
								chkdep = "1";
							}else{
								checked = "";
								classli = "";
							}
			        		$("#list_sale2").append("<li class='" + classli + "'><p style='padding:5px 20px;'><input type='checkbox' class='chksales2' style='margin-top:-2px' name='chkids' value='" + empno + "' " + checked + " /> " + name + "</p></li>");
						});
						if(chkdep == '1'){
							$('#asales21').removeClass("collapsed");
							$('#isales2').removeClass("icon-right-dir").addClass("icon-down-dir");
							$('#collapseTwo').addClass("in");
							$('#collapseTwo').css("height","auto");
							$('#isales2').removeClass("icon-right-dir").addClass("icon-down-dir");

							$(".Sales2All").prop("checked", true);
							$(".chksales2").prop("checked", true);
							$('.chksales2').closest('li').addClass("current");	
						}
				$('.Sales2All').on('click',function(){
					if ($(".Sales2All").is(':checked')) {
						$(".chksales2").prop("checked", true);
						$('.chksales2').closest('li').addClass("current");
						$('#asales21').removeClass("collapsed");
						$('#isales2').removeClass("icon-right-dir").addClass("icon-down-dir");
						$('#collapseTwo').addClass("in");
						$('#collapseTwo').css("height","auto");
					} else {
						$(".chksales2").prop("checked", false);
						$('.chksales2').closest('li').removeClass("current");
						/*$('#asales21').addClass("collapsed");
						$('#isales2').removeClass("icon-down-dir").addClass("icon-right-dir");
						$('#collapseTwo').removeClass("in");
						$('#collapseTwo').css("height","0px");*/
						
					}
		    	});
				$('.chksales2').on('click',function(){
					if ( $(this).prop('checked')) {
						$(this).closest('li').addClass("current");
				    } else {
						$(this).closest('li').removeClass("current");
				    }
					if( $(".chksales2:checked").length == $(".chksales2").length){
						$(".Sales2All").prop("checked", true);
					}else{
						$(".Sales2All").prop("checked", false);
					}
				});
				$('#asales21').on('click',function(){
					if($("#collapseTwo").css("height") == "0px") { 
						$('#isales2').removeClass("icon-right-dir").addClass("icon-down-dir");
				    } else {
						$('#isales2').removeClass("icon-down-dir").addClass("icon-right-dir");
				    }
				});
			        }
			    });
				//sale4
				$.ajax({ 
					type:"POST",
					dataType: "json",
					data:{
						saledept : "sales 4"
					},
			        url: "AJAX/SV_Select_SaleDept.php",
			        success: function (json) {
			        	var chkdep = "0";
			        	$(json).each(function () {

							//console.log(this);
							var empno=this[0];
							var name=this[1];
							if(empno == saleid){
								checked = "checked";
								classli = "current";
								chkdep = "1";
							}else{
								checked = "";
								classli = "";
							}
			        		$("#list_sale4").append("<li class='" + classli + "'><p style='padding:5px 20px;'><input type='checkbox' class='chksales4' style='margin-top:-2px' name='chkids' value='" + empno + "' " + checked + " /> " + name + "</p></li>");
						});
						if(chkdep == '1'){
							$('#asales41').removeClass("collapsed");
							$('#isales4').removeClass("icon-right-dir").addClass("icon-down-dir");
							$('#collapseThree').addClass("in");
							$('#collapseThree').css("height","auto");
							$('#isales4').removeClass("icon-right-dir").addClass("icon-down-dir");	

							$(".Sales4All").prop("checked", true);
							$(".chksales4").prop("checked", true);
							$('.chksales4').closest('li').addClass("current");
						}
				$('.Sales4All').on('click',function(){
					if ($(".Sales4All").is(':checked')) {
						$(".chksales4").prop("checked", true);
						$('.chksales4').closest('li').addClass("current");
						$('#asales41').removeClass("collapsed");
						$('#isales4').removeClass("icon-right-dir").addClass("icon-down-dir");
						$('#collapseThree').addClass("in");
						$('#collapseThree').css("height","auto");
					} else {
						$(".chksales4").prop("checked", false);
						$('.chksales4').closest('li').removeClass("current");
						/*$('#asales41').addClass("collapsed");
						$('#isales4').removeClass("icon-down-dir").addClass("icon-right-dir");
						$('#collapseThree').removeClass("in");
						$('#collapseThree').css("height","0px");*/
						
					}
		    	});
				$('.chksales4').on('click',function(){
					if ( $(this).prop('checked')) {
						$(this).closest('li').addClass("current");
				    } else {
						$(this).closest('li').removeClass("current");
				    }
					if( $(".chksales4:checked").length == $(".chksales4").length){
						$(".Sales4All").prop("checked", true);
					}else{
						$(".Sales4All").prop("checked", false);
					}
				});
				$('#asales41').on('click',function(){
					if($("#collapseThree").css("height") == "0px") { 
						$('#isales4').removeClass("icon-right-dir").addClass("icon-down-dir");
				    } else {
						$('#isales4').removeClass("icon-down-dir").addClass("icon-right-dir");
				    }
				});
			        }
			    });
				//saleTM
				$.ajax({ 
					type:"POST",
					dataType: "json",
					data:{
						saledept : "sales tm"
					},
			        url: "AJAX/SV_Select_SaleDept.php",
			        success: function (json) {
			        	var chkdep = "0";
			        	$(json).each(function () {

							//console.log(this);
							var empno=this[0];
							var name=this[1];
							if(empno == saleid){
								checked = "checked";
								classli = "current";
								chkdep = "1";
							}else{
								checked = "";
								classli = "";
							}
			        		$("#list_saleTM").append("<li class='" + classli + "'><p style='padding:5px 20px;'><input type='checkbox' class='chksalesTM' style='margin-top:-2px' name='chkids' value='" + empno + "' " + checked + " /> " + name + "</p></li>");
						});
						if(chkdep == '1'){
							$('#asalesTM1').removeClass("collapsed");
							$('#isalesTM').removeClass("icon-right-dir").addClass("icon-down-dir");
							$('#collapseFour').addClass("in");
							$('#collapseFour').css("height","auto");
							$('#isalesTM').removeClass("icon-right-dir").addClass("icon-down-dir");

							$(".SalesTMAll").prop("checked", true);
							$(".chksalesTM").prop("checked", true);
							$('.chksalesTM').closest('li').addClass("current");	
						}
				$('.SalesTMAll').on('click',function(){
					if ($(".SalesTMAll").is(':checked')) {
						$(".chksalesTM").prop("checked", true);
						$('.chksalesTM').closest('li').addClass("current");
						$('#asalesTM1').removeClass("collapsed");
						$('#isalesTM').removeClass("icon-right-dir").addClass("icon-down-dir");
						$('#collapseFour').addClass("in");
						$('#collapseFour').css("height","auto");
					} else {
						$(".chksalesTM").prop("checked", false);
						$('.chksalesTM').closest('li').removeClass("current");
						/*$('#asalesTM1').addClass("collapsed");
						$('#isalesTM').removeClass("icon-down-dir").addClass("icon-right-dir");
						$('#collapseFour').removeClass("in");
						$('#collapseFour').css("height","0px");*/
						
					}
		    	});
				$('.chksalesTM').on('click',function(){
					if ( $(this).prop('checked')) {
						$(this).closest('li').addClass("current");
				    } else {
						$(this).closest('li').removeClass("current");
				    }
					if( $(".chksalesTM:checked").length == $(".chksalesTM").length){
						$(".SalesTMAll").prop("checked", true);
					}else{
						$(".SalesTMAll").prop("checked", false);
					}
				});
				$('#asalesTM1').on('click',function(){
					if($("#collapseFour").css("height") == "0px") { 
						$('#isalesTM').removeClass("icon-right-dir").addClass("icon-down-dir");
				    } else {
						$('#isalesTM').removeClass("icon-down-dir").addClass("icon-right-dir");
				    }
				});
			        }
			    });
				
				//saleCSD
				$.ajax({ 
					type:"POST",
					dataType: "json",
					data:{
						saledept : "sales CSD"
					},
			        url: "AJAX/SV_Select_SaleDept.php",
			        success: function (json) {
			        	var chkdep = "0";
			        	$(json).each(function () {

							//console.log(this);
							var empno=this[0];
							var name=this[1];
							if(empno == saleid){
								checked = "checked";
								classli = "current";
								chkdep = "1";
							}else{
								checked = "";
								classli = "";
							}
			        		$("#list_saleCSD").append("<li class='" + classli + "'><p style='padding:5px 20px;'><input type='checkbox' class='chksalesCSD' style='margin-top:-2px' name='chkids' value='" + empno + "' " + checked + " /> " + name + "</p></li>");
						});
						if(chkdep == '1'){
							$('#asalesCSD4').removeClass("collapsed");
							$('#isalesCSD').removeClass("icon-right-dir").addClass("icon-down-dir");
							$('#collapseFive').addClass("in");
							$('#collapseFive').css("height","auto");
							$('#isalesCSD').removeClass("icon-right-dir").addClass("icon-down-dir");	

							$(".SalesCSDAll").prop("checked", true);
							$(".chksalesCSD").prop("checked", true);
							$('.chksalesCSD').closest('li').addClass("current");	
						}
				$('.SalesCSDAll').on('click',function(){
					if ($(".SalesCSDAll").is(':checked')) {
						$(".chksalesCSD").prop("checked", true);
						$('.chksalesCSD').closest('li').addClass("current");
						$('#asalesCSD4').removeClass("collapsed");
						$('#isalesCSD').removeClass("icon-right-dir").addClass("icon-down-dir");
						$('#collapseFive').addClass("in");
						$('#collapseFive').css("height","auto");
					} else {
						$(".chksalesCSD").prop("checked", false);
						$('.chksalesCSD').closest('li').removeClass("current");
						/*$('#asalesCSD4').addClass("collapsed");
						$('#isalesCSD').removeClass("icon-down-dir").addClass("icon-right-dir");
						$('#collapseFive').removeClass("in");
						$('#collapseFive').css("height","0px");*/
						
					}
		    	});
				$('.chksalesCSD').on('click',function(){
					if ( $(this).prop('checked')) {
						$(this).closest('li').addClass("current");
				    } else {
						$(this).closest('li').removeClass("current");
				    }
					if( $(".chksalesCSD:checked").length == $(".chksalesCSD").length){
						$(".SalesCSDAll").prop("checked", true);
					}else{
						$(".SalesCSDAll").prop("checked", false);
					}
				});
				$('#asalesCSD4').on('click',function(){
					if($("#collapseFive").css("height") == "0px") { 
						$('#isalesCSD').removeClass("icon-right-dir").addClass("icon-down-dir");
				    } else {
						$('#isalesCSD').removeClass("icon-down-dir").addClass("icon-right-dir");
				    }
				});
			        }
			    });
				//CMLV

				$.ajax({ 
					type:"POST",
					dataType: "json",
					data:{
						saledept : "sales CLMV"
					},
			        url: "AJAX/SV_Select_SaleDept.php",
			        success: function (json) {
			        	var chkdep = "0";
			        	$(json).each(function () {

							//console.log(this);
							var empno=this[0];
							var name=this[1];
							if(this.empno == saleid){
								checked = "checked";
								classli = "current";
								chkdep = "1";
							}else{
								checked = "";
								classli = "";
							}
			        		$("#list_saleCLMV").append("<li class='" + classli + "'><p style='padding:5px 20px;'><input type='checkbox' class='chksalesCLMV' style='margin-top:-2px' name='chkids' value='" + empno + "' " + checked + " /> " + name + "</p></li>");
						});
						if(chkdep == '1'){
							$('#asalesCLMV2').removeClass("collapsed");
							$('#isalesCLMV').removeClass("icon-right-dir").addClass("icon-down-dir");
							$('#collapseSeven').addClass("in");
							$('#collapseSeven').css("height","auto");
							$('#isalesCLMV').removeClass("icon-right-dir").addClass("icon-down-dir");	

							$(".SalesCLMVAll").prop("checked", true);
							$(".chksalesCLMV").prop("checked", true);
							$('.chksalesCLMV').closest('li').addClass("current");	
						}
				$('.SalesCLMVAll').on('click',function(){
					if ($(".SalesCLMVAll").is(':checked')) {
						$(".chksalesCLMV").prop("checked", true);
						$('.chksalesCLMV').closest('li').addClass("current");
						$('#asalesCLMV2').removeClass("collapsed");
						$('#isalesCLMV').removeClass("icon-right-dir").addClass("icon-down-dir");
						$('#collapseSeven').addClass("in");
						$('#collapseSeven').css("height","auto");
					} else {
						$(".chksalesCLMV").prop("checked", false);
						$('.chksalesCLMV').closest('li').removeClass("current");
						/*$('#asalesCSD4').addClass("collapsed");
						$('#isalesCSD').removeClass("icon-down-dir").addClass("icon-right-dir");
						$('#collapseFive').removeClass("in");
						$('#collapseFive').css("height","0px");*/
						
					}
		    	});
				$('.chksalesCLMV').on('click',function(){
					if ( $(this).prop('checked')) {
						$(this).closest('li').addClass("current");
				    } else {
						$(this).closest('li').removeClass("current");
				    }
					if( $(".chksalesCLMV:checked").length == $(".chksalesCLMV").length){
						$(".SalesCLMVAll").prop("checked", true);
					}else{
						$(".SaleCLMVAll").prop("checked", false);
					}
				});
				$('#asalesCLMV2').on('click',function(){
					if($("#collapseSeven").css("height") == "0px") { 
						$('#isalesCLMV').removeClass("icon-right-dir").addClass("icon-down-dir");
				    } else {
						$('#isalesCLMV').removeClass("icon-down-dir").addClass("icon-right-dir");
				    }
				});
			        }
			    });
				//saleGEO
				$.ajax({ 
					type:"POST",
					dataType: "json",
					data:{
						saledept : "sales geo"
					},
			        url: "AJAX/SV_Select_SaleDept.php",
			        success: function (json) {
			        	var chkdep = "0";
			        	$(json).each(function () {

							//console.log(this);
							var empno=this[0];
							var name=this[1];
							if(empno == saleid){
								checked = "checked";
								classli = "current";
								chkdep = "1";
							}else{
								checked = "";
								classli = "";
							}
			        		$("#list_saleGEO").append("<li class='" + classli + "'><p style='padding:5px 20px;'><input type='checkbox' class='chksalesGEO' style='margin-top:-2px' name='chkids' value='" + empno + "' " + checked + " /> " + name + "</p></li>");
						});
						if(chkdep == '1'){
							$('#asalesGEO2').removeClass("collapsed");
							$('#isalesGEO').removeClass("icon-right-dir").addClass("icon-down-dir");
							$('#collapseSix').addClass("in");
							$('#collapseSix').css("height","auto");
							$('#isalesGEO').removeClass("icon-right-dir").addClass("icon-down-dir");	

							$(".SalesGEOAll").prop("checked", true);
							$(".chksalesGEO").prop("checked", true);
							$('.chksalesGEO').closest('li').addClass("current");	
						}
				$('.SalesGEOAll').on('click',function(){
					if ($(".SalesGEOAll").is(':checked')) {
						$(".chksalesGEO").prop("checked", true);
						$('.chksalesGEO').closest('li').addClass("current");
						$('#asalesGEO2').removeClass("collapsed");
						$('#isalesGEO').removeClass("icon-right-dir").addClass("icon-down-dir");
						$('#collapseSix').addClass("in");
						$('#collapseSix').css("height","auto");
					} else {
						$(".chksalesGEO").prop("checked", false);
						$('.chksalesGEO').closest('li').removeClass("current");
						/*$('#asalesTM2').addClass("collapsed");
						$('#isalesTM').removeClass("icon-down-dir").addClass("icon-right-dir");
						$('#collapseFour').removeClass("in");
						$('#collapseFour').css("height","0px");*/
						
					}
		    	});
				$('.chksalesGEO').on('click',function(){
					if ( $(this).prop('checked')) {
						$(this).closest('li').addClass("current");
				    } else {
						$(this).closest('li').removeClass("current");
				    }
					if( $(".chksalesGEO:checked").length == $(".chksalesGEO").length){
						$(".SalesGEOAll").prop("checked", true);
					}else{
						$(".SalesGEOAll").prop("checked", false);
					}
				});
				$('#asalesGEO2').on('click',function(){
					if($("#collapseSix").css("height") == "0px") { 
						$('#isalesGEO').removeClass("icon-right-dir").addClass("icon-down-dir");
				    } else {
						$('#isalesGEO').removeClass("icon-down-dir").addClass("icon-right-dir");
				    }
				});
			        }
			    });
			//showReport('0');
			$("#loading").hide();
			$('#showGraph').hide();
			function showReport(thisQur) {
				$("#loading").show();
				console.log('month: ',slcYear+' '+saleid+' '+typeYear+' '+slcOption);
				$.ajax({
					type: "POST",
					dataType: "json",
					data: {
						BidingYear: slcYear,
						selectYear: typeYear,
						saleidin: saleid,
						option: slcOption
					},
					url: "AJAX/report_potential.php",
					success: function(json) {
						$("#detail_report").empty();
						var PT1=0.00;
						var PT2=0.00;
						var PT3=0.00;

						$.each(json, function() {
	    					var item = this;
							if(item[1]==null)item[1]=0.00;
							if(item[2]==null)item[2]=0.00;
							if(item[3]==null)item[3]=0.00;
							PT1+=Number(item[1]);
							PT2+=Number(item[2]);
							PT3+=Number(item[3]);
							console.log(parseFloat(PT1));
							
			        		//var tr = $('<tr><td id="td2" class="submenubpC11NBG" align=right style="background-color: #FCF8E3;" ><b>' + numFormat(parseFloat(item[2]).toFixed(2)) + '</b>&nbsp;</td><td id="td3"  class="submenubpR11NBG" align=right style="background-color: #F2DEDE;" ><b>' + numFormat(parseFloat(item[3]).toFixed(2)) + '</b>&nbsp;</td></tr>');
							//
							var newrow="";
							var cols="";
							monthNo = getMonthNo(item[0]);
							potentialG = "1";
							potentialE = "2";
							potentialL = "3";
            	    		newrow=$('<tr>').attr('class','');
							cols+='<td align="center" style="background-color:#DDF1FF"><b>' + item[0] + '</b></td>';
							cols+='<td style="cursor:pointer;text-align:right;font-size:16px;"><a href="javascript:showdetailPotential('+potentialG+','+monthNo+','+item[1]+');" title="Show detail">' + numFormat(parseFloat(item[1]).toFixed(2)) + '&nbsp;</a></td>';
							cols+='<td style="cursor:pointer;text-align:right;font-size:16px;"><a href="javascript:showdetailPotential('+potentialE+','+monthNo+','+item[2]+');" title="Show detail">' + numFormat(parseFloat(item[2]).toFixed(2)) + '&nbsp;</a></td>';
							cols+='<td  style="cursor:pointer;text-align:right;font-size:16px;"><a href="javascript:showdetailPotential('+potentialL+','+monthNo+','+item[3]+');" title="Show detail">' + numFormat(parseFloat(item[3]).toFixed(2)) + '&nbsp;</a></td>';
							newrow.append(cols);
	    					$("#detail_report").append(newrow);
							
			        	});
							var newrow="";
							var cols="";
            	    		newrow=$('<tr>').attr('class','');
							cols+='<td align="center" style="background-color:#DDF1FF;color:red;"><b>Total</b></td>';
							cols+='<td style="text-align:right;background-color:#DDF1FF;color:red;font-size:16px;"><b>' + numFormat(parseFloat(PT1).toFixed(2)) + '</b>&nbsp;</td>';
							cols+='<td style="text-align:right;background-color:#DDF1FF;color:red;font-size:16px;"><b>' + numFormat(parseFloat(PT2).toFixed(2)) + '</b>&nbsp;</td>';
							cols+='<td style="text-align:right;background-color:#DDF1FF;color:red;font-size:16px;"><b>' + numFormat(parseFloat(PT3).toFixed(2)) + '</b>&nbsp;</td>';
							newrow.append(cols);
	    					$("#detail_report").append(newrow);
					},
					error: function() {
						alert("เกิดปัญหาในการค้นหากรุณาลองใหม่อีกครั้ง");
					}
				});						
				$("#loading").hide();
			}
			
			$('#btnReport').on('click',function(){
				$("#detail_report").empty();
				$('#showGraph').show();
				$('#showError').addClass("hide");
				$('#showWarning').addClass("hide");
				if ( ($(".chksalesTM:checked").length == 0) &&($(".chksales4:checked").length == 0) &&($(".chksales2:checked").length == 0) &&($(".chksales1:checked").length == 0)&&($(".chksalesGEO:checked").length == 0) &&($(".chksalesCLMV:checked").length == 0) &&($(".chksalesCSD:checked").length == 0)) {
						$('#showError').removeClass("hide");
					}else{
						var chkid = '';
						$("input[name='chkids']").each(function () {
							if ( $(this).prop('checked')) {
								if(chkid=='')
								{
									chkid= $(this).val();
								}
								else
								{
									chkid = chkid+","+$(this).val();
								}
							}
						});

						if ($(".optionsYear1").is(':checked')) {
							typeYear=$('#optionsRadios1').val();	
							slcYear=$("#slc_year").val();
						}else{
							typeYear=$('#optionsRadios2').val();			
							slcYear=$("#slc_year_sign").val();
						}
						saleid=chkid;
						if ( $('.chkSpecial').prop('checked')) {
							slcOption="0,1";
						}else{
							slcOption="0";
						}
						showReport('0');
						
					}
			});
		});
			function getMonthNo(mName){
				var month_no = '';
				switch(mName)
				{
					case 'มกราคม':
						month_no = "1";
						break;
					case 'กุมภาพันธ์':
						month_no = "2";
						break;
					case 'มีนาคม':
						month_no = "3";
						break;
					case 'เมษายน':
						month_no = "4";
						break;
					case 'พฤษภาคม':
						month_no = "5 ";
						break;
					case 'มิถุนายน':
						month_no = "6";
						break;
					case 'กรกฎาคม':
						month_no = "7";
						break;
					case 'สิงหาคม':
						month_no = "8";
						break;
					case 'กันยายน':
						month_no = "9";
						break;
					case 'ตุลาคม':
						month_no = "10";
						break;
					case 'พฤศจิกายน':
						month_no = "11";
						break;
					default:
						month_no = "12";
				}
				return month_no;
			}
			
			function showdetailPotential(potential,slc_monthNo,sum_income){
				document.getElementById('focusShow').scrollIntoView();
				$("#loading").show();
				if(potential == '1'){
					slc_potential = 'Potential>50';
				}else if(potential == '2'){
					slc_potential = 'Potential=50';
				}else{
					slc_potential = 'Potential<50';
				}
				console.log('potential ',slc_potential);
				$(".charts-data-quarter").text(slc_potential+" ประจำเดือน " + slc_monthNo +"/"+slcYear);
				$("#sumResult").text(sum_income.nformat() + ' บาท');
				console.log('month_detail: ',slcYear+' '+saleid+' '+slc_monthNo+' '+typeYear+''+sum_income);
				
				$.ajax({
					type: "POST",
					dataType: "json",
					data: {
						BidingYear: slcYear,
						selectYear: typeYear,
						saleid: saleid,
						potential: slc_potential,
						slc_monthNo: slc_monthNo,
						option: slcOption
					},
					url: "AJAX/report_potential_datail.php",
					success: getDetail,
					error: function() {
						alert("เกิดปัญหาในการค้นหากรุณาลองใหม่อีกครั้ง");
					}
				});			
				$("#loading").hide();
			}
			function getDetail(json) {
            	    console.log("showTable:",json);
					document.getElementById('numResult').innerHTML=json.length;
					$('#detail > tbody:last').empty();
					if(json.length== 0){
						var noresult = "<tr><td colspan='7' style='text-align:center;'><p class='lead14'>ไม่พบข้อมูล</p></td></tr>";
						$('#detail').append(noresult);
					}else{
						$.each(json, function() {
							var item = this;
							var str_Potential = item[0];
							var str_PEContractNo = item[1];
							var str_Project = item[2];
							var str_SaleRepresentative = item[3];
							var str_TimeFrameBidingDate = item[4];
							var str_TimeFrameContractSigndate = item[5];
							var str_TargetIncome = numFormat(parseFloat(item[6]).toFixed(2));
							var str_SaleID = item[7];
							var str_ID = item[8];
								
							var newrow="";
							if(str_Potential>50)
							{
								newrow=$('<tr>').attr('class','success');
							}
							else if(str_Potential==50)
							{
								newrow=$('<tr>').attr('class','warning');
							}else if(str_Potential<50)
							{
								newrow=$('<tr>').attr('class','error');
							}
							var cols="";
							cols+="<td><p class='lead14'>"+str_PEContractNo+"</p></td>";
							cols+="<td><p class='lead14'>"+str_Project+"</p></td>";
							cols+="<td><p class='lead14'>"+str_SaleRepresentative+"</p></td>";
							cols+="<td><p class='lead14'>"+str_TimeFrameBidingDate+"</p></td>";
							cols+="<td><p class='lead14'>"+str_TimeFrameContractSigndate+"</p></td>";
							cols+="<td style='text-align:right'><p class='lead16'>"+str_TargetIncome+"</p></td>";
							/*if(str_SaleID==userid)
							{
								cols+="<td style='text-align:center'><a href='javascript:edit("+str_ID+");' title='Edit'><i class='i_btn icon-pencil-1 iconEdit' style='color:#2ABF9E;'></i></a></td>";
							}
							else
							{
								cols+="<td style='text-align:center'><a href='javascript:view("+str_ID+");' title='View'><i class='i_btn icon-newspaper iconView' style='color:#2ABF9E;'></i></a></td>";
							}*/
							
								cols+="<td style='text-align:center'><a href='javascript:view("+str_ID+");' title='View'><i class='i_btn icon-newspaper iconView' style='color:#2ABF9E;'></i></a></td>";
							newrow.append(cols);

							$('#detail').append(newrow);
								
						});		
					}
				$("#loading").hide();
           	}
			
			function edit(id){
				window.location="newproject.php?id="+id+"&page=report_potential_all";
			}
			function view(id){
				//window.location="previewforecast.php?id="+id+"&page=report_potential_all";

				window.open("previewforecastReport.php?id="+id);
			}
		</script>
  	</body>

<!-- Mirrored from bootstrapstyler.com/preview/_/flatdashboard/ by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 18 Oct 2013 07:00:04 GMT -->
</html>