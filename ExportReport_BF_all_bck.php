<?php
function funcSaleIn($id)
{
	$myid=split(",",$id);
	$myCount=count($myid);
	$mythiname="";
	for($i=0;$i<$myCount;$i++)
	{
		if($i===0)
		{
			$mythiname="'".$myid[$i]."'";
		}
		else
		{
			$mythiname=$mythiname.",'".$myid[$i]."'";
		}
	}
	return $mythiname;
}
function funcSpecialIn($id)
	{
		$myid=split(",",$id);
		$myCount=count($myid);
		$mythiname="";
		for($i=0;$i<$myCount;$i++)
		{
			if($i==0)
			{
				$mythiname="'".$myid[$i]."'";
			}
			else
			{
				$mythiname=$mythiname.",'".$myid[$i]."'";
			}
		}
		return $mythiname;
	}
function funcProgressIn($id)
	{
		$myid=split(",",$id);
		$myCount=count($myid);
		$mythiname="";
		if($myCount==1)
		{
			$mythiname="'".$id."'";
		}
		else
		{
			for($i=0;$i<$myCount;$i++)
			{
				if($i==0)
				{
					$mythiname="'".$myid[$i]."'";
				}
				else
				{
					$mythiname=$mythiname.",'".$myid[$i]."'";
				}
			}
		}
		return $mythiname;
	}
function myDescription($idforecast) 
{
	$mydesc="";
	include ("INC/connectSFC.php");
	$sql="SELECT TypeDescription.TypeName, DescriptionDetail.Description FROM DescriptionDetail INNER JOIN TypeDescription ON DescriptionDetail.IDTypeDescription = TypeDescription.IDTypeDescription WHERE (DescriptionDetail.IDForecast = '$idforecast')ORDER BY TypeDescription.TypeName";
	$result=sqlsrv_query($ConnectSaleForecast,$sql);
	$i=1;
	while ($obj=sqlsrv_fetch_object($result)) 
	{	
		$TypeName=$obj->TypeName;
		$Description=$obj->Description;
		if($i==1)
		{
			$mydesc=$TypeName." : ".$Description;
		}
		else
		{
			$mydesc=$mydesc." , ".$TypeName." : ".$Description;
		}
		$i++;
	}
	return $mydesc;
}
function myStatus($idforecast) 
{
	$mystat="";
	include ("INC/connectSFC.php");
	$sql="SELECT  *,convert(varchar,DateStatus,103) as DateStatus from statusDetail WHERE IDForecast = '$idforecast'  order by Year(DateStatus) desc,Month(DateStatus) desc ,day(DateStatus) desc ";
	$result=sqlsrv_query($ConnectSaleForecast,$sql);
	$i=1;
	while ($obj=sqlsrv_fetch_object($result)) 
	{	
		$DateStatus=$obj->DateStatus;
		$Description=$obj->Description;
		if($i==1)
		{
			$mystat=$DateStatus." : ".$Description;
		}
		else
		{
			$mystat=$mystat." , ".$DateStatus." : ".$Description;
		}
		$i++;
	}
	return $mystat;
}
function CheckDepartmentSupportNeeded($IDForecast,$DSNName)
{
	include("INC/connectSFC.php");
	$sql="SELECT  * from DepartmentSupportNeeded WHERE IDForecast = '$IDForecast'  and DSNName='$DSNName' ";
	$result=sqlsrv_query($ConnectSaleForecast,$sql);
	$num=sqlsrv_num_rows($result);
	if($num==0)
		$num=0;
	else
		$num=1;
	return $num;
}
function CheckApplicationSupportNeeded($IDForecast,$ASNName)
{
	include("INC/connectSFC.php");
	$sql="SELECT  * from ApplicationSupportNeeded WHERE IDForecast = '$IDForecast'  and ASNName='$ASNName' ";
	$result=sqlsrv_query($ConnectSaleForecast,$sql);
	$num=sqlsrv_num_rows($result);
	return $num;
}
function CheckESRIProduct($IDForecast,$EPName)
{
	include("INC/connectSFC.php");
	$sql="SELECT  * from ESRIProduct WHERE IDForecast = '$IDForecast'  and EPName='$EPName' ";
	$result=sqlsrv_query($ConnectSaleForecast,$sql);
	$num=0;
	while($obj=sqlsrv_fetch_object($result))
	{
		$num=$obj->qty;
	}
	return $num;
}
function CheckLeicaProduct($IDForecast,$LPName)
{
	include("INC/connectSFC.php");
	$sql="SELECT  * from LeicaProduct WHERE IDForecast = '$IDForecast'  and LPName='$LPName' ";
	$result=sqlsrv_query($ConnectSaleForecast,$sql);
	$num=0;
	while($obj=sqlsrv_fetch_object($result))
	{
		$num=$obj->qty;
	}
	return $num;
}
function CheckGEOProduct($IDForecast,$GPName)
{
	include("INC/connectSFC.php");
	$sql="SELECT  * from GEOProduct WHERE IDForecast = '$IDForecast'  and GPName='$GPName' ";
	$result=sqlsrv_query($ConnectSaleForecast,$sql);
	$num=0;
	while($obj=sqlsrv_fetch_object($result))
	{
		$num=$obj->qty;
	}
	return $num;
}
function CheckTMProduct($IDForecast,$TMPName)
{
	include("INC/connectSFC.php");
	$sql="SELECT  * from TMProduct WHERE IDForecast = '$IDForecast'  and TMPName='$TMPName' ";
	$result=sqlsrv_query($ConnectSaleForecast,$sql);
	$num=0;
	while($obj=sqlsrv_fetch_object($result))
	{
		$num=$obj->qty;
	}
	return $num;
}
	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
	header("Content-Type: application/download");
	header("Content-Transfer-Encoding: binary ");
	header("Content-type: application/ms-excel");		
	header("Content-Disposition: attachment; filename=".basename("Report.xls").";");
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-874" />
<title>:Report:</title>
<!--<style type="text/css">
td.menubb {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:14px;BORDER-BOTTOM: 1px solid blue;BORDER-LEFT: 1px solid blue;BORDER-TOP: 1px solid blue;BORDER-RIGHT: 1px solid blue; text-align: center;background:#CCFFCC;font-weight: bold;
}
td.menubbn {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;BORDER-BOTTOM: 1px solid blue;BORDER-LEFT: 1px solid blue;BORDER-TOP: 1px solid blue;BORDER-RIGHT: 1px solid blue; text-align: center;background:#CCFFCC;font-weight: bold;
}
td.menubbE {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:14px;BORDER-BOTTOM: 1px solid blue;BORDER-LEFT: 1px solid blue;BORDER-TOP: 1px solid blue;BORDER-RIGHT: 1px solid blue; text-align: center;background:#CDE6FF;font-weight: bold;
}
td.menubbnE {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;BORDER-BOTTOM: 1px solid blue;BORDER-LEFT: 1px solid blue;BORDER-TOP: 1px solid blue;BORDER-RIGHT: 1px solid blue; text-align: center;background:#CDE6FF;font-weight: bold;
}
td.menubbG {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:14px;BORDER-BOTTOM: 1px solid blue;BORDER-LEFT: 1px solid blue;BORDER-TOP: 1px solid blue;BORDER-RIGHT: 1px solid blue; text-align: center;background:#B2FFFF;font-weight: bold;
}
td.menubbnG {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;BORDER-BOTTOM: 1px solid blue;BORDER-LEFT: 1px solid blue;BORDER-TOP: 1px solid blue;BORDER-RIGHT: 1px solid blue; text-align: center;background:#B2FFFF;font-weight: bold;
}
td.menubbT {
	color:red; font-family: sans-serif , Tahoma; font-size:16px;BORDER-BOTTOM: 1px solid blue;BORDER-LEFT: 1px solid blue;BORDER-TOP: 1px solid blue;BORDER-RIGHT: 1px solid blue; text-align: left;background:#FFE6E6;font-weight: bold;
}
td.menubbTc {
	color:red; font-family: sans-serif , Tahoma; font-size:16px;BORDER-BOTTOM: 1px solid blue;BORDER-LEFT: 1px solid blue;BORDER-TOP: 1px solid blue;BORDER-RIGHT: 1px solid blue; text-align: center;background:#FFE6E6;font-weight: bold;
}
td.menubbnT {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;BORDER-BOTTOM: 1px solid blue;BORDER-LEFT: 1px solid blue;BORDER-TOP: 1px solid blue;BORDER-RIGHT: 1px solid blue; text-align: center;background:#FFB3FE;font-weight: bold;
}
td.menubbL {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:14px;BORDER-BOTTOM: 1px solid blue;BORDER-LEFT: 1px solid blue;BORDER-TOP: 1px solid blue;BORDER-RIGHT: 1px solid blue; text-align: center;background:#E6CDFF;font-weight: bold;
}
td.menubbnL {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;BORDER-BOTTOM: 1px solid blue;BORDER-LEFT: 1px solid blue;BORDER-TOP: 1px solid blue;BORDER-RIGHT: 1px solid blue; text-align: center;background:#E6CDFF;font-weight: bold;
}
td.menubbA {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:14px;BORDER-BOTTOM: 1px solid blue;BORDER-LEFT: 1px solid blue;BORDER-TOP: 1px solid blue;BORDER-RIGHT: 1px solid blue; text-align: center;background:#CCFFCC;font-weight: bold;
}
td.menubbnA {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;BORDER-BOTTOM: 1px solid blue;BORDER-LEFT: 1px solid blue;BORDER-TOP: 1px solid blue;BORDER-RIGHT: 1px solid blue; text-align: center;background:#CCFFCC;font-weight: bold;
}
td.menubbO {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:14px;BORDER-BOTTOM: 1px solid blue;BORDER-LEFT: 1px solid blue;BORDER-TOP: 1px solid blue;BORDER-RIGHT: 1px solid blue; text-align: center;background:#FFF0B3;font-weight: bold;
}
td.menubbnO {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;BORDER-BOTTOM: 1px solid blue;BORDER-LEFT: 1px solid blue;BORDER-TOP: 1px solid blue;BORDER-RIGHT: 1px solid blue; text-align: center;background:#FFF0B3;font-weight: bold;
}
td.menubbY {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:14px;BORDER-BOTTOM: 1px solid blue;BORDER-LEFT: 1px solid blue;BORDER-TOP: 1px solid blue;BORDER-RIGHT: 1px solid blue; text-align: center;background:#FFFF99;font-weight: bold;
}
td.menubbnY {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;BORDER-BOTTOM: 1px solid blue;BORDER-LEFT: 1px solid blue;BORDER-TOP: 1px solid blue;BORDER-RIGHT: 1px solid blue; text-align: center;background:#FFFF99;font-weight: bold;
}
td.menubbY2 {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:14px;BORDER-BOTTOM: 1px solid blue;BORDER-LEFT: 1px solid blue;BORDER-TOP: 1px solid blue;BORDER-RIGHT: 1px solid blue; text-align: center;background:#FFFD4D;font-weight: bold;
}
td.menubbnY2 {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;BORDER-BOTTOM: 1px solid blue;BORDER-LEFT: 1px solid blue;BORDER-TOP: 1px solid blue;BORDER-RIGHT: 1px solid blue; text-align: center;background:#FFFD4D;font-weight: bold;
}
td.menubbD {
	color:red; font-family: sans-serif , Tahoma; font-size:14px;BORDER-BOTTOM: 1px solid blue;BORDER-LEFT: 1px solid blue;BORDER-TOP: 1px solid blue;BORDER-RIGHT: 1px solid blue; text-align: center;background:#ECFFFF;font-weight: bold;
}
td.menubbnD {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;BORDER-BOTTOM: 1px solid blue;BORDER-LEFT: 1px solid blue;BORDER-TOP: 1px solid blue;BORDER-RIGHT: 1px solid blue; text-align: center;background:#ECFFFF;font-weight: bold;
}
td.menubbDl {
	color:red; font-family: sans-serif , Tahoma; font-size:14px;BORDER-BOTTOM: 1px solid blue;BORDER-LEFT: 1px solid blue;BORDER-TOP: 1px solid blue;BORDER-RIGHT: 1px solid blue; text-align: left;background:#ECFFFF;font-weight: bold;
}
td.submenubb {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;BORDER-BOTTOM: 1px solid blue;BORDER-LEFT: 1px solid blue;BORDER-TOP: 1px solid blue;BORDER-RIGHT: 1px solid blue;text-align: left;
}
td.submenubbR {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;BORDER-BOTTOM: 1px solid blue;BORDER-LEFT: 1px solid blue;BORDER-TOP: 1px solid blue;BORDER-RIGHT: 1px solid blue;text-align: right;
}
td.submenubbC {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;BORDER-BOTTOM: 1px solid blue;BORDER-LEFT: 1px solid blue;BORDER-TOP: 1px solid blue;BORDER-RIGHT: 1px solid blue;text-align: center;
}
td.submenubbCb {
	color:red; font-family: sans-serif , Tahoma; font-size:16px;BORDER-BOTTOM: 1px solid blue;BORDER-LEFT: 1px solid blue;BORDER-TOP: 1px solid blue;BORDER-RIGHT: 1px solid blue;text-align: center;font-weight: bold;
}
td.txt13 { 
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;BORDER-LEFT: 1px solid blue;BORDER-RIGHT: 1px solid blue;
}

</style> -->
</head>
<body >
<?php
function spiltword($str)
{
	$DecimalPlace = strpos($str, ":");
	$DecimalPlace1 = strpos($str1, " ");
	If($DecimalPlace > 0)
	{
		$Mystr = substr($str,0, $DecimalPlace);
	}
	else
	{
		$DecimalPlace1 = strpos($str, " ");
		If($DecimalPlace1 > 0)
		{
			$Mystr = substr($str,0, $DecimalPlace1);
		}
		else
		{
			$Mystr = substr($str,0);
		}
	}
	return trim($Mystr);
}
function convertfloat($number,$Format=6){
	$max=strlen($number);
	$DecimalPlace = strpos($number, ".");
	If($DecimalPlace > 0)
	{
		$MyNumber = substr($number,0, $DecimalPlace);
	}
	else
	{
		$MyNumber = $number;
	}
	$mymax=strlen($MyNumber);
	//echo "<br>";
	if($mymax<=4)
	{
		$digit=0;
	}
	else if($mymax==5)
	{
		$MyNumber="0".$MyNumber;
		$digit=substr($MyNumber,0-$Format, 2);
	}
	else if($mymax>=6)
	{
		$digit=substr($MyNumber,0-$Format, 2);
	}
	if($digit!=0)
	{
		$mt=substr($MyNumber,0, strlen($MyNumber)-$Format).".".$digit;
	}
	else
	{
		$mt=substr($MyNumber,0, strlen($MyNumber)-$Format);
	}
	return $mt;
}
function convertMoney($mt)
{
	if($mt==0)
	{
		$num_return="-";
	}
	else
	{
		$num_return=number_format($mt, 2);
	}
	return $num_return;
}	
function convertMoney1($number,$Format=6)
{
	$max=strlen($number);
	$DecimalPlace = strpos($number, ".");
	If($DecimalPlace > 0)
	{
		$MyNumber = substr($number,0, $DecimalPlace);
	}
	else
	{
		$MyNumber = $number;
	}
	$digit=substr($MyNumber,0-$Format, $Format);
	if($digit!=0)
	{
		$mt=substr($MyNumber,0, strlen($MyNumber)-$Format).".".$digit;
	}
	else
	{
		$mt=substr($MyNumber,0, strlen($MyNumber)-$Format);
	}
	if($mt==0)
	{
		$num_return="-";
	}
	else
	{
		$num_return=number_format($mt, 2);
	}
	return $num_return;
}	
	$wherestrYearStartSummary=" Year(TimeFrameContractSigndate) ";
	$wherestrYearStart=" Year(TimeFrameContractSigndate) ";
	$wherestrMonthStartSummary=" Month(TimeFrameContractSigndate) ";
	$wherestrMonthStart=" TimeFrameContractSigndate ";
	//$BidingYear=$year
	$BidingYear=$PotentialYear;
	$getYear=Date("Y");
	//$saleid="1455";
	$getMonth=(int)date("m");
	$getDay=(int)date("d");
	$getDay_num=array("","31","29","31","30","31","30","31","31","30","31","30","31");
	if($getDay==1)
	{
		$getMonthBack=$getDay-1;
		$getDayBack=$getDay_num[$getMonthBack];
		
		while(checkdate($getMonthBack, $getDayBack, $BidingYear))
		{
			$getDayBack=$getDay-1;
		}
	}
	else
	{
		$getDayBack=$getDay-1;
		$getMonthBack=$getMonth;
	}
	$getToday=$BidingYear.date("md");
	if($getDayBack<10)
	{
		if($getMonthBack<10)
		{
			$getTodayBack=$BidingYear."0".$getMonthBack."0".$getDayBack;
		}
		else
		{
			$getTodayBack=$BidingYear.$getMonthBack."0".$getDayBack;
		}		
	}
	else
	{
		if($getMonthBack<10)
		{
			$getTodayBack=$BidingYear."0".$getMonthBack.$getDayBack;
		}
		else
		{
			$getTodayBack=$BidingYear.$getMonthBack.$getDayBack;
		}		
	}
	$strYearStart=$BidingYear;
	$strYearStartSummary=$BidingYear;
	$month_names=array("","Jan","Fab","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
	$SL=array("","4371","0416");
	$pTotal_SL1=0;
	$pTotal_SL2=0;
	$pTotal_SL1_2=0;
	$mpTotal_SL1=0;
	$mpTotal_SL2=0;
	$mpTotal_SL1_2=0;
if($BidingYear==$getYear)
{
?>
<table cellpadding="0" cellspacing="0" Border="0"width="300%" >
	<tr Align=left><td colspan=17><font SIZE="4" COLOR="#0000FF"><B>SL1&2 Project List</B></font></td></tr>
  <tr Align="center">
	<td  class ="menubbY" rowspan="2" style="width:150px;">SL Team</td>
    <td  class ="menubbY" rowspan="2" style="width:200px;">SL Name.</td>
	<?php if($getMonth=="1")
		{
			$nameActual="1 - ".$getDayBack."Jan ".$strYearStart;
		}
		else
		{
			$nameActual="1 Jan - ".$getDayBack." ".$month_names[$getMonth]." ".$strYearStart;
		}
		$strYearStart1=$strYearStart+1;
		$strYearStart2=$strYearStart+2;
		$strYearStart3=$strYearStart+3;
	?>
		
    <td  class ="menubbY" colspan="5" ><?php echo $nameActual?></td>
	<?php
	if($getMonth=="12")
		{
			$nameForecast=$getDay." - 30 Dec  ".$strYearStart;
		}
		else
		{
			$nameForecast=$getDay." ".$month_names[$getMonth]." - 30 Dec  ".$strYearStart;
		}
	?>
    <td  class ="menubbY" colspan="5" ><?php echo $nameForecast?></td>
    <td  class ="menubbY" colspan="5" ><?php echo "Total Jan-Dec ".$strYearStart;?></td>

  </tr>
   <tr Align="center">
	<!--nameActual-->
	<td class ="menubbnO" style="width:100px;">P.O(MB)</td>
	<td class ="menubbnO" style="width:100px;">Book(MB)</td>
	<td class ="menubbnO" style="width:100px;"><?php echo "FWD ".$strYearStart1."".chr(10)."(MB)"?></td>
	<td class ="menubbnO" style="width:100px;"><?php echo "FWD ".$strYearStart2."".chr(10)."(MB)"?></td>
	<td class ="menubbnO" style="width:100px;"><?php echo "FWD ".$strYearStart3."".chr(10)."(MB)"?></td>
	<!--nameForecast-->
	<td class ="menubbnO" style="width:100px;">P.O(MB)</td>
	<td class ="menubbnO" style="width:100px;">Book(MB)</td>
	<td class ="menubbnO" style="width:100px;"><?php echo "FWD ".$strYearStart1."".chr(10)."(MB)"?></td>
	<td class ="menubbnO" style="width:100px;"><?php echo "FWD ".$strYearStart2."".chr(10)."(MB)"?></td>
	<td class ="menubbnO" style="width:100px;"><?php echo "FWD ".$strYearStart3."".chr(10)."(MB)"?></td>
	<!--Total-->
	<td class ="menubbnO" style="width:100px;">P.O(MB)</td>
	<td class ="menubbnO" style="width:100px;">Book(MB)</td>
	<td class ="menubbnO" style="width:100px;"><?php echo "FWD ".$strYearStart1."".chr(10)."(MB)"?></td>
	<td class ="menubbnO" style="width:100px;"><?php echo "FWD ".$strYearStart2."".chr(10)."(MB)"?></td>
	<td class ="menubbnO" style="width:100px;"><?php echo "FWD ".$strYearStart3."".chr(10)."(MB)"?></td>
  </tr>
<?php		$i=1;
			$GTotalSummaryTargetIncome=0;
			$GTotalSummaryBook=0;
			$GTotalSummaryForward=0;
			$GTotalSummaryForward2=0;
			$GTotalSummaryForward3=0;
			$GTotalTargetIncomeF=0;
			$GTotalBookF=0;
			$GTotalForwardF=0;
			$GTotalForward2F=0;
			$GTotalForward3F=0;
			$GTotalTargetIncomeA=0;
			$GTotalBookA=0;
			$GTotalForwardA=0;
			$GTotalForward2A=0;
			$GTotalForward3A=0;
		for($p=1;$p<=2;$p++)
		{
			$TotalSummaryTargetIncome=0;
			$TotalSummaryBook=0;
			$TotalSummaryForward=0;
			$TotalSummaryForward2=0;
			$TotalSummaryForward3=0;
			$TotalTargetIncomeF=0;
			$TotalBookF=0;
			$TotalForwardF=0;
			$TotalForward2F=0;
			$TotalForward3F=0;
			$TotalTargetIncomeA=0;
			$TotalBookA=0;
			$TotalForwardA=0;
			$TotalForward2A=0;
			$TotalForward3A=0;
			$startp=$i;
			include("INC/connectSFC.php");
			$sqlSM="select  * from SaleManager where SaleManager='$SL[$p]' order by SaleIn";
			$queSM=sqlsrv_query($ConnectSaleForecast,$sqlSM);
			$numSM=sqlsrv_num_rows($queSM);
			$numSM=$numSM+2;
			?>
			<!----Start Manager-->
			<tr>
				<td  class ="submenubbC" valign="MIDDLE" Align="center" rowspan="<?php echo $numSM;?>"><?php echo "SL".$p;?></td>
			<?php
			$TargetIncomeAM=0;
			$BookAM=0;
			$ForwardAM=0;
			$Forward2AM=0;
			$Forward3AM=0;
			if($getMonth=="1")
				{
					include("INC/connectSFC.php");
					$sqlSI="select  sum(TargetIncome) as sumTargetIncomeA,sum(Book) as sumBookA,sum(Forward)  as sumForwardA ,sum(Forward2)  as sumForward2A,sum(Forward3)  as sumForward3A  from forecast where SaleID='$SL[$p]' and $wherestrYearStart='$getYear' and $wherestrMonthStart='1' and StatusDel='0' and TargetSpecialProject='0' and  Progress='100' ";
					$queSI=sqlsrv_query($ConnectSaleForecast,$sqlSI);
					while($objSI=sqlsrv_fetch_object($queSI))
					{
						$TargetIncomeAM=$objSI->sumTargetIncomeA;
						$BookAM=$objSI->sumBookA;
						$ForwardAM=$objSI->sumForwardA;
						$Forward2AM=$objSI->sumForward2A;
						$Forward3AM=$objSI->sumForward3A;
						//$sumTotalForwardA=$sumForwardA+$sumForward2A;
					}
				}
				else
				{
					include("INC/connectSFC.php");
					$sqlSI="select  sum(TargetIncome) as sumTargetIncomeA,sum(Book) as sumBookA,sum(Forward)  as sumForwardA ,sum(Forward2)  as sumForward2A,sum(Forward3)  as sumForward3A   from forecast where SaleID='$SL[$p]' and $wherestrYearStart='$getYear'  and $wherestrMonthStart < '$getToday'  and StatusDel='0' and TargetSpecialProject='0' and  Progress='100'";
					$queSI=sqlsrv_query($ConnectSaleForecast,$sqlSI);
					while($objSI=sqlsrv_fetch_object($queSI))
					{
						$TargetIncomeAM=$objSI->sumTargetIncomeA;
						$BookAM=$objSI->sumBookA;
						$ForwardAM=$objSI->sumForwardA;
						$Forward2AM=$objSI->sumForward2A;
						$Forward3AM=$objSI->sumForward3A;
						//$sumTotalForwardA=$sumForwardA+$sumForward2A;
						
					}
				}
				include("INC/connectDB.php");
				$sql="select * from employeeesri where empno='$SL[$p]' ";
				$que=sqlsrv_query($ConnectDB,$sql);
				while($obj=sqlsrv_fetch_object($que))
				{
					$thiname=trim($obj->thiname);
				}
				$TargetIncomeAM=convertfloat($TargetIncomeAM,6);
				$BookAM=convertfloat($BookAM,6);
				$ForwardAM=convertfloat($ForwardAM,6);
				$Forward2AM=convertfloat($Forward2AM,6);
				$Forward3AM=convertfloat($Forward3AM,6);
?>
				<td  class ="submenubb" valign=top ><?php echo $thiname;?></td>
				<td  class ="submenubbC" valign=top ><?php echo convertMoney($TargetIncomeAM);?></td>
				<td  class ="submenubbC" valign=top ><?php echo convertMoney($BookAM);?></td>
				<td  class ="submenubbC" valign=top ><?php echo convertMoney($ForwardAM);?></td>
				<td  class ="submenubbC" valign=top ><?php echo convertMoney($Forward2AM);?></td>
				<td  class ="submenubbC" valign=top ><?php echo convertMoney($Forward3AM);?></td>
				<?php
				//forecast
				$TargetIncomeFM=0;
					$BookFM=0;
					$ForwardFM=0;
					$Forward2FM=0;
					$Forward3FM=0;
				if($getMonth=="12")
				{
					include("INC/connectSFC.php");
					$sqlF="select  sum(TargetIncome) as sumTargetIncomeF,sum(Book) as sumBookF,sum(Forward)  as sumForwardF ,sum(Forward2)  as sumForward2F,sum(Forward3)  as sumForward3F  from forecast where SaleID='$SL[$p]' and $wherestrYearStart='$getYear' and  $wherestrMonthStart='12' and StatusDel='0' and TargetSpecialProject='0' and  Progress not in ('100','0','v') ";
					$queF=sqlsrv_query($ConnectSaleForecast,$sqlF);
					while($objF=sqlsrv_fetch_object($queF))
					{
						$TargetIncomeFM=$objF->sumTargetIncomeF;
						$BookFM=$objF->sumBookF;
						$ForwardFM=$objF->sumForwardF;
						$Forward2FM=$objF->sumForward2F;
						$Forward3FM=$objF->sumForward3F;
					}
				}
				else
				{
					include("INC/connectSFC.php");
					$sqlF="select  sum(TargetIncome) as sumTargetIncomeF,sum(Book) as sumBookF,sum(Forward)  as sumForwardF ,sum(Forward2)  as sumForward2F,sum(Forward3)  as sumForward3F   from forecast where SaleID='$SL[$p]' and $wherestrYearStart='$getYear'  and  $wherestrMonthStart>='$getToday'  and StatusDel='0' and TargetSpecialProject='0' and  Progress not in ('100','0','v') ";
					$queF=sqlsrv_query($ConnectSaleForecast,$sqlF);
					while($objF=sqlsrv_fetch_object($queF))
					{
						$TargetIncomeFM=$objF->sumTargetIncomeF;
						$BookFM=$objF->sumBookF;
						$ForwardFM=$objF->sumForwardF;
						$Forward2FM=$objF->sumForward2F;
						$Forward3FM=$objF->sumForward3F;
						
					}
				}	
				$TargetIncomeFM=convertfloat($TargetIncomeFM,6);
				$BookFM=convertfloat($BookFM,6);
				$ForwardFM=convertfloat($ForwardFM,6);
				$Forward2FM=convertfloat($Forward2FM,6);
				$Forward3FM=convertfloat($Forward3FM,6);
				?>
				<td  class ="submenubbC" valign=top ><?php echo convertMoney($TargetIncomeFM);?></td>
				<td  class ="submenubbC" valign=top ><?php echo convertMoney($BookFM);?></td>
				<td  class ="submenubbC" valign=top ><?php echo convertMoney($ForwardFM);?></td>
				<td  class ="submenubbC" valign=top ><?php echo convertMoney($Forward2FM);?></td>
				<td  class ="submenubbC" valign=top ><?php echo convertMoney($Forward3FM);?></td>
			<?php
			$SummaryTargetIncomeM=$TargetIncomeAM+$TargetIncomeFM;
			$SummaryBookM=$BookAM+$BookFM;
			$SummaryForwardM=$ForwardAM+$ForwardFM;
			$SummaryForward2M=$Forward2AM+$Forward2FM;
			$SummaryForward3M=$Forward3AM+$sumForward3FM;

			?>
				<td  class ="submenubbC" valign=top ><?php echo convertMoney($SummaryTargetIncomeM);?></td>
				<td  class ="submenubbC" valign=top ><?php echo convertMoney($SummaryBookM);?></td>
				<td  class ="submenubbC" valign=top ><?php echo convertMoney($SummaryForwardM);?></td>
				<td  class ="submenubbC" valign=top ><?php echo convertMoney($SummaryForward2M);?></td>
				<td  class ="submenubbC" valign=top ><?php echo convertMoney($SummaryForward3M);?></td>
			</tr>
		<!----End Manager-->
			<?php
				$TotalSummaryTargetIncome=$TotalSummaryTargetIncome+$SummaryTargetIncomeM;
				$TotalSummaryBook=$TotalSummaryBook+$SummaryBookM;
				$TotalSummaryForward=$TotalSummaryForward+$SummaryForwardM;
				$TotalSummaryForward2=$TotalSummaryForward2+$SummaryForward2M;
				$TotalSummaryForward3=$TotalSummaryForward3+$SummaryForward3M;
				$TotalTargetIncomeF=$TotalTargetIncomeF+$TargetIncomeFM;
				$TotalBookF=$TotalBookF+$BookFM;
				$TotalForwardF=$TotalForwardF+$ForwardFM;
				$TotalForward2F=$TotalForward2F+$Forward2FM;
				$TotalForward3F=$TotalForward3F+$Forward3FM;
				$TotalTargetIncomeA=$TotalTargetIncomeA+$TargetIncomeAM;
				$TotalBookA=$TotalBookA+$BookAM;
				$TotalForwardA=$TotalForwardA+$ForwardAM;
				$TotalForward2A=$TotalForward2A+$Forward2AM;
				$TotalForward3A=$TotalForward3A+$Forward3AM;
			while($objSM=sqlsrv_fetch_object($queSM))
			{
				$SaleIn=$objSM->SaleIn;
				$sumTargetIncomeA=0;
				$sumBookA=0;
				$sumForwardA=0;
				$sumForward2A=0;
				$sumForward3A=0;
				if($getMonth=="1")
				{
					include("INC/connectSFC.php");
					$sqlSI="select  sum(TargetIncome) as sumTargetIncomeA,sum(Book) as sumBookA,sum(Forward)  as sumForwardA ,sum(Forward2)  as sumForward2A,sum(Forward3)  as sumForward3A  from forecast where SaleID='$SaleIn' and $wherestrYearStart='$getYear' and $wherestrMonthStart='1' and StatusDel='0' and TargetSpecialProject='0' and  Progress='100' ";
					$queSI=sqlsrv_query($ConnectSaleForecast,$sqlSI);
					while($objSI=sqlsrv_fetch_object($queSI))
					{
						$sumTargetIncomeA=$objSI->sumTargetIncomeA;
						$sumBookA=$objSI->sumBookA;
						$sumForwardA=$objSI->sumForwardA;
						$sumForward2A=$objSI->sumForward2A;
						$sumForward3A=$objSI->sumForward3A;
						//$sumTotalForwardA=$sumForwardA+$sumForward2A;
					}
				}
				else
				{
					include("INC/connectSFC.php");
					$sqlSI="select  sum(TargetIncome) as sumTargetIncomeA,sum(Book) as sumBookA,sum(Forward)  as sumForwardA ,sum(Forward2)  as sumForward2A,sum(Forward3)  as sumForward3A   from forecast where SaleID='$SaleIn' and $wherestrYearStart='$getYear'  and $wherestrMonthStart<'$getToday'  and StatusDel='0' and TargetSpecialProject='0' and  Progress='100'";
					$queSI=sqlsrv_query($ConnectSaleForecast,$sqlSI);
					while($objSI=sqlsrv_fetch_object($queSI))
					{
						$sumTargetIncomeA=$objSI->sumTargetIncomeA;
						$sumBookA=$objSI->sumBookA;
						$sumForwardA=$objSI->sumForwardA;
						$sumForward2A=$objSI->sumForward2A;
						$sumForward3A=$objSI->sumForward3A;
						//$sumTotalForwardA=$sumForwardA+$sumForward2A;
						
					}
				}
				include("INC/connectDB.php");
				$sql="select * from employeeesri where empno='$SaleIn' ";
				$que=sqlsrv_query($ConnectDB,$sql);
				while($obj=sqlsrv_fetch_object($que))
				{
					$thiname=trim($obj->thiname);
				}
				$sumTargetIncomeA=convertfloat($sumTargetIncomeA,6);
				$sumBookA=convertfloat($sumBookA,6);
				$sumForwardA=convertfloat($sumForwardA,6);
				$sumForward2A=convertfloat($sumForward2A,6);
				$sumForward3A=convertfloat($sumForward3A,6);
				?>
				<tr>
				<td  class ="submenubb" valign=top ><?php echo $thiname;?></td>
				<td  class ="submenubbC" valign=top ><?php echo convertMoney($sumTargetIncomeA);?></td>
				<td  class ="submenubbC" valign=top ><?php echo convertMoney($sumBookA);?></td>
				<td  class ="submenubbC" valign=top ><?php echo convertMoney($sumForwardA);?></td>
				<td  class ="submenubbC" valign=top ><?php echo convertMoney($sumForward2A);?></td>
				<td  class ="submenubbC" valign=top ><?php echo convertMoney($sumForward3A);?></td>
				<?php
				//forecast
				$sumTargetIncomeF=0;
					$sumBookF=0;
					$sumForwardF=0;
					$sumForward2F=0;
					$sumForward3F=0;
				if($getMonth=="12")
				{
					include("INC/connectSFC.php");
					$sqlF="select  sum(TargetIncome) as sumTargetIncomeF,sum(Book) as sumBookF,sum(Forward)  as sumForwardF ,sum(Forward2)  as sumForward2F,sum(Forward3)  as sumForward3F  from forecast where SaleID='$SaleIn' and $wherestrYearStart='$getYear' and  $wherestrMonthStart='12' and StatusDel='0' and TargetSpecialProject='0' and  Progress not in ('100','0','v') ";
					$queF=sqlsrv_query($ConnectSaleForecast,$sqlF);
					while($objF=sqlsrv_fetch_object($queF))
					{
						$sumTargetIncomeF=$objF->sumTargetIncomeF;
						$sumBookF=$objF->sumBookF;
						$sumForwardF=$objF->sumForwardF;
						$sumForward2F=$objF->sumForward2F;
						$sumForward3F=$objF->sumForward3F;
					}
				}
				else
				{
					include("INC/connectSFC.php");
					$sqlF="select  sum(TargetIncome) as sumTargetIncomeF,sum(Book) as sumBookF,sum(Forward)  as sumForwardF ,sum(Forward2)  as sumForward2F,sum(Forward3)  as sumForward3F   from forecast where SaleID='$SaleIn' and $wherestrYearStart='$getYear'  and  $wherestrMonthStart>='$getToday'  and StatusDel='0' and TargetSpecialProject='0' and  Progress not in ('100','0','v') ";
					$queF=sqlsrv_query($ConnectSaleForecast,$sqlF);
					while($objF=sqlsrv_fetch_object($queF))
					{
						$sumTargetIncomeF=$objF->sumTargetIncomeF;
						$sumBookF=$objF->sumBookF;
						$sumForwardF=$objF->sumForwardF;
						$sumForward2F=$objF->sumForward2F;
						$sumForward3F=$objF->sumForward3F;
						
					}
				}	
				$sumTargetIncomeF=convertfloat($sumTargetIncomeF,6);
				$sumBookF=convertfloat($sumBookF,6);
				$sumForwardF=convertfloat($sumForwardF,6);
				$sumForward2F=convertfloat($sumForward2F,6);
				$sumForward3F=convertfloat($sumForward3F,6);
				?>
				<td  class ="submenubbC" valign=top ><?php echo convertMoney($sumTargetIncomeF);?></td>
				<td  class ="submenubbC" valign=top ><?php echo convertMoney($sumBookF);?></td>
				<td  class ="submenubbC" valign=top ><?php echo convertMoney($sumForwardF);?></td>
				<td  class ="submenubbC" valign=top ><?php echo convertMoney($sumForward2F);?></td>
				<td  class ="submenubbC" valign=top ><?php echo convertMoney($sumForward3F);?></td>
				<?php
				$SummaryTargetIncome=$sumTargetIncomeA+$sumTargetIncomeF;
				$SummaryBook=$sumBookA+$sumBookF;
				$SummaryForward=$sumForwardA+$sumForwardF;
				$SummaryForward2=$sumForward2A+$sumForward2F;
				$SummaryForward3=$sumForward3A+$sumForward3F;
				?>
				<td  class ="submenubbC" valign=top ><?php echo convertMoney($SummaryTargetIncome);?></td>
				<td  class ="submenubbC" valign=top ><?php echo convertMoney($SummaryBook);?></td>
				<td  class ="submenubbC" valign=top ><?php echo convertMoney($SummaryForward);?></td>
				<td  class ="submenubbC" valign=top ><?php echo convertMoney($SummaryForward2);?></td>
				<td  class ="submenubbC" valign=top ><?php echo convertMoney($SummaryForward3);?></td>
				</tr>
			<?
				$i++;
				$TotalSummaryTargetIncome=$TotalSummaryTargetIncome+$SummaryTargetIncome;
				$TotalSummaryBook=$TotalSummaryBook+$SummaryBook;
				$TotalSummaryForward=$TotalSummaryForward+$SummaryForward;
				$TotalSummaryForward2=$TotalSummaryForward2+$SummaryForward2;
				$TotalSummaryForward3=$TotalSummaryForward3+$SummaryForward3;
				$TotalTargetIncomeF=$TotalTargetIncomeF+$sumTargetIncomeF;
				$TotalBookF=$TotalBookF+$sumBookF;
				$TotalForwardF=$TotalForwardF+$sumForwardF;
				$TotalForward2F=$TotalForward2F+$sumForward2F;
				$TotalForward3F=$TotalForward3F+$sumForward3F;
				$TotalTargetIncomeA=$TotalTargetIncomeA+$sumTargetIncomeA;
				$TotalBookA=$TotalBookA+$sumBookA;
				$TotalForwardA=$TotalForwardA+$sumForwardA;
				$TotalForward2A=$TotalForward2A+$sumForward2A;
				$TotalForward3A=$TotalForward3A+$sumForward3A;
			}
			?>
			<tr>
				<td  class ="menubbDl" valign=top ><?php echo "Total SL".$p;?></td>
				<td  class ="menubbD" valign=top ><?php echo convertMoney($TotalTargetIncomeA);?></td>
				<td  class ="menubbD" valign=top ><?php echo convertMoney($TotalBookA);?></td>
				<td  class ="menubbD" valign=top ><?php echo convertMoney($TotalForwardA);?></td>
				<td  class ="menubbD" valign=top ><?php echo convertMoney($TotalForward2A);?></td>
				<td  class ="menubbD" valign=top ><?php echo convertMoney($TotalForward3A);?></td>
				<td  class ="menubbD" valign=top ><?php echo convertMoney($TotalTargetIncomeF);?></td>
				<td  class ="menubbD" valign=top ><?php echo convertMoney($TotalBookF);?></td>
				<td  class ="menubbD" valign=top ><?php echo convertMoney($TotalForwardF);?></td>
				<td  class ="menubbD" valign=top ><?php echo convertMoney($TotalForward2F);?></td>
				<td  class ="menubbD" valign=top ><?php echo convertMoney($TotalForward3F);?></td>
				<td  class ="menubbD" valign=top ><?php echo convertMoney($TotalSummaryTargetIncome);?></td>
				<td  class ="menubbD" valign=top ><?php echo convertMoney($TotalSummaryBook);?></td>
				<td  class ="menubbD" valign=top ><?php echo convertMoney($TotalSummaryForward);?></td>
				<td  class ="menubbD" valign=top ><?php echo convertMoney($TotalSummaryForward2);?></td>
				<td  class ="menubbD" valign=top ><?php echo convertMoney($TotalSummaryForward3);?></td>
			</tr>
				<?php
			        $GTotalSummaryTargetIncome=$TotalSummaryTargetIncome+$GTotalSummaryTargetIncome;
				$GTotalSummaryBook=$TotalSummaryBook+$GTotalSummaryBook;
				$GTotalSummaryForward=$TotalSummaryForward+$GTotalSummaryForward;
				$GTotalSummaryForward2=$TotalSummaryForward2+$GTotalSummaryForward2;
				$GTotalSummaryForward3=$TotalSummaryForward3+$GTotalSummaryForward3;
				$GTotalTargetIncomeF=$TotalTargetIncomeF+$GTotalTargetIncomeF;
				$GTotalBookF=$TotalBookF+$GTotalBookF;
				$GTotalForwardF=$TotalForwardF+$GTotalForwardF;
				$GTotalForward2F=$TotalForward2F+$GTotalForward2F;
				$GTotalForward3F=$TotalForward3F+$GTotalForward3F;
				$GTotalTargetIncomeA=$TotalTargetIncomeA+$GTotalTargetIncomeA;
				$GTotalBookA=$TotalBookA+$GTotalBookA;
				$GTotalForwardA=$TotalForwardA+$GTotalForwardA;
				$GTotalForward2A=$TotalForward2A+$GTotalForward2A;
				$GTotalForward3A=$TotalForward3A+$GTotalForward3A;
		}
		?>	
		<tr>
				<td  class ="submenubbC" valign=top > </td>
				<td  class ="menubbT" valign=top >Total SL1+SL2</td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($GTotalTargetIncomeA);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($GTotalBookA);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($GTotalForwardA);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($GTotalForward2A);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($GTotalForward3A);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($GTotalTargetIncomeF);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($GTotalBookF);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($GTotalForwardF);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($GTotalForward2F);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($GTotalForward3F);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($GTotalSummaryTargetIncome);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($GTotalSummaryBook);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($GTotalSummaryForward);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($GTotalSummaryForward2);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($GTotalSummaryForward3);?></td>
			</tr>
			<tr><td colspan=17> <td></tr>
			<!--Special Project List-->
		<tr align=left><td colspan=17><FONT SIZE="4" COLOR="#0000FF"><B>SL1&2 Special Project List</B></FONT></td></tr>
		<?php
			$SGTotalSummaryTargetIncome=0;
			$SGTotalSummaryBook=0;
			$SGTotalSummaryForward=0;
			$SGTotalSummaryForward2=0;
			$SGTotalSummaryForward3=0;
			$SGTotalTargetIncomeF=0;
			$SGTotalBookF=0;
			$SGTotalForwardF=0;
			$SGTotalForward2F=0;
			$SGTotalForward3F=0;
			$SGTotalTargetIncomeA=0;
			$SGTotalBookA=0;
			$SGTotalForwardA=0;
			$SGTotalForward2A=0;
			$SGTotalForward3A=0;
		for($mp=1;$mp<=2;$mp++)
		{
			$TotalSummaryTargetIncome=0;
			$TotalSummaryBook=0;
			$TotalSummaryForward=0;
			$TotalSummaryForward2=0;
			$TotalSummaryForward3=0;
			$TotalTargetIncomeF=0;
			$TotalBookF=0;
			$TotalForwardF=0;
			$TotalForward2F=0;
			$TotalForward3F=0;
			$TotalTargetIncomeA=0;
			$TotalBookA=0;
			$TotalForwardA=0;
			$TotalForward2A=0;
			$TotalForward3A=0;
			$startmp=$i;
			include("INC/connectSFC.php");
			$sqlSM="select  * from SaleManager where SaleManager='$SL[$mp]' order by SaleIn";
			$queSM=sqlsrv_query($ConnectSaleForecast,$sqlSM);
			$numSM2=sqlsrv_num_rows($queSM);
			$queSM2=sqlsrv_query($ConnectSaleForecast,$sqlSM);
			$sqlM1="select  * from forecast where SaleID='$SL[$mp]' and $wherestrYearStart='$getYear'  and StatusDel='0' and TargetSpecialProject='1' and  Progress in ('100','90','80','70','60','50','30','10')";
			$queM1=sqlsrv_query($ConnectSaleForecast,$sqlM1);
			$num1=sqlsrv_num_rows($queM1);
			$numSSS=$num1;
			while($objSM2=sqlsrv_fetch_object($queSM2))
			{
				$SaleIDM=$objSM2->SaleIn;
				include("INC/connectSFC.php");
				$sqlM="select  * from forecast where SaleID='$SaleIDM' and $wherestrYearStart='$getYear'  and StatusDel='0' and TargetSpecialProject='1' and  Progress in ('100','90','80','70','60','50','30','10')";
				$queM=sqlsrv_query($ConnectSaleForecast,$sqlM);
				$num2=sqlsrv_num_rows($queM);
				$numSSS=$numSSS+$num2;
			}
			?><tr>
				<td  class ="submenubbC" valign="MIDDLE" align="center" rowspan="<?php echo $numSSS+1?>" id="SPL<?php echo $mp;?>" name="SPL<?php echo $mp;?>"><?php echo "SL".$mp;?></td>
				
			<?php //start manager
				include("INC/connectSFC.php");
				$sqlM="select  * from forecast where SaleID='$SL[$mp]' and $wherestrYearStart='$getYear'  and StatusDel='0' and TargetSpecialProject='1' and  Progress in ('100','90','80','70','60','50','30','10')";
				$queM=sqlsrv_query($ConnectSaleForecast,$sqlM);
				$iii=1;
				while($objM=sqlsrv_fetch_object($queM))
				{
					$IDForecast=$objM->IDForecast;
					$SaleRepresentative=$objM->SaleRepresentative;
					$Project=$objM->Project;
					$SplitProject=spiltword($Project);
					if($iii!=1)
					{
						echo "<tr>";
						$iii++;
					}
					?>
					<td  class ="submenubb" valign=top ><?php echo $SaleRepresentative."(".$SplitProject.")";?></td>
					<?php
					$TargetIncomeAM=0;
					$BookAM=0;
					$ForwardAM=0;
					$Forward2AM=0;
					$Forward3AM=0;
					if($getMonth=="1")
					{
						include("INC/connectSFC.php");
						$sqlA="select  * from forecast where IDForecast='$IDForecast'  and $wherestrYearStart='$getYear' and $wherestrMonthStart='1' and StatusDel='0' and TargetSpecialProject='1' and Progress='100'";
						$queA=sqlsrv_query($ConnectSaleForecast,$sqlA);
						while($objA=sqlsrv_fetch_object($queA))
						{
							$TargetIncomeAM=$objA->TargetIncome;
							$BookAM=$objA->Book;
							$ForwardAM=$objA->Forward;
							$Forward2AM=$objA->Forward2;							
							$Forward3AM=$objA->Forward3;
						}
					}
					else
					{
						include("INC/connectSFC.php");
						$sqlA="select  *  from forecast where IDForecast='$IDForecast' and $wherestrYearStart='$getYear'  and $wherestrMonthStart<'$getToday'  and StatusDel='0' and TargetSpecialProject='1' and Progress='100'";
						$queA=sqlsrv_query($ConnectSaleForecast,$sqlA);
						while($objA=sqlsrv_fetch_object($queA))
						{
							$TargetIncomeAM=$objA->TargetIncome;
							$BookAM=$objA->Book;
							$ForwardAM=$objA->Forward;
							$Forward2AM=$objA->Forward2;						
							$Forward3AM=$objA->Forward3;
							
						}
					}
					
					$TargetIncomeAM=convertfloat($TargetIncomeAM,6);
					$BookAM=convertfloat($BookAM,6);
					$ForwardAM=convertfloat($ForwardAM,6);
					$Forward2AM=convertfloat($Forward2AM,6);
					$Forward3AM=convertfloat($Forward3AM,6);
					?>

						<td  class ="submenubbC" valign=top ><?php echo convertMoney($TargetIncomeAM);?></td>
						<td  class ="submenubbC" valign=top ><?php echo convertMoney($BookAM);?></td>
						<td  class ="submenubbC" valign=top ><?php echo convertMoney($ForwardAM);?></td>
						<td  class ="submenubbC" valign=top ><?php echo convertMoney($Forward2AM);?></td>
						<td  class ="submenubbC" valign=top ><?php echo convertMoney($Forward3AM);?></td>
					<?php //FORECAST
					$TargetIncomeFM=0;
					$BookFM=0;
					$ForwardFM=0;
					$Forward2FM=0;
					$Forward3FM=0;
					if($getMonth=="12")
					{
						include("INC/connectSFC.php");
						$sqlF="select  * from forecast where IDForecast='$IDForecast' and $wherestrYearStart='$getYear' and $wherestrMonthStart='12' and StatusDel='0' and TargetSpecialProject='1' and  Progress in ('90','80','70','60','50','30','10')";
						$queF=sqlsrv_query($ConnectSaleForecast,$sqlF);
						while($objF=sqlsrv_fetch_object($queF))
						{
							$TargetIncomeFM=$objF->TargetIncome;
							$BookFM=$objF->Book;
							$ForwardFM=$objF->Forward;
							$Forward2FM=$objF->Forward2;
							$Forward3FM=$objF->Forward3;
						}
					}
					else
					{
						include("INC/connectSFC.php");
						$sqlF="select  * from forecast where IDForecast='$IDForecast' and $wherestrYearStart='$getYear' and $wherestrMonthStart>='$getToday' and StatusDel='0' and TargetSpecialProject='1' and  Progress in ('90','80','70','60','50','30','10')";
						$queF=sqlsrv_query($ConnectSaleForecast,$sqlF);
						while($objF=sqlsrv_fetch_object($queF))
						{
							$TargetIncomeFM=$objF->TargetIncome;
							$BookFM=$objF->Book;
							$ForwardFM=$objF->Forward;
							$Forward2FM=$objF->Forward2;
							$Forward3FM=$objF->Forward3;
							
						}
					}
					$TargetIncomeFM=convertfloat($TargetIncomeFM,6);
					$BookFM=convertfloat($BookFM,6);
					$ForwardFM=convertfloat($ForwardFM,6);
					$Forward2FM=convertfloat($Forward2FM,6);
					$Forward3FM=convertfloat($Forward3FM,6);
					?>
					<td  class ="submenubbC" valign=top ><?php echo convertMoney($TargetIncomeFM);?></td>
					<td  class ="submenubbC" valign=top ><?php echo convertMoney($BookFM);?></td>
					<td  class ="submenubbC" valign=top ><?php echo convertMoney($ForwardFM);?></td>
					<td  class ="submenubbC" valign=top ><?php echo convertMoney($Forward2FM);?></td>
					<td  class ="submenubbC" valign=top ><?php echo convertMoney($Forward3FM);?></td>
					<?php
					$SummaryTargetIncomeM=$TargetIncomeAM+$TargetIncomeFM;
					$SummaryBookM=$BookAM+$BookFM;
					$SummaryForwardM=$ForwardAM+$ForwardFM;
					$SummaryForward2M=$Forward2AM+$Forward2FM;
					$SummaryForward3M=$Forward3AM+$Forward3FM;
					?>
					<td  class ="submenubbC" valign=top ><?php echo convertMoney($SummaryTargetIncomeM);?></td>
					<td  class ="submenubbC" valign=top ><?php echo convertMoney($SummaryBookM);?></td>
					<td  class ="submenubbC" valign=top ><?php echo convertMoney($SummaryForwardM);?></td>
					<td  class ="submenubbC" valign=top ><?php echo convertMoney($SummaryForward2M);?></td>
					<td  class ="submenubbC" valign=top ><?php echo convertMoney($SummaryForward3M);?></td>
					</tr><?php
					$TotalSummaryTargetIncome=$TotalSummaryTargetIncome+$SummaryTargetIncomeM;
					$TotalSummaryBook=$TotalSummaryBook+$SummaryBookM;
					$TotalSummaryForward=$TotalSummaryForward+$SummaryForwardM;
					$TotalSummaryForward2=$TotalSummaryForward2+$SummaryForward2M;
					$TotalSummaryForward3=$TotalSummaryForward3+$SummaryForward3M;
					$TotalTargetIncomeF=$TotalTargetIncomeF+$TargetIncomeFM;
					$TotalBookF=$TotalBookF+$BookFM;
					$TotalForwardF=$TotalForwardF+$ForwardFM;
					$TotalForward2F=$TotalForward2F+$Forward2FM;
					$TotalForward3F=$TotalForward3F+$Forward3FM;
					$TotalTargetIncomeA=$TotalTargetIncomeA+$TargetIncomeAM;
					$TotalBookA=$TotalBookA+$BookAM;
					$TotalForwardA=$TotalForwardA+$ForwardAM;
					$TotalForward2A=$TotalForward2A+$Forward2AM;
					$TotalForward3A=$TotalForward3A+$Forward3AM;
				}
				//$iii=1;
			while($objSM=sqlsrv_fetch_object($queSM))
			{
				$SaleIDM=$objSM->SaleIn;
				include("INC/connectSFC.php");
				$sqlM="select  * from forecast where SaleID='$SaleIDM' and $wherestrYearStart='$getYear'  and StatusDel='0' and TargetSpecialProject='1' and  Progress in ('100','90','80','70','60','50','30','10')";
				$queM=sqlsrv_query($ConnectSaleForecast,$sqlM);
				while($objM=sqlsrv_fetch_object($queM))
				{
					$IDForecast=$objM->IDForecast;
					$SaleRepresentative=$objM->SaleRepresentative;
					$Project=$objM->Project;
					$SplitProject=spiltword($Project);
					if($iii!=1)
					{
					    echo "<tr>";
						$iii++;
					}?>
					<td  class ="submenubb" valign=top ><?php echo $SaleRepresentative."(".$SplitProject.")";?></td>
					<?php
					$TargetIncomeA=0;
					$BookA=0;
					$ForwardA=0;
					$Forward2A=0;
					$Forward3A=0;
					if($getMonth=="1")
					{
						include("INC/connectSFC.php");
						$sqlA="select  * from forecast where IDForecast='$IDForecast'  and $wherestrYearStart='$getToday' and $wherestrMonthStart='1' and StatusDel='0' and TargetSpecialProject='1' and  Progress='100'";
						$queA=sqlsrv_query($ConnectSaleForecast,$sqlA);
						while($objA=sqlsrv_fetch_object($queA))
						{
							$TargetIncomeA=$objA->TargetIncome;
							$BookA=$objA->Book;
							$ForwardA=$objA->Forward;
							$Forward2A=$objA->Forward2;
							$Forward3A=$objA->Forward3;
						}
					}
					else
					{
						include("INC/connectSFC.php");
						$sqlA="select  *  from forecast where IDForecast='$IDForecast' and $wherestrYearStart='$getYear'  and $wherestrMonthStart < '$getToday'  and StatusDel='0' and TargetSpecialProject='1' and Progress='100'";
						$queA=sqlsrv_query($ConnectSaleForecast,$sqlA);
						while($objA=sqlsrv_fetch_object($queA))
						{
							$TargetIncomeA=$objA->TargetIncome;
							$BookA=$objA->Book;
							$ForwardA=$objA->Forward;
							$Forward2A=$objA->Forward2;
							$Forward3A=$objA->Forward3;
							
						}
					}
					$TargetIncomeA=convertfloat($TargetIncomeA,6);
					$BookA=convertfloat($BookA,6);
					$ForwardA=convertfloat($ForwardA,6);
					$Forward2A=convertfloat($Forward2A,6);
					$Forward3A=convertfloat($Forward3A,6);
					?>
						
						<td  class ="submenubbC" valign=top ><?php echo convertMoney($TargetIncomeA);?></td>
						<td  class ="submenubbC" valign=top ><?php echo convertMoney($BookA);?></td>
						<td  class ="submenubbC" valign=top ><?php echo convertMoney($ForwardA);?></td>
						<td  class ="submenubbC" valign=top ><?php echo convertMoney($Forward2A);?></td>
						<td  class ="submenubbC" valign=top ><?php echo convertMoney($Forward3A);?></td>
					<?php //FORECAST
					$TargetIncomeF=0;
					$BookF=0;
					$ForwardF=0;
					$Forward2F=0;
					$Forward3F=0;
					if($getMonth=="12")
					{
						include("INC/connectSFC.php");
						$sqlF="select  * from forecast where IDForecast='$IDForecast' and $wherestrYearStart='$getYear' and $wherestrMonthStart='12' and StatusDel='0' and TargetSpecialProject='1' and  Progress in ('90','80','70','60','50','30','10')";
						$queF=sqlsrv_query($ConnectSaleForecast,$sqlF);
						while($objF=sqlsrv_fetch_object($queF))
						{
							$TargetIncomeF=$objF->TargetIncome;
							$BookF=$objF->Book;
							$ForwardF=$objF->Forward;
							$Forward2F=$objF->Forward2;
							$Forward3F=$objF->Forward3;
						}
					}
					else
					{
						include("INC/connectSFC.php");
						$sqlF="select  * from forecast where IDForecast='$IDForecast' and $wherestrYearStart='$getYear' and $wherestrMonthStart>='$getToday' and StatusDel='0' and TargetSpecialProject='1' and  Progress in ('90','80','70','60','50','30','10')";
						$queF=sqlsrv_query($ConnectSaleForecast,$sqlF);
						while($objF=sqlsrv_fetch_object($queF))
						{
							$TargetIncomeF=$objF->TargetIncome;
							$BookF=$objF->Book;
							$ForwardF=$objF->Forward;
							$Forward2F=$objF->Forward2;
							$Forward3F=$objF->Forward3;
							
						}
					}
					$TargetIncomeF=convertfloat($TargetIncomeF,6);
					$BookF=convertfloat($BookF,6);
					$ForwardF=convertfloat($ForwardF,6);
					$Forward2F=convertfloat($Forward2F,6);
					$Forward3F=convertfloat($Forward3F,6);
					?>
					<td  class ="submenubbC" valign=top ><?php echo convertMoney($TargetIncomeF);?></td>
					<td  class ="submenubbC" valign=top ><?php echo convertMoney($BookF);?></td>
					<td  class ="submenubbC" valign=top ><?php echo convertMoney($ForwardF);?></td>
					<td  class ="submenubbC" valign=top ><?php echo convertMoney($Forward2F);?></td>
					<td  class ="submenubbC" valign=top ><?php echo convertMoney($Forward3F);?></td>
					<?php 
					$SummaryTargetIncome=$TargetIncomeA+$TargetIncomeF;
					$SummaryBook=$BookA+$BookF;
					$SummaryForward=$ForwardA+$ForwardF;
					$SummaryForward2=$Forward2A+$Forward2F;
					$SummaryForward3=$Forward3A+$Forward3F;
					?>
					<td  class ="submenubbC" valign=top ><?php echo convertMoney($SummaryTargetIncome);?></td>
					<td  class ="submenubbC" valign=top ><?php echo convertMoney($SummaryBook);?></td>
					<td  class ="submenubbC" valign=top ><?php echo convertMoney($SummaryForward);?></td>
					<td  class ="submenubbC" valign=top ><?php echo convertMoney($SummaryForward2);?></td>
					<td  class ="submenubbC" valign=top ><?php echo convertMoney($SummaryForward3);?></td>
					</tr><?php
				$TotalSummaryTargetIncome=$TotalSummaryTargetIncome+$SummaryTargetIncome;
				$TotalSummaryBook=$TotalSummaryBook+$SummaryBook;
				$TotalSummaryForward=$TotalSummaryForward+$SummaryForward;
				$TotalSummaryForward2=$TotalSummaryForward2+$SummaryForward2;
				$TotalSummaryForward3=$TotalSummaryForward3+$SummaryForward3;
				$TotalTargetIncomeF=$TotalTargetIncomeF+$TargetIncomeF;
				$TotalBookF=$TotalBookF+$BookF;
				$TotalForwardF=$TotalForwardF+$ForwardF;
				$TotalForward2F=$TotalForward2F+$Forward2F;
				$TotalForward3F=$TotalForward3F+$Forward3F;
				$TotalTargetIncomeA=$TotalTargetIncomeA+$TargetIncomeA;
				$TotalBookA=$TotalBookA+$BookA;
				$TotalForwardA=$TotalForwardA+$ForwardA;
				$TotalForward2A=$TotalForward2A+$Forward2A;
				$TotalForward3A=$TotalForward3A+$Forward3A;
				}				
			}
				if($iii!=1)
				{
					echo "<tr>";
					$iii++;
				}?>
				
				<td  class ="menubbDl" valign=top ><?php echo "Total SL".$mp;?></td>
				<td  class ="menubbD" valign=top ><?php echo convertMoney($TotalTargetIncomeA);?></td>
				<td  class ="menubbD" valign=top ><?php echo convertMoney($TotalBookA);?></td>
				<td  class ="menubbD" valign=top ><?php echo convertMoney($TotalForwardA);?></td>
				<td  class ="menubbD" valign=top ><?php echo convertMoney($TotalForward2A);?></td>
				<td  class ="menubbD" valign=top ><?php echo convertMoney($TotalForward3A);?></td>
				<td  class ="menubbD" valign=top ><?php echo convertMoney($TotalTargetIncomeF);?></td>
				<td  class ="menubbD" valign=top ><?php echo convertMoney($TotalBookF);?></td>
				<td  class ="menubbD" valign=top ><?php echo convertMoney($TotalForwardF);?></td>
				<td  class ="menubbD" valign=top ><?php echo convertMoney($TotalForward2F);?></td>
				<td  class ="menubbD" valign=top ><?php echo convertMoney($TotalForward3F);?></td>
				<td  class ="menubbD" valign=top ><?php echo convertMoney($TotalSummaryTargetIncome);?></td>
				<td  class ="menubbD" valign=top ><?php echo convertMoney($TotalSummaryBook);?></td>
				<td  class ="menubbD" valign=top ><?php echo convertMoney($TotalSummaryForward);?></td>
				<td  class ="menubbD" valign=top ><?php echo convertMoney($TotalSummaryForward2);?></td>
				<td  class ="menubbD" valign=top ><?php echo convertMoney($TotalSummaryForward3);?></td>
			</tr>
		<?php
				$SGTotalSummaryTargetIncome=$TotalSummaryTargetIncome+$SGTotalSummaryTargetIncome;
				$SGTotalSummaryBook=$TotalSummaryBook+$SGTotalSummaryBook;
				$SGTotalSummaryForward=$TotalSummaryForward+$SGTotalSummaryForward;
				$SGTotalSummaryForward2=$TotalSummaryForward2+$SGTotalSummaryForward2;
				$SGTotalSummaryForward3=$TotalSummaryForward3+$SGTotalSummaryForward3;
				$SGTotalTargetIncomeF=$TotalTargetIncomeF+$SGTotalTargetIncomeF;
				$SGTotalBookF=$TotalBookF+$SGTotalBookF;
				$SGTotalForwardF=$TotalForwardF+$SGTotalForwardF;
				$SGTotalForward2F=$TotalForward2F+$SGTotalForward2F;
				$SGTotalForward3F=$TotalForward3F+$SGTotalForward3F;
				$SGTotalTargetIncomeA=$TotalTargetIncomeA+$SGTotalTargetIncomeA;
				$SGTotalBookA=$TotalBookA+$SGTotalBookA;
				$SGTotalForwardA=$TotalForwardA+$GSTotalForwardA;
				$SGTotalForward2A=$TotalForward2A+$SGTotalForward2A;
				$SGTotalForward3A=$TotalForward3A+$SGTotalForward3A;
		}
		?>	
		<tr>
				<td  class ="submenubbC" valign=top > </td>
				<td  class ="menubbT" valign=top >Total SL1+SL2</td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($SGTotalTargetIncomeA);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($SGTotalBookA);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($SGTotalForwardA);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($SGTotalForward2A);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($SGTotalForward3A);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($SGTotalTargetIncomeF);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($SGTotalBookF);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($SGTotalForwardF);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($SGTotalForward2F);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($SGTotalForward3F);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($SGTotalSummaryTargetIncome);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($SGTotalSummaryBook);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($SGTotalSummaryForward);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($SGTotalSummaryForward2);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($SGTotalSummaryForward3);?></td>
			</tr>
			<?php
				$TSGTotalSummaryTargetIncome=$GTotalSummaryTargetIncome+$SGTotalSummaryTargetIncome;
				$TSGTotalSummaryBook=$GTotalSummaryBook+$SGTotalSummaryBook;
				$TSGTotalSummaryForward=$GTotalSummaryForward+$SGTotalSummaryForward;
				$TSGTotalSummaryForward2=$GTotalSummaryForward2+$SGTotalSummaryForward2;
				$TSGTotalSummaryForward3=$GTotalSummaryForward3+$SGTotalSummaryForward3;
				$TSGTotalTargetIncomeF=$GTotalTargetIncomeF+$SGTotalTargetIncomeF;
				$TSGTotalBookF=$GTotalBookF+$SGTotalBookF;
				$TSGTotalForwardF=$GTotalForwardF+$SGTotalForwardF;
				$TSGTotalForward2F=$GTotalForward2F+$SGTotalForward2F;
				$TSGTotalForward3F=$GTotalForward3F+$SGTotalForward3F;
				$TSGTotalTargetIncomeA=$GTotalTargetIncomeA+$SGTotalTargetIncomeA;
				$TSGTotalBookA=$GTotalBookA+$SGTotalBookA;
				$TSGTotalForwardA=$GTotalForwardA+$GSTotalForwardA;
				$TSGTotalForward2A=$GTotalForward2A+$SGTotalForward2A;
				$TSGTotalForward3A=$GTotalForward3A+$SGTotalForward3A;
				
				
				?>
			<tr>
				<td  class ="menubbTc" valign=top colspan=2>Grand Total SL1+SL2</td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($TSGTotalTargetIncomeA);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($TSGTotalBookA);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($TSGTotalForwardA);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($TSGTotalForward2A);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($TSGTotalForward3A);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($TSGTotalTargetIncomeF);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($TSGTotalBookF);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($TSGTotalForwardF);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($TSGTotalForward2F);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($TSGTotalForward3F);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($TSGTotalSummaryTargetIncome);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($TSGTotalSummaryBook);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($TSGTotalSummaryForward);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($TSGTotalSummaryForward2);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($TSGTotalSummaryForward3);?></td>
			</tr>
	</table>
 <?php
 }
 else
 {
 //difference year
 ?>
 <table cellpadding="0" cellspacing="0" border="0"width="300%" >
	<tr align=left><td colspan=17><FONT SIZE="4" COLOR="#0000FF"><B>SL1&2 Project List</B></FONT></td></tr>
  <tr align="center">
	<td  class ="menubbY" rowspan="2" style="width:150px;">SL Team</td>
    <td  class ="menubbY" rowspan="2" style="width:200px;">SL Name.</td>
	<?php	$strYearStart1=$strYearStart+1;
		$strYearStart2=$strYearStart+2;
		$strYearStart3=$strYearStart+3;
	?>
		
    
    <td  class ="menubbY" colspan="5" ><?php echo "Total Jan-Dec ".$strYearStart;?></td>

  </tr>
   <tr align="center">
	<!--Total-->
	<td class ="menubbnO" style="width:100px;">P.O(MB)</td>
	<td class ="menubbnO" style="width:100px;">Book(MB)</td>
	<td class ="menubbnO" style="width:100px;"><?php echo "FWD ".$strYearStart1."".chr(10)."(MB)"?></td>
	<td class ="menubbnO" style="width:100px;"><?php echo "FWD ".$strYearStart2."".chr(10)."(MB)"?></td>
	<td class ="menubbnO" style="width:100px;"><?php echo "FWD ".$strYearStart3."".chr(10)."(MB)"?></td>
  </tr>
<?php		$i=1;
			$GTotalSummaryTargetIncome=0;
			$GTotalSummaryBook=0;
			$GTotalSummaryForward=0;
			$GTotalSummaryForward2=0;
			$GTotalSummaryForward3=0;
			$GTotalTargetIncomeF=0;
			$GTotalBookF=0;
			$GTotalForwardF=0;
			$GTotalForward2F=0;
			$GTotalForward3F=0;
			$GTotalTargetIncomeA=0;
			$GTotalBookA=0;
			$GTotalForwardA=0;
			$GTotalForward2A=0;
			$GTotalForward3A=0;
			$PGTotalTargetIncomeA=0;
			$PGTotalBookA=0;
			$PGTotalForwardA=0;
			$PGTotalForward2A=0;
			$PGTotalForward3A=0;
		for($p=1;$p<=2;$p++)
		{
			$TotalSummaryTargetIncome=0;
			$TotalSummaryBook=0;
			$TotalSummaryForward=0;
			$TotalSummaryForward2=0;
			$TotalSummaryForward3=0;
			$TotalTargetIncomeF=0;
			$TotalBookF=0;
			$TotalForwardF=0;
			$TotalForward2F=0;
			$TotalForward3F=0;
			$TotalTargetIncomeA=0;
			$TotalBookA=0;
			$TotalForwardA=0;
			$TotalForward2A=0;
			$TotalForward3A=0;
			$startp=$i;
			
			include("INC/connectSFC.php");
			$sqlSM="select  * from SaleManager where SaleManager='$SL[$p]' order by SaleIn";
			$queSM=sqlsrv_query($ConnectSaleForecast,$sqlSM);
			$numSM=sqlsrv_num_rows($queSM);
			$numSM=$numSM+2;
			?>
			<!----Start Manager-->
			<tr>
				<td  class ="submenubbC" valign="MIDDLE" align="center" rowspan="<?php echo $numSM;?>"><?php echo "SL".$p;?></td>
			<?php
			$TargetIncomeAM=0;
			$BookAM=0;
			$ForwardAM=0;
			$Forward2AM=0;
			$Forward3AM=0;
					include("INC/connectSFC.php");
					$sqlSI="select  sum(TargetIncome) as sumTargetIncomeA,sum(Book) as sumBookA,sum(Forward)  as sumForwardA ,sum(Forward2)  as sumForward2A,sum(Forward3)  as sumForward3A   from forecast where SaleID='$SL[$p]' and $wherestrYearStart='$strYearStart'   and StatusDel='0' and TargetSpecialProject='0' and  Progress not in ('100','0','v') ";
					$queSI=sqlsrv_query($ConnectSaleForecast,$sqlSI);
					while($objSI=sqlsrv_fetch_object($queSI))
					{
						$TargetIncomeAM=$objSI->sumTargetIncomeA;
						$BookAM=$objSI->sumBookA;
						$ForwardAM=$objSI->sumForwardA;
						$Forward2AM=$objSI->sumForward2A;
						$Forward3AM=$objSI->sumForward3A;
						//$sumTotalForwardA=$sumForwardA+$sumForward2A;
						
					}
				
				include("INC/connectDB.php");
				$sql="select * from employeeesri where empno='$SL[$p]' ";
				$que=sqlsrv_query($ConnectDB,$sql);

				while($obj=sqlsrv_fetch_object($que))
				{
					$thiname=trim($obj->thiname);
				}
				$TargetIncomeAM=convertfloat($TargetIncomeAM,6);
				$BookAM=convertfloat($BookAM,6);
				$ForwardAM=convertfloat($ForwardAM,6);
				$Forward2AM=convertfloat($Forward2AM,6);
				$Forward3AM=convertfloat($Forward3AM,6);
?>
				<td  class ="submenubb" valign=top ><?php echo $thiname;?></td>
				<td  class ="submenubbC" valign=top ><?php echo convertMoney($TargetIncomeAM);?></td>
				<td  class ="submenubbC" valign=top ><?php echo convertMoney($BookAM);?></td>
				<td  class ="submenubbC" valign=top ><?php echo convertMoney($ForwardAM);?></td>
				<td  class ="submenubbC" valign=top ><?php echo convertMoney($Forward2AM);?></td>
				<td  class ="submenubbC" valign=top ><?php echo convertMoney($Forward3AM);?></td>
				<?php
			while($objSM=sqlsrv_fetch_object($queSM))
			{
				$SaleIn=$objSM->SaleIn;
				$sumTargetIncomeA=0;
				$sumBookA=0;
				$sumForwardA=0;
				$sumForward2A=0;
				$sumForward3A=0;
					include("INC/connectSFC.php");
					$sqlSI="select  sum(TargetIncome) as sumTargetIncomeA,sum(Book) as sumBookA,sum(Forward)  as sumForwardA ,sum(Forward2)  as sumForward2A,sum(Forward3)  as sumForward3A   from forecast where SaleID='$SaleIn' and $wherestrYearStart='$strYearStart'   and StatusDel='0' and TargetSpecialProject='0' and  Progress not in ('100','0','v')";
					$queSI=sqlsrv_query($ConnectSaleForecast,$sqlSI);
					while($objSI=sqlsrv_fetch_object($queSI))
					{
						$sumTargetIncomeA=$objSI->sumTargetIncomeA;
						$sumBookA=$objSI->sumBookA;
						$sumForwardA=$objSI->sumForwardA;
						$sumForward2A=$objSI->sumForward2A;
						$sumForward3A=$objSI->sumForward3A;
						//$sumTotalForwardA=$sumForwardA+$sumForward2A;
						
					}
				include("INC/connectDB.php");
				$sql="select * from employeeesri where empno='$SaleIn' ";
				$que=sqlsrv_query($ConnectDB,$sql);
				while($obj=sqlsrv_fetch_object($que))
				{
					$thiname=trim($obj->thiname);
				}
				$sumTargetIncomeA=convertfloat($sumTargetIncomeA,6);
				$sumBookA=convertfloat($sumBookA,6);
				$sumForwardA=convertfloat($sumForwardA,6);
				$sumForward2A=convertfloat($sumForward2A,6);
				$sumForward3A=convertfloat($sumForward3A,6);
				?>
				<tr>
				<td  class ="submenubb" valign=top ><?php echo $thiname;?></td>
				<td  class ="submenubbC" valign=top ><?php echo convertMoney($sumTargetIncomeA);?></td>
				<td  class ="submenubbC" valign=top ><?php echo convertMoney($sumBookA);?></td>
				<td  class ="submenubbC" valign=top ><?php echo convertMoney($sumForwardA);?></td>
				<td  class ="submenubbC" valign=top ><?php echo convertMoney($sumForward2A);?></td>
				<td  class ="submenubbC" valign=top ><?php echo convertMoney($sumForward3A);?></td>
				</tr>
			<?php
				$i++;
				$TotalSummaryTargetIncome=$TotalSummaryTargetIncome+$sumTargetIncomeA;
				$TotalSummaryBook=$TotalSummaryBook+$sumBookA;
				$TotalSummaryForward=$TotalSummaryForward+$sumForwardA;
				$TotalSummaryForward2=$TotalSummaryForward2+$sumForward2A;
				$TotalSummaryForward3=$TotalSummaryForward3+$sumForward3A;
			}
			?>
			<tr>
				<td  class ="menubbDl" valign=top ><?php echo "Total SL".$p;?></td>
				<td  class ="menubbD" valign=top ><?php echo convertMoney($TotalSummaryTargetIncome);?></td>
				<td  class ="menubbD" valign=top ><?php echo convertMoney($TotalSummaryBook);?></td>
				<td  class ="menubbD" valign=top ><?php echo convertMoney($TotalSummaryForward);?></td>
				<td  class ="menubbD" valign=top ><?php echo convertMoney($TotalSummaryForward2);?></td>
				<td  class ="menubbD" valign=top ><?php echo convertMoney($TotalSummaryForward3);?></td>
			</tr>
				<?php
				
				$PGTotalTargetIncomeA=$TotalSummaryTargetIncome+$PGTotalTargetIncomeA;
				$PGTotalBookA=$TotalSummaryBook+$PGTotalBookA;
				$PGTotalForwardA=$TotalSummaryForward+$PGTotalForwardA;
				$PGTotalForward2A=$TotalSummaryForward2+$PGTotalForward2A;
				$PGTotalForward3A=$TotalSummaryForward3+$PGTotalForward3A;
		}
		?>	
		<tr>
				<td  class ="submenubbC" valign=top > </td>
				<td  class ="menubbT" valign=top >Total SL1+SL2</td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($PGTotalTargetIncomeA);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($PGTotalBookA);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($PGTotalForwardA);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($PGTotalForward2A);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($PGTotalForward3A);?></td>
			</tr>
			
<!--*********************************			
**************************************->		
			<!--Special Project List-->
		<tr align=left><td colspan=17><FONT SIZE="4" COLOR="#0000FF"><B>SL1&2 Special Project List</B></FONT></td></tr>
		<?php
			$SGTotalSummaryTargetIncome=0;
			$SGTotalSummaryBook=0;
			$SGTotalSummaryForward=0;
			$SGTotalSummaryForward2=0;
			$SGTotalSummaryForward3=0;
			$SGTotalTargetIncomeF=0;
			$SGTotalBookF=0;
			$SGTotalForwardF=0;
			$SGTotalForward2F=0;
			$SGTotalForward3F=0;
			$SGTotalTargetIncomeA=0;
			$SGTotalBookA=0;
			$SGTotalForwardA=0;
			$SGTotalForward2A=0;
			$SGTotalForward3A=0;
		for($mp=1;$mp<=2;$mp++)
		{
			$TotalTargetIncomeA=0;
			$TotalBookA=0;
			$TotalForwardA=0;
			$TotalForward2A=0;
			$TotalForward3A=0;
			$startmp=$i;
			include("INC/connectSFC.php");
			$sqlSM="select  * from SaleManager where SaleManager='$SL[$mp]' order by SaleIn";
			$queSM=sqlsrv_query($ConnectSaleForecast,$sqlSM);
			$numSM2=sqlsrv_num_rows($queSM);
			$queSM2=sqlsrv_query($ConnectSaleForecast,$sqlSM);
			include("INC/connectSFC.php");
			$sqlM1="select  * from forecast where SaleID='$SL[$mp]' and $wherestrYearStart='$strYearStart'  and StatusDel='0' and TargetSpecialProject='1' and  Progress not in ('100','0','v')";
			$queM1=sqlsrv_query($ConnectSaleForecast,$sqlM1);
			$num1=sqlsrv_num_rows($queM1);
			$numSSS=0;
			while($objSM2=sqlsrv_fetch_object($queSM2))
			{
				$SaleIDM=$objSM2->SaleIn;
				include("INC/connectSFC.php");
				$sqlM="select  * from forecast where SaleID='$SaleIDM' and $wherestrYearStart='$strYearStart'  and StatusDel='0' and TargetSpecialProject='1' and  Progress not in ('100','0','v')";
				$queM=sqlsrv_query($ConnectSaleForecast,$sqlM);
				$num2=sqlsrv_num_rows($queM);
				$numSSS=$numSSS+$num2;
			}
			$numSSS=$numSSS+$num1;
			?><tr>
				<td  class ="submenubbC" valign="MIDDLE" Align="center" rowspan="<?php echo $numSSS+1?>" id="SPL<?php echo $mp;?>" name="SPL<?php echo $mp;?>"><?php echo "SL".$mp;?></td>
				<?php
			//start manager
				include("INC/connectSFC.php");
				$sqlM="select  * from forecast where SaleID='$SL[$mp]' and $wherestrYearStart='$strYearStart'  and StatusDel='0' and TargetSpecialProject='1' and  Progress not in ('100','0','v')";
				$queM=sqlsrv_query($ConnectSaleForecast,$sqlM);
				$iii=1;
				while($objM=sqlsrv_fetch_object($queM))
				{
					$IDForecast=$objM->IDForecast;
					$SaleRepresentative=$objM->SaleRepresentative;
					$Project=$objM->Project;
					$SplitProject=spiltword($Project);
					?><td  class ="submenubb" valign=top ><?php echo $SaleRepresentative."(".$SplitProject.")";?></td>
					<?php
					$TargetIncomeAM=0;
					$BookAM=0;
					$ForwardAM=0;
					$Forward2AM=0;
					$Forward3AM=0;
						include("INC/connectSFC.php");
						$sqlA="select  *  from forecast where IDForecast='$IDForecast' and $wherestrYearStart='$strYearStart'  and StatusDel='0' and TargetSpecialProject='1' and Progress not in ('100','0','v')";
						$queA=sqlsrv_query($ConnectSaleForecast,$sqlA);
						while($objA=sqlsrv_fetch_object($queA))
						{
							$TargetIncomeAM=$objA->TargetIncome;
							$BookAM=$objA->Book;
							$ForwardAM=$objA->Forward;
							$Forward2AM=$objA->Forward2;						
							$Forward3AM=$objA->Forward3;
							
						}
					if($iii!=1)
					{
						echo "<tr>";
						$iii++;
					}
					$TargetIncomeAM=convertfloat($TargetIncomeAM,6);
					$BookAM=convertfloat($BookAM,6);
					$ForwardAM=convertfloat($ForwardAM,6);
					$Forward2AM=convertfloat($Forward2AM,6);
					$Forward3AM=convertfloat($Forward3AM,6);
					?>
						<td  class ="submenubb" valign=top ><?php echo $thiname;?></td>
						<td  class ="submenubbC" valign=top ><?php echo convertMoney($TargetIncomeAM);?></td>
						<td  class ="submenubbC" valign=top ><?php echo convertMoney($BookAM);?></td>
						<td  class ="submenubbC" valign=top ><?php echo convertMoney($ForwardAM);?></td>
						<td  class ="submenubbC" valign=top ><?php echo convertMoney($Forward2AM);?></td>
						<td  class ="submenubbC" valign=top ><?php echo convertMoney($Forward3AM);?></td>
					</tr>
<?php
					$TotalSummaryTargetIncome=$TotalSummaryTargetIncome+$TargetIncomeAM;
					$TotalSummaryBook=$TotalSummaryBook+$BookAM;
					$TotalSummaryForward=$TotalSummaryForward+$ForwardAM;
					$TotalSummaryForward2=$TotalSummaryForward2+$Forward2AM;
					$TotalSummaryForward3=$TotalSummaryForward3+$Forward3AM;
				}
			while($objSM=sqlsrv_fetch_object($queSM))
			{
				$SaleIDM=$objSM->SaleIn;
				include("INC/connectSFC.php");
				$sqlM="select  * from forecast where SaleID='$SaleIDM' and $wherestrYearStart='$strYearStart'  and StatusDel='0' and TargetSpecialProject='1' and  Progress not in ('100','0','v')";
				$queM=sqlsrv_query($ConnectSaleForecast,$sqlM);
				while($objM=sqlsrv_fetch_object($queM))
				{
					$IDForecast=$objM->IDForecast;
					$SaleRepresentative=$objM->SaleRepresentative;
					$Project=$objM->Project;
					$SplitProject=spiltword($Project);
					if($iii!=1)
					{
					    echo "<tr>";
						$iii++;
					}?>
					<td  class ="submenubb" valign=top ><?php echo $SaleRepresentative."(".$SplitProject.")";?></td>
					<?php
					$TargetIncomeA=0;
					$BookA=0;
					$ForwardA=0;
					$Forward2A=0;
					$Forward3A=0;
						include("INC/connectSFC.php");
						$sqlA="select  *  from forecast where IDForecast='$IDForecast' and $wherestrYearStart='$strYearStart'   and StatusDel='0' and TargetSpecialProject='1' and Progress not in ('100','0','v')";
						$queA=sqlsrv_query($ConnectSaleForecast,$sqlA);
						while($objA=sqlsrv_fetch_object($queA))
						{
							$TargetIncomeA=$objA->TargetIncome;
							$BookA=$objA->Book;
							$ForwardA=$objA->Forward;
							$Forward2A=$objA->Forward2;
							$Forward3A=$objA->Forward3;
							
						}
					$TargetIncomeA=convertfloat($TargetIncomeA,6);
					$BookA=convertfloat($BookA,6);
					$ForwardA=convertfloat($ForwardA,6);
					$Forward2A=convertfloat($Forward2A,6);
					$Forward3A=convertfloat($Forward3A,6);
					?>
						
						<td  class ="submenubbC" valign=top ><?php echo convertMoney($TargetIncomeA);?></td>
						<td  class ="submenubbC" valign=top ><?php echo convertMoney($BookA);?></td>
						<td  class ="submenubbC" valign=top ><?php echo convertMoney($ForwardA);?></td>
						<td  class ="submenubbC" valign=top ><?php echo convertMoney($Forward2A);?></td>
						<td  class ="submenubbC" valign=top ><?php echo convertMoney($Forward3A);?></td>
					</tr>
				<?php
				$TotalTargetIncomeA=$TotalTargetIncomeA+$TargetIncomeA;
				$TotalBookA=$TotalBookA+$BookA;
				$TotalForwardA=$TotalForwardA+$ForwardA;
				$TotalForward2A=$TotalForward2A+$Forward2A;
				$TotalForward3A=$TotalForward3A+$Forward3A;
				}				
			}
			if($iii!=1)
				{
					echo "<tr>";
					$iii++;
				}?>
				<td  class ="menubbDl" valign=top ><?php echo "Total SL".$mp;?></td>
				<td  class ="menubbD" valign=top ><?php echo convertMoney($TotalTargetIncomeA);?></td>
				<td  class ="menubbD" valign=top ><?php echo convertMoney($TotalBookA);?></td>
				<td  class ="menubbD" valign=top ><?php echo convertMoney($TotalForwardA);?></td>
				<td  class ="menubbD" valign=top ><?php echo convertMoney($TotalForward2A);?></td>
				<td  class ="menubbD" valign=top ><?php echo convertMoney($TotalForward3A);?></td>
			</tr>
		<?php
				$SGTotalTargetIncomeA=$TotalTargetIncomeA+$SGTotalTargetIncomeA;
				$SGTotalBookA=$TotalBookA+$SGTotalBookA;
				$SGTotalForwardA=$TotalForwardA+$GSTotalForwardA;
				$SGTotalForward2A=$TotalForward2A+$SGTotalForward2A;
				$SGTotalForward3A=$TotalForward3A+$SGTotalForward3A;
		}
		?>	
		<tr>
				<td  class ="submenubbC" valign=top > </td>
				<td  class ="menubbT" valign=top >Total SL1+SL2</td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($SGTotalTargetIncomeA);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($SGTotalBookA);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($GSTotalForwardA);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($SGTotalForward2A);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($SGTotalForward3A);?></td>
			</tr><?php
				$TSGTotalTargetIncomeA=$PGTotalTargetIncomeA+$SGTotalTargetIncomeA;
				$TSGTotalBookA=$PGTotalBookA+$SGTotalBookA;
				$TSGTotalForwardA=$PGTotalForwardA+$GSTotalForwardA;
				$TSGTotalForward2A=$PGTotalForward2A+$SGTotalForward2A;
				$TSGTotalForward3A=$PGTotalForward3A+$SGTotalForward3A;
				
				
				?>
			<tr>
				<td  class ="menubbTc" valign=top colspan=2>Grand Total SL1+SL2</td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($TSGTotalTargetIncomeA);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($TSGTotalBookA);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($TSGTotalForwardA);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($TSGTotalForward2A);?></td>
				<td  class ="submenubbCb" valign=top ><?php echo convertMoney($TSGTotalForward3A);?></td>
			</tr>
	</table>
 <?php
 }
 ?>
</body>
</html>
