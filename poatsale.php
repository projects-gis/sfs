﻿<!DOCTYPE html>
<html lang="en">
<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
		<meta charset="utf-8">
		<title>SFS:::Postsales</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="Izuddin Helmi">

		<!-- Stylesheets -->
		<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
		<link href="js/library/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
		<link href="css/styler/style.css" rel="stylesheet">
		<link href="js/library/syntaxHighlighter/shCore.css" rel="stylesheet" type="text/css" />
		<link href="js/library/syntaxHighlighter/shThemeDefault.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="css/styler/fontello.css">
	    <link rel="stylesheet" href="css/styler/animation.css"><!--[if IE 7]>
	    <link rel="stylesheet" href="css/styler/fontello-ie7.css"><![endif]-->
		<link rel="stylesheet" href="css/main.css">
	    

		<!-- Custom Fonts -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300,500' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

		<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
		  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
<!-- Javascript -->
		<!-- JS:jquery-->
		<!--<script src="../../../../ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>-->
  		<script src="js/library/jquery/jquery.min.js"></script>
		<script src="js/library/jquery/jquery-ui.min.js"></script>

		<!-- JS:masonry-->
		<script src="js/library/masonry/jquery.masonry.min.js"></script>

		<!-- JS:flexslider-->
		<script src="js/library/flexslider/jquery.flexslider-min.js"></script>

		<!-- JS:tables-->
		<script src="js/library/dataTables/jquery.dataTables.min.js"></script>

		<!-- JS:calendar-->
		<script src="js/library/fullcalendar/fullcalendar.min.js"></script>
		<script src="js/library/fullcalendar/gcal.js"></script>

		<!-- JS:forms-->
		<script src="js/library/forms/jquery.hotkeys.js"></script>
		<script src="js/library/forms/bootstrap-wysiwyg.js"></script>
		<script src="js/library/forms/picker.js"></script>
		<script src="js/library/forms/picker.date.js"></script>
		<script src="js/library/forms/picker.time.js"></script>
		<script src="js/library/forms/jquery.bootstrap.wizard.min.js"></script>

		<!-- JS:charts-->
		<script src="js/library/charts/jquery.flot.js"></script>
		<script src="js/library/charts/jquery.flot.resize.js"></script>
		<script src="js/library/charts/jquery.flot.stack.js"></script>
		<script src="js/library/charts/jquery.flot.pie.min.js"></script>
		<script src="js/library/charts/jquery.sparkline.min.js"></script>
		<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="excanvas.min.js"></script><![endif]-->

		<!-- JS:syntaxHighlighter-->
		<script src="js/library/syntaxHighlighter/shCore.js"></script>
		<script src="js/library/syntaxHighlighter/shBrushCss.js"></script>
		<script src="js/library/syntaxHighlighter/shBrushXml.js"></script>
		<script src="js/library/syntaxHighlighter/shBrushJScript.js"></script>

		<!-- JS:bootstrap-->
		<script src="bootstrap/js/bootstrap.min.js"></script>
		
		<!-- JS:custom js for this template -->
		<script src="js/styler/custom.js"></script>
		<script src="js/set_cookie.js"></script>
		<script type="text/javascript">
			var GlobalSaleID = "<?php echo $_COOKIE['Ses_ID']?>";
			Number.prototype.nformat = function() {
			    return this.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
			};
		    $(document).ready(function(){
				
		    	/*$('.widget-sales').on('click','#loadsales',function(){
		        	$('#myModal').modal('show');
		    	});
				$('#example > tbody:last').empty();
				var check_SesKeyword = getCookie("Ses_Keyword");
				if (check_SesKeyword!=null && check_SesKeyword!=""){
					document.getElementById("txtKeyword").value = check_SesKeyword;
					deleteCookie("Ses_Keyword");
					search();
				}else{
					$("#loading").show();
					$.ajax({ 
						dataType: "json",
						url: "http://157.179.28.116/sfs/sv.svc/forecast_select_saleid?callback=?",
						data: {
							saleid: GlobalSaleID
						},
						success: getSearch
					});
				}
				 
				$( "#btnSearch" ).on( "click", search);
				$('#txtKeyword').keyup(function(e) {
					if(e.keyCode == 13) {
						search();
					}
				});*/
		    });
			
		function search(){
			$("#loading").show();
			var Keyword = document.getElementById("txtKeyword").value;
			if(Keyword.replace(" ","") == ""){
				$.ajax({ 
					type:"POST",
					dataType: "json",
					url: "forecast_select_saleid.php",
					data: {
						saleid: GlobalSaleID
					},
					success: getSearch
			});
			}else{
				Keyword=Keyword.replace("[","[[]");//แก้ปัญหาเรื่อง Wildcard 
				console.log("Keyword : ",Keyword );
				$.ajax({ 
					type:"POST",
					dataType: "json",
					url: "Search_Select_keyword.php",
					data: {
						saleid: GlobalSaleID,
						keyword: Keyword
					},
					success: getSearch
			});
			}
		}
		function getSearch(json) {
					document.getElementById('sumResult').innerHTML=json.items.length;
					$('#example > tbody:last').empty();
					if(json.items.length== 0){
						var noresult = "<tr><td colspan='7' style='text-align:center;'><p class='lead14'>ไม่พบข้อมูล</p></td></tr>";
						$('#example').append(noresult);
					}else{
            	    for(i=0;i<json.items.length;i++)
            	    {
            	    	var newrow="";
            	    	if(json.items[i].Potential>50)
            	    	{
            	    		newrow=$('<tr>').attr('class','success');
            	    	}
            	    	else if(json.items[i].Potential==50)
            	    	{
            	    		newrow=$('<tr>').attr('class','warning');
            	    	}else if(json.items[i].Potential<50)
            	    	{
            	    		newrow=$('<tr>').attr('class','error');
            	    	}
						
						var PEContractNo = (json.items[i].PEContractNo!= null)?json.items[i].PEContractNo:"";
						var Project = (json.items[i].Project!= null)?json.items[i].Project:"";
						var SaleRepresentative = (json.items[i].SaleRepresentative!= null)?json.items[i].SaleRepresentative:"";
						var TimeFrameBidingDate = (json.items[i].TimeFrameBidingDate!= null)?json.items[i].TimeFrameBidingDate:"";
						var TimeFrameContractSigndate = (json.items[i].TimeFrameContractSigndate!= null)?json.items[i].TimeFrameContractSigndate:"";
						var TargetIncome = (json.items[i].TargetIncome!= null)?json.items[i].TargetIncome.nformat():"";

            	    	var cols="";

            	    	cols+="<td><p class='lead14'>"+PEContractNo+"</p></td>";
            	    	cols+="<td><p class='lead14'>"+Project+"</p></td>";
            	    	cols+="<td><p class='lead14'>"+SaleRepresentative+"</p></td>";
            	    	cols+="<td><p class='lead14'>"+TimeFrameBidingDate+"</p></td>";
            	    	cols+="<td><p class='lead14'>"+TimeFrameContractSigndate+"</p></td>";
            	    	cols+="<td style='text-align:right'><p class='lead16'>"+TargetIncome+"</p></td>";
            	    	if(json.items[i].SaleID==GlobalSaleID)
            	    	{
            	    		cols+="<td style='text-align:center'><a href='javascript:edit("+json.items[i].ID+");' title='Edit'><i class='i_btn icon-pencil-1 iconEdit'></i></a></td>";
            	    	}
            	    	else
            	    	{
            	    		cols+="<td style='text-align:center'><a href='javascript:view("+json.items[i].ID+");' title='View'><i class='i_btn icon-newspaper iconView'></i></a></td>";
            	    	}
            	    	newrow.append(cols);

            	    	$('#example').append(newrow);
            	    }
					$('.iconEdit').tooltip({
						placement: 'bottom',
						title : 'Edit'
					});
					$('.iconView').tooltip({
						placement: 'bottom',
						title : 'View'
					});
					}
					$("#loading").hide();
           		 }
			function edit(id){
				setCookie("Ses_Keyword",document.getElementById("txtKeyword").value);
				window.location="newproject.php?id="+id+"&page=search";
			}
			function view(id){
				setCookie("Ses_Keyword",document.getElementById("txtKeyword").value);
				window.location="previewforecast.php?id="+id+"&page=search";
			}
		</script>
	</head>
<?php  $checkmenu = '9'; ?>
<body cz-shortcut-listen="true" style="margin-top:0px;">	
<div id="loading" style="height: 100%; width: 100%; position: fixed; left: 0; top: 0; z-index: 1051 !important; background-color: black; filter: alpha(opacity=9); -khtml-opacity: 0.9; -moz-opacity: 0.9; opacity: 0.9;"><img src="img/loading.gif" style="width: 150px; height: 150px; position: fixed; top: 50%; left: 50%; margin: -75px 0 0 -75px;"></div>
<section class="page-header bg-color white">
	<div class="container">
		<div class="row">
			<div class="span12">
				<div class="section-header clearfix">
					<div class="pull-left">
						<h2><i class="icon-user-5"></i>Postsale</h2>
					</div>
					<div id="shortcut" class="nav-collapse pull-right"><ul class="shortcuts unstyled">
						<?php include("menu.php");?>
					</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="dashboard content">
	<div class="container">
			<div class="row">
				<div class="span12 widget">		
					<!--<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-info-circled"></i> Search</span>						
					</div>-->
					<div class="widget widget_search">
						<div class="bg-color white rounded">
						
							<div style="padding:10px 20px 5px 20px;text-align:left">
								<span id="sumResult" class="label label-inverse " style="font-size:20px;padding:10px;" title="Number of results">0</span>
								&nbsp;&nbsp;<font color="#777777" style="font-size:14px"><b>รายการ</b></font>
								<span class="pull-right">
								<input class="span4 " type="text" id="txtKeyword" style="width:300px;margin-bottom:0px;margin-top:0px;" sthle="margin-bottom: 0px;" placeholder="ค้นหาจาก PE Contract และชื่อโครงการ">
								<img id="btnSearch" src="img/styler/icons/search.png" style="cursor: pointer;padding-left:5px;" width="16" height="16" border="0" data-toggle="tooltip" data-placement="bottom" data-original-title="Search"/>
								</span>
							</div>
							<div class="box-padding" style="padding:5px">
								<label style="background-color: #F2DEDE; display: inline-block; width: 150px; text-align: center; float: left; cursor: default;color: #777777;">Potential&lt;50</label>
								<label style="background-color: #FCF8E3; display: inline-block; width: 150px; text-align: center; float: left; cursor: default;color: #777777;">Potential=50</label>
								<label style="background-color: #DFF0D8; display: inline-block; width: 150px; text-align: center; float: left; cursor: default;color: #777777;">Potential>50</label>
								<span class="pull-right" style="color: #777777;">Sort by : Signdate
								</span>
							</div>
							<!--<div class="form-search pull-right">
								<input class="input-medium search-query" type="text" placeholder="Search anything here">
								<button class="btn" type="submit">Search</button>
							</div>-->
							<div class="box-padding" style="padding:5px">
				    			<table class="table table-striped" id="example">
									<thead>
										<tr role="row">
											<th style="background: none repeat scroll 0 0 #2ABF9E;color: #FFFFFF;" width="80px">PE Contract</th>
											<th style="background: none repeat scroll 0 0 #2ABF9E;color: #FFFFFF;"width="500px">Project Name</th>
											<th style="background: none repeat scroll 0 0 #2ABF9E;color: #FFFFFF;" width="120px">SalesName</th>
											<th style="background: none repeat scroll 0 0 #2ABF9E;color: #FFFFFF;" width="80px">BiddingDate</th>
											<th style="background: none repeat scroll 0 0 #2ABF9E;color: #FFFFFF;" width="80px">SignDate</th>
											<th style="background: none repeat scroll 0 0 #2ABF9E;color: #FFFFFF;text-align:right" width="80px">Income</th>
											<th style="background: none repeat scroll 0 0 #2ABF9E;color: #FFFFFF;" width="40px"><i class="icon-pencil-1" title='Edit'></i>|<i class='icon-newspaper' title='View'></i></th>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
					        </div>
				        </div>
			    	</div>			
				</div>
			</div>
	</div>
</section>


		<div id="footer" class="bg-color dark-blue">
			<div class="container">
				<div class="box-padding">
					Copyright &copy; 2013 Sales Forecast
				</div>
			</div>
		</div>
		
  	</body>

<!-- Mirrored from bootstrapstyler.com/preview/_/flatdashboard/ by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 18 Oct 2013 07:00:04 GMT -->
</html>