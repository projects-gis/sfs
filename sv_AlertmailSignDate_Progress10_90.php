<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<meta http-equiv="refresh" content="86400">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="Izuddin Helmi">
	<title>Sendmail แจ้งเตือน Sales เจ้าของโครงการ หากเกินกำหนดวัน sign date และ Progress ยังไม่เท่ากับ 100%</title>
	<link href="bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="bootstrap/css/bootstrap-responsive.css" rel="stylesheet" type="text/css">
	<link href="js/library/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
	<link href="css/styler/style_edit.css" rel="stylesheet" type="text/css">
	<link href="css/main.css" rel="stylesheet" type="text/css">
	<link href="js/library/syntaxHighlighter/shCore.css" rel="stylesheet" type="text/css"/>
	<link href="js/library/syntaxHighlighter/shThemeDefault.css" rel="stylesheet" type="text/css"/>
	<link href="css/styler/fontello-edit.css" rel="stylesheet" type="text/css">
    <link href="css/styler/animation.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="js/library/jquery/jquery-ui-1.10.3.custom.min.css" />

	<link href="libs/bootstrap/css/bootstrap-edit.css" rel="stylesheet">
    <link href="libs/prettify/prettify-bootstrap.css" rel="stylesheet">
    <!--Bootstrap-editable-->
    <link href="bootstrap-editable/css/bootstrap-editable.css" rel="stylesheet">

</head>
<body cz-shortcut-listen="true" style="margin-top: 0px;">
<div id="footer" class="bg-color dark-blue">
</div>
	<script src="js/library/jquery/jquery-1.9.1.js"></script>
	<script src="js/library/jquery/jquery-ui-1.10.3.custom.js"></script>
	<script src="js/library/masonry/jquery.masonry.min.js"></script>
	<script src="js/library/flexslider/jquery.flexslider-min.js"></script>
	<script src="js/library/dataTables/jquery.dataTables.min.js"></script>
	<script src="js/library/fullcalendar/fullcalendar.min.js"></script>
	<script src="js/library/fullcalendar/gcal.js"></script>
	<script src="js/library/forms/jquery.hotkeys.js"></script>
	<script src="js/library/forms/bootstrap-wysiwyg.js"></script>
	<script src="js/library/forms/picker.js"></script>
	<script src="js/library/forms/picker.date.js"></script>
	<script src="js/library/forms/picker.time.js"></script>
	<script src="js/library/forms/jquery.bootstrap.wizard.min.js"></script>
	<script src="js/library/charts/jquery.flot.js"></script>
	<script src="js/library/charts/jquery.flot.resize.js"></script>
	<script src="js/library/charts/jquery.flot.stack.js"></script>
	<script src="js/library/charts/jquery.flot.pie.min.js"></script>
	<script src="js/library/charts/jquery.sparkline.min.js"></script>
	<script src="js/library/syntaxHighlighter/shCore.js"></script>
	<script src="js/library/syntaxHighlighter/shBrushCss.js"></script>
	<script src="js/library/syntaxHighlighter/shBrushXml.js"></script>
	<script src="js/library/syntaxHighlighter/shBrushJScript.js"></script>
	<script src="js/library/jquery.number.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="js/styler/custom.js"></script>
	<script src="js/set_cookie.js"></script>

 <!--<script src="libs/jquery/jquery-1.8.2.min.js"></script>-->
  <script src="libs/bootstrap/js/bootstrap.min.js"></script>
  <script src="bootstrap-editable/js/bootstrap-editable.js"></script>
  <script src="libs/prettify/prettify.js"></script>
  <script src="libs/mockjax/jquery.mockjax.js"></script>

	<script src="js/jquery-scrolltofixed.js"></script>
	<script src="main.js"></script>

	<script type="text/javascript">

	$(document).ready(function() {
	 	function countProperties(obj) {
			  var prop;
			  var propCount = 0;

			  for (prop in obj) {
			    propCount++;
			  }
			  return propCount;
		}
	  	var date = new Date();
		var today = date.getDate();
		var lastMonth = date.getMonth();
		var year = date.getFullYear();
		var myService = "Ajax/CA_salemanager_select_all.php";
	    $.getJSON(myService, {
	    	tags: "Load Sale",
	      tagmode: "any",
	      format: "json"
	    })
	    .done(function (data) {
	    	//console.log(data);
	    	$.each(data, function() {
				var item = this;
				var SaleIn=item[0];
				var ManagerID=item[1];
				var myService2 = "Ajax/SV_Alert_Signdate_Progress10_90.php?SaleIn="+SaleIn;
			    $.getJSON(myService2, {
			    	tags: "Load E-contact",
			      	tagmode: "any",
			      	format: "json"
			    })
			    .done(function (data2) {
		    		var countProp=countProperties(data2);
		    		var result=0;
		    		var MailContract="";
		    		var i=0;
		    		$.each(data2, function() {
						var item = this;
						var str_ID= item[0];
						var str_EContract = item[2];
						var str_PEContractNo = item[3];
						var str_Project = item[4];
						var str_FlagSiteRef = item[5];
						var str_TimeFrameDeliveryDate = item[6];
						var str_DateDiff = item[7];
						var str_Progress = item[8];
		    			if(str_DateDiff>=90)
		    			{
		    				MailContract=MailContract+"["+str_PEContractNo+"]["+str_Progress+"] : "+str_Project+"<br/>";
							i++;	
		    			}		
			    	});
		    		//Sendmail
		    		if(i!=0)
		    		{
		    			sendmailAlert(SaleIn,MailContract,ManagerID);
			    		//return false; 
		    		}

			    });
		  	});
	    });
	});
	//sendmailSiteRef("0563","ทดสอบ Test","0416");
	function sendmailAlert(saleid,content,ManagerID)
	{
		console.log(" sendmailAlert : ",saleid," ",content);
		
		var tr = $('<tr class="warning"><td class="text-center">' + ManagerID+ '</td><td><p class="lead14">' + saleid+ '</p></td><td><p class="lead14">' + content + '</p></td>');
	    $("#detail").append(tr);
		$.ajax({
			type: "POST",
			dataType: "json",
			url: "AJAX/sendmailSigndate_Progress10_90.php",
			data: {
				SaleID: saleid,
				Content: content,
				ManagerID: ManagerID
			},
			success: function(json) {
				console.log("sendmail complete ",saleid);
			},
			error: function() {

			}
		});
	}
	function writeData(contract,saleid,status)
	{
		
	}

			
</script>
<table class="table table-striped showdetail"  >
										<thead>
											<tr role="row">
												<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;" width="100px">E-Contract</th>
												<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;" width="80px">SaleID</th>
												<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;" width="80px">Status</th>
											</tr>
										</thead>
										<tbody id="detail"></tbody>
									</table>
</body>
</html>