<?php 
function ThaiIToUTF8($in) { 
	$out = ""; 
	for ($i = 0; $i < strlen($in); $i++) 
	{
		if (ord($in[$i]) <= 126) 
		$out .= $in[$i];
	else 
		$out .= "&#" . (ord($in[$i]) - 161 + 3585) . ";"; 
	} 
	return $out; 
} 
function funcSpecialIn($id)
	{
		$myid=explode(",",$id);
		$myCount=count($myid);
		$mythiname="";
		for($i=0;$i<$myCount;$i++)
		{
			if($i==0)
			{
				$mythiname="'".$myid[$i]."'";
			}
			else
			{
				$mythiname=$mythiname.",'".$myid[$i]."'";
			}
		}
		return $mythiname;
	}
function funcSaleIn($id) 
{
	$myid=explode(",",$id);
	$myCount=count($myid);
	$mythiname="";
	for($i=0;$i<$myCount;$i++)
	{
		if($i===0)
		{
			$mythiname="'".$myid[$i]."'";
		}
		else
		{
			$mythiname=$mythiname.",'".$myid[$i]."'";
		}
	}
	return $mythiname;
}
function funcProgressIn($id)
	{
		$myid=explode(",",$id);
		$myCount=count($myid);
		$mythiname="";
		if($myCount==1)
		{
			$mythiname="'".$id."'";
		}
		else
		{
			for($i=0;$i<$myCount;$i++)
			{
				if($i==0)
				{
					$mythiname="'".$myid[$i]."'";
				}
				else
				{
					$mythiname=$mythiname.",'".$myid[$i]."'";
				}
			}
		}
		return $mythiname;
	}
function myDescription($idforecast) 
{
	$mydesc="";
	include ("INC/connectSFC.php");
	$sql="SELECT     TypeDescription.TypeName, DescriptionDetail.Description FROM         DescriptionDetail INNER JOIN TypeDescription ON DescriptionDetail.IDTypeDescription = TypeDescription.IDTypeDescription WHERE (DescriptionDetail.IDForecast = '$idforecast')ORDER BY TypeDescription.TypeName";
	$result=sqlsrv_query($ConnectSaleForecast,$sql);
	$i=1;
	while ($obj=sqlsrv_fetch_object($result)) 
	{	
		$TypeName=$obj->TypeName;
		$Description=$obj->Description;
		if($i==1)
		{
			$mydesc=$TypeName." : ".$Description;
		}
		else
		{
			$mydesc=$mydesc." , ".$TypeName." : ".$Description;
		}
		$i++;
	}
	return $mydesc;
}
function myStatus($idforecast) 
{
	$mystat="";
	include ("INC/connectSFC.php");
	$sql="SELECT  *,convert(varchar,DateStatus,103) as DateStatus from statusDetail WHERE IDForecast = '$idforecast'  order by Year(DateStatus) desc,Month(DateStatus) desc ,day(DateStatus) desc ";
	$result=sqlsrv_query($ConnectSaleForecast,$sql);
	$i=1;
	while ($obj=sqlsrv_fetch_object($result)) 
	{	
		$DateStatus=$obj->DateStatus;
		$Description=$obj->Description;
		if($i==1)
		{
			$mystat=$DateStatus." : ".$Description;
		}
		else
		{
			$mystat=$mystat." , ".$DateStatus." : ".$Description;
		}
		$i++;
	}
	return $mystat;
}
function CheckDepartmentSupportNeeded($IDForecast,$DSNNameID)
{
	include("INC/connectSFC.php");
	$sql="SELECT  * from DepartmentSupportNeeded WHERE IDForecast = '$IDForecast'  and DSNNameID='$DSNNameID' ";
	$result=sqlsrv_query($ConnectSaleForecast,$sql);
	$num=sqlsrv_num_rows($result);
	if($num==0)
		$num=0;
	else
		$num=1;
	return $num;
}
function CheckApplicationSupportNeeded($IDForecast,$ASNName)
{
	include("INC/connectSFC.php");
	$sql="SELECT  * from ApplicationSupportNeeded WHERE IDForecast = '$IDForecast'  and ASNName='$ASNName' ";
	$result=sqlsrv_query($ConnectSaleForecast,$sql);
	$num=sqlsrv_num_rows($result);
	return $num;
}
function CheckQty($IDForecast,$EPName)
{
	include("INC/connectSofia.php");
	$sql="SELECT qty from Report_ESRIProduct1 WHERE IDForecast = '$IDForecast' and productneng = '$EPName'";
	$result=sqlsrv_query($ConnectSFS116,$sql);
	$num=0;
	while($obj=sqlsrv_fetch_object($result))
	{
		$num=$obj->qty;
	}
	return $num;
	//print $sql;
}
function copydata()
{
	include("INC/connectSFC.php");
	$sqlDelete ="DELETE FROM Report_ESRIProduct1";
	$resultDelete=sqlsrv_query($ConnectSaleForecast,$sqlDelete);


	sqlsrv_query($ConnectSaleForecast,'SET ANSI_NULLS ON');	
	sqlsrv_query($ConnectSaleForecast,'SET  ANSI_WARNINGS ON');
	$rs=sqlsrv_query($ConnectSaleForecast,'INSERT INTO Report_ESRIProduct1(id, product_id, productgrp, productneng, qty, IDForecast) SELECT     id, product_id, productgrp, productneng, qty, IDForecast FROM Report_ESRI_Product');
	/*include 'INC/connectSFS_Adodb.php';
  
    $rs = $DBSFS_ESRIT->Execute('SET ANSI_NULLS ON');
    $rs = $DBSFS_ESRIT->Execute('SET  ANSI_WARNINGS ON');
    $rs = $DBSFS_ESRIT->Execute('INSERT INTO Report_ESRIProduct1(id, product_id, productgrp, productneng, qty, IDForecast) SELECT     id, product_id, productgrp, productneng, qty, IDForecast FROM Report_ESRI_Product');
  if (!$rs){  print $DBSFS_ESRIT->ErrorMsg();}*/

}
	copydata();
	$Progress=funcProgressIn($_POST['Progress2']);
	$SpecialProject=funcSpecialIn($_POST['SpecialProject']);
	$BidingYear = $_POST['BidingYear'];
	$where = $_POST['where'];
	$option = $_POST['option'];
	include("INC/connectSFC.php");
	$sqlForecast ="select IDForecast from Forecast where IDForecast in (select IDForecast from ESRIProduct ) and Progress in ($Progress) and TargetSpecialProject in ($SpecialProject)  and year($where)='$BidingYear' ";
	$result=sqlsrv_query($ConnectSaleForecast,$sqlForecast);
	$sqlSpa = '';
	while($obj=sqlsrv_fetch_object($result))
	{
		if($sqlSpa == ''){
			$sqlSpa = "select productneng from Report_ESRIProduct1 where IDForecast = '$obj->IDForecast'";
			//echo $sqlSpa="SELECT     id, product_id, product, productgrp, productneng, productnthai, supplierpartno, partno, baht, dollar, detail, Activeflag, Search, userupdated, dateupdated FROM         OPENQUERY(lsqt, 'select * from quotation2014.dbo.products') AS derivedtbl_1";
		}else{
			//$sqlSpa=$sqlSpa." union SELECT     T1.productneng FROM  dbo.products AS T1 LEFT OUTER JOIN  dbo.ESRIProduct AS T2 ON T1.id = T2.Product_id WHERE     (T1.product = 'ESRI Product') and IDForecast = '$obj->IDForecast' "
			$sqlSpa = $sqlSpa." union select productneng from Report_ESRIProduct1 where IDForecast = '$obj->IDForecast'";
		}
		//$sqlSpa = "select productneng from [SaleForecast].[dbo].[Report_ESRI Product] where IDForecast = '$obj->IDForecast'";
	}
	//sqlsrv_close($ConnectSaleForecast);
	$sqlSpa = $sqlSpa." order by productneng ";
	include("INC/connectSofia.php");
	$result2=sqlsrv_query($ConnectSFS116,$sqlSpa);
	$sumCol=sqlsrv_num_rows($result2);
	$count = 0;
	while($obj2=sqlsrv_fetch_object($result2))
	{
		$productArr[$count]=$obj2->productneng;
		$count= $count+1;
	}
	//sqlsrv_close($ConnectSFS116);
	//$count;
	//<!--Query Department Support Needed-->
	include("INC/connectSFC.php");
	$sqlDepSup ="SELECT id,depName FROM DepartmentDetail where flag <> '1' and  export = '1' order by depName";
	$result3=sqlsrv_query($ConnectSaleForecast,$sqlDepSup);
	$sumDep=sqlsrv_num_rows($result3);
	$countDep = 0;
	while($obj3=sqlsrv_fetch_object($result3))
	{
		$DepIDArr[$countDep]=$obj3->id;
		$DepNameArr[$countDep]=$obj3->depName;
		$countDep= $countDep+1;
	}
	
	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
	header("Content-Type: application/download");
	header("Content-Transfer-Encoding: binary ");
	header('Content-type: application/ms-excel');		
	header("Content-Disposition: attachment; filename=".basename("Report_potential.xls").";");
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="content-type" content="text/html;charset=window-874" />
<title>:Report:</title>
<style type="text/css">

td.menubb {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:14px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue; text-Align: center;background:#CCFFCC;font-weight: bold;
}
td.menubbn {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue; text-Align: center;background:#CCFFCC;font-weight: bold;
}
td.menubbE {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:14px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue; text-Align: center;background:#CDE6FF;font-weight: bold;
}
td.menubbnE {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue; text-Align: center;background:#CDE6FF;font-weight: bold;
}
td.menubbG {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:14px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue; text-Align: center;background:#B2FFFF;font-weight: bold;
}
td.menubbnG {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue; text-Align: center;background:#B2FFFF;font-weight: bold;
}
td.menubbT {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:14px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue; text-Align: center;background:#FFB3FE;font-weight: bold;
}
td.menubbnT {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue; text-Align: center;background:#FFB3FE;font-weight: bold;
}
td.menubbL {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:14px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue; text-Align: center;background:#E6CDFF;font-weight: bold;
}
td.menubbnL {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue; text-Align: center;background:#E6CDFF;font-weight: bold;
}
td.menubbA {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:14px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue; text-Align: center;background:#CCFFCC;font-weight: bold;
}
td.menubbnA {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue; text-Align: center;background:#CCFFCC;font-weight: bold;
}
td.menubbO {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:14px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue; text-Align: center;background:#FFF0B3;font-weight: bold;
}
td.menubbnO {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue; text-Align: center;background:#FFF0B3;font-weight: bold;
}
td.menubbY {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:14px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue; text-Align: center;background:#FFFF99;font-weight: bold;
}
td.menubbnY {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue; text-Align: center;background:#FFFF99;font-weight: bold;
}
td.menubbY2 {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:14px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue; text-Align: center;background:#FFFD4D;font-weight: bold;
}
td.menubbnY2 {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue; text-Align: center;background:#FFFD4D;font-weight: bold;
}
td.menubbD {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:14px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue; text-Align: center;background:#00FF00;font-weight: bold;
}
td.menubbnD {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue; text-Align: center;background:#00FF00;font-weight: bold;
}
td.submenubb {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue;text-Align: left;
}
td.submenubbR {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue;text-Align: right;
}
td.submenubbC {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue;text-Align: center;
}
td.txt13 { 
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;Border-LEFT: 1px solid blue;Border-RIGHT: 1px solid blue;
}

</style>
</head>
<body >
<table cellpadding="0" cellspacing="0" Border="0" width="80%" >
	<tr Align="left"><td colspan="4" ><FONT SIZE="3" COLOR="#0000FF"><B>ESRI Product Year <?php echo $BidingYear?></B></FONT></td></tr>
   <tr Align="center">
    <td  class ="menubbY" rowspan="2" width="5%">No.</td>
    <td  class ="menubbY" rowspan="2" width="60%">Description</td>
    <td  class ="menubbY" rowspan="2" width="60%">Price</td>
    <td  class ="menubbY" width="5%" colspan="2">Potential < 50%</td>
    <td  class ="menubbY" width="5%" colspan="2">Potential = 50%</td>
    <td  class ="menubbY" width="5%" colspan="2">Potential > 50%</td>
  </tr>
  <tr Align="center">
	<!--Target-->
    <td class ="menubbY" >(unit)</td>
    <td class ="menubbY" >Total</td>
    <td class ="menubbY" >(unit)</td>
    <td class ="menubbY" >Total</td>
    <td class ="menubbY" >(unit)</td>
    <td class ="menubbY" >Total</td>
	</tr>
  <?php
	
	$saleidin=funcSaleIn($_POST['saleid']);
	include ("INC/connectSFC.php");	
		$sql="SELECT     id, productgrp, productneng,baht FROM  products WHERE     (product = 'ESRI Product') ORDER BY productgrp";
	//print $sql;
	$result=sqlsrv_query($ConnectSaleForecast,$sql);
	$i=1;
	while ($obj=sqlsrv_fetch_object($result)) 
	{	
		$potential50L="0";
		$potential50C="0";
		$potential50R="0";
		$potential50LTotal="0";
		$potential50CTotal="0";
		$potential50RTotal="0";
		$Description=$obj->productneng;
		$Price=$obj->baht;
		$sql2="SELECT     SUM(CONVERT(int, e.qty)) AS qty FROM         ESRIProduct AS e INNER JOIN                       Forecast AS f ON e.IDForecast = f.IDForecast WHERE f.SaleID in ($saleidin) and     (e.Product_id = '$obj->id') AND (YEAR(f.TimeFrameContractSigndate) = '$BidingYear') AND (f.Potential IN ('10', '25')) and f.Progress in ($Progress) and f.TargetSpecialProject in ($SpecialProject)";
		$result2=sqlsrv_query($ConnectSaleForecast,$sql2);
		while ($obj2=sqlsrv_fetch_object($result2)) 
		{
			$potential50L=$obj2->qty;
			$potential50LTotal=$Price*$potential50L;
		}	
		$sql3="SELECT     SUM(CONVERT(int, e.qty)) AS qty FROM         ESRIProduct AS e INNER JOIN                       Forecast AS f ON e.IDForecast = f.IDForecast WHERE  f.SaleID in ($saleidin) and   (e.Product_id = '$obj->id') AND (YEAR(f.TimeFrameContractSigndate) = '$BidingYear') AND (f.Potential IN ('50')) and f.Progress in ($Progress) and f.TargetSpecialProject in ($SpecialProject)";
		$result3=sqlsrv_query($ConnectSaleForecast,$sql3);
		while ($obj3=sqlsrv_fetch_object($result3)) 
		{
			$potential50C=$obj3->qty;
			$potential50CTotal=$Price*$potential50C;
		}	
		$sql4="SELECT     SUM(CONVERT(int, e.qty)) AS qty FROM         ESRIProduct AS e INNER JOIN                       Forecast AS f ON e.IDForecast = f.IDForecast WHERE  f.SaleID in ($saleidin) and   (e.Product_id = '$obj->id') AND (YEAR(f.TimeFrameContractSigndate) = '$BidingYear') AND (f.Potential IN ('75','100')) and f.Progress in ($Progress) and f.TargetSpecialProject in ($SpecialProject)";
		$result4=sqlsrv_query($ConnectSaleForecast,$sql4);
		while ($obj4=sqlsrv_fetch_object($result4)) 
		{
			$potential50R=$obj4->qty;
			$potential50RTotal=$Price*$potential50R;
		}	
		?>
			<tr>
				<td  class ="submenubbC" vAlign="top" width="5%"><?php echo $i++?></td>
		    <td  class ="submenubb" vAlign="top" width="65%"><?php echo $Description?>&nbsp;</td>
		    <td  class ="submenubbC" vAlign="top" width="65%" style="mso-number-format:'\#\,\#\#0'"><?php echo $Price?></td>
		    <td class ="submenubbC" vAlign="top" width="5%" style="mso-number-format:'\#\,\#\#0'"><?php echo ($potential50L ? $potential50L : '0')?></td>
		    <td class ="submenubbC" vAlign="top" width="5%" style="mso-number-format:'\#\,\#\#0'"><?php echo ($potential50LTotal ? $potential50LTotal : '0')?></td>
		    <td class ="submenubbC" vAlign="top" width="5%" style="mso-number-format:'\#\,\#\#0'"><?php echo ($potential50C ? $potential50C : '0')?></td>
		    <td class ="submenubbC" vAlign="top" width="5%" style="mso-number-format:'\#\,\#\#0'"><?php echo ($potential50CTotal ? $potential50CTotal : '0')?></td>
		    <td class ="submenubbC" vAlign="top" width="5%" style="mso-number-format:'\#\,\#\#0'"><?php echo ($potential50R ? $potential50R : '0')?></td>
		    <td class ="submenubbC" vAlign="top" width="5%" style="mso-number-format:'\#\,\#\#0'"><?php echo ($potential50RTotal ? $potential50RTotal : '0')?></td>
			
		</tr>
	<?php
	}
	?>
 </table>
</body>
</html>
