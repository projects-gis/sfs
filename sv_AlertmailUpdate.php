<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<meta http-equiv="refresh" content="86400">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="Izuddin Helmi">
	<title>Sendmail Update</title>
	<link href="bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="bootstrap/css/bootstrap-responsive.css" rel="stylesheet" type="text/css">
	<link href="js/library/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
	<link href="css/styler/style_edit.css" rel="stylesheet" type="text/css">
	<link href="css/main.css" rel="stylesheet" type="text/css">
	<link href="js/library/syntaxHighlighter/shCore.css" rel="stylesheet" type="text/css"/>
	<link href="js/library/syntaxHighlighter/shThemeDefault.css" rel="stylesheet" type="text/css"/>
	<link href="css/styler/fontello-edit.css" rel="stylesheet" type="text/css">
    <link href="css/styler/animation.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="js/library/jquery/jquery-ui-1.10.3.custom.min.css" />

	<link href="libs/bootstrap/css/bootstrap-edit.css" rel="stylesheet">
    <link href="libs/prettify/prettify-bootstrap.css" rel="stylesheet">
    <!--Bootstrap-editable-->
    <link href="bootstrap-editable/css/bootstrap-editable.css" rel="stylesheet">

</head>
<body cz-shortcut-listen="true" style="margin-top: 0px;">
<div id="footer" class="bg-color dark-blue">
</div>
	<script src="js/library/jquery/jquery-1.9.1.js"></script>
	<script src="js/library/jquery/jquery-ui-1.10.3.custom.js"></script>
	<script src="js/library/masonry/jquery.masonry.min.js"></script>
	<script src="js/library/flexslider/jquery.flexslider-min.js"></script>
	<script src="js/library/dataTables/jquery.dataTables.min.js"></script>
	<script src="js/library/fullcalendar/fullcalendar.min.js"></script>
	<script src="js/library/fullcalendar/gcal.js"></script>
	<script src="js/library/forms/jquery.hotkeys.js"></script>
	<script src="js/library/forms/bootstrap-wysiwyg.js"></script>
	<script src="js/library/forms/picker.js"></script>
	<script src="js/library/forms/picker.date.js"></script>
	<script src="js/library/forms/picker.time.js"></script>
	<script src="js/library/forms/jquery.bootstrap.wizard.min.js"></script>
	<script src="js/library/charts/jquery.flot.js"></script>
	<script src="js/library/charts/jquery.flot.resize.js"></script>
	<script src="js/library/charts/jquery.flot.stack.js"></script>
	<script src="js/library/charts/jquery.flot.pie.min.js"></script>
	<script src="js/library/charts/jquery.sparkline.min.js"></script>
	<script src="js/library/syntaxHighlighter/shCore.js"></script>
	<script src="js/library/syntaxHighlighter/shBrushCss.js"></script>
	<script src="js/library/syntaxHighlighter/shBrushXml.js"></script>
	<script src="js/library/syntaxHighlighter/shBrushJScript.js"></script>
	<script src="js/library/jquery.number.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="js/styler/custom.js"></script>
	<script src="js/set_cookie.js"></script>

 <!--<script src="libs/jquery/jquery-1.8.2.min.js"></script>-->
  <script src="libs/bootstrap/js/bootstrap.min.js"></script>
  <script src="bootstrap-editable/js/bootstrap-editable.js"></script>
  <script src="libs/prettify/prettify.js"></script>
  <script src="libs/mockjax/jquery.mockjax.js"></script>

	<script src="js/jquery-scrolltofixed.js"></script>
	<script src="main.js"></script>

	<script type="text/javascript">

	 $(document).ready(function() {
	  var date = new Date();
		var today = date.getDate();
		var Month = date.getMonth()+1;		
		var lastMonth = date.getMonth();
		var year = date.getFullYear();	
		var lastyear = date.getFullYear();
		if(Month==1)
		{
			lastMonth=12;
			lastyear=year-1;
		}
		if(today=="1")
		{
			$.ajax({
				type: "POST",
				dataType: "json",
				url: "AJAX/CA_salemanager_select_all.php",
				data: {
					ID: ""
				},
				success: function(json) {
					$.each(json, function() {
						var item = this;
						var SaleIn=item[0];
						var ManagerID=item[1];
						//console.log("load ",item[0]);
						$.ajax({
							type: "POST",
							dataType: "json",
							url: "AJAX/CA_forecast_select_bySaleID.php",
							data: {
								SaleIn: SaleIn
							},
							success: function(json) {
								var emailSB="<br/>";
								var username="";
								$.each(json, function() {
									var item = this;
									var datestatus=item[5].split("/");
									var username=item[6];
									if(datestatus[1]==lastMonth && datestatus[2]==lastyear)
									{

										//emailSB+="PEContract : "+item[2]+" Project : "+item[3]+" Date Status Last : "+item[5]+"<br/>";
									}
									else
									{										
										//emailSB+="PEContract : "+item[2]+" Project : "+item[3]+" <font color='red'>[Salename : "+item[6]+"]</font><br/>";
										emailSB+="PEContract : "+item[2]+" Project : "+item[3]+"<br/>";
									}
								});
								if(emailSB!="")
								{
									$.ajax({
										type: "POST",
										dataType: "json",
										url: "AJAX/sendmailAlert.php",
										data: {
											SaleID: SaleIn,
											ManagerID: ManagerID,
											Message : emailSB,
											month :lastMonth,
											year:lastyear

										},
										success: function(json) {
											//console.log("sendmail complete ",SaleIn);
										},
										error: function() {

										}
									});
								}
							},
							error: function() {

							}
						});
					});
				},
				error: function() {

				}
			});
		}
	});

			
</script>

</body>
</html>