<?php
// file mime_mail.inc
	##########################################################################
	# Class Name		: mime_mail
	# Class Purpose	 	: Send mail
	##########################################################################
	class mime_mail {
		var $parts;
		var $from;
		var $to;
		var $headers;
		var $subject;
		var $body;

		##########################################################################
		# Function Name		: mime_mail
		# Function Purpose	 	: Class constructor
		##########################################################################
		function mime_mail() {
			$this->parts = array();
			$this->to = "";
			$this->from = "";
			$this->subject = "";
			$this->body = "";
			$this->headers = "";
		} // end function mime_mail

		##########################################################################
		# Function Name		: add_attachment
		# Function Purpose	 	: Add an attachment to the mail object
		# Recieves					: $message
		#								: $name
		#								: $ctype
		##########################################################################
		function add_attachment($message, $name = "", $ctype = "application/octet-stream") {
			$encode = "base64";
			$this->parts[] = array ("ctype" => $ctype,
											"message" => $message,
											"encode" => $encode,
											"name" => $name);
		} // end function add_attachment

		##########################################################################
		# Function Name		: build_message
		# Function Purpose	 	: Build message parts of an multipart mail
		# Recieves					: $part
		##########################################################################
		function build_message($parts) {
			$message = $parts["message"];
			$message = chunk_split(base64_encode($message));
			$encoding = "base64";
			return "Content-Type: " . $parts["ctype"] . ($parts["name"]?"; name = \"" . $parts["name"] . "\"" : "") . "\nContent-Transfer-Encoding: $encoding\n\n$message\n";
		} // end function build_message

		##########################################################################
		# Function Name		: build_multipart
		# Function Purpose	 	: Build a multipart mail
		##########################################################################
		function build_multipart() {
			$boundary = "b" . md5(uniqid(time()));
			$multipart = "Content-Type: multipart/mixed; boundary = $boundary\n\nThis is a MIME encoded message.\n\n--$boundary";
			for($i = sizeof($this->parts) - 1; $i >= 0; $i--) {
				$multipart .=  "\n" . $this->build_message($this->parts[$i]) . "--$boundary";
			} // end for
			return $multipart .= "--\n";
		} // end function build_multipart

		##########################################################################
		# Function Name		: get_mail
		# Function Purpose	 	: Returns the constructed mail
		# Recieves					: $complete
		##########################################################################
		function get_mail($complete = true) {
			$mime = "";
			if(!empty($this->from)) {
				$mime .=  "From: " . $this->from . "\n";
			} // end if
			if(!empty($this->headers)) {
				$mime .= $this->headers . "\n";
			} // end if
			if($complete) {
				if(!empty($this->to)) {
					$mime .= "To: $this->to\n";
				} // end if
				if(!empty($this->subject)) {
					$mime .= "Subject: $this->subject\n";
				} // end if
			} // end if
			if(!empty($this->body)) {
				$this->add_attachment($this->body, "", "text/plain");
			} // end if
			$mime .= "MIME-Version: 1.0\n" . $this->build_multipart();

			return $mime;
		} // end function get_mail

		##########################################################################
		# Function Name		: send
		# Function Purpose	 	: Send the mail (last class-function to be called)
		##########################################################################
		function send() {
			$mime = $this->get_mail(false);
			mail($this->to, $this->subject, "", $mime);
		} // end function send
	} // end class
// end file mime_mail.inc
?>