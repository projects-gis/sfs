<?php
// file smtp_mail.inc
	##########################################################################
	# Class Name		: smtp_mail
	# Class Purpose	 	: Send mail
	##########################################################################
	class smtp_mail {
		var $fp = false;
		var $lastmsg = "";

		##########################################################################
		# Function Name		: read_line
		# Function Purpose	 	: Reads a line from the socket and returns the numeric code and the rest of the line
		##########################################################################
		function read_line() {
			$result = false;
			$line = fgets($this->fp, 1024);

			if(ereg("^([0-9]+).(.*)$", $line, &$data)) {
				$recv_code = $data[1];
				$recv_msg  = $data[2];
				$result = array($recv_code, $recv_msg);
			} // end if

			return $result;
		} // end function read_line

		##########################################################################
		# Function Name		: dialogue
		# Function Purpose	 	: Sends a command $cmd to the remote server and
		#								  Checks whether the numeric return code is the one we expect ($code)
		# Recieves					: $code
		#								: $cmd
		##########################################################################
		function dialogue($code, $cmd) {
			$result = true;

			fwrite($this->fp, $cmd . "\r\n");
			$line = $this->read_line($this->fp);
			if($line == false) {
				$result = false;
				$this->lastmsg = "";
			} else {
				$this->lastmsg = "$line[0] $line[1]";
				if($line[0] != $code) {
					$result = false;
				} // end if
			} // end if

			return $result;
		} // end function dialogue

		##########################################################################
		# Function Name		: error_message
		# Function Purpose	 	: Prints out an error message, including the last message received from the SMTP server
		##########################################################################
		function error_message() {
			echo "SMTP protocol failure (" . $this->lastmsg . ").<br />";
		} // end function error_message

		##########################################################################
		# Function Name		: crlf_encode
		# Function Purpose	 	: Fixes line endings RFC 788 specifies CRLF (hex 0x13 0x10) as line endings
		# Recieves					: $data
		##########################################################################
		function crlf_encode($data) {
			// Make sure that the data ends with a newline character
			$data .= "\n";
			// Remove all CRs and replace single LFs with CRLFs
			$data = str_replace("\n", "\r\n", str_replace("\r", "", $data));
			// In the SMTP protocol a line consisting of a single "." has a special meaning. We therefore escape it by appending one space.
			$data = str_replace("\n.\r\n", "\n. \r\n", $data);

			return $data;
		} // end function crlf_encode

		##########################################################################
		# Function Name		: handle_email
		# Function Purpose	 	: Talks to the SMTP server
		# Recieves					: $from
		#								: $to
		#								: $data
		##########################################################################
		function handle_email($from, $to, $data) {
			// Split recipient list
			$rcpts = explode(",", $to);
			$err = false;
			if(!$this->dialogue(250, "HELO phpclient") || !$this->dialogue(250, "MAIL FROM:$from")) {
				$err = true;
			} // end if

			for($i = 0; !$err && $i < count($rcpts); $i++) {
				if(!$this->dialogue(250, "RCPT TO:$rcpts[$i]")) {
					$err = true;
				} // end if
			} // end for

			if($err || !$this->dialogue(354, "DATA") || !fwrite($this->fp, $data) || !$this->dialogue(250, ".") || !$this->dialogue(221, "QUIT")) {
				$err = true;
			} // end if

			if($err) {
				$this->error_message();
			} // end if

			return !$err;
		} // end function handle email

		##########################################################################
		# Function Name		: connect
		# Function Purpose	 	: Connects to an SMTP server on the well-known port 25
		# Recieves					: $hostname
		##########################################################################
		function connect($hostname) {
			$result = false;

			$this->fp = fsockopen($hostname, 25);
			if($this->fp) {
				$result = true;
			} // end if

			return $result;
		} // end function connect

		##########################################################################
		# Function Name		: send_e-mail
		# Function Purpose	 	: Connects to an SMTP server, encodes the message optionally, and sends $data.
		#								  The envelope sender address is $from. A comma-separated list of recipients is expected in $to. 
		# Recieves					: $hostname
		#								  $from
		#								  $to
		#								  $data
		#								  $crlf_encode
		##########################################################################
		function send_email($hostname, $from, $to, $data, $crlf_encode = 0) {
			if(!$this->connect($hostname)) {
				echo "cannot open socket<br>\n";
				return false;
			} // end if

			$line = $this->read_line();
			$result  = false;

			if($line && $line[0] == "220") {
				if($crlf_encode) {
					$data = $this->crlf_encode($data);
				} // end if

				$result = $this->handle_email($from, $to, $data);
			} else {
				$this->error_message();
			} // end if

			fclose($this->fp);
			return $result;
		} // end function send_email
	} // end class smtp_mail
// end file smtp_mail.inc
?>