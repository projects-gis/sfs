<?php
// file smail.php
	//require_once('../nusoap.php');		// use NuSOAP - Web Services Toolkit for PHP
	// config files
	require_once('mime_mail.inc');
	require_once('smtp_mail.inc');

	$soapserver = new soap_server;		// create soap server
	$soapserver->register('smail');		// register smail services

	##########################################################################
	# Function Name		: smail
	# Function Purpose	 	: Send mail to user
	# Recieves					: $from - email address that send email
	#								  $to - email address that receive email
	#								  $subject - topic of email
	#								  $body - body of email
	# Returns					: true if success, false otherwise
	##########################################################################
	function smail($from, $to, $subject, $body) {
		# our relaying SMTP server
		$smtp_server = "esrimail.cdg.co.th";
		# the sender address
		// $from = "webmaster@ce.kmitl.ac.th";
		# the recipient(s)
		// $to = "juthakiat@hotmail.com";
		# the subject of the e-mail
		// $subject = "Test email";
		# ... and its body
		// $body = "Real text of e-mail		// multiple lines are allowed, of course

		# create mime_mail instance
		$mail = new mime_mail;
		$mail->from = $from;
		$mail->to = $to;
		$mail->subject = $subject;
		$mail->body = $body;

		# get the constructed e-mail data
		$data = $mail->get_mail();

		# create smtp_mail instance
		$smtp = new smtp_mail;

		# send e-mail
		$flag = $smtp->send_email($smtp_server, $from, $to, $data);
		return $flag;
	} // end function smail

	$soapserver->service($HTTP_RAW_POST_DATA);
// end file smail.php
?>