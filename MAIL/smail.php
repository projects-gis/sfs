<?php
// file smail.php
	##########################################################################
	# Function Name		: smail
	# Function Purpose	 	: Send mail to user
	# Recieves					: $from - email address that send email
	#								  $to - email address that receive email
	#								  $subject - topic of email
	#								  $body - body of email
	# Returns					: true if success, false otherwise
	##########################################################################
	function smail($from, $to, $subject, $body) {
		require_once('../../nusoap.php');		// use NuSOAP - Web Services Toolkit for PHP

		$parameters = array('from'=>$from, 'to'=>$to, 'subject'=>$subject, 'body'=>$body);
		$soapclient = new soapclient('http://161.246.6.59/services/alert/smail.php');		// create soap client
		$flag = $soapclient->call('smail', $parameters);		// call smail services

		return $flag;
	} // end function smail
// end file smail.php
?>