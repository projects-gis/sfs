<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="Izuddin Helmi">
	<title>Smart Update</title>
	<link href="bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="bootstrap/css/bootstrap-responsive.css" rel="stylesheet" type="text/css">
	<link href="js/library/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
	<link href="css/styler/style_edit.css" rel="stylesheet" type="text/css">
	<link href="css/main.css" rel="stylesheet" type="text/css">
	<link href="js/library/syntaxHighlighter/shCore.css" rel="stylesheet" type="text/css"/>
	<link href="js/library/syntaxHighlighter/shThemeDefault.css" rel="stylesheet" type="text/css"/>
	<link href="css/styler/fontello-edit.css" rel="stylesheet" type="text/css">
    <link href="css/styler/animation.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="js/library/jquery/jquery-ui-1.10.3.custom.min.css" />

	<link href="libs/bootstrap/css/bootstrap-edit.css" rel="stylesheet">
    <link href="libs/prettify/prettify-bootstrap.css" rel="stylesheet">
    <!--Bootstrap-editable-->
    <link href="bootstrap-editable/css/bootstrap-editable.css" rel="stylesheet">
    <script>
    	var $Global_thismail;
		function OpenProject(id)
		{
			window.location="previewForecast.php?id="+id+"&page=quickupdate_all";
		}
		</script>

</head>
<?php  $checkmenu = '7'; ?>
<body cz-shortcut-listen="true" style="margin-top: 0px;">
<div id="loading" style="height: 100%; width: 100%; position: fixed; left: 0; top: 0; z-index: 1051 !important; background-color: black; filter: alpha(opacity=9); -khtml-opacity: 0.9; -moz-opacity: 0.9; opacity: 0.9;"><img src="img/loading.gif" style="width: 150px; height: 150px; position: fixed; top: 50%; left: 50%; margin: -75px 0 0 -75px;"></div>
<section class="page-header bg-color white">
	<div class="container">
		<div class="row">
			<div class="span12">
				<div class="section-header clearfix">
					<div class="pull-left">
						<h2><i class="icon-clock-5"></i>Smart Update</h2>
					</div>
					<div id="shortcut" class="nav-collapse pull-right">
						<ul class="shortcuts unstyled">
							<?php include("menu.php");?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="dashboard content" >
	<div class="container">
		<div class="row">			
			<div class="span12">
			  <div class="tab-content">
				<div id="site0" class="tab-pane fade active in"><!--mysite1-->
					<div class="widget-header clearfix bg-color red rounded-top">
						<div class="box-padding" style="margin-bottom:0px;text-align:right;">
							<br/>
							<h1 class="pull-left">Smart Update </h1>
							<h4>
							<input type="text" id="txtKeyword" style="width:300px;margin-bottom:0px;margin-top:1px;" sthle="margin-bottom: 0px;" placeholder="ค้นหา project" >&nbsp;&nbsp;
							<span id="sumResult" class="label label-inverse" style="font-size:20px;padding:10px;margin-top:10px" title="Number of results">0</span>&nbsp;&nbsp;<font color="#FFFFFF" style="font-size:14px"><b>รายการ</b></font>
							</h4><span>Sort by Signdate,Potential,Progress</span>

						</div>
					</div>
					<div class="bg-color white rounded-bottom">
						<div class="box-padding" style="padding:0px 0px;">
							<table class="table table-striped" >
								<thead id="setUp">
									<tr role="row">
										<th width="20px" style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align:center" title='Sendmail' rowspan="2"><!--<i class="icon-mail-5" id="btn_sendmail" style="cursor:pointer"></i><input type="checkbox" title="Sendmail" id="checkAll">--></th>
										 <th rowspan="2" width="200px" style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align:center">Project</th>
						                <th rowspan="2" width="50px" style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align:center">Potential</th>
						                <th rowspan="2" width="50px" style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align:center">Progress</th>
						                <th colspan="10" width="700px" style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align:center">Timeline Progress</th>
									</tr>
									<tr>
						                <th width="70px" style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align:center" title='Build solution'>10</th>
						                <th width="70px" style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align:center" title='Presentation/Demo'>20</th>
						                <th width="70px" style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align:center" title='ร่างโครงการ (ของบประมาณ) '>30</th>
						                <th width="70px" style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align:center" title='ขั้นตอนการพิจารณาอนุมัติงบประมาณ '>40</th>
						                <th width="70px" style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align:center" title='ได้งบประมาณ '>50</th>
						                <th width="70px" style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align:center" title='TOR Final '>60</th>
						                <th width="70px" style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align:center" title='Bidding date'>70</th>
						                <th width="70px" style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align:center" title='ยื่นซองแล้ว '>80</th>
						                <th width="70px" style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align:center" title='ประกาศผลแล้วรอเซ็นสัญญา'>90</th>
						                <th width="70px" style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align:center" title='signdate'>100</th>
						            </tr>
								</thead>
								<tbody id="detail">
									<tr>
										<td title="" style="width: 20px;"></td>
										<td title="" style="width: 200px;"></td>
										<td title="" style="width: 80px;"></td>
										<td title="" style="width: 80px;"></td>
										<td title="" style="width: 65px; font-size: 11px;"></td>
										<td title="" style="width: 65px; font-size: 11px;"></td>
										<td title="" style="width: 65px; font-size: 11px;"></td>
										<td title="" style="width: 65px; font-size: 11px;"></td>
										<td title="" style="width: 65px; font-size: 11px;"></td>
										<td title="" style="width: 65px; font-size: 11px;"></td>
										<td title="" style="width: 65px; font-size: 11px;"></td>
										<td title="" style="width: 65px; font-size: 11px;"></td>
										<td title="" style="width: 65px; font-size: 11px;"></td>
										<td title="" style="width: 65px; font-size: 11px;"></td>
										<td title="" style="width: 65px; font-size: 11px;"></td>
									</tr>
								</tbody>
								<tfoot>

									<tr role="row">
										<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;" colspan="14"></th>
										
									</tr>
								</tfoot>
							</table><td title="" style="width: 65px; font-size: 11px;"></td>
						</div>
					</div>
				</div>
			  </div>
			</div>
		</div>
	</div>
</section>
<!--Modal Select Sales-->
					<div class="modal hide fade" id="sendmail_modal">
						<div class="modal-header bg-color light-green aligncenter">
						    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						    <h1>Message Warning <span id="sendmail_title"></span></h1>
						    </div>
						<div class="modal-body">
							<textarea id="sendmail_body" rows="3" style="width: 500px;"></textarea>
						</div>
						<div class="modal-footer alignright">
						    <a id="sendmail_btn_ok" href="#" class="btn btn-blue">OK</a>
						    <a href="#" class="btn btn-red" data-dismiss="modal">CLOSE</a>
						</div>
					</div>
<!--<form action="" method="post" id="mycontactform" >
	<input type="hidden" name="forecastID" id="forecastID">
</form>
<iframe name="sendmailQuick" id="sendmailQuick" frameborder="0"  width="577"  scrolling=auto ></iframe>-->
<div id="footer" class="bg-color dark-blue">
	<div class="container">
		<div class="box-padding">
		Copyright &copy; 2013 Sales Forecast
		</div>
	</div>
</div>
	<script src="js/library/jquery/jquery-1.9.1.js"></script>
	<script src="js/library/jquery/jquery-ui-1.10.3.custom.js"></script>
	<script src="js/library/masonry/jquery.masonry.min.js"></script>
	<script src="js/library/flexslider/jquery.flexslider-min.js"></script>
	<script src="js/library/dataTables/jquery.dataTables.min.js"></script>
	<script src="js/library/fullcalendar/fullcalendar.min.js"></script>
	<script src="js/library/fullcalendar/gcal.js"></script>
	<script src="js/library/forms/jquery.hotkeys.js"></script>
	<script src="js/library/forms/bootstrap-wysiwyg.js"></script>
	<script src="js/library/forms/picker.js"></script>
	<script src="js/library/forms/picker.date.js"></script>
	<script src="js/library/forms/picker.time.js"></script>
	<script src="js/library/forms/jquery.bootstrap.wizard.min.js"></script>
	<script src="js/library/charts/jquery.flot.js"></script>
	<script src="js/library/charts/jquery.flot.resize.js"></script>
	<script src="js/library/charts/jquery.flot.stack.js"></script>
	<script src="js/library/charts/jquery.flot.pie.min.js"></script>
	<script src="js/library/charts/jquery.sparkline.min.js"></script>
	<script src="js/library/syntaxHighlighter/shCore.js"></script>
	<script src="js/library/syntaxHighlighter/shBrushCss.js"></script>
	<script src="js/library/syntaxHighlighter/shBrushXml.js"></script>
	<script src="js/library/syntaxHighlighter/shBrushJScript.js"></script>
	<script src="js/library/jquery.number.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="js/styler/custom.js"></script>
	<script src="js/set_cookie.js"></script>




	
        <!--<script src="libs/jquery/jquery-1.8.2.min.js"></script>-->
        <script src="libs/bootstrap/js/bootstrap.min.js"></script>
        <script src="bootstrap-editable/js/bootstrap-editable.js"></script>
        <script src="libs/prettify/prettify.js"></script>
        <script src="libs/mockjax/jquery.mockjax.js"></script>

	<script src="js/jquery-scrolltofixed.js"></script>

        


        <script src="main.js"></script>

	<script type="text/javascript">

		function quicksave(settings)
	    {
	        
	        //document.ifrQuicksave.location="quicksave.php?pk="+settings.data.pk+"&name="+settings.data.name+"&value="+settings.data.value;
	        var SaleID = "<?php echo $_COOKIE['Ses_ID']?>";
	        $.ajax({
	            type: "POST",
	            dataType: "json",
	            data: {
	              name: settings.data.name,
	              pk: settings.data.pk,
	              value: settings.data.value,
	              salesid: SaleID
	            },
	            url: "AJAX/quicksave.php",
	            success: function(json) {
	                //console.log(json);
	            },
	            error: function() {}
	        });

	    }
		var GlobalSaleID = "<?php echo $_COOKIE['Ses_ID']?>";
		var keywordHome = "";
	    $(document).ready(function() {
			console.log('1');
	    	$("#loading").hide();        
			initQuickUpdate();
			$('#setUp').scrollToFixed();
			/*$('#checkAll').click(function () {    
			     $('input:checkbox').prop('checked', this.checked);    
			 });*/
			/*$('.btn_sendmail').click(function () {   
				console.log("testmail");
				var sendmailID="";
				var count=1; 
			    $('.checkMeOut').each(function() 
				{    
					if(count==1)
					{
						if($(this).is(':checked')){
							sendmailID=$(this).val();
							count=0;
						}
					}
					else
					{
						if($(this).is(':checked')){
							sendmailID+=","+$(this).val();
						}
					}
				    
				});
				console.log(sendmailID);
				if(sendmailID!="")
				{
					$("#loading").show();
					//$("#forecastID").val(sendmailID);
					//$("#sendmailQuick").attr("src", "AJAX/sendmailQuick.php?forecastID="+sendmailID);
					//$("#loading").hide();
					/*$.post("AJAX/sendmailQuick.php", $("#mycontactform").serialize(),  function(response) {
						console.log("sendmailQuick");
						$("#loading").hide();
					});
					$.ajax({
								type: "POST",
								dataType: "json",
							    url: "AJAX/sendmailQuick.php",
							    data: {
							    	ID: sendmailID
							    },
							    success: function(json) {
									console.log("sendmailQuick");
									$("#loading").hide();
								},
								error: function() {
									alert("เกิดปัญหาในการบันทึกกรุณาลองใหม่อีกครั้ง");
									$("#loading").hide();
								}
							});*/
				//}
			 //});
			
			
			$('#sendmail_btn_ok').click(function () {

				$("#sendmail_modal").modal("hide");	
				$("#loading").show();
				$thisMail=$Global_thismail;
				var classname=$thisMail.context.attributes.class.nodeValue;
				var sendmailID=$thisMail.context.attributes.name.nodeValue;
				//console.log($thisMail.context.attributes.class.nodeValue);
				var messagemail=$("#sendmail_body").val();
					$.ajax({
								type: "POST",
								dataType: "json",
							    url: "AJAX/sendmailQuick.php",
							    data: {
							    	ID: sendmailID,
							    	Message:messagemail,
							    	ManagerID:GlobalSaleID
							    },
							    success: function(json) {
									//console.log("sendmailQuick");
									//$(this).Remo.addClass("icon-alert");

									//$(this).removeClass("icon-mail-5 btn_sendmail").addClass( "icon-alert" );	
									$thisMail.removeClass("icon-mail-5 btn_sendmail").addClass( "icon-alert" );
									$thisMail.attr("style","color:#DD1144");
									//$thisParagraph.toggleClass( "icon-alert", "icon-mail-5 btn_sendmail" );
									//$(document).scrollTo($thisMail);
									$("#loading").hide();
								},
								error: function() {
									alert("เกิดปัญหาในการบันทึกกรุณาลองใหม่อีกครั้ง");
									$("#loading").hide();
								}
					});
			});
		});
		function loadData(json)
		{			
			$("#detail").empty();
			$("#sumResult").text(json.length);
		        	//console.log(json);
		        	var i=0;
		        	$.each(json, function() {
		        		i++;
						var item = this;
						var currentProgressPiority =parseInt(item[15]);
						//alert(currentProgressPiority);
		        		$tr = $("<tr>");
		        		//check to sendmail
				        $td0 = $("<td>").appendTo($tr);	
				        	$td0.attr({ title:'',style:'width:20px'});	
				        if(item[16]==0)
				        {
				        	$btnSendmail=$("<i>").attr({ style:'cursor:pointer;color:#0088CC;font-size:20px;',name:item[14],title:'sendmail'}).addClass("icon-mail-5 btn_sendmail");

				    	}
				    	else
				    	{
				    		$btnSendmail=$("<i>").attr({ title:'',style:'color:#DD1144;font-size:20px;'}).addClass("icon-alert");
				    	}
				        /*$checkbox=$("<a>");
				        var $checkbox = $('<input/>').attr({ type: 'checkbox', name:'checkMeOut',value:item[14]}).addClass("checkMeOut");*/
						$btnSendmail.appendTo($td0);		        		

		        		/* project */
				        $td1 = $("<td>").appendTo($tr);	
				        	$td1.attr({ title:'',style:'width:200px'});		        
				        //$span1 = $("<a>").text(item[1]).appendTo($td1);
				        	$a1=$("<a>");
				        	//$a1.attr("href","javascript:OpenProject('"+item[14]+"')");
				        	$a1.attr("href","previewForecastReport.php?id="+item[14]+"&page=quickupdate_all");
				        	$a1.attr("target","_blank");
				        	$a1.attr("title","Income : "+numFormat(parseFloat(item[17]).toFixed(2)));
				        	$a1.text(item[1]);	
				        	$a1.appendTo($td1);	

		        		/* potential */
				        $td2 = $("<td>").text(item[2]).appendTo($tr);
				        	$td2.attr({ title:'',style:'width:80px'});
				        	$td2.attr("class","info");	      
				        $td3 = $("<td>").text(item[3]).appendTo($tr);	
				        	$td3.attr({ title:'',style:'width:80px'});
				        	$td3.attr("class","info");

				        $td5 = $("<td>").text(item[4]).appendTo($tr);
				        	$td5.attr({ title:'',style:'width: 65px; font-size: 11px;'});
				        	if(currentProgressPiority==10)
				        	{	
				        		$td5.attr("class","current");	
				        	}
				        	else if(currentProgressPiority<10)
				        	{
				        		$td5.attr("class","success");
				        	}
				        	else
				        	{
				        		$td5.attr("class","warning");
				        	}
				        $td6 = $("<td>").text(item[5]).appendTo($tr);
				        	$td6.attr({ title:'',style:'width: 65px; font-size: 11px;'});
				        	$td6.attr("class","success");	
				        	if(currentProgressPiority==9)
				        	{	
				        		$td6.attr("class","current");	
				        	}
				        	else if(currentProgressPiority<9)
				        	{
				        		$td6.attr("class","success");
				        	}
				        	else
				        	{
				        		$td6.attr("class","warning");
				        	}
				        $td7 = $("<td>").text(item[6]).appendTo($tr);
				        	$td7.attr({ title:'',style:'width: 65px; font-size: 11px;'});
				        	$td7.attr("class","success");	
				        	if(currentProgressPiority==8)
				        	{	
				        		$td7.attr("class","current");	
				        	}
				        	else if(currentProgressPiority<8)
				        	{
				        		$td7.attr("class","success");
				        	}
				        	else
				        	{
				        		$td7.attr("class","warning");
				        	}
				        $td8 = $("<td>").text(item[7]).appendTo($tr);
				        	$td8.attr({ title:'',style:'width: 65px; font-size: 11px;'});
				        	$td8.attr("class","success");
				        	if(currentProgressPiority==7)
				        	{	
				        		$td8.attr("class","current");	
				        	}
				        	else if(currentProgressPiority<7)
				        	{
				        		$td8.attr("class","success");
				        	}
				        	else
				        	{
				        		$td8.attr("class","warning");
				        	}
				        $td9 = $("<td>").text(item[8]).appendTo($tr);
				        	$td9.attr({ title:'',style:'width: 65px; font-size: 11px;'});
				        	$td9.attr("class","success");
				        	if(currentProgressPiority==6)
				        	{	
				        		$td9.attr("class","current");	
				        	}
				        	else if(currentProgressPiority<6)
				        	{
				        		$td9.attr("class","success");
				        	}
				        	else
				        	{
				        		$td9.attr("class","warning");
				        	}	
				        $td10 = $("<td>").text(item[9]).appendTo($tr);
				        	$td10.attr({ title:'',style:'width: 65px; font-size: 11px;'});
				        	$td10.attr("class","success");
				        	if(currentProgressPiority==5)
				        	{	
				        		$td10.attr("class","current");	
				        	}
				        	else if(currentProgressPiority<5)
				        	{
				        		$td10.attr("class","success");
				        	}
				        	else
				        	{
				        		$td10.attr("class","warning");
				        	}
				        $td11 = $("<td>").text(item[10]).appendTo($tr);	
				        	$td11.attr({ title:'',style:'width: 65px; font-size: 11px;'});
				        	$td11.attr("class","success");
				        	if(currentProgressPiority==4)
				        	{	
				        		$td11.attr("class","current");	
				        	}
				        	else if(currentProgressPiority<4)
				        	{
				        		$td11.attr("class","success");
				        	}
				        	else
				        	{
				        		$td11.attr("class","warning");
				        	}
				        $td12 = $("<td>").text(item[11]).appendTo($tr);
				        	$td12.attr({ title:'',style:'width: 65px; font-size: 11px;'});
				        	$td12.attr("class","success");
				        	if(currentProgressPiority==3)
				        	{	
				        		$td12.attr("class","current");	
				        	}
				        	else if(currentProgressPiority<3)
				        	{
				        		$td12.attr("class","success");
				        	}
				        	else
				        	{
				        		$td12.attr("class","warning");
				        	}
				        $td13 = $("<td>").text(item[12]).appendTo($tr);	
				        	$td13.attr({ title:'',style:'width: 65px; font-size: 11px;'});
				        	$td13.attr("class","success");
				        	if(currentProgressPiority==2)
				        	{	
				        		$td13.attr("class","current");	
				        	}
				        	else if(currentProgressPiority<2)
				        	{
				        		$td13.attr("class","success");
				        	}
				        	else
				        	{
				        		$td13.attr("class","warning");
				        	}
				        $td14 = $("<td>").text(item[13]).appendTo($tr);
				        	$td14.attr({ title:'',style:'width: 65px; font-size: 11px;'});
				        	$td14.attr("class","success");
				        	if(currentProgressPiority==1)
				        	{	
				        		$td14.attr("class","current");	
				        	}
				        	else if(currentProgressPiority<1)
				        	{
				        		$td14.attr("class","success");
				        	}
				        	else
				        	{
				        		$td14.attr("class","warning");
				        	}
				        	
				        	if(currentProgressPiority==10)
				        	{

				        		$td5.attr("style","background-color:#FFFEDB!important;width: 65px; font-size: 11px;");
				        	}
				        	if(currentProgressPiority==9)
				        	{

				        		$td6.attr("style","background-color:#FFFEDB!important;width: 65px; font-size: 11px;");
				        	}
				        	if(currentProgressPiority==8)
				        	{

				        		$td7.attr("style","background-color:#FFFEDB!important;width: 65px; font-size: 11px;");
				        	}
				        	if(currentProgressPiority==7)
				        	{

				        		$td8.attr("style","background-color:#FFFEDB!important;width: 65px; font-size: 11px;");
				        	}
				        	if(currentProgressPiority==6)
				        	{

				        		$td9.attr("style","background-color:#FFFEDB!important;width: 65px; font-size: 11px;");
				        	}
				        	if(currentProgressPiority==5)
				        	{

				        		$td10.attr("style","background-color:#FFFEDB!important;width: 65px; font-size: 11px;");
				        	}
				        	if(currentProgressPiority==4)
				        	{

				        		$td11.attr("style","background-color:#FFFEDB!important;width: 65px; font-size: 11px;");
				        	}
				        	if(currentProgressPiority==3)
				        	{

				        		$td12.attr("style","background-color:#FFFEDB!important;width: 65px; font-size: 11px;");
				        	}
				        	if(currentProgressPiority==2)
				        	{

				        		$td13.attr("style","background-color:#FFFEDB!important;width: 65px; font-size: 11px;");
				        	}
				        	if(currentProgressPiority==1)
				        	{

				        		$td14.attr("style","background-color:#FFFEDB!important;width: 65px; font-size: 11px;");
				        	}
				        $("#detail").append($tr);		        		
					});

			$('.btn_sendmail').click(function () {   
				
				$thisMail=$(this);
				$("#sendmail_body").val("");
				var classname=$thisMail.context.attributes.class.nodeValue;
				var sendmailID=$thisMail.context.attributes.name.nodeValue;
				console.log(classname);
				if(classname!="icon-alert")
				{
					$Global_thismail=$thisMail;
					$("#sendmail_modal").modal("show");					
					
				}
			 });
			$("#loading").hide();
		}

		function initQuickUpdate()
		{
			

		}$.ajax({ 
				type: "POST",
				dataType: "json", 
				data: {			        	
			        	saleid: GlobalSaleID,
			        	keyword: ""
			        },
		        url: "AJAX/LoadProject103_all_sign.php",
		        success: function (json) {
		        	loadData(json);
					//main.js to update file
		        },
				error: function() {
					alert("เกิดปัญหาในการโหลดข้อมูลกรุณารีเฟรชหน้าเว็บอีกครั้ง");
					$("#loading").hide();
				}
		    });
				$('#txtKeyword').keyup(function(e) {
					if(e.keyCode == 13) {
						search();
					}
				});
				function search(){
					//$("#loading").show();
					var Keyword = $("#txtKeyword").val();

					//document.getElementById("txtKeyword").value;
					$.ajax({ 
						type: "POST",
						dataType: "json", 
						data: {			        	
					        	saleid: GlobalSaleID,
					        	keyword: Keyword
					        },
				        url: "AJAX/LoadProject103_all_sign.php",
				        success: function (json) {
				        	loadData(json);
							//main.js to update file
				        },
						error: function() {
							alert("เกิดปัญหาในการโหลดข้อมูลกรุณารีเฟรชหน้าเว็บอีกครั้ง");
							$("#loading").hide();
						}
				    });

				}
				function getSearch(json) {
            	    console.log("json:",json);	
					document.getElementById('sumResult1').innerHTML=json.items.length;
					$('#detail1 > tbody:last').empty();
					if(json.items.length== 0){
						var noresult = "<tr><td colspan='6' style='text-align:center;'><p class='lead14'>ไม่พบข้อมูล</p></td></tr>";
						$('#detail1').append(noresult);
					}else{
						for(i=0;i<json.items.length;i++)
						{
							var organizationid = '"'+json.items[i].organizationid+'"';
							var thaioganization = (json.items[i].thaioganization!= null)?json.items[i].thaioganization:"";
							var organization = (json.items[i].organization!= null)?json.items[i].organization:" ";
							var thaidepartment = (json.items[i].thaidepartment!= null)?json.items[i].thaidepartment:"";
							var department = (json.items[i].department!= null)?json.items[i].department:"";
							var thaidivision = (json.items[i].thaidivision!= null)?json.items[i].thaidivision:"";
							var division = (json.items[i].division!= null)?json.items[i].division:" ";
							
							var cols="";
							cols+="<tr><td><p class='lead14'>"+json.items[i].organizationinitial+"</p></td>";
							cols+="<td><p class='lead14'>"+thaioganization+"<br>"+organization+"</p></td>";
							cols+="<td><p class='lead14'>"+thaidepartment+"<br>"+department+"</p></td>";
							cols+="<td><p class='lead14'>"+thaidivision+"<br>"+division+"</p></td>";
							if(division.replace(" ","") != "" || thaidivision.replace(" ","") != ""){
								cols+="<td style='text-align:center'></td>";
									
							}else if(department.replace(" ","") != "" || thaidepartment.replace(" ","") != ""){
								cols+="<td style='text-align:center'><a href='javascript:addDiv("+organizationid+",1);' ><i class='i_btn icon-plus-circle iconAddDiv' ></i></a></td>";
								
							}else{
								cols+="<td style='text-align:center'><a href='javascript:addDep("+organizationid+",1);' ><i class='i_btn icon-plus-circle iconAddDep'></i></a></td>";
							}
							cols+="<td style='text-align:center'><a href='javascript:edit("+organizationid+",1);' ><i class='i_btn icon-pencil-1 iconEdit' ></i></a></td></tr>";
							$('#detail1').append(cols);
							
						}
					$('.iconAddDep').tooltip({
						placement: 'bottom',
						title : 'Add Department'
					});
					$('.iconAddDiv').tooltip({
						placement: 'bottom',
						title : 'Add Division'
					});
					$('.iconEdit').tooltip({
						placement: 'bottom',
						title : 'Edit'
					});
					}
					$("#loading").hide();
				}
								
				function edit(id,site){
					setCookie("Ses_Keyword_index",keywordHome);
					setCookie("Ses_Site",site);
					window.location = "new.php?id=" + id + "&page=home";
				}
				function addDep(id,site){
					setCookie("Ses_Keyword_index",keywordHome);
					setCookie("Ses_Site",site);
					window.location = "new.php?org=" + parseInt(id.substr(0, 4)) + "&page=home";
				}
				function addDiv(id,site){
					setCookie("Ses_Keyword_index",keywordHome);
					setCookie("Ses_Site",site);
					window.location = "new.php?org=" + parseInt(id.substr(0, 4)) + "&dep=" + parseInt(id.substr(4, 3)) + "&page=home";
				}
				
		

//------------------------------------------------- Function ------------------------------------------------
		function numFormat(x) {
				var parts = x.toString().split(".");
			    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			    return parts.join(".");
		}
//-----------------------------------------------------------------------------------------------------------
	</script>

</body>
</html>