<?php 
function ThaiIToUTF8($in) { 
	$out = ""; 
	for ($i = 0; $i < strlen($in); $i++) 
	{
		if (ord($in[$i]) <= 126) 
		$out .= $in[$i];
	else 
		$out .= "&#" . (ord($in[$i]) - 161 + 3585) . ";"; 
	} 
	return $out; 
} 
function funcSaleIn($id) 
{
	$myid=split(",",$id);
	$myCount=count($myid);
	$mythiname="";
	for($i=0;$i<$myCount;$i++)
	{
		if($i===0)
		{
			$mythiname="'".$myid[$i]."'";
		}
		else
		{
			$mythiname=$mythiname.",'".$myid[$i]."'";
		}
	}
	return $mythiname;
}
	

	/*include("INC/connectSFC.php");
	$sqlForecast ="select IDForecast from Forecast where IDForecast in (select IDForecast from ESRIProduct ) and Progress in ($Progress) and year($where)='$BidingYear' ";
	$result=sqlsrv_query($sqlForecast,$ConnectSaleForecast);
	$sqlSpa = '';
	while($obj=sqlsrv_fetch_object($result))
	{

	}*/
	
	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
	header("Content-Type: application/download");
	header("Content-Transfer-Encoding: binary ");
	header('Content-type: application/ms-excel');		
	header("Content-Disposition: attachment; filename=".basename("SW_monthly_Report.xls").";");
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="content-type" content="text/html;charset=window-874" />
<title>:Report:</title>

</head>
<body >
<table cellpadding="0" cellspacing="0" border="0"width="100%" >
	<thead>
		<tr>
			<th style="text-align:center; vertical-align:middle; border-left: 1px solid black; border-top: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black;">No.</th>
			<th style="text-align:center; vertical-align:middle; border-top: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black;">PE-Contract</th>
			<th style="text-align:center; vertical-align:middle; border-top: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black;">Potential</th>
			<th style="text-align:center; vertical-align:middle; border-top: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black;">Progress</th>	
			<th style="text-align:center; vertical-align:middle; border-top: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black;">Project Name</th>
			<th style="text-align:center; vertical-align:middle; border-top: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black;">ESRI SW List</th>
			<th style="text-align:center; vertical-align:middle; border-top: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black;">Amount</th>
			<th style="text-align:center; vertical-align:middle; border-top: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black;">List Price (THB)</th>
			<th style="text-align:center; vertical-align:middle; border-top: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black;">Total Price (THB)</th>
			<th style="text-align:center; vertical-align:middle; border-top: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black;">Sign Date</th>
			<th style="text-align:center; vertical-align:middle; border-top: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black;">Delivery Date</th>
			<th style="text-align:center; vertical-align:middle; border-top: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black;">Estimate P.O Date</th>
			<th style="text-align:center; vertical-align:middle; border-top: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black;">Sales Name</th>
		</tr>	
  	</thead>
  	<tbody>
  		<?php	
  			if(date('Y',strtotime("now"))==$BidingYear){
  				$mindate1 = date('Y-m',strtotime("now"))."-01";
  				$mindate2 = date('Y',strtotime("now"))."-01-01";
  				$maxdate1 = $BidingYear."-12-31";
  				$checkDate1 = "AND a.".$where." >= '".$mindate1."' AND a.".$where." <= '".$maxdate1."'";
  				$checkDate2 = "AND a.".$where." >= '".$mindate2."' AND a.".$where." <= '".$maxdate1."'";
  			}else{
  				$mindate1 = $BidingYear."-01-01";  				  				
  				$maxdate1 = $BidingYear."-12-31";
  				$checkDate1 = "AND a.".$where." >= '".$mindate1."' AND a.".$where." <= '".$maxdate1."'";
  				$checkDate2 = "AND a.".$where." >= '".$mindate1."' AND a.".$where." <= '".$maxdate1."'";
  			}
  			include("INC/connectSFC.php");
			$sql= "SELECT a.PEContractNo, a.Potential, a.Progress, a.Project, convert(varchar,a.TimeFrameContractSigndate,103) as TimeFrameContractSigndate, convert(varchar,a.TimeFrameDeliveryDate, 103) as TimeFrameDeliveryDate, a.SaleRepresentative, a.SaleID, a.IDForecast, b.EPName, b.qty, c.baht as thbaht
					FROM (Forecast as a RIGHT JOIN ESRIProduct as b ON a.IDForecast= b.IDForecast) LEFT JOIN products as c ON b.Product_id=c.id
					WHERE a.Progress NOT IN ('v', '0', '100') $checkDate1
					UNION ALL
					SELECT a.PEContractNo, a.Potential, a.Progress, a.Project, convert(varchar,a.TimeFrameContractSigndate,103) as TimeFrameContractSigndate, convert(varchar,a.TimeFrameDeliveryDate, 103) as TimeFrameDeliveryDate, a.SaleRepresentative, a.SaleID, a.IDForecast, b.EPName, b.qty, c.baht as thbaht 
					FROM (Forecast as a RIGHT JOIN ESRIProduct as b ON a.IDForecast= b.IDForecast) LEFT JOIN products as c ON b.Product_id=c.id 
					WHERE a.Progress = '100' $checkDate2
					ORDER BY SaleRepresentative, TimeFrameContractSigndate";
  			$result=sqlsrv_query($ConnectSaleForecast,$sql);
			$i=0;
			$PEContractNoChk = "";
			$ProjectChk = "";
			while($obj=sqlsrv_fetch_object($result))
			{
				$PEContractNo = ThaiIToUTF8($obj->PEContractNo);
				$Potential = ThaiIToUTF8($obj->Potential);
				$Progress = ThaiIToUTF8($obj->Progress);
				$Project = ThaiIToUTF8($obj->Project);
				$TimeFrameContractSigndate = ThaiIToUTF8($obj->TimeFrameContractSigndate);
				$TimeFrameDeliveryDate = ThaiIToUTF8($obj->TimeFrameDeliveryDate);
				list($day, $month, $year) = split('[/.-]', $TimeFrameDeliveryDate);
				$EstimateReceiveDate = gmdate ("d-m-Y", mktime (0,0,0,$month,$day-30,$year));
				$SaleRepresentative = ThaiIToUTF8($obj->SaleRepresentative);
				$IDForecast = ThaiIToUTF8($obj->IDForecast);
				$EPName = ThaiIToUTF8($obj->EPName);
				$qty = ThaiIToUTF8($obj->qty);
				$baht = $obj->thbaht;
			?>	
			<tr>
				<td style="text-align:center; vertical-align:middle; border-left: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black;"><?php if($PEContractNoChk!=$PEContractNo || $ProjectChk!=$Project){$i++; echo $i; $PEContractNoChk=$PEContractNo; $ProjectChk=$Project;}else{echo $i;}?></td>
				<td style="text-align:center; vertical-align:middle; border-right: 1px solid black; border-bottom: 1px solid black;"><?php echo $PEContractNo?></td>
				<td style="text-align:center; vertical-align:middle; border-right: 1px solid black; border-bottom: 1px solid black;"><?php echo $Potential?></td>
				<td style="text-align:center; vertical-align:middle; border-right: 1px solid black; border-bottom: 1px solid black;"><?php echo $Progress?></td>
				<td style="text-align:left; vertical-align:top; border-right: 1px solid black; border-bottom: 1px solid black;"><?php echo $Project?></td>
				<td style="text-align:left; vertical-align:top; border-right: 1px solid black; border-bottom: 1px solid black;"><?php echo $EPName?></td>
				<td style="text-align:center; vertical-align:middle; border-right: 1px solid black; border-bottom: 1px solid black;"><?php echo $qty?></td>
				<td style="text-align:right; vertical-align:middle; border-right: 1px solid black; border-bottom: 1px solid black;"><?php echo number_format(trim($baht),2)?></td>
				<td style="text-align:right; vertical-align:middle; border-right: 1px solid black; border-bottom: 1px solid black;"><?php echo number_format((double)$qty*(double)$baht,2); ?></td>
				<td style="text-align:center; vertical-align:middle; border-right: 1px solid black; border-bottom: 1px solid black;"><?php echo $TimeFrameContractSigndate;?></td>
				<td style="text-align:center; vertical-align:middle; border-right: 1px solid black; border-bottom: 1px solid black;"><?php echo $TimeFrameDeliveryDate;?></td>
				<td style="text-align:center; vertical-align:middle; border-right: 1px solid black; border-bottom: 1px solid black;"><?php echo $EstimateReceiveDate;?></td>
				<td style="text-align:left; vertical-align:middle; border-right: 1px solid black; border-bottom: 1px solid black;"><?php echo $SaleRepresentative?></td>			
			</tr>

		<?php
			
			}

  		?>
  		<tr>
  			<td colspan=13></td>
  		</tr>
  	</tbody>
 </table> 
</body>
</html>
