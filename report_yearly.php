﻿<!DOCTYPE html>
<html lang="en">
	
<!-- Mirrored from bootstrapstyler.com/preview/_/flatdashboard/ui.html by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 18 Oct 2013 07:02:36 GMT -->
<!-- Added by HTTrack --><!-- /Added by HTTrack -->
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
		<meta charset="utf-8">
		<title>SFS:::Yearly Report</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="Izuddin Helmi">

		<!-- Stylesheets -->
		<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
		<link href="js/library/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
		<link href="css/styler/style.css" rel="stylesheet">
		<link href="js/library/syntaxHighlighter/shCore.css" rel="stylesheet" type="text/css" />
		<link href="js/library/syntaxHighlighter/shThemeDefault.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="css/styler/fontello.css">
	    <link rel="stylesheet" href="css/styler/animation.css"><!--[if IE 7]>
	    <link rel="stylesheet" href="css/styler/fontello-ie7.css"><![endif]-->
	    

		<!-- Custom Fonts -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300,500' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
		<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
		  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	</head>
<?php  $checkmenu = '4'; ?>
<body cz-shortcut-listen="true" style="margin-top:0px;">	
<div id="loading" style="height: 100%; width: 100%; position: fixed; left: 0; top: 0; z-index: 1051 !important; background-color: black; filter: alpha(opacity=9); -khtml-opacity: 0.9; -moz-opacity: 0.9; opacity: 0.9;"><img src="img/loading.gif" style="width: 150px; height: 150px; position: fixed; top: 50%; left: 50%; margin: -75px 0 0 -75px;"></div>	
<section class="page-header bg-color white">
	<div class="container">
		<div class="row">
			<div class="span12">
				<div class="section-header clearfix">
					<div class="pull-left">
						<h2><i class="icon-clipboard-1"></i>Report</h2>
					</div>
					<div id="shortcut" class="nav-collapse pull-right"><ul class="shortcuts unstyled">
						<?php include("menu.php");?>
					</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="content">
	<div class="page-tab">
		<div class="page-tab-header">
			<div class="container">
				<ul class="nav nav-tabs clearfix" id="socialTab">
					<li><a href="report_PT_all.php"><h5>Potential</h5></a></li>
					<li ><a  href="report_all.php"><h5>Progress</h5></a></li>
					<li ><a href="report_month_all.php"><h5>Month</h5></a></li>
					<li class="active"><a href="#"><h5>Yearly</h5></a></li>
					<li><a href="report_Qur_all.php"><h5>ภาพรวมรายไตรมาส</h5></a></li>
					<li><a href="report_potential_all.php"><h5>ภาพรวมรายเดือน</h5></a></li>
					<li ><a href="report_siteref_all.php"><h5>Site Reference หนังสือรับรอง</h5></a></li>
				</ul>
			</div>
		</div>

		<div class="page-tab-header" style="background: #EEEFF2!important">
			<div class="container">
				<ul class="nav nav-tabs clearfix" id="socialTab" style="margin-left:250px;">
					<li class="active"><a href="#"><h5>Yearly Report</h5></a></li>
					<li ><a href="report_yearly_config.php"><h5>กำหนดโครงการเพื่อออกรายงาน Yearly Report</h5></a></li>
				</ul>
			</div>
		</div>
		<div class="tab-content">
			<div class="tab-pane fade active in container" id="ReportProgress">
				<div class="widget bg-color turqoise rounded" id="focusReport">
					<div class="box-padding">
						<h1>Yearly Report</h1>
						<p class="lead"></p>
					</div>
				</div>
				<div class="row">
					<div class="span2 widget widget-option"></div>
					<div class="span3 widget widget-option">
						<div class="profile-head bg-color dark-blue rounded-top">
							<div class="box-padding" style="text-align:center">
									<h3 class="normal"><i class="icon-calendar"></i>Year</h3>
									<span class=" pull-left" style="margin-left:-10px"></span>
							</div>
						</div>
						<div class="bg-color white rounded-bottom">
								<ul class="menu unstyled">
									<li class="current"><p>
									  	
									  	 Signdate <span class="pull-right"  style="margin-top:-8px"><select id="slc_year_sign" name="slc_year_sign" class="span1" style="width:80px;"></select></span></p></li>
								</ul>
						</div>

						<div class="profile-head bg-color dark-blue rounded-top">
							<div class="box-padding" style="text-align:center">
									<h3 class="normal"><i class="icon-th-thumb-empty"></i>Option</h3>
							</div>
						</div>
						<div class="bg-color white rounded-bottom">
								<ul class="menu unstyled">
									<li><p class="sales">
									  	<input type="checkbox" value="1" style="margin-top:-2px" class="chkSpecial"> Mega Project<span class="pull-right" style="margin-top:-8px"></span></p></li>
								</ul>
						</div>
						<div style="text-align:center"><a class="btn btn-blue btn-large" id="btnReport" style="width:190px">Report</a></div>

						<form name="frmexport"  action="ExportReport_Yearly.php" method="POST">
							
							<input type="hidden" name="year"  value="">
							<input type="hidden" name="Fyear" value="">
							<input type="hidden" name="Ryear" value="">
							<input type="hidden" name="saleid" value="<?php echo $_COOKIE['Ses_ID']?>">
							<input type="hidden" name="option" value="0">
							<input type="hidden" name="Json1" value="0">
							<input type="hidden" name="Json2" value="0">
						</form>
						<div style="text-align:center"><a class="btn btn-blue btn-large" id="btnExport" style="width:190px">Export by Industry</a></div>
						<form name="frmexport_ministry"  action="ExportReport_Yearly_Ministry.php"  method="POST" >
							<input type="hidden" name="year"  value="">
							<input type="hidden" name="Fyear" value="">
							<input type="hidden" name="Ryear" value="">
							<input type="hidden" name="saleid" value="<?php echo $_COOKIE['Ses_ID']?>">
							<input type="hidden" name="option" value="0">
							<input type="hidden" name="Json1M" value="0">
							<input type="hidden" name="Json2M" value="0">
						</form>
						<div style="text-align:center"><a class="btn btn-blue btn-large" id="btnExport_Ministry" style="width:190px">Export by Ministry</a></div>
						<!--
						//icon-info-circle icon-info-circled icon-warning icon-attention-circle icon-attention-3
						<div id="showError" class="alert alert-error hide" style="margin-top:10px;margin-bottom:-10px;">
							<button type="button" class="close" data-dismiss="alert">×</button>
							<strong>Error!</strong> กรุณาเลือก Sale ด้วย
						</div>
						-->
						
						<div id="showError" class="alert alert-error hide" style="margin-top:10px;margin-bottom:-10px;">
							<button type="button" class="close" data-dismiss="alert">×</button>
								<i class="icon-attention-3 pull-left" style="font-size:20px;margin-top:-3px;"></i>
								<div class="media-body">
									<strong>กรุณาเลือก Sale ด้วย</strong>
								</div>
						</div>
						<div id="showWarning" class="alert alert-block hide" style="margin-top:10px;margin-bottom:-10px;">
							<button type="button" class="close" data-dismiss="alert">×</button>
							<i class="icon-info-circle pull-left" style="font-size:25px;margin-top:-7px;"></i>
							<div class="media-body">
								<strong>ไม่พบข้อมูล</strong>
							</div>
						</div>
					</div>				
					
					<!-- <div class="span5">
						<div class="widget widget-profile">
							<div class="profile-head bg-color dark-blue rounded-top">
								<div class="box-padding">
									<h3 class="normal"><i class="icon-progress-3"></i>Progress</h3>
									<span class=" pull-right" style="margin-right:-10px;margin-top:-30px;"><input type="checkbox" id="progressAll" title="Select all"></span>
								</div>
							</div>

							<div class="bg-color white rounded-bottom">
								<ul class="menu unstyled">
									<li class="current"><p>100% Sign date<span class=" pull-right"><input type="checkbox" name="chkprog" value="100" class="progress" checked></span></p></li>
									<li class="current"><p>90% <b>ประกาศผลแล้วรอเซ็นสัญญา</b><span class="pull-right"><input type="checkbox" name="chkprog" value="90" class="progress" checked></span></p></li>
									<li class="current"><p>80% <b>ยื่นซองแล้ว</b><span class="pull-right"><input type="checkbox" name="chkprog" value="80" class="progress" checked ></span></p></li>
									<li class="current"><p>70% Bidding date<span class="pull-right"><input type="checkbox" name="chkprog" value="70" class="progress" checked></span></p></li>
									<li class="current"><p>60% TOR Final<span class="pull-right"><input type="checkbox" name="chkprog" value="60" class="progress" checked></span></p></li>
									<li class="current"><p>50% <b>ได้งบประมาณ</b><span class="pull-right"><input type="checkbox" name="chkprog" value="50" class="progress" checked></span></p></li>
									<li class="current"><p>40% <b>ขั้นตอนการพิจารณาอนุมัติงบประมาณ</b><span class="pull-right"><input type="checkbox" name="chkprog" value="40" class="progress" checked></span></p></li>
									<li class="current"><p>30% <b>ร่างโครงการ (ของบประมาณ)</b><span class="pull-right"><input type="checkbox" name="chkprog" value="30" class="progress" checked></span></p></li>
									<li class="current"><p>20% Presentation/Demo<span class="pull-right"><input type="checkbox" name="chkprog" value="20" class="progress" checked></span></p></li>
									<li class="current"><p>10% Build solution<span class="pull-right"><input type="checkbox" name="chkprog" value="10" class="progress"  checked></span></p></li>
									<li><p>0% <b>แพ้</b><span class="pull-right"><input type="checkbox" name="chkprog" value="0" class="progress" ></span></p></li>
									<li><p>V <b>ยกเลิกโครงการ</b><span class="pull-right"><input type="checkbox" name="chkprog" value="v" class="progress" ></span></p></li>
									<li><p>P <b>Pendding</b><span class="pull-right"><input type="checkbox" name="chkprog" value="p" class="progress" ></span></p></li>
									
								</ul>
							</div>

						</div>
					</div> -->
					<div class="span5 widget widget-option">

						<div class="profile-head bg-color dark-blue rounded-top">
							<div class="box-padding" style="text-align:center">
									<h3 class="normal"><i class="icon-users-1"></i>Sales</h3>
									<span class=" pull-left" style="margin-left:-10px;margin-top:-20px"><input type="checkbox" class="salesAll" title="Select all" checked></span>
							</div>
						</div>
						<div class="bg-color white rounded-bottom">
								<ul class="menu unstyled" id="listsaleName"></ul>
						</div>
					</div>
				</div>
				<div class="widget widget-chart bg-color rounded" id="PO_Detail" style="display:none"><!--"-->

					<div class="clearfix" style="background-color: #EEEFF2;" id="focusShow">
						<h2 class="charts-data-quarter pull-left" style="color: black;" >SALES FORECAST <p class="PO_year" style="display: inline;font-size: 31.5px;color:black"></p> (<p id="PO_saleDep"  style="display: inline;font-size: 31.5px;color:black"></p>) - </h2>
						<div ><a class="btn btn-success btn-large" id="byIndustry" style="width:150px">by Industry</a> <a class="btn btn-gray btn-large" id="byMinistry" style="width:150px">by Ministry</a></div>
					</div>

					
					<div id="sortbyIndustry"  style="display:block;">
						<div style="background-color: #2ABF9E;">
							<div id="charts-data" class="monthly clearfix">	
								<div class="box-padding" style="padding:20px;">
									<div class="clearfix" >

										<h2 class="charts-data-quarter pull-right" style="color: #FFFFFF;" >by Industry</h2>
										<h2 class="charts-data-quarter pull-left" style="color: #FFFFFF;" >PO FORECAST BY QUARTERS</h2>
									</div>
								</div>
								<div class="charts-data-table">
									<div class="box-padding" style="padding:0px;" >
										<table class="table table-striped showdetail" id="PO_tbldetail" >
											<thead>
												<tr role="row">
													<th width="300px"></th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Q1</th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Q2</th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Q3</th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Q4</th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Sum</th>
												</tr>
											</thead>
											<tbody ></tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div style="background-color: #2ABF9E;margin-top: 50px;">
							<div id="charts-data" class="monthly clearfix">	
								<div class="box-padding" style="padding:20px;">
									<div class="clearfix" >
										
										<h2 class="charts-data-quarter pull-left" style="color: #FFFFFF;" >Book Of Balance Forward</h2>
									</div>
								</div>
								<div class="charts-data-table">
									<div class="box-padding" style="padding:0px;" >
										<table class="table table-striped showdetail" id="PO3_tbldetail" >
											<thead>
												<tr role="row">
													<th width="300px"></th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Q1</th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Q2</th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Q3</th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Q4</th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Sum</th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px"></th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px"></th>
												</tr>
											</thead>
											<tbody ></tbody>
										</table>
									</div>
								</div>
								<div class="box-padding" style="padding:20px;">
									<div class="clearfix" >
										
										<h2 class="charts-data-quarter pull-left" style="color: #FFFFFF;" >Book Of <p class="PO_year"  style="display: inline;font-size: 31.5px;color:#FFFFFF;"></p>  PO</h2>
									</div>
								</div>
								<div class="charts-data-table">
									<div class="box-padding" style="padding:0px;" >
										<table class="table table-striped showdetail" id="PO2_tbldetail" >
											<thead>
												<tr role="row">
													<th width="300px"></th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Q1</th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Q2</th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Q3</th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Q4</th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Book <p class="PO_year"  style="display: inline;color: #FFFFFF!important;"></p></th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Forward <p class="PO_year1"  style="display: inline;color: #FFFFFF!important;"></p></th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Sum</th>
												</tr>
											</thead>
											<tbody ></tbody>
										</table>
									</div>
								</div>
							</div>
						</div>

						<div style="background-color: #2ABF9E;margin-top: 50px;">
							<div id="charts-data" class="monthly clearfix">	
								<div class="box-padding" style="padding:20px;">
									<div class="clearfix" >
										
										<h2 class="charts-data-quarter pull-left" style="color: #FFFFFF;" >ESRI SW PO FORECAST BY QUARTERS</h2>
									</div>
								</div>
								<div class="charts-data-table">
									<div class="box-padding" style="padding:0px;" >
										<table class="table table-striped showdetail" id="PO4_tbldetail" >
											<thead>
												<tr role="row">
													<th width="300px"></th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Q1</th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Q2</th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Q3</th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Q4</th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Book <p class="PO_year"  style="display: inline;color: #FFFFFF!important;"></p></th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Forward <p class="PO_year1"  style="display: inline;color: #FFFFFF!important;"></p></th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Sum</th>
												</tr>
											</thead>
											<tbody ></tbody>
										</table>
									</div>
								</div>
							</div>
							<div style="background-color: #2ABF9E;margin-top: 50px;">
								<div class="box-padding" style="padding:20px;">
									<div class="clearfix" >
										
										<h2 class="charts-data-quarter pull-left" style="color: #FFFFFF;" >ESRI SW PO FORECAST TO ESRI THAILAND BY QUARTERS</h2>
									</div>
								</div>
								<div class="charts-data-table">
									<div class="box-padding" style="padding:0px;" >
										<table class="table table-striped showdetail" id="PO5_tbldetail" >
											<thead>
												<tr role="row">
													<th width="300px"></th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Q1</th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Q2</th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Q3</th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Q4</th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Book <p class="PO_year"  style="display: inline;color: #FFFFFF!important;"></p></th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Forward <p class="PO_year1"  style="display: inline;color: #FFFFFF!important;"></p></th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Sum</th>
												</tr>
											</thead>
											<tbody ></tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div id="sortbyMinistry" style="display:none;">
						<div style="background-color: #2ABF9E;">
							<div id="charts-data" class="monthly clearfix">	
								<div class="box-padding" style="padding:20px;">
									<div class="clearfix" >
										
										<h2 class="charts-data-quarter pull-right" style="color: #FFFFFF;" >by Ministry</h2>
										<h2 class="charts-data-quarter pull-left" style="color: #FFFFFF;" >PO FORECAST BY QUARTERS</h2>
									</div>
								</div>
								<div class="charts-data-table">
									<div class="box-padding" style="padding:0px;" >
										<table class="table table-striped showdetail" id="PO_tbldetail_Ministry" >
											<thead>
												<tr role="row">
													<th width="300px"></th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Q1</th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Q2</th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Q3</th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Q4</th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Sum</th>
												</tr>
											</thead>
											<tbody ></tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div style="background-color: #2ABF9E;margin-top: 50px;">
							<div id="charts-data" class="monthly clearfix">	
								<div class="box-padding" style="padding:20px;">
									<div class="clearfix" >
										
										<h2 class="charts-data-quarter pull-left" style="color: #FFFFFF;" >Book Of Balance Forward</h2>
									</div>
								</div>
								<div class="charts-data-table">
									<div class="box-padding" style="padding:0px;" >
										<table class="table table-striped showdetail" id="PO3_tbldetail_Ministry" >
											<thead>
												<tr role="row">
													<th width="300px"></th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Q1</th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Q2</th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Q3</th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Q4</th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Sum</th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px"></th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px"></th>
												</tr>
											</thead>
											<tbody ></tbody>
										</table>
									</div>
								</div>
								<div class="box-padding" style="padding:20px;">
									<div class="clearfix" >
										
										<h2 class="charts-data-quarter pull-left" style="color: #FFFFFF;" >Book Of <p class="PO_year"  style="display: inline;font-size: 31.5px;color:#FFFFFF;"></p>  PO</h2>
									</div>
								</div>
								<div class="charts-data-table">
									<div class="box-padding" style="padding:0px;" >
										<table class="table table-striped showdetail" id="PO2_tbldetail_Ministry" >
											<thead>
												<tr role="row">
													<th width="300px"></th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Q1</th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Q2</th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Q3</th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Q4</th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Book <p class="PO_year"  style="display: inline;color: #FFFFFF!important;"></p></th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Forward <p class="PO_year1"  style="display: inline;color: #FFFFFF!important;"></p></th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Sum</th>
												</tr>
											</thead>
											<tbody ></tbody>
										</table>
									</div>
								</div>
							</div>
						</div>

						<div style="background-color: #2ABF9E;margin-top: 50px;">
							<div id="charts-data" class="monthly clearfix">	
								<div class="box-padding" style="padding:20px;">
									<div class="clearfix" >
										
										<h2 class="charts-data-quarter pull-left" style="color: #FFFFFF;" >ESRI SW PO FORECAST BY QUARTERS</h2>
									</div>
								</div>
								<div class="charts-data-table">
									<div class="box-padding" style="padding:0px;" >
										<table class="table table-striped showdetail" id="PO4_tbldetail_Ministry" >
											<thead>
												<tr role="row">
													<th width="300px"></th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Q1</th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Q2</th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Q3</th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Q4</th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Book <p class="PO_year"  style="display: inline;color: #FFFFFF!important;"></p></th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Forward <p class="PO_year1"  style="display: inline;color: #FFFFFF!important;"></p></th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Sum</th>
												</tr>
											</thead>
											<tbody ></tbody>
										</table>
									</div>
								</div>
							</div>
							<div style="background-color: #2ABF9E;margin-top: 50px;">
								<div class="box-padding" style="padding:20px;">
									<div class="clearfix" >
										
										<h2 class="charts-data-quarter pull-left" style="color: #FFFFFF;" >ESRI SW PO FORECAST TO ESRI THAILAND BY QUARTERS</h2>
									</div>
								</div>
								<div class="charts-data-table">
									<div class="box-padding" style="padding:0px;" >
										<table class="table table-striped showdetail" id="PO5_tbldetail_Ministry" >
											<thead>
												<tr role="row">
													<th width="300px"></th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Q1</th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Q2</th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Q3</th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Q4</th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Book <p class="PO_year"  style="display: inline;color: #FFFFFF!important;"></p></th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Forward <p class="PO_year1"  style="display: inline;color: #FFFFFF!important;"></p></th>
													<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align: center" width="120px">Sum</th>
												</tr>
											</thead>
											<tbody ></tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
		<div id="footer" class="bg-color dark-blue">
			<div class="container">
				<div class="box-padding">
					Copyright &copy; 2013 Sales Forecast
				</div>
			</div>
		</div>
		<!-- Javascript -->
		<!-- JS:jquery-->
		<!--<script src="../../../../ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>-->
  		<script src="js/library/jquery/jquery.min.js"></script>
		<script src="js/library/jquery/jquery-ui.min.js"></script>

		<!-- JS:masonry-->
		<script src="js/library/masonry/jquery.masonry.min.js"></script>

		<!-- JS:flexslider-->
		<script src="js/library/flexslider/jquery.flexslider-min.js"></script>

		<!-- JS:tables-->
		<script src="js/library/dataTables/jquery.dataTables.min.js"></script>

		<!-- JS:calendar-->
		<script src="js/library/fullcalendar/fullcalendar.min.js"></script>
		<script src="js/library/fullcalendar/gcal.js"></script>

		<!-- JS:forms-->
		<script src="js/library/forms/jquery.hotkeys.js"></script>
		<script src="js/library/forms/bootstrap-wysiwyg.js"></script>
		<script src="js/library/forms/picker.js"></script>
		<script src="js/library/forms/picker.date.js"></script>
		<script src="js/library/forms/picker.time.js"></script>
		<script src="js/library/forms/jquery.bootstrap.wizard.min.js"></script>

		<!-- JS:charts-->
		<script src="js/library/charts/jquery.flot.js"></script>
		<script src="js/library/charts/jquery.flot.resize.js"></script>
		<script src="js/library/charts/jquery.flot.stack.js"></script>
		<script src="js/library/charts/jquery.flot.pie.min.js"></script>
		<script src="js/library/charts/jquery.sparkline.min.js"></script>
		<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="excanvas.min.js"></script><![endif]-->

		<!-- JS:syntaxHighlighter-->
		<script src="js/library/syntaxHighlighter/shCore.js"></script>
		<script src="js/library/syntaxHighlighter/shBrushCss.js"></script>
		<script src="js/library/syntaxHighlighter/shBrushXml.js"></script>
		<script src="js/library/syntaxHighlighter/shBrushJScript.js"></script>

		<!-- JS:bootstrap-->
		<script src="bootstrap/js/bootstrap.min.js"></script>
		
		<!-- JS:custom js for this template -->
		<script src="js/styler/custom.js"></script>

							<script src="js/highcharts.js"></script>
							<script src="js/exporting.js"></script>
		<script src="js/log_forecast.js"></script>
		<script type="text/javascript">
			var saleid= '<?php echo $_COOKIE['Ses_ID']?>';
			var userid= '<?php echo $_COOKIE['Ses_ID']?>';
			var typeYear= 'signdate';
			var slcYear=(new Date).getFullYear();
			var slcYear1=slcYear+1;
			var slcOption='0';
			var slcOptionstart='0';
			var slcOptionend='0';
			var selectNo = '1';
			var PotentialIncome100=0;
			var PotentialIncome75=0;
			var PotentialIncome50=0;
			var PotentialIncome25=0;
			var PotentialSum100=0;
			var PotentialSum75=0;
			var PotentialSum50=0;
			var PotentialSum25=0;
			var PotentialSelect="PotentialTag100";
			var ckSale = saleid;
			var ckYear = slcYear;
			var GrandTotalQ1=0;
			var GrandTotalQ2=0;
			var GrandTotalQ3=0;
			var GrandTotalQ4=0;
			var GrandTotalBook=0;
			var GrandTotalForward=0;
			var GrandTotalSum=0;
			var GrandTotalQ1M=0;
			var GrandTotalQ2M=0;
			var GrandTotalQ3M=0;
			var GrandTotalQ4M=0;
			var GrandTotalBookM=0;
			var GrandTotalForwardM=0;
			var GrandTotalSumM=0;

			Number.prototype.nformat = function() {
			    return this.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
			};
			var _loginID = "<?php echo $_COOKIE['Ses_ID']?>";
		    Log_Forecast("Report_All","Progress",_loginID,"Start","Load","-");
		    $(document).ready(function(){   

				function getSaleID(){
						var chkid='';
						$("input[name='chkids']").each(function () {
								if ( $(this).prop('checked')) {
									if(chkid=='')
									{
										chkid= $(this).val();
									}
									else
									{
										chkid = chkid+","+$(this).val();
									}
								}
						});
						document.frmreport.saleid.value=chkid;
						//showGraph();
				}
				function getSaleIDNO(){
						var chkid='';
						$("input[name='chkids']").each(function () {
								if ( $(this).prop('checked')) {
									if(chkid=='')
									{
										chkid= $(this).val();
									}
									else
									{
										chkid = chkid+","+$(this).val();
									}
								}
						});
						document.frmreport.saleid.value=chkid;
						//showGraph();
				}
			    $.ajax({ 
			    	cache: false,
					type: "POST",
					dataType: "json",
		        	url: "AJAX/select_yearSign.php",
		        	success: function (json) {
		        		$.each(json, function() {
							var item = this;
		        			$("#slc_year_sign").append("<option value='" + item[0] + "'>" + item[0] + "</option>");
						});
						$("#slc_year_sign").val((new Date).getFullYear());
			        },
			        error: function(err)
			        {
			        	Log_Forecast("Report_All","Progress",_loginID,"select_yearSign","error",err);
			        }
			    });
				
				if (_loginID == "0095") {
						depMng = "sales TM";
					} else if (_loginID == "4371") {
						depMng = "sales 1";
					} else if (_loginID == "0416") {
						depMng = "sales 2";
					} else if (_loginID == "3581") {
						depMng = "sales 3";
					} else if (_loginID == "3882") {
						depMng = "sales 4";
					} else if (_loginID == "4583") {
						depMng = "sales CSD";
					} else if (_loginID == "1190") {
						depMng = "Sales GEO";
					} else if (_loginID == "1994") {
						depMng = "Sales CLMV";
					} else if (_loginID == "3784") {
						depMng = "Sales CSD";
					}else {
						depMng = "";
					}
					$.ajax({ 
						type:"POST",
						dataType: "json",
						url: "Ajax/SV_Select_Sale.php",
						data: {
							saledept: depMng,
							loginid : _loginID
						},
						success: function (json) {
							depMng = json[0];
							if(json[0].length>1)
								{
									checked = "checked";
									classli = "current";
									for(var ii = 0;ii<json[0].length;ii++){
										$("#listsaleName").append('<li class="' + classli + '"><p class="sales"><input type="checkbox" name="selectSales_allsale" style="margin-top:-2px" class="selectSales_all'+ii+'"  ' + checked + ' onclick="SelectSale('+ii+')" />All '+json[0][ii]+'<span class=" pull-right" style="margin-top:-8px"></span></p></li>');
										$(json).each(function() {
				        		var empno=this[0];
				        		var name=this[1];
								
								
								if(this[2] == json[0][ii]){
				        		$("#listsaleName").append('<li class="' + classli + '"><p class="sales"><input type="checkbox" name="chkname'+ii+'" style="margin-top:-2px" class="chksales" value="' + empno + '" ' + checked + ' /> ' + name + '<span class=" pull-right" style="margin-top:-8px"></span></p></li>');
								}
							});
										}
								}else{
							$(json).each(function() {
				        		var empno=this[0];
				        		var name=this[1];
								
								checked = "checked";
								classli = "current";
								if(this[2] == json[0][0]){
				        		$("#listsaleName").append('<li class="' + classli + '"><p class="sales"><input type="checkbox" name="chkname" style="margin-top:-2px" class="chksales" value="' + empno + '" ' + checked + ' /> ' + name + '<span class=" pull-right" style="margin-top:-8px"></span></p></li>');
								}
							});
								}
								$('.salesAll').on('click',function(){
								if ($(".salesAll").is(':checked')) {
									$(".chksales").prop("checked", true);
									$('[name="selectSales_allsale"]').prop("checked", true);
									$('[name="selectSales_allsale"]').closest('li').attr("class", "current");
									$('.chksales').closest('li').attr("class", "current");
								} else {
									$(".chksales").prop("checked", false);
									$('[name="selectSales_allsale"]').prop("checked", false);
									$('[name="selectSales_allsale"]').closest('li').attr("class", "");
									$('.chksales').closest('li').attr("class", "");
								}
					    	});
							$('.chksales').on('click',function(){
								if ( $(this).prop('checked')) {
									$(this).closest('li').attr("class", "current");
							    } else {
									$(this).closest('li').attr("class", "");
							    }
								if( $(".chksales:checked").length == $(".chksales").length){
									$(".salesAll").prop("checked", true);
								}else{
									$(".salesAll").prop("checked", false);
								}
								for(var index = 0 ; index < json[0].length;index++){
									if( $('[name="chkname'+index+'"]:checked').length == $('[name="chkname'+index+'"]').length){
									$(".selectSales_all"+index).prop("checked", true);
									$('.selectSales_all'+index).closest('li').attr("class", "current");
								}else{
									$(".selectSales_all"+index).prop("checked", false);
									$('.selectSales_all'+index).closest('li').attr("class", "");
								}
								}
							});	
												
				        },
						error: function() {
							alert("เกิดปัญหาในการโหลดข้อมูลกรุณารีเฟรชหน้าเว็บอีกครั้ง");
							$("#loading").hide();
						}
				    });
			//showReport('1');
			$("#loading").hide();
			//$('#showGraph').hide();
			$('#byIndustry').on('click',function(){
					$('#byIndustry').attr("class", "btn btn-success btn-large");
					$('#byMinistry').attr("class", "btn btn-gray btn-large");					
					$('#sortbyIndustry').css("display", "block");					
					$('#sortbyMinistry').css("display", "none");

			});
			$('#byMinistry').on('click',function(){
					$('#byIndustry').attr("class", "btn btn-gray btn-large");
					$('#byMinistry').attr("class", "btn btn-success btn-large");					
					$('#sortbyIndustry').css("display", "none");					
					$('#sortbyMinistry').css("display", "block");

			});
			function showReport(showNo) {
				//$("#loading").show();				
			}
			
			function RenderPieChart(elementId, dataList) {
                new Highcharts.Chart({
                    chart: {
                        renderTo: elementId,
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false
                    }, 
                    credits:{
                    	enabled: false
                    },
                    exporting: {
			            enabled: false
			        },
                    title: {
                        text: "Progress "+slcYear
                    },
                    tooltip: {
                        formatter: function () {
                            return '<b>' + this.point.name + '</b> : ' + this.percentage.nformat() + ' %<br>click to detail' ;
                        }
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                color: '#000000',
                                connectorColor: '#000000',
                                formatter: function () {
                                    return '<b>' + this.point.name + '</b> : ' + this.point.y.nformat() ;
                                }
                            }
                        },
                        series: {                        	
							 events: {
			                    click: function(event) {
									document.getElementById('focusShow').scrollIntoView();
									selectNo = event.point.name;
									showdetail1(event.point.name,event.point.y);
			                    }
			                }
                        }
                    },
                    series: [{
                        type: 'pie',
                        name: 'Progress',
                        data: dataList
                    }],
                    
                    colors: [
					   '#749DD1', 
					   '#D67B76', 
					   '#C5DC96', 
					   '#AE9BC8', 
					   '#66BCD5', 
					   '#F19F55', 
					   '#84B5EF', 
					   '#8AB9EF', 
					   '#F89995', 
					   '#D8F4A0', 
					   '#C5B1E6', 
					   '#94EAFB', 
					   '#FEC283', 
					   '#CADDFB', 
					   '#FECCCD', 
					   '#EAFBCE'
					]
                });
				$("#loading").hide();
            }
			function RanderBarChart(elementId,dataList){
				$('#'+elementId).highcharts({

			        chart: {
			            type: 'column'
			        },			         
                    credits:{
                    	enabled: false
                    },
                    exporting: {
			            enabled: false
			        },
                    title: {
                        text: "Progress "+slcYear
                    },
			        xAxis: {
			            categories: ['100', '90', '80', '70', '60', '50', '40' , '30' , '20' , '10', '0','P', 'v']  
			        },
					tooltip: {
                        formatter: function () {
                            return '<b>Income</b> : ' + this.y.nformat() + '<br>click to detail' ;
                        }
                    },
			        plotOptions: {
			            series: {
			                allowPointSelect: true,
			                cursor: 'pointer',
			                point: {
			                    events: {
			                        click: function() {
										document.getElementById('focusShow').scrollIntoView();
										selectNo = this.category;
										showdetail1(this.category,this.y);				
			                        }
			                    }
			                }
			            }
			        },

			        series: [{
			        	data: dataList      
			            ,name:"Income "       
			        }]
			    });
				$("#loading").hide();
			}
								
			function showdetail1(){
				// $("#loading").show();
				$(".PO_year").text(slcYear);
				$(".PO_year1").text(slcYear1);
				$("#PO_saleDep").text(depMng);
				//console.log('progress_detail: ',slcYear+' '+saleid+' '+typeYear);
				$.ajax({ 
					type:"POST",
					dataType: "json",
					url: "AJAX/SV_report_yearly_PO.php",
					data: {
						year: slcYear,
						saleid: saleid,
						type: typeYear,
						option: slcOption
					},
					success: function(json){
						//console.log(json);
						getDetail1(json);
						showdetail3();
					},
				});			
			}

			function getDetail1(json) {
				$("#PO_Detail").css("display","block");
            	    console.log("showTable:",json);	
					//document.getElementById('numResult').innerHTML=json.length;
				$('#PO_tbldetail > tbody:last').empty();
				var SumQ1=0;
				var SumQ2=0;
				var SumQ3=0;
				var SumQ4=0;
				var SumTotal=0;
				$.each(json, function() {
	            	    	var newrow="";
	            	    	var Industry=this["Industry"];
	            	    	var Q1=this["Q1"];
	            	    	var Q2=this["Q2"];
	            	    	var Q3=this["Q3"];
	            	    	var Q4=this["Q4"];
	            	    	var Total=this["Total"];

	            	    	newrow=$('<tr>').css({'background-color':'white'});
	            	    	var cols="";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead14'>"+Industry+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Q1.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Q2.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Q3.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Q4.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Total.nformat()+"</p></td>";
	            	    	newrow.append(cols);	            	    	
	            	    	$('#PO_tbldetail').append(newrow);

							SumQ1=SumQ1+Q1;
							SumQ2=SumQ2+Q2;
							SumQ3=SumQ3+Q3;
							SumQ4=SumQ4+Q4;
							SumTotal=SumTotal+Total;
	            	    });	

	            	    	var colsum="";
	            	    	var rowsum=$('<tr>').css({'background':'black','font-size':'31.5px;','color':'white'});
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>Net Total</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumQ1.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumQ2.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumQ3.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumQ4.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumTotal.nformat()+"</td>";
	            	    	rowsum.append(colsum);	            	    	
	            	    	$('#PO_tbldetail').append(rowsum);
				//$("#loading").hide();
           	}
           	function showdetail2(){
				//$("#loading").show();
				$(".PO_year").text(slcYear);
				$(".PO_year1").text(slcYear1);
				$("#PO_saleDep").text(depMng);
				//console.log('progress_detail: ',slcYear+' '+saleid+' '+typeYear);
				$.ajax({ 
					type:"POST",
					dataType: "json",
					url: "AJAX/SV_report_yearly_Book.php",
					data: {
						year: slcYear,
						saleid: saleid,
						type: typeYear,
						option: slcOption
					},
					success: function(json){
						//console.log(json);
						getDetail2(json);
						showdetail4();
					}
				});			
			}

			function getDetail2(json) {
				//$("#Potential_Detail").css("display","block");
            	    //console.log("showTable:",json);	
					//document.getElementById('numResult').innerHTML=json.length;
				$('#PO2_tbldetail > tbody:last').empty();
				var SumQ1=0;
				var SumQ2=0;
				var SumQ3=0;
				var SumQ4=0;
				var SumBook=0;
				var SumForward=0;
				var SumTotal=0;
				$.each(json, function() {
	            	    	var newrow="";
	            	    	var Industry=this["Industry"];
	            	    	var Q1=this["Q1"];
	            	    	var Q2=this["Q2"];
	            	    	var Q3=this["Q3"];
	            	    	var Q4=this["Q4"];
	            	    	var Book=this["Book"];
	            	    	var Forward=this["Forward"];
	            	    	var Total=this["Total"];

	            	    	newrow=$('<tr>').css({'background-color':'white'});
	            	    	var cols="";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead14'>"+Industry+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Q1.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Q2.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Q3.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Q4.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Book.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Forward.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Total.nformat()+"</p></td>";
	            	    	newrow.append(cols);	            	    	
	            	    	$('#PO2_tbldetail').append(newrow);

							SumQ1=SumQ1+Q1;
							SumQ2=SumQ2+Q2;
							SumQ3=SumQ3+Q3;
							SumQ4=SumQ4+Q4;
							SumBook=SumBook+Book;
							SumForward=SumForward+Forward;
							SumTotal=SumTotal+Total;
	            	    });	

						GrandTotalQ1=GrandTotalQ1+SumQ1;
						GrandTotalQ2=GrandTotalQ2+SumQ2;
						GrandTotalQ3=GrandTotalQ3+SumQ3;
						GrandTotalQ4=GrandTotalQ4+SumQ4;
						GrandTotalBook=GrandTotalBook+SumTotal;
						GrandTotalForward=SumForward;
						GrandTotalSum=GrandTotalSum+GrandTotalBook+SumForward;
	            	    	var colsum="";
	            	    	var rowsum=$('<tr>').css({'background':'black','font-size':'31.5px;','color':'white'});
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>Net Total</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumQ1.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumQ2.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumQ3.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumQ4.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumBook.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumForward.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumTotal.nformat()+"</td>";
	            	    	rowsum.append(colsum);	            	    	
	            	    	$('#PO2_tbldetail').append(rowsum);

	            	    	var colGrand="";
	            	    	var rowGrand=$('<tr>').css({'background':'black','font-size':'31.5px;','color':'white'});
	            	    	colGrand+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>Grand Total</td>";
	            	    	colGrand+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+GrandTotalQ1.nformat()+"</td>";
	            	    	colGrand+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+GrandTotalQ2.nformat()+"</td>";
	            	    	colGrand+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+GrandTotalQ3.nformat()+"</td>";
	            	    	colGrand+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+GrandTotalQ4.nformat()+"</td>";
	            	    	colGrand+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+GrandTotalBook.nformat()+"</td>";
	            	    	colGrand+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+GrandTotalForward.nformat()+"</td>";
	            	    	colGrand+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+GrandTotalSum.nformat()+"</td>";
	            	    	rowGrand.append(colGrand);	            	    	
	            	    	$('#PO2_tbldetail').append(rowGrand);
				//$("#loading").hide();
           	}
           	function showdetail3(){
           		var RYear=slcYear-1;
				//$("#loading").show();
				$(".PO_year").text(slcYear);
				$(".PO_year1").text(slcYear1);
				$("#PO_saleDep").text(depMng);
				//console.log('progress_detail: ',slcYear+' '+saleid+' '+typeYear);
				$.ajax({ 
					type:"POST",
					dataType: "json",
					url: "AJAX/SV_report_yearly_BalanceForward.php",
					data: {
						year: slcYear,
						saleid: saleid,
						type: typeYear,
						Ryear: RYear,
						option: slcOption
					},
					success: function(json){
						console.log("SV_report_yearly_BalanceForward ",json);
						getDetail3(json);
						showdetail2();
					},
					error:function(e){
						console.log("Error : ",e);
					}
				});			
			}

			function getDetail3(json) {
				//$("#Potential_Detail").css("display","block");
            	    //console.log("showTable:",json);	
					//document.getElementById('numResult').innerHTML=json.length;
				$('#PO3_tbldetail > tbody:last').empty();
				var SumQ1=0;
				var SumQ2=0;
				var SumQ3=0;
				var SumQ4=0;
				var SumBook=0;
				var SumForward=0;
				var SumTotal=0;
				$.each(json, function() {
	            	    	var newrow="";
	            	    	var Industry=this["Industry"];
	            	    	var Q1=this["Q1"];
	            	    	var Q2=this["Q2"];
	            	    	var Q3=this["Q3"];
	            	    	var Q4=this["Q4"];
	            	    	var Total=this["Total"];

	            	    	newrow=$('<tr>').css({'background-color':'white'});
	            	    	var cols="";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead14'>"+Industry+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Q1.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Q2.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Q3.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Q4.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Total.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'></p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'></p></td>";
	            	    	newrow.append(cols);	            	    	
	            	    	$('#PO3_tbldetail').append(newrow);

							SumQ1=SumQ1+Q1;
							SumQ2=SumQ2+Q2;
							SumQ3=SumQ3+Q3;
							SumQ4=SumQ4+Q4;
							SumTotal=SumTotal+Total;
	            	    });	

	            	    	var colsum="";
	            	    	var rowsum=$('<tr>').css({'background':'black','font-size':'31.5px;','color':'white'});
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>Net Total</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumQ1.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumQ2.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumQ3.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumQ4.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumTotal.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'><p style='line-height: 1!important;' class='lead16'></p></td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'><p style='line-height: 1!important;' class='lead16'></p></td>";
	            	    	rowsum.append(colsum);	            	    	
	            	    	$('#PO3_tbldetail').append(rowsum);

						GrandTotalQ1=SumQ1;
						GrandTotalQ2=SumQ2;
						GrandTotalQ3=SumQ3;
						GrandTotalQ4=SumQ4;
						GrandTotalBook=SumTotal;
						GrandTotalForward=0;
						GrandTotalSum=SumTotal;
				//$("#loading").hide();
           	}
           	function showdetail4(){
           		var RYear=slcYear-1;
				//$("#loading").show();
				$(".PO_year").text(slcYear);
				$(".PO_year1").text(slcYear1);
				$("#PO_saleDep").text(depMng);
				//console.log('progress_detail: ',slcYear+' '+saleid+' '+typeYear);
				$.ajax({ 
					type:"POST",
					dataType: "json",
					url: "AJAX/SOAP_GetPO_GISC_Industry.php",
					data: {
						year: slcYear
					},
					success: function(json){
						//console.log("showdetail4 ",json);
						getDetail4(json.Item.PO2Inc_Industry);
						showdetail5();
						//showdetail2();
					},
					error:function(e){
						console.log("Error showdetail4 : ",e);
					}
				});			
			}

			function getDetail4(json) {
				//$("#Potential_Detail").css("display","block");
            	//console.log("showTable:",json);	
					//document.getElementById('numResult').innerHTML=json.length;
				$('#PO4_tbldetail > tbody:last').empty();
				var SumQ1=0;
				var SumQ2=0;
				var SumQ3=0;
				var SumQ4=0;
				var SumBook=0;
				var SumForward=0;
				var SumTotal=0;
				$.each(json, function() {
	            	    	var newrow="";
	            	    	var Industry=this["industry"];
	            	    	var Q1=parseFloat(this["Q1"]);
	            	    	var Q2=parseFloat(this["Q2"]);
	            	    	var Q3=parseFloat(this["Q3"]);
	            	    	var Q4=parseFloat(this["Q4"]);
	            	    	var Book=parseFloat(this["Book"]);
	            	    	var Forward=parseFloat(this["Forward"]);
	            	    	var Total=Book+Forward;

	            	    	newrow=$('<tr>').css({'background-color':'white'});
	            	    	var cols="";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead14'>"+Industry+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Q1.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Q2.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Q3.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Q4.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Book.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Forward.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Total.nformat()+"</p></td>";
	            	    	newrow.append(cols);	            	    	
	            	    	$('#PO4_tbldetail').append(newrow);

							SumQ1=SumQ1+Q1;
							SumQ2=SumQ2+Q2;
							SumQ3=SumQ3+Q3;
							SumQ4=SumQ4+Q4;
							SumBook=SumBook+Book;
							SumForward=SumForward+Forward;
							SumTotal=SumBook+SumForward;
	            	    });	

	            	    	var colsum="";
	            	    	var rowsum=$('<tr>').css({'background':'black','font-size':'31.5px;','color':'white'});
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>Net Total</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumQ1.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumQ2.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumQ3.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumQ4.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumBook.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumForward.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumTotal.nformat()+"</td>";
	            	    	rowsum.append(colsum);	            	    	
	            	    	$('#PO4_tbldetail').append(rowsum);
				//$("#loading").hide();
           	}
           	function showdetail5(){
           		var RYear=slcYear-1;
				//$("#loading").show();
				$(".PO_year").text(slcYear);
				$(".PO_year1").text(slcYear1);
				$("#PO_saleDep").text(depMng);
				//console.log('progress_detail: ',slcYear+' '+saleid+' '+typeYear);
				$.ajax({ 
					type:"POST",
					dataType: "json",
					url: "AJAX/SOAP_GetPO_GISC_Industry_ESRIT.php",
					data: {
						year: slcYear
					},
					success: function(json){
						//console.log("showdetail5 ",json);
						getDetail5(json.Item.PO2Inc_Industry);
						showdetail1_Ministry();
						//showdetail2();
					},
					error:function(e){
						console.log("Error showdetail5 : ",e);
					}
				});			
			}

			function getDetail5(json) {
				//$("#Potential_Detail").css("display","block");
            	//console.log("showTable:",json);	
					//document.getElementById('numResult').innerHTML=json.length;
				$('#PO5_tbldetail > tbody:last').empty();
				var SumQ1=0;
				var SumQ2=0;
				var SumQ3=0;
				var SumQ4=0;
				var SumBook=0;
				var SumForward=0;
				var SumTotal=0;
				$.each(json, function() {
	            	    	var newrow="";
	            	    	var Industry=this["industry"];
	            	    	var Q1=parseFloat(this["Q1"]);
	            	    	var Q2=parseFloat(this["Q2"]);
	            	    	var Q3=parseFloat(this["Q3"]);
	            	    	var Q4=parseFloat(this["Q4"]);
	            	    	var Book=parseFloat(this["Book"]);
	            	    	var Forward=parseFloat(this["Forward"]);
	            	    	var Total=Book+Forward;

	            	    	newrow=$('<tr>').css({'background-color':'white'});
	            	    	var cols="";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead14'>"+Industry+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Q1.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Q2.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Q3.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Q4.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Book.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Forward.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Total.nformat()+"</p></td>";
	            	    	newrow.append(cols);	            	    	
	            	    	$('#PO5_tbldetail').append(newrow);

							SumQ1=SumQ1+Q1;
							SumQ2=SumQ2+Q2;
							SumQ3=SumQ3+Q3;
							SumQ4=SumQ4+Q4;
							SumBook=SumBook+Book;
							SumForward=SumForward+Forward;
							SumTotal=SumBook+SumForward;
	            	    });	

	            	    	var colsum="";
	            	    	var rowsum=$('<tr>').css({'background':'black','font-size':'31.5px;','color':'white'});
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>Net Total</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumQ1.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumQ2.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumQ3.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumQ4.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumBook.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumForward.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumTotal.nformat()+"</td>";
	            	    	rowsum.append(colsum);	            	    	
	            	    	$('#PO5_tbldetail').append(rowsum);
				//$("#loading").hide();
           	}
			
			function showdetail4_json1(){
				$.ajax({ 
					type:"POST",
					dataType: "text",
					url: "AJAX/SOAP_GetPO_GISC_Industry_Json.php",
					data: {
						year: slcYear
					},
					success: function(json){
						console.log("showdetail4 Json1",json);
						document.frmexport.Json1.value=json;
						showdetail5_json2();
					},
					error:function(e){
						console.log("Error showdetail4 Json1: ",e);
						$("#loading").hide();
					}
				});			
			}

			function showdetail5_json2(){
				$.ajax({ 
					type:"POST",
					dataType: "text",
					url: "AJAX/SOAP_GetPO_GISC_Industry_ESRIT_Json.php",
					data: {
						year: slcYear
					},
					success: function(json){
						console.log("showdetail5 Json1",json);
						document.frmexport.Json2.value=json;

						//document.frmexport.Json1.value=JSON.stringify(json);
						
						document.frmexport.submit();
						$("#loading").hide();
						//showdetail2();
					},
					error:function(e){
						console.log("Error showdetail5 Json1: ",e);
						$("#loading").hide();
					}
				});			
			}
			//Ministry
			function showdetail1_Ministry(){
				//$("#loading").show();
				$(".PO_year").text(slcYear);
				$(".PO_year1").text(slcYear1);
				$("#PO_saleDep").text(depMng);
				console.log('SV_report_yearly_PO_Ministry: ',slcYear+' '+saleid+' '+typeYear);
				$.ajax({ 
					type:"POST",
					dataType: "json",
					url: "AJAX/SV_report_yearly_PO_Ministry.php",
					data: {
						year: slcYear,
						saleid: saleid,
						type: typeYear,
						option: slcOption
					},
					success: function(json){
						console.log("SV_report_yearly_PO_Ministry",json);
						getDetail1_Ministry(json);
						showdetail3_Ministry();
					},
					error:function(e)
					{
						console.log("error :",e);						
					}
				});			
			}

			function getDetail1_Ministry(json) {
            	    //console.log("showTable:",json);	
					//document.getElementById('numResult').innerHTML=json.length;
				$('#PO_tbldetail_Ministry > tbody:last').empty();
				var SumQ1=0;
				var SumQ2=0;
				var SumQ3=0;
				var SumQ4=0;
				var SumTotal=0;
				$.each(json, function() {
	            	    	var newrow="";
	            	    	var Ministry=this["Ministry"];
	            	    	var Q1=this["Q1"];
	            	    	var Q2=this["Q2"];
	            	    	var Q3=this["Q3"];
	            	    	var Q4=this["Q4"];
	            	    	var Total=this["Total"];

	            	    	newrow=$('<tr>').css({'background-color':'white'});
	            	    	var cols="";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead14'>"+Ministry+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Q1.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Q2.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Q3.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Q4.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Total.nformat()+"</p></td>";
	            	    	newrow.append(cols);	            	    	
	            	    	$('#PO_tbldetail_Ministry').append(newrow);

							SumQ1=SumQ1+Q1;
							SumQ2=SumQ2+Q2;
							SumQ3=SumQ3+Q3;
							SumQ4=SumQ4+Q4;
							SumTotal=SumTotal+Total;
	            	    });	

	            	    	var colsum="";
	            	    	var rowsum=$('<tr>').css({'background':'black','font-size':'31.5px;','color':'white'});
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>Net Total</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumQ1.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumQ2.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumQ3.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumQ4.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumTotal.nformat()+"</td>";
	            	    	rowsum.append(colsum);	            	    	
	            	    	$('#PO_tbldetail_Ministry').append(rowsum);
				//$("#loading").hide();
           	}


           	function showdetail2_Ministry(){
				//$("#loading").show();
				$(".PO_year").text(slcYear);
				$(".PO_year1").text(slcYear1);
				$("#PO_saleDep").text(depMng);
				console.log('progress_detail: ',slcYear+' '+saleid+' '+typeYear);
				$.ajax({ 
					type:"POST",
					dataType: "json",
					url: "AJAX/SV_report_yearly_Book_Ministry.php",
					data: {
						year: slcYear,
						saleid: saleid,
						type: typeYear,
						option: slcOption
					},
					success: function(json){
						console.log(json);
						getDetail2_Ministry(json);
						showdetail4_Ministry();
					}
				});			
			}

			function getDetail2_Ministry(json) {
				//$("#Potential_Detail").css("display","block");
            	    //console.log("showTable:",json);	
					//document.getElementById('numResult').innerHTML=json.length;
				$('#PO2_tbldetail_Ministry > tbody:last').empty();
				var SumQ1=0;
				var SumQ2=0;
				var SumQ3=0;
				var SumQ4=0;
				var SumBook=0;
				var SumForward=0;
				var SumTotal=0;
				$.each(json, function() {
	            	    	var newrow="";
	            	    	var Ministry=this["Ministry"];
	            	    	var Q1=this["Q1"];
	            	    	var Q2=this["Q2"];
	            	    	var Q3=this["Q3"];
	            	    	var Q4=this["Q4"];
	            	    	var Book=this["Book"];
	            	    	var Forward=this["Forward"];
	            	    	var Total=this["Total"];

	            	    	newrow=$('<tr>').css({'background-color':'white'});
	            	    	var cols="";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead14'>"+Ministry+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Q1.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Q2.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Q3.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Q4.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Book.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Forward.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Total.nformat()+"</p></td>";
	            	    	newrow.append(cols);	            	    	
	            	    	$('#PO2_tbldetail_Ministry').append(newrow);

							SumQ1=SumQ1+Q1;
							SumQ2=SumQ2+Q2;
							SumQ3=SumQ3+Q3;
							SumQ4=SumQ4+Q4;
							SumBook=SumBook+Book;
							SumForward=SumForward+Forward;
							SumTotal=SumTotal+Total;
	            	    });	

						GrandTotalQ1M=GrandTotalQ1M+SumQ1;
						GrandTotalQ2M=GrandTotalQ2M+SumQ2;
						GrandTotalQ3M=GrandTotalQ3M+SumQ3;
						GrandTotalQ4M=GrandTotalQ4M+SumQ4;
						GrandTotalBookM=GrandTotalBookM+SumTotal;
						GrandTotalForwardM=SumForward;
						GrandTotalSumM=GrandTotalSumM+GrandTotalBookM+SumForward;
	            	    	var colsum="";
	            	    	var rowsum=$('<tr>').css({'background':'black','font-size':'31.5px;','color':'white'});
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>Net Total</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumQ1.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumQ2.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumQ3.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumQ4.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumBook.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumForward.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumTotal.nformat()+"</td>";
	            	    	rowsum.append(colsum);	            	    	
	            	    	$('#PO2_tbldetail_Ministry').append(rowsum);

	            	    	var colGrand="";
	            	    	var rowGrand=$('<tr>').css({'background':'black','font-size':'31.5px;','color':'white'});
	            	    	colGrand+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>Grand Total</td>";
	            	    	colGrand+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+GrandTotalQ1M.nformat()+"</td>";
	            	    	colGrand+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+GrandTotalQ2M.nformat()+"</td>";
	            	    	colGrand+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+GrandTotalQ3M.nformat()+"</td>";
	            	    	colGrand+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+GrandTotalQ4M.nformat()+"</td>";
	            	    	colGrand+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+GrandTotalBookM.nformat()+"</td>";
	            	    	colGrand+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+GrandTotalForwardM.nformat()+"</td>";
	            	    	colGrand+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+GrandTotalSumM.nformat()+"</td>";
	            	    	rowGrand.append(colGrand);	            	    	
	            	    	$('#PO2_tbldetail_Ministry').append(rowGrand);
				//$("#loading").hide();
           	}
           	function showdetail3_Ministry(){
           		var RYear=slcYear-1;
				//$("#loading").show();
				$(".PO_year").text(slcYear);
				$(".PO_year1").text(slcYear1);
				$("#PO_saleDep").text(depMng);
				console.log('progress_detail: ',slcYear+' '+saleid+' '+typeYear);
				$.ajax({ 
					type:"POST",
					dataType: "json",
					url: "AJAX/SV_report_yearly_BalanceForward_Ministry.php",
					data: {
						year: slcYear,
						saleid: saleid,
						type: typeYear,
						Ryear: RYear,
						option: slcOption
					},
					success: function(json){
						console.log("showdetail3M ",json);
						getDetail3_Ministry(json);
						showdetail2_Ministry();
					},
					error:function(e){
						console.log("Error : ",e);
					}
				});			
			}

			function getDetail3_Ministry(json) {
				//$("#Potential_Detail").css("display","block");
            	    //console.log("showTable:",json);	
					//document.getElementById('numResult').innerHTML=json.length;
				$('#PO3_tbldetail_Ministry > tbody:last').empty();
				var SumQ1=0;
				var SumQ2=0;
				var SumQ3=0;
				var SumQ4=0;
				var SumBook=0;
				var SumForward=0;
				var SumTotal=0;
				$.each(json, function() {
	            	    	var newrow="";
	            	    	var Ministry=this["Ministry"];
	            	    	var Q1=this["Q1"];
	            	    	var Q2=this["Q2"];
	            	    	var Q3=this["Q3"];
	            	    	var Q4=this["Q4"];
	            	    	var Total=this["Total"];

	            	    	newrow=$('<tr>').css({'background-color':'white'});
	            	    	var cols="";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead14'>"+Ministry+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Q1.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Q2.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Q3.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Q4.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Total.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'></p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'></p></td>";
	            	    	newrow.append(cols);	            	    	
	            	    	$('#PO3_tbldetail_Ministry').append(newrow);

							SumQ1=SumQ1+Q1;
							SumQ2=SumQ2+Q2;
							SumQ3=SumQ3+Q3;
							SumQ4=SumQ4+Q4;
							SumTotal=SumTotal+Total;
	            	    });	

	            	    	var colsum="";
	            	    	var rowsum=$('<tr>').css({'background':'black','font-size':'31.5px;','color':'white'});
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>Net Total</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumQ1.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumQ2.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumQ3.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumQ4.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumTotal.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;'><p style='line-height: 1!important;' class='lead16'></p></td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;'><p style='line-height: 1!important;' class='lead16'></p></td>";
	            	    	rowsum.append(colsum);	            	    	
	            	    	$('#PO3_tbldetail_Ministry').append(rowsum);

						GrandTotalQ1M=SumQ1;
						GrandTotalQ2M=SumQ2;
						GrandTotalQ3M=SumQ3;
						GrandTotalQ4M=SumQ4;
						GrandTotalBookM=SumTotal;
						GrandTotalForwardM=0;
						GrandTotalSumM=SumTotal;
				//$("#loading").hide();
           	}

           	function showdetail4_Ministry(){
           		var RYear=slcYear-1;
				//$("#loading").show();
				$(".PO_year").text(slcYear);
				$(".PO_year1").text(slcYear1);
				$("#PO_saleDep").text(depMng);
				console.log('progress_detail: ',slcYear+' '+saleid+' '+typeYear);
				$.ajax({ 
					type:"POST",
					dataType: "json",
					url: "AJAX/SOAP_GetPO_GISC_Ministry.php",
					data: {
						year: slcYear,
						saleid: saleid
					},
					success: function(json){
						console.log("showdetail4 ",json);
						// getDetail4_Ministry(json.Item.PO2Inc_Ministry);
						getDetail4_Ministry(json);
						showdetail5_Ministry();
						//showdetail2();
					},
					error:function(e){
						console.log("Error showdetail4 : ",e);
					}
				});			
			}

			function getDetail4_Ministry(json) {
				//$("#Potential_Detail").css("display","block");
            	//console.log("showTable:",json);	
					//document.getElementById('numResult').innerHTML=json.length;
				$('#PO4_tbldetail_Ministry > tbody:last').empty();
				var SumQ1=0;
				var SumQ2=0;
				var SumQ3=0;
				var SumQ4=0;
				var SumBook=0;
				var SumForward=0;
				var SumTotal=0;
				$.each(json, function() {
	            	    	var newrow="";
	            	    	var Ministry=this["ministry"];
	            	    	var Q1=parseFloat(this["Q1"]);
	            	    	var Q2=parseFloat(this["Q2"]);
	            	    	var Q3=parseFloat(this["Q3"]);
	            	    	var Q4=parseFloat(this["Q4"]);
	            	    	var Book=parseFloat(this["Book"]);
	            	    	var Forward=parseFloat(this["Forward"]);
	            	    	var Total=Book+Forward;

	            	    	newrow=$('<tr>').css({'background-color':'white'});
	            	    	var cols="";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead14'>"+Ministry+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Q1.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Q2.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Q3.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Q4.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Book.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Forward.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Total.nformat()+"</p></td>";
	            	    	newrow.append(cols);	            	    	
	            	    	$('#PO4_tbldetail_Ministry').append(newrow);

							SumQ1=SumQ1+Q1;
							SumQ2=SumQ2+Q2;
							SumQ3=SumQ3+Q3;
							SumQ4=SumQ4+Q4;
							SumBook=SumBook+Book;
							SumForward=SumForward+Forward;
							SumTotal=SumBook+SumForward;
	            	    });	

	            	    	var colsum="";
	            	    	var rowsum=$('<tr>').css({'background':'black','font-size':'31.5px;','color':'white'});
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>Net Total</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumQ1.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumQ2.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumQ3.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumQ4.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumBook.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumForward.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumTotal.nformat()+"</td>";
	            	    	rowsum.append(colsum);	            	    	
	            	    	$('#PO4_tbldetail_Ministry').append(rowsum);
				//$("#loading").hide();
           	}
           	function showdetail5_Ministry(){
           		var RYear=slcYear-1;
				//$("#loading").show();
				$(".PO_year").text(slcYear);
				$(".PO_year1").text(slcYear1);
				$("#PO_saleDep").text(depMng);
				console.log('progress_detail: ',slcYear+' '+saleid+' '+typeYear);
				$.ajax({ 
					type:"POST",
					dataType: "json",
					url: "AJAX/SOAP_GetPO_GISC_Ministry_ESRIT.php",
					data: {
						year: slcYear,
						saleid: saleid
					},
					success: function(json){
						console.log("showdetail5 ",json);
						getDetail5_Ministry(json);
						$("#loading").hide();
						//showdetail2();
					},
					error:function(e){
						console.log("Error showdetail5 : ",e);
						$("#loading").hide();
					}
				});			
			}

			function getDetail5_Ministry(json) {
				//$("#Potential_Detail").css("display","block");
            	//console.log("showTable:",json);	
					//document.getElementById('numResult').innerHTML=json.length;
				$('#PO5_tbldetail_Ministry > tbody:last').empty();
				var SumQ1=0;
				var SumQ2=0;
				var SumQ3=0;
				var SumQ4=0;
				var SumBook=0;
				var SumForward=0;
				var SumTotal=0;
				$.each(json, function() {
	            	    	var newrow="";
	            	    	var Ministry=this["ministry"];
	            	    	var Q1=parseFloat(this["Q1"]);
	            	    	var Q2=parseFloat(this["Q2"]);
	            	    	var Q3=parseFloat(this["Q3"]);
	            	    	var Q4=parseFloat(this["Q4"]);
	            	    	var Book=parseFloat(this["Book"]);
	            	    	var Forward=parseFloat(this["Forward"]);
	            	    	var Total=Book+Forward;

	            	    	newrow=$('<tr>').css({'background-color':'white'});
	            	    	var cols="";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead14'>"+Ministry+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Q1.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Q2.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Q3.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Q4.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Book.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Forward.nformat()+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p style='line-height: 1!important;' class='lead16'>"+Total.nformat()+"</p></td>";
	            	    	newrow.append(cols);	            	    	
	            	    	$('#PO5_tbldetail_Ministry').append(newrow);

							SumQ1=SumQ1+Q1;
							SumQ2=SumQ2+Q2;
							SumQ3=SumQ3+Q3;
							SumQ4=SumQ4+Q4;
							SumBook=SumBook+Book;
							SumForward=SumForward+Forward;
							SumTotal=SumBook+SumForward;
	            	    });	

	            	    	var colsum="";
	            	    	var rowsum=$('<tr>').css({'background':'black','font-size':'31.5px;','color':'white'});
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>Net Total</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumQ1.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumQ2.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumQ3.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumQ4.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumBook.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumForward.nformat()+"</td>";
	            	    	colsum+="<td style='text-align:right;background: none repeat scroll 0 0 #333333;color: #FFFFFF;'>"+SumTotal.nformat()+"</td>";
	            	    	rowsum.append(colsum);	            	    	
	            	    	$('#PO5_tbldetail_Ministry').append(rowsum);
				//$("#loading").hide();
           	}
			
			function showdetail4_Ministry_json1(){
				$.ajax({ 
					type:"POST",
					dataType: "json",
					url: "AJAX/SOAP_GetPO_GISC_Ministry_Json.php",
					data: {
						year: slcYear,
						saleid: saleid
					},
					success: function(json){
						
						console.log("showdetail4 Json1",json);
						// var jsonObj = [];
						// $.each(json, function() {
	     //        	    	var item = {}
	     //        	    	item ["ministry"]=this["ministry"];
	     //        	    	item ["Q1"]=this["Q1"];
	     //        	    	item ["Q2"]=this["Q2"];
	     //        	    	item ["Q3"]=this["Q3"];
	     //        	    	item ["Q4"]=this["Q4"];
	     //        	    	item ["Book"]=this["Book"];
	     //        	    	item ["Forward"]=this["Forward"];
	     //        	    	item ["Total"]=this["Total"];
	     //        	    	jsonObj.push(item);
	     //        	    });
						document.frmexport_ministry.Json1M.value=JSON.stringify(json);
						showdetail5_Ministry_json2();
					},
					error:function(e){
						console.log("Error showdetail4 Json1: ",e);
						$("#loading").hide();
					}
				});			
			}

			function showdetail5_Ministry_json2(){
				$.ajax({ 
					type:"POST",
					dataType: "json",
					url: "AJAX/SOAP_GetPO_GISC_Ministry_ESRIT_Json.php",
					data: {
						year: slcYear,
						saleid: saleid
					},
					success: function(json){
						console.log("showdetail5 Json1",json);
						document.frmexport_ministry.Json2M.value=JSON.stringify(json);

						//document.frmexport.Json1.value=JSON.stringify(json);
						
						document.frmexport_ministry.submit();
						$("#loading").hide();
						//showdetail2();
					},
					error:function(e){
						console.log("Error showdetail5 Json1: ",e);
						$("#loading").hide();
					}
				});			
			}
			$('#btnSwitch').on('click',function(){
				showReport(selectNo);
				if($('#containerBar').is(':hidden')) {
					$("#containerBar").show();
					$("#containerPie").hide();
				}else{
					$("#containerBar").hide();
					$("#containerPie").show();
				}
			});
			$('#btnReport').on('click',function(){

				$("#loading").show();
				$('#showError').addClass("hide");
				$('#showWarning').addClass("hide");
						checked = false;
						if ($(".chksales:checked").length == 0) {
							checked = false;
						}
						else
						{
							checked = true;
						}
						var chkid = '';
						$(".chksales").each(function () {
							if ( $(this).prop('checked')) {
								if(chkid=='')
								{
									chkid= $(this).val();
								}
								else
								{
									chkid = chkid+","+$(this).val();
								}
							}
						});
			
						slcYear=$("#slc_year_sign").val();

						saleid=chkid;
						
						if ( $('.chkSpecial').prop('checked')) {
							slcOption="0,1";
						}else{
							slcOption="0";
						}

						if (checked) {
							showdetail1();
							// showdetail3();
							// showdetail4();
							// showdetail5();
							// showdetail1_Ministry();
							// showdetail3_Ministry();
							// showdetail4_Ministry();
							// showdetail5_Ministry();
						} else {
							alert("กรุณาเลือก Sales อย่างน้อย 1 รายการ");
						}
						//$("#loading").hide();

			});
			$('#btnExport').on('click',function(){ 
				//document.frmexport.submit();
				$("#loading").show();
				checked = false;
						if ($(".chksales:checked").length == 0) {
							checked = false;
						}
						else
						{
							checked = true;
						}
						var chkid = '';
						$(".chksales").each(function () {
							if ( $(this).prop('checked')) {
								if(chkid=='')
								{
									chkid= $(this).val();
								}
								else
								{
									chkid = chkid+","+$(this).val();
								}
							}
						});
			
						slcYear=$("#slc_year_sign").val();

						saleid=chkid;
						
						if ( $('.chkSpecial').prop('checked')) {
							slcOption="0,1";
						}else{
							slcOption="0";
						}

						document.frmexport.saleid.value=chkid;
						document.frmexport.option.value=slcOption;
						document.frmexport.year.value=slcYear;
						document.frmexport.Fyear.value=parseInt(slcYear)+1;
						document.frmexport.Ryear.value=parseInt(slcYear)-1;

						if (checked) {
							showdetail4_json1();
							//if(result=="success")
							//{
							//document.frmexport.submit();
							//}
							//else
							//{
							//	console.log('error btnExport');
							//}
						} else {
							alert("กรุณาเลือก Sales อย่างน้อย 1 รายการ");
							$("#loading").hide();
						}
			});

			$('#btnExport_Ministry').on('click',function(){ 
				//document.frmexport.submit();
				$("#loading").show();
				checked = false;
						if ($(".chksales:checked").length == 0) {
							checked = false;
						}
						else
						{
							checked = true;
						}
						var chkid = '';
						$(".chksales").each(function () {
							if ( $(this).prop('checked')) {
								if(chkid=='')
								{
									chkid= $(this).val();
								}
								else
								{
									chkid = chkid+","+$(this).val();
								}
							}
						});
			
						slcYear=$("#slc_year_sign").val();

						saleid=chkid;
						
						if ( $('.chkSpecial').prop('checked')) {
							slcOption="0,1";
						}else{
							slcOption="0";
						}

						document.frmexport_ministry.saleid.value=chkid;
						document.frmexport_ministry.option.value=slcOption;
						document.frmexport_ministry.year.value=slcYear;
						document.frmexport_ministry.Fyear.value=parseInt(slcYear)+1;
						document.frmexport_ministry.Ryear.value=parseInt(slcYear)-1;


						if (checked) {
							showdetail4_Ministry_json1();
							//document.frmexport_ministry.submit();
						} else {
							alert("กรุณาเลือก Sales อย่างน้อย 1 รายการ");
							$("#loading").hide();
						}
			});
		});
			
			function edit(id){
				window.location="newproject.php?id="+id+"&page=report_all";
			}
			function view(id){
				//window.location="previewforecast.php?id="+id+"&page=report_all";			
				window.open("previewforecastReport.php?id="+id);
			}
			function SelectSale(i) {
			if ($(".selectSales_all"+i).is(':checked')) {
									$('[name="chkname'+i+'"]').prop("checked", true);
									$('[name="chkname'+i+'"]').closest('li').attr("class", "current");
									$(".selectSales_all"+i).closest('li').attr("class", "current");
								} else {
									$('[name="chkname'+i+'"]').prop("checked", false);
									$('[name="chkname'+i+'"]').closest('li').attr("class", "");
									$(".selectSales_all"+i).closest('li').attr("class", "");
								}
								if( $(".chksales:checked").length == $(".chksales").length){
									$(".salesAll").prop("checked", true);
								}else{
									$(".salesAll").prop("checked", false);
								}
}
		</script>
  	</body>

<!-- Mirrored from bootstrapstyler.com/preview/_/flatdashboard/ by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 18 Oct 2013 07:00:04 GMT -->
</html>