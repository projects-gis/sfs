﻿<!DOCTYPE html>
<html lang="en">
<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
		<meta charset="utf-8">
		<title>SFS:::Postsales</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="Izuddin Helmi">

		<!-- Stylesheets -->
		<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
		<link href="js/library/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
		<link href="css/styler/style.css" rel="stylesheet">
		<link href="js/library/syntaxHighlighter/shCore.css" rel="stylesheet" type="text/css" />
		<link href="js/library/syntaxHighlighter/shThemeDefault.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="css/styler/fontello.css">
	    <link rel="stylesheet" href="css/styler/animation.css"><!--[if IE 7]>
	    <link rel="stylesheet" href="css/styler/fontello-ie7.css"><![endif]-->
		<link rel="stylesheet" href="css/main.css">
	    

		<!-- Custom Fonts -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300,500' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

		<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
		  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
<!-- Javascript -->
		<!-- JS:jquery-->
		<!--<script src="../../../../ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>-->
  		<script src="js/library/jquery/jquery.min.js"></script>
		<script src="js/library/jquery/jquery-ui.min.js"></script>

		<!-- JS:masonry-->
		<script src="js/library/masonry/jquery.masonry.min.js"></script>

		<!-- JS:flexslider-->
		<script src="js/library/flexslider/jquery.flexslider-min.js"></script>

		<!-- JS:tables-->
		<script src="js/library/dataTables/jquery.dataTables.min.js"></script>

		<!-- JS:calendar-->
		<script src="js/library/fullcalendar/fullcalendar.min.js"></script>
		<script src="js/library/fullcalendar/gcal.js"></script>

		<!-- JS:forms-->
		<script src="js/library/forms/jquery.hotkeys.js"></script>
		<script src="js/library/forms/bootstrap-wysiwyg.js"></script>
		<script src="js/library/forms/picker.js"></script>
		<script src="js/library/forms/picker.date.js"></script>
		<script src="js/library/forms/picker.time.js"></script>
		<script src="js/library/forms/jquery.bootstrap.wizard.min.js"></script>

		<!-- JS:charts-->
		<script src="js/library/charts/jquery.flot.js"></script>
		<script src="js/library/charts/jquery.flot.resize.js"></script>
		<script src="js/library/charts/jquery.flot.stack.js"></script>
		<script src="js/library/charts/jquery.flot.pie.min.js"></script>
		<script src="js/library/charts/jquery.sparkline.min.js"></script>
		<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="excanvas.min.js"></script><![endif]-->

		<!-- JS:syntaxHighlighter-->
		<script src="js/library/syntaxHighlighter/shCore.js"></script>
		<script src="js/library/syntaxHighlighter/shBrushCss.js"></script>
		<script src="js/library/syntaxHighlighter/shBrushXml.js"></script>
		<script src="js/library/syntaxHighlighter/shBrushJScript.js"></script>

		<!-- JS:bootstrap-->
		<script src="bootstrap/js/bootstrap.min.js"></script>
		
		<!-- JS:custom js for this template -->
		<script src="js/styler/custom.js"></script>
		<script src="js/set_cookie.js"></script>
		<script type="text/javascript">
			var GlobalSaleID = "<?php echo $_COOKIE['Ses_ID']?>";
			Number.prototype.nformat = function() {
			    return this.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
			};
		    $(document).ready(function(){
				
		    	/*$('.widget-sales').on('click','#loadsales',function(){
		        	$('#myModal').modal('show');
		    	});*/
					$('#detail > tbody:last').empty();
					var check_SesKeyword = getCookie("Ses_Keyword");
					if (check_SesKeyword!=null && check_SesKeyword!=""){
						document.getElementById("txtKeyword").value = check_SesKeyword;
						deleteCookie("Ses_Keyword");
						search();
					}else{
						$("#loading").show();
						refreshdata();
					}
					 
					$( "#btnSearch" ).on( "click", function()
						{
							refreshdata();
						});
					$('#txtKeyword').keyup(function(e) {
						if(e.keyCode == 13) {
							refreshdata();
						}
					});

					$( "#adduser" ).on( "click", callbackemployee_open);
					$( "#close_modal" ).on( "click", function()
						{
							refreshdata();
		    			$("#AddEdit_modal").modal('hide');


						});
					$("#loading").hide();
		    });
			function refreshdata()
			{
				$.ajax({ 
							type:'POST',
							dataType: "json",
							url: "AJAX/SV_SFS_Postsales_Select_E_all.php",
							data:{
								txtKeyword:$("#txtKeyword").val()
							},
							success: function(json) {
					//console.log(json);
								getSearch(json);

							},
							error:function()
							{
								alert("ระบบมีปัญหาในการดึงข้อมูล ");
							}

						});
			}
			function callbackemployee_open()
			{
				page = "../../Callback_open/indexOrg.php";				
				window.open(page,'','Left=50,Top=50,width=1024px,height=600px') ;
				
			}
			function returnCallbackemployee(value)
			{
				
					var lists=value.split(":");
					var name = lists[0];
					var empno = lists[1];
					var org = lists[2];
					if($("#AddEdit_tbody_list").children().length>0)
					{
						var checklist=0;
						$("#AddEdit_tbody_list tr").children().each(function() {
							//eachitemArray = [];
							tempP = $(this).find("p");
							if($(tempP[0]).text()==empno)
							{
									checklist=1;
							}
						});
						if(checklist==0)
						{
							AddUser(name,empno,org);
						}
						else
						{
							alert("กรุณากรองข้อมูลใหม่ เนื่องจากมีข้อมูลอยู่แล้ว");
						}
					}
					else
					{
						AddUser(name,empno,org);
					}
			}
			function callbackemployee()
			{
				page = "../../Callback/indexOrg.php";
				selectprojects = window.showModalDialog(page,"Callback_employee","dialogWidth:1024px;dialogHeight:600px;status:no;center:yes;edge:raised;resizable:no;");
				//alert(selectprojects);
				console.log(selectprojects);
				if(selectprojects != undefined ) {
					
					//lists=selectprojects.split(":");
					var name = selectprojects.name;
					var empno = selectprojects.empno;
					var org = selectprojects.org;
					if($("#AddEdit_tbody_list").children().length>0)
					{
						var checklist=0;
						$("#AddEdit_tbody_list tr").children().each(function() {
							//eachitemArray = [];
							tempP = $(this).find("p");
							if($(tempP[0]).text()==empno)
							{
									checklist=1;
							}
						});
						if(checklist==0)
						{
							AddUser(name,empno,org);
						}
						else
						{
							alert("กรุณากรองข้อมูลใหม่ เนื่องจากมีข้อมูลอยู่แล้ว");
						}
					}
					else
					{
						AddUser(name,empno,org);
					}
					//$("#showEContract").text();
					//var  m = eval("css."+owner);
					//m.value = t1;
				}
			}
			function AddUser(name,empno,org)
			{
				console.log(name);
				var contract=$("#showEContract").text();
				$.ajax({ 
					cache: false,
					type: "POST",
					dataType: "json",
					url: "AJAX/SV_SFS_Postsales_insertuser.php",							
					data: {
						name: name,
						empno: empno,
						org: org,
						contract: contract
					},
					success: function(json) {
								console.log(json);
								loadData(contract);
								//getSearch(json);
							},
							error:function()
							{
								alert("ระบบมีปัญหาในการดึงข้อมูล ");
							}

				});
			}
			function DeleteUser(id)
			{
				console.log(id);
				var contract=$("#showEContract").text();
				if(confirm("ยืนยันลบข้อมูล"))
				{
					$.ajax({ 
						cache: false,
						type: "POST",
						dataType: "json",
						url: "AJAX/SV_SFS_Postsales_deleteuser.php",							
						data: {
							id: id
						},
						success: function(json) {
							console.log(json);
							alert("ลบข้อมูลเรียบร้อย");
							loadData(contract);
									//getSearch(json);
						},
						error:function()
						{
							alert("ระบบมีปัญหาในการดึงข้อมูล ");
						}

					});
				}
			}
		// function search(){
		// 	$("#loading").show();
		// 	var Keyword = document.getElementById("txtKeyword").value;
		// 	if(Keyword.replace(" ","") == ""){
		// 		$.ajax({ 
		// 			dataType: "json",
		// 			url: "http://157.179.28.116/sfs/sv.svc/forecast_select_saleid?callback=?",
		// 			data: {
		// 				saleid: GlobalSaleID
		// 			},
		// 			success: getSearch
		// 	});
		// 	}else{
		// 		Keyword=Keyword.replace("[","[[]");//แก้ปัญหาเรื่อง Wildcard 
		// 		console.log("Keyword : ",Keyword );
		// 		$.ajax({ 
		// 			dataType: "json",
		// 			url: "http://157.179.28.116/sfs/sv.svc/Search_Select_keyword?callback=?",
		// 			data: {
		// 				saleid: GlobalSaleID,
		// 				keyword: Keyword
		// 			},
		// 			success: getSearch
		// 	});
		// 	}
		// }
		function countProperties(obj) {
			  var prop;
			  var propCount = 0;

			  for (prop in obj) {
			    propCount++;
			  }
			  return propCount;
			}
		function getSearch(json) {
          console.log("showTable:",countProperties(json));
					document.getElementById('sumResult').innerHTML=countProperties(json);
					$('#detail > tbody:last').empty();
					if(json.length== 0){
						var noresult = "<tr><td colspan='4' style='text-align:center;'><p class='lead14'>ไม่พบข้อมูล</p></td></tr>";
						$('#detail').append(noresult);
					}else{
						$.each(json, function() {
							var item = this;
							var str_EContract = item[2];
							var str_PEContractNo = item[3];
							var str_Project = item[4];
							var str_FlagSiteRef = item[5];
							var str_TimeFrameDeliveryDate = item[6];
							var str_SaleRepresentative = item[8];
							var str_TimeFrameContractSigndate = item[9];
							var newrow="";
							
							tr = $("<tr><td><p class='lead14'>"+str_EContract+"</p></td><td><p class='lead14'>"+str_Project+"</p></td><td><p class='lead14'>"+str_SaleRepresentative+"</p></td><td><p class='lead14'>"+str_TimeFrameContractSigndate+"</p></td><td><p class='lead14'>"+str_TimeFrameDeliveryDate+"</p></td><td>"+item[10]+"</td><td></td></tr>");

		 					var td_edit = $('<i class="i_btn icon-pencil-1" title="Edit" style="color: black;"></i>');
							td_edit.on("click", function() {
								$("#showEContract").text(str_EContract);
								$("#loading").show();
								loadData(str_EContract);
		    				$("#AddEdit_modal").modal('show');
		    				$("#loading").hide();
		    			});
		    			$(tr.find("td")[6]).append(td_edit);
							$('#detail').append(tr);
	    				
								
						});		
					}
				
      }
      function loadData(contract)
      {
      	$.ajax({
					type: "POST",
					dataType: "json",
					data: {
						contract: contract
					},
					url: "AJAX/SV_SFS_Postsales_select_contract.php",
					success: function(json)
					{
						
						$('#AddEdit_tbody_list').empty();
						if(json.length== 0){
							var noresult = "<tr><td colspan='4' style='text-align:center;'><p class='lead14'>ไม่พบข้อมูล</p></td></tr>";
							$('#AddEdit_tbody_list').append(noresult);
						}else{
							$.each(json, function() {
								var item = this;
								
								tr = $("<tr><td><p class='lead14'>"+item[1]+"</p></td><td><p class='lead14'>"+item[2]+"</p></td><td><p class='lead14'>"+item[3]+"</p></td><td></td></tr>");

			 					var td_del = $('<i class="i_btn icon-minus-circled-1" title="Delete" style="color: black;"></i>');
								td_del.on("click", function() {
										console.log(item[0]);
										DeleteUser(item[0]);
			    			});
			    			$(tr.find("td")[3]).append(td_del);
								$('#AddEdit_tbody_list').append(tr);
		    				
									
							});	
						}	
					},
					error: function() {
						alert("เกิดปัญหาในการค้นหากรุณาลองใหม่อีกครั้ง");
					}
				});			
      }
			function edit(id){
				setCookie("Ses_Keyword",document.getElementById("txtKeyword").value);
				window.location="newproject.php?id="+id+"&page=search";
			}
			function view(id){
				setCookie("Ses_Keyword",document.getElementById("txtKeyword").value);
				window.location="previewforecast.php?id="+id+"&page=search";
			}
		</script>
	</head>
<?php  $checkmenu = '9'; ?>
<body cz-shortcut-listen="true" style="margin-top:0px;">	
<div id="loading" style="height: 100%; width: 100%; position: fixed; left: 0; top: 0; z-index: 1051 !important; background-color: black; filter: alpha(opacity=9); -khtml-opacity: 0.9; -moz-opacity: 0.9; opacity: 0.9;"><img src="img/loading.gif" style="width: 150px; height: 150px; position: fixed; top: 50%; left: 50%; margin: -75px 0 0 -75px;"></div>
<section class="page-header bg-color white">
	<div class="container">
		<div class="row">
			<div class="span12">
				<div class="section-header clearfix">
					<div class="pull-left">
						<h2><i class="icon-user-5"></i>Postsale</h2>
					</div>
					<div id="shortcut" class="nav-collapse pull-right"><ul class="shortcuts unstyled">
						<?php include("menu.php");?>
					</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="dashboard content">
	<div class="container">
			<div class="row">
				<div class="span12 widget">		
					<!--<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-info-circled"></i> Search</span>						
					</div>-->
					<div class="widget widget_search">
						<div class="bg-color white rounded">
						
							<div style="padding:10px 20px 5px 20px;text-align:left">
								<span id="sumResult" class="label label-inverse " style="font-size:20px;padding:10px;" title="Number of results">0</span>
								&nbsp;&nbsp;<font color="#777777" style="font-size:14px"><b>รายการ</b></font>
								<span class="pull-right">
								<input class="span4 " type="text" id="txtKeyword" style="width:300px;margin-bottom:0px;margin-top:0px;" sthle="margin-bottom: 0px;" placeholder="ค้นหาจาก E- Contract หรือชื่อโครงการ หรือชื่อ sales">
								<img id="btnSearch" src="img/styler/icons/search.png" style="cursor: pointer;padding-left:5px;" width="16" height="16" border="0" data-toggle="tooltip" data-placement="bottom" data-original-title="Search"/>
								</span>
							</div>
							<!--<div class="box-padding" style="padding:5px">
								<label style="background-color: #F2DEDE; display: inline-block; width: 150px; text-align: center; float: left; cursor: default;color: #777777;">Potential&lt;50</label>
								<label style="background-color: #FCF8E3; display: inline-block; width: 150px; text-align: center; float: left; cursor: default;color: #777777;">Potential=50</label>
								<label style="background-color: #DFF0D8; display: inline-block; width: 150px; text-align: center; float: left; cursor: default;color: #777777;">Potential>50</label>
								<span class="pull-right" style="color: #777777;">Sort by : Signdate
								</span>
							</div>-->
							<!--<div class="form-search pull-right">
								<input class="input-medium search-query" type="text" placeholder="Search anything here">
								<button class="btn" type="submit">Search</button>
							</div>-->
							<div class="box-padding" style="padding:5px">
				    			<table class="table table-striped" id="detail">
									<thead>
										<tr role="row">
											<th style="background: none repeat scroll 0 0 #2ABF9E;color: #FFFFFF;" width="80px">E-Contract</th>
											<th style="background: none repeat scroll 0 0 #2ABF9E;color: #FFFFFF;"width="500px">Project Name</th>
											<th style="background: none repeat scroll 0 0 #2ABF9E;color: #FFFFFF;" width="120px">SalesName</th>
											<th style="background: none repeat scroll 0 0 #2ABF9E;color: #FFFFFF;" width="80px">SignDate</th>
											<th style="background: none repeat scroll 0 0 #2ABF9E;color: #FFFFFF;" width="120px">End of Contract</th>
											<th style="background: none repeat scroll 0 0 #2ABF9E;color: #FFFFFF;" width="40px"><i class="icon-user-5" title="รายชื่อผู้เกี่ยวข้อง"></i></th>
											<th style="background: none repeat scroll 0 0 #2ABF9E;color: #FFFFFF;" width="40px"><i class='icon-plus-6' title='Add'></i>|<i class="icon-pencil-1" title='Edit'></i></th>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
					        </div>
				        </div>
			    	</div>			
				</div>
			</div>
	</div>
</section>


		<div id="footer" class="bg-color dark-blue">
			<div class="container">
				<div class="box-padding">
					Copyright &copy; 2013 Sales Forecast
				</div>
			</div>
		</div>
		
<div class="modal hide fade" id="AddEdit_modal" data-backdrop="static">
	<div class="modal-header bg-color light-green aligncenter">
	    <h1 id="showEContract"></h1>
	</div>
	<div class="modal-body" style="padding-top: 0px;max-height:300px;">
		<table class="table table-striped">
			<thead>
				<tr >
					<th style="width: 100%;text-align:right" colspan="4" align="right"><i title="Add" class="i_btn icon-plus-6" id="adduser"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
				</tr>
				<tr style=" background-color: #2ABF9E; color: #FFFFFF;">
					<th style="width: 15%;">empno</th>
					<th style="width: 45%;">name</th>
					<th style="width: 30%;">department</th>
					<th style="width: 10%;"><i class="icon-minus-circled-1" title="Delete" ></i></th>
				</tr>
			</thead>
			<tbody id="AddEdit_tbody_list"></tbody>
		</table>
	</div>
	<div class="modal-footer alignright">
	    <a class="btn btn-red" data-dismiss="modal" id="close_modal">CLOSE</a>
	</div>
</div>
  	</body>

<!-- Mirrored from bootstrapstyler.com/preview/_/flatdashboard/ by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 18 Oct 2013 07:00:04 GMT -->
</html>