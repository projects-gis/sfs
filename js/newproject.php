<?
	$id ? $checkmenu = "0" : $checkmenu = "2";
	function replaceSQuoteAndNl($textinp) {
		$textinp = preg_replace("/\r|\n/", "", $textinp);
		return str_replace("'", "\'", $textinp);
	}
	if ($id) {
		$conn = mssql_connect("157.179.28.116", "sa", "dioro@4321");
		mssql_select_db("SaleForecast", $conn);
		$sqlStr = "SELECT IDForecast FROM Forecast WHERE ID='$id'";
		$query = mssql_query(iconv("UTF-8", "TIS-620", $sqlStr), $conn);
		if ($obj = mssql_fetch_object($query)) {
			$id = $obj->IDForecast;
		} else {
?>
	<script type="text/javascript">
		alert("ไม่มีข้อมูลของรายการนี้ในระบบ");
<?
			if ($page) {
?>
		window.location = "<?=$page?>.php";
<?
			} else {
?>
		window.location = "index.php";
<?
			}
?>
	</script>
<?
		}
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>SFS:::Add/Edit</title>
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css">
	<link href="js/library/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
	<link href="css/styler/style.css" rel="stylesheet" type="text/css">
	<link href="css/main.css" rel="stylesheet" type="text/css">
	<link href="js/library/syntaxHighlighter/shCore.css" rel="stylesheet" type="text/css"/>
	<link href="js/library/syntaxHighlighter/shThemeDefault.css" rel="stylesheet" type="text/css"/>
	<link href="css/styler/fontello.css" rel="stylesheet" type="text/css">
    <link href="css/styler/animation.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="js/library/jquery/jquery-ui-1.10.3.custom.min.css" />
</head>
<style type="text/css">
	.ui-autocomplete {
		font-size: 12px;
	}
</style>
<body cz-shortcut-listen="true" style="margin-top:0px;" >
<div id="loading" style="height: 100%; width: 100%; position: fixed; left: 0; top: 0; z-index: 1051 !important; background-color: black; filter: alpha(opacity=9); -khtml-opacity: 0.9; -moz-opacity: 0.9; opacity: 0.9;"><img src="img/loading.gif" style="width: 150px; height: 150px; position: fixed; top: 50%; left: 50%; margin: -75px 0 0 -75px;"></div>
<section class="page-header bg-color white">
	<div class="container">
		<div class="row">
			<div class="span12">
				<div class="section-header clearfix">
					<div class="pull-left">
					<?
						if ($id) {
							echo '<h2 id="forecastid"><i class="icon-edit-1"></i>'.$id.'</h2>';
						} else {
							echo '<h2 id="forecastid"><i class="icon-plus-1"></i>New Forecast</h2>';
						}
					?>
					</div>
					<div id="shortcut" class="nav-collapse pull-right">
						<ul class="shortcuts unstyled">
							<?include("menu.php");?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="dashboard content">
	<div class="container">
<!-- ============================================= Cont & Cus =========================================== -->
		<div class="row">				
			<div class="span6">					
				<div class="widget widget-initial">
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-suitcase"></i> Contract</span>
					</div>		
					<div class="bg-color turqoise rounded">
						<div class="box-padding" ><h4>
							<table>
							<tr>
								<td class="text-right">PEContract&nbsp;</td>
								<td><input id="inp_contractpe" class="span2" style="width:240px" type="text" style="margin-bottom: 0px;" placeholder="auto-generate after save." readonly><?if (!$id) {?><i id="btn_pEContract_modal" class="i_btn icon-search-3" data-toggle="tooltip" data-placement="bottom" data-original-title="Search PEContract"></i><i id="btn_pEContract_del" class="i_btn icon-cancel-alt" data-toggle="tooltip" data-placement="bottom" data-original-title="Delete PEContract"></i><? }?></td>
							</tr>
							<tr>
								<td class="text-right">E-Contract&nbsp;</td>
								<td><input id="inp_contractec" class="span2" style="width:240px" type="text" placeholder="Get E-Contract" readonly><i id="btn_eContract" class="i_btn icon-exchange" data-toggle="tooltip" data-placement="bottom" data-original-title="Get E-Contract"></i></td>
							</tr>
							<tr>
								<td class="text-right">Project&nbsp;</td>
								<td><textarea id="inp_contractproj" rows="3" style="width: 255px;"></textarea><i style="vertical-align: bottom;margin-bottom:5px;" id="btn_help_modal" class="i_btn icon-newspaper" data-toggle="tooltip" data-placement="bottom" data-original-title="เอกสารหลักการตั้งชื่อโครงการ"></i></td>
							</tr>
							<tr>
								<td class="text-right">Vertical Market Group&nbsp;</td>
								<td>
									<select id="slc_verticalmarket" class="span3" style="width: 280px;">
										<option value="0">None</option>
										<option value="2">Banking and Financial Service</option>
										<option value="6">Education</option>
										<option value="4">Emergency and Disaster</option>
										<option value="5">Environment Mgnt</option>
										<option value="3">Government</option>
										<option value="1">Utilities and Commu</option>
									</select>
								</td>
							</tr>
							<tr>
								<td></td>
								<td>
									<span id="siproject" class="label bg-color dark-blue label-inverse text-center" title="SI project = App + Data + HW + SW" style="font-size: 20px; padding: 10px; width: 260px; margin: 5px 0px 5px 0px;">Non SI Project</span>
								</td>
							</tr>
							<tr>
								<td></td>
								<td>
									<span id="sigproject" class="label bg-color dark-blue label-inverse text-center" title="Significant Project = Potential >= 50%, Income >= 5MB" style="font-size: 20px; padding: 10px; width: 260px; margin: 5px 0px 5px 0px;">Non Significant Project</span>
								</td>
							</tr>
							</table>							
						</h4></div>
					</div>
				</div>
			</div>
			<div class="span6">					
				<div class="widget widget-customer">	
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-users-3"></i> Customer</span>
					</div>					
					<div class="bg-color turqoise rounded">
						<div class="box-padding"><h4>
							<table>
								<tr>
									<td class="text-right">Name&nbsp;</td>
									<td><input id="inp_custid" type="hidden"><input id="inp_custno" type="hidden"><input id="inp_custini" type="hidden"><input id="inp_custtname" class="span1" type="text" readonly>&nbsp;<input id="inp_custfname" class="span2" type="text" style="width: 100px;" readonly>&nbsp;<input id="inp_custlname" class="span2" type="text" style="width: 100px;" readonly><i id="btn_customer_modal" class="i_btn icon-search-3" data-toggle="tooltip" data-placement="bottom" data-original-title="Search Customer"></i></td>
									<!--<? //if (!$id) { ?><i id="btn_customer_modal" class="i_btn icon-search-3" data-toggle="tooltip" data-placement="bottom" data-original-title="Search Customer"></i><? //} ?>-->
								</tr>	
								<tr>
									<td class="text-right">Division&nbsp;</td>
									<td><input id="inp_custdiv" class="span3" type="text" style="width: 315px;" readonly></td>
								</tr>
								<tr>
									<td class="text-right">Department&nbsp;</td>
									<td><input id="inp_custdep" class="span3" type="text" style="width: 315px;" readonly></td>
								</tr>
								<tr>
									<td class="text-right">Organization&nbsp;</td>
									<td><input id="inp_custorg" class="span3" type="text" style="width: 315px;" readonly></td>
								</tr>
								<tr height="15px">
									<td colspan="2"></td>
								</tr>
								<? if (!$id) { ?><tr>
									<td></td>
									<td><a id="btn_import_modal" class="btn btn-blue bordered" style="width: 297px; margin-top: 6px;">Import from Quotation</a></td>
								</tr><? } ?>
								<tr>
									<td></td>
									<td id="importedFrom"></td>
								</tr>
							</table>							
						</h4></div>
					</div>
				</div>				
			</div>
		</div>
<!-- ==================================================================================================== -->

<!-- ============================================= Targ & Time ========================================== -->
		<div class="row">				
			<div class="span6">					
				<div class="widget widget-target">	
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-target-2"></i> Target</span>
						<div class="dropdown pull-right"><h4 style="margin: 0px;">
							<label class="checkbox inline" style="color: #0072C6; font-weight: bold; padding-top: 0px;">
								<input id="chk_esri" type="checkbox" value="1">
								ESRI's TOR
							</label>
							<label class="checkbox inline" style="color: #0072C6; font-weight: bold; padding: 0px;">
								<input id="chk_edu" name="rd_pType" type="radio" value="1" style="margin: 0px;">
								Education
							</label>
							<label class="checkbox inline" style="color: #0072C6; font-weight: bold; padding: 0px;">
								<input id="chk_proj" name="rd_pType" type="radio" value="1" style="margin: 0px;" checked>
								Project
							</label>
							<label class="checkbox inline" style="color: #AC193D; font-weight: bold; padding: 0px;">
								<input id="chk_sproj" name="rd_pType" type="radio" value="1" style="margin: 0px;">
								Special Project
							</label>
						</h4></div>
					</div>					
					<div class="bg-color turqoise rounded">
						<div class="box-padding"><h5>
							<table style="margin-bottom: 10px;">
								<tr>
									<td class="text-right" style="width: 100px;"><h4>Potential&nbsp;</h4></td>
									<td class="text-right" style="width: 60px;"><input id="inp_potentialp" class="span1" type="text" value="10%" readonly></td>
									<td class="text-right"><input id="inp_potentialt" class="span3" type="text" value="โอกาสน้อยมาก" readonly><i id="btn_potential_modal" class="i_btn icon-th-list" data-toggle="tooltip" data-placement="bottom" data-original-title="Load Potential"></i><i id="btn_hPotential_modal" class="i_btn icon-clock-8" data-toggle="tooltip" data-placement="bottom" data-original-title="History" style="display: none;"></i></td>
								</tr>
								<tr>
									<td class="text-right"><h4>Progress&nbsp;</h4></td>
									<td class="text-right"><input id="inp_progressp" class="span1" type="text" value="10%" readonly></td>
									<td class="text-right"><input id="inp_progresst" class="span3" type="text" value="Build solution" readonly><i id="btn_progress_modal" class="i_btn icon-th-list" data-toggle="tooltip" data-placement="bottom" data-original-title="Load Progress"></i><i id="btn_hProgress_modal" class="i_btn icon-clock-8" data-toggle="tooltip" data-placement="bottom" data-original-title="History" style="display: none;"></i></td>
								</tr>
							</table>
							<table>
								<tbody>
									<tr>
										<td colspan="2" class="text-right" style="width: 270px;"><h4>Excluded VAT</h4></td>
										<td style="width: 50px;"></td>
										<td class="text-right"><h4>Included VAT</h4></td>
										<td style="width: 50px;"></td>
									</tr>
									<tr>
										<td class="text-right">Budget&nbsp;</td>
										<td><input id="inp_budg_evat" class="span2 number text-right" type="text" value="0.00"></td>
										<td>Baht</td>
										<td><input id="inp_budg_ivat" class="span2 number text-right" type="text" value="0.00"></td>
										<td>Baht</td>
									</tr>
									<tr>
										<td class="text-right">Income&nbsp;</td>
										<td><input id="inp_inco_evat" class="span2 number text-right" type="text" value="0.00"></td>
										<td>Baht</td>
										<td><input id="inp_inco_ivat" class="span2 number text-right" type="text" value="0.00"></td>
										<td>Baht</td>
									</tr>
									<tr>
										<td class="text-right">Cost&nbsp;</td>
										<td><input id="inp_cost_evat" class="span2 number text-right" type="text" value="0.00"></td>
										<td>Baht</td>
										<td><input id="inp_cost_ivat" class="span2 number text-right" type="text" value="0.00"></td>
										<td>Baht</td>
									</tr>
								</tbody>
							</table>
						</h5></div>
					</div>
				</div>
			</div>
			<div class="span6">					
				<div class="widget widget-timeframe">	
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-calendar-6"></i> Time Frame</span>
					</div>					
					<div class="bg-color turqoise rounded">
						<div class="box-padding"><h5>
							<table>
							<tr>
								<td class="text-right">Bidding Date&nbsp;</td>
								<td><input id="inp_biddDate" class="form_date" style="width: 80px;" type="text" placeholder="choose date"></td>
								<td class="text-right">Contract Sign Date&nbsp;</td>
								<td><input id="inp_signDate" class="form_date" style="width: 80px;" type="text" placeholder="choose date"></td>
							</tr>	
							<tr>
								<td class="text-right">Project Duration&nbsp;</td>
								<td><input id="inp_pDuration" class="span1 nNumber text-right" type="text" placeholder="0"> Days</td>
								<td class="text-right">End of Contract&nbsp;</td>
								<td><input id="inp_endCont" class="span1" style="width: 80px;" type="text" readonly></td>
							</tr>
							<tr>
								<td class="text-right"><input id="chk_downPayment" type="checkbox" value="" style="margin-top: 0px;" checked> Down Payment&nbsp;</td>
								<td><input id="inp_downPayment" class="span1 nNumber text-right" type="text" placeholder="0">&nbsp;&nbsp;&nbsp;&nbsp;% =</td>
								<td colspan="2"><input id="inp_dPBaht" class="span2 number text-right" type="text" style="width: 150px;" placeholder="0.00" readonly> Baht</td>
							</tr>
							<tr>
								<td class="text-right">Inv.Date&nbsp;</td>
								<td><input id="inp_dPDays" class="span1 nNumber text-right" type="text" placeholder="0"> Days =&nbsp;</td>
								<td colspan="2"><input id="inp_dPDate" class="form_date" style="width: 80px;" type="text" placeholder="choose date"></td>
							</tr>
							</table>							
							 </h5>
						</div>
					</div>
				</div>				
			</div>
		</div>
<!-- ==================================================================================================== -->

<!-- ============================================= Invo & Book ========================================== -->
		<div class="row">				
			<div class="span8">					
				<div class="widget widget-invoicing">	
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-list-alt"></i> Invoicing Plan</span>
						<div class="dropdown pull-right">
							<h3 style="margin:0px;margin-top:-5px"><i id="btn_invoicing_modal" class="i_btn icon-edit-1" data-toggle="tooltip" data-placement="bottom" data-original-title="Manage Plan"></i></h3>
						</div>
					</div>					
					<div class="bg-color white rounded">
						<div class="box-padding" style="padding:5px">
			    			<table class="table table-striped">
								<thead>
									<tr style="background-color: #2ABF9E; color: #FFFFFF;">
										<th style="width: 50px;">Phase</th>
										<th style="width: 50px;">Days</th>
										<th style="width: 70px;">Date</th>
										<th style="width: 50px;">%</th>
										<th style="width: 120px;">Amount</th>
										<th>Remark</th>
									</tr>
								</thead>
								<tbody id="tbody_invoicing"></tbody>
							</table>
				        </div>
					</div>
				</div>
			</div>
			<div class="span4">					
				<div class="widget widget-book">	
					<div class="widget-header clearfix">
						<span class="pull-left" style="padding-bottom: 3px;"><i class="icon-dollar"></i> Book/Forward</span>
					</div>					
					<div class="bg-color white rounded">
						<div class="box-padding" style="padding:5px">
			    			<table class="table table-striped">
								<thead>
									<tr style="background-color: #2ABF9E; color: #FFFFFF;">
										<th style="width: 80px;">Type</th>
										<th style="width: 80px;">Year</th>
										<th>Amount</th>
									</tr>
								</thead>
								<tbody id="tbody_bank"></tbody>
							</table>
				        </div>
					</div>
				</div>
			</div>
		</div>
<!-- ==================================================================================================== -->

<!-- ============================================= Sales Rep. =========================================== -->
		<div class="row">
			<div class="span12">
				<div class="widget widget-representative">
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-user-5"></i> sales representative</span>
					</div>		
					<div class="bg-color dark-blue rounded">
						<div class="box-padding">
							<h4>
							<table>
								<tr>
									<td class="text-right">Sales Representative&nbsp;</td>
									<td><input id="inp_salesid" type="hidden"><input id="inp_salesname" class="span3" type="text" readonly><i id="btn_sales_modal" class="i_btn icon-search-3" data-toggle="tooltip" data-placement="bottom" data-original-title="Load Sales"></i></td>
								</tr>	
								<tr>
									<td class="text-right">Project By&nbsp;</td>
									<td>
										<select id="slc_salesproj" class="span2">
											<option value="Sales 1">Sales 1</option>
											<option value="Sales 2">Sales 2</option>
											<option value="Sales 3">Sales 3</option>
											<option value="Sales 4">Sales 4</option>
											<option value="Sales TM">Sales TM</option>
											<option value="Sales CSD">Sales CSD</option>
										</select>
									</td>
								</tr>
								<tr>
									<td class="text-right">Remark&nbsp;</td>
									<td><textarea id="txa_salesremark" rows="3" style="width: 255px;"></textarea></td>
								</tr>
							</table>							
							</h4>
						</div>
					</div>
				</div>
			</div>	
		</div>
<!-- ==================================================================================================== -->

<!-- ============================================= Status & Dept ======================================== -->
		<div class="row">					
			<div class="span8">					
				<div class="widget widget-status">	
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-tasks-1"></i> Status</span>
						<div class="dropdown pull-right">
							<h3 style="margin:0px;margin-top:-5px"><i id="btn_status_modal" class="i_btn icon-list-add" data-toggle="tooltip" data-placement="bottom" data-original-title="Add Status"></i></h3>
						</div>
					</div>					
					<div class="bg-color white rounded">
						<div class="box-padding" style="padding:5px">
			    			<table class="table table-striped">
								<thead>
									<tr style="background-color: #2ABF9E; color: #FFFFFF;">
										<th style="width: 100px;">Date</th>
										<th>Description</th>
										<th class="text-center" style="width: 20px;"><i class="icon-pencil-1" data-toggle="tooltip" data-placement="bottom" data-original-title='Edit'></i></th>
										<th class="text-center" style="width: 20px;"><i class="icon-minus-circled-1" data-toggle="tooltip" data-placement="bottom" data-original-title='Delete'></i></th>
									</tr>
								</thead>
								<tbody id="tbody_status"></tbody>
							</table>
				        </div>
					</div>
				</div>
			</div>
			<div class="span4">					
				<div class="widget widget-Dept">	
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-sitemap"></i> Department Support</span>
						<div class="dropdown pull-right">
							<h3 style="margin:0px;margin-top:-5px"><i id="btn_deptsup_modal" class="i_btn icon-check-1" data-toggle="tooltip" data-placement="bottom" data-original-title="Choose Department"></i></h3>
						</div>
					</div>					
					<div class="bg-color white rounded">
						<div class="box-padding" style="padding:5px">
			    			<table class="table table-striped">
								<thead>
									<tr style="background-color: #2ABF9E; color: #FFFFFF;">
										<th>Description</th>
										<th style="width: 20px; text-aglign: center;"><i class="icon-minus-circled-1" data-toggle="tooltip" data-placement="bottom" data-original-title='Delete'></i></th>
									</tr>
								</thead>
								<tbody id="tbody_deptsup"></tbody>
							</table>
				        </div>
					</div>
				</div>
			</div>
		</div>
<!-- ==================================================================================================== -->

<!-- ============================================= Desc & % ============================================= -->
		<div class="row">				
			<div class="span12">					
				<div class="widget widget-description">	
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-article-alt-1"></i> Description</span>
						<div class="dropdown pull-right">
							<h3 style="margin:0px;margin-top:-5px"><i id="btn_description_modal" class="i_btn icon-list-add" data-toggle="tooltip" data-placement="bottom" data-original-title="Add Description"></i></h3>
						</div>
					</div>					
					<div class="bg-color white rounded-top">
						<div class="box-padding" style="padding:5px">
			    			<table class="table table-striped">
								<thead>
									<tr style="background-color: #2ABF9E; color: #FFFFFF;">
										<th style="width: 100px;">Type</th>
										<th>Description</th>
										<th style="width: 100px;">Price</th>
										<th class="text-center" style="width: 20px;"><i class="icon-pencil-1" data-toggle="tooltip" data-placement="bottom" data-original-title='Edit'></i></th>
										<th class="text-center" style="width: 20px;"><i class="icon-minus-circled-1" data-toggle="tooltip" data-placement="bottom" data-original-title='Delete'></i></th>
									</tr>
								</thead>
								<tbody id="tbody_description"></tbody>
							</table>
				        </div>
					</div>
				</div>
			</div>
		</div>
<!-- ==================================================================================================== -->

<!-- ============================================= Product ============================================== -->
		<div class="row">
			<div class="span6">					
				<div class="widget widget-esriproduct">	
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-star-5"></i>ESRI Product</span>
						<div class="dropdown pull-right">
							<input type="hidden" value="ESRI Product">
							<h3 style="margin:0px;margin-top:-5px"><i class="add_product i_btn icon-list-add" data-toggle="tooltip" data-placement="bottom" data-original-title="Add Product"></i></h3>
						</div>
					</div>
					<div class="bg-color white rounded">
						<div class="box-padding" style="padding:5px">
			    			<table class="table table-striped">
								<thead>
									<tr style="background-color: #2ABF9E; color: #FFFFFF;">
										<th>Description</th>
										<th style="width: 50px;">Qty</th>
										<th class="text-center" style="width: 20px;"><i class="icon-pencil-1" data-toggle="tooltip" data-placement="bottom" data-original-title='Edit'></i></th>
										<th class="text-center" style="width: 20px;"><i class="icon-minus-circled-1" data-toggle="tooltip" data-placement="bottom" data-original-title='Delete'></i></th>
									</tr>
								</thead>
								<tbody id="tbody_esri"></tbody>
							</table>
				        </div>
					</div>
				</div>
			</div>
			<div class="span6">					
				<div class="widget widget-esriproduct">	
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-star-5"></i>Leica Product</span>
						<div class="dropdown pull-right">
							<input type="hidden" value="Leica">
							<h3 style="margin:0px;margin-top:-5px"><i class="add_product i_btn icon-list-add" data-toggle="tooltip" data-placement="bottom" data-original-title="Add Product"></i></h3>
						</div>
					</div>					
					<div class="bg-color white rounded">
						<div class="box-padding" style="padding:5px">
			    			<table class="table table-striped">
								<thead>
									<tr style="background-color: #2ABF9E; color: #FFFFFF;">
										<th>Description</th>
										<th style="width: 50px;">Qty</th>
										<th class="text-center" style="width: 20px;"><i class="icon-pencil-1" data-toggle="tooltip" data-placement="bottom" data-original-title='Edit'></i></th>
										<th class="text-center" style="width: 20px;"><i class="icon-minus-circled-1" data-toggle="tooltip" data-placement="bottom" data-original-title='Delete'></i></th>
									</tr>
								</thead>
								<tbody id="tbody_leica"></tbody>
							</table>
				        </div>
					</div>
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="span6">					
				<div class="widget widget-garmin">	
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-star-5"></i>Garmin Product</span>
						<div class="dropdown pull-right">
							<input type="hidden" value="Garmin">
							<h3 style="margin:0px;margin-top:-5px"><i class="add_product i_btn icon-list-add" data-toggle="tooltip" data-placement="bottom" data-original-title="Add Product"></i></h3>
						</div>
					</div>					
					<div class="bg-color white rounded">
						<div class="box-padding" style="padding:5px">
			    			<table class="table table-striped">
								<thead>
									<tr style="background-color: #2ABF9E; color: #FFFFFF;">
										<th>Description</th>
										<th style="width: 50px;">Qty</th>
										<th class="text-center" style="width: 20px;"><i class="icon-pencil-1" data-toggle="tooltip" data-placement="bottom" data-original-title='Edit'></i></th>
										<th class="text-center" style="width: 20px;"><i class="icon-minus-circled-1" data-toggle="tooltip" data-placement="bottom" data-original-title='Delete'></i></th>
									</tr>
								</thead>
								<tbody id="tbody_garmin"></tbody>
							</table>
				        </div>
					</div>
				</div>
			</div>
			<div class="span6">					
				<div class="widget widget-nostra">	
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-star-5"></i>Nostra Product </span>
						<div class="dropdown pull-right">
							<input type="hidden" value="NOSTRA">
							<h3 style="margin:0px;margin-top:-5px"><i class="add_product i_btn icon-list-add" data-toggle="tooltip" data-placement="bottom" data-original-title="Add Product"></i></h3>
						</div>
					</div>					
					<div class="bg-color white rounded">
						<div class="box-padding" style="padding:5px">
			    			<table class="table table-striped">
								<thead>
									<tr style="background-color: #2ABF9E; color: #FFFFFF;">
										<th>Description</th>
										<th style="width: 50px;">Qty</th>
										<th class="text-center" style="width: 20px;"><i class="icon-pencil-1" data-toggle="tooltip" data-placement="bottom" data-original-title='Edit'></i></th>
										<th class="text-center" style="width: 20px;"><i class="icon-minus-circled-1" data-toggle="tooltip" data-placement="bottom" data-original-title='Delete'></i></th>
									</tr>
								</thead>
								<tbody id="tbody_nostra"></tbody>
							</table>
				        </div>
					</div>
				</div>
			</div>				
		</div>
<!-- ==================================================================================================== -->
		<div class="row">
			<div class="span12">
				<div class="widget widget-save">
					<div class="widget-header clearfix">
						<div class="dropdown pull-right">
							<a id="btn_save" class="btn btn-blue" style="color: #FFFFFF"><i class="i_btn icon-floppy" data-toggle="tooltip" data-placement="bottom" data-original-title="Save"></i> SAVE</a>&nbsp;&nbsp;&nbsp;<a id="btn_cancel" class="btn btn-blue" style="color: #FFFFFF">CANCEL</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="bg-color dark-blue">
	<div class="container">
		<div class="box-padding">
			Copyright &copy; 2013 Sales Forecast
		</div>
	</div>
</div>
<div class="modal hide fade" id="pEContract_modal">
	<div class="modal-header bg-color light-green aligncenter">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    <h1>PEContract</h1>
	</div>
	<div class="modal-body" style="padding-bottom: 0px;">
		<span id="pEContract_amt" class="label label-inverse pull-left" title="Amount of results" style="font-size: 20px; padding: 10px; margin-top: 14px; cursor: default;">0</span>
		<h4 class="pull-right"><input id="pEContract_inp_keywords" class="span3" type="text" style="margin-bottom: 0px;" placeholder="ค้นหาจาก PEContract และ Project Name"><i class="i_btn icon-search-3" id="searchPE"></i></h4>
	</div>
	<div class="modal-body" style="padding-top: 0px;">
		<table class="table table-striped table-condensed table_select">
			<thead>
				<tr style=" background-color: #2ABF9E; color: #FFFFFF;">
					<th style="width: 15%;">PEContract</th>
					<th style="width: 15%;">Code</th>
					<th style="width: 70%;">Project Name</th>
				</tr>
			</thead>
			<tbody id="pEContract_tbody_list"></tbody>
		</table>
	</div>
	<div class="modal-footer alignright">
	    <a class="btn btn-red" data-dismiss="modal">CLOSE</a>
	</div>
</div>
<div class="modal hide fade" id="customer_modal" style="width: 1200px; margin-left: -600px;">
	<div class="modal-header bg-color light-green aligncenter">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    <h1>Customer</h1>
	</div>
	<div class="modal-body" style="padding-bottom: 0px;">
		<span id="customer_amt" class="label label-inverse pull-left" title="Amount of results" style="font-size: 20px; padding: 10px; margin-top: 14px; cursor: default;">0</span>
		<h4 class="pull-right"><input id="customer_inp_keywords" class="span3" type="text" style="margin-bottom: 0px; height: 18px;" placeholder="Search Customer"><i id="customer_btn_search" class="i_btn icon-search-3"></i>&nbsp;&nbsp;&nbsp;&nbsp;<i class="i_btn icon-user-add" onclick="window.open('http://itsystem/cis2/checkuser.php?userlogin=<?=$_COOKIE['Ses_ID']?>&page=new_customer')" title="Add Customer"></i></h4>
	</div>
	<div class="modal-body" style="padding-top: 0px;">
		<table class="table table-striped table-condensed table_select">
			<thead>
				<tr style=" background-color: #2ABF9E; color: #FFFFFF;">
					<th style="width: 5%;">Title</th>
					<th style="width: 10%;">Name</th>
					<th style="width: 10%;">Surname</th>
					<th style="width: 25%;">Division</th>
					<th style="width: 25%;">Department</th>
					<th style="width: 25%;">Organization</th>
				</tr>
			</thead>
			<tbody id="customer_tbody_list"></tbody>
		</table>
	</div>
	<div class="modal-footer alignright">
	    <a class="btn btn-red" data-dismiss="modal">CLOSE</a>
	</div>
</div>
<div class="modal hide fade" id="import_modal" style="width: 1200px; margin-left: -600px; top: 0%;">
	<div class="modal-header bg-color light-green aligncenter">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    <h1>Import from Quotation</h1>
	</div>
	<div class="modal-body" style="padding-bottom: 0px;">
		<span id="import_amt" class="label label-inverse pull-left" title="Amount of results" style="font-size: 20px; padding: 10px; margin-top: 14px; cursor: default;">0</span>
		<h4 class="pull-right">
			<select id="import_slc_year" style="width: 100px;">
				<option value="All">All</option>
			<?
				$conn = mssql_connect("wellington", "sa", "dioro4321");
				mssql_select_db("Quotation_Test", $conn);
				$sqlStr = "SELECT DISTINCT qyear FROM Quotation ORDER BY qyear DESC";
				$query = mssql_query(iconv("UTF-8", "TIS-620", $sqlStr), $conn);
				while ($obj = mssql_fetch_object($query)) {
					if (trim($obj->qyear) != "") echo '<option value="'.trim($obj->qyear).'">'.trim($obj->qyear).'</option>';
				}
			?>
			</select>
			<select id="import_slc_sales" class="span3">
				<option value="All">All</option>
			<?
				$sqlStr = "SELECT id, REPLACE(REPLACE(REPLACE(thainame,'นางสาว',''),'นาง',''),'นาย','') AS thainame FROM User_Lookup WHERE Flag != '0' ORDER BY thainame";
				$query = mssql_query(iconv("UTF-8", "TIS-620", $sqlStr), $conn);
				while ($obj = mssql_fetch_object($query)) {
					echo '<option value="'.trim($obj->id).'">'.replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", trim($obj->thainame))).'</option>';
				}
			?>
			</select>
			<input id="import_inp_keywords" class="span3" type="text" style="margin-bottom: 0px; height: 18px;" placeholder="Search quotation no., customer details">
			<i id="import_btn_search" class="i_btn icon-search-3"></i>
		</h4>
	</div>
	<div class="modal-body" style="height: 100px; padding-top: 0px;">
		<table class="table table-striped table-condensed table_select">
			<thead>
				<tr style=" background-color: #2ABF9E; color: #FFFFFF;">
					<th style="width: 7%;">Quotation</th>
					<th style="width: 7%;">Date</th>
					<th style="width: 18%;">Customer</th>
					<th style="width: 25%;">Department</th>
					<th style="width: 25%;">Organization</th>
					<th style="width: 18%;">Sales</th>
				</tr>
			</thead>
			<tbody id="import_tbody_qlist"></tbody>
		</table>
	</div>
	<div class="modal-body import_pbody" style="padding-bottom: 0px; padding-top: 0px;">
		<span id="import_quotno" class="label label-inverse pull-left" style="width: 107px; font-size: 20px; padding: 10px; margin-top: 15px; margin-bottom: 15px; background-color: #82BA00; cursor: default;"></span>
		<h5>
			&nbsp;&nbsp;&nbsp;Sales <input id="import_sales" type="text" style="width: 180px" readonly>
			&nbsp;&nbsp;&nbsp;Date <input id="import_date" type="text" style="width: 70px" readonly>
			&nbsp;&nbsp;&nbsp;Customer <input id="import_customer" type="text" style="width: 180px" readonly>
			&nbsp;&nbsp;&nbsp;Organization <input id="import_organization" type="text" style="width: 206px" readonly>
		</h5>
		<input id="import_cid" type="hidden">
		<input id="import_cno" type="hidden">
		<input id="import_cini" type="hidden">
		<input id="import_ctitle" type="hidden">
		<input id="import_cname" type="hidden">
		<input id="import_csname" type="hidden">
		<input id="import_division" type="hidden">
		<input id="import_department" type="hidden">
	</div>
	<div class="modal-body import_pbody" style="height: 150px; padding-top: 0px;">
		<table class="table table-striped table-condensed">
			<thead>
				<tr style=" background-color: #2ABF9E; color: #FFFFFF;">
					<th style="width: 2%;"></th>
					<th style="width: 10%;">Type</th>
					<th style="width: 10%;">Brand</th>
					<th style="width: 18%;">Group</th>
					<th style="width: 40%;">Description</th>
					<th style="width: 5%;">Qty.</th>
					<th style="width: 15%;">Total Price</th>
				</tr>
			</thead>
			<tbody id="import_tbody_plist"></tbody>
		</table>
	</div>
	<div class="modal-footer alignright">
	    <a id="import_btn_ok" class="btn btn-blue">OK</a>
	    <a class="btn btn-red" data-dismiss="modal">CLOSE</a>
	</div>
</div>
<div class="modal hide fade" id="idescription_modal" style="width: 800px; margin-left: -400px;">
	<div class="modal-header bg-color light-green aligncenter">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    <h1>Description</h1>
	</div>
	<div class="modal-body">
		<table class="table">
			<thead>
				<tr style=" background-color: #2ABF9E; color: #FFFFFF;">
					<th style="width: 20%;">Type</th>
					<th style="width: 50%;">Description</th>
					<th style="width: 30%;">Price</th>
				</tr>
			</thead>
			<tbody id="idescription_tbody_list"></tbody>
		</table>
	</div>
	<div class="modal-footer alignright">
	    <a id="idescription_btn_ok" class="btn btn-blue">OK</a>
	    <a class="btn btn-red" data-dismiss="modal">CLOSE</a>
	</div>
</div>
<div class="modal hide fade" id="description_modal" style="width: 800px; margin-left: -400px;">
	<div class="modal-header bg-color light-green aligncenter">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    <h1>Description</h1>
	</div>
	<div class="modal-body">
		<table class="table">
			<thead>
				<tr style=" background-color: #2ABF9E; color: #FFFFFF;">
					<th style="width: 20%;">Type</th>
					<th style="width: 50%;">Description</th>
					<th style="width: 30%;">Price</th>
				</tr>
			</thead>
			<tbody id="description_tbody_list"></tbody>
		</table>
	</div>
	<div class="modal-footer alignright">
	    <a id="description_btn_ok" class="btn btn-blue">OK</a>
	    <a class="btn btn-red" data-dismiss="modal">CLOSE</a>
	</div>
</div>
<div class="modal hide fade" id="editdescription_modal" style="width: 500px; margin-left: -250px;">
	<div class="modal-header bg-color light-green aligncenter">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    <h1>Edit Description</h1>
	</div>
	<div class="modal-body"><h5>
		<input id="editdescription_inp_eid" type="hidden">
		<table style="width: 100%">
			<tr>
				<td class="text-right" style="width: 20%">Type&nbsp;</td>
				<td style="width: 80%">
					<select class="span2" id="editdescription_slc_type">
						<option value="HW">HW</option>
						<option value="SW">SW</option>
						<option value="Application">Application</option>
						<option value="Data">Data</option>
						<option value="Site Prep">Site Prep</option>
						<option value="On Site">On Site</option>
						<option value="Training">Training</option>
						<option value="Other">Other</option>
						<option value="ESRI">ESRI</option>
						<option value="Leica">Leica</option>
						<option value="Garmin">Garmin</option>
						<option value="NOSTRA">NOSTRA</option>
					</select>
				</td>
			</tr>
			<tr>
				<td class="text-right">Description&nbsp;</td>
				<td><input id="editdescription_inp_description" type="text" style="width: 90%;height: 18px; margin-bottom: 0px;"></td>
			</tr>
			<tr>
				<td class="text-right">Price&nbsp;</td>
				<td><input id="editdescription_inp_price" class="span2 text-right number" type="text"></td>
			</tr>
		</table>
	</h5></div>
	<div class="modal-footer alignright">
	    <a id="editdescription_btn_ok" class="btn btn-blue">OK</a>
	    <a class="btn btn-red" data-dismiss="modal">CLOSE</a>
	</div>
</div>
<div class="modal hide fade" id="potential_modal">
	<div class="modal-header bg-color light-green aligncenter">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    <h1>Potential</h1>
	</div>
	<div class="modal-body">
		<table class="table table-striped table-condensed table_select">
			<thead>
				<tr style=" background-color: #2ABF9E; color: #FFFFFF;">
					<th style="width: 20%;">%</th>
					<th style="width: 80%;">Description</th>
				</tr>
			</thead>
			<tbody id="potential_tbody_list"></tbody>
		</table>
	</div>
	<div class="modal-footer alignright">
	    <a class="btn btn-red" data-dismiss="modal">CLOSE</a>
	</div>
</div>
<div class="modal hide fade" id="progress_modal">
	<div class="modal-header bg-color light-green aligncenter">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    <h1>Progress</h1>
	</div>
	<div class="modal-body">
		<table class="table table-striped table-condensed table_select">
			<thead>
				<tr style=" background-color: #2ABF9E; color: #FFFFFF;">
					<th style="width: 20%;">%</th>
					<th style="width: 80%;">Description</th>
				</tr>
			</thead>
			<tbody id="progress_tbody_list"></tbody>
		</table>
	</div>
	<div class="modal-footer alignright">
	    <a class="btn btn-red" data-dismiss="modal">CLOSE</a>
	</div>
</div>
<div class="modal hide fade" id="hPotential_modal">
	<div class="modal-header bg-color light-green aligncenter">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    <h1>Potential History</h1>
	</div>
	<div class="modal-body">
		<table class="table table-striped">
			<thead>
				<tr style=" background-color: #2ABF9E; color: #FFFFFF;">
					<th style="width: 15%;">Date</th>
					<th style="width: 15%;">Potential</th>
					<th style="width: 70%;">Description</th>
				</tr>
			</thead>
			<tbody id="hPotential_tbody_list"></tbody>
		</table>
	</div>
	<div class="modal-footer alignright">
	    <a class="btn btn-red" data-dismiss="modal">CLOSE</a>
	</div>
</div>
<div class="modal hide fade" id="hProgress_modal">
	<div class="modal-header bg-color light-green aligncenter">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    <h1>Progress History</h1>
	</div>
	<div class="modal-body">
		<table class="table table-striped">
			<thead>
				<tr style=" background-color: #2ABF9E; color: #FFFFFF;">
					<th style="width: 15%;">Date</th>
					<th style="width: 15%;">Progress</th>
					<th style="width: 70%;">Description</th>
				</tr>
			</thead>
			<tbody id="hProgress_tbody_list"></tbody>
		</table>
	</div>
	<div class="modal-footer alignright">
	    <a class="btn btn-red" data-dismiss="modal">CLOSE</a>
	</div>
</div>
<div class="modal hide fade" id="invoicing_modal" style="width: 800px; margin-left: -400px;">
	<div class="modal-header bg-color light-green aligncenter">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    <h1>Invoicing Plan</h1>
	</div>
	<div class="modal-body">
		<table id="invoicing_table_details" style="width: 100%">
			<tr>
				<td style="width: 50%;"></td>
				<td style="width: 50%;"></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
			</tr>
		</table>
		<table class="table table-striped table-condensed">
			<thead>
				<tr style=" background-color: #2ABF9E; color: #FFFFFF;">
					<th style="width: 42px;">Phase</th>
					<th style="width: 56px;">Days</th>
					<th style="width: 96px;">Date</th>
					<th style="width: 76px;">%</th>
					<th style="width: 126px;">Amount</th>
					<th style="width: 284px;">Remark</th>
					<th><i class="icon-minus-circled-1" title='Delete'></i></th>
				</tr>
			</thead>
			<tbody id="invoicing_tbody_list"></tbody>
		</table>
	</div>
	<div class="modal-footer alignright">
	    <a id="invoicing_btn_ok" class="btn btn-blue">OK</a>
	    <a class="btn btn-red" data-dismiss="modal">CLOSE</a>
	</div>
</div>
<div class="modal hide fade" id="sales_modal">
	<div class="modal-header bg-color light-green aligncenter">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    <h1>Sales Representative</h1>
	</div>
	<div class="modal-body">
		<table class="table table-striped table-condensed table_select">
			<thead>
				<tr style=" background-color: #2ABF9E; color: #FFFFFF;">
					<th style="width: 15%;">Emp. No.</th>
					<th style="width: 85%;">Name</th>
				</tr>
			</thead>
			<tbody id="sales_tbody_list"></tbody>
		</table>
	</div>
	<div class="modal-footer alignright">
	    <a class="btn btn-red" data-dismiss="modal">CLOSE</a>
	</div>
</div>
<div class="modal hide fade" id="status_modal">
	<div class="modal-header bg-color light-green aligncenter">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    <h1>Status</h1>
	</div>
	<div class="modal-body"><h5>
		<div id="status_alert" class="alert alert-error" style="display: none;">
        	กรุณาเลือกวันที่และระบุรายละเอียด
        </div>
		<table style="width: 100%;">
			<tbody>
				<tr>
					<td class="text-right" style="width: 20%;">Date&nbsp;</td>
					<td><input id="status_inp_eid" type="hidden"><input id="status_inp_date" class="form_date" style="width: 80px;" type="text" placeholder="choose date"></td>
				</tr>
				<tr>
					<td class="text-right" style="width: 20%;">Description&nbsp;</td>
					<td><textarea id="status_inp_desc" class="span4"></textarea></td>
				</tr>
			</tbody>
		</table>
	</h5></div>
	<div class="modal-footer alignright">
	    <a id="status_btn_ok" class="btn btn-blue">OK</a>
	    <a class="btn btn-red" data-dismiss="modal">CLOSE</a>
	</div>
</div>
<div class="modal hide fade" id="deptsup_modal">
	<div class="modal-header bg-color light-green aligncenter">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    <h1>Department support</h1>
	</div>
	<div class="modal-body" style="padding-left: 100px;">
		<table style="width: 85%;">
			<tbody id="deptsup_tbody_list"></tbody>
		</table>
	</div>
	<div class="modal-footer alignright">
	    <a id="deptsup_btn_ok" class="btn btn-blue">OK</a>
	    <a class="btn btn-red" data-dismiss="modal">CLOSE</a>
	</div>
</div>
<div class="modal hide fade" id="help_modal" style="width: 800px; margin-left: -400px;">
	<div class="modal-header bg-color light-green aligncenter">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    <h1>หลักการตั้งชื่อโครงการ</h1>
	</div>
	<div class="modal-body" style="padding:20px 40px;">
		<u><b>หลักการคร่าวๆ ประกอบด้วย 3 ส่วนหลักๆ ได้แก่</b></u><br/>
		1.    ชื่อย่อหน่วยงาน (ภาษาอังกฤษ) เช่น MEA, NHA<br/>
		2.    ชื่อโครงการ<br/>
		3.    ปีงบประมาณ โดยหากมี<font color="#FF0000">เครื่องหมาย *** หมายถึง เป็นโครงการที่เราเสนอให้กับลูกค้า ยังไม่ได้รับการอนุมัติงบประมาณจากหน่วยงาน</font><br/><br/>
		<u><b>ตัวอย่าง</b></u><br/>
		NHA-DHDS : GIS 57 (งบ 57) <br/>
		NHA-DHDS : โครงการพัฒนาโปรแกรมประยุกต์บน Mobile (งบประมาณเหลือจ่ายปี 2554) <br/>
		DIW : GIS กรมโรงงาน (งบ 57) <br/>
		NHA : ระบบฐานข้อมูลภูมิสารสนเทศอาคารสิ่งปลูกสร้างรุกล้ำเส้นทางน้ำและที่ดินแปลงว่างสนับสนุนคณะกรรมการบริหารจัดการนำและอุทกภัย (กบอ.) (งบ 57) <br/>
		NHA-DHDS : Suitable Area (งบ 57)  <br/>
		IEAT : Maptaput (Pilot) (งบ 57) <br/>
		SMOIT : โครงการจัดทำระบบสารสนเทศภูมิศาสตร์เพื่อบริหารจัดการโรงงานอุตสาหกรรม (งบ 58***) <br/>
		NEP : จัดหางานให้คนพิการที่ว่างงาน (งบ 58***) <br/>
	</div>
	<div class="modal-footer alignright">
	    <a class="btn btn-red" data-dismiss="modal">CLOSE</a>
	</div>
</div>
<div class="modal hide fade" id="product_modal" style="width: 800px; margin-left: -400px;">
	<div class="modal-header bg-color light-green aligncenter">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    <h1 id="product_title"></h1>
	</div>
	<div class="modal-body" style="padding-bottom: 0px;">
		<span id="product_amt" class="label label-inverse pull-left" title="Amount of results" style="font-size: 20px; padding: 10px; margin-top: 14px; cursor: default;">0</span>
		<h4 class="pull-right">
			<input id="product_inp_brand" type="hidden">
			<select id="product_slc_group" class="span3"></select>
			<input id="product_inp_keywords" class="span3" type="text" style="margin-bottom: 0px; height: 18px;" placeholder="Search Product">
			<i id="product_btn_search" class="i_btn icon-search-3"></i>
		</h4>
	</div>
	<div class="modal-body" style="padding-top: 0px; height: 250px;">
		<table class="table table-striped table-condensed table_select">
			<thead>
				<tr style=" background-color: #2ABF9E; color: #FFFFFF;">
					<th style="width: 25%;">Group</th>
					<th style="width: 75%;">Product</th>
				</tr>
			</thead>
			<tbody id="product_tbody_list"></tbody>
		</table>
	</div>
	<div class="modal-footer alignright">
	    <a class="btn btn-red" data-dismiss="modal">CLOSE</a>
	</div>
</div>
<div class="modal hide fade" id="quantity_modal" style="width: 400px; margin-left: -200px; top: 30%;">
	<div class="modal-header bg-color light-green aligncenter">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    <h1 id="quantity_title"></h1>
	</div>
	<div class="modal-body">
		<input id="quantity_inp_eid" type="hidden">
		<input id="quantity_inp_id" type="hidden">
		<h5 id="quantity_product"></h5>
	</div>
	<div class="modal-footer" style="text-align: left;">
		<h5 style="display: inline-block; margin: 0px; width: 210px;">Quantity <input id="quantity_inp_quantity" type="text" class="span1 nNumber text-center" style="height: 25px;"></h5>
	    <a id="quantity_btn_ok" class="btn btn-blue">OK</a>
	    <a class="btn btn-red" data-dismiss="modal">CLOSE</a>
	</div>
</div>
	<script src="js/library/jquery/jquery-1.9.1"></script>
	<script src="js/library/jquery/jquery-ui-1.10.3.custom.js"></script>
	<script src="js/library/masonry/jquery.masonry.min.js"></script>
	<script src="js/library/flexslider/jquery.flexslider-min.js"></script>
	<script src="js/library/dataTables/jquery.dataTables.min.js"></script>
	<script src="js/library/fullcalendar/fullcalendar.min.js"></script>
	<script src="js/library/fullcalendar/gcal.js"></script>
	<script src="js/library/forms/jquery.hotkeys.js"></script>
	<script src="js/library/forms/bootstrap-wysiwyg.js"></script>
	<script src="js/library/forms/picker.js"></script>
	<script src="js/library/forms/picker.date.js"></script>
	<script src="js/library/forms/picker.time.js"></script>
	<script src="js/library/forms/jquery.bootstrap.wizard.min.js"></script>
	<script src="js/library/charts/jquery.flot.js"></script>
	<script src="js/library/charts/jquery.flot.resize.js"></script>
	<script src="js/library/charts/jquery.flot.stack.js"></script>
	<script src="js/library/charts/jquery.flot.pie.min.js"></script>
	<script src="js/library/charts/jquery.sparkline.min.js"></script>
	<script src="js/library/syntaxHighlighter/shCore.js"></script>
	<script src="js/library/syntaxHighlighter/shBrushCss.js"></script>
	<script src="js/library/syntaxHighlighter/shBrushXml.js"></script>
	<script src="js/library/syntaxHighlighter/shBrushJScript.js"></script>
	<script src="js/library/jquery.number.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="js/styler/custom.js"></script>

	<script type="text/javascript">
	    $(document).ready(function(){
//------------------------------------------------- Function ------------------------------------------------
			function numFormat(x) {
				var parts = x.toString().split(".");
			    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			    return parts.join(".");
			}
			function nNumFormat(x) {
				var parts = x.toString().split(".");
			    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			    return parts[0];
			}
			function calSIProj() {
				hw = sw = app = data = false;
				$("#tbody_description").children().each(function() {
					tempP = $($(this).find("p")[0]).text();
					if (tempP == "HW") {
						hw = true;
					} else if (tempP == "SW") {
						sw = true;
					} else if (tempP == "Application") {
						app = true;
					} else if (tempP == "Data") {
						data = true;
					}
				});
				(hw && sw && app && data) ? $("#siproject").text("SI Project") : $("#siproject").text("Non SI Project");
			}
			function calSignProj() {
				if (parseInt($("#inp_potentialp").val().replace(/%/g, "")) >= 50 && parseFloat($("#inp_inco_evat").val()) >= 5000000) {
    				$("#sigproject").text("Significant Project");
    			} else {
    				$("#sigproject").text("Non Significant Project");
    			}
			}
			function calEndCont() {
				if ($("#inp_signDate").val() != "") {
					slcdate = $("#inp_signDate").datepicker('getDate', '+1d');
					if ($("#inp_pDuration").val() != "") {
						slcdate.setDate(slcdate.getDate() + parseInt($("#inp_pDuration").val()));
					}
					$("#inp_endCont").val(("0" + slcdate.getDate()).slice(-2) + "/" + ("0" + (slcdate.getMonth() + 1)).slice(-2) + "/" + slcdate.getFullYear());

					slcdate = $("#inp_signDate").datepicker('getDate', '+1d');
					if ($("#inp_dPDays").val() != "") {
						slcdate.setDate(slcdate.getDate() + parseInt($("#inp_dPDays").val()));
					}
					$("#inp_dPDate").val(("0" + slcdate.getDate()).slice(-2) + "/" + ("0" + (slcdate.getMonth() + 1)).slice(-2) + "/" + slcdate.getFullYear());
				} else {
					$("#inp_endCont").val("");
					$("#inp_dPDate").val("");
				}
			}
			function saveSalesForecast(sfid) {
				IDForecast = sfid;
				FCYear = new Date().getFullYear().toString();
				PEContractNo = $("#inp_contractpe").val();
				EContract = $("#inp_contractec").val();
				Project = $("#inp_contractproj").val();
				if ($("#inp_custid").val() != "" && $("#inp_custid").val() != "0") {
					CustID = $("#inp_custid").val();
					title = "";
					CustName = "";
					custsurname = "";
					division = "";
					department = "";
					CustOrg = "";
				} else {
					CustID = "0";
					title = $("#inp_custtname").val();
					CustName = $("#inp_custfname").val();
					custsurname = $("#inp_custlname").val();
					division = $("#inp_custdiv").val();
					department = $("#inp_custdep").val();
					CustOrg = $("#inp_custorg").val();
				}
				quotno = $("#importedFrom").text().replace("Imported from quotation: ", "");
				Potential = $("#inp_potentialp").val().replace(/%/g, "");
				PotentialDesc = $("#inp_potentialt").val();
				Progress = $("#inp_progressp").val().replace(/%/g, "");
				ProgressDesc = $("#inp_progresst").val();
				ESRIisTOR = $("#chk_esri").prop("checked") ? 1 : 0;
				Education = $("#chk_edu").prop("checked") ? 1 : 0;
				TargetProject = $("#chk_proj").prop("checked") ? 1 : 0;
				TargetSpecialProject = $("#chk_sproj").prop("checked") ? 1 : 0;
				TargetIncome = $("#inp_inco_evat").val() != "" ? $("#inp_inco_evat").val() : 0;
				TargetBudget = $("#inp_budg_evat").val() != "" ? $("#inp_budg_evat").val() : 0;
				TargetCost = $("#inp_cost_evat").val() != "" ? $("#inp_cost_evat").val() : 0;
				if ($("#inp_biddDate").val() != "") {
					TimeFrameBidingDate = $("#inp_biddDate").val().split("/");
					TimeFrameBidingDate = "'" + TimeFrameBidingDate[2] + "-" + TimeFrameBidingDate[1] + "-" + TimeFrameBidingDate[0] + "'";
				} else {
					TimeFrameBidingDate = "NULL";
				}
				if ($("#inp_signDate").val() != "") {
					TimeFrameContractSigndate = $("#inp_signDate").val().split("/");
					TimeFrameContractSigndate = "'" + TimeFrameContractSigndate[2] + "-" + TimeFrameContractSigndate[1] + "-" + TimeFrameContractSigndate[0] + "'";
				} else {
					TimeFrameContractSigndate = "NULL";
				}
				TimeFrameProjectDuration = $("#inp_pDuration").val();
				if ($("#inp_endCont").val() != "") {
					TimeFrameDeliveryDate = $("#inp_endCont").val().split("/");
					TimeFrameDeliveryDate = "'" + TimeFrameDeliveryDate[2] + "-" + TimeFrameDeliveryDate[1] + "-" + TimeFrameDeliveryDate[0] + "'";
				} else {
					TimeFrameDeliveryDate = "NULL";
				}
				chkDP = $("#chk_downPayment").prop("checked") ? 1 : 0;
				PercentAmount = $("#inp_downPayment").val();
				InvoiceAmount = $("#inp_dPBaht").val() != "" ? $("#inp_dPBaht").val() : 0;
				InvDuration = $("#inp_dPDays").val();
				if ($("#inp_dPDate").val() != "") {
					Invdate = $("#inp_dPDate").val().split("/");
					Invdate = "'" + Invdate[2] + "-" + Invdate[1] + "-" + Invdate[0] + "'";
				} else {
					Invdate = "NULL";
				}
				Book = 0.00;
				Forward = 0.00;
				Forward2 = 0.00;
				Forward3 = 0.00;
				tempDate = new Date().getFullYear();
				$("#tbody_bank").children().each(function() {
					tempYear = parseInt($($(this).find("p")[1]).text());
					tempPrice = parseFloat($($(this).find("p")[2]).text().replace(/,/g, ""));
					if (tempYear <= tempDate) {
						Book += tempPrice;
					} else if ((tempYear - tempDate) == 1) {
						Forward += tempPrice;
					} else if ((tempYear - tempDate) == 2) {
						Forward2 += tempPrice;
					} else if ((tempYear - tempDate) == 3) {
						Forward3 += tempPrice;
					}
				});
				Remark = $("#txa_salesremark").val();
				SaleID = $("#inp_salesid").val();
				SaleRepresentative = $("#inp_salesname").val();
				SignificantProjects = $("#sigproject").text() == "Significant Project" ? 1 : 0;
				SIProjects = $("#siproject").text() == "SI Project" ? 1 : 0;
				VerticalMarketGroup = $("#slc_verticalmarket").val();
				UserUpdated = _loginID;
				ProjectBy = $("#slc_salesproj").val();
				jsonDesc = "";
				jsonInvo = "";
				jsonStat = "";
				jsonDepa = "";
				jsonEsriP = "";
				jsonLeicaP = "";
				jsonGarminP = "";
				jsonNostraP = "";
				if ($("#tbody_description").children().length > 0) {
					itemsArray = [];
					$("#tbody_description").children().each(function() {
						eachitemArray = [];
						tempP = $(this).find("p");
						switch ($(tempP[0]).text()) {
			    			case "Site Prep": tempType = 1; break;
			    			case "HW": tempType = 2; break;
			    			case "SW": tempType = 3; break;
			    			case "Data": tempType = 4; break;
			    			case "Application": tempType = 5; break;
			    			case "On Site": tempType = 6; break;
			    			case "Training": tempType = 7; break;
			    			case "GPS": tempType = 8; break;
			    			case "SURVEY EQUIPMENT": tempType = 9; break;
			    			case "SYSTEMS": tempType = 10; break;
			    			case "Other": tempType = 11; break;
			    			case "CONSULT": tempType = 12; break;
			    			case "ESRI": tempType = 13; break;
			    			case "Leica": tempType = 14; break;
			    			case "Garmin": tempType = 15; break;
			    			case "NOSTRA": tempType = 16; break;
			    			default: tempType = 0; break;
			    		}
						eachitemArray.push(tempType);
						eachitemArray.push($(tempP[1]).text());
						eachitemArray.push($(tempP[2]).text() == "" ? "0.00" : $(tempP[2]).text().replace(/,/g, ""));
						itemsArray.push(eachitemArray);
					});
					jsonDesc = JSON.stringify(itemsArray);
				}
				if ($("#tbody_invoicing").children().length > 0) {
					itemsArray = [];
					$("#tbody_invoicing").children().each(function() {
						eachitemArray = [];
						tempP = $(this).find("p");
						eachitemArray.push($(tempP[0]).text());
						eachitemArray.push($(tempP[1]).text());
						if ($(tempP[2]).text() != "") {
							tempDate = $(tempP[2]).text().split("/");
							tempDate = "'" + tempDate[2] + "-" + tempDate[1] + "-" + tempDate[0] + "'";
						} else {
							tempDate = "NULL";
						}
						eachitemArray.push(tempDate);
						eachitemArray.push($(tempP[4]).text().replace(/,/g, ""));
						eachitemArray.push($(tempP[3]).text());
						eachitemArray.push($(tempP[5]).text());
						itemsArray.push(eachitemArray);
					});
					jsonInvo = JSON.stringify(itemsArray);
				}
				if ($("#tbody_status").children().length > 0) {
					itemsArray = [];
					$("#tbody_status").children().each(function() {
						eachitemArray = [];
						tempP = $(this).find("p");
						if ($(tempP[0]).text() != "") {
							tempDate = $(tempP[0]).text().split("/");
							tempDate = "'" + tempDate[2] + "-" + tempDate[1] + "-" + tempDate[0] + "'";
						} else {
							tempDate = "NULL";
						}
						eachitemArray.push(tempDate);
						eachitemArray.push($(tempP[1]).text());
						itemsArray.push(eachitemArray);
					});
					jsonStat = JSON.stringify(itemsArray);
				}
				if ($("#tbody_deptsup").children().length > 0) {
					itemsArray = [];
					$("#tbody_deptsup").children().each(function() {
						eachitemArray = [];
						tempP = $(this).find("p");
						eachitemArray.push($(tempP[0]).text());
						itemsArray.push(eachitemArray);
					});
					jsonDepa = JSON.stringify(itemsArray);
				}
				if ($("#tbody_esri").children().length > 0) {
					itemsArray = [];
					$("#tbody_esri").children().each(function() {
						eachitemArray = [];
						tempP = $(this).find("p");
						eachitemArray.push($(tempP[0]).text());
						eachitemArray.push($(tempP[1]).text());
						eachitemArray.push($(tempP[2]).text());
						itemsArray.push(eachitemArray);
					});
					jsonEsriP = JSON.stringify(itemsArray);
				}
				if ($("#tbody_leica").children().length > 0) {
					itemsArray = [];
					$("#tbody_leica").children().each(function() {
						eachitemArray = [];
						tempP = $(this).find("p");
						eachitemArray.push($(tempP[0]).text());
						eachitemArray.push($(tempP[1]).text());
						eachitemArray.push($(tempP[2]).text());
						itemsArray.push(eachitemArray);
					});
					jsonLeicaP = JSON.stringify(itemsArray);
				}
				if ($("#tbody_garmin").children().length > 0) {
					itemsArray = [];
					$("#tbody_garmin").children().each(function() {
						eachitemArray = [];
						tempP = $(this).find("p");
						eachitemArray.push($(tempP[0]).text());
						eachitemArray.push($(tempP[1]).text());
						eachitemArray.push($(tempP[2]).text());
						itemsArray.push(eachitemArray);
					});
					jsonGarminP = JSON.stringify(itemsArray);
				}
				if ($("#tbody_nostra").children().length > 0) {
					itemsArray = [];
					$("#tbody_nostra").children().each(function() {
						eachitemArray = [];
						tempP = $(this).find("p");
						eachitemArray.push($(tempP[0]).text());
						eachitemArray.push($(tempP[1]).text());
						eachitemArray.push($(tempP[2]).text());
						itemsArray.push(eachitemArray);
					});
					jsonNostraP = JSON.stringify(itemsArray);
				}
				$.ajax({
					type: "POST",
					dataType: "json",
					url: "AJAX/saveForecast.php",
					data: {
						IDForecast: IDForecast,
						FCYear: FCYear,
						PEContractNo: PEContractNo,
						EContract: EContract,
						Project: Project,
						CustID: CustID,
						CustName: CustName,
						CustOrg: CustOrg,
						Potential: Potential,
						PotentialDesc: PotentialDesc,
						Progress: Progress,
						ProgressDesc: ProgressDesc,
						ESRIisTOR: ESRIisTOR,
						Education: Education,
						TargetProject: TargetProject,
						TargetSpecialProject: TargetSpecialProject,
						TargetIncome: TargetIncome,
						TargetBudget: TargetBudget,
						TargetCost: TargetCost,
						TimeFrameBidingDate: TimeFrameBidingDate,
						TimeFrameContractSigndate: TimeFrameContractSigndate,
						TimeFrameProjectDuration: TimeFrameProjectDuration,
						TimeFrameDeliveryDate: TimeFrameDeliveryDate,
						chkDP: chkDP,
						PercentAmount: PercentAmount,
						InvoiceAmount: InvoiceAmount,
						InvDuration: InvDuration,
						Invdate: Invdate,
						Book: Book,
						Forward: Forward,
						Forward2: Forward2,
						Forward3: Forward3,
						Remark: Remark,
						SaleID: SaleID,
						SaleRepresentative: SaleRepresentative,
						SignificantProjects: SignificantProjects,
						SIProjects: SIProjects,
						VerticalMarketGroup: VerticalMarketGroup,
						UserUpdated: UserUpdated,
						ProjectBy: ProjectBy,
						title: title,
						custsurname: custsurname,
						division: division,
						department: department,
						quotno: quotno
					},
					success: function(json) {
						if (json != "failed" && json != "") {
							var id_forecast =json;
							$.ajax({
								type: "POST",
								dataType: "json",
							    url: "AJAX/saveSalesForecastDetails.php",
							    data: {
							    	IDForecast: json,
							    	UserUpdated: _loginID,
							    	jsonDesc: jsonDesc,
									jsonInvo: jsonInvo,
									jsonStat: jsonStat,
									jsonDepa: jsonDepa,
									jsonEsriP: jsonEsriP,
									jsonLeicaP: jsonLeicaP,
									jsonGarminP: jsonGarminP,
									jsonNostraP: jsonNostraP
							    },
							    success: function(json) {
									if (json == "successed") {
									window.location = "sendmail.php?IDForecast="+ id_forecast + "&page=<?=$page?>" ;
										/*alert("ทำการบันทึกข้อมูลเรียบร้อย");
									<?
										if ($page) {
									?>
										window.location = "<?=$page?>.php";
									<?
										} else {
									?>
										window.location = "index.php";
									<?
										}
									?>*/
									} else {
										alert("เกิดปัญหาระหว่างการบันทึกกรุณาตรวจสอบข้อมูลที่กรอกลองใหม่อีกครั้ง");
										$("#loading").hide();
									}
								},
								error: function() {
									alert("เกิดปัญหาในการบันทึกกรุณาลองใหม่อีกครั้ง");
									$("#loading").hide();
								}
							});
						} else {
							alert("เกิดปัญหาระหว่างการบันทึกกรุณาตรวจสอบข้อมูลที่กรอกลองใหม่อีกครั้ง");
							$("#loading").hide();
						}
					},
					error: function() {
						alert("เกิดปัญหาในการบันทึกกรุณาลองใหม่อีกครั้ง");
						$("#loading").hide();
					}
				});
			}
//-----------------------------------------------------------------------------------------------------------

//------------------------------------------------- Initial -------------------------------------------------
			var _loginID = "<?=$_COOKIE['Ses_ID']?>";
			var _lastStatus = "";
			var _lastPotential, _lastProgress, _lastIncome, _lastBidding, _lastSign = "";
	    	var old_inco_ivat, old_inco_evat, old_biddDate, old_signDate, old_pDuration, old_downPayment, old_dPBaht;
	    	var description_type = ["HW", "SW", "Application", "Data", "Site Prep", "On Site", "Training", "Other"];
	    	var div_list = new Array();
			var dep_list = new Array();
			var org_list = new Array();
		<?
			$conn = mssql_connect("157.179.28.116", "sa", "dioro@4321");
			mssql_select_db("NewCISTest",  $conn);
			$sqlStr = "SELECT division, department, organization FROM Organizations WHERE division != '' AND division != 'n/a' GROUP BY division, department, organization";
			$query = mssql_query($sqlStr, $conn);
			while ($obj = mssql_fetch_object($query)) {
				echo "div_list.push('".replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->division))." | ".replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->department))." | ".replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->organization))."');";
			}
			$sqlStr = "SELECT thaidivision, thaidepartment, thaioganization FROM Organizations WHERE thaidivision != '' AND thaidivision != 'n/a' GROUP BY thaidivision, thaidepartment, thaioganization";
			$query = mssql_query($sqlStr, $conn);
			while ($obj = mssql_fetch_object($query)) {
				echo "div_list.push('".replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->thaidivision))." | ".replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->thaidepartment))." | ".replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->thaioganization))."');";
			}
			$sqlStr = "SELECT department, organization FROM Organizations WHERE department != '' AND department != 'n/a' GROUP BY department, organization";
			$query = mssql_query($sqlStr, $conn);
			while ($obj = mssql_fetch_object($query)) {
				echo "dep_list.push('".replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->department))." | ".replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->organization))."');";
			}
			$sqlStr = "SELECT thaidepartment, thaioganization FROM Organizations WHERE thaidepartment != '' AND thaioganization != 'n/a' GROUP BY thaidepartment, thaioganization";
			$query = mssql_query($sqlStr, $conn);
			while ($obj = mssql_fetch_object($query)) {
				echo "dep_list.push('".replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->thaidepartment))." | ".replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->thaioganization))."');";
			}
			$sqlStr = "SELECT DISTINCT organization FROM Organizations";
			$query = mssql_query($sqlStr, $conn);
			while ($obj = mssql_fetch_object($query)) {
				echo "org_list.push('".replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->organization))."');";
			}
			$sqlStr = "SELECT DISTINCT thaioganization FROM Organizations";
			$query = mssql_query($sqlStr, $conn);
			while ($obj = mssql_fetch_object($query)) {
				echo "org_list.push('".replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->thaioganization))."');";
			}
		?>
			$("#inp_custdiv").autocomplete({
				source: div_list
			});
			$("#inp_custdep").autocomplete({
				source: dep_list
			});
			$("#inp_custorg").autocomplete({
				source: org_list
			});
			$(".form_date").datepicker({
		        dateFormat: 'dd/mm/yy',
		        changeYear: true
	    	});
	    	$(".form_date").on("keydown", function(e) {
	    		switch (e.keyCode) {
	    			case 8: $(this).val(""); break;
	    			default: e.preventDefault(); break;
	    		}
	    	});
	    	$(".nNumber").number(true, 0);
	    	$(".number").number(true, 2);
	    	$(".form_date").css("cursor", "pointer");
	    	list_potential = [["100%", "มั่นใจว่าได้แน่"], ["75%", "มั่นใจ"], ["50%", "ไม่แน่ใจว่าจะได้โครงการหรือไม่"], ["25%", "พอมีโอกาส"], ["10%", "โอกาสน้อยมาก"]];
	    	list_progress = [["100%", "Sign date"], ["90%", "ประกาศผลแล้วรอเซ็นสัญญา"], ["80%", "ยื่นซองแล้ว"], ["70%", "Bidding date"], ["60%", "TOR Final"], ["50%", "ได้งบประมาณ"], ["40%", "ขั้นตอนการพิจารณาอนุมัติงบประมาณ"], ["30%", "ร่างโครงการ (ของบประมาณ)"], ["20%", "Presentation/Demo"], ["10%", "Build solution"], ["0%", "แพ้"], ["v", "ยกเลิกโครงการ"]];
	    	$(list_potential).each(function() {
	    		var item = this;
	    		tr = $('<tr><td>' + item[0] + '</td><td>' + item[1] + '</td></tr>');
	    		tr.on("click", function() {
	    			$("#inp_potentialp").val(item[0]);
	    			$("#inp_potentialt").val(item[1]);
	    			$("#potential_modal").modal("hide");
	    		});
	    		$("#potential_tbody_list").append(tr);
    		});
	    	$(list_progress).each(function() {
	    		var item = this;
	    		tr = $('<tr><td>' + item[0] + '</td><td>' + item[1] + '</td></tr>');
	    		tr.on("click", function() {
	    			$("#inp_progressp").val(item[0]);
	    			$("#inp_progresst").val(item[1]);
	    			$("#progress_modal").modal("hide");
	    		});
	    		$("#progress_tbody_list").append(tr);
    		});
			$.ajax({
    			type: "POST",
				dataType: "json",
		        url: "AJAX/listSalesRepresentative.php",
		        success: function(json) {
		        	$.each(json, function() {
	    				var item = this;
	    				tr = $('<tr><td>' + item[0] + '</td><td>' + item[1] + '</td></tr>');
	    				tr.on("click", function() {
			    			$("#inp_salesid").val(item[0]);
			    			$("#inp_salesname").val(item[1]);
			    			$.ajax({
			    				type: "POST",
								dataType: "json",
						        url: "AJAX/getSalesDepartment.php",
						        data: {
						        	emp: item[0]
						        },
						        success: function(json) {
						        	$("#slc_salesproj").val(json[0][0]);
								}
						    });
			    			$("#sales_modal").modal("hide");
			    		});
			    		if (parseInt(_loginID) == item[0]) tr.click();
			    		$("#sales_tbody_list").append(tr);
	    			});
		        },
				error: function() {
					alert("เกิดปัญหาในการโหลดข้อมูลกรุณารีเฟรชหน้าเว็บอีกครั้ง");
					$("#loading").hide();
				}
		    });
    		$.ajax({
    			type: "POST",
				dataType: "json",
		        url: "AJAX/listDepSupport.php",
		        success: function(json) {
					var count = 3;
					var tr = $('<tr><td style="width: 34%; vertical-align: top;"></td><td style="width: 33%; vertical-align: top;"></td><td style="width: 33%; vertical-align: top;"></td>');
					var td1 = '';
					var td2 = '';
					var td3 = '';
		        	$.each(json, function() {
	    				var item = this;
						if(count%3== 0){
							td1+='<label class="checkbox"><input type="checkbox" value="'+ item[0] +'">'+ item[0] +'</label>';
						}else if(count%3== 1){
							td2+='<label class="checkbox"><input type="checkbox" value="'+ item[0] +'">'+ item[0] +'</label>';
						}else{
							td3+='<label class="checkbox"><input type="checkbox" value="'+ item[0] +'">'+ item[0] +'</label>';
						}
						count++;
	    			});
					$(tr.find("td")[0]).append(td1);
	    			$(tr.find("td")[1]).append(td2);
					$(tr.find("td")[2]).append(td3);
					$("#deptsup_tbody_list").append(tr);
		        },
				error: function() {
					alert("เกิดปัญหาในการโหลดข้อมูลกรุณารีเฟรชหน้าเว็บอีกครั้ง");
					$("#loading").hide();
				}
		    });
		    if ($("#forecastid").text() == "New Forecast") {
	 			function addRegStatus() {
	 				currDate = new Date();
	 				var DayDate=(currDate.getDate() < 10) ? '0' + currDate.getDate() : currDate.getDate();
	 				var MonDate=((currDate.getMonth() + 1) < 10) ? '0' + (currDate.getMonth() + 1) : (currDate.getMonth() + 1);
		 			var tr = $('<tr class="warning"><td><p class="lead14">' + DayDate + '/' + MonDate + '/' + currDate.getFullYear() + '</p></td><td><p class="lead14">New Register</p></td><td class="text-center"></td><td class="text-center"></td>');
		 			var td_edit = $('<i class="i_btn icon-pencil-1" title="Edit" style="color: black;"></i>');
	    			var td_del = $('<i class="i_btn icon-minus-circled-1" title="Delete" style="color: red;"></i>');
		    		td_edit.on("click", function() {
		    			$("#status_inp_eid").val(tr.index());
		    			$("#status_inp_date").val($(tr.find("p")[0]).text());
		    			$("#status_inp_desc").val($(tr.find("p")[1]).text());
	    				$("#status_alert").hide();
	    				$("#status_modal").modal("show");
		    		});
		    		td_del.on("click", function() {
		    			if (window.confirm("ต้องการลบรายการนี้หรือไม่")) {
		    				tr.remove();
		    			}
		    		});
	    			$(tr.find("td")[2]).append(td_edit);
	    			$(tr.find("td")[3]).append(td_del);
		 			$("#tbody_status").append(tr);
	 			}
	 			addRegStatus();
		    }
//-----------------------------------------------------------------------------------------------------------

//------------------------------------------------- Event ---------------------------------------------------
	    	$("#btn_pEContract_del").on("click", function() {
				$("#inp_contractpe").val("");
				$("#inp_contractec").val("");
				//$("#inp_contractproj").val("");
				$("#inp_custtname").val("");
				$("#inp_custfname").val("");
				$("#inp_custlname").val("");
				$("#inp_custdiv").val("");
				$("#inp_custdep").val("");
				$("#inp_custorg").val("");
			});
			$("#btn_pEContract_modal").on("click", function() {
	    		$("#pEContract_inp_keywords").val("PE" + new Date().getFullYear().toString().substr(2,2));
	    		$("#searchPE").click();
	    		$("#pEContract_modal").modal("show");
	    	});
	    	$("#pEContract_inp_keywords").on("keydown", function(e) {
	    		if (e.keyCode == 13) $("#searchPE").click();
    		});
	    	$("#searchPE").on("click", function() {
	        	$("#pEContract_tbody_list").empty();
				$("#pEContract_amt").text("0");
				$("#loading").show();
	    		$.ajax({
	    			type: "POST",
					dataType: "json",
			        url: "AJAX/searchPEContract.php",
			        data: {
			        	keywordPE: $("#pEContract_inp_keywords").val(),
			        	sid: _loginID
			        },
			        success: function(json) {
			        	$("#pEContract_amt").text(json.length);
			        	$.each(json, function() {
		    				var item = this;
		    				tr = $('<tr><td>' + item[0] + '</td><td>' + item[1] + '</td><td>' + item[2] + '</td></tr>');
		    				tr.on("click", function() {
		    					$("#inp_contractpe").val("");
					        	$("#inp_contractec").val("");
				    			$("#inp_contractproj").val("");
					        	$("#inp_custtname").val("");
					        	$("#inp_custfname").val("");
					        	$("#inp_custlname").val("");
					        	$("#inp_custdiv").val("");
					        	$("#inp_custdep").val("");
					        	$("#inp_custorg").val("");
				    			$("#inp_contractpe").val(item[0]);
				    			$("#inp_contractproj").val(item[1] + ": " + item[2]);
				    			$.ajax({
				    				type: "POST",
									dataType: "json",
							        url: "AJAX/getPEContractDetails.php",
							        data: {
							        	pid: item[0]
							        },
							        success: function(json) {
							        	$("#inp_custfname").val(json[0][0]);
							        	$("#inp_custdep").val(json[0][1]);
							        	if (json[0][2] != "") {
								        	old_biddDate = $("#inp_biddDate").datepicker('getDate', '+1d');
								        	old_signDate = $("#inp_signDate").datepicker('getDate', '+1d');
							        		$("#inp_signDate").datepicker('setDate', new Date(json[0][2]));
								        	if ($("#tbody_invoicing").children().length > 0 && $("#inp_signDate").datepicker('getDate', '+1d') != old_signDate) {
								        		if (window.confirm("หากเปลี่ยน Sign date จำนวนวันและวันที่ใน Invoicing plan อาจเปลี่ยนโดยอัตโนมัติ\nยืนยันหรือไม่")) {
								        			slcdate = $("#inp_signDate").datepicker('getDate', '+1d');
													slcdate.setDate(slcdate.getDate() - 45);
													$("#inp_biddDate").datepicker('setDate', slcdate);
										    		calEndCont();
										    		if ($("#btn_invoicing_modal").click()) {
										    			$("#tbody_invoicing").empty();
										    			tr = '<tr class="error"><td><p class="lead14">Book</p></td><td><p class="lead14">' + $("#inp_signDate").datepicker('getDate', '+1d').getFullYear() + '</p></td><td><p class="lead14">' + numFormat(parseFloat($("#inp_inco_evat").val()).toFixed(2)) + '</p></td></tr>';
														$("#tbody_bank").empty();
														$("#tbody_bank").append(tr);
										    		}
										    		alert("คำเตือน: โปรดกด OK หลังการแก้ไขไม่เช่นนั้นข้อมูลใหม่นี้จะไม่ถูกบันทึก\nเนื่องจากคุณได้เปลี่ยนแปลงค่าที่ส่งผลต่อ Invoicing plan ดังนั้นระบบจำเป็นต้องลบรายการเดิมทั้งหมดเพื่อป้องกันข้อผิดพลาดของข้อมูล");
								        		} else {
								        			$("#inp_biddDate").datepicker('setDate', old_biddDate);
								        			$("#inp_signDate").datepicker('setDate', old_signDate);
								        		}
								        	} else {
									        	slcdate = $("#inp_signDate").datepicker('getDate', '+1d');
												slcdate.setDate(slcdate.getDate() - 45);
												$("#inp_biddDate").datepicker('setDate', slcdate);
												calEndCont();
								    			tr = '<tr class="error"><td><p class="lead14">Book</p></td><td><p class="lead14">' + ($("#inp_signDate").val() != "" ? $("#inp_signDate").datepicker('getDate', '+1d').getFullYear() : new Date().getFullYear()) + '</p></td><td><p class="lead14">' + numFormat(parseFloat($("#inp_inco_evat").val()).toFixed(2)) + '</p></td></tr>';
												$("#tbody_bank").empty();
												$("#tbody_bank").append(tr);
								        	}
							        	}
							        },
									error: function() {
										alert("เกิดปัญหาในการโหลดข้อมูล PEContract กรุณาเลือกใหม่อีกครับ");
										$("#loading").hide();
									}
							    });
								$.ajax({
									type: "POST",
									dataType: "json",
							        url: "AJAX/getEContract.php",
							        data: {
							        	pid: $("#inp_contractpe").val()
							        },
							        success: function(json) {
							        	if (json[0][0] != "") $("#inp_contractec").val(json[0][0]);
						        	},
									error: function() {
										alert("เกิดปัญหาในการโหลดข้อมูล E-Contract กรุณาเลือกใหม่อีกครับ");
										$("#loading").hide();
									}
							    });
				    			$("#pEContract_modal").modal("hide");
				    		});
				    		$("#pEContract_tbody_list").append(tr);
		    			});
						if (json.length == 0) {
							$("#pEContract_tbody_list").append($('<tr><td colspan="3" style="text-align: center;">ไม่พบข้อมูล</td></tr>'));
						}
						$("#loading").hide()
			        },
					error: function() {
						alert("เกิดปัญหาในการค้นหากรุณาลองใหม่อีกครั้ง");
						$("#loading").hide();
					}
			    });
	    	});
			$("#btn_eContract").on("click", function() {
				$("#loading").show();
				$.ajax({
					type: "POST",
					dataType: "json",
			        url: "AJAX/getEContract.php",
			        data: {
			        	pid: $("#inp_contractpe").val()
			        },
			        success: function(json) {
			        	if (json[0][0] != "") {
			        		$("#inp_contractec").val(json[0][0]);
			        	} else {
			        		$("#inp_contractec").val("");
			        		alert("ไม่พบ E-Contract");
			        	}
			        	$("#loading").hide();
		        	},
					error: function() {
						alert("เกิดปัญหาในการค้นหากรุณาลองใหม่อีกครั้ง");
						$("#loading").hide();
					}
			    });
			});
			$("#inp_custdiv").on("change", function() {
				split_org = $("#inp_custdiv").val().split(" | ");
				if (split_org.length > 1) {
					$("#inp_custdiv").val(split_org[0]);
					$("#inp_custdep").val(split_org[1]);
					$("#inp_custorg").val(split_org[2]);
				}
			});
			$("#inp_custdep").on("change", function() {
				split_org = $("#inp_custdep").val().split(" | ");
				if (split_org.length > 1) {
					$("#inp_custdep").val(split_org[0]);
					$("#inp_custorg").val(split_org[1]);
				}
			});
	    	$("#btn_customer_modal").on("click", function() {
	    		$("#customer_inp_keywords").val("");
	    		//$("#customer_btn_search").click();
	    		$("#customer_modal").modal("show");
	    	});
	    	$("#customer_inp_keywords").on("keydown", function(e) {
	    		if (e.keyCode == 13) $("#customer_btn_search").click();
    		});
	    	$("#customer_btn_search").on("click", function() {
	    		$("#loading").show();
	        	$("#customer_tbody_list").empty();
				$("#customer_amt").text("0");
	    		$.ajax({
	    			type: "POST",
					dataType: "json",
			        url: "AJAX/searchCustomer.php",
			        data: {
			        	keyword: $("#customer_inp_keywords").val()
			        },
			        success: function(json) {
			        	$("#customer_amt").text(json.length);
			        	$.each(json, function() {
		    				var item = this;
		    				tr = $('<tr><td>' + item[0] + '</td><td>' + item[1] + '</td><td>' + item[2] + '</td><td>' + item[3] + '</td><td>' + item[4] + '</td><td>' + item[5] + '</td></tr>');
		    				tr.on("click", function() {
					        	$("#inp_custid").val(item[6]);
					        	$("#inp_custno").val(item[7]);
					        	$("#inp_custini").val(item[8]);
					        	$("#inp_custtname").val(item[0]);
					        	$("#inp_custfname").val(item[1]);
					        	$("#inp_custlname").val(item[2]);
					        	$("#inp_custdiv").val(item[3]);
					        	$("#inp_custdep").val(item[4]);
					        	$("#inp_custorg").val(item[5]);
				    			$("#customer_modal").modal("hide");
		    				});
				    		$("#customer_tbody_list").append(tr);
	    				});
						if (json.length == 0) {
							$("#customer_tbody_list").append($('<tr><td colspan="6" style="text-align: center;">ไม่พบข้อมูล</td></tr>'));
						}
						$("#loading").hide();
			        },
					error: function() {
						alert("เกิดปัญหาในการค้นหากรุณาลองใหม่อีกครั้ง");
						$("#loading").hide();
					}
			    });
	    	});
	    	$("#btn_import_modal").on("click", function() {
	    		$("#import_slc_year").val("All")
	    		$("#import_slc_sales").val("All")
	    		$("#import_inp_keywords").val("");
	        	$("#import_tbody_qlist").empty();
				$("#import_tbody_plist").empty();
				$("#import_quotno").text("");
				$("#import_sales").val("");
				$("#import_date").val("");
				$("#import_customer").val("");
				$("#import_cid").val("");
				$("#import_cno").val("");
				$("#import_cini").val("");
				$("#import_ctitle").val("");
				$("#import_cname").val("");
				$("#import_csname").val("");
				$("#import_division").val("");
				$("#import_department").val("");
				$("#import_organization").val("");
	    		$(".import_pbody").hide();
	    		$("#import_btn_ok").hide();
	    		$("#import_btn_search").click();
	    		$("#import_modal").modal("show");
	    	});
	    	$("#import_inp_keywords").on("keydown", function(e) {
	    		if (e.keyCode == 13) $("#import_btn_search").click();
    		});
	    	$("#import_slc_year").on("change", function() {
	    		$("#import_btn_search").click();
    		});
	    	$("#import_slc_sales").on("change", function() {
	    		$("#import_btn_search").click();
    		});
	    	$("#import_btn_search").on("click", function() {
	    		$("#loading").show();
	        	$("#import_tbody_qlist").empty();
				$("#import_amt").text("0");
	    		$.ajax({
	    			type: "POST",
					dataType: "json",
			        url: "AJAX/searchQuotation.php",
			        data: {
			        	qyear: $("#import_slc_year").val(),
			        	salesid: $("#import_slc_sales").val(),
			        	keyword: $("#import_inp_keywords").val()
			        },
			        success: function(json) {
			        	$("#import_amt").text(json.length);
			        	$.each(json, function() {
		    				var item = this;
		    				tr = $('<tr><td>' + item[0] + '</td><td>' + item[1] + '</td><td>' + item[2] + item[3] + ' ' + item[4] + '</td><td>' + item[6] + '</td><td>' + item[7] + '</td><td>' + item[10] + '</td></tr>');
		    				tr.on("click", function() {
		    					$("#import_tbody_plist").empty();
		    					$("#import_quotno").text(item[0]);
		    					$("#import_sales").val(item[10]);
		    					$("#import_date").val(item[1]);
		    					$("#import_customer").val(item[2] + item[3] + ' ' + item[4]);
		    					$("#import_cid").val(item[11]);
		    					$("#import_cno").val(item[8]);
		    					$("#import_cini").val(item[9]);
		    					$("#import_ctitle").val(item[2]);
		    					$("#import_cname").val(item[3]);
		    					$("#import_csname").val(item[4]);
		    					$("#import_division").val(item[5]);
		    					$("#import_department").val(item[6]);
		    					$("#import_organization").val(item[7]);
		    					$(".import_pbody").show();
								$("#import_btn_ok").show();
		    					$.ajax({
		    						type: "POST",
									dataType: "json",
							        url: "AJAX/searchQuotationProducts.php",
							        data: {
							        	quotno: item[0]
							        },
							        success: function(json) {
							        	$.each(json, function() {
							        		var item = this;
	    									tr = $('<tr><td><input type="checkbox" style="margin-top: 0px;" checked><input type="hidden" value="' + item[6] + '"></td><td>' + item[0] + '</td><td>' + item[1] + '</td><td>' + item[2] + '</td><td>' + item[3] + '</td><td>' + parseInt(item[4]) + '</td><td style="text-align: right;">' + numFormat(parseFloat(item[5]).toFixed(2)) + '</td></tr>');
	    									$("#import_tbody_plist").append(tr);
						        		});
							        },
									error: function() {
										alert("เกิดปัญหาในการโหลดข้อมูลรายการสินค้ากรุณาเลือกรายการอีกครั้ง");
										$("#loading").hide();
									}
			    				});
		    				});
				    		$("#import_tbody_qlist").append(tr);
	    				});
						if (json.length == 0) {
							$("#import_tbody_qlist").append($('<tr><td colspan="7" style="text-align: center;">ไม่พบข้อมูล</td></tr>'));
						}
						$("#loading").hide();
			        },
					error: function() {
						alert("เกิดปัญหาในการค้นหากรุณาลองใหม่อีกครั้ง");
						$("#loading").hide();
					}
			    });
	    	});
			$("#import_btn_ok").on("click", function() {
				if (window.confirm("ต้องการนำเข้ารายการสินค้า ราคารวม และรายละเอียดลูกค้าหรือไม่")) {
					$("#importedFrom").text("Imported from quotation: " + $("#import_quotno").text());
					$("#inp_custid").val($("#import_cid").val());
					$("#inp_custno").val($("#import_cno").val());
					$("#inp_custini").val($("#import_cini").val());
					$("#inp_custtname").val($("#import_ctitle").val());
					$("#inp_custfname").val($("#import_cname").val());
					$("#inp_custlname").val($("#import_csname").val());
					$("#inp_custdiv").val($("#import_division").val());
					$("#inp_custdep").val($("#import_department").val());
					$("#inp_custorg").val($("#import_organization").val());
					$("#idescription_tbody_list").empty();
					slcProducts = {};
					allPrice = 0;
					$("#import_tbody_plist").children().each(function() {
						if ($($(this).find("input")[0]).prop("checked")) {
							tempType = $($(this).find("td")[1]).text();
							if (tempType == "") {
								if ($($(this).find("td")[2]).text() == "ESRI Product") {
									tempType = "ESRI";
								} else {
									tempType = $($(this).find("td")[2]).text();
								}
							}
							if (typeof slcProducts[tempType] == "undefined") {
								slcProducts[tempType] = [];
							}
							slcProducts[tempType].push($(this));
						}
					});
					$.each(slcProducts, function(key) {
						tempTr = $('<tr><td>' + key + '</td><td><input type="text" style="width: 434px; height: 18px; margin-bottom: 0px;"></td><td><input class="text-right" type="text" style="width: 150px; margin: 0px;"></td></tr>');
						tempTable = $('<table class="table table-striped table-condensed"><thead><tr style=" background-color: #2ABF9E; color: #FFFFFF;"><td>Description</td><td style="width: 30px;">Qty.</td></tr></thead></table>');
						tempTbody = $('<tbody></tbody>');
						totalPrice = 0.00;
						$.each(this, function() {
							var brand = $($(this).find("td")[2]).text();
							switch (brand) {
				    			case "ESRI Product":
				    				tbody = $("#tbody_esri");
				    				break;
				    			case "Leica":
				    				tbody = $("#tbody_leica");
				    				break;
				    			case "Garmin":
				    				tbody = $("#tbody_garmin");
				    				break;
				    			case "NOSTRA":
				    				tbody = $("#tbody_nostra");
				    				break;
			    				default: 
			    					tbody = $("#tbody_esri");
				    		}
			    			var td_edit = $('<i class="i_btn icon-pencil-1" title="Edit" style="color: black;"></i>');
			    			var td_del = $('<i class="i_btn icon-minus-circled-1" title="Delete" style="color: red;"></i>');
				    		var tr = $('<tr class="warning"><td><p class="lead14">' + $($(this).find("td")[4]).text() + '</p></td><td><p class="lead14">' + $($(this).find("td")[5]).text() + '</p><p style="display: none;">' + $($(this).find("input")[1]).val() + '</p></td><td class="text-center"></td><td class="text-center"></td></tr>');
				    		td_edit.on("click", function() {
				    			tempObj = tr.find("p");
				    			$("#product_inp_brand").val(brand);
			        			$("#quantity_title").text("Edit Product");
				    			$("#quantity_product").text($(tempObj[0]).text());
			        			$("#quantity_inp_id").val($(tempObj[2]).text());
			        			$("#quantity_inp_eid").val($(this).closest("tr").index());
			        			$("#quantity_inp_quantity").val($(tempObj[1]).text());
			        			$("#quantity_modal").modal("show");
				    		});
				    		td_del.on("click", function() {
				    			if (window.confirm("ต้องการลบรายการนี้หรือไม่")) {
				    				tr.remove();
				    			}
				    		});
			    			$(tr.find("td")[2]).append(td_edit);
			    			$(tr.find("td")[3]).append(td_del);
			    			tbody.append(tr);
							totalPrice += parseFloat($($(this).find("td")[6]).text().replace(/,/g, ""));
							tempPTr = $('<tr><td>' + $($(this).find("td")[4]).text() + '</td><td>' + $($(this).find("td")[5]).text() + '</td></tr>');
							tempTbody.append(tempPTr);
						});
						tempTable.append(tempTbody);
						$(tempTr.find("input")[1]).number(true, 2);
						$(tempTr.find("input")[1]).val(totalPrice);
						$(tempTr.find("td")[1]).append(tempTable);
						$("#idescription_tbody_list").append(tempTr);
						allPrice += totalPrice;
					});
					$("#inp_inco_evat").val(allPrice).blur();
					$("#idescription_modal").modal("show");
					$("#import_modal").modal("hide");
				}
			});
			$("#idescription_btn_ok").on("click", function() {
				$("#idescription_tbody_list").children().each(function() {
	    			var td_edit = $('<i class="i_btn icon-pencil-1" title="Edit" style="color: black;"></i>');
	    			var td_del = $('<i class="i_btn icon-minus-circled-1" title="Delete" style="color: red;"></i>');
	    			tempObj = $(this).find("input[type='text']");
		    		var tr = $('<tr class="warning"><td><p class="lead14">' + $($(this).find("td")[0]).text() + '</p></td><td><p class="lead14">' + $(tempObj[0]).val() + '</p></td><td><p class="lead14 text-right">' + numFormat(parseFloat($(tempObj[1]).val() != "" ? $(tempObj[1]).val() : "0.00").toFixed(2)) + '</p></td><td class="text-center"></td><td class="text-center"></td></tr>');
		    		td_edit.on("click", function() {
		    			tempObj = tr.find("p");
	        			$("#editdescription_inp_eid").val($(this).closest("tr").index());
	        			$("#editdescription_slc_type").val($(tempObj[0]).text());
	        			$("#editdescription_inp_description").val($(tempObj[1]).text());
	        			$("#editdescription_inp_price").val($(tempObj[2]).text());
		    			$("#editdescription_modal").modal("show");
		    		});
		    		td_del.on("click", function() {
		    			if (window.confirm("ต้องการลบรายการนี้หรือไม่")) {
		    				tr.remove();
		    				calSIProj();
		    			}
		    		});
	    			$(tr.find("td")[3]).append(td_edit);
	    			$(tr.find("td")[4]).append(td_del);
	    			$("#tbody_description").append(tr);
	    			calSIProj();
				});
				$("#idescription_modal").modal("hide");
			});
			$("#btn_description_modal").on("click", function() {
				$("#description_tbody_list").empty();
				tempArr = [];
				tempArr.push.apply(tempArr, description_type);
				$("#tbody_description").children().each(function() {
					tempIndx = tempArr.indexOf($($(this).find("p")[0]).text());
					if (tempIndx >= 0) tempArr.splice(tempIndx, 1);
				});
				$.each(tempArr, function() {
					tr = $('<tr><td style="vertical-align: middle;"><label class="checkbox" style="margin-bottom: 0px;"><input type="checkbox" value="' + this + '">' + this + '</label></td><td><input type="text" style="width: 434px; height: 18px; margin-bottom: 0px;"></td><td><input class="text-right" type="text" style="width: 150px; margin: 0px;"></td></tr>');
					$(tr.find("input")[2]).number(true, 2);
					$("#description_tbody_list").append(tr);
				});
				$("#description_modal").modal("show");
			});
			$("#description_btn_ok").on("click", function() {
				$("#description_tbody_list").find("input[type='checkbox']").each(function() {
					if ($(this).prop("checked")) {
		    			var td_edit = $('<i class="i_btn icon-pencil-1" title="Edit" style="color: black;"></i>');
		    			var td_del = $('<i class="i_btn icon-minus-circled-1" title="Delete" style="color: red;"></i>');
			    		var tr = $('<tr class="warning"><td><p class="lead14">' + $(this).val() + '</p></td><td><p class="lead14">' + $($(this).closest("tr").find("input[type='text']")[0]).val() + '</p></td><td><p class="lead14 text-right">' + numFormat(parseFloat($($(this).closest("tr").find("input[type='text']")[1]).val() != "" ? $($(this).closest("tr").find("input[type='text']")[1]).val() : "0.00").toFixed(2)) + '</p></td><td class="text-center"></td><td class="text-center"></td></tr>');
			    		td_edit.on("click", function() {
			    			tempObj = tr.find("p");
		        			$("#editdescription_inp_eid").val($(this).closest("tr").index());
		        			$("#editdescription_slc_type").val($(tempObj[0]).text());
		        			$("#editdescription_inp_description").val($(tempObj[1]).text());
		        			$("#editdescription_inp_price").val($(tempObj[2]).text());
			    			$("#editdescription_modal").modal("show");
			    		});
			    		td_del.on("click", function() {
			    			if (window.confirm("ต้องการลบรายการนี้หรือไม่")) {
			    				tr.remove();
			    				calSIProj();
			    			}
			    		});
		    			$(tr.find("td")[3]).append(td_edit);
		    			$(tr.find("td")[4]).append(td_del);
		    			$("#tbody_description").append(tr);
					}
				});
    			calSIProj();
				$("#description_modal").modal("hide");
			});
			$("#editdescription_btn_ok").on("click", function() {
				tempObj = $($("#tbody_description").children()[$("#editdescription_inp_eid").val()]).find("p");
				$(tempObj[0]).text($("#editdescription_slc_type").val());
				$(tempObj[1]).text($("#editdescription_inp_description").val());
				$(tempObj[2]).text(numFormat(parseFloat($("#editdescription_inp_price").val() != "" ? $("#editdescription_inp_price").val() : "0.00").toFixed(2)));
				$("#editdescription_modal").modal("hide");
				calSIProj();
			});
	    	$("#btn_potential_modal").on("click", function() {
	    		$("#potential_modal").modal("show");
	    	});
	    	$("#btn_progress_modal").on("click", function() {
	    		$("#progress_modal").modal("show");
	    	});
	    	$("#btn_hPotential_modal").on("click", function() {
	    		$("#hPotential_modal").modal("show");
	    	});
	    	$("#btn_hProgress_modal").on("click", function() {
	    		$("#hProgress_modal").modal("show");
	    	});
	    	$("#inp_biddDate").on("change", function() {
	    		if ($("#inp_biddDate").val() != "" && $("#inp_signDate").val() == "") {
		    		slcdate = $("#inp_biddDate").datepicker('getDate', '+1d');
					slcdate.setDate(slcdate.getDate() + 45);
					$("#inp_signDate").datepicker('setDate', slcdate);
					calEndCont();
	    		}
	    	});
	    	$("#inp_signDate").on("change", function() {
	    		if ($("#tbody_invoicing").children().length > 0 && $("#inp_signDate").datepicker('getDate', '+1d') != old_signDate) {
	    			if (window.confirm("หากเปลี่ยน Sign date จำนวนวันและวันที่ใน Invoicing plan อาจเปลี่ยนโดยอัตโนมัติ\nยืนยันหรือไม่")) {
		    			if ($("#inp_signDate").val() != "" && $("#inp_biddDate").val() == "") {
				    		slcdate = $("#inp_signDate").datepicker('getDate', '+1d');
							slcdate.setDate(slcdate.getDate() - 45);
							$("#inp_biddDate").datepicker('setDate', slcdate);
			    		}
			    		calEndCont();
			    		if ($("#btn_invoicing_modal").click()) {
			    			$("#tbody_invoicing").empty();
			    			tr = '<tr class="error"><td><p class="lead14">Book</p></td><td><p class="lead14">' + $("#inp_signDate").datepicker('getDate', '+1d').getFullYear() + '</p></td><td><p class="lead14">' + numFormat(parseFloat($("#inp_inco_evat").val()).toFixed(2)) + '</p></td></tr>';
							$("#tbody_bank").empty();
							$("#tbody_bank").append(tr);
			    		}
			    		alert("คำเตือน: โปรดกด OK หลังการแก้ไขไม่เช่นนั้นข้อมูลใหม่นี้จะไม่ถูกบันทึก\nเนื่องจากคุณได้เปลี่ยนแปลงค่าที่ส่งผลต่อ Invoicing plan ดังนั้นระบบจำเป็นต้องลบรายการเดิมทั้งหมดเพื่อป้องกันข้อผิดพลาดของข้อมูล");
	    			} else {
	    				$("#inp_signDate").datepicker('setDate', old_signDate);
		    			if ($("#inp_signDate").val() != "" && $("#inp_biddDate").val() == "") {
				    		slcdate = $("#inp_signDate").datepicker('getDate', '+1d');
							slcdate.setDate(slcdate.getDate() - 45);
							$("#inp_biddDate").datepicker('setDate', slcdate);
			    		}
			    		calEndCont();
	    			}
	    		} else {
	    			if ($("#inp_signDate").val() != "" && $("#inp_biddDate").val() == "") {
			    		slcdate = $("#inp_signDate").datepicker('getDate', '+1d');
						slcdate.setDate(slcdate.getDate() - 45);
						$("#inp_biddDate").datepicker('setDate', slcdate);
		    		}
		    		calEndCont();
		    		tr = '<tr class="error"><td><p class="lead14">Book</p></td><td><p class="lead14">' + ($("#inp_signDate").val() != "" ? $("#inp_signDate").datepicker('getDate', '+1d').getFullYear() : new Date().getFullYear()) + '</p></td><td><p class="lead14">' + numFormat(parseFloat($("#inp_inco_evat").val()).toFixed(2)) + '</p></td></tr>';
					$("#tbody_bank").empty();
					$("#tbody_bank").append(tr);
	    		}
	    	});
	    	$("#inp_signDate").on("keydown", function(e) {
	    		if (e.keyCode == 8) {
	    			if ($("#tbody_invoicing").children().length > 0) {
	    				e.preventDefault();
	    				alert("ไม่สามารถลบวันที่ได้หากต้องการลบกรุณาลบรายการ Invoicing plan ทั้งหมดก่อน");
	    				$("#inp_signDate").datepicker('setDate', old_signDate);
	    			} else {
	    				calEndCont();
			    		tr = '<tr class="error"><td><p class="lead14">Book</p></td><td><p class="lead14">' + new Date().getFullYear() + '</p></td><td><p class="lead14">' + numFormat(parseFloat($("#inp_inco_evat").val()).toFixed(2)) + '</p></td></tr>';
						$("#tbody_bank").empty();
						$("#tbody_bank").append(tr);
	    			}
	    		}
	    	});
	    	$("#inp_inco_evat").on("blur", function() {
				console.log('$("#inp_budg_evat").val()',$("#inp_budg_evat").val());
				console.log('$("#inp_inco_evat").val()',$("#inp_inco_evat").val());
				if (parseInt($("#inp_budg_evat").val()) < parseInt($("#inp_inco_evat").val())) {
	    			alert("Income ที่ระบุเกิน Budget กรุณาระบุ Income ใหม่");
	    			$("#inp_inco_ivat").val("0.00");
	    			$("#inp_inco_evat").val("0.00");
	    		}else if (parseInt($("#inp_cost_evat").val()) > parseInt($("#inp_inco_evat").val()) && parseInt($("#inp_cost_evat").val()) > 0) {
	    			alert("Cost ที่ระบุเกิน Income กรุณาระบุ Cost ใหม่");
	    			$("#inp_cost_ivat").val("0.00");
	    			$("#inp_cost_evat").val("0.00");
	    		}else{
					if ($(this).val() == "") $(this).val("0.00");
					if ($("#tbody_invoicing").children().length > 0 && $("#inp_inco_evat").val() != old_inco_evat) {
						if (window.confirm("หากเปลี่ยนแปลง Income ระบบจะคำนวนจำนวนเงินใน Invoicing plan ให้ใหม่อัตโนมัติ\nยืนยันหรือไม่")) {
							$("#inp_inco_ivat").val($("#inp_inco_evat").val() * 1.07);
							$("#inp_dPBaht").val($("#inp_inco_evat").val() * ($("#inp_downPayment").val() / 100));
							$("#inp_downPayment").val((100 / $("#inp_inco_evat").val()) * $("#inp_dPBaht").val());
							if ($("#btn_invoicing_modal").click()) {
								$("#tbody_invoicing").empty();
								tr = '<tr class="error"><td><p class="lead14">Book</p></td><td><p class="lead14">' + $("#inp_signDate").datepicker('getDate', '+1d').getFullYear() + '</p></td><td><p class="lead14">' + numFormat(parseFloat($("#inp_inco_evat").val()).toFixed(2)) + '</p></td></tr>';
								$("#tbody_bank").empty();
								$("#tbody_bank").append(tr);
							}
							calSignProj();
							alert("คำเตือน: โปรดกด OK หลังการแก้ไขไม่เช่นนั้นข้อมูลใหม่นี้จะไม่ถูกบันทึก\nเนื่องจากคุณได้เปลี่ยนแปลงค่าที่ส่งผลต่อ Invoicing plan ดังนั้นระบบจำเป็นต้องลบรายการเดิมทั้งหมดเพื่อป้องกันข้อผิดพลาดของข้อมูล");
						} else {
							$("#inp_inco_evat").val(old_inco_evat);
							$("#inp_inco_ivat").val($("#inp_inco_evat").val() * 1.07);
							$("#inp_dPBaht").val($("#inp_inco_evat").val() * ($("#inp_downPayment").val() / 100));
							$("#inp_downPayment").val((100 / $("#inp_inco_evat").val()) * $("#inp_dPBaht").val());
						}
					} else {
						$("#inp_inco_ivat").val($("#inp_inco_evat").val() * 1.07);
						$("#inp_dPBaht").val($("#inp_inco_evat").val() * ($("#inp_downPayment").val() / 100));
						$("#inp_downPayment").val((100 / $("#inp_inco_evat").val()) * $("#inp_dPBaht").val());
						tr = '<tr class="error"><td><p class="lead14">Book</p></td><td><p class="lead14">' + ($("#inp_signDate").val() != "" ? $("#inp_signDate").datepicker('getDate', '+1d').getFullYear() : new Date().getFullYear()) + '</p></td><td><p class="lead14">' + numFormat(parseFloat($("#inp_inco_evat").val()).toFixed(2)) + '</p></td></tr>';
						$("#tbody_bank").empty();
						$("#tbody_bank").append(tr);
						calSignProj();
					}
	    		}
	    	});
	    	$("#inp_budg_evat").on("blur", function() {
	    		if ($(this).val() == "") {
	    			$(this).val("0.00");
	    		}
	    		if ($(this).val() >= $("#inp_inco_evat").val()) {
	    			$("#inp_budg_ivat").val($("#inp_budg_evat").val() * 1.07);
	    		} else {
	    			alert("Budget ที่ระบุน้อยกว่า Income กรุณาระบุ Budget ใหม่");
	    			$(this).val("0.00");
	    			$("#inp_budg_ivat").val("0.00");
	    		}
	    	});
	    	$("#inp_cost_evat").on("blur", function() {
	    		if ($(this).val() == "") {
	    			$(this).val("0.00");
	    		}
	    		if (parseInt($(this).val()) <= parseInt($("#inp_inco_evat").val()) || parseInt($("#inp_cost_evat").val()) == 0) {
	    			$("#inp_cost_ivat").val($("#inp_cost_evat").val() * 1.07);
	    		} else {
	    			alert("Cost ที่ระบุเกิน Income กรุณาระบุ Cost ใหม่");
	    			$(this).val("0.00");
	    			$("#inp_cost_ivat").val("0.00");
	    		}
	    	});
	    	$("#inp_inco_ivat").on("blur", function() {
				if (parseInt($("#inp_budg_ivat").val()) < parseInt($("#inp_inco_ivat").val())) {
	    			alert("Income ที่ระบุเกิน Budget กรุณาระบุ Income ใหม่");
	    			$("#inp_inco_ivat").val("0.00");
	    			$("#inp_inco_evat").val("0.00");
	    		}else if (parseInt($("#inp_cost_ivat").val()) > parseInt($("#inp_inco_ivat").val()) && parseInt($("#inp_cost_ivat").val()) > 0) {
	    			alert("Cost ที่ระบุเกิน Income กรุณาระบุ Cost ใหม่");
	    			$("#inp_cost_ivat").val("0.00");
	    			$("#inp_cost_evat").val("0.00");
	    		}else{
	    		if ($(this).val() == "") $(this).val("0.00");
	    		if ($("#tbody_invoicing").children().length > 0 && $("#inp_inco_ivat").val() != old_inco_ivat) {
	    			if (window.confirm("หากเปลี่ยนแปลง Income ระบบจะคำนวนจำนวนเงินใน Invoicing plan ให้ใหม่อัตโนมัติ\nยืนยันหรือไม่")) {
			    		$("#inp_inco_evat").val($("#inp_inco_ivat").val() / 1.07);
			    		$("#inp_dPBaht").val($("#inp_inco_evat").val() * ($("#inp_downPayment").val() / 100));
						$("#inp_downPayment").val((100 / $("#inp_inco_evat").val()) * $("#inp_dPBaht").val());
						if ($("#btn_invoicing_modal").click()) {
			    			$("#tbody_invoicing").empty();
			    			tr = '<tr class="error"><td><p class="lead14">Book</p></td><td><p class="lead14">' + $("#inp_signDate").datepicker('getDate', '+1d').getFullYear() + '</p></td><td><p class="lead14">' + numFormat(parseFloat($("#inp_inco_evat").val()).toFixed(2)) + '</p></td></tr>';
							$("#tbody_bank").empty();
							$("#tbody_bank").append(tr);
			    		}
			    		calSignProj();
			    		alert("คำเตือน: โปรดกด OK หลังการแก้ไขไม่เช่นนั้นข้อมูลใหม่นี้จะไม่ถูกบันทึก\nเนื่องจากคุณได้เปลี่ยนแปลงค่าที่ส่งผลต่อ Invoicing plan ดังนั้นระบบจำเป็นต้องลบรายการเดิมทั้งหมดเพื่อป้องกันข้อผิดพลาดของข้อมูล");
	    			} else {
	    				$("#inp_inco_ivat").val(old_inco_ivat);
			    		$("#inp_inco_evat").val($("#inp_inco_ivat").val() / 1.07);
			    		$("#inp_dPBaht").val($("#inp_inco_evat").val() * ($("#inp_downPayment").val() / 100));
						$("#inp_downPayment").val((100 / $("#inp_inco_evat").val()) * $("#inp_dPBaht").val());
	    			}
	    		} else {
		    		$("#inp_inco_evat").val($("#inp_inco_ivat").val() / 1.07);
		    		$("#inp_dPBaht").val($("#inp_inco_evat").val() * ($("#inp_downPayment").val() / 100));
					$("#inp_downPayment").val((100 / $("#inp_inco_evat").val()) * $("#inp_dPBaht").val());
		    		tr = '<tr class="error"><td><p class="lead14">Book</p></td><td><p class="lead14">' + ($("#inp_signDate").val() != "" ? $("#inp_signDate").datepicker('getDate', '+1d').getFullYear() : new Date().getFullYear()) + '</p></td><td><p class="lead14">' + numFormat(parseFloat($("#inp_inco_evat").val()).toFixed(2)) + '</p></td></tr>';
					$("#tbody_bank").empty();
					$("#tbody_bank").append(tr);
					calSignProj();
	    		}
	    		}
	    	});
	    	$("#inp_budg_ivat").on("blur", function() {
	    		if ($(this).val() == "") {
	    			$(this).val("0.00");
	    		}
	    		if ($(this).val() >= $("#inp_inco_ivat").val()) {
	    			$("#inp_budg_evat").val($("#inp_budg_ivat").val() / 1.07);
	    		} else {
	    			alert("Budget ที่ระบุน้อยกว่า Income กรุณาระบุ Budget ใหม่");
	    			$(this).val("0.00");
	    			$("#inp_budg_evat").val("0.00");
	    		}
	    	});
	    	$("#inp_cost_ivat").on("blur", function() {
				if ($(this).val() == "") {
	    			$(this).val("0.00");
	    		}
	    		if (parseInt($(this).val()) <= parseInt($("#inp_inco_ivat").val()) || parseInt($("#inp_cost_ivat").val()) == 0) {
	    			$("#inp_cost_evat").val($("#inp_cost_ivat").val() / 1.07);
	    		} else {
	    			alert("Cost ที่ระบุเกิน Income กรุณาระบุ Cost ใหม่");
	    			$(this).val("0.00");
	    			$("#inp_cost_evat").val("0.00");
	    		}
	    	});
	    	$("#chk_downPayment").on("change", function() {
	    		if ($("#tbody_invoicing").children().length > 0) {
	    			if (window.confirm("หากเปลี่ยนแปลง Down payment ระบบจะคำนวน Invoicing plan ให้ใหม่อัตโนมัติ\nยืนยันหรือไม่")) {
			    		if ($(this).prop("checked")) {
			    			$("#inp_downPayment").prop("disabled", false);
			    			$("#inp_dPBaht").prop("disabled", false);
			    			$("#inp_dPDays").prop("disabled", false);
			    			$("#inp_dPDate").prop("disabled", false);
			    		} else {
			    			$("#inp_downPayment").prop("disabled", true);
			    			$("#inp_dPBaht").prop("disabled", true);
			    			$("#inp_dPDays").prop("disabled", true);
			    			$("#inp_dPDate").prop("disabled", true);
			    		}
						if ($("#btn_invoicing_modal").click()) {
			    			$("#tbody_invoicing").empty();
			    			tr = '<tr class="error"><td><p class="lead14">Book</p></td><td><p class="lead14">' + $("#inp_signDate").datepicker('getDate', '+1d').getFullYear() + '</p></td><td><p class="lead14">' + numFormat(parseFloat($("#inp_inco_evat").val()).toFixed(2)) + '</p></td></tr>';
							$("#tbody_bank").empty();
							$("#tbody_bank").append(tr);
			    		}
			    		alert("คำเตือน: โปรดกด OK หลังการแก้ไขไม่เช่นนั้นข้อมูลใหม่นี้จะไม่ถูกบันทึก\nเนื่องจากคุณได้เปลี่ยนแปลงค่าที่ส่งผลต่อ Invoicing plan ดังนั้นระบบจำเป็นต้องลบรายการเดิมทั้งหมดเพื่อป้องกันข้อผิดพลาดของข้อมูล");
	    			} else {

			    		if (!$(this).prop("checked")) {
			    			$(this).prop("checked", true);
			    			$("#inp_downPayment").prop("disabled", false);
			    			$("#inp_dPBaht").prop("disabled", false);
			    			$("#inp_dPDays").prop("disabled", false);
			    			$("#inp_dPDate").prop("disabled", false);
			    		} else {
			    			$(this).prop("checked", false);
			    			$("#inp_downPayment").prop("disabled", true);
			    			$("#inp_dPBaht").prop("disabled", true);
			    			$("#inp_dPDays").prop("disabled", true);
			    			$("#inp_dPDate").prop("disabled", true);
			    		}
	    			}
	    		} else {
		    		if ($(this).prop("checked")) {
		    			$("#inp_downPayment").prop("disabled", false);
		    			$("#inp_dPBaht").prop("disabled", false);
		    			$("#inp_dPDays").prop("disabled", false);
		    			$("#inp_dPDate").prop("disabled", false);
		    		} else {
		    			$("#inp_downPayment").prop("disabled", true);
		    			$("#inp_dPBaht").prop("disabled", true);
		    			$("#inp_dPDays").prop("disabled", true);
		    			$("#inp_dPDate").prop("disabled", true);
		    		}
	    		}
	    	});
	    	$("#inp_pDuration").on("blur", function() {
	    		if ($("#tbody_invoicing").children().length > 0 && $("#inp_pDuration").val() != old_pDuration) {
	    			if (window.confirm("หากเปลี่ยนจำนวนวันจะส่งผลต่อจำนวนวันและวันที่ของรายการใน Invoicing plan\nยืนยันหรือไม่")) {
		    			if (parseInt($("#inp_pDuration").val()) <= 0 || $("#inp_pDuration").val() == "") {
		    				alert("ไม่สามารถระบุเป็น 0 ได้เนื่องจากมีรายการใน Invoicing plan ที่อ้างอิงจำนวนวันอยู่");
		    				$("#inp_pDuration").val("1");
		    				calEndCont();
		    			} else {
		    				calEndCont();
		    			}
		    			if ($("#btn_invoicing_modal").click()) {
			    			$("#tbody_invoicing").empty();
			    			tr = '<tr class="error"><td><p class="lead14">Book</p></td><td><p class="lead14">' + $("#inp_signDate").datepicker('getDate', '+1d').getFullYear() + '</p></td><td><p class="lead14">' + numFormat(parseFloat($("#inp_inco_evat").val()).toFixed(2)) + '</p></td></tr>';
							$("#tbody_bank").empty();
							$("#tbody_bank").append(tr);
			    		}
			    		alert("คำเตือน: โปรดกด OK หลังการแก้ไขไม่เช่นนั้นข้อมูลใหม่นี้จะไม่ถูกบันทึก\nเนื่องจากคุณได้เปลี่ยนแปลงค่าที่ส่งผลต่อ Invoicing plan ดังนั้นระบบจำเป็นต้องลบรายการเดิมทั้งหมดเพื่อป้องกันข้อผิดพลาดของข้อมูล");
	    			} else {
	    				$("#inp_pDuration").val(old_pDuration);
	    				calEndCont();
	    			}
	    		} else {
	    			calEndCont();
	    		}
	    	});
	    	$("#inp_downPayment").on("blur", function() {
	    		if ($("#inp_downPayment").val() <= 100) {
		    		if ($("#tbody_invoicing").children().length > 0 && $("#inp_downPayment").val() != old_downPayment) {
		    			if (window.confirm("หากเปลี่ยนแปลง Down payment ระบบจะคำนวน Invoicing plan ให้ใหม่อัตโนมัติ\nยืนยันหรือไม่")) {
				    		$("#inp_dPBaht").val($("#inp_inco_evat").val() * ($("#inp_downPayment").val() / 100));
							if ($("#btn_invoicing_modal").click()) {
				    			$("#tbody_invoicing").empty();
				    			tr = '<tr class="error"><td><p class="lead14">Book</p></td><td><p class="lead14">' + $("#inp_signDate").datepicker('getDate', '+1d').getFullYear() + '</p></td><td><p class="lead14">' + numFormat(parseFloat($("#inp_inco_evat").val()).toFixed(2)) + '</p></td></tr>';
								$("#tbody_bank").empty();
								$("#tbody_bank").append(tr);
				    		}
				    		alert("คำเตือน: โปรดกด OK หลังการแก้ไขไม่เช่นนั้นข้อมูลใหม่นี้จะไม่ถูกบันทึก\nเนื่องจากคุณได้เปลี่ยนแปลงค่าที่ส่งผลต่อ Invoicing plan ดังนั้นระบบจำเป็นต้องลบรายการเดิมทั้งหมดเพื่อป้องกันข้อผิดพลาดของข้อมูล");
		    			} else {
		    				$("#inp_downPayment").val(old_downPayment);
				    		$("#inp_dPBaht").val($("#inp_inco_evat").val() * ($("#inp_downPayment").val() / 100));
		    			}
		    		} else {
			    		$("#inp_dPBaht").val($("#inp_inco_evat").val() * ($("#inp_downPayment").val() / 100));
		    		}
	    		} else {
	    			alert("จำนวน % ที่ระบุจะต้องไม่เกิน 100%");
	    			$("#inp_downPayment").val(old_downPayment);
	    		}
	    	});
	    	$("#inp_dPBaht").on("blur", function() {
	    		if ($("#inp_dPBaht").val() <= parseFloat($("#inp_inco_evat").val())) {
		    		if ($("#tbody_invoicing").children().length > 0 && $("#inp_dPBaht").val() != old_dPBaht) {
		    			if (window.confirm("หากเปลี่ยนแปลง Down payment ระบบจะคำนวน Invoicing plan ให้ใหม่อัตโนมัติ\nยืนยันหรือไม่")) {
				    		$("#inp_downPayment").val((100 / $("#inp_inco_evat").val()) * $("#inp_dPBaht").val());
							if ($("#btn_invoicing_modal").click()) {
				    			$("#tbody_invoicing").empty();
				    			tr = '<tr class="error"><td><p class="lead14">Book</p></td><td><p class="lead14">' + $("#inp_signDate").datepicker('getDate', '+1d').getFullYear() + '</p></td><td><p class="lead14">' + numFormat(parseFloat($("#inp_inco_evat").val()).toFixed(2)) + '</p></td></tr>';
								$("#tbody_bank").empty();
								$("#tbody_bank").append(tr);
				    		}
				    		alert("คำเตือน: โปรดกด OK หลังการแก้ไขไม่เช่นนั้นข้อมูลใหม่นี้จะไม่ถูกบันทึก\nเนื่องจากคุณได้เปลี่ยนแปลงค่าที่ส่งผลต่อ Invoicing plan ดังนั้นระบบจำเป็นต้องลบรายการเดิมทั้งหมดเพื่อป้องกันข้อผิดพลาดของข้อมูล");
		    			} else {
		    				$("#inp_dPBaht").val(old_dPBaht);
				    		$("#inp_downPayment").val((100 / $("#inp_inco_evat").val()) * $("#inp_dPBaht").val());
		    			}
		    		} else {
			    		$("#inp_downPayment").val((100 / $("#inp_inco_evat").val()) * $("#inp_dPBaht").val());
		    		}
	    		} else {
	    			alert("จำนวนเงินที่ระบุจะต้องไม่เกิน Excluded VAT income");
	    			$("#inp_dPBaht").val(old_dPBaht);
	    		}
	    	});
	    	$("#inp_dPDays").on("blur", function() {
	    		if ($("#tbody_invoicing").children().length > 0) {
	    			calEndCont();
	    			alert("ระบบจะทำการคำนวน Book/Forward ให้ใหม่อัตโนมัติ");
	    			if ($("#btn_invoicing_modal").click()) {
		    			$("#tbody_invoicing").empty();
		    			tr = '<tr class="error"><td><p class="lead14">Book</p></td><td><p class="lead14">' + $("#inp_signDate").datepicker('getDate', '+1d').getFullYear() + '</p></td><td><p class="lead14">' + numFormat(parseFloat($("#inp_inco_evat").val()).toFixed(2)) + '</p></td></tr>';
						$("#tbody_bank").empty();
						$("#tbody_bank").append(tr);
	    				$("#invoicing_btn_ok").click();
		    		}
	    		} else {
	    			calEndCont();
	    		}
	    	});
	    	$("#inp_dPDate").on("change", function() {
	    		tempDays = ($("#inp_dPDate").datepicker('getDate', '+1d') - $("#inp_signDate").datepicker('getDate', '+1d')) / (60 * 60 * 24 * 1000);
	    		if (tempDays >= 0) {
	    			$("#inp_dPDays").val(tempDays);
		    		if ($("#tbody_invoicing").children().length > 0) {
		    			$("#inp_dPDays").val(tempDays);
		    			alert("ระบบจะทำการคำนวน Book/Forward ให้ใหม่อัตโนมัติ");
		    			if ($("#btn_invoicing_modal").click()) {
			    			$("#tbody_invoicing").empty();
			    			tr = '<tr class="error"><td><p class="lead14">Book</p></td><td><p class="lead14">' + $("#inp_signDate").datepicker('getDate', '+1d').getFullYear() + '</p></td><td><p class="lead14">' + numFormat(parseFloat($("#inp_inco_evat").val()).toFixed(2)) + '</p></td></tr>';
							$("#tbody_bank").empty();
							$("#tbody_bank").append(tr);
		    				$("#invoicing_btn_ok").click();
			    		}
		    		} else {
	    				$("#inp_dPDays").val(tempDays);
		    		}
	    		} else {
	    			alert("กรุณาระบุวันหลังจาก Sign Date");
	    			$("#inp_dPDays").val(0);
	    			$("#inp_dPDays").blur();
	    		}
	    	});
	    	$("#inp_inco_ivat").on("click", function() {
	    		old_inco_ivat = $(this).val();
	    	});
	    	$("#inp_inco_evat").on("click", function() {
	    		old_inco_evat = $(this).val();
	    	});
	    	$("#inp_signDate").on("click", function() {
	    		old_signDate = $(this).datepicker('getDate', '+1d');
	    	});
	    	$("#inp_pDuration").on("click", function() {
	    		old_pDuration = $(this).val();
	    	});
	    	$("#inp_downPayment").on("click", function() {
	    		old_downPayment = $(this).val();
	    	});
	    	$("#inp_dPBaht").on("click", function() {
	    		old_dPBaht = $(this).val();
	    	});
	    	$("#btn_invoicing_modal").on("click", function() {
				
	    		if (($("#inp_inco_evat").val() > 0) && ($("#inp_signDate").val() != "") && ($("#inp_pDuration").val() > 0)) {
    				$("#invoicing_tbody_list").empty();
    				downPayment = "0.00";
    				salesBal = parseFloat($("#inp_inco_evat").val());
    				leftPerc = 100;
    				if ($("#chk_downPayment").prop("checked")) {
    					downPayment = numFormat(parseFloat($("#inp_dPBaht").val() != "" ? $("#inp_dPBaht").val() : "0").toFixed(2));
    					salesBal = parseFloat($("#inp_inco_evat").val()) - parseFloat($("#inp_dPBaht").val() != "" ? $("#inp_dPBaht").val() : "0");
    					leftPerc = 100 - parseFloat($("#inp_downPayment").val() != "" ? $("#inp_downPayment").val() : "0");
    				}
	    			tempTd = $("#invoicing_table_details").find("td");
	    			tempTd[0].innerHTML = '<h5 style="display: inline-block;">Income:&nbsp;</h5>' + numFormat(parseFloat($("#inp_inco_evat").val()));
		    		tempTd[1].innerHTML = '<h5 style="display: inline-block;">Sign Date:&nbsp;</h5>' + $("#inp_signDate").val();
		    		tempTd[2].innerHTML = '<h5 style="display: inline-block;">Down Payment:&nbsp;</h5>' + downPayment;
		    		tempTd[3].innerHTML = '<h5 style="display: inline-block;">End of Contract:&nbsp;</h5>' + $("#inp_endCont").val();
		    		tempTd[4].innerHTML = '<h5 style="display: inline-block;">Sales Balance:&nbsp;</h5><input type="text" class="span2 salesBal" value="' + salesBal + '" readonly>';
		    		tempTd[5].innerHTML = '<h5 style="display: inline-block;">Left (%):&nbsp;</h5><input type="text" class="span1 leftPerc" value="' + leftPerc + '" readonly>';
		    		$(".salesBal").number(true, 2);
		    		$(".leftPerc").number(true, 2);
		    		var tr = $('<tr><td style="text-align: center; vertical-align: middle;"></td><td></td><td style="vertical-align: middle;"></td><td></td><td></td><td><input class="invoicing_add_rema" type="text" style="width: 258px;"></td><td></td></tr>');
	    			var td0 = $('<i class="i_btn icon-plus-circle-1" title="Add"></i>');
	    			var td1 = $('<input class="invoicing_add_days" type="text" style="width: 30px;">');
	    			var td2 = $('<input class="invoicing_add_date" type="text" style="width: 70px;" readonly>');
	    			var td3 = $('<input class="invoicing_add_perc text-right" type="text" style="width: 50px;">');
	    			var td4 = $('<input class="invoicing_add_amnt text-right" type="text" style="width: 100px;" readonly>');
			    	td1.number(true, 0);
			    	td3.number(true, 0);
			    	td4.number(true, 2);
			    	td0.on("click", function() {
			    		if ($(".invoicing_add_days").val() > 0 && $(".invoicing_add_perc").val() > 0 && $(".invoicing_add_amnt").val() > 0) {
			    			var tr = $('<tr><td style="text-align: center; vertical-align: middle;">' + ($("#invoicing_tbody_list").children().length) + '</td><td></td><td style="vertical-align: middle;"></td><td></td><td></td><td><input type="text" style="width: 258px;" value="' + $(".invoicing_add_rema").val() + '"></td><td style="text-align: center; vertical-align: middle;"></td></tr>');
			    			var td1 = $('<input type="text" style="width: 30px;" value="' + $(".invoicing_add_days").val() + '">');
			    			var td2 = $('<input type="text" style="width: 70px;" value="' + $(".invoicing_add_date").val() + '" readonly>');
			    			var td3 = $('<input class="text-right" type="text" style="width: 50px;" value="' + $(".invoicing_add_perc").val() + '">');
			    			var td4 = $('<input class="text-right" type="text" style="width: 100px;" value="' + $(".invoicing_add_amnt").val() + '" readonly>');
			    			var td6 = $('<i class="i_btn icon-minus-circled-1" title="Delete"></i>');
					    	td1.number(true, 0);
					    	td3.number(true, 0);
					    	td4.number(true, 2);
					    	td1.on("blur", function() {
								if ($(this).val() > 0 && parseInt($(this).val()) <= parseInt($("#inp_pDuration").val())) {
									slcdate = $("#inp_signDate").datepicker('getDate', '+1d');
									slcdate.setDate(slcdate.getDate() + parseInt($(this).val()));
									$($(this).closest("tr").find("input")[1]).val(("0" + slcdate.getDate()).slice(-2) + "/" + ("0" + (slcdate.getMonth() + 1)).slice(-2) + "/" + slcdate.getFullYear());
								} else {
									alert("จำนวนวันที่ระบุจะต้องมากกว่า 0 และไม่เกิน Project duration (" + $("#inp_pDuration").val() + " วัน)");
									$(this).val("1");
									$(this).blur();
								}
					    	});
					    	td3.on("blur", function() {
					    		tempTr = $("#invoicing_tbody_list").children();
					    		tempTrIndx = $(this).closest("tr").index();
					    		tempLeftPerc = tempSalesBal = 0.00;
					    		for (i = 0; i < (tempTr.length - 1); i++) {
					    			if (i != tempTrIndx) {
					    				tempLeftPerc += parseFloat($($(tempTr[i]).find("input")[2]).val() == "" ? "0.00" : $($(tempTr[i]).find("input")[2]).val());
					    				tempSalesBal += parseFloat($($(tempTr[i]).find("input")[3]).val() == "" ? "0.00" : $($(tempTr[i]).find("input")[3]).val());
					    			}
					    		}
					    		$(".leftPerc").val(leftPerc - tempLeftPerc);
					    		$(".salesBal").val(salesBal - tempSalesBal);
					    		if ($(this).val() > 0) {
					    			tempLeftPerc = parseFloat($(".leftPerc").val());
					    			if (parseFloat($(this).val()) >= tempLeftPerc) {
					    				if (parseFloat($(this).val()) > tempLeftPerc) alert("จำนวนที่ระบุเกิน % ที่เหลืออยู่ ระบบจะคำนวนค่าที่เหลือให้อัตโนมัติ");
					    				$(this).val(tempLeftPerc);
					    				$($(this).closest("tr").find("input")[3]).val($(".salesBal").val());
					    				$(".leftPerc").val("");
					    				$(".salesBal").val("");
					    			} else {
					    				$($(this).closest("tr").find("input")[3]).val((parseFloat($("#inp_inco_evat").val()) / 100) * parseFloat($(this).val()));
					    				tempLeftPerc -= parseFloat($(this).val());
					    				tempSalesBal = parseFloat($(".salesBal").val()) - parseFloat($($(this).closest("tr").find("input")[3]).val());
					    				$(".leftPerc").val(tempLeftPerc);
					    				$(".salesBal").val(tempSalesBal);
					    			}
					    		} else  {
					    			alert("จำนวนที่ระบุจะต้องมากกว่า 0");
					    			$(this).val($(".leftPerc").val());
					    			$(this).blur();
					    		}
					    		$(".invoicing_add_perc").val(parseFloat($(".leftPerc").val()));
					    		$(".invoicing_add_amnt").val(parseFloat($(".salesBal").val()));
					    	});
					    	td4.on("blur", function() {
					    		tempTr = $("#invoicing_tbody_list").children();
					    		tempTrIndx = $(this).closest("tr").index();
					    		tempLeftPerc = tempSalesBal = 0.00;
					    		for (i = 0; i < (tempTr.length - 1); i++) {
					    			if (i != tempTrIndx) {
					    				tempLeftPerc += parseFloat($($(tempTr[i]).find("input")[2]).val() == "" ? "0.00" : $($(tempTr[i]).find("input")[2]).val());
					    				tempSalesBal += parseFloat($($(tempTr[i]).find("input")[3]).val() == "" ? "0.00" : $($(tempTr[i]).find("input")[3]).val());
					    			}
					    		}
					    		$(".leftPerc").val(leftPerc - tempLeftPerc);
					    		$(".salesBal").val(salesBal - tempSalesBal);
					    		if ($(this).val() > 0) {
					    			tempSalesBal = parseFloat($(".salesBal").val());
					    			if (parseFloat($(this).val()) >= tempSalesBal) {
					    				if (parseFloat($(this).val()) > tempSalesBal) alert("จำนวนที่ระบุเกินจำนวนที่เหลืออยู่ ระบบจะคำนวนค่าที่เหลือให้อัตโนมัติ");
					    				$(this).val(tempSalesBal);
					    				$($(this).closest("tr").find("input")[2]).val($(".leftPerc").val());
					    				$(".leftPerc").val("");
					    				$(".salesBal").val("");
					    			} else {
					    				$($(this).closest("tr").find("input")[2]).val((100 / parseFloat($("#inp_inco_evat").val())) * parseFloat($(this).val()));
					    				tempSalesBal -= parseFloat($(this).val());
					    				tempLeftPerc = parseFloat($(".leftPerc").val()) - parseFloat($($(this).closest("tr").find("input")[2]).val());
					    				$(".leftPerc").val(tempLeftPerc);
					    				$(".salesBal").val(tempSalesBal);
					    			}
					    		} else  {
					    			alert("จำนวนที่ระบุจะต้องมากกว่า 0");
					    			$(this).val($(".salesBal").val());
					    			$(this).blur();
					    		}
					    		$(".invoicing_add_perc").val(parseFloat($(".leftPerc").val()));
					    		$(".invoicing_add_amnt").val(parseFloat($(".salesBal").val()));
					    	});
							td6.on("click", function() {
								if (window.confirm("ต้องการลบรายการนี้หรือไม่")) {
									$(this).closest("tr").remove();
									tempTr = $("#invoicing_tbody_list").children();
						    		tempLeftPerc = tempSalesBal = 0.00;
						    		for (i = 0; i < (tempTr.length - 1); i++) {
						    			$($(tempTr[i]).find("td")[0]).text(i + 1);
					    				tempLeftPerc += parseFloat($($(tempTr[i]).find("input")[2]).val() == "" ? "0.00" : $($(tempTr[i]).find("input")[2]).val());
					    				tempSalesBal += parseFloat($($(tempTr[i]).find("input")[3]).val() == "" ? "0.00" : $($(tempTr[i]).find("input")[3]).val());
						    		}
						    		$(".leftPerc").val(leftPerc - tempLeftPerc);
						    		$(".salesBal").val(salesBal - tempSalesBal);
						    		$(".invoicing_add_perc").val(parseFloat($(".leftPerc").val()));
						    		$(".invoicing_add_amnt").val(parseFloat($(".salesBal").val()));
								}
							});
			    			$(tr.find("td")[1]).append(td1);
			    			$(tr.find("td")[2]).append(td2);
			    			$(tr.find("td")[3]).append(td3);
			    			$(tr.find("td")[4]).append(td4);
			    			$(tr.find("td")[6]).append(td6);
							$("#invoicing_tbody_list > tr:last-child").before(tr);

				    		tempTr = $("#invoicing_tbody_list").children();
				    		tempLeftPerc = tempSalesBal = 0.00;
				    		for (i = 0; i < (tempTr.length - 1); i++) {
			    				tempLeftPerc += parseFloat($($(tempTr[i]).find("input")[2]).val() == "" ? "0.00" : $($(tempTr[i]).find("input")[2]).val());
			    				tempSalesBal += parseFloat($($(tempTr[i]).find("input")[3]).val() == "" ? "0.00" : $($(tempTr[i]).find("input")[3]).val());
				    		}
				    		$(".leftPerc").val(leftPerc - tempLeftPerc);
				    		$(".salesBal").val(salesBal - tempSalesBal);
				    		$(".invoicing_add_days").val($("#inp_pDuration").val());
				    		$(".invoicing_add_days").blur();
				    		$(".invoicing_add_perc").val(parseFloat($(".leftPerc").val()));
				    		$(".invoicing_add_amnt").val(parseFloat($(".salesBal").val()));
				    		$(".invoicing_add_rema").val("");
			    		} else {
			    			alert("เพิ่มรายการไม่ได้เนื่องจาก Days, %, Amount มีค่าเท่ากับ 0 หรือไม่ได้ระบุค่า");
			    		}
			    	});
			    	td1.on("blur", function() {
						if ($(this).val() > 0 && parseInt($(this).val()) <= parseInt($("#inp_pDuration").val())) {
							slcdate = $("#inp_signDate").datepicker('getDate', '+1d');
							slcdate.setDate(slcdate.getDate() + parseInt($(this).val()));
							$($(this).closest("tr").find("input")[1]).val(("0" + slcdate.getDate()).slice(-2) + "/" + ("0" + (slcdate.getMonth() + 1)).slice(-2) + "/" + slcdate.getFullYear());
						} else {
							alert("จำนวนวันที่ระบุจะต้องมากกว่า 0 และไม่เกิน Project duration (" + $("#inp_pDuration").val() + " วัน)");
							$(this).val("1");
							$(this).blur();
						}
			    	});
			    	td3.on("blur", function() {
			    		if ($(this).val() > 0) {
			    			tempLeftPerc = parseFloat($(".leftPerc").val());
			    			if (parseFloat($(this).val()) >= tempLeftPerc) {
			    				$(this).val(tempLeftPerc);
			    				$($(this).closest("tr").find("input")[3]).val($(".salesBal").val());
			    			} else {
			    				$($(this).closest("tr").find("input")[3]).val((parseFloat($("#inp_inco_evat").val()) / 100) * parseFloat($(this).val()));
			    			}
			    		} else  {
			    			alert("จำนวนที่ระบุจะต้องมากกว่า 0");
			    			$(this).val("");
			    			$($(this).closest("tr").find("input")[3]).val("");
			    		}
			    	});
			    	td4.on("blur", function() {
			    		if ($(this).val() > 0) {
			    			tempSalesBal = parseFloat($(".salesBal").val());
			    			if (parseFloat($(this).val()) >= tempSalesBal) {
			    				$(this).val(tempSalesBal);
			    				$($(this).closest("tr").find("input")[2]).val($(".leftPerc").val());
			    			} else {
			    				$($(this).closest("tr").find("input")[2]).val((100 / parseFloat($("#inp_inco_evat").val())) * parseFloat($(this).val()));
			    			}
			    		} else  {
			    			alert("จำนวนที่ระบุจะต้องมากกว่า 0");
			    			$(this).val("");
			    			$($(this).closest("tr").find("input")[2]).val("");
			    		}
			    	});
	    			$(tr.find("td")[0]).append(td0);
	    			$(tr.find("td")[1]).append(td1);
	    			$(tr.find("td")[2]).append(td2);
	    			$(tr.find("td")[3]).append(td3);
	    			$(tr.find("td")[4]).append(td4);
					$("#invoicing_tbody_list").append(tr);

	    			$("#tbody_invoicing").children().each(function() {
	    				if (parseFloat($(".leftPerc").val()) > 0) {
		    				tempP = $(this).find("p");
		    				if (parseInt($(tempP[1]).text()) > parseInt($("#inp_pDuration").val())) {
		    					$(".invoicing_add_days").val($("#inp_pDuration").val());
		    				} else {
		    					$(".invoicing_add_days").val($(tempP[1]).text());
		    				}
			    			$(".invoicing_add_days").blur();
		    				//$(".invoicing_add_date").val($(tempP[2]).text());
			    			$(".invoicing_add_perc").val($(tempP[3]).text());
			    			$(".invoicing_add_perc").blur();
			    			//$(".invoicing_add_amnt").val($(tempP[4]).text());
			    			$(".invoicing_add_rema").val($(tempP[5]).text());
			    			td0.click();
	    				}
    				});

		    		$(".invoicing_add_days").val($("#inp_pDuration").val());
		    		$(".invoicing_add_days").blur();
		    		$(".invoicing_add_perc").val(parseFloat($(".leftPerc").val()));
		    		$(".invoicing_add_amnt").val(parseFloat($(".salesBal").val()));
		    		$(".invoicing_add_rema").val("");
		    		$("#invoicing_modal").modal("show");
	    		} else {
	    			alert("กรุณาระบุ Income, Sign date และ Project duration ก่อน");
	    		}
				$('.invoicing_add_days').keyup(function(event) {
					if(event.keyCode == 13) {
						$('.invoicing_add_perc').select();
					}
				});
				$('.invoicing_add_perc').keyup(function(event) {
					if(event.keyCode == 13) {
						$('.invoicing_add_rema').select();
					}
				});
	    	});
			
			$("#invoicing_btn_ok").on("click", function() {
	    		if (parseFloat($(".salesBal").val()) > 0 && parseFloat($(".leftPerc").val()) > 0) {
	    			alert("ยังเหลือยอดที่ยังไม่ได้ระบุในงวดอยู่ โปรดระบุยอดที่ค้างอยู่ให้หมด");
	    		} else {
	    			$("#tbody_invoicing").empty();
	    			$("#tbody_bank").empty();
	    			phaseNo = 1;
	    			tempArr = {};
	    			if ($("#chk_downPayment").prop("checked") && parseFloat($("#inp_dPBaht").val()) > 0) {
	    				if ($("#inp_dPDate").val() != "") {
	    					tempArr[$("#inp_dPDate").datepicker('getDate', '+1d').getFullYear()] = parseFloat($("#inp_dPBaht").val()).toFixed(2);
	    				} else {
	    					tempArr[$("#inp_signDate").datepicker('getDate', '+1d').getFullYear()] = parseFloat($("#inp_dPBaht").val()).toFixed(2);
	    				}
	    			}
	    			$("#invoicing_tbody_list tr:not(:last-child)").each(function() {
	    				tempInps = $(this).find("input");
	    				tempYear = $(tempInps[1]).val().split("/")[2];
	    				if (tempArr[tempYear] > 0) {
	    					tempArr[tempYear] = (parseFloat(tempArr[tempYear]) + parseFloat($(tempInps[3]).val())).toFixed(2);
	    				} else {
	    					tempArr[tempYear] = parseFloat($(tempInps[3]).val()).toFixed(2);
	    				}
	    				tr = '<tr class="warning"><td><p class="lead14">' + phaseNo++ + '</p></td><td><p class="lead14">' + $(tempInps[0]).val() + '</p></td><td><p class="lead14">' + $(tempInps[1]).val() + '</p></td><td><p class="lead14 text-right">' + numFormat(parseFloat($(tempInps[2]).val()).toFixed(2)) + '</p></td><td><p class="lead14 text-right">' + numFormat(parseFloat($(tempInps[3]).val()).toFixed(2)) + '</p></td><td><p class="lead14">' + $(tempInps[4]).val() + '</p></td></tr>';
	    				$("#tbody_invoicing").append(tr);
    				});
    				for (tempKey in tempArr) {
    					tempType = "Book";
    					if (parseInt(tempKey) > $("#inp_signDate").datepicker('getDate', '+1d').getFullYear()) tempType = "Forward";
    					tr = '<tr class="error"><td><p class="lead14">' + tempType + '</p></td><td><p class="lead14">' + tempKey + '</p></td><td><p class="lead14">' + numFormat(parseFloat(tempArr[tempKey]).toFixed(2)) + '</p></td></tr>';
    					$("#tbody_bank").append(tr);
    				}
	    			$("#invoicing_modal").modal("hide");
	    		}
	    	});
	    	$("#btn_sales_modal").on("click", function() {
	    		$("#sales_modal").modal("show");
	    	});
	    	$("#btn_status_modal").on("click", function() {
	    		$("#status_inp_eid").val("");
				$("#status_inp_date").datepicker("setDate", new Date());
				$("#status_inp_desc").val("");
	    		$("#status_alert").hide();
	    		$("#status_modal").modal("show");
	    	});
	    	$("#status_btn_ok").on("click", function() {
	    		if (($("#status_inp_date").val() == "") || ($("#status_inp_desc").val() == "")) {
	    			$("#status_alert").show();
	    		} else if ($("#status_inp_eid").val() == "") {
	    			var td_edit = $('<i class="i_btn icon-pencil-1" title="Edit" style="color: black;"></i>');
	    			var td_del = $('<i class="i_btn icon-minus-circled-1" title="Delete" style="color: red;"></i>');
	    			var tr = $('<tr class="warning"><td><p class="lead14">' + $("#status_inp_date").val() + '</p></td><td><p class="lead14">' + $("#status_inp_desc").val() + '</p></td><td class="text-center"></td><td class="text-center"></td>');
		    		td_edit.on("click", function() {
		    			$("#status_inp_eid").val(tr.index());
		    			$("#status_inp_date").val($(tr.find("p")[0]).text());
		    			$("#status_inp_desc").val($(tr.find("p")[1]).text());
	    				$("#status_alert").hide();
	    				$("#status_modal").modal("show");
		    		});
		    		td_del.on("click", function() {
		    			if (window.confirm("ต้องการลบรายการนี้หรือไม่")) {
		    				tr.remove();
		    			}
		    		});
	    			$(tr.find("td")[2]).append(td_edit);
	    			$(tr.find("td")[3]).append(td_del);
	    			$("#tbody_status").prepend(tr);
	    			$("#status_modal").modal("hide");
	    		} else {
	    			tr_eid = $($("#tbody_status").children()[$("#status_inp_eid").val()]);
	    			$(tr_eid.find("p")[0]).text($("#status_inp_date").val());
	    			$(tr_eid.find("p")[1]).text($("#status_inp_desc").val());
	    			$("#status_modal").modal("hide");
	    		}
	    	});
			$("#btn_help_modal").on("click", function() {
	    		$("#help_modal").modal("show");
	    	});
	    	$("#btn_deptsup_modal").on("click", function() {
	    		$("#deptsup_tbody_list").find("input").each(function() {
					$(this).prop("checked", false);
					names = $("#tbody_deptsup").find("p");
					for (i = 0; i < names.length; i++) {
						if (names[i].innerHTML == $(this).val()) {
							$(this).prop("checked", true);
						}
					}
				});
	    		$("#deptsup_modal").modal("show");
	    	});
	    	$("#deptsup_btn_ok").on("click", function() {
	    		$("#tbody_deptsup").empty();
				$("#deptsup_tbody_list").find("input").each(function() {
					if ($(this).prop("checked")) {
						var tr = $('<tr class="warning"><td><p class="lead14">' + $(this).val() + '</p></td><td class="text-center"></td></tr>');
	    				var td_del = $('<i class="i_btn icon-minus-circled-1" title="Delete" style="color: red;"></i>');
			    		td_del.on("click", function() {
			    			if (window.confirm("ต้องการลบรายการนี้หรือไม่")) {
			    				tr.remove();
			    			}
			    		});
	    				$(tr.find("td")[1]).append(td_del);
	    				$("#tbody_deptsup").append(tr);
					}
				});
				$("#deptsup_modal").modal("hide");
    		});
	    	$(".add_product").on("click", function() {
	    		$("#loading").show();
	    		$("#product_slc_group").empty();
	    		$("#product_inp_keywords").val("");
	    		opt = $('<option value="All" selected>All</option>');
	    		$("#product_slc_group").append(opt);
	    		tempBrand = $(this).closest("div").find("input").val();
	    		$("#product_title").text(tempBrand.split(" ")[0] + " Product");
	    		$("#product_inp_brand").val(tempBrand);
				$.ajax({
	    			type: "POST",
					dataType: "json",
			        url: "AJAX/listProductGroup.php",
			        data: {
			        	brand: tempBrand
			        },
			        success: function(json) {
			        	$.each(json, function() {
			        		opt = $('<option value="' + this[0] + '">' + this[0] + '</option>');
	    					$("#product_slc_group").append(opt);
			        	});
			        	$("#loading").hide();
		        	},
					error: function() {
						alert("เกิดปัญหาในการค้นหากรุณาลองใหม่อีกครั้ง");
						$("#loading").hide();
					}
			    });
			    $("#product_btn_search").click();
	    		$("#product_modal").modal("show");
	    	});
	    	$("#product_slc_group").on("change", function() {
	    		$("#loading").show();
	    		$("#product_tbody_list").empty();
	        	$("#product_amt").text("0");
	    		$.ajax({
	    			type: "POST",
					dataType: "json",
			        url: "AJAX/searchProduct.php",
			        data: {
			        	brand: $("#product_inp_brand").val(),
			        	group: $("#product_slc_group").val(),
			        	keyword: $("#product_inp_keywords").val()
			        },
			        success: function(json) {
			        	$("#product_amt").text(json.length);
			        	$.each(json, function() {
	    					var item = this;
			        		var tr = $('<tr><td>' + item[1] + '</td><td>' + item[2] + '</td></tr>');
			        		tr.on("click", function() {
			        			$("#quantity_title").text("Add Product");
			        			$("#quantity_product").text(item[2]);
			        			$("#quantity_inp_id").val(item[0]);
			        			$("#quantity_inp_eid").val("");
			        			$("#quantity_inp_quantity").val("1");
	    						$("#product_modal").modal("hide");
			        			$("#quantity_modal").modal("show");
			        		});
	    					$("#product_tbody_list").append(tr);
			        	});
						if (json.length == 0) {
							$("#product_tbody_list").append($('<tr><td colspan="2" style="text-align: center;">ไม่พบข้อมูล</td></tr>'));
						}
						$("#loading").hide();
		        	},
					error: function() {
						alert("เกิดปัญหาในการค้นหากรุณาลองใหม่อีกครั้ง");
						$("#loading").hide();
					}
			    });
	    	});
	    	$("#product_inp_keywords").on("keydown", function(e) {
	    		if (e.keyCode == 13) $("#product_btn_search").click();
    		});
	    	$("#product_btn_search").on("click", function() {
	    		$("#loading").show();
	    		$("#product_tbody_list").empty();
	        	$("#product_amt").text("0");
	    		$.ajax({
	    			type: "POST",
					dataType: "json",
			        url: "AJAX/searchProduct.php",
			        data: {
			        	brand: $("#product_inp_brand").val(),
			        	group: $("#product_slc_group").val(),
			        	keyword: $("#product_inp_keywords").val()
			        },
			        success: function(json) {
			        	$("#product_amt").text(json.length);
			        	$.each(json, function() {
	    					var item = this;
			        		var tr = $('<tr><td>' + item[1] + '</td><td>' + item[2] + '</td></tr>');
			        		tr.on("click", function() {
			        			$("#quantity_title").text("Add Product");
			        			$("#quantity_product").text(item[2]);
			        			$("#quantity_inp_id").val(item[0]);
			        			$("#quantity_inp_eid").val("");
			        			$("#quantity_inp_quantity").val("1");
	    						$("#product_modal").modal("hide");
			        			$("#quantity_modal").modal("show");
			        		});
	    					$("#product_tbody_list").append(tr);
			        	});
						if (json.length == 0) {
							$("#product_tbody_list").append($('<tr><td colspan="2" style="text-align: center;">ไม่พบข้อมูล</td></tr>'));
						}
						$("#loading").hide();
		        	},
					error: function() {
						alert("เกิดปัญหาในการค้นหากรุณาลองใหม่อีกครั้ง");
						$("#loading").hide();
					}
			    });
	    	});
	    	$("#quantity_btn_ok").on("click", function() {
	    		var brand = $("#product_inp_brand").val();
	    		switch (brand) {
	    			case "ESRI Product":
	    				tempTbody = $("#tbody_esri");
	    				break;
	    			case "Leica":
	    				tempTbody = $("#tbody_leica");
	    				break;
	    			case "Garmin":
	    				tempTbody = $("#tbody_garmin");
	    				break;
	    			case "NOSTRA":
	    				tempTbody = $("#tbody_nostra");
	    				break;
    				default: 
    					tempTbody = $("#tbody_esri");
	    		}
	    		if ($("#quantity_inp_eid").val() == "") {
	    			var td_edit = $('<i class="i_btn icon-pencil-1" title="Edit" style="color: black;"></i>');
	    			var td_del = $('<i class="i_btn icon-minus-circled-1" title="Delete" style="color: red;"></i>');
		    		var tr = $('<tr class="warning"><td><p class="lead14">' + $("#quantity_product").text() + '</p></td><td><p class="lead14">' + nNumFormat($("#quantity_inp_quantity").val()) + '</p><p style="display: none;">' + $("#quantity_inp_id").val() + '</p></td><td class="text-center"></td><td class="text-center"></td></tr>');
		    		td_edit.on("click", function() {
		    			tempObj = tr.find("p");
		    			$("#product_inp_brand").val(brand);
	        			$("#quantity_title").text("Edit Product");
		    			$("#quantity_product").text($(tempObj[0]).text());
	        			$("#quantity_inp_id").val($(tempObj[2]).text());
	        			$("#quantity_inp_eid").val($(this).closest("tr").index());
	        			$("#quantity_inp_quantity").val($(tempObj[1]).text());
	        			$("#quantity_modal").modal("show");
		    		});
		    		td_del.on("click", function() {
		    			if (window.confirm("ต้องการลบรายการนี้หรือไม่")) {
		    				tr.remove();
		    			}
		    		});
	    			$(tr.find("td")[2]).append(td_edit);
	    			$(tr.find("td")[3]).append(td_del);
	    			tempTbody.append(tr);
	    		} else {
	    			$($(tempTbody.find("tr")[$("#quantity_inp_eid").val()]).find("p")[1]).text(nNumFormat($("#quantity_inp_quantity").val()));
	    		}
    			$("#quantity_modal").modal("hide");
    		});
			document.getElementById("quantity_modal").addEventListener ('DOMAttrModified', function() {
				if ($("#quantity_modal").css('display') == "none" && $("#quantity_title").text() == "Add Product") $("#product_modal").modal("show");
			}, false);
//-----------------------------------------------------------------------------------------------------------
//------------------------------------------------- cancel ----------------------------------------------------
	$("#btn_cancel").on("click", function() {
		<?
		if ($page) {
		?>
			window.location = "<?=$page?>.php";
		<?
		} else {
		?>
			window.location = "index.php";
		<?
			}
		?>
	});
//-----------------------------------------------------------------------------------------------------------
//------------------------------------------------- Save ----------------------------------------------------
			$("#btn_save").on("click", function() {
				if ($("#inp_contractproj").val() == "" || $("#inp_inco_evat").val() == "" || $("#inp_signDate").val() == "") {
					alert("กรุณาระบุข้อมูลที่จำเป็นให้ครบ\nProject, Income, Contract Sign Date");
					window.location = "#";
					$("#inp_contractproj").focus();
				} else if ($("#forecastid").text() == "New Forecast" && $("#inp_custid").val() == "") {
					alert("กรุณาเลือก Customer");
				} else if (($("#forecastid").text() != "New Forecast") && (_lastStatus == $($("#tbody_status tr:first-child").find("p")[1]).text())) {
					alert("กรุณา Update Status ทุกครั้งที่มีการแก้ไข");
					$("#btn_status_modal").click();
					if (_lastPotential != $("#inp_potentialp").val()) {
						tempText = "ปรับปรุง Potential จาก " + _lastPotential + " เป็น " + $("#inp_potentialp").val();
					} else if (_lastProgress != $("#inp_progressp").val()) {
						tempText = "ปรับปรุง Progress จาก " + _lastProgress + " เป็น " + $("#inp_progressp").val();
					} else if (_lastIncome != parseFloat($("#inp_inco_evat").val())) {
						tempText = "ปรับปรุง Income จาก " + numFormat(_lastIncome.toFixed(2)) + " เป็น " + numFormat(parseFloat($("#inp_inco_evat").val()).toFixed(2));
					} else if (_lastBidding != $("#inp_biddDate").val()) {
						tempText = "ปรับปรุง Bidding Date จาก " + _lastBidding + " เป็น " + $("#inp_biddDate").val();
					} else if (_lastSign != $("#inp_signDate").val()) {
						tempText = "ปรับปรุง Contract Sign Date จาก " + _lastSign + " เป็น " + $("#inp_signDate").val();
					} else {
						tempText = "";
					}
					$("#status_inp_desc").val(tempText);
				} else {
					if ($("#forecastid").text() == "New Forecast") {
						if ($("#inp_custini").val() != "") {
							if (window.confirm("ต้องการบันทึกหรือไม่")) {
								$("#loading").show();
								if ($("#inp_contractpe").val() != "") {
									saveSalesForecast("new");
								} else {
									$.ajax({
										type: "POST",
										dataType: "text",
										data: {
											ContYear: new Date().getFullYear().toString(),
											CustomerNo: $("#inp_custno").val(),
											UpdBy: _loginID,
											PreContractName: $("#inp_contractproj").val(),
											CustPersonName: $("#inp_custtname").val() + " " + $("#inp_custfname").val() + " " + $("#inp_custlname").val()
										},
									    url: "AJAX/IS_getPEContract.php",
									    success: function(json) {
									    	console.log(json);
									   //  	if (json != ", ErrMsg" && json != "") {
										  //   	$("#inp_contractpe").val(json);
												// saveSalesForecast("new");
									   //  	} else {
									   //  		alert("เกิดปัญหาในการ Generate PEContract กรุณาลองใหม่");
									   //  		$("#loading").hide();
									   //  	}
										},
										error: function() {
											alert("เกิดปัญหาในการค้นหากรุณาลองใหม่อีกครั้ง");
											$("#loading").hide();
										}
									});
								}
							}
						} else {
							alert("กรุณเลือก Customer หากเลือกแล้วยังไม่ได้อาจเกิดจากข้อมูล Customer ขาดหาย กรุณาติดต่อ Team IT");
						}
					} else {
						if (window.confirm("ต้องการบันทึกหรือไม่")) {
							$("#loading").show();
							saveSalesForecast($("#forecastid").text());
						}
					}
				}
			});
//-----------------------------------------------------------------------------------------------------------

//------------------------------------------------- Load Function -------------------------------------------
			tempDescType = ["", "Site Prep", "HW", "SW", "Data", "Application", "On Site", "Training", "GPS", "SURVEY EQUIPMENT", "SYSTEMS", "Other", "CONSULT", "ESRI", "Leica", "Garmin", "NOSTRA"];
			function load_Tb_Description(type, desc, price) {
				var td_edit = $('<i class="i_btn icon-pencil-1" title="Edit" style="color: black;"></i>');
				var td_del = $('<i class="i_btn icon-minus-circled-1" title="Delete" style="color: red;"></i>');
	    		var tr = $('<tr class="warning"><td><p class="lead14">' + tempDescType[type] + '</p></td><td><p class="lead14">' + desc + '</p></td><td><p class="lead14 text-right">' + numFormat(parseFloat(price != "" ? price : "0.00").toFixed(2)) + '</p></td><td class="text-center"></td><td class="text-center"></td></tr>');
	    		td_edit.on("click", function() {
	    			tempObj = tr.find("p");
	    			$("#editdescription_inp_eid").val($(this).closest("tr").index());
	    			$("#editdescription_slc_type").val($(tempObj[0]).text());
	    			$("#editdescription_inp_description").val($(tempObj[1]).text());
	    			$("#editdescription_inp_price").val($(tempObj[2]).text());
	    			$("#editdescription_modal").modal("show");
	    		});
	    		td_del.on("click", function() {
	    			if (window.confirm("ต้องการลบรายการนี้หรือไม่")) {
	    				tr.remove();
	    				calSIProj();
	    			}
	    		});
				$(tr.find("td")[3]).append(td_edit);
				$(tr.find("td")[4]).append(td_del);
				$("#tbody_description").append(tr);
			}
			function load_Tb_Status(date, desc) {
				var td_edit = $('<i class="i_btn icon-pencil-1" title="Edit" style="color: black;"></i>');
    			var td_del = $('<i class="i_btn icon-minus-circled-1" title="Delete" style="color: red;"></i>');
    			var tr = $('<tr class="warning"><td><p class="lead14">' + date + '</p></td><td><p class="lead14">' + desc + '</p></td><td class="text-center"></td><td class="text-center"></td>');
	    		td_edit.on("click", function() {
	    			$("#status_inp_eid").val(tr.index());
	    			$("#status_inp_date").val($(tr.find("p")[0]).text());
	    			$("#status_inp_desc").val($(tr.find("p")[1]).text());
    				$("#status_alert").hide();
    				$("#status_modal").modal("show");
	    		});
	    		td_del.on("click", function() {
	    			if (window.confirm("ต้องการลบรายการนี้หรือไม่")) {
	    				tr.remove();
	    			}
	    		});
    			$(tr.find("td")[2]).append(td_edit);
    			$(tr.find("td")[3]).append(td_del);
    			$("#tbody_status").append(tr);
			}
			function load_Tb_DeptSupport(dept) {
				var tr = $('<tr class="warning"><td><p class="lead14">' + dept + '</p></td><td class="text-center"></td></tr>');
				var td_del = $('<i class="i_btn icon-minus-circled-1" title="Delete" style="color: red;"></i>');
				var tVal = dept;
				var tempObj = $("#deptsup_tbody_list").find("input[value='" + tVal + "']");
				if (tempObj.length > 0) {
					tempObj[0].checked = true;
		    		td_del.on("click", function() {
		    			if (window.confirm("ต้องการลบรายการนี้หรือไม่")) {
		    				tempObj[0].checked = false;
		    				tr.remove();
		    			}
		    		});
				} else {
					td_del.on("click", function() {
		    			if (window.confirm("ต้องการลบรายการนี้หรือไม่")) {
		    				tr.remove();
		    			}
		    		});
				}
				$(tr.find("td")[1]).append(td_del);
				$("#tbody_deptsup").append(tr);
			}
			function load_Tb_Product(brand, product, qty, pid) {
				switch (brand) {
	    			case "ESRI Product":
	    				tempTbody = $("#tbody_esri");
	    				break;
	    			case "Leica":
	    				tempTbody = $("#tbody_leica");
	    				break;
	    			case "Garmin":
	    				tempTbody = $("#tbody_garmin");
	    				break;
	    			case "NOSTRA":
	    				tempTbody = $("#tbody_nostra");
	    				break;
    				default: 
    					tempTbody = $("#tbody_esri");
	    		}
	    		var tr = $('<tr class="warning"><td><p class="lead14">' + product + '</p></td><td><p class="lead14">' + qty + '</p><p style="display: none;">' + pid + '</p></td><td class="text-center"></td><td class="text-center"></td></tr>');
    			var td_edit = $('<i class="i_btn icon-pencil-1" title="Edit" style="color: black;"></i>');
    			var td_del = $('<i class="i_btn icon-minus-circled-1" title="Delete" style="color: red;"></i>');
	    		td_edit.on("click", function() {
	    			tempObj = tr.find("p");
	    			$("#product_inp_brand").val(brand);
        			$("#quantity_title").text("Edit Product");
	    			$("#quantity_product").text($(tempObj[0]).text());
        			$("#quantity_inp_id").val($(tempObj[2]).text());
        			$("#quantity_inp_eid").val($(this).closest("tr").index());
        			$("#quantity_inp_quantity").val($(tempObj[1]).text());
        			$("#quantity_modal").modal("show");
	    		});
	    		td_del.on("click", function() {
	    			if (window.confirm("ต้องการลบรายการนี้หรือไม่")) {
	    				tr.remove();
	    			}
	    		});
    			$(tr.find("td")[2]).append(td_edit);
    			$(tr.find("td")[3]).append(td_del);
    			tempTbody.append(tr);
			}
//-----------------------------------------------------------------------------------------------------------

//------------------------------------------------- Load ----------------------------------------------------
		<?
			if ($id) {
				$conn = mssql_connect("157.179.28.116", "sa", "dioro@4321");
				mssql_select_db("SaleForecast", $conn);
				$sqlStr = "SELECT ID, FCYear, PEContractNo, EContract, Project, CustName, CustOrg, Potential, Progress, ESRIisTOR, Education, TargetProject, TargetSpecialProject, TargetIncome, TargetBudget, TargetCost, CONVERT(VARCHAR(10), TimeFrameBidingDate, 103) AS TimeFrameBidingDate, CONVERT(VARCHAR(10), TimeFrameContractSigndate, 103) AS TimeFrameContractSigndate, TimeFrameProjectDuration, chkDP, PercentAmount, InvoiceAmount, InvDuration, Remark, SaleID, SaleRepresentative, VerticalMarketGroup, ProjectBy, title, custsurname, division, department, CustID, quotno FROM Forecast WHERE IDForecast='$id'";
				$query = mssql_query(iconv("UTF-8", "TIS-620", $sqlStr), $conn);
				if ($obj = mssql_fetch_object($query)) {
		?>
			tempFCYear = '<?=$obj->FCYear?>';
			$("#inp_contractpe").val('<?=$obj->PEContractNo?>');
			$("#inp_contractec").val('<?=$obj->EContract?>');
			if('<?=$obj->quotno?>' != "") $("#importedFrom").text("Imported from quotation: " + '<?=replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->quotno))?>');
			$("#inp_contractproj").val('<?=replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->Project))?>');
		<?
				$sqlStr = "SELECT a.title, a.name, a.surname, b.thaidivision, b.thaidepartment, b.thaioganization FROM Customers a, Organizations b WHERE a.id=".($obj->CustID == "" ? 0 : $obj->CustID)." AND b.organizationid = a.organizationid";
				mssql_select_db("NewCISTest", $conn);
				$query = mssql_query(iconv("UTF-8", "TIS-620", $sqlStr), $conn);
				if ($objCust = mssql_fetch_object($query)) {
		?>
			$("#inp_custid").val('<?=$obj->CustID?>');
			$("#inp_custtname").val('<?=replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $objCust->title))?>');
			$("#inp_custfname").val('<?=replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $objCust->name))?>');
			$("#inp_custlname").val('<?=replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $objCust->surname))?>');
			$("#inp_custdiv").val('<?=replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $objCust->thaidivision))?>');
			$("#inp_custdep").val('<?=replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $objCust->thaidepartment))?>');
			$("#inp_custorg").val('<?=replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $objCust->thaioganization))?>');
		<?
				} else {
		?>
			$("#inp_custid").val('0');
			$("#inp_custtname").val('<?=replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->title))?>');
			$("#inp_custfname").val('<?=replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->CustName))?>');
			$("#inp_custlname").val('<?=replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->custsurname))?>');
			$("#inp_custdiv").val('<?=replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->division))?>');
			$("#inp_custdep").val('<?=replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->department))?>');
			$("#inp_custorg").val('<?=replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->CustOrg))?>');
		<?
				}
				mssql_select_db("SaleForecast", $conn);
		?>
			$("#potential_tbody_list").children().each(function() {
				if ($($(this).children()[0]).text() == '<?=$obj->Potential?>%') {
					$(this).click();
					return false;
				}
			});
			$("#progress_tbody_list").children().each(function() {
				var chkProgress = '';
				if('<?=$obj->Progress?>' == 'v'){
					chkProgress = '<?=$obj->Progress?>';
				}else{
					chkProgress = '<?=$obj->Progress?>%';
				}
				if ($($(this).children()[0]).text() == chkProgress) {
					$(this).click();
					return false;
				}
			});
			_lastPotential = '<?=$obj->Potential?>%';
			_lastProgress = '<?=$obj->Progress?>%';
			_lastIncome = parseFloat('<?=$obj->TargetIncome?>');
			_lastBidding = '<?=$obj->TimeFrameBidingDate?>';
			_lastSign = '<?=$obj->TimeFrameContractSigndate?>';
			if('<?=$obj->ESRIisTOR?>' == "1") $("#chk_esri").prop("checked", true);
			if('<?=$obj->Education?>' == "1") $("#chk_edu").prop("checked", true);
			if('<?=$obj->TargetProject?>' == "1") $("#chk_proj").prop("checked", true);
			if('<?=$obj->TargetSpecialProject?>' == "1") $("#chk_sproj").prop("checked", true);
			$("#inp_budg_evat").val('<?=$obj->TargetBudget?>').blur();
			$("#inp_inco_evat").val('<?=$obj->TargetIncome?>').blur();
			$("#inp_cost_evat").val('<?=$obj->TargetCost?>').blur();
			$("#inp_biddDate").val('<?=$obj->TimeFrameBidingDate?>').change();
			$("#inp_signDate").val('<?=$obj->TimeFrameContractSigndate?>').change();
			$("#inp_pDuration").val('<?=$obj->TimeFrameProjectDuration?>').blur();
			if('<?=$obj->chkDP?>' == "0") $("#chk_downPayment").click();
			$("#inp_downPayment").val('<?=$obj->PercentAmount?>');
			$("#inp_dPBaht").val('<?=$obj->InvoiceAmount?>');
			$("#inp_dPDays").val('<?=$obj->InvDuration?>').blur();
			$("#txa_salesremark").val('<?=replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->Remark))?>');
			$("#inp_salesid").val('<?=$obj->SaleID?>');
			$("#inp_salesname").val('<?=replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->SaleRepresentative))?>');
			$("#slc_verticalmarket").val('<?=$obj->VerticalMarketGroup?>');
			$("#slc_salesproj").val('<?=replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->ProjectBy))?>');
		<?
					$sqlStr = "SELECT IDTypeDescription, Description, Price FROM DescriptionDetail WHERE IDForecast='$id'";
					$query = mssql_query(iconv("UTF-8", "TIS-620", $sqlStr), $conn);
					while ($obj = mssql_fetch_object($query)) {
		?>
			load_Tb_Description(<?=$obj->IDTypeDescription?>, '<?=replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->Description))?>', '<?=$obj->Price?>');
		<?
					}
		?>
			calSIProj();
			tempArr = {};
			if ($("#chk_downPayment").prop("checked") && parseFloat($("#inp_dPBaht").val()) > 0) {
				if ($("#inp_dPDate").val() != "") {
					tempArr[$("#inp_dPDate").datepicker('getDate', '+1d').getFullYear()] = parseFloat($("#inp_dPBaht").val()).toFixed(2);
				} else {
					tempArr[tempFCYear] = parseFloat($("#inp_dPBaht").val()).toFixed(2);
				}
			}
		<?
					$sqlStr = "SELECT PlanPhase, DurationDelivery, CONVERT(VARCHAR(10), DeliveryDate, 103) AS DeliveryDate, Amount_Percent, Amount, Remark FROM InvoicingPlan WHERE IDForecast='$id' ORDER BY InvoicingPlanID";
					$query = mssql_query(iconv("UTF-8", "TIS-620", $sqlStr), $conn);
					if (mssql_num_rows($query) > 0) {
		?>
			$("#tbody_bank").empty();
		<?
					}
					while ($obj = mssql_fetch_object($query)) {
		?>
			tempYear = '<?=$obj->DeliveryDate?>'.split("/")[2];
			if (tempArr[tempYear] > 0) {
				tempArr[tempYear] = (parseFloat(tempArr[tempYear]) + parseFloat('<?=$obj->Amount?>')).toFixed(2);
			} else {
				tempArr[tempYear] = parseFloat('<?=$obj->Amount?>').toFixed(2);
			}
			tr = '<tr class="warning"><td><p class="lead14"><?=$obj->PlanPhase?></p></td><td><p class="lead14"><?=$obj->DurationDelivery?></p></td><td><p class="lead14"><?=$obj->DeliveryDate?></p></td><td><p class="lead14 text-right">' + numFormat(parseFloat('<?=$obj->Amount_Percent?>').toFixed(2)) + '</p></td><td><p class="lead14 text-right">' + numFormat(parseFloat('<?=$obj->Amount?>').toFixed(2)) + '</p></td><td><p class="lead14"><?=replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->Remark))?></p></td></tr>';
			$("#tbody_invoicing").append(tr);
		<?
					}
		?>
			for (tempKey in tempArr) {
				tempType = "Book";
				if (parseInt(tempKey) > $("#inp_signDate").datepicker('getDate', '+1d').getFullYear()) tempType = "Forward";
				tr = '<tr class="error"><td><p class="lead14">' + tempType + '</p></td><td><p class="lead14">' + tempKey + '</p></td><td><p class="lead14">' + numFormat(parseFloat(tempArr[tempKey]).toFixed(2)) + '</p></td></tr>';
				$("#tbody_bank").append(tr);
			}
		<?
					$sqlStr = "SELECT CONVERT(VARCHAR(10), DateStatus, 103) AS CDateStatus, Description FROM StatusDetail WHERE IDForecast='$id' ORDER BY DateStatus DESC";
					$query = mssql_query(iconv("UTF-8", "TIS-620", $sqlStr), $conn);
					while ($obj = mssql_fetch_object($query)) {
		?>
			load_Tb_Status('<?=$obj->CDateStatus?>', '<?=replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->Description))?>');
			if (_lastStatus == "") _lastStatus = '<?=replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->Description))?>';
		<?
					}
					$sqlStr = "SELECT DSNName FROM DepartmentSupportNeeded WHERE IDForecast='$id' ORDER BY DSNName";
					$query = mssql_query(iconv("UTF-8", "TIS-620", $sqlStr), $conn);
					while ($obj = mssql_fetch_object($query)) {
		?>
			load_Tb_DeptSupport('<?=replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->DSNName))?>');
		<?
					}
					$sqlStr = "SELECT EPName, qty, Product_id FROM ESRIProduct WHERE IDForecast='$id' ORDER BY IDEP";
					$query = mssql_query(iconv("UTF-8", "TIS-620", $sqlStr), $conn);
					while ($obj = mssql_fetch_object($query)) {
		?>
			load_Tb_Product("ESRI Product", '<?=replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->EPName))?>', '<?=$obj->qty?>', '<?=$obj->Product_id?>');
		<?
					}
					$sqlStr = "SELECT LPName, qty, Product_id FROM LeicaProduct WHERE IDForecast='$id' ORDER BY IDLP";
					$query = mssql_query(iconv("UTF-8", "TIS-620", $sqlStr), $conn);
					while ($obj = mssql_fetch_object($query)) {
		?>
			load_Tb_Product("Leica", '<?=replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->LPName))?>', '<?=$obj->qty?>', '<?=$obj->Product_id?>');
		<?
					}
					$sqlStr = "SELECT TMPName, qty, Product_id FROM TMProduct WHERE IDForecast='$id' ORDER BY IDTMP";
					$query = mssql_query(iconv("UTF-8", "TIS-620", $sqlStr), $conn);
					while ($obj = mssql_fetch_object($query)) {
		?>
			load_Tb_Product("Garmin", '<?=replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->TMPName))?>', '<?=$obj->qty?>', '<?=$obj->Product_id?>');
		<?
					}
					$sqlStr = "SELECT GPName, qty, Product_id FROM GEOProduct WHERE IDForecast='$id' ORDER BY IDGP";
					$query = mssql_query(iconv("UTF-8", "TIS-620", $sqlStr), $conn);
					while ($obj = mssql_fetch_object($query)) {
		?>
			load_Tb_Product("NOSTRA", '<?=replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->GPName))?>', '<?=$obj->qty?>', '<?=$obj->Product_id?>');
		<?
					}
		?>
			$("#btn_hPotential_modal").show();
			$("#btn_hProgress_modal").show();
		<?
					$sqlStr = "SELECT CONVERT(VARCHAR(10), DateChange, 103) AS DateChange, Potential, PotentialName FROM HistoryPotential WHERE IDForecast='$id' ORDER BY ID DESC";
					$query = mssql_query(iconv("UTF-8", "TIS-620", $sqlStr), $conn);
					while ($obj = mssql_fetch_object($query)) {
		?>
			$("#hPotential_tbody_list").append($('<tr><td><?=$obj->DateChange?></td><td><?=$obj->Potential?>%</td><td><?=replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->PotentialName))?></td></tr>'));
		<?
					}
					$sqlStr = "SELECT CONVERT(VARCHAR(10), DateChange, 103) AS DateChange, Progress, ProgressName FROM HistoryProgress WHERE IDForecast='$id' ORDER BY ID DESC";
					$query = mssql_query(iconv("UTF-8", "TIS-620", $sqlStr), $conn);
					while ($obj = mssql_fetch_object($query)) {
		?>
			$("#hProgress_tbody_list").append($('<tr><td><?=$obj->DateChange?></td><td><?=$obj->Progress?>%</td><td><?=replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->ProgressName))?></td></tr>'));
		<?
					}
				} else {
		?>
			$("#chk_downPayment").click();
		<?
				}
			} else {
		?>
			$("#chk_downPayment").click();
		<?
			}
		?>
//-----------------------------------------------------------------------------------------------------------
			$("#loading").hide();

	    });
	</script>

</body>
</html>