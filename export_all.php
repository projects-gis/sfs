﻿<!DOCTYPE html>
<html lang="en">
	
<!-- Mirrored from bootstrapstyler.com/preview/_/flatdashboard/ui.html by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 18 Oct 2013 07:02:36 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
		<meta charset="utf-8">
		<title>SFS:::Export->Monthly</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="Izuddin Helmi">

		<!-- Stylesheets -->
		<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
		<link href="js/library/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
		<link href="css/styler/style.css" rel="stylesheet">
		<link href="js/library/syntaxHighlighter/shCore.css" rel="stylesheet" type="text/css" />
		<link href="js/library/syntaxHighlighter/shThemeDefault.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="css/styler/fontello.css">
	    <link rel="stylesheet" href="css/styler/animation.css"><!--[if IE 7]>
	    <link rel="stylesheet" href="css/styler/fontello-ie7.css"><![endif]-->
	    

		<!-- Custom Fonts -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300,500' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

		<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
		  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	</head>
<?php
$checkmenu = '5';
$menuExport = '1';
?>
<body cz-shortcut-listen="true" style="margin-top:0px;">
<div id="loading" style="height: 100%; width: 100%; position: fixed; left: 0; top: 0; z-index: 1051 !important; background-color: black; filter: alpha(opacity=9); -khtml-opacity: 0.9; -moz-opacity: 0.9; opacity: 0.9;"><img src="img/loading.gif" style="width: 150px; height: 150px; position: fixed; top: 50%; left: 50%; margin: -75px 0 0 -75px;"></div>
<form name="frmexport"  action="ExportReport.php" method="POST">
<input type="hidden" name="Progress2" value="100,90,80,70,60,50,40,30,20,10">
<input type="hidden" name="saleid" value="<?php echo $_COOKIE['Ses_ID']?>">
<input type="hidden" name="SpecialProject" value="0">
<input type="hidden" name="BidingYear" value="<?php echo date('Y')?>">
<input type="hidden" name="where" value="TimeFrameContractSigndate">
<section class="page-header bg-color white">
	<div class="container">
		<div class="row">
			<div class="span12">
				<div class="section-header clearfix">
					<div class="pull-left">
						<h2><i class="icon-export-4"></i>Export</h2>
					</div>
					<div id="shortcut" class="nav-collapse pull-right"><ul class="shortcuts unstyled">
					<?php include("menu.php");?>
					</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="content">
	<div class="page-tab">
		<div class="page-tab-header">
			<div class="container">
				<ul class="nav nav-tabs clearfix" id="socialTab">
					<!--<li class="active"><a href="#"><h5>Monthly Report</h5></a></li>
					<li><a href="export_SPA.php"><h5>Spacial Report</h5></a></li>
					<li><a href="export_Globetech.php"><h5>Globetech Report</h5></a></li>
					<li><a href="export_TM.php"><h5>TM Report</h5></a></li>
					<li><a href="export_GEO.php"><h5>GEO Report</h5></a></li>
					<li><a href="export_BF.php"><h5>Book&Forward Report</h5></a></li>-->
					<?php include("menu_export.php");?>
				</ul>
			</div>
		</div>
		<div class="tab-content">
			<div class="tab-pane fade active in container" id="monthly">
				<div class="widget bg-color turqoise rounded">
					<div class="box-padding">
						<h1>Monthly</h1>
						<p class="lead">All Project</p>
					</div>
				</div>
				<div class="row">
					<div class="span3 widget widget-option">
						<div class="profile-head bg-color dark-blue rounded-top">
							<div class="box-padding" style="text-align:center">
									<h3 class="normal"><i class="icon-calendar"></i>Year</h3>
									<span class=" pull-left" style="margin-left:-10px"></span>
							</div>
						</div>
						<div class="bg-color white rounded-bottom">
								<ul class="menu unstyled">
									<li ><p>
									  	<input type="radio"  class="optionsYear1" id="optionsRadios1" name="rdoYear1" value="TimeFrameBidingDate" style="margin-top:-2px">
									  	 Bidding Date <span class=" pull-right" style="margin-top:-8px"><select id="slc_year" name="slc_year" class="span1" style="width:80px;"></select></span></p></li>
									<li class="current"><p>
									  	<input type="radio" checked class="optionsYear2" id="optionsRadios1" name="rdoYear1" value="TimeFrameContractSigndate" style="margin-top:-2px">
									  	 Signdate <span class="pull-right" style="margin-top:-8px"><select id="slc_year_sign" name="slc_year_sign" class="span1" style="width:80px;"></select></span></p></li>
								</ul>
						</div>
						<div class="profile-head bg-color dark-blue rounded-top">
							<div class="box-padding" style="text-align:center">
									<h3 class="normal"><i class="icon-th-thumb-empty"></i>Option</h3>
							</div>
						</div>
						<div class="bg-color white rounded-bottom">
								<ul class="menu unstyled">
									<li><p class="sales">
									  	<input type="checkbox" value="1" style="margin-top:-2px" class="chkSpecial"> Mega Project<span class="pull-right"style="margin-top:-8px"></span></p></li>
								</ul>
						</div>

						<div style="text-align:center"><a class="btn btn-blue btn-large" id="btnExport" style="width:190px">Export</a></div>
						<div id="showWarning" class="alert alert-error hide" style="margin-top:10px;margin-bottom:-10px;">
							<button type="button" class="close" data-dismiss="alert">×</button>
								<i class="icon-attention-3 pull-left" style="font-size:20px;margin-top:-3px;"></i>
								<div class="media-body">
									<strong>กรุณาเลือก Progress และ Sale ด้วย</strong>
								</div>
						</div>
						<div id="warningGraph" class="alert alert-block hide" style="margin-top:10px;margin-bottom:-10px;">
							<button type="button" class="close" data-dismiss="alert">×</button>
							<i class="icon-info-circle pull-left" style="font-size:25px;margin-top:-7px;"></i>
							<div class="media-body">
								<strong>ไม่พบข้อมูล</strong>
							</div>
						</div>
						<!--Modal Select Sales-->
						<div class="modal hide fade" id="export_modal">
							<div class="alert alert-success">
							    <button type="button" class="close" data-dismiss="alert">×</button>
							    <div class="media">
							        <i class="icon-ok-circled pull-left" style="font-size:60px"></i>
							        <div class="media-body">
							            <h2 class="media-heading">Success!</h2>
							            <p class="lead">You successfully read this important alert message.</p>
							        </div>
							    </div>
							</div>
						</div>
					</div>
					<div class="span5">
						<div class="widget widget-profile">
							<!--<div class="widget-header clearfix">
								<span class="pull-left"><i class="icon-user-male"></i> Progress</span>
								<div class="dropdown pull-right">
								</div>
							</div>-->
							<div class="profile-head bg-color dark-blue rounded-top">
								<div class="box-padding">
									<h3 class="normal"><i class="icon-progress-3"></i>Progress</h3>
									<span class=" pull-right" style="margin-right:-10px;margin-top:-30px;"><input type="checkbox" id="progressAll" title="Select all"></span>
								</div>
							</div>

							<div class="bg-color white rounded-bottom">
								<ul class="menu unstyled">
									<li class="current"><p>100% Sign date<span class=" pull-right"><input type="checkbox" name="chkprog" value="100" class="progress" checked></span></p></li>
									<li class="current"><p>90% <b>ประกาศผลแล้วรอเซ็นสัญญา</b><span class="pull-right"><input type="checkbox" name="chkprog" value="90" class="progress" checked></span></p></li>
									<li class="current"><p>80% <b>ยื่นซองแล้ว</b><span class="pull-right"><input type="checkbox" name="chkprog" value="80" class="progress" checked ></span></p></li>
									<li class="current"><p>70% Bidding date<span class="pull-right"><input type="checkbox" name="chkprog" value="70" class="progress" checked></span></p></li>
									<li class="current"><p>60% TOR Final<span class="pull-right"><input type="checkbox" name="chkprog" value="60" class="progress" checked></span></p></li>
									<li class="current"><p>50% <b>ได้งบประมาณ</b><span class="pull-right"><input type="checkbox" name="chkprog" value="50" class="progress" checked></span></p></li>
									<li class="current"><p>40% <b>ขั้นตอนการพิจารณาอนุมัติงบประมาณ</b><span class="pull-right"><input type="checkbox" name="chkprog" value="40" class="progress" checked></span></p></li>
									<li class="current"><p>30% <b>ร่างโครงการ (ของบประมาณ)</b><span class="pull-right"><input type="checkbox" name="chkprog" value="30" class="progress" checked></span></p></li>
									<li class="current"><p>20% Presentation/Demo<span class="pull-right"><input type="checkbox" name="chkprog" value="20" class="progress" checked></span></p></li>
									<li class="current"><p>10% Build solution<span class="pull-right"><input type="checkbox" name="chkprog" value="10" class="progress"  checked></span></p></li>
									<li><p>0% <b>แพ้</b><span class="pull-right"><input type="checkbox" name="chkprog" value="0" class="progress" ></span></p></li>
									<li><p>V <b>ยกเลิกโครงการ</b><span class="pull-right"><input type="checkbox" name="chkprog" value="v" class="progress" ></span></p></li>
									<li><p>P <b>Pendding</b><span class="pull-right"><input type="checkbox" name="chkprog" value="p" class="progress" ></span></p></li>
									
								</ul>
							</div>

						</div>
					</div>

					<div class="span4 widget widget-option">
						<div class="tab-content" id="Graph">
								<div id="containerPie" style="min-width: 370px; height: 300px; margin: 0 auto;display:none" ></div>
								<div id="containerBar" style="min-width: 370px; height: 300px; margin: 0 auto"></div>
								<!--<div style="text-align:right" ><a style="margin:10px 0px;" class="btn btn-blue" id="btnSwitch" >Switch Graph</a></div>-->
						</div>
						<div class="profile-head bg-color dark-blue rounded-top">
							<div class="box-padding" style="text-align:center">
									<h3 class="normal"><i class="icon-users-1"></i>Sales</h3>
									<span class=" pull-left" style="margin-left:-10px"></span>
							</div>
						</div>

						<div class="bg-color white rounded-bottom">
							<div class="accordion" id="accordion2">
				                <div class="accordion-group">
				                  <div class="accordion-heading">
				                    <a id="asales1" class="accordion-toggle collapsed clearfix" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
				                      <h4 class="pull-left" style="margin-left:25px;">Sales 1</h4>
				                       <i id="isales1" class="icon-right-dir pull-right" style="color:#43484D; font-size:18px;"></i>
				                    </a>
				                    <span class="pull-left" style="margin-left:20px;margin-top:-36px"><input type="checkbox" class="Sales1All" title="Select all"></span>
				                  </div>
				                  <div id="collapseOne" class="accordion-body collapse in" >
				                    	<ul id="list_sale1" class="menu unstyled"></ul>
				                  </div>
				                </div>
				                <div class="accordion-group">
				                  <div class="accordion-heading">
				                    <a id="asales2" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">				                      
				                      <h4 class="pull-left" style="margin-left:25px;">Sales 2</h4>
				                       <i id="isales2" class="icon-right-dir pull-right" style="color:#43484D; font-size:18px;"></i>
				                    </a>
				                    <span class="pull-left" style="margin-left:20px;margin-top:-36px"><input type="checkbox" class="Sales2All" title="Select all"></span>
				                    </a>
				                  </div>
				                  <div id="collapseTwo" class="accordion-body collapse in">
				                    	<ul id="list_sale2" class="menu unstyled"></ul>
				                  </div>
				                </div>
				                <div class="accordion-group">
				                  <div class="accordion-heading">
				                    <a id="asales4" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
				                      <h4 class="pull-left" style="margin-left:25px;">Sales 4</h4>
				                       <i id="isales4" class="icon-right-dir pull-right" style="color:#43484D; font-size:18px;"></i>
				                    </a>
				                    <span class="pull-left" style="margin-left:20px;margin-top:-36px"><input type="checkbox" class="Sales4All" title="Select all"></span>
				                    </a>
				                  </div>
				                  <div id="collapseThree" class="accordion-body collapse in">
				                    	<ul id="list_sale4" class="menu unstyled"></ul>
				                  </div>
				                </div>

				                <div class="accordion-group">
				                  <div class="accordion-heading">
				                    <a id="asalesTM" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion2" href="#collapseFour">
				                      <h4 class="pull-left" style="margin-left:25px;">Sales TM</h4>
				                       <i id="isalesTM" class="icon-right-dir pull-right" style="color:#43484D; font-size:18px;"></i>
				                    </a>
				                    <span class="pull-left" style="margin-left:20px;margin-top:-36px"><input type="checkbox" class="SalesTMAll" title="Select all"></span>
				                    </a>
				                  </div>
								  <div id="collapseFour" class="accordion-body collapse in">
				                    	<ul id="list_saleTM" class="menu unstyled">
										</ul>
				                  </div>
				                </div>
								
								<div class="accordion-group">
				                  <div class="accordion-heading">
				                    <a id="asalesCSD" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion2" href="#collapseFive">
				                      <h4 class="pull-left" style="margin-left:25px;">Sales CSD</h4>
				                       <i id="isalesCSD" class="icon-right-dir pull-right" style="color:#43484D; font-size:18px;"></i>
				                    </a>
				                    <span class="pull-left" style="margin-left:20px;margin-top:-36px"><input type="checkbox" class="SalesCSDAll" title="Select all"></span>
				                    </a>
				                  </div>
								  <div id="collapseFive" class="accordion-body collapse in">
				                    	<ul id="list_saleCSD" class="menu unstyled">
										</ul>
				                  </div>
				                </div>
								
								<div class="accordion-group">
				                  <div class="accordion-heading">
				                    <a id="asalesGEO" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion2" href="#collapseSix">
				                      <h4 class="pull-left" style="margin-left:25px;">Sales GEO</h4>
				                       <i id="isalesGEO" class="icon-right-dir pull-right" style="color:#43484D; font-size:18px;"></i>
				                    </a>
				                    <span class="pull-left" style="margin-left:20px;margin-top:-36px"><input type="checkbox" class="SalesGEOAll" title="Select all"></span>
				                    </a>
				                  </div>
								  <div id="collapseSix" class="accordion-body collapse in">
				                    	<ul id="list_saleGEO" class="menu unstyled">
										</ul>
				                  </div>
				                </div>
								<div class="accordion-group">
				                  <div class="accordion-heading">
				                    <a id="asalesCLMV" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion2" href="#collapseSeven">
				                      <h4 class="pull-left" style="margin-left:25px;">Sales CLMV</h4>
				                       <i id="isalesCLMV" class="icon-right-dir pull-right" style="color:#43484D; font-size:18px;"></i>
				                    </a>
				                    <span class="pull-left" style="margin-left:20px;margin-top:-36px"><input type="checkbox" class="SalesCLMVAll" title="Select all"></span>
				                    </a>
				                  </div>
								  <div id="collapseSeven" class="accordion-body collapse in">
				                    	<ul id="list_saleCLMV" class="menu unstyled">
										</ul>
				                  </div>
				                </div>
				            </div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
		<div id="footer" class="bg-color dark-blue">
			<div class="container">
				<div class="box-padding">
					Copyright &copy; 2013 Sales Forecast
				</div>
			</div>
		</div>
		<!-- Javascript -->
		<!-- JS:jquery-->
		<!--<script src="../../../../ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>-->
  		<script src="js/library/jquery/jquery.min.js"></script>
		<script src="js/library/jquery/jquery-ui.min.js"></script>

		<!-- JS:masonry-->
		<script src="js/library/masonry/jquery.masonry.min.js"></script>

		<!-- JS:flexslider-->
		<script src="js/library/flexslider/jquery.flexslider-min.js"></script>

		<!-- JS:tables-->
		<script src="js/library/dataTables/jquery.dataTables.min.js"></script>

		<!-- JS:calendar-->
		<script src="js/library/fullcalendar/fullcalendar.min.js"></script>
		<script src="js/library/fullcalendar/gcal.js"></script>

		<!-- JS:forms-->
		<script src="js/library/forms/jquery.hotkeys.js"></script>
		<script src="js/library/forms/bootstrap-wysiwyg.js"></script>
		<script src="js/library/forms/picker.js"></script>
		<script src="js/library/forms/picker.date.js"></script>
		<script src="js/library/forms/picker.time.js"></script>
		<script src="js/library/forms/jquery.bootstrap.wizard.min.js"></script>

		<!-- JS:charts-->
		<script src="js/library/charts/jquery.flot.js"></script>
		<script src="js/library/charts/jquery.flot.resize.js"></script>
		<script src="js/library/charts/jquery.flot.stack.js"></script>
		<script src="js/library/charts/jquery.flot.pie.min.js"></script>
		<script src="js/library/charts/jquery.sparkline.min.js"></script>
		<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="excanvas.min.js"></script><![endif]-->

		<!-- JS:syntaxHighlighter-->
		<script src="js/library/syntaxHighlighter/shCore.js"></script>
		<script src="js/library/syntaxHighlighter/shBrushCss.js"></script>
		<script src="js/library/syntaxHighlighter/shBrushXml.js"></script>
		<script src="js/library/syntaxHighlighter/shBrushJScript.js"></script>

		<!-- JS:bootstrap-->
		<script src="bootstrap/js/bootstrap.min.js"></script>
		
		<!-- JS:custom js for this template -->
		<script src="js/styler/custom.js"></script>
		<script src="js/highcharts.js"></script>
		<script src="js/exporting.js"></script>
		<script type="text/javascript">
			var titleYear = '2013';
			var check1='';
			var check2='';
			var check3='';
			var check4='';
			var check5='';
			var check6='';
			var check7='';
			var _loginID = "<?php echo $_COOKIE['Ses_ID']?>";
			Number.prototype.nformat = function() {
			    return this.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
			};
		    $(document).ready(function(){
		    	$("#Graph").css("display","none");
				$("#slc_year").disableSelection();
		    	$('.widget-sales').on('click','#loadsales',function(){
		        	$('#myModal').modal('show');
		    	});
		    	$.ajax({ 
		    		cache: false,
					type: "POST",
					dataType: "json",
		        	url: "AJAX/select_year.php",
		        	success: function (json) {
		        		$.each(json, function() {
							var item = this;
		        			$("#slc_year").append("<option value='" + item[0] + "'>" + item[0] + "</option>");
						});
						$("#slc_year").val((new Date).getFullYear());
			        }
			    });
			    $.ajax({ 
			    	cache: false,
					type: "POST",
					dataType: "json",
		        	url: "AJAX/select_yearSign.php",
		        	success: function (json) {
		        		$.each(json, function() {
							var item = this;
		        			$("#slc_year_sign").append("<option value='" + item[0] + "'>" + item[0] + "</option>");
						});
						$("#slc_year_sign").val((new Date).getFullYear());
			        }
			    });
				//sale1
				$.ajax({ 
					type:"POST",
					dataType: "json",
					data:{
						saledept : "sales 1"
					},
			        url: "AJAX/SV_Select_SaleDept.php",
			        success: function (json) {
						var chkdep = "0";
						//console.log(json);
			        	$(json).each(function () {

							//console.log(this);
							var empno=this[0];
							var name=this[1];
							if(empno == _loginID){
								checked = "checked";
								classli = "current";
								chkdep = "1";
							}else{
								checked = "";
								classli = "";
							}
			        		$("#list_sale1").append("<li class='" + classli + "'><p style='padding:5px 20px;'><input type='checkbox' class='chksales1' style='margin-top:-2px' name='chkids' value='" + empno + "' " + checked + " /> " + name + "</p></li>");
						});
						if(chkdep == '1'){
							$('#asales1').removeClass("collapsed");
							$('#isales1').removeClass("icon-right-dir").addClass("icon-down-dir");
							$('#collapseOne').addClass("in");
							$('#collapseOne').css("height","auto");
							$('#isales1').removeClass("icon-right-dir").addClass("icon-down-dir");

							$(".Sales1All").prop("checked", true);
							$(".chksales1").prop("checked", true);
							$('.chksales1').closest('li').addClass("current");	
							getSaleIDNO();
						}
						$('.Sales1All').on('click',function(){
						if ($(".Sales1All").is(':checked')) {
							$(".chksales1").prop("checked", true);
							$('.chksales1').closest('li').addClass("current");
							$('#asales1').removeClass("collapsed");
							$('#isales1').removeClass("icon-right-dir").addClass("icon-down-dir");
							$('#collapseOne').addClass("in");
							$('#collapseOne').css("height","auto");
							getSaleID();
						} else {
							$(".chksales1").prop("checked", false);
							$('.chksales1').closest('li').removeClass("current");
							/*$('#asales1').addClass("collapsed");
							$('#isales1').removeClass("icon-down-dir").addClass("icon-right-dir");
							$('#collapseOne').removeClass("in");
							$('#collapseOne').css("height","0px");
							$('#showWarning').removeClass("hide");*/
							showGraph();
						}
			    	});
					$('.chksales1').on('click',function(){
						if ( $(this).prop('checked')) {
							$(this).closest('li').attr("class", "current");
					    } else {
							$(this).closest('li').attr("class", "");
					    }
						
						if( $(".chksales1:checked").length == $(".chksales1").length){
							$(".Sales1All").prop("checked", true);
						}else{
							$(".Sales1All").prop("checked", false);
						}
						getSaleID();
					});
					$('#asales1').on('click',function(){
						if($("#collapseOne").css("height") == "0px") { 
							$('#isales1').removeClass("icon-right-dir").addClass("icon-down-dir");
					    } else {
							$('#isales1').removeClass("icon-down-dir").addClass("icon-right-dir");
					    }
					});
					check1 = '1';
					startFunction();
			        }
			    });
				/*$.ajax({ 
					dataType: "json",
			        url: "http://157.179.28.116/sfs/sv.svc/Select_SaleDept?saledept=sales 1&callback=?",
			        success: function (json) {
						var chkdep = "0";
			        	$(json.items).each(function () {
							if(this.empno == _loginID){
								checked = "checked";
								classli = "current";
								chkdep = "1";
							}else{
								checked = "";
								classli = "";
							}
			        		$("#list_sale1").append("<li class='" + classli + "'><p style='padding:5px 20px;'><input type='checkbox' class='chksales1' style='margin-top:-2px' name='chkids' value='" + this.empno + "' " + checked + " /> " + this.name + "</p></li>");
						});
						if(chkdep == '1'){
							$('#asales1').removeClass("collapsed");
							$('#isales1').removeClass("icon-right-dir").addClass("icon-down-dir");
							$('#collapseOne').addClass("in");
							$('#collapseOne').css("height","auto");
							$('#isales1').removeClass("icon-right-dir").addClass("icon-down-dir");

							$(".Sales1All").prop("checked", true);
							$(".chksales1").prop("checked", true);
							$('.chksales1').closest('li').addClass("current");	
							getSaleIDNO();
						}
						$('.Sales1All').on('click',function(){
						if ($(".Sales1All").is(':checked')) {
							$(".chksales1").prop("checked", true);
							$('.chksales1').closest('li').addClass("current");
							$('#asales1').removeClass("collapsed");
							$('#isales1').removeClass("icon-right-dir").addClass("icon-down-dir");
							$('#collapseOne').addClass("in");
							$('#collapseOne').css("height","auto");
							getSaleID();
						} else {
							$(".chksales1").prop("checked", false);
							$('.chksales1').closest('li').removeClass("current");
							showGraph();
						}
			    	});
					$('.chksales1').on('click',function(){
						if ( $(this).prop('checked')) {
							$(this).closest('li').attr("class", "current");
					    } else {
							$(this).closest('li').attr("class", "");
					    }
						
						if( $(".chksales1:checked").length == $(".chksales1").length){
							$(".Sales1All").prop("checked", true);
						}else{
							$(".Sales1All").prop("checked", false);
						}
						getSaleID();
					});
					$('#asales1').on('click',function(){
						if($("#collapseOne").css("height") == "0px") { 
							$('#isales1').removeClass("icon-right-dir").addClass("icon-down-dir");
					    } else {
							$('#isales1').removeClass("icon-down-dir").addClass("icon-right-dir");
					    }
					});
					check1 = '1';
					startFunction();
			        }
			    });*/
				//sale2
				$.ajax({ 
					type:"POST",
					dataType: "json",
					data:{
						saledept : "sales 2"
					},
			        url: "AJAX/SV_Select_SaleDept.php",
			        success: function (json) {
			        	var chkdep = "0";
			        	$(json).each(function () {

							//console.log(this);
							var empno=this[0];
							var name=this[1];
							if(empno == _loginID){
								checked = "checked";
								classli = "current";
								chkdep = "1";
							}else{
								checked = "";
								classli = "";
							}
			        		$("#list_sale2").append("<li class='" + classli + "'><p style='padding:5px 20px;'><input type='checkbox' class='chksales2' style='margin-top:-2px' name='chkids' value='" + empno + "' " + checked + " /> " + name + "</p></li>");
						});
						if(chkdep == '1'){
							$('#asales2').removeClass("collapsed");
							$('#isales2').removeClass("icon-right-dir").addClass("icon-down-dir");
							$('#collapseTwo').addClass("in");
							$('#collapseTwo').css("height","auto");
							$('#isales2').removeClass("icon-right-dir").addClass("icon-down-dir");	

							$(".Sales2All").prop("checked", true);
							$(".chksales2").prop("checked", true);
							$('.chksales2').closest('li').addClass("current");	
							getSaleIDNO();	
						}
					$('.Sales2All').on('click',function(){
						if ($(".Sales2All").is(':checked')) {
							$(".chksales2").prop("checked", true);
							$('.chksales2').closest('li').addClass("current");
							$('#asales2').removeClass("collapsed");
							$('#isales2').removeClass("icon-right-dir").addClass("icon-down-dir");
							$('#collapseTwo').addClass("in");
							$('#collapseTwo').css("height","auto");
							getSaleID();
						} else {
							$(".chksales2").prop("checked", false);
							$('.chksales2').closest('li').removeClass("current");
							/*$('#asales2').addClass("collapsed");
							$('#isales2').removeClass("icon-down-dir").addClass("icon-right-dir");
							$('#collapseTwo').removeClass("in");
							$('#collapseTwo').css("height","0px");
							$('#showWarning').removeClass("hide");*/
							showGraph();
						}
			    	});
					$('.chksales2').on('click',function(){
						if ( $(this).prop('checked')) {
							$(this).closest('li').addClass("current");
					    } else {
							$(this).closest('li').removeClass("current");
					    }
						if( $(".chksales2:checked").length == $(".chksales2").length){
							$(".Sales2All").prop("checked", true);
						}else{
							$(".Sales2All").prop("checked", false);
						}
						getSaleID();
					});
					$('#asales2').on('click',function(){
						if($("#collapseTwo").css("height") == "0px") { 
							$('#isales2').removeClass("icon-right-dir").addClass("icon-down-dir");
					    } else {
							$('#isales2').removeClass("icon-down-dir").addClass("icon-right-dir");
					    }
					});
					check2='1';
					startFunction();
			        }
			    });
				//sale4
				$.ajax({ 
					type:"POST",
					dataType: "json",
					data:{
						saledept : "sales 4"
					},
			        url: "AJAX/SV_Select_SaleDept.php",
			        success: function (json) {
			        	var chkdep = "0";
			        	$(json).each(function () {

							//console.log(this);
							var empno=this[0];
							var name=this[1];
							if(empno == _loginID){
								checked = "checked";
								classli = "current";
								chkdep = "1";
							}else{
								checked = "";
								classli = "";
							}
			        		$("#list_sale4").append("<li class='" + classli + "'><p style='padding:5px 20px;'><input type='checkbox' class='chksales4' style='margin-top:-2px' name='chkids' value='" + empno + "' " + checked + " /> " + name + "</p></li>");
						});
						if(chkdep == '1'){
							$('#asales4').removeClass("collapsed");
							$('#isales4').removeClass("icon-right-dir").addClass("icon-down-dir");
							$('#collapseThree').addClass("in");
							$('#collapseThree').css("height","auto");
							$('#isales4').removeClass("icon-right-dir").addClass("icon-down-dir");	

							$(".Sales4All").prop("checked", true);
							$(".chksales4").prop("checked", true);
							$('.chksales4').closest('li').addClass("current");	
							getSaleIDNO();
						}
					$('.Sales4All').on('click',function(){
						if ($(".Sales4All").is(':checked')) {
							$(".chksales4").prop("checked", true);
							$('.chksales4').closest('li').addClass("current");
							$('#asales4').removeClass("collapsed");
							$('#isales4').removeClass("icon-right-dir").addClass("icon-down-dir");
							$('#collapseThree').addClass("in");
							$('#collapseThree').css("height","auto");
							getSaleID();
						} else {
							$(".chksales4").prop("checked", false);
							$('.chksales4').closest('li').removeClass("current");
							/*$('#asales4').addClass("collapsed");
							$('#isales4').removeClass("icon-down-dir").addClass("icon-right-dir");
							$('#collapseThree').removeClass("in");
							$('#collapseThree').css("height","0px");
							$('#showWarning').removeClass("hide");*/
							showGraph();
							
						}
			    	});
					$('.chksales4').on('click',function(){
						if ( $(this).prop('checked')) {
							$(this).closest('li').addClass("current");
					    } else {
							$(this).closest('li').removeClass("current");
					    }
						if( $(".chksales4:checked").length == $(".chksales4").length){
							$(".Sales4All").prop("checked", true);
						}else{
							$(".Sales4All").prop("checked", false);
						}
						getSaleID();
					});
					$('#asales4').on('click',function(){
						if($("#collapseThree").css("height") == "0px") { 
							$('#isales4').removeClass("icon-right-dir").addClass("icon-down-dir");
					    } else {
							$('#isales4').removeClass("icon-down-dir").addClass("icon-right-dir");
					    }
					});
					check3='1';
					startFunction();
			        }
			    });
				//saleTM
				$.ajax({ 
					type:"POST",
					dataType: "json",
					data:{
						saledept : "sales tm"
					},
			        url: "AJAX/SV_Select_SaleDept.php",
			        success: function (json) {
			        	var chkdep = "0";
			        	$(json).each(function () {

							//console.log(this);
							var empno=this[0];
							var name=this[1];
							if(empno == _loginID){
								checked = "checked";
								classli = "current";
								chkdep = "1";
							}else{
								checked = "";
								classli = "";
							}
			        		$("#list_saleTM").append("<li class='" + classli + "'><p style='padding:5px 20px;'><input type='checkbox' class='chksalesTM' style='margin-top:-2px' name='chkids' value='" + empno + "' " + checked + " /> " + name + "</p></li>");
						});
						if(chkdep == '1'){
							$('#asalesTM').removeClass("collapsed");
							$('#isalesTM').removeClass("icon-right-dir").addClass("icon-down-dir");
							$('#collapseFour').addClass("in");
							$('#collapseFour').css("height","auto");
							$('#isalesTM').removeClass("icon-right-dir").addClass("icon-down-dir");	

							$(".SalesTMAll").prop("checked", true);
							$(".chksalesTM").prop("checked", true);
							$('.chksalesTM').closest('li').addClass("current");	
							getSaleIDNO();
						}
					$('.SalesTMAll').on('click',function(){
						if ($(".SalesTMAll").is(':checked')) {
							$(".chksalesTM").prop("checked", true);
							$('.chksalesTM').closest('li').addClass("current");
							$('#asalesTM').removeClass("collapsed");
							$('#isalesTM').removeClass("icon-right-dir").addClass("icon-down-dir");
							$('#collapseFour').addClass("in");
							$('#collapseFour').css("height","auto");
							getSaleID();
						} else {
							$(".chksalesTM").prop("checked", false);
							$('.chksalesTM').closest('li').removeClass("current");
							/*$('#asalesTM').addClass("collapsed");
							$('#isalesTM').removeClass("icon-down-dir").addClass("icon-right-dir");
							$('#collapseFour').removeClass("in");
							$('#collapseFour').css("height","0px");
							$('#showWarning').removeClass("hide");*/
							showGraph();
						}
					});
					$('.chksalesTM').on('click',function(){
						if ( $(this).prop('checked')) {
							$(this).closest('li').addClass("current");
						} else {
							$(this).closest('li').removeClass("current");
						}
						if( $(".chksalesTM:checked").length == $(".chksalesTM").length){
							$(".SalesTMAll").prop("checked", true);
						}else{
							$(".SalesTMAll").prop("checked", false);
						}
						getSaleID();
					});
					$('#asalesTM').on('click',function(){
						if($("#collapseFour").css("height") == "0px") { 
							$('#isalesTM').removeClass("icon-right-dir").addClass("icon-down-dir");
						} else {
							$('#isalesTM').removeClass("icon-down-dir").addClass("icon-right-dir");
						}
					});
					check4 ='1';
					startFunction();
			    	}
			    });
				
				//saleCSD
				$.ajax({ 
					type:"POST",
					dataType: "json",
					data:{
						saledept : "sales CSD"
					},
			        url: "AJAX/SV_Select_SaleDept.php",
			        success: function (json) {
			        	var chkdep = "0";
			        	$(json).each(function () {

							//console.log(this);
							var empno=this[0];
							var name=this[1];
							if(empno == _loginID){
								checked = "checked";
								classli = "current";
								chkdep = "1";
							}else{
								checked = "";
								classli = "";
							}
			        		$("#list_saleCSD").append("<li class='" + classli + "'><p style='padding:5px 20px;'><input type='checkbox' class='chksalesCSD' style='margin-top:-2px' name='chkids' value='" + empno + "' " + checked + " /> " + name + "</p></li>");
						});
						if(chkdep == '1'){
							$('#asalesCSD').removeClass("collapsed");
							$('#isalesCSD').removeClass("icon-right-dir").addClass("icon-down-dir");
							$('#collapseFive').addClass("in");
							$('#collapseFive').css("height","auto");
							$('#isalesCSD').removeClass("icon-right-dir").addClass("icon-down-dir");	

							$(".SalesCSDAll").prop("checked", true);
							$(".chksalesCSD").prop("checked", true);
							$('.chksalesCSD').closest('li').addClass("current");	
							getSaleIDNO();
						}
					$('.SalesCSDAll').on('click',function(){
						if ($(".SalesCSDAll").is(':checked')) {
							$(".chksalesCSD").prop("checked", true);
							$('.chksalesCSD').closest('li').addClass("current");
							$('#asalesCSD').removeClass("collapsed");
							$('#isalesCSD').removeClass("icon-right-dir").addClass("icon-down-dir");
							$('#collapseFive').addClass("in");
							$('#collapseFive').css("height","auto");
							getSaleID();
						} else {
							$(".chksalesCSD").prop("checked", false);
							$('.chksalesCSD').closest('li').removeClass("current");
							/*$('#asalesCSD').addClass("collapsed");
							$('#isalesCSD').removeClass("icon-down-dir").addClass("icon-right-dir");
							$('#collapseFive').removeClass("in");
							$('#collapseFive').css("height","0px");
							$('#showWarning').removeClass("hide");*/
							showGraph();
						}
					});
					$('.chksalesCSD').on('click',function(){
						if ( $(this).prop('checked')) {
							$(this).closest('li').addClass("current");
						} else {
							$(this).closest('li').removeClass("current");
						}
						if( $(".chksalesCSD:checked").length == $(".chksalesCSD").length){
							$(".SalesCSDAll").prop("checked", true);
						}else{
							$(".SalesCSDAll").prop("checked", false);
						}
						getSaleID();
					});
					$('#asalesCSD').on('click',function(){
						if($("#collapseFive").css("height") == "0px") { 
							$('#isalesCSD').removeClass("icon-right-dir").addClass("icon-down-dir");
						} else {
							$('#isalesCSD').removeClass("icon-down-dir").addClass("icon-right-dir");
						}
					});
					check5 ='1';
					startFunction();
			    	}
			    });
				//sale CLMV
				$.ajax({ 
					type:"POST",
					dataType: "json",
					data:{
						saledept : "sales CLMV"
					},
			        url: "AJAX/SV_Select_SaleDept.php",
			        success: function (json) {
			        	var chkdep = "0";
			        	$(json).each(function () {

							//console.log(this);
							var empno=this[0];
							var name=this[1];
							if(empno == _loginID){
								checked = "checked";
								classli = "current";
								chkdep = "1";
							}else{
								checked = "";
								classli = "";
							}
			        		$("#list_saleCLMV").append("<li class='" + classli + "'><p style='padding:5px 20px;'><input type='checkbox' class='chksalesCLMV' style='margin-top:-2px' name='chkids' value='" + empno + "' " + checked + " /> " + name + "</p></li>");
						});
						if(chkdep == '1'){
							$('#asalesCLMV').removeClass("collapsed");
							$('#isalesCLMV').removeClass("icon-right-dir").addClass("icon-down-dir");
							$('#collapseSeven').addClass("in");
							$('#collapseSeven').css("height","auto");
							$('#isalesCLMV').removeClass("icon-right-dir").addClass("icon-down-dir");	

							$(".SalesCLMVAll").prop("checked", true);
							$(".chksalesCLMV").prop("checked", true);
							$('.chksalesCLMV').closest('li').addClass("current");	
							getSaleIDNO();
						}
					$('.SalesCLMVAll').on('click',function(){
						if ($(".SalesCLMVAll").is(':checked')) {
							$(".chksalesCLMV").prop("checked", true);
							$('.chksalesCLMV').closest('li').addClass("current");
							$('#asalesCLMV').removeClass("collapsed");
							$('#isalesCLMV').removeClass("icon-right-dir").addClass("icon-down-dir");
							$('#collapseSeven').addClass("in");
							$('#collapseSeven').css("height","auto");
							getSaleID();
						} else {
							$(".chksalesCLMV").prop("checked", false);
							$('.chksalesCLMV').closest('li').removeClass("current");
							/*$('#asalesTM').addClass("collapsed");
							$('#isalesTM').removeClass("icon-down-dir").addClass("icon-right-dir");
							$('#collapseFour').removeClass("in");
							$('#collapseFour').css("height","0px");
							$('#showWarning').removeClass("hide");*/
							showGraph();
						}
					});
					$('.chksalesCLMV').on('click',function(){
						if ( $(this).prop('checked')) {
							$(this).closest('li').addClass("current");
						} else {
							$(this).closest('li').removeClass("current");
						}
						if( $(".chksalesCLMV:checked").length == $(".chksalesCLMV").length){
							$(".SalesCLMVAll").prop("checked", true);
						}else{
							$(".SalesCLMVAll").prop("checked", false);
						}
						getSaleID();
					});
					$('#asalesCLMV').on('click',function(){
						if($("#collapseSeven").css("height") == "0px") { 
							$('#isalesCLMV').removeClass("icon-right-dir").addClass("icon-down-dir");
						} else {
							$('#isalesCLMV').removeClass("icon-down-dir").addClass("icon-right-dir");
						}
					});
					check7 ='1';
					startFunction();
			    	}
			    });
				
				//saleGEO
				$.ajax({ 
					type:"POST",
					dataType: "json",
					data:{
						saledept : "sales geo"
					},
			        url: "AJAX/SV_Select_SaleDept.php",
			        success: function (json) {
			        	var chkdep = "0";
			        	$(json).each(function () {

							//console.log(this);
							var empno=this[0];
							var name=this[1];
							if(empno == _loginID){
								checked = "checked";
								classli = "current";
								chkdep = "1";
							}else{
								checked = "";
								classli = "";
							}
			        		$("#list_saleGEO").append("<li class='" + classli + "'><p style='padding:5px 20px;'><input type='checkbox' class='chksalesGEO' style='margin-top:-2px' name='chkids' value='" + empno + "' " + checked + " /> " + name + "</p></li>");
						});
						if(chkdep == '1'){
							$('#asalesGEO').removeClass("collapsed");
							$('#isalesGEO').removeClass("icon-right-dir").addClass("icon-down-dir");
							$('#collapseSix').addClass("in");
							$('#collapseSix').css("height","auto");
							$('#isalesGEO').removeClass("icon-right-dir").addClass("icon-down-dir");	

							$(".SalesGEOAll").prop("checked", true);
							$(".chksalesGEO").prop("checked", true);
							$('.chksalesGEO').closest('li').addClass("current");	
							getSaleIDNO();
						}
					$('.SalesGEOAll').on('click',function(){
						if ($(".SalesGEOAll").is(':checked')) {
							$(".chksalesGEO").prop("checked", true);
							$('.chksalesGEO').closest('li').addClass("current");
							$('#asalesGEO').removeClass("collapsed");
							$('#isalesGEO').removeClass("icon-right-dir").addClass("icon-down-dir");
							$('#collapseSix').addClass("in");
							$('#collapseSix').css("height","auto");
							getSaleID();
						} else {
							$(".chksalesGEO").prop("checked", false);
							$('.chksalesGEO').closest('li').removeClass("current");
							/*$('#asalesTM').addClass("collapsed");
							$('#isalesTM').removeClass("icon-down-dir").addClass("icon-right-dir");
							$('#collapseFour').removeClass("in");
							$('#collapseFour').css("height","0px");
							$('#showWarning').removeClass("hide");*/
							showGraph();
						}
					});
					$('.chksalesGEO').on('click',function(){
						if ( $(this).prop('checked')) {
							$(this).closest('li').addClass("current");
						} else {
							$(this).closest('li').removeClass("current");
						}
						if( $(".chksalesGEO:checked").length == $(".chksalesGEO").length){
							$(".SalesGEOAll").prop("checked", true);
						}else{
							$(".SalesGEOAll").prop("checked", false);
						}
						getSaleID();
					});
					$('#asalesGEO').on('click',function(){
						if($("#collapseSix").css("height") == "0px") { 
							$('#isalesGEO').removeClass("icon-right-dir").addClass("icon-down-dir");
						} else {
							$('#isalesGEO').removeClass("icon-down-dir").addClass("icon-right-dir");
						}
					});
					check6 ='1';
					startFunction();
			    }
			    });
				
				
				//progress
	    		$("#progressAll").click(function () {
			          $('.progress').attr('checked', this.checked);
			          if(this.checked)
			          {			          	
			          	$('.progress').closest('li').attr("class", "current");
						document.frmexport.Progress2.value="100,90,80,70,60,50,40,30,20,10,0,v,P";
						showGraph();
			          }
			          else
			          {			          	
			          	$('.progress').closest('li').removeAttr("class");
						$('#showWarning').removeClass("hide");
						showGraph();
			          }
			    });
			    $(".progress").click(function(){
			 
			        if($(".progress").length == $(".progress:checked").length) {
			            $("#progressAll").attr("checked", "checked");
			        } else {
			            $("#progressAll").removeAttr("checked");
			        }
			        if(this.checked)
			        {			          	
			          	$(this).closest('li').attr("class", "current");
			        }
			        else
			        {			          	
			         	$(this).closest('li').removeAttr("class");
			        }
					getProgress();					
			    });
				//year
				$('.optionsYear1').on('click',function(){
					$('.optionsYear1').closest('li').attr("class", "current");
					$('.optionsYear2').closest('li').attr("class", "");
					$("#slc_year_sign").disableSelection();
					$("#slc_year").enableSelection();
					document.frmexport.BidingYear.value=document.frmexport.slc_year.value;
					document.frmexport.where.value=document.frmexport.rdoYear1[0].value;
					showGraph();
				});
				$('.optionsYear2').on('click',function(){
					$('.optionsYear1').closest('li').attr("class", "");
					$('.optionsYear2').closest('li').attr("class", "current");
					$("#slc_year").disableSelection();
					$("#slc_year_sign").enableSelection();
					document.frmexport.BidingYear.value=document.frmexport.slc_year_sign.value;
					document.frmexport.where.value=document.frmexport.rdoYear1[1].value;
					showGraph();
				});
				//Special
				$('.chkSpecial').on('click',function(){
					if ( $(this).prop('checked')) {
						$(this).closest('li').attr("class", "current");
						document.frmexport.SpecialProject.value="0,1";
					}else{
						$(this).closest('li').attr("class", "");
						document.frmexport.SpecialProject.value="0";
					}
					showGraph();
				});
				$("#slc_year").change(function(){
					document.frmexport.BidingYear.value=document.frmexport.slc_year.value;
					document.frmexport.where.value=document.frmexport.rdoYear1[0].value;
					showGraph();
				});
				$("#slc_year_sign").change(function(){
					document.frmexport.BidingYear.value=document.frmexport.slc_year_sign.value;
					document.frmexport.where.value=document.frmexport.rdoYear1[1].value;
					showGraph();
				});
				function getSaleID(){
						var chkid='';
						$("input[name='chkids']").each(function () {
								if ( $(this).prop('checked')) {
									if(chkid=='')
									{
										chkid= $(this).val();
									}
									else
									{
										chkid = chkid+","+$(this).val();
									}
								}
						});
						document.frmexport.saleid.value=chkid;
						showGraph();
				}
				function getSaleIDNO(){
						var chkid='';
						$("input[name='chkids']").each(function () {
								if ( $(this).prop('checked')) {
									if(chkid=='')
									{
										chkid= $(this).val();
									}
									else
									{
										chkid = chkid+","+$(this).val();
									}
								}
						});
						document.frmexport.saleid.value=chkid;
						//showGraph();
				}
				function getProgress(){
						var chkidlist = '';
						$("input[name='chkprog']").each(function () {
								if ( $(this).prop('checked')) {
									if(chkidlist=='')
									{
										chkidlist= $(this).val();
									}
									else
									{
										chkidlist = chkidlist+","+$(this).val();
									}
								}
						});
						document.frmexport.Progress2.value=chkidlist;
						showGraph();
				}

				//click export
				$('#btnExport').on('click',function(){ 
					if($('#warningGraph').is(':hidden')) {
						$('#showWarning').addClass("hide");
						if ( (($(".chksalesTM:checked").length == 0) &&($(".chksales4:checked").length == 0) &&($(".chksales2:checked").length == 0) &&($(".chksales1:checked").length == 0)&&($(".chksalesCSD:checked").length == 0)&&($(".chksalesGEO:checked").length == 0)) || ($(".progress:checked").length == 0)) {
							$('#showWarning').removeClass("hide");
						}else{
							console.log("Export ",document.frmexport.BidingYear.value+' '+document.frmexport.where.value+' '+document.frmexport.Progress2.value+' '+document.frmexport.saleid.value+' '+document.frmexport.SpecialProject.value);
							document.frmexport.submit();
						}
					}
				});
				//
				//////////////////////////////show graph
			function startFunction(){
				if(check1 != '' && check2 != '' && check3 != '' && check4 != ''&& check5 != ''&& check6 != ''&& check7 != '' ){
					showGraph();//start
				}
			}
			$('#btnSwitch').on('click',function(){
				if($('#containerBar').is(':hidden')) {
					$("#containerBar").show();
					$("#containerPie").hide();
				}else{
					$("#containerBar").hide();
					$("#containerPie").show();
				}
			});
			
			function showGraph() {
				$("#loading").hide();
				/*$("#loading").show();
				console.log('progress:checked.length: ',$(".progress:checked").length);
				console.log('sale:checked.length: ',$("input[name='chkids']:checked").length);
				$('#showWarning').addClass("hide");
				$('#warningGraph').addClass("hide");
					if($(".progress:checked").length == 0 || $("input[name='chkids']:checked").length == 0){
						$('#showWarning').removeClass("hide");
						$('#Graph').hide();
					}else{
						var typeExport = '';
						if(document.frmexport.where.value=='TimeFrameBidingDate'){
							typeExport = 'bidding';
						}else{
							typeExport = 'signdate';
						}
						console.log("change ",document.frmexport.BidingYear.value+' '+document.frmexport.where.value+' '+document.frmexport.Progress2.value+' '+document.frmexport.saleid.value+' '+document.frmexport.SpecialProject.value);
					
						$.ajax({ 
						dataType: "json",
						url: "http://157.179.28.116/sfs/sv.svc/export_monthly?callback=?",
						data: {
							year: document.frmexport.BidingYear.value,
							saleid: document.frmexport.saleid.value,
							type: typeExport,
							special: document.frmexport.SpecialProject.value
						},
						success: function (json) {
						if(json.items.length == 0){
							$('#warningGraph').removeClass("hide");
							$('#Graph').hide();
						}else{
							$('#Graph').show();
							var dataBar ='';
							var dataPie ='';
							var dataArr=new Array(); 
							var cateArr=new Array(); 
							var count = 0;
							
							if($("input[name='chkprog'][value='100']").prop('checked')){
								dataArr[count]= json.items[0].P100;
								cateArr[count]= '100';
								count++;
							}
							
							if($("input[name='chkprog'][value='90']").prop('checked')){
								dataArr[count]= json.items[0].P90;
								cateArr[count]= '90';
								count++;
							}
							if($("input[name='chkprog'][value='80']").prop('checked')){
								dataArr[count]= json.items[0].P80;
								cateArr[count]= '80';
								count++;
							}
							if($("input[name='chkprog'][value='70']").prop('checked')){
								dataArr[count]= json.items[0].P70;
								cateArr[count]= '70';
								count++;
							}
							if($("input[name='chkprog'][value='60']").prop('checked')){
								dataArr[count]= json.items[0].P60;
								cateArr[count]= '60';
								count++;
							}
							if($("input[name='chkprog'][value='50']").prop('checked')){
								dataArr[count]= json.items[0].P50;
								cateArr[count]= '50';
								count++;
							}
							if($("input[name='chkprog'][value='40']").prop('checked')){
								dataArr[count]= json.items[0].P40;
								cateArr[count]= '40';
								count++;
							}
							if($("input[name='chkprog'][value='30']").prop('checked')){
								dataArr[count]= json.items[0].P30;
								cateArr[count]= '30';
								count++;
							}
							if($("input[name='chkprog'][value='20']").prop('checked')){
								dataArr[count]= json.items[0].P20;
								cateArr[count]= '20';
								count++;
							}
							if($("input[name='chkprog'][value='10']").prop('checked')){
								dataArr[count]= json.items[0].P10;
								cateArr[count]= '10';
								count++;
							}
							if($("input[name='chkprog'][value='0']").prop('checked')){
								dataArr[count]= json.items[0].P0;
								cateArr[count]= '0';
								count++;
							}
							if($("input[name='chkprog'][value='v']").prop('checked')){
								dataArr[count]= json.items[0].Pv;
								cateArr[count]= 'v';
								count++;
							}
							
							titleYear = json.items[0].year;
							//dataBar= [Bar100,Bar90,Bar80,Bar70,Bar60,Bar50,Bar40,Bar30,Bar20,Bar10,Bar0,Barv];
							//dataPie =[Pie100,Pie90,Pie80,Pie70,Pie60,Pie50,Bar40,Pie30,Bar20,Pie10,Pie0,Piev];
							RanderBarChart('containerBar', dataArr,cateArr);
							//RenderPieChart('containerPie', dataPie);
						}
						}
						});
					}
					$("#loading").hide();*/
				}
			function RenderPieChart(elementId, dataList) {
                new Highcharts.Chart({
                    chart: {
                        renderTo: elementId,
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false
                    }, 
                    credits:{
                    	enabled: false
                    },
                    exporting: {
			            enabled: false
			        },
                    title: {
                        text: titleYear
                    },
                    tooltip: {
                        formatter: function () {
                            return '<b>' + this.point.name + '</b> : ' + this.percentage.nformat() + ' %' ;
                        }
                    },
                    /*plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                color: '#000000',
                                connectorColor: '#000000',
                                formatter: function () {
                                    return '<b>' + this.point.name + '</b> : ' + this.point.y.nformat() ;
                                }
                            }
                        },
                        series: {                        	
							 events: {
			                    click: function(event) {
									//showdetail(event.point.name,event.point.y);
			                    }
			                }
                        }
                    },*/
                    series: [{
                        type: 'pie',
                        name: titleYear,
                        data: dataList
                    }],
                    
                    colors: [
					   '#749DD1', 
					   '#D67B76', 
					   '#C5DC96', 
					   '#AE9BC8', 
					   '#66BCD5', 
					   '#F19F55', 
					   '#84B5EF', 
					   '#8AB9EF', 
					   '#F89995', 
					   '#D8F4A0', 
					   '#C5B1E6', 
					   '#94EAFB', 
					   '#FEC283', 
					   '#CADDFB', 
					   '#FECCCD', 
					   '#EAFBCE'
					]
                });
				$("#loading").hide();
            }

			function RanderBarChart(elementId,dataList,categoriesList){
				$('#'+elementId).highcharts({

			        chart: {
			            type: 'column'
			        },			         
                    credits:{
                    	enabled: false
                    },
                    exporting: {
			            enabled: false
			        },
                    title: {
                        text: titleYear
                    },
			        xAxis: {
			            categories: categoriesList
			        },
			        /*plotOptions: {
			            series: {
			                allowPointSelect: true,
			                cursor: 'pointer',
			                point: {
			                    events: {
			                        click: function() {
										//showdetail(this.category,this.y);				
			                        }
			                    }
			                }
			            }
			        },*/

			        series: [{
			        	data: dataList      
			            ,name:"Income "        
			        }]
			    });
				$("#loading").hide();
			}
		    });
			
			$("#loading").hide();
		</script>
	</form>
  	</body>

<!-- Mirrored from bootstrapstyler.com/preview/_/flatdashboard/ by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 18 Oct 2013 07:00:04 GMT -->
</html>