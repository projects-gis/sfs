<!DOCTYPE html>
<html lang="en">
	
<!-- Mirrored from bootstrapstyler.com/preview/_/flatdashboard/ui.html by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 18 Oct 2013 07:02:36 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
		<meta charset="utf-8">
		<title>SFS:::Export->ESRI Product</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="Izuddin Helmi">

		<!-- Stylesheets -->
		<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
		<link href="js/library/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
		<link href="css/styler/style.css" rel="stylesheet">
		<link href="js/library/syntaxHighlighter/shCore.css" rel="stylesheet" type="text/css" />
		<link href="js/library/syntaxHighlighter/shThemeDefault.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="css/styler/fontello.css">
	    <link rel="stylesheet" href="css/styler/animation.css"><!--[if IE 7]>
	    <link rel="stylesheet" href="css/styler/fontello-ie7.css"><![endif]-->
		<!-- Custom Fonts -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300,500' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

		<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
		  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	</head>
<?php  
$checkmenu = '5';
$menuExport = '2';
?>
<body cz-shortcut-listen="true" style="margin-top:0px;">
<form name="frmexport" id="frmexport" action="ExportReport_SPA_potential.php" method="POST" >	
<input type="hidden" name="Progress2" value="100,90,80,70,60,50,40,30,20,10">
<input type="hidden" name="saleid" value="<?php echo $_COOKIE['Ses_ID']?>">
<input type="hidden" name="SpecialProject" value="0">
<input type="hidden" name="BidingYear" value="<?php echo date('Y')?>">
<input type="hidden" name="where" value="TimeFrameContractSigndate">
<input type="hidden" name="option" value="1">
<section class="page-header bg-color white">
	<div class="container">
		<div class="row">
			<div class="span12">
				<div class="section-header clearfix">
					<div class="pull-left">
						<h2><i class="icon-export-4"></i>Export</h2>
					</div>
					<div id="shortcut" class="nav-collapse pull-right"><ul class="shortcuts unstyled">
						<?php include("menu.php");?>
					</ul>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="content">
	<div class="page-tab">
		<div class="page-tab-header">
			<div class="container">
				<ul class="nav nav-tabs clearfix" id="socialTab">
					<?php include("menu_export.php");?>	
				</ul>
		</div>
		<div class="tab-content">
			<div class="tab-pane fade active in container" id="spacial">
				<div class="widget bg-color turqoise rounded">
					<div class="box-padding">
						<h1>ESRI Product</h1>
						<p class="lead"></p>
						<!-- <p class="lead">Choose Option Progress or Potential</p> -->
					</div>
				</div>
				<div class="row">
					<div class="span1 widget widget-option"></div>
					<div class="span3 widget widget-option">
						<div class="profile-head bg-color dark-blue rounded-top">
							<div class="box-padding" style="text-align:center">
									<h3 class="normal"><i class="icon-calendar"></i>Year</h3>
									<span class=" pull-left" style="margin-left:-10px"></span>
							</div>
						</div>
						<div class="bg-color white rounded-bottom">
								<ul class="menu unstyled">
									<li ><p>
									  	<input type="radio"  class="optionsYear1" id="optionsRadios1" name="rdoYear1" value="TimeFrameBidingDate" style="margin-top:-2px">
									  	 Bidding Date <span class=" pull-right" style="margin-top:-8px"><select id="slc_year" name="slc_year" class="span1" style="width:80px;"></select></span></p></li>
									<li class="current"><p>
									  	<input type="radio" checked class="optionsYear2" id="optionsRadios1" name="rdoYear1" style="margin-top:-2px" value="TimeFrameContractSigndate">
									  	 Signdate <span class="pull-right"style="margin-top:-8px"><select id="slc_year_sign" name="slc_year_sign" class="span1" style="width:80px;"></select></span></p></li>
								</ul>
						</div>
						<div class="profile-head bg-color dark-blue rounded-top">
							<div class="box-padding" style="text-align:center">
									<h3 class="normal"><i class="icon-th-thumb-empty"></i>Include</h3>
							</div>
						</div>
						<div class="bg-color white rounded-bottom">
								<ul class="menu unstyled">
									<li><p class="sales">
									  	<input type="checkbox" value="1" style="margin-top:-2px" class="chkSpecial"> Mega Project<span class="pull-right"style="margin-top:-8px"></span></p></li>
								</ul>
						</div>
						<div style="text-align:center"><a class="btn btn-blue btn-large" id="btnExport" style="width:190px">Export</a></div>
						<div id="showWarning" class="alert alert-error hide" style="margin-top:10px;margin-bottom:-10px;">
							<button type="button" class="close" data-dismiss="alert">×</button>
								<i class="icon-attention-3 pull-left" style="font-size:20px;margin-top:-3px;"></i>
								<div class="media-body">
									<strong>กรุณาเลือก Progress ด้วย</strong>
								</div>
						</div>
					</div>
					<div class="span3 widget widget-option">
						<div class="widget widget-profile">
							<div class="profile-head bg-color dark-blue rounded-top">
							<div class="box-padding" style="text-align:center">
									<h3 class="normal"><i class="icon-th-thumb-empty"></i>Option</h3>
							</div>
						</div>
						<div class="bg-color white rounded-bottom">
								<ul class="menu unstyled">
									<li class="current"><p class="sales">
									  	<input type="radio" value="0" style="margin-top:-2px" class="chkSpecial0" checked id="optionsRadios" name="rdoOption"> by Project<span class="pull-right"style="margin-top:-8px"></span></p></li>
									<li ><p class="sales">
									  	<input type="radio" value="1" style="margin-top:-2px" class="chkSpecial1" id="optionsRadios" name="rdoOption"> by Progress<span class="pull-right"style="margin-top:-8px"></span></p></li>
									<li ><p class="sales">
									  	<input type="radio" value="2" style="margin-top:-2px" class="chkSpecial2" id="optionsRadios" name="rdoOption"> by Potential<span class="pull-right"style="margin-top:-8px"></span></p></li>
									<li ><p class="sales">
									  	<input type="radio" value="3" style="margin-top:-2px" class="chkSpecial3" id="optionsRadios" name="rdoOption"> SW Monthly Report<span class="pull-right"style="margin-top:-8px"></span></p></li>

								</ul>
						</div>
					</div>
					
				</div>
					<div class="span4 widget widget-option">
						<div class="profile-head bg-color dark-blue rounded-top">
							<div class="box-padding" style="text-align:center">
									<h3 class="normal"><i class="icon-users-1"></i>Sales</h3>
									<span class=" pull-left" style="margin-left:-10px"></span>
							</div>
						</div>

						<div class="bg-color white rounded-bottom">
							<div class="accordion" id="accordion2">
				                <div class="accordion-group">
				                  <div class="accordion-heading">
				                    <a id="asales1" class="accordion-toggle collapsed clearfix" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
				                      <h4 class="pull-left" style="margin-left:25px;">Sales 1</h4>
				                       <i id="isales1" class="icon-right-dir pull-right" style="color:#43484D; font-size:18px;"></i>
				                    </a>
				                    <span class="pull-left" style="margin-left:20px;margin-top:-36px"><input type="checkbox" class="Sales1All" title="Select all"></span>
				                  </div>
				                  <div id="collapseOne" class="accordion-body collapse in" >
				                    	<ul id="list_sale1" class="menu unstyled"></ul>
				                  </div>
				                </div>
				                <div class="accordion-group">
				                  <div class="accordion-heading">
				                    <a id="asales2" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">				                      
				                      <h4 class="pull-left" style="margin-left:25px;">Sales 2</h4>
				                       <i id="isales2" class="icon-right-dir pull-right" style="color:#43484D; font-size:18px;"></i>
				                    </a>
				                    <span class="pull-left" style="margin-left:20px;margin-top:-36px"><input type="checkbox" class="Sales2All" title="Select all"></span>
				                    </a>
				                  </div>
				                  <div id="collapseTwo" class="accordion-body collapse in">
				                    	<ul id="list_sale2" class="menu unstyled"></ul>
				                  </div>
				                </div>
				                <div class="accordion-group">
				                  <div class="accordion-heading">
				                    <a id="asales4" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
				                      <h4 class="pull-left" style="margin-left:25px;">Sales 4</h4>
				                       <i id="isales4" class="icon-right-dir pull-right" style="color:#43484D; font-size:18px;"></i>
				                    </a>
				                    <span class="pull-left" style="margin-left:20px;margin-top:-36px"><input type="checkbox" class="Sales4All" title="Select all"></span>
				                    </a>
				                  </div>
				                  <div id="collapseThree" class="accordion-body collapse in">
				                    	<ul id="list_sale4" class="menu unstyled"></ul>
				                  </div>
				                </div>

				                <div class="accordion-group">
				                  <div class="accordion-heading">
				                    <a id="asalesTM" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion2" href="#collapseFour">
				                      <h4 class="pull-left" style="margin-left:25px;">Sales TM</h4>
				                       <i id="isalesTM" class="icon-right-dir pull-right" style="color:#43484D; font-size:18px;"></i>
				                    </a>
				                    <span class="pull-left" style="margin-left:20px;margin-top:-36px"><input type="checkbox" class="SalesTMAll" title="Select all"></span>
				                    </a>
				                  </div>
								  <div id="collapseFour" class="accordion-body collapse in">
				                    	<ul id="list_saleTM" class="menu unstyled">
										</ul>
				                  </div>
				                </div>
								
								<div class="accordion-group">
				                  <div class="accordion-heading">
				                    <a id="asalesCSD" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion2" href="#collapseFive">
				                      <h4 class="pull-left" style="margin-left:25px;">Sales CSD</h4>
				                       <i id="isalesCSD" class="icon-right-dir pull-right" style="color:#43484D; font-size:18px;"></i>
				                    </a>
				                    <span class="pull-left" style="margin-left:20px;margin-top:-36px"><input type="checkbox" class="SalesCSDAll" title="Select all"></span>
				                    </a>
				                  </div>
								  <div id="collapseFive" class="accordion-body collapse in">
				                    	<ul id="list_saleCSD" class="menu unstyled">
										</ul>
				                  </div>
				                </div>
								
								<div class="accordion-group">
				                  <div class="accordion-heading">
				                    <a id="asalesGEO" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion2" href="#collapseSix">
				                      <h4 class="pull-left" style="margin-left:25px;">Sales GEO</h4>
				                       <i id="isalesGEO" class="icon-right-dir pull-right" style="color:#43484D; font-size:18px;"></i>
				                    </a>
				                    <span class="pull-left" style="margin-left:20px;margin-top:-36px"><input type="checkbox" class="SalesGEOAll" title="Select all"></span>
				                    </a>
				                  </div>
								  <div id="collapseSix" class="accordion-body collapse in">
				                    	<ul id="list_saleGEO" class="menu unstyled">
										</ul>
				                  </div>
				                </div>
								<div class="accordion-group">
				                  <div class="accordion-heading">
				                    <a id="asalesCLMV" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion2" href="#collapseSeven">
				                      <h4 class="pull-left" style="margin-left:25px;">Sales CLMV</h4>
				                       <i id="isalesCLMV" class="icon-right-dir pull-right" style="color:#43484D; font-size:18px;"></i>
				                    </a>
				                    <span class="pull-left" style="margin-left:20px;margin-top:-36px"><input type="checkbox" class="SalesCLMVAll" title="Select all"></span>
				                    </a>
				                  </div>
								  <div id="collapseSeven" class="accordion-body collapse in">
				                    	<ul id="list_saleCLMV" class="menu unstyled">
										</ul>
				                  </div>
				                </div>
				            </div>
						</div>
						
					</div>
			</div>
		</div>
	</div>
</section>
		<div id="footer" class="bg-color dark-blue">
			<div class="container">
				<div class="box-padding">
					Copyright &copy; 2013 Sales Forecast
				</div>
			</div>
		</div>
		<!-- Javascript -->
		<!-- JS:jquery-->
		<!--<script src="../../../../ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>-->
  		<script src="js/library/jquery/jquery.min.js"></script>
		<script src="js/library/jquery/jquery-ui.min.js"></script>

		<!-- JS:masonry-->
		<script src="js/library/masonry/jquery.masonry.min.js"></script>

		<!-- JS:flexslider-->
		<script src="js/library/flexslider/jquery.flexslider-min.js"></script>

		<!-- JS:tables-->
		<script src="js/library/dataTables/jquery.dataTables.min.js"></script>

		<!-- JS:calendar-->
		<script src="js/library/fullcalendar/fullcalendar.min.js"></script>
		<script src="js/library/fullcalendar/gcal.js"></script>

		<!-- JS:forms-->
		<script src="js/library/forms/jquery.hotkeys.js"></script>
		<script src="js/library/forms/bootstrap-wysiwyg.js"></script>
		<script src="js/library/forms/picker.js"></script>
		<script src="js/library/forms/picker.date.js"></script>
		<script src="js/library/forms/picker.time.js"></script>
		<script src="js/library/forms/jquery.bootstrap.wizard.min.js"></script>

		<!-- JS:charts-->
		<script src="js/library/charts/jquery.flot.js"></script>
		<script src="js/library/charts/jquery.flot.resize.js"></script>
		<script src="js/library/charts/jquery.flot.stack.js"></script>
		<script src="js/library/charts/jquery.flot.pie.min.js"></script>
		<script src="js/library/charts/jquery.sparkline.min.js"></script>
		<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="excanvas.min.js"></script><![endif]-->

		<!-- JS:syntaxHighlighter-->
		<script src="js/library/syntaxHighlighter/shCore.js"></script>
		<script src="js/library/syntaxHighlighter/shBrushCss.js"></script>
		<script src="js/library/syntaxHighlighter/shBrushXml.js"></script>
		<script src="js/library/syntaxHighlighter/shBrushJScript.js"></script>

		<!-- JS:bootstrap-->
		<script src="bootstrap/js/bootstrap.min.js"></script>
		
		<!-- JS:custom js for this template -->
		<script src="js/styler/custom.js"></script>
		<script type="text/javascript">
		    $(document).ready(function(){
			var check1='';var check2='';var check3='';var check4='';var check5='';var check6='';
			var _loginID = "<?php echo $_COOKIE['Ses_ID']?>";
				$("#slc_year").disableSelection();
		    	$('.widget-sales').on('click','#loadsales',function(){
		        	$('#myModal').modal('show');
		    	});
		    	$.ajax({ 
		    		cache: false,
					type: "POST",
					dataType: "json",
		        	url: "AJAX/select_year.php",
		        	success: function (json) {
		        		$.each(json, function() {
							var item = this;
		        			$("#slc_year").append("<option value='" + item[0] + "'>" + item[0] + "</option>");
						});
						$("#slc_year").val((new Date).getFullYear());
			        }
			    });
			    $.ajax({ 
			    	cache: false,
					type: "POST",
					dataType: "json",
		        	url: "AJAX/select_yearSign.php",
		        	success: function (json) {
		        		$.each(json, function() {
							var item = this;
		        			$("#slc_year_sign").append("<option value='" + item[0] + "'>" + item[0] + "</option>");
						});
						$("#slc_year_sign").val((new Date).getFullYear());
			        }
			    });
		    	
				$('.optionsYear1').on('click',function(){
					$('.optionsYear1').closest('li').attr("class", "current");
					$('.optionsYear2').closest('li').attr("class", "");
					$("#slc_year_sign").disableSelection();
					$("#slc_year").enableSelection();
					document.frmexport.BidingYear.value=document.frmexport.slc_year.value;
					document.frmexport.where.value=document.frmexport.rdoYear1[0].value;
					//
				});
				$('.optionsYear2').on('click',function(){
					$('.optionsYear1').closest('li').attr("class", "");
					$('.optionsYear2').closest('li').attr("class", "current");
					$("#slc_year").disableSelection();
					$("#slc_year_sign").enableSelection();
					document.frmexport.BidingYear.value=document.frmexport.slc_year_sign.value;
					document.frmexport.where.value=document.frmexport.rdoYear1[1].value;
					//
				});
				//Special
				$('.chkSpecial').on('click',function(){
					if ( $(this).prop('checked')) {
						$(this).closest('li').attr("class", "current");
						document.frmexport.SpecialProject.value="0,1";
					}else{
						$(this).closest('li').attr("class", "");
						document.frmexport.SpecialProject.value="0";
					}
					//
				});
				$('.chkSpecial1').on('click',function(){
					$('.chkSpecial1').closest('li').attr("class", "current");
					$('.chkSpecial2').closest('li').attr("class", "");
					$('.chkSpecial3').closest('li').attr("class", "");
					$('.chkSpecial0').closest('li').attr("class", "");
					document.frmexport.option.value=document.frmexport.rdoOption[1].value;
					//
				});
				$('.chkSpecial2').on('click',function(){
					$('.chkSpecial1').closest('li').attr("class", "");
					$('.chkSpecial3').closest('li').attr("class", "");
					$('.chkSpecial2').closest('li').attr("class", "current");
					$('.chkSpecial0').closest('li').attr("class", "");
					document.frmexport.option.value=document.frmexport.rdoOption[2].value;
					//
				});
				$('.chkSpecial0').on('click',function(){
					$('.chkSpecial1').closest('li').attr("class", "");
					$('.chkSpecial2').closest('li').attr("class", "");
					$('.chkSpecial3').closest('li').attr("class", "");
					$('.chkSpecial0').closest('li').attr("class", "current");
					document.frmexport.option.value=document.frmexport.rdoOption[0].value;
					//
				});
				$('.chkSpecial3').on('click',function(){
					$('.chkSpecial1').closest('li').attr("class", "");
					$('.chkSpecial2').closest('li').attr("class", "");
					$('.chkSpecial3').closest('li').attr("class", "current");
					$('.chkSpecial0').closest('li').attr("class", "");
					document.frmexport.option.value=document.frmexport.rdoOption[3].value;
					//
				});
				$("#slc_year").change(function(){
					document.frmexport.BidingYear.value=document.frmexport.slc_year.value;
					document.frmexport.where.value=document.frmexport.rdoYear1[0].value;
					//
				});
				$("#slc_year_sign").change(function(){
					document.frmexport.BidingYear.value=document.frmexport.slc_year_sign.value;
					document.frmexport.where.value=document.frmexport.rdoYear1[1].value;
					//
				});
				function startFunction(){
				if(check1 != '' && check2 != '' && check3 != '' && check4 != ''&& check5 != ''&& check6 != '' ){
					////start
				}
			}
				$('#btnExport').on('click',function(){
					$('#showWarning').addClass("hide");
					if(document.frmexport.rdoOption[0].checked)
					{
   					$('#frmexport').attr('action', 'ExportReport_SPA_project.php');
					}
					else if(document.frmexport.rdoOption[1].checked)
					{						
   					$('#frmexport').attr('action', 'ExportReport_SPA_progress.php');
					}
					else if(document.frmexport.rdoOption[2].checked)
					{						
   					$('#frmexport').attr('action', 'ExportReport_SPA_potential.php');
					}
					else
					{
						$('#frmexport').attr('action', 'ExportReport_SW_monthly_option.php');
						
					}
					//if ($(".progress:checked").length == 0) {
					//	$('#showWarning').removeClass("hide");
					//}else{
						console.log("Export ",document.frmexport.BidingYear.value+' '+document.frmexport.where.value+' '+document.frmexport.option.value);
						document.frmexport.submit();
					//}
				});	
				
				//sale1
				$.ajax({ 
					type:"POST",
					dataType: "json",
					data:{
						saledept : "sales 1"
					},
			        url: "AJAX/SV_Select_SaleDept.php",
			        success: function (json) {
						var chkdep = "0";
						//console.log(json);
			        	$(json).each(function () {

							//console.log(this);
							var empno=this[0];
							var name=this[1];
							if(empno == _loginID){
								checked = "checked";
								classli = "current";
								chkdep = "1";
							}else{
								checked = "";
								classli = "";
							}
			        		$("#list_sale1").append("<li class='" + classli + "'><p style='padding:5px 20px;'><input type='checkbox' class='chksales1' style='margin-top:-2px' name='chkids' value='" + empno + "' " + checked + " /> " + name + "</p></li>");
						});
						if(chkdep == '1'){
							$('#asales1').removeClass("collapsed");
							$('#isales1').removeClass("icon-right-dir").addClass("icon-down-dir");
							$('#collapseOne').addClass("in");
							$('#collapseOne').css("height","auto");
							$('#isales1').removeClass("icon-right-dir").addClass("icon-down-dir");

							$(".Sales1All").prop("checked", true);
							$(".chksales1").prop("checked", true);
							$('.chksales1').closest('li').addClass("current");	
							getSaleIDNO();
						}
						$('.Sales1All').on('click',function(){
						if ($(".Sales1All").is(':checked')) {
							$(".chksales1").prop("checked", true);
							$('.chksales1').closest('li').addClass("current");
							$('#asales1').removeClass("collapsed");
							$('#isales1').removeClass("icon-right-dir").addClass("icon-down-dir");
							$('#collapseOne').addClass("in");
							$('#collapseOne').css("height","auto");
							getSaleID();
						} else {
							$(".chksales1").prop("checked", false);
							$('.chksales1').closest('li').removeClass("current");
							/*$('#asales1').addClass("collapsed");
							$('#isales1').removeClass("icon-down-dir").addClass("icon-right-dir");
							$('#collapseOne').removeClass("in");
							$('#collapseOne').css("height","0px");
							$('#showWarning').removeClass("hide");*/
							
						}
			    	});
					$('.chksales1').on('click',function(){
						if ( $(this).prop('checked')) {
							$(this).closest('li').attr("class", "current");
					    } else {
							$(this).closest('li').attr("class", "");
					    }
						
						if( $(".chksales1:checked").length == $(".chksales1").length){
							$(".Sales1All").prop("checked", true);
						}else{
							$(".Sales1All").prop("checked", false);
						}
						getSaleID();
					});
					$('#asales1').on('click',function(){
						if($("#collapseOne").css("height") == "0px") { 
							$('#isales1').removeClass("icon-right-dir").addClass("icon-down-dir");
					    } else {
							$('#isales1').removeClass("icon-down-dir").addClass("icon-right-dir");
					    }
					});
					check1 = '1';
					startFunction();
			        }
			    });
				//sale2
				$.ajax({ 
					type:"POST",
					dataType: "json",
					data:{
						saledept : "sales 2"
					},
			        url: "AJAX/SV_Select_SaleDept.php",
			        success: function (json) {
			        	var chkdep = "0";
			        	$(json).each(function () {

							//console.log(this);
							var empno=this[0];
							var name=this[1];
							if(empno == _loginID){
								checked = "checked";
								classli = "current";
								chkdep = "1";
							}else{
								checked = "";
								classli = "";
							}
			        		$("#list_sale2").append("<li class='" + classli + "'><p style='padding:5px 20px;'><input type='checkbox' class='chksales2' style='margin-top:-2px' name='chkids' value='" + empno + "' " + checked + " /> " + name + "</p></li>");
						});
						if(chkdep == '1'){
							$('#asales2').removeClass("collapsed");
							$('#isales2').removeClass("icon-right-dir").addClass("icon-down-dir");
							$('#collapseTwo').addClass("in");
							$('#collapseTwo').css("height","auto");
							$('#isales2').removeClass("icon-right-dir").addClass("icon-down-dir");	

							$(".Sales2All").prop("checked", true);
							$(".chksales2").prop("checked", true);
							$('.chksales2').closest('li').addClass("current");	
							getSaleIDNO();	
						}
					$('.Sales2All').on('click',function(){
						if ($(".Sales2All").is(':checked')) {
							$(".chksales2").prop("checked", true);
							$('.chksales2').closest('li').addClass("current");
							$('#asales2').removeClass("collapsed");
							$('#isales2').removeClass("icon-right-dir").addClass("icon-down-dir");
							$('#collapseTwo').addClass("in");
							$('#collapseTwo').css("height","auto");
							getSaleID();
						} else {
							$(".chksales2").prop("checked", false);
							$('.chksales2').closest('li').removeClass("current");
							/*$('#asales2').addClass("collapsed");
							$('#isales2').removeClass("icon-down-dir").addClass("icon-right-dir");
							$('#collapseTwo').removeClass("in");
							$('#collapseTwo').css("height","0px");
							$('#showWarning').removeClass("hide");*/
							
						}
			    	});
					$('.chksales2').on('click',function(){
						if ( $(this).prop('checked')) {
							$(this).closest('li').addClass("current");
					    } else {
							$(this).closest('li').removeClass("current");
					    }
						if( $(".chksales2:checked").length == $(".chksales2").length){
							$(".Sales2All").prop("checked", true);
						}else{
							$(".Sales2All").prop("checked", false);
						}
						getSaleID();
					});
					$('#asales2').on('click',function(){
						if($("#collapseTwo").css("height") == "0px") { 
							$('#isales2').removeClass("icon-right-dir").addClass("icon-down-dir");
					    } else {
							$('#isales2').removeClass("icon-down-dir").addClass("icon-right-dir");
					    }
					});
					check2='1';
					startFunction();
			        }
			    });
				//sale4
				$.ajax({ 
					type:"POST",
					dataType: "json",
					data:{
						saledept : "sales 4"
					},
			        url: "AJAX/SV_Select_SaleDept.php",
			        success: function (json) {
			        	var chkdep = "0";
			        	$(json).each(function () {

							//console.log(this);
							var empno=this[0];
							var name=this[1];
							if(empno == _loginID){
								checked = "checked";
								classli = "current";
								chkdep = "1";
							}else{
								checked = "";
								classli = "";
							}
			        		$("#list_sale4").append("<li class='" + classli + "'><p style='padding:5px 20px;'><input type='checkbox' class='chksales4' style='margin-top:-2px' name='chkids' value='" + empno + "' " + checked + " /> " + name + "</p></li>");
						});
						if(chkdep == '1'){
							$('#asales4').removeClass("collapsed");
							$('#isales4').removeClass("icon-right-dir").addClass("icon-down-dir");
							$('#collapseThree').addClass("in");
							$('#collapseThree').css("height","auto");
							$('#isales4').removeClass("icon-right-dir").addClass("icon-down-dir");	

							$(".Sales4All").prop("checked", true);
							$(".chksales4").prop("checked", true);
							$('.chksales4').closest('li').addClass("current");	
							getSaleIDNO();
						}
					$('.Sales4All').on('click',function(){
						if ($(".Sales4All").is(':checked')) {
							$(".chksales4").prop("checked", true);
							$('.chksales4').closest('li').addClass("current");
							$('#asales4').removeClass("collapsed");
							$('#isales4').removeClass("icon-right-dir").addClass("icon-down-dir");
							$('#collapseThree').addClass("in");
							$('#collapseThree').css("height","auto");
							getSaleID();
						} else {
							$(".chksales4").prop("checked", false);
							$('.chksales4').closest('li').removeClass("current");
							/*$('#asales4').addClass("collapsed");
							$('#isales4').removeClass("icon-down-dir").addClass("icon-right-dir");
							$('#collapseThree').removeClass("in");
							$('#collapseThree').css("height","0px");
							$('#showWarning').removeClass("hide");*/
							
							
						}
			    	});
					$('.chksales4').on('click',function(){
						if ( $(this).prop('checked')) {
							$(this).closest('li').addClass("current");
					    } else {
							$(this).closest('li').removeClass("current");
					    }
						if( $(".chksales4:checked").length == $(".chksales4").length){
							$(".Sales4All").prop("checked", true);
						}else{
							$(".Sales4All").prop("checked", false);
						}
						getSaleID();
					});
					$('#asales4').on('click',function(){
						if($("#collapseThree").css("height") == "0px") { 
							$('#isales4').removeClass("icon-right-dir").addClass("icon-down-dir");
					    } else {
							$('#isales4').removeClass("icon-down-dir").addClass("icon-right-dir");
					    }
					});
					check3='1';
					startFunction();
			        }
			    });
				//saleTM
				$.ajax({ 
					type:"POST",
					dataType: "json",
					data:{
						saledept : "sales tm"
					},
			        url: "AJAX/SV_Select_SaleDept.php",
			        success: function (json) {
			        	var chkdep = "0";
			        	$(json).each(function () {

							//console.log(this);
							var empno=this[0];
							var name=this[1];
							if(empno == _loginID){
								checked = "checked";
								classli = "current";
								chkdep = "1";
							}else{
								checked = "";
								classli = "";
							}
			        		$("#list_saleTM").append("<li class='" + classli + "'><p style='padding:5px 20px;'><input type='checkbox' class='chksalesTM' style='margin-top:-2px' name='chkids' value='" + empno + "' " + checked + " /> " + name + "</p></li>");
						});
						if(chkdep == '1'){
							$('#asalesTM').removeClass("collapsed");
							$('#isalesTM').removeClass("icon-right-dir").addClass("icon-down-dir");
							$('#collapseFour').addClass("in");
							$('#collapseFour').css("height","auto");
							$('#isalesTM').removeClass("icon-right-dir").addClass("icon-down-dir");	

							$(".SalesTMAll").prop("checked", true);
							$(".chksalesTM").prop("checked", true);
							$('.chksalesTM').closest('li').addClass("current");	
							getSaleIDNO();
						}
					$('.SalesTMAll').on('click',function(){
						if ($(".SalesTMAll").is(':checked')) {
							$(".chksalesTM").prop("checked", true);
							$('.chksalesTM').closest('li').addClass("current");
							$('#asalesTM').removeClass("collapsed");
							$('#isalesTM').removeClass("icon-right-dir").addClass("icon-down-dir");
							$('#collapseFour').addClass("in");
							$('#collapseFour').css("height","auto");
							getSaleID();
						} else {
							$(".chksalesTM").prop("checked", false);
							$('.chksalesTM').closest('li').removeClass("current");
							/*$('#asalesTM').addClass("collapsed");
							$('#isalesTM').removeClass("icon-down-dir").addClass("icon-right-dir");
							$('#collapseFour').removeClass("in");
							$('#collapseFour').css("height","0px");
							$('#showWarning').removeClass("hide");*/
							
						}
					});
					$('.chksalesTM').on('click',function(){
						if ( $(this).prop('checked')) {
							$(this).closest('li').addClass("current");
						} else {
							$(this).closest('li').removeClass("current");
						}
						if( $(".chksalesTM:checked").length == $(".chksalesTM").length){
							$(".SalesTMAll").prop("checked", true);
						}else{
							$(".SalesTMAll").prop("checked", false);
						}
						getSaleID();
					});
					$('#asalesTM').on('click',function(){
						if($("#collapseFour").css("height") == "0px") { 
							$('#isalesTM').removeClass("icon-right-dir").addClass("icon-down-dir");
						} else {
							$('#isalesTM').removeClass("icon-down-dir").addClass("icon-right-dir");
						}
					});
					check4 ='1';
					startFunction();
			    	}
			    });
				
				//saleCSD
				$.ajax({ 
					type:"POST",
					dataType: "json",
					data:{
						saledept : "sales CSD"
					},
			        url: "AJAX/SV_Select_SaleDept.php",
			        success: function (json) {
			        	var chkdep = "0";
			        	$(json).each(function () {

							//console.log(this);
							var empno=this[0];
							var name=this[1];
							if(empno == _loginID){
								checked = "checked";
								classli = "current";
								chkdep = "1";
							}else{
								checked = "";
								classli = "";
							}
			        		$("#list_saleCSD").append("<li class='" + classli + "'><p style='padding:5px 20px;'><input type='checkbox' class='chksalesCSD' style='margin-top:-2px' name='chkids' value='" + empno + "' " + checked + " /> " + name + "</p></li>");
						});
						if(chkdep == '1'){
							$('#asalesCSD').removeClass("collapsed");
							$('#isalesCSD').removeClass("icon-right-dir").addClass("icon-down-dir");
							$('#collapseFive').addClass("in");
							$('#collapseFive').css("height","auto");
							$('#isalesCSD').removeClass("icon-right-dir").addClass("icon-down-dir");	

							$(".SalesCSDAll").prop("checked", true);
							$(".chksalesCSD").prop("checked", true);
							$('.chksalesCSD').closest('li').addClass("current");	
							getSaleIDNO();
						}
					$('.SalesCSDAll').on('click',function(){
						if ($(".SalesCSDAll").is(':checked')) {
							$(".chksalesCSD").prop("checked", true);
							$('.chksalesCSD').closest('li').addClass("current");
							$('#asalesCSD').removeClass("collapsed");
							$('#isalesCSD').removeClass("icon-right-dir").addClass("icon-down-dir");
							$('#collapseFive').addClass("in");
							$('#collapseFive').css("height","auto");
							getSaleID();
						} else {
							$(".chksalesCSD").prop("checked", false);
							$('.chksalesCSD').closest('li').removeClass("current");
							/*$('#asalesCSD').addClass("collapsed");
							$('#isalesCSD').removeClass("icon-down-dir").addClass("icon-right-dir");
							$('#collapseFive').removeClass("in");
							$('#collapseFive').css("height","0px");
							$('#showWarning').removeClass("hide");*/
							
						}
					});
					$('.chksalesCSD').on('click',function(){
						if ( $(this).prop('checked')) {
							$(this).closest('li').addClass("current");
						} else {
							$(this).closest('li').removeClass("current");
						}
						if( $(".chksalesCSD:checked").length == $(".chksalesCSD").length){
							$(".SalesCSDAll").prop("checked", true);
						}else{
							$(".SalesCSDAll").prop("checked", false);
						}
						getSaleID();
					});
					$('#asalesCSD').on('click',function(){
						if($("#collapseFive").css("height") == "0px") { 
							$('#isalesCSD').removeClass("icon-right-dir").addClass("icon-down-dir");
						} else {
							$('#isalesCSD').removeClass("icon-down-dir").addClass("icon-right-dir");
						}
					});
					check5 ='1';
					startFunction();
			    	}
			    });
				//sale CLMV
				$.ajax({ 
					type:"POST",
					dataType: "json",
					data:{
						saledept : "sales CLMV"
					},
			        url: "AJAX/SV_Select_SaleDept.php",
			        success: function (json) {
			        	var chkdep = "0";
			        	$(json).each(function () {

							//console.log(this);
							var empno=this[0];
							var name=this[1];
							if(empno == _loginID){
								checked = "checked";
								classli = "current";
								chkdep = "1";
							}else{
								checked = "";
								classli = "";
							}
			        		$("#list_saleCLMV").append("<li class='" + classli + "'><p style='padding:5px 20px;'><input type='checkbox' class='chksalesCLMV' style='margin-top:-2px' name='chkids' value='" + empno + "' " + checked + " /> " + name + "</p></li>");
						});
						if(chkdep == '1'){
							$('#asalesCLMV').removeClass("collapsed");
							$('#isalesCLMV').removeClass("icon-right-dir").addClass("icon-down-dir");
							$('#collapseSeven').addClass("in");
							$('#collapseSeven').css("height","auto");
							$('#isalesCLMV').removeClass("icon-right-dir").addClass("icon-down-dir");	

							$(".SalesCLMVAll").prop("checked", true);
							$(".chksalesCLMV").prop("checked", true);
							$('.chksalesCLMV').closest('li').addClass("current");	
							getSaleIDNO();
						}
					$('.SalesCLMVAll').on('click',function(){
						if ($(".SalesCLMVAll").is(':checked')) {
							$(".chksalesCLMV").prop("checked", true);
							$('.chksalesCLMV').closest('li').addClass("current");
							$('#asalesCLMV').removeClass("collapsed");
							$('#isalesCLMV').removeClass("icon-right-dir").addClass("icon-down-dir");
							$('#collapseSeven').addClass("in");
							$('#collapseSeven').css("height","auto");
							getSaleID();
						} else {
							$(".chksalesCLMV").prop("checked", false);
							$('.chksalesCLMV').closest('li').removeClass("current");
							/*$('#asalesTM').addClass("collapsed");
							$('#isalesTM').removeClass("icon-down-dir").addClass("icon-right-dir");
							$('#collapseFour').removeClass("in");
							$('#collapseFour').css("height","0px");
							$('#showWarning').removeClass("hide");*/
							
						}
					});
					$('.chksalesCLMV').on('click',function(){
						if ( $(this).prop('checked')) {
							$(this).closest('li').addClass("current");
						} else {
							$(this).closest('li').removeClass("current");
						}
						if( $(".chksalesCLMV:checked").length == $(".chksalesCLMV").length){
							$(".SalesCLMVAll").prop("checked", true);
						}else{
							$(".SalesCLMVAll").prop("checked", false);
						}
						getSaleID();
					});
					$('#asalesCLMV').on('click',function(){
						if($("#collapseSeven").css("height") == "0px") { 
							$('#isalesCLMV').removeClass("icon-right-dir").addClass("icon-down-dir");
						} else {
							$('#isalesCLMV').removeClass("icon-down-dir").addClass("icon-right-dir");
						}
					});
					check7 ='1';
					startFunction();
			    	}
			    });
				
				//saleGEO
				$.ajax({ 
					type:"POST",
					dataType: "json",
					data:{
						saledept : "sales geo"
					},
			        url: "AJAX/SV_Select_SaleDept.php",
			        success: function (json) {
			        	var chkdep = "0";
			        	$(json).each(function () {

							//console.log(this);
							var empno=this[0];
							var name=this[1];
							if(empno == _loginID){
								checked = "checked";
								classli = "current";
								chkdep = "1";
							}else{
								checked = "";
								classli = "";
							}
			        		$("#list_saleGEO").append("<li class='" + classli + "'><p style='padding:5px 20px;'><input type='checkbox' class='chksalesGEO' style='margin-top:-2px' name='chkids' value='" + empno + "' " + checked + " /> " + name + "</p></li>");
						});
						if(chkdep == '1'){
							$('#asalesGEO').removeClass("collapsed");
							$('#isalesGEO').removeClass("icon-right-dir").addClass("icon-down-dir");
							$('#collapseSix').addClass("in");
							$('#collapseSix').css("height","auto");
							$('#isalesGEO').removeClass("icon-right-dir").addClass("icon-down-dir");	

							$(".SalesGEOAll").prop("checked", true);
							$(".chksalesGEO").prop("checked", true);
							$('.chksalesGEO').closest('li').addClass("current");	
							getSaleIDNO();
						}
					$('.SalesGEOAll').on('click',function(){
						if ($(".SalesGEOAll").is(':checked')) {
							$(".chksalesGEO").prop("checked", true);
							$('.chksalesGEO').closest('li').addClass("current");
							$('#asalesGEO').removeClass("collapsed");
							$('#isalesGEO').removeClass("icon-right-dir").addClass("icon-down-dir");
							$('#collapseSix').addClass("in");
							$('#collapseSix').css("height","auto");
							getSaleID();
						} else {
							$(".chksalesGEO").prop("checked", false);
							$('.chksalesGEO').closest('li').removeClass("current");
							/*$('#asalesTM').addClass("collapsed");
							$('#isalesTM').removeClass("icon-down-dir").addClass("icon-right-dir");
							$('#collapseFour').removeClass("in");
							$('#collapseFour').css("height","0px");
							$('#showWarning').removeClass("hide");*/
							
						}
					});
					$('.chksalesGEO').on('click',function(){
						if ( $(this).prop('checked')) {
							$(this).closest('li').addClass("current");
						} else {
							$(this).closest('li').removeClass("current");
						}
						if( $(".chksalesGEO:checked").length == $(".chksalesGEO").length){
							$(".SalesGEOAll").prop("checked", true);
						}else{
							$(".SalesGEOAll").prop("checked", false);
						}
						getSaleID();
					});
					$('#asalesGEO').on('click',function(){
						if($("#collapseSix").css("height") == "0px") { 
							$('#isalesGEO').removeClass("icon-right-dir").addClass("icon-down-dir");
						} else {
							$('#isalesGEO').removeClass("icon-down-dir").addClass("icon-right-dir");
						}
					});
					check6 ='1';
					startFunction();
			    }
			    });
				function getSaleID(){
						var chkid='';
						$("input[name='chkids']").each(function () {
								if ( $(this).prop('checked')) {
									if(chkid=='')
									{
										chkid= $(this).val();
									}
									else
									{
										chkid = chkid+","+$(this).val();
									}
								}
						});
						document.frmexport.saleid.value=chkid;
					
				}
				function getSaleIDNO(){
						var chkid='';
						$("input[name='chkids']").each(function () {
								if ( $(this).prop('checked')) {
									if(chkid=='')
									{
										chkid= $(this).val();
									}
									else
									{
										chkid = chkid+","+$(this).val();
									}
								}
						});
						document.frmexport.saleid.value=chkid;
						//
				}
		   });
		</script>
</form>
  	</body>

<!-- Mirrored from bootstrapstyler.com/preview/_/flatdashboard/ by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 18 Oct 2013 07:00:04 GMT -->
</html>