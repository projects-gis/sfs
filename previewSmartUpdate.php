<?php 
	$checkmenu = "0";
	function replaceSQuoteAndNl($textinp) {
		$textinp = preg_replace("/\r|\n/", "", $textinp); 
		return str_replace("'", "\'", $textinp);
	}
	if ($id) {
		//$conn = sqlsrv_connect("157.179.28.116", "sa", "dioro@4321");
		//sqlsrv_select_db("SaleForecast", $conn);
		include("INC/connectSFC.php");
		$conn=$ConnectSaleForecast;
		$sqlStr = "SELECT IDForecast FROM Forecast WHERE ID='$id'";
		$query = sqlsrv_query($ConnectSaleForecast,iconv("UTF-8", "TIS-620", $sqlStr));
		if ($obj = sqlsrv_fetch_object($query)) {
			$id = $obj->IDForecast;
		} else {
?>
	<script type="text/javascript">
		alert("ไม่มีข้อมูลของรายการนี้ในระบบ");
<?php 
			if ($page) {
?>
		window.location = "<?php echo $page?>.php";
<?php 
			} else {
?>
		window.location = "index.php";
<?php 
			}
?>
	</script>
<?php 
		}
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="Izuddin Helmi">
	<title>SFS:::View</title>
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css">
	<link href="js/library/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
	<link href="css/styler/style.css" rel="stylesheet" type="text/css">
	<link href="css/main.css" rel="stylesheet" type="text/css">
	<link href="js/library/syntaxHighlighter/shCore.css" rel="stylesheet" type="text/css"/>
	<link href="js/library/syntaxHighlighter/shThemeDefault.css" rel="stylesheet" type="text/css"/>
	<link href="css/styler/fontello.css" rel="stylesheet" type="text/css">
    <link href="css/styler/animation.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="js/library/jquery/jquery-ui-1.10.3.custom.min.css" />

	<!--timeline-->
	<link rel="stylesheet" href="libs/timeline/css/style.timeline.css">
	<link rel="stylesheet" href="libs/timeline/css/timelinexml.basic.css">
	<script src="libs/timeline/libs/modernizr-2.0.6.min.js"></script>
</head>
<body cz-shortcut-listen="true" style="margin-top:0px;">	
<section class="page-header bg-color white">
	<div class="container">
		<div class="row">
			<div class="span12">
				<div class="section-header clearfix">
					<div class="pull-left">
						<h2 id="forecastid"><i class="icon-chart-2"></i><?php echo $id?></h2>
					</div>
					<div id="shortcut" class="nav-collapse pull-right">
						<ul class="shortcuts unstyled">
							
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="dashboard content">
	<div class="container">
<!-- ============================================= Cont & Cus =========================================== -->
		<div class="row">				
			<div class="span6">					
				<div class="widget widget-initial">
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-suitcase"></i> Contract</span>
					</div>		
					<div class="bg-color turqoise rounded">
						<div class="box-padding" ><h4><input id="inp_ForecastID" type="hidden">
							<table>
							<tr>
								<td class="text-right">PEContract&nbsp;</td>
								<td><input id="inp_contractpe" class="span3"  type="text" style="margin-bottom: 0px;" placeholder="Click Get PEContract" readonly><!--<?php if (!$id) {?><i id="btn_pEContract_modal" class="i_btn icon-search-3" data-toggle="tooltip" data-placement="bottom" data-original-title="Search PEContract"></i><i id="btn_pEContract_del" class="i_btn icon-cancel-alt" data-toggle="tooltip" data-placement="bottom" data-original-title="Delete PEContract"></i><?php  }?>--></td>
							</tr>
							<tr>
								<td class="text-right">E-Contract&nbsp;</td>
								<td><input id="inp_contractec" class="span3"  type="text" placeholder="Get E-Contract" readonly><i id="btn_eContract" class="i_btn icon-exchange" data-toggle="tooltip" data-placement="bottom" data-original-title="Get E-Contract" style="display:none"></i></td>
							</tr>							
							</table>						
						</h4></div>
					</div>

							
				</div>
			</div>
			<div class="span6">
				<div class="widget widget-customer">	
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-article-alt-1"></i> Project Description</span>
					</div>					
					<div class="bg-color turqoise rounded-top">
						<div class="box-padding" style="padding:15px;"><h4>
							<table>
								<tr>
									<td class="text-right">ปีงบประมาณ&nbsp;</td>
									<td ><input id="projDesc_Year" class="span1" type="text" placeholder="2557" style="background:#FDD752;"></td>
								</tr>
								<tr>
									<td class="text-right">Project Name&nbsp;</td>
									<td><textarea id="projDesc_Name" rows="3" class="span3" style="background:#FDD752;"></textarea><i style="vertical-align: bottom;margin-bottom:5px;" id="btn_help_modal" class="i_btn icon-newspaper" data-toggle="tooltip" data-placement="bottom" data-original-title="เอกสารหลักการตั้งชื่อโครงการ" style="display:none"></i></td>
								</tr>
							</table>							
						</h4></div>
					</div>					
					<div class="bg-color dark-grey rounded-bottom" style="padding:15px;">
						<span id="projDesc_show" title="[ปีงบประมาณ] Payer code/enduser code:project name"></span>
					</div>
				</div>				
			</div>
		</div>

		<div class="row">				
			<div class="span6">					
				<div class="widget widget-initial">
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-users-3"></i> Payer <a class="btn btn-pink" id="payer_copy" style="margin-top: -15px;font-weight:bold;display:none">Copy Enduser</a></span>
					</div>		<input id="payer_custno" type="hidden">
					<div class="bg-color turqoise rounded">
						<div class="box-padding" style="padding:15px;"><h4>
							<table>
								<tr>
									<td class="text-right">Customer Name&nbsp;</td>
									<td><input id="payer_name" class="span3" type="text" readonly><i id="btn_payercust_modal" class="i_btn icon-search-3" data-toggle="tooltip" data-placement="bottom" data-original-title="Search Customer" style="display:none"></i></td>
									<!--<?php  //if (!$id) { ?><i id="btn_customer_modal" class="i_btn icon-search-3" data-toggle="tooltip" data-placement="bottom" data-original-title="Search Customer"></i><?php  //} ?>-->
								</tr>	
								<tr>
									<td class="text-right">dep/div Code&nbsp;</td>
									<td><input id="payer_code" class="span3" type="text" placeholder="dep-div-sec-user" style="background:#FDD752;" readonly><i id="btn_payercode_modal" class="i_btn icon-search-3" data-toggle="tooltip" data-placement="bottom" data-original-title="Search Oraganization" style="display:none"></i></td>
								</tr>
								<tr>
									<td class="text-right">dep/div Name&nbsp;</td>
									<td><textarea id="payer_org" rows="3" class="span3" readonly></textarea></td>
								</tr>
							</table>							
						</h4></div>
					</div>
				</div>
			</div>
			<div class="span6">					
				<div class="widget widget-customer">	
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-users-3"></i> End User <a class="btn btn-pink" style="margin-top: -15px;font-weight:bold;display:none" id="enduser_copy">Copy Payer</a></span>
						<input id="enduser_custno" type="hidden">
					</div>					
					<div class="bg-color turqoise rounded">
						<div class="box-padding" style="padding:15px;"><h4>
							<table>
								<tr>
									<td class="text-right">Customer Name&nbsp;</td>
									<td><input id="enduser_name" class="span3" type="text" readonly><i id="btn_endusercust_modal" class="i_btn icon-search-3" data-toggle="tooltip" data-placement="bottom" data-original-title="Search Customer" style="display:none"></i></td>
									<!--<?php  //if (!$id) { ?><i id="btn_customer_modal" class="i_btn icon-search-3" data-toggle="tooltip" data-placement="bottom" data-original-title="Search Customer"></i><?php  //} ?>-->
								</tr>	
								<tr>
									<td class="text-right">dep/div Code&nbsp;</td>
									<td><input id="enduser_code" class="span3" type="text" placeholder="dep-div-sec-user" style="background:#FDD752;" readonly><i id="btn_endusercode_modal" class="i_btn icon-search-3" data-toggle="tooltip" data-placement="bottom" data-original-title="Search Oraganization" style="display:none"></i></td>
								</tr>
								<tr>
									<td class="text-right">dep/div Name&nbsp;</td>
									<td><textarea id="enduser_org" rows="3" class="span3" readonly></textarea></td>
								</tr>
							</table>							
						</h4></div>
					</div>
				</div>				
			</div>
		</div>
<!-- ==================================================================================================== -->
<!-- ============================================= Targ & Time ========================================== -->
		<div class="row">				
			<div class="span6">					
				<div class="widget widget-target">	
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-target-2"></i> Target</span>
						<div class="dropdown pull-right"><h4 style="margin: 0px;">
							<label class="checkbox inline" style="color: #0072C6; font-weight: bold; padding-top: 0px;">
								<input id="chk_esri" type="checkbox" value="1" disabled>
								ESRI's TOR
							</label>
							<label class="checkbox inline" style="color: #0072C6; font-weight: bold; padding-top: 0px;">
								<input id="chk_edu" type="checkbox" value="1" disabled>
								Education
							</label>
							<label class="checkbox inline" style="color: #0072C6; font-weight: bold; padding-top: 0px;">
								<input id="chk_proj" type="checkbox" value="1" disabled>
								Project
							</label>
							<label class="checkbox inline" style="color: #AC193D; font-weight: bold; padding-top: 0px;">
								<input id="chk_sproj" type="checkbox" value="1" disabled>
								Special Project
							</label>
						</h4></div>
					</div>					
					<div class="bg-color turqoise rounded">
						<div class="box-padding"><h5>
							<table>
								<tr>
									<td class="text-right" style="width: 100px;"><h4>Potential&nbsp;</h4></td>
									<td class="text-right" style="width: 60px;"><input id="inp_potentialp" class="span1" type="text" value="10%" readonly></td>
									<td class="text-right"><input id="inp_potentialt" class="span3" type="text" value="โอกาสน้อยมาก" readonly><i id="btn_potential_modal" class="i_btn icon-th-list" title="Load Potential" style="display: none;"></i><i id="btn_hPotential_modal" class="i_btn icon-clock-8" title="History"></i></td>
									
								</tr>
								<tr>
									<td class="text-right"><h4>Progress&nbsp;</h4></td>
									<td class="text-right"><input id="inp_progressp" class="span1" type="text" value="10%" readonly></td>
									<td class="text-right"><input id="inp_progresst" class="span3" type="text" value="พิจารณาจัดตั้งโครงการใหม่อยู่" readonly><i id="btn_progress_modal" class="i_btn icon-th-list" title="Load Progress" style="display: none;"></i><i id="btn_hProgress_modal" class="i_btn icon-clock-8" title="History"></i></td>
									
								</tr>
							</table>							
							<table>
								<tbody>
									<tr>
										<td colspan="2" class="text-right" style="width: 270px;"><h4>Excluded VAT</h4></td>
										<td style="width: 50px;"></td>
										<td class="text-right"><h4>Included VAT</h4></td>
										<td style="width: 50px;"></td>
									</tr>
									<tr>
										<td class="text-right">Income&nbsp;</td>
										<td><input id="inp_inco_evat" class="span2 number text-right" type="text" placeholder="0.00" readonly></td>
										<td>Baht</td>
										<td><input id="inp_inco_ivat" class="span2 number text-right" type="text" placeholder="0.00" readonly></td>
										<td>Baht</td>
									</tr>
									<tr>
										<td class="text-right">Budget&nbsp;</td>
										<td><input id="inp_budg_evat" class="span2 number text-right" type="text" placeholder="0.00" readonly></td>
										<td>Baht</td>
										<td><input id="inp_budg_ivat" class="span2 number text-right" type="text" placeholder="0.00" readonly></td>
										<td>Baht</td>
									</tr>
									<tr>
										<td class="text-right">Cost&nbsp;</td>
										<td><input id="inp_cost_evat" class="span2 number text-right" type="text" placeholder="0.00" readonly></td>
										<td>Baht</td>
										<td><input id="inp_cost_ivat" class="span2 number text-right" type="text" placeholder="0.00" readonly></td>
										<td>Baht</td>
									</tr>
								</tbody>
							</table>
						</h5></div>
					</div>
				</div>
			</div>
			<div class="span6">					
				<div class="widget widget-timeframe">	
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-calendar-6"></i> Time Frame</span>
						<div class="dropdown pull-right"></div>
					</div>					
					<div class="bg-color turqoise rounded">
						<div class="box-padding"><h5>
							<table>
							<tr>
								<td class="text-right">Bidding Date&nbsp;</td>
								<td><input id="inp_biddDate" class="form_date" style="width: 80px;" type="text" placeholder="choose date" readonly></td>
								<td class="text-right">Contract Sign Date&nbsp;</td>
								<td><input id="inp_signDate" class="form_date" style="width: 80px;" type="text" placeholder="choose date" readonly></td>
							</tr>	
							<tr>
								<td class="text-right">Project Duration&nbsp;</td>
								<td><input id="inp_pDuration" class="span1 nNumber text-right" type="text" placeholder="0" readonly> Days</td>
								<td class="text-right">End of Contract&nbsp;</td>
								<td><input id="inp_endCont" class="span1" style="width: 80px;" type="text" readonly></td>
							</tr>
							<tr>
								<td class="text-right"><input id="chk_downPayment" type="checkbox" value="" style="margin-top: 0px;" checked disabled> Down Payment&nbsp;</td>
								<td><input id="inp_downPayment" class="span1 number text-right" type="text" placeholder="0" readonly>&nbsp;&nbsp;&nbsp;&nbsp;% =</td>
								<td colspan="2"><input id="inp_dPBaht" class="span2 number text-right" type="text" style="width: 150px;" placeholder="0.00" readonly> Baht</td>
							</tr>
							<tr>
								<td class="text-right">Inv.Date&nbsp;</td>
								<td><input id="inp_dPDays" class="span1 nNumber text-right" type="text" placeholder="0" readonly> Days =&nbsp;</td>
								<td colspan="2"><input id="inp_dPDate" class="form_date" style="width: 80px;" type="text" placeholder="choose date" readonly></td>
							</tr>
							</table>							
							 </h5>
						</div>						
					</div>
					<div class="widget-header clearfix" style="margin-top:5px;">
						<span class="pull-left"><i class="icon-clock-5"></i>Timeline Progress</span>
						<section class="demo">
							<div class="demo-box">
							</div>
						</section>
					</div>
				</div>				
			</div>
		</div>
<!-- ==================================================================================================== -->
<!-- ============================================= Invo & Book ========================================== -->
		<div class="row">				
			<div class="span8">					
				<div class="widget widget-invoicing">	
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-list-alt"></i> Invoicing Plan</span>
					</div>					
					<div class="bg-color white rounded">
						<div class="box-padding" style="padding:5px">
			    			<table class="table table-striped">
								<thead>
									<tr style="background-color: #2ABF9E; color: #FFFFFF;">
										<th style="width: 50px;">Phase</th>
										<th style="width: 50px;">Days</th>
										<th style="width: 70px;">Date</th>
										<th style="width: 50px;">%</th>
										<th style="width: 120px;">Amount</th>
										<th>Remark</th>
									</tr>
								</thead>
								<tbody id="tbody_invoicing"></tbody>
							</table>
				        </div>
					</div>
				</div>
			</div>
			<div class="span4">					
				<div class="widget widget-book">	
					<div class="widget-header clearfix">
						<span class="pull-left" style="padding-bottom: 3px;"><i class="icon-dollar"></i> Book/Forward</span>
					</div>					
					<div class="bg-color white rounded">
						<div class="box-padding" style="padding:5px">
			    			<table class="table table-striped">
								<thead>
									<tr style="background-color: #2ABF9E; color: #FFFFFF;">
										<th style="width: 80px;">Type</th>
										<th style="width: 80px;">Year</th>
										<th>Amount</th>
									</tr>
								</thead>
								<tbody id="tbody_bank"></tbody>
							</table>
				        </div>
					</div>
				</div>
			</div>
		</div>
<!-- ==================================================================================================== -->
<!-- ============================================= Sales Rep. =========================================== -->
		<div class="row">
			<div class="span12">					
				<div class="widget widget-representative">
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-user-5"></i> sales representative</span>
						<div class="dropdown pull-right"></div>
					</div>		
					<div class="bg-color dark-blue rounded">
						<div class="box-padding">
							<h4>
							<table>
								<tr>
									<td class="text-right">Sales Representative&nbsp;</td>
									<td><input id="inp_salesname" class="span3" type="text" readonly></td>
								</tr>	
								<tr>
									<td class="text-right">Project By&nbsp;</td>
									<td>
										<select id="slc_salesproj" class="span2" style="background-color: #F2DEDE !important;" disabled>
											<option value="Sales 1">Sales 1</option>
											<option value="Sales 2">Sales 2</option>
											<option value="Sales 3">Sales 3</option>
											<option value="Sales 4">Sales 4</option>
											<option value="Sales TM">Sales TM</option>
											<option value="Sales CSD">Sales CSD</option>
											<option value="Sales GEO">Sales GEO</option>
											<option value="Sales CLMV">Sales CLMV</option>
										</select>
									</td>
								</tr>
								<tr>
									<td class="text-right">Remark&nbsp;</td>
									<td><textarea id="txa_salesremark" rows="3" style="width: 255px;" readonly></textarea></td>
								</tr>
							</table>							
							</h4>
						</div>
					</div>
				</div>
			</div>	
		</div>
<!-- ==================================================================================================== -->
<!-- ============================================= Status & Dept ======================================== -->
		<div class="row">					
			<div class="span8">					
				<div class="widget widget-status">	
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-tasks-1"></i> Status</span>
					</div>					
					<div class="bg-color white rounded">
						<div class="box-padding" style="padding:5px">
			    			<table class="table table-striped">
								<thead>
									<tr style="background-color: #2ABF9E; color: #FFFFFF;">
										<th style="width: 100px;">Date</th>
										<th>Description</th>
									</tr>
								</thead>
								<tbody id="tbody_status"></tbody>
							</table>
				        </div>
					</div>
				</div>
			</div>
			<div class="span4">					
				<div class="widget widget-Dept">	
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-sitemap"></i> Department Support</span>
					</div>					
					<div class="bg-color white rounded">
						<div class="box-padding" style="padding:5px">
			    			<table class="table table-striped">
								<thead>
									<tr style="background-color: #2ABF9E; color: #FFFFFF;">
										<th>Description</th>
									</tr>
								</thead>
								<tbody id="tbody_deptsup"></tbody>
							</table>
				        </div>
					</div>
				</div>
			</div>
		</div>
<!-- ==================================================================================================== -->
<!-- ============================================= Desc & % ============================================= -->
		<div class="row">				
			<div class="span12">					
				<div class="widget widget-description">	
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-article-alt-1"></i> Description</span>
					</div>					
					<div class="bg-color white rounded-top">
						<div class="box-padding" style="padding:5px">
			    			<table class="table table-striped">
								<thead>
									<tr style="background-color: #2ABF9E; color: #FFFFFF;">
										<th style="width: 100px;">Type</th>
										<th>Description</th>
										<th style="width: 150px;">Price</th>
									</tr>
								</thead>
								<tbody id="tbody_description"></tbody>
							</table>
				        </div>
					</div>
				</div>
			</div>
		</div>
<!-- ==================================================================================================== -->
<!-- ============================================= Product ============================================== -->
		<div class="row">
			<div class="span6">					
				<div class="widget widget-esriproduct">	
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-star-5"></i>ESRI Product</span>
					</div>					
					<div class="bg-color white rounded">
						<div class="box-padding" style="padding:5px">
			    			<table class="table table-striped">
								<thead>
									<tr style="background-color: #2ABF9E; color: #FFFFFF;">
										<th>Description</th>
										<th style="width: 50px;">Qty</th>
									</tr>
								</thead>
								<tbody id="tbody_esri"></tbody>
							</table>
				        </div>
					</div>
				</div>
			</div>
			<div class="span6">					
				<div class="widget widget-SEproduct">	
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-star-5"></i>Schneider Electric Product</span>
					</div>					
					<div class="bg-color white rounded">
						<div class="box-padding" style="padding:5px">
			    			<table class="table table-striped">
								<thead>
									<tr style="background-color: #2ABF9E; color: #FFFFFF;">
										<th>Description</th>
										<th style="width: 50px;">Qty</th>
									</tr>
								</thead>
								<tbody id="tbody_SE"></tbody>
							</table>
				        </div>
					</div>
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="span6">					
				<div class="widget widget-garmin">	
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-star-5"></i>Garmin Product</span>
					</div>					
					<div class="bg-color white rounded">
						<div class="box-padding" style="padding:5px">
			    			<table class="table table-striped">
								<thead>
									<tr style="background-color: #2ABF9E; color: #FFFFFF;">
										<th>Description</th>
										<th style="width: 50px;">Qty</th>
									</tr>
								</thead>
								<tbody id="tbody_garmin"></tbody>
							</table>
				        </div>
					</div>
				</div>
			</div>
			<div class="span6">					
				<div class="widget widget-nostra">	
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-star-5"></i>Nostra Product </span>
					</div>					
					<div class="bg-color white rounded">
						<div class="box-padding" style="padding:5px">
			    			<table class="table table-striped">
								<thead>
									<tr style="background-color: #2ABF9E; color: #FFFFFF;">
										<th>Description</th>
										<th style="width: 50px;">Qty</th>
									</tr>
								</thead>
								<tbody id="tbody_nostra"></tbody>
							</table>
				        </div>
					</div>
				</div>
			</div>				
		</div>
		<div class="row">			
			<div class="span6">					
				<div class="widget widget-leicaproduct">	
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-star-5"></i>Geomatic's Product</span>
					</div>					
					<div class="bg-color white rounded">
						<div class="box-padding" style="padding:5px">
			    			<table class="table table-striped">
								<thead>
									<tr style="background-color: #2ABF9E; color: #FFFFFF;">
										<th>Description</th>
										<th style="width: 50px;">Qty</th>
									</tr>
								</thead>
								<tbody id="tbody_leica"></tbody>
							</table>
				        </div>
					</div>
				</div>
			</div>		
			<div class="span6">	
			</div>
		</div>
<!-- ==================================================================================================== -->
		<div class="row">
			<div class="span12">
				<div class="widget widget-save">
					<div class="widget-header clearfix">
						<div class="dropdown pull-right">
							<a id="btn_cancel" class="btn btn-blue" style="color: #FFFFFF">CLOSE</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div id="footer" class="bg-color dark-blue">
	<div class="container">
		<div class="box-padding">
			Copyright &copy; 2013 Sales Forecast
		</div>
	</div>
</div>
<div class="modal hide fade" id="potential_modal">
	<div class="modal-header bg-color light-green aligncenter">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    <h1>Potential</h1>
	</div>
	<div class="modal-body">
		<table class="table table-striped table-condensed table_select">
			<thead>
				<tr style=" background-color: #2ABF9E; color: #FFFFFF;">
					<th style="width: 20%;">%</th>
					<th style="width: 80%;">Description</th>
				</tr>
			</thead>
			<tbody id="potential_tbody_list"></tbody>
		</table>
	</div>
	<div class="modal-footer alignright">
	    <a class="btn btn-red" data-dismiss="modal">CLOSE</a>
	</div>
</div>
<div class="modal hide fade" id="progress_modal">
	<div class="modal-header bg-color light-green aligncenter">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    <h1>Progress</h1>
	</div>
	<div class="modal-body">
		<table class="table table-striped table-condensed table_select">
			<thead>
				<tr style=" background-color: #2ABF9E; color: #FFFFFF;">
					<th style="width: 20%;">%</th>
					<th style="width: 80%;">Description</th>
				</tr>
			</thead>
			<tbody id="progress_tbody_list"></tbody>
		</table>
	</div>
	<div class="modal-footer alignright">
	    <a class="btn btn-red" data-dismiss="modal">CLOSE</a>
	</div>
</div>

<div class="modal hide fade" id="hPotential_modal">
	<div class="modal-header bg-color light-green aligncenter">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    <h1>Potential History</h1>
	</div>
	<div class="modal-body">
		<table class="table table-striped">
			<thead>
				<tr style=" background-color: #2ABF9E; color: #FFFFFF;">
					<th style="width: 15%;">Date</th>
					<th style="width: 15%;">Potential</th>
					<th style="width: 70%;">Description</th>
				</tr>
			</thead>
			<tbody id="hPotential_tbody_list"></tbody>
		</table>
	</div>
	<div class="modal-footer alignright">
	    <a class="btn btn-red" data-dismiss="modal">CLOSE</a>
	</div>
</div>
<div class="modal hide fade" id="hProgress_modal">
	<div class="modal-header bg-color light-green aligncenter">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    <h1>Progress History</h1>
	</div>
	<div class="modal-body">
		<table class="table table-striped">
			<thead>
				<tr style=" background-color: #2ABF9E; color: #FFFFFF;">
					<th style="width: 15%;">Date</th>
					<th style="width: 15%;">Progress</th>
					<th style="width: 70%;">Description</th>
				</tr>
			</thead>
			<tbody id="hProgress_tbody_list"></tbody>
		</table>
	</div>
	<div class="modal-footer alignright">
	    <a class="btn btn-red" data-dismiss="modal">CLOSE</a>
	</div>
</div>
<div class="modal hide fade" id="sales_modal">
	<div class="modal-header bg-color light-green aligncenter">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    <h1>Sales Representative</h1>
	</div>
	<div class="modal-body">
		<table class="table table-striped table-condensed table_select">
			<thead>
				<tr style=" background-color: #2ABF9E; color: #FFFFFF;">
					<th style="width: 15%;">Emp. No.</th>
					<th style="width: 85%;">Name</th>
				</tr>
			</thead>
			<tbody id="sales_tbody_list"></tbody>
		</table>
	</div>
	<div class="modal-footer alignright">
	    <a class="btn btn-red" data-dismiss="modal">CLOSE</a>
	</div>
</div>
	<script src="js/library/jquery/jquery-1.9.1"></script>
	<script src="js/library/jquery/jquery-ui-1.10.3.custom.js"></script>
	<script src="js/library/masonry/jquery.masonry.min.js"></script>
	<script src="js/library/flexslider/jquery.flexslider-min.js"></script>
	<script src="js/library/dataTables/jquery.dataTables.min.js"></script>
	<script src="js/library/fullcalendar/fullcalendar.min.js"></script>
	<script src="js/library/fullcalendar/gcal.js"></script>
	<script src="js/library/forms/jquery.hotkeys.js"></script>
	<script src="js/library/forms/bootstrap-wysiwyg.js"></script>
	<script src="js/library/forms/picker.js"></script>
	<script src="js/library/forms/picker.date.js"></script>
	<script src="js/library/forms/picker.time.js"></script>
	<script src="js/library/forms/jquery.bootstrap.wizard.min.js"></script>
	<script src="js/library/charts/jquery.flot.js"></script>
	<script src="js/library/charts/jquery.flot.resize.js"></script>
	<script src="js/library/charts/jquery.flot.stack.js"></script>
	<script src="js/library/charts/jquery.flot.pie.min.js"></script>
	<script src="js/library/charts/jquery.sparkline.min.js"></script>
	<script src="js/library/syntaxHighlighter/shCore.js"></script>
	<script src="js/library/syntaxHighlighter/shBrushCss.js"></script>
	<script src="js/library/syntaxHighlighter/shBrushXml.js"></script>
	<script src="js/library/syntaxHighlighter/shBrushJScript.js"></script>
	<script src="js/library/jquery.number.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="js/styler/custom.js"></script>

	<!--timeline-->
	<script defer src="libs/timeline/plugins.js"></script>
	<script defer src="libs/timeline/mylibs/timelinexml.js"></script>
	<script defer src="libs/timeline/script.js"></script>

	<script type="text/javascript">
	    $(document).ready(function(){
	    	function CurrentDate(format)
			{
				var today = new Date();
				var dd = today.getDate();
				var mm = today.getMonth()+1; //January is 0!
				var yyyy = today.getFullYear();

				if(dd<10) {
				    dd='0'+dd
				} 

				if(mm<10) {
				    mm='0'+mm
				} 
				if(format==103)
				{
					today = dd+'/'+mm+'/'+yyyy;
				}
				else if(format==104)
				{
					today = dd+'.'+mm+'.'+yyyy;					
				}
				return today;
			}
	    	function loadTimeline(id)
			{		
				
				if(id!="")
				{
					$(".demo-box").html('');
					console.log("loadTimeline <?php echo $id?>");
				<?php 
					//$conn = sqlsrv_connect("157.179.28.116", "sa", "dioro@4321");
					//sqlsrv_select_db("SaleForecast", $conn);

					include("INC/connectSFC.php");
					$results = array();	
					$sqlStr = "SELECT IDForecast,Project,Potential,Progress, CONVERT(VARCHAR(10), DateProgress10, 104) as DateProgress10, CONVERT(VARCHAR(10), DateProgress20, 104) as DateProgress20,CONVERT(VARCHAR(10), DateProgress30, 104) as DateProgress30,CONVERT(VARCHAR(10), DateProgress40, 104) as  DateProgress40,CONVERT(VARCHAR(10), DateProgress50, 104) as  DateProgress50,CONVERT(VARCHAR(10), DateProgress60, 104) as  DateProgress60,CONVERT(VARCHAR(10), DateProgress70, 104) as  DateProgress70,CONVERT(VARCHAR(10), DateProgress80, 104) as  DateProgress80,CONVERT(VARCHAR(10), DateProgress90, 104) as  DateProgress90,CONVERT(VARCHAR(10), DateProgress100, 104) as  DateProgress100 FROM  Forecast where IDForecast='$id' ";
					$query = sqlsrv_query($ConnectSaleForecast,$sqlStr );//CONVERT(VARCHAR(10), GETDATE(), 104) AS [DD.MM.YYYY]
					$r = 0;
					while ($obj = sqlsrv_fetch_object($query)) {
						$IDForecast = iconv("TIS-620", "UTF-8", $obj->IDForecast);
						$DateProgress10 = iconv("TIS-620", "UTF-8", $obj->DateProgress10);
						$DateProgress20 = iconv("TIS-620", "UTF-8", $obj->DateProgress20);
						$DateProgress30 = iconv("TIS-620", "UTF-8", $obj->DateProgress30);
						$DateProgress40 = iconv("TIS-620", "UTF-8", $obj->DateProgress40);
						$DateProgress50 = iconv("TIS-620", "UTF-8", $obj->DateProgress50);
						$DateProgress60 = iconv("TIS-620", "UTF-8", $obj->DateProgress60);
						$DateProgress70 = iconv("TIS-620", "UTF-8", $obj->DateProgress70);
						$DateProgress80 = iconv("TIS-620", "UTF-8", $obj->DateProgress80);
						$DateProgress90 = iconv("TIS-620", "UTF-8", $obj->DateProgress90);
						$DateProgress100 = iconv("TIS-620", "UTF-8", $obj->DateProgress100);
					}	?>		
						var DP10="<?php echo $DateProgress10?>";
						var DP20="<?php echo $DateProgress20?>";
						var DP30="<?php echo $DateProgress30?>";
						var DP40="<?php echo $DateProgress40?>";
						var DP50="<?php echo $DateProgress50?>";
						var DP60="<?php echo $DateProgress60?>";
						var DP70="<?php echo $DateProgress70?>";
						var DP80="<?php echo $DateProgress80?>";
						var DP90="<?php echo $DateProgress90?>";
						var DP100="<?php echo $DateProgress100?>";						
		                	var today=CurrentDate(104);
						console.log("loadTimeline1"+DP100);
						$divmy = $("<div>").attr("id","my-timeline");
						$divwrap = $("<div>").attr("class","timeline-html-wrap").appendTo($divmy);
						if(DP10!='')
						{
							$div1 = $("<div>").attr("class","timeline-event");
			        		$div1_1 = $("<div>").attr("class","timeline-date").text(DP10).appendTo($div1);	
			        		$div1_2 = $("<div>").attr("class","timeline-title").text("10% Build solution").appendTo($div1);	
			        		$div1_3 = $("<div>").attr("class","timeline-thumb").text("").appendTo($div1);	
			        		$div1_4 = $("<div>").attr("class","timeline-content").text("").appendTo($div1);	
			        		$div1_5 = $("<div>").attr("class","timeline-link").text("").appendTo($div1);
							$divwrap.append($div1);
						}
						if(DP20!='')
						{
							$div2 = $("<div>").attr("class","timeline-event");
			        		$div2_1 = $("<div>").attr("class","timeline-date").text(DP20).appendTo($div2);	
			        		$div2_2 = $("<div>").attr("class","timeline-title").text("20% Presentation/Demo").appendTo($div2);	
			        		$div2_3 = $("<div>").attr("class","timeline-thumb").text("").appendTo($div2);	
			        		$div2_4 = $("<div>").attr("class","timeline-content").text("").appendTo($div2);	
			        		$div2_5 = $("<div>").attr("class","timeline-link").text("").appendTo($div2);
							$divwrap.append($div2);
						}
						if(DP30!='')
						{
							$div3 = $("<div>").attr("class","timeline-event");
			        		$div3_1 = $("<div>").attr("class","timeline-date").text(DP30).appendTo($div3);	
			        		$div3_2 = $("<div>").attr("class","timeline-title").text("30% ร่างโครงการ (ของบประมาณ)").appendTo($div3);	
			        		$div3_3 = $("<div>").attr("class","timeline-thumb").text("").appendTo($div3);	
			        		$div3_4 = $("<div>").attr("class","timeline-content").text("").appendTo($div3);	
			        		$div3_5 = $("<div>").attr("class","timeline-link").text("").appendTo($div3);
							$divwrap.append($div3);
						}
						if(DP40!='')
						{
							$div4 = $("<div>").attr("class","timeline-event");
			        		$div4_1 = $("<div>").attr("class","timeline-date").text(DP40).appendTo($div4);	
			        		$div4_2 = $("<div>").attr("class","timeline-title").text("40% ขั้นตอนการพิจารณาอนุมัติงบประมาณ").appendTo($div4);	
			        		$div4_3 = $("<div>").attr("class","timeline-thumb").text("").appendTo($div4);	
			        		$div4_4 = $("<div>").attr("class","timeline-content").text("").appendTo($div4);	
			        		$div4_5 = $("<div>").attr("class","timeline-link").text("").appendTo($div4);
							$divwrap.append($div4);
						}
						if(DP50!='')
						{
							$div5 = $("<div>").attr("class","timeline-event");
			        		$div5_1 = $("<div>").attr("class","timeline-date").text(DP50).appendTo($div5);	
			        		$div5_2 = $("<div>").attr("class","timeline-title").text("50% ได้งบประมาณ").appendTo($div5);	
			        		$div5_3 = $("<div>").attr("class","timeline-thumb").text("").appendTo($div5);	
			        		$div5_4 = $("<div>").attr("class","timeline-content").text("").appendTo($div5);	
			        		$div5_5 = $("<div>").attr("class","timeline-link").text("").appendTo($div5);
							$divwrap.append($div5);
						}
						if(DP60!='')
						{
							$div6 = $("<div>").attr("class","timeline-event");
			        		$div6_1 = $("<div>").attr("class","timeline-date").text(DP60).appendTo($div6);	
			        		$div6_2 = $("<div>").attr("class","timeline-title").text("60% TOR Final").appendTo($div6);	
			        		$div6_3 = $("<div>").attr("class","timeline-thumb").text("").appendTo($div6);	
			        		$div6_4 = $("<div>").attr("class","timeline-content").text("").appendTo($div6);	
			        		$div6_5 = $("<div>").attr("class","timeline-link").text("").appendTo($div6);
							$divwrap.append($div6);
						}
						if(DP70!='')
						{
							$div7 = $("<div>").attr("class","timeline-event");
			        		$div7_1 = $("<div>").attr("class","timeline-date").text(DP70).appendTo($div7);	
			        		$div7_2 = $("<div>").attr("class","timeline-title").text("70% Bidding date").appendTo($div7);	
			        		$div7_3 = $("<div>").attr("class","timeline-thumb").text("").appendTo($div7);	
			        		$div7_4 = $("<div>").attr("class","timeline-content").text("").appendTo($div7);	
			        		$div7_5 = $("<div>").attr("class","timeline-link").text("").appendTo($div7);
							$divwrap.append($div7);
						}
						if(DP80!='')
						{
							$div8 = $("<div>").attr("class","timeline-event");
			        		$div8_1 = $("<div>").attr("class","timeline-date").text(DP80).appendTo($div8);	
			        		$div8_2 = $("<div>").attr("class","timeline-title").text("80% ยื่นซองแล้ว").appendTo($div8);	
			        		$div8_3 = $("<div>").attr("class","timeline-thumb").text("").appendTo($div8);	
			        		$div8_4 = $("<div>").attr("class","timeline-content").text("").appendTo($div8);	
			        		$div8_5 = $("<div>").attr("class","timeline-link").text("").appendTo($div8);
							$divwrap.append($div8);
						}
						if(DP90!='')
						{
							$div9 = $("<div>").attr("class","timeline-event");
			        		$div9_1 = $("<div>").attr("class","timeline-date").text(DP90).appendTo($div9);	
			        		$div9_2 = $("<div>").attr("class","timeline-title").text("90% ประกาศผลแล้วรอเซ็นสัญญา").appendTo($div9);	
			        		$div9_3 = $("<div>").attr("class","timeline-thumb").text("").appendTo($div9);	
			        		$div9_4 = $("<div>").attr("class","timeline-content").text("").appendTo($div9);
			        		$div9_5 = $("<div>").attr("class","timeline-link").text("").appendTo($div9);
							$divwrap.append($div9);
						}
						if(DP100!='')
						{
							$div10 = $("<div>").attr("class","timeline-event");
			        		$div10_1 = $("<div>").attr("class","timeline-date").text(DP100).appendTo($div10);
			        		$div10_2 = $("<div>").attr("class","timeline-title").text("100% Sign date").appendTo($div10);
			        		$div10_3 = $("<div>").attr("class","timeline-thumb").text("").appendTo($div10);
			        		$div10_4 = $("<div>").attr("class","timeline-content").text("").appendTo($div10);
			        		$div10_5 = $("<div>").attr("class","timeline-link").text("").appendTo($div10);
							$divwrap.append($div10);
						}
						$divT = $("<div>").attr("class","timeline-event");
								//$divT.attr("style","background: none repeat scroll 0 0 yellow;");
				        		$divT_1 = $("<div>").attr("class","timeline-date").text(today).appendTo($divT);
				        		$divT_2 = $("<div>").attr("class","timeline-title").text("Current Date").appendTo($divT);
				        		$divT_3 = $("<div>").attr("class","timeline-thumb").text("").appendTo($divT);
				        		$divT_4 = $("<div>").attr("class","timeline-content").text("Current Date").appendTo($divT);
				        		$divT_5 = $("<div>").attr("class","timeline-link").text("").appendTo($divT);
								$divwrap.append($divT);
						$(".demo-box").append($divmy);
						//form.refresh();
						//$('#my-timeline').timelinexml();
				}				
			}
//------------------------------------------------- cancel ----------------------------------------------------
	$("#btn_cancel").on("click", function() {
		 window.open('','_parent','');
		 window.close();
	});
//-----------------------------------------------------------------------------------------------------------
//------------------------------------------------- Function ------------------------------------------------
			function numFormat(x) {
				var parts = x.toString().split(".");
			    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			    return parts.join(".");
			}
			function calSIProj() {
				hw = sw = app = data = false;
				$("#tbody_description").children().each(function() {
					tempP = $($(this).find("p")[0]).text();
					if (tempP == "HW") {
						hw = true;
					} else if (tempP == "SW") {
						sw = true;
					} else if (tempP == "Application") {
						app = true;
					} else if (tempP == "Data") {
						data = true;
					}
				});
				(hw && sw && app && data) ? $("#siproject").text("SI Project") : $("#siproject").text("Non SI Project");
			}
			function calSignProj() {
				if (parseInt($("#inp_potentialp").val().replace(/%/g, "")) >= 50 && parseFloat($("#inp_inco_evat").val()) >= 5000000) {
    				$("#sigproject").text("Significant Project");
    			} else {
    				$("#sigproject").text("Non Significant Project");
    			}
			}
			function calEndCont() {
				if ($("#inp_signDate").val() != "") {
					slcdate = $("#inp_signDate").datepicker('getDate', '+1d');
					if ($("#inp_pDuration").val() != "") {
						slcdate.setDate(slcdate.getDate() + parseInt($("#inp_pDuration").val()));
					}
					$("#inp_endCont").val(("0" + slcdate.getDate()).slice(-2) + "/" + ("0" + (slcdate.getMonth() + 1)).slice(-2) + "/" + slcdate.getFullYear());

					slcdate = $("#inp_signDate").datepicker('getDate', '+1d');
					if ($("#inp_dPDays").val() != "") {
						slcdate.setDate(slcdate.getDate() + parseInt($("#inp_dPDays").val()));
					}
					$("#inp_dPDate").val(("0" + slcdate.getDate()).slice(-2) + "/" + ("0" + (slcdate.getMonth() + 1)).slice(-2) + "/" + slcdate.getFullYear());
				} else {
					$("#inp_endCont").val("");
					$("#inp_dPDate").val("");
				}
			}
//-----------------------------------------------------------------------------------------------------------

//------------------------------------------------- Initial -------------------------------------------------
			loadTimeline('<?php echo $id?>');
			var _loginID = "<?php echo $_COOKIE['Ses_ID']?>";
	    	var old_inco_ivat, old_inco_evat, old_biddDate, old_signDate, old_pDuration, old_downPayment, old_dPBaht;
	    	var description_type = ["HW", "SW", "Application", "Data", "Site Prep", "On Site", "Training", "Other"];

			$(".form_date").datepicker({
		        dateFormat: 'dd/mm/yy',
		        minDate: -1,
		        maxDate: -2
	    	});
	    	$(".nNumber").number(true, 0);
	    	$(".number").number(true, 2);
	    	$(".form_date").css("cursor", "pointer");
	    	list_potential = [["100%", "มั่นใจว่าได้แน่"], ["75%", "มั่นใจ"], ["50%", "ไม่แน่ใจว่าจะได้โครงการหรือไม่"], ["25%", "พอมีโอกาส"], ["10%", "โอกาสน้อยมาก"]];
	    	list_progress = [["100%", "เซ็นสัญญาแล้ว"], ["90%", "ประกาศผลแล้วรอเซ็นสัญญา"], ["80%", "ยื่นซองแล้ว"], ["70%", "ประกาศประกวดราคาแล้วรอยื่นซอง"], ["60%", "จัดส่งของบประมาณให้สำนักงบประมาณแล้ว"], ["50%", "จัดทำ spec. ให้หน่วยงานอยู่"], ["40%", "ขั้นตอนการพิจารณาอนุมัติงบประมาณ"], ["30%", "ร่างโครงการ (ของบประมาณ)"], ["20%", "Presentation/Demo"], ["10%", "พิจารณาจัดตั้งโครงการใหม่อยู่"], ["0%", "แพ้"],["P", "Pending"], ["v", "ยกเลิกโครงการ"]];
	    	$(list_potential).each(function() {
	    		var item = this;
	    		tr = $('<tr><td>' + item[0] + '</td><td>' + item[1] + '</td></tr>');
	    		tr.click(function() {
	    			$("#inp_potentialp").val(item[0]);
	    			$("#inp_potentialt").val(item[1]);
	    			$("#potential_modal").modal("hide");
	    		});
	    		$("#potential_tbody_list").append(tr);
    		});
	    	$(list_progress).each(function() {
	    		var item = this;
	    		tr = $('<tr><td>' + item[0] + '</td><td>' + item[1] + '</td></tr>');
	    		tr.click(function() {
	    			$("#inp_progressp").val(item[0]);
	    			$("#inp_progresst").val(item[1]);
	    			$("#progress_modal").modal("hide");
	    		});
	    		$("#progress_tbody_list").append(tr);
    		});
    		/*$.ajax({
    			type: "POST",
				dataType: "json",
		        url: "AJAX/listSalesRepresentative.php",
		        success: function(json) {
		        	$.each(json, function() {
	    				var item = this;
	    				tr = $('<tr><td>' + item[0] + '</td><td>' + item[1] + '</td></tr>');
	    				tr.click(function() {
			    			$("#inp_salesname").val(item[1]);
			    			$.ajax({
			    				type: "POST",
								dataType: "json",
						        url: "AJAX/getSalesDepartment.php",
						        data: {
						        	emp: item[0]
						        },
						        success: function(json) {
						        	$("#slc_salesproj").val(json[0][0]);
								}
						    });
			    			$("#sales_modal").modal("hide");
			    		});
			    		if (parseInt(_loginID) == item[0]) tr.click();
			    		$("#sales_tbody_list").append(tr);
	    			});
		        }
		    });*/
//-----------------------------------------------------------------------------------------------------------

//------------------------------------------------- Event ---------------------------------------------------
	    	$("#btn_hPotential_modal").click(function() {
	    		$("#hPotential_modal").modal("show");
	    	});
	    	$("#btn_hProgress_modal").click(function() {
	    		$("#hProgress_modal").modal("show");
	    	});
	    	$("#inp_biddDate").change(function() {
	    		if ($("#inp_biddDate").val() != "" && $("#inp_signDate").val() == "") {
		    		slcdate = $("#inp_biddDate").datepicker('getDate', '+1d');
					slcdate.setDate(slcdate.getDate() + 45);
					$("#inp_signDate").datepicker('setDate', slcdate);
					calEndCont();
	    		}
	    	});
	    	$("#inp_signDate").change(function() {
    			if ($("#inp_signDate").val() != "" && $("#inp_biddDate").val() == "") {
		    		slcdate = $("#inp_signDate").datepicker('getDate', '+1d');
					slcdate.setDate(slcdate.getDate() - 45);
					$("#inp_biddDate").datepicker('setDate', slcdate);
	    		}
	    		calEndCont();
	    	});
	    	$("#inp_inco_evat").blur(function() {
	    		$("#inp_inco_ivat").val($("#inp_inco_evat").val() * 1.07);
	    		$("#inp_dPBaht").val($("#inp_inco_evat").val() * ($("#inp_downPayment").val() / 100));
				$("#inp_downPayment").val((100 / $("#inp_inco_evat").val()) * $("#inp_dPBaht").val());
				tr = '<tr class="error"><td><p class="lead14">Book</p></td><td><p class="lead14">' + new Date().getFullYear() + '</p></td><td><p class="lead14">' + numFormat(parseFloat($("#inp_inco_evat").val()).toFixed(2)) + '</p></td></tr>';
				$("#tbody_bank").empty();
				$("#tbody_bank").append(tr);
				calSignProj();
	    	});
	    	$("#inp_budg_evat").blur(function() {
	    		$("#inp_budg_ivat").val($("#inp_budg_evat").val() * 1.07);
	    	});
	    	$("#inp_cost_evat").blur(function() {
				$("#inp_cost_ivat").val($("#inp_cost_evat").val() * 1.07);
	    	});
	    	$("#chk_downPayment").change(function() {
	    		if ($(this).prop("checked")) {
	    			$("#inp_downPayment").prop("disabled", false);
	    			$("#inp_dPBaht").prop("disabled", false);
	    			$("#inp_dPDays").prop("disabled", false);
	    			$("#inp_dPDate").prop("disabled", false);
	    		} else {
	    			$("#inp_downPayment").prop("disabled", true);
	    			$("#inp_dPBaht").prop("disabled", true);
	    			$("#inp_dPDays").prop("disabled", true);
	    			$("#inp_dPDate").prop("disabled", true);
	    		}
	    	});
	    	$("#inp_pDuration").blur(function() {
    			calEndCont();
	    	});
	    	$("#inp_dPDays").blur(function() {
    			calEndCont();
	    	});
//-----------------------------------------------------------------------------------------------------------

//------------------------------------------------- Load ----------------------------------------------------
		<?php 
			if ($id) {
				include("INC/connectSFC.php");
				$sqlStr = "SELECT ID, FCYear, PEContractNo, EContract, Project, CustName, CustOrg, Potential, Progress, ESRIisTOR, Education, TargetProject, TargetSpecialProject, TargetIncome, TargetBudget, TargetCost, CONVERT(VARCHAR(10), TimeFrameBidingDate, 103) AS TimeFrameBidingDate, CONVERT(VARCHAR(10), TimeFrameContractSigndate, 103) AS TimeFrameContractSigndate, TimeFrameProjectDuration, chkDP, PercentAmount, InvoiceAmount, InvDuration, Book, Forward, Forward2, Forward3, Remark, SaleID, SaleRepresentative, VerticalMarketGroup, ProjectBy, title, custsurname, division, department, CustID, quotno,ProjectName,BudgetYear,EnduserNo,EnduserName,EnduserCode,EnduserOrg,PayerNo,PayerName,PayerCode,PayerOrg,Industry,PayerIndustry,ReasonTechnic,ReasonRelation,ReasonPrice,ProjectStatus,CONVERT(VARCHAR(10), DateUpdated, 103) AS DateUpdated,FlagGenerate, P30company1, P30price1, P30company2, P30price2, P30company3, P30price3, P60company1, P60price1,P60company2, P60price2, P60company3, P60price3  FROM Forecast WHERE IDForecast='$id'";
				$query = sqlsrv_query($ConnectSaleForecast,iconv("UTF-8", "TIS-620", $sqlStr));
				if ($obj = sqlsrv_fetch_object($query)) {
		?>
			tempDescType = ["", "Site Prep", "HW", "SW", "Data", "Application", "On Site", "Training", "GPS", "SURVEY EQUIPMENT", "SYSTEMS", "Other", "CONSULT", "ESRI", "Leica", "Garmin", "NOSTRA"];
			tempFCYear = '<?php echo $obj->FCYear?>';
			//Contract Block 
			$("#inp_contractpe").val('<?php echo $obj->PEContractNo?>');
			$("#inp_contractec").val('<?php echo trim($obj->EContract)?>');
			//Project Description Block 
			$("#projDesc_Year").val('<?php echo $obj->BudgetYear?>');
			$("#projDesc_show").text('<?php echo replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->Project))?>');
			$("#projDesc_Name").val('<?php echo replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->ProjectName))?>');
			//Payer Block 			
			$("#payer_custno").val('<?php echo $obj->PayerNo?>');
			$("#payer_name").val('<?php echo iconv("TIS-620", "UTF-8", $obj->PayerName)?>');
			$("#payer_code").val('<?php echo $obj->PayerCode?>');
			$("#payer_org").val('<?php echo iconv("TIS-620", "UTF-8", $obj->PayerOrg)?>');
			//End User Block 
			$("#enduser_custno").val('<?php echo $obj->EnduserNo?>');
			$("#enduser_name").val('<?php echo iconv("TIS-620", "UTF-8", $obj->EnduserName)?>');
			$("#enduser_code").val('<?php echo $obj->EnduserCode?>');
			$("#enduser_org").val('<?php echo iconv("TIS-620", "UTF-8", $obj->EnduserOrg)?>');
			if('<?php echo trim($obj->quotno)?>' != "") $("#importedFrom").text("Imported from quotation: " + '<?php echo replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->quotno))?>');
			//Target Block 
			$("#potential_tbody_list").children().each(function() {

				if ($($(this).children()[0]).text() == '<?php echo $obj->Potential?>%') {
					$(this).click();
					return false;
				}
			});
			
			$("#progress_tbody_list").children().each(function() {
				var chkProgress = '';
				if('<?php echo $obj->Progress?>' == 'v' || '<?php echo $obj->Progress?>' == 'P'){
					chkProgress = '<?php echo $obj->Progress?>';
				}else{
					chkProgress = '<?php echo $obj->Progress?>%';
				}
				if ($($(this).children()[0]).text() == chkProgress) {
					$(this).click();
					return false;
				}
			});
			var Sprogress='<?php echo $obj->Progress?>';
			if(Sprogress=='P'||Sprogress=='v'||Sprogress=='0'||Sprogress=='10'||Sprogress=='20')
			{
			}
			else if(Sprogress=='30'||Sprogress=='40'||Sprogress=='50')
	    {
	    				//$("#Reason").css("display", "none");
	    				//$("#Price30_modal").modal('show');
	    	$("#Progress_30_div").css("display", "block");'<?php echo iconv("TIS-620", "UTF-8", $obj->P30company1)?>'
				$("#P30company1_name").val('<?php echo iconv("TIS-620", "UTF-8",$obj->P30company1)?>');
				$("#P30company2_name").val('<?php echo iconv('TIS-620', 'UTF-8',$obj->P30company2)?>');
				$("#P30company3_name").val('<?php echo iconv('TIS-620', 'UTF-8',$obj->P30company3)?>');
				$("#P30company1_price").val('<?php echo $obj->P30price1?>').blur();
				$("#P30company2_price").val('<?php echo $obj->P30price2?>').blur();
				$("#P30company3_price").val('<?php echo $obj->P30price3?>').blur();
	    				
	    }
	    else if(Sprogress=='60'||Sprogress=='70'||Sprogress=='80'||Sprogress=='90'||Sprogress=='100')
	    {
	    				//$("#Reason").css("display", "none");
	    				//$("#Price60_modal").modal('show');
	    	$("#Progress_30_div").css("display", "block");
	    	$("#Progress_60_div").css("display", "block");
				$("#P30company1_name").val('<?php echo iconv('TIS-620', 'UTF-8',$obj->P30company1)?>');
				$("#P30company2_name").val('<?php echo iconv('TIS-620', 'UTF-8',$obj->P30company2)?>');
				$("#P30company3_name").val('<?php echo iconv('TIS-620', 'UTF-8',$obj->P30company3)?>');
				$("#P30company1_price").val('<?php echo $obj->P30price1?>').blur();
				$("#P30company2_price").val('<?php echo $obj->P30price2?>').blur();
				$("#P30company3_price").val('<?php echo $obj->P30price3?>').blur();
				$("#P60company1_name").val('<?php echo iconv('TIS-620', 'UTF-8',$obj->P60company1)?>');
				$("#P60company2_name").val('<?php echo iconv('TIS-620', 'UTF-8',$obj->P60company2)?>');
				$("#P60company3_name").val('<?php echo iconv('TIS-620', 'UTF-8',$obj->P60company3)?>');
				$("#P60company1_price").val('<?php echo $obj->P60price1?>').blur();
				$("#P60company2_price").val('<?php echo $obj->P60price2?>').blur();
				$("#P60company3_price").val('<?php echo $obj->P60price3?>').blur();
	    				
	    }

			
			// if('<?php echo $obj->ESRIisTOR?>' == "1") $("#chk_esri").prop("checked", true);
			// if('<?php echo $obj->Education?>' == "1") $("#chk_edu").prop("checked", true);
			// if('<?php echo $obj->TargetProject?>' == "1") $("#chk_proj").prop("checked", true);
			if('<?php echo $obj->TargetSpecialProject?>' == "1") $("#chk_sproj").prop("checked", true);
			$("#inp_inco_evat").val('<?php echo $obj->TargetIncome?>').blur();
			$("#inp_budg_evat").val('<?php echo $obj->TargetBudget?>').blur();
			$("#inp_cost_evat").val('<?php echo $obj->TargetCost?>').blur();
			$("#inp_biddDate").val('<?php echo $obj->TimeFrameBidingDate?>').change();
			$("#inp_signDate").val('<?php echo $obj->TimeFrameContractSigndate?>').change();
			$("#inp_pDuration").val('<?php echo $obj->TimeFrameProjectDuration?>').blur();
			if('<?php echo $obj->chkDP?>' == "0") $("#chk_downPayment").click();
			$("#inp_downPayment").val('<?php echo $obj->PercentAmount?>');
			$("#inp_dPBaht").val('<?php echo $obj->InvoiceAmount?>');
			$("#inp_dPDays").val('<?php echo $obj->InvDuration?>').blur();
			$("#txa_salesremark").val('<?php echo replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->Remark))?>');
			$("#inp_salesname").val('<?php echo replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->SaleRepresentative))?>');
			$("#slc_verticalmarket").val('<?php echo $obj->VerticalMarketGroup?>');
			$("#slc_salesproj").val('<?php echo replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->ProjectBy))?>');
			$("#inp_custtname").val('<?php echo replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->title))?>');
			$("#inp_custlname").val('<?php echo replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->custsurname))?>');
			$("#inp_custdiv").val('<?php echo replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->division))?>');
			$("#inp_custdep").val('<?php echo replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->department))?>');
		<?php 
					$sqlStr = "SELECT IDTypeDescription, Description, Price FROM DescriptionDetail WHERE IDForecast='$id'";
					$query = sqlsrv_query($ConnectSaleForecast,iconv("UTF-8", "TIS-620", $sqlStr));
					while ($obj = sqlsrv_fetch_object($query)) {
		?>
			tr = $('<tr class="warning"><td><p class="lead14">' + tempDescType[<?php echo $obj->IDTypeDescription?>] + '</p></td><td><p class="lead14"><?php echo replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->Description))?></p></td><td><p class="lead14 text-right">' + numFormat(parseFloat('<?php echo $obj->Price?>' != "" ? '<?php echo $obj->Price?>' : "0.00").toFixed(2)) + '</p></td></tr>');
			$("#tbody_description").append(tr);
		<?php 
					}
		?>
			calSIProj();
			tempArr = {};

			
	    	tempstart = $("#inp_signDate").val().split("/")[2];
	    	tempend = $("#inp_endCont").val().split("/")[2];
	    	for(var iii=tempstart;iii<=tempend;iii++)
	    	{
	    		tempArr[iii]=0;
	    	}
	    	
			if ($("#chk_downPayment").prop("checked") && parseFloat($("#inp_dPBaht").val()) > 0) {
				if ($("#inp_dPDate").val() != "") {
					tempArr[$("#inp_dPDate").datepicker('getDate', '+1d').getFullYear()] = parseFloat($("#inp_dPBaht").val()).toFixed(2);
				} else {
					tempArr[tempFCYear] = parseFloat($("#inp_dPBaht").val()).toFixed(2);
				}
			}
		<?php 
					$sqlStr = "SELECT PlanPhase, DurationDelivery, CONVERT(VARCHAR(10), DeliveryDate, 103) AS DeliveryDate, Amount_Percent, Amount, Remark FROM InvoicingPlan WHERE IDForecast='$id' ORDER BY InvoicingPlanID";
					$query = sqlsrv_query($ConnectSaleForecast,iconv("UTF-8", "TIS-620", $sqlStr));
					if (sqlsrv_num_rows($query) > 0) {
		?>
			$("#tbody_bank").empty();
		<?php 
					}
					while ($obj = sqlsrv_fetch_object($query)) {
		?>
			tempYear = '<?php echo $obj->DeliveryDate?>'.split("/")[2];
			if (tempArr[tempYear] > 0) {
				tempArr[tempYear] = (parseFloat(tempArr[tempYear]) + parseFloat('<?php echo $obj->Amount?>')).toFixed(2);
			} else {
				tempArr[tempYear] = parseFloat('<?php echo $obj->Amount?>').toFixed(2);
			}
			tr = '<tr class="warning"><td><p class="lead14"><?php echo $obj->PlanPhase?></p></td><td><p class="lead14"><?php echo $obj->DurationDelivery?></p></td><td><p class="lead14"><?php echo $obj->DeliveryDate?></p></td><td><p class="lead14 text-right">' + numFormat(parseFloat('<?php echo $obj->Amount_Percent?>').toFixed(2)) + '</p></td><td><p class="lead14 text-right">' + numFormat(parseFloat('<?php echo $obj->Amount?>').toFixed(2)) + '</p></td><td><p class="lead14"><?php echo replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->Remark))?></p></td></tr>';
			$("#tbody_invoicing").append(tr);
		<?php 
					}
		?>
			for (tempKey in tempArr) {
				tempType = "Book";
				//if (parseInt(tempKey) > new Date().getFullYear()) tempType = "Forward";
				if (parseInt(tempKey) > $("#inp_signDate").datepicker('getDate', '+1d').getFullYear()) tempType = "Forward";//22/12/14 edit by TUA
				tr = '<tr class="error"><td><p class="lead14">' + tempType + '</p></td><td><p class="lead14">' + tempKey + '</p></td><td><p class="lead14">' + numFormat(parseFloat(tempArr[tempKey]).toFixed(2)) + '</p></td></tr>';
				$("#tbody_bank").append(tr);
			}
		<?php 
					$sqlStr = "SELECT CONVERT(VARCHAR(10), DateStatus, 103) AS DateStatus, Description FROM StatusDetail WHERE IDForecast='$id' ORDER BY IDStatus";
					$query = sqlsrv_query($ConnectSaleForecast,iconv("UTF-8", "TIS-620", $sqlStr));
					while ($obj = sqlsrv_fetch_object($query)) {
		?>
			tr = $('<tr class="warning"><td><p class="lead14"><?php echo $obj->DateStatus?></p></td><td><p class="lead14"><?php echo replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->Description))?></p></td>');
			$("#tbody_status").append(tr);
		<?php 
					}
					$sqlStr = "SELECT DSNNameID FROM DepartmentSupportNeeded WHERE IDForecast='$id' ORDER BY IDDSN";
					$query = sqlsrv_query($ConnectSaleForecast,iconv("UTF-8", "TIS-620", $sqlStr));
					while ($obj = sqlsrv_fetch_object($query)) {
						$DSNNameIDdata = replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->DSNNameID));
						
						$sqlStr2 = "SELECT depName FROM DepartmentDetail WHERE id ='$DSNNameIDdata' and flag <> '1'";
						$query2 = sqlsrv_query($ConnectSaleForecast,iconv("UTF-8", "TIS-620", $sqlStr2) );
						while ($obj2 = sqlsrv_fetch_object($query2)) {
							$DSNNamedata = replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj2->depName));
							?>
								tr = $('<tr class="warning"><td><p class="lead14"><?php echo replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $DSNNamedata))?></p></td></tr>');
								$("#tbody_deptsup").append(tr);
							<?php 
						}
					}
					$sqlStr = "SELECT EPName, qty FROM ESRIProduct WHERE IDForecast='$id' ORDER BY IDEP";
					$query = sqlsrv_query($ConnectSaleForecast,iconv("UTF-8", "TIS-620", $sqlStr));
					while ($obj = sqlsrv_fetch_object($query)) {
		?>
    		tr = $('<tr class="warning"><td><p class="lead14"><?php echo replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->EPName))?></p></td><td><p class="lead14"><?php echo $obj->qty?></p></td></tr>');
			$("#tbody_esri").append(tr);
		<?php 
					}
					$sqlStr = "SELECT Name, qty FROM SEProduct WHERE IDForecast='$id' ORDER BY ID";
					$query = sqlsrv_query($ConnectSaleForecast,iconv("UTF-8", "TIS-620", $sqlStr));
					while ($obj = sqlsrv_fetch_object($query)) {
		?>
    		tr = $('<tr class="warning"><td><p class="lead14"><?php echo replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->Name))?></p></td><td><p class="lead14"><?php echo $obj->qty?></p></td></tr>');
			$("#tbody_SE").append(tr);
		<?php 
					}
					$sqlStr = "SELECT LPName, qty FROM LeicaProduct WHERE IDForecast='$id' ORDER BY IDLP";
					$query = sqlsrv_query($ConnectSaleForecast,iconv("UTF-8", "TIS-620", $sqlStr));
					while ($obj = sqlsrv_fetch_object($query)) {
		?>
    		tr = $('<tr class="warning"><td><p class="lead14"><?php echo replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->LPName))?></p></td><td><p class="lead14"><?php echo $obj->qty?></p></td></tr>');
			$("#tbody_leica").append(tr);
		<?php 
					}
					$sqlStr = "SELECT TMPName, qty FROM TMProduct WHERE IDForecast='$id' ORDER BY IDTMP";
					$query = sqlsrv_query($ConnectSaleForecast,iconv("UTF-8", "TIS-620", $sqlStr));
					while ($obj = sqlsrv_fetch_object($query)) {
		?>
    		tr = $('<tr class="warning"><td><p class="lead14"><?php echo replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->TMPName))?></p></td><td><p class="lead14"><?php echo $obj->qty?></p></td></tr>');
			$("#tbody_garmin").append(tr);
		<?php 
					}
					$sqlStr = "SELECT GPName, qty FROM GEOProduct WHERE IDForecast='$id' ORDER BY IDGP";
					$query = sqlsrv_query($ConnectSaleForecast,iconv("UTF-8", "TIS-620", $sqlStr));
					while ($obj = sqlsrv_fetch_object($query)) {
		?>
    		tr = $('<tr class="warning"><td><p class="lead14"><?php echo replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->GPName))?></p></td><td><p class="lead14"><?php echo $obj->qty?></p></td></tr>');
			$("#tbody_nostra").append(tr);
		<?php 
					}
					$sqlStr = "SELECT CONVERT(VARCHAR(10), DateChange, 103) AS DateChange, Potential, PotentialName FROM HistoryPotential WHERE IDForecast='$id' ORDER BY ID DESC";
					$query = sqlsrv_query($ConnectSaleForecast,iconv("UTF-8", "TIS-620", $sqlStr));
					while ($obj = sqlsrv_fetch_object($query)) {
		?>
			$("#hPotential_tbody_list").append($('<tr><td><?php echo $obj->DateChange?></td><td><?php echo $obj->Potential?>%</td><td><?php echo replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->PotentialName))?></td></tr>'));
		<?php 
					}
					$sqlStr = "SELECT CONVERT(VARCHAR(10), DateChange, 103) AS DateChange, Progress, ProgressName FROM HistoryProgress WHERE IDForecast='$id' ORDER BY ID DESC";
					$query = sqlsrv_query($ConnectSaleForecast,iconv("UTF-8", "TIS-620", $sqlStr));
					while ($obj = sqlsrv_fetch_object($query)) {
		?>
			$("#hProgress_tbody_list").append($('<tr><td><?php echo $obj->DateChange?></td><td><?php echo $obj->Progress?>%</td><td><?php echo replaceSQuoteAndNl(iconv("TIS-620", "UTF-8", $obj->ProgressName))?></td></tr>'));
		<?php 
					}
				}
			}
		?>
//-----------------------------------------------------------------------------------------------------------

	    });
	</script>

</body>
</html>