﻿<!DOCTYPE html>
<html lang="en">
	
<!-- Mirrored from bootstrapstyler.com/preview/_/flatdashboard/ui.html by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 18 Oct 2013 07:02:36 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
		<meta charset="utf-8">
		<title>sfs:::Report->Potential</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="Izuddin Helmi">

		<!-- Stylesheets -->
		<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
		<link href="js/library/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
		<link href="css/styler/style.css" rel="stylesheet">
		<link href="js/library/syntaxHighlighter/shCore.css" rel="stylesheet" type="text/css" />
		<link href="js/library/syntaxHighlighter/shThemeDefault.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="css/styler/fontello.css">
	    <link rel="stylesheet" href="css/styler/animation.css"><!--[if IE 7]>
	    <link rel="stylesheet" href="css/styler/fontello-ie7.css"><![endif]-->
	    

		<!-- Custom Fonts -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300,500' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

		<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
		  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	</head>
<?php  $checkmenu = '4'; ?>
<body cz-shortcut-listen="true" style="margin-top:0px;">  		
<div id="loading" style="height: 100%; width: 100%; position: fixed; left: 0; top: 0; z-index: 1051 !important; background-color: black; filter: alpha(opacity=9); -khtml-opacity: 0.9; -moz-opacity: 0.9; opacity: 0.9;"><img src="img/loading.gif" style="width: 150px; height: 150px; position: fixed; top: 50%; left: 50%; margin: -75px 0 0 -75px;"></div>
<section class="page-header bg-color white">
	<div class="container">
		<div class="row">
			<div class="span12">
				<div class="section-header clearfix">
					<div class="pull-left">
						<h2><i class="icon-clipboard-1"></i>Report</h2>
					</div>
					<div id="shortcut" class="nav-collapse pull-right"><ul class="shortcuts unstyled">
						<?php include("menu.php");?>
					</ul>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="content">
	<div class="page-tab">
		<div class="page-tab-header">
			<div class="container">
				<ul class="nav nav-tabs clearfix" id="socialTab">
					<li class="active"><a href="#"><h5>Potential</h5></a></li>
					<li ><a  href="report.php"><h5>Progress</h5></a></li>
					
					<li ><a href="report_month.php"><h5>Month</h5></a></li>
					<li><a href="report_Qur.php"><h5>ภาพรวมรายไตรมาส</h5></a></li>
					<li ><a href="report_potential.php"><h5>ภาพรวมรายเดือน</h5></a></li>
					<li ><a href="report_siteref.php"><h5>Site Reference หนังสือรับรอง</h5></a></li>
				</ul>
			</div>
		</div>
		<div class="tab-content">
			<div class="tab-pane fade active in container" id="ReportProgress">
				<div class="widget bg-color turqoise rounded" id="focusReport">
					<div class="box-padding">
						<h1>Potential</h1>
						<p class="lead"></p>
					</div>
				</div>
				<div class="row">
					<div class="span3 widget widget-option">
						<div class="profile-head bg-color dark-blue rounded-top">
							<div class="box-padding" style="text-align:center">
									<h3 class="normal"><i class="icon-calendar"></i>Year</h3>
									<span class=" pull-left" style="margin-left:-10px"></span>
							</div>
						</div>
						<div class="bg-color white rounded-bottom">
								<ul class="menu unstyled">
									<li ><p>
									  	<input type="radio"  class="optionsYear1" id="optionsRadios1" name="rdoYear1" value="bidding" style="margin-top:-2px">
									  	 Bidding Date <span class=" pull-right" style="margin-top:-8px"><select id="slc_year" name="slc_year" class="span1" style="width:80px;"></select></span></p></li>
									<li class="current"><p>
									  	<input type="radio" checked class="optionsYear2" id="optionsRadios2" name="rdoYear1" style="margin-top:-2px" value="signdate">
									  	 Signdate <span class="pull-right"style="margin-top:-8px"><select id="slc_year_sign" name="slc_year_sign" class="span1" style="width:80px;"></select></span></p></li>
								</ul>
						</div>

						<div class="profile-head bg-color dark-blue rounded-top">
							<div class="box-padding" style="text-align:center">
									<h3 class="normal"><i class="icon-th-thumb-empty"></i>Option</h3>
							</div>
						</div>
						<div class="bg-color white rounded-bottom">
								<ul class="menu unstyled">
									<li><p class="sales">
									  	<input type="checkbox" value="1" style="margin-top:-2px" class="chkSpecial"> Mega Project<span class="pull-right"style="margin-top:-8px"></span></p></li>
								</ul>
						</div>
						<div style="text-align:center"><a class="btn btn-blue btn-large" id="btnReport" style="width:190px">Report</a></div>
						<div id="showError" class="alert alert-error hide" style="margin-top:10px;margin-bottom:-10px;">
							<button type="button" class="close" data-dismiss="alert">×</button>
								<i class="icon-attention-3 pull-left" style="font-size:20px;margin-top:-3px;"></i>
								<div class="media-body">
									<strong>กรุณาเลือก Sale ด้วย</strong>
								</div>
						</div>
						<div id="showWarning" class="alert alert-block hide" style="margin-top:10px;margin-bottom:-10px;">
							<button type="button" class="close" data-dismiss="alert">×</button>
							<i class="icon-info-circle pull-left" style="font-size:25px;margin-top:-7px;"></i>
							<div class="media-body">
								<strong>ไม่พบข้อมูล</strong>
							</div>
						</div>
					</div>
					
					<div class="span3 widget widget-option">

						<div class="profile-head bg-color dark-blue rounded-top">
							<div class="box-padding" style="text-align:center">
									<h3 class="normal"><i class="icon-users-1"></i>Sales</h3>
									<span class=" pull-left" style="margin-left:-10px;margin-top:-20px"><input type="checkbox" class="salesAll" title="Select all"></span>
							</div>
						</div>
						<div class="bg-color white rounded-bottom">
								<ul class="menu unstyled" id="listsaleName"></ul>
						</div>
					</div>
					<div id="showGraph">
					 <div class="span6 bg-color white rounded">
						<div class="box-padding">
							
							<div class="tab-content">
								<div id="containerPie" style="min-width: 400px; height: 400px; margin: 0 auto;display:none;" ></div>
								<div id="containerBar" style="min-width: 400px; height: 400px; margin: 0 auto;"></div>
							</div>
							

						</div>
						
					</div>
					<div style="text-align:right" class="bg-color rounded" ><a style="margin:10px 0px;" class="btn btn-blue" id="btnSwitch" >Switch Graph</a></div>
					</div>
				</div>
				<div class="widget widget-chart bg-color dark-blue rounded">
					<div style="background-color: #F0AD4E;">
						<div id="charts-data" class="monthly clearfix">	
							<div class="box-padding" style="padding:20px;">
							<div class="clearfix" >
								<h2 class="charts-data-quarter pull-left" style="color: #FFFFFF;"></h2>
								<h2 id="sumResult" class="pull-right" style="margin: 10px 0;color: #FFFFFF;" title="Income">0</h2>
							</div>
							<div class="clearfix" >
								<span id="numResult" class="label label-inverse pull-left" style="font-size:20px;padding:10px;margin-right:10px;" title="Number of results">0</span>
								<font class="pull-left" style="color: #FFFFFF;font-size:16px;"><b>รายการ</b></font>
								<div class="box-padding pull-right" style="padding:5px;margin-right:-5px;">
									<label style="background-color: #DFF0D8; display: inline-block; width: 150px; text-align: center; float: right; cursor: default;color: #777777;">Potential>50</label>
									<label style="background-color: #FCF8E3; display: inline-block; width: 150px; text-align: center; float: right; cursor: default;color: #777777;">Potential=50</label>
									<label style="background-color: #F2DEDE; display: inline-block; width: 150px; text-align: center; float: right; cursor: default;color: #777777;">Potential&lt;50</label>	
								</div>
							</div>
							</div>
							<div class="charts-data-table">
								<div class="box-padding" style="padding:0px;" id="focusShow">
									<table class="table table-striped showdetail" id="detail" >
										<thead>
											<tr role="row">
												<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;" width="80px">PE Contract</th>
												<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;"width="480px">Project Name</th>
												<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;" width="120px">SalesName</th>
												<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;" width="80px">BiddingDate</th>
												<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;" width="80px">SignDate</th>
												<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align:right" width="100px">Income</th>
												<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;" width="20px"><i class='icon-newspaper' title='View'></i></th>
											</tr>
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
		<div id="footer" class="bg-color dark-blue">
			<div class="container">
				<div class="box-padding">
					Copyright &copy; 2013 Sales Forecast
				</div>
			</div>
		</div>
		<!-- Javascript -->
		<!-- JS:jquery-->
		<!--<script src="../../../../ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>-->
  		<script src="js/library/jquery/jquery.min.js"></script>
		<script src="js/library/jquery/jquery-ui.min.js"></script>

		<!-- JS:masonry-->
		<script src="js/library/masonry/jquery.masonry.min.js"></script>

		<!-- JS:flexslider-->
		<script src="js/library/flexslider/jquery.flexslider-min.js"></script>

		<!-- JS:tables-->
		<script src="js/library/dataTables/jquery.dataTables.min.js"></script>

		<!-- JS:calendar-->
		<script src="js/library/fullcalendar/fullcalendar.min.js"></script>
		<script src="js/library/fullcalendar/gcal.js"></script>

		<!-- JS:forms-->
		<script src="js/library/forms/jquery.hotkeys.js"></script>
		<script src="js/library/forms/bootstrap-wysiwyg.js"></script>
		<script src="js/library/forms/picker.js"></script>
		<script src="js/library/forms/picker.date.js"></script>
		<script src="js/library/forms/picker.time.js"></script>
		<script src="js/library/forms/jquery.bootstrap.wizard.min.js"></script>

		<!-- JS:charts-->
		<script src="js/library/charts/jquery.flot.js"></script>
		<script src="js/library/charts/jquery.flot.resize.js"></script>
		<script src="js/library/charts/jquery.flot.stack.js"></script>
		<script src="js/library/charts/jquery.flot.pie.min.js"></script>
		<script src="js/library/charts/jquery.sparkline.min.js"></script>
		<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="excanvas.min.js"></script><![endif]-->

		<!-- JS:syntaxHighlighter-->
		<script src="js/library/syntaxHighlighter/shCore.js"></script>
		<script src="js/library/syntaxHighlighter/shBrushCss.js"></script>
		<script src="js/library/syntaxHighlighter/shBrushXml.js"></script>
		<script src="js/library/syntaxHighlighter/shBrushJScript.js"></script>

		<!-- JS:bootstrap-->
		<script src="bootstrap/js/bootstrap.min.js"></script>
		
		<!-- JS:custom js for this template -->
		<script src="js/styler/custom.js"></script>

							<script src="js/highcharts.js"></script>
							<script src="js/exporting.js"></script>
			<script type="text/javascript">
			var saleid= "";
			var userid= "";
			var typeYear= 'signdate';
			var slcYear=(new Date).getFullYear();
			var slcOption='0';
			var selectNo = '1';
			Number.prototype.nformat = function() {
			    return this.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
			};
		    $(document).ready(function(){   
				saleid= '<?php echo $_COOKIE['Ses_ID']?>';
			 	userid= '<?php echo $_COOKIE['Ses_ID']?>';
				$("#slc_year_sign").disableSelection();
		    	$.ajax({ 
		    		cache: false,
					type: "POST",
					dataType: "json",
		        	url: "AJAX/select_year.php",
		        	success: function (json) {
		        		$.each(json, function() {
							var item = this;
		        			$("#slc_year").append("<option value='" + item[0] + "'>" + item[0] + "</option>");
						});
						$("#slc_year").val((new Date).getFullYear());
			        }
			    });
			    $.ajax({ 
			    	cache: false,
					type: "POST",
					dataType: "json",
		        	url: "AJAX/select_yearSign.php",
		        	success: function (json) {
		        		$.each(json, function() {
							var item = this;
		        			$("#slc_year_sign").append("<option value='" + item[0] + "'>" + item[0] + "</option>");
						});
						$("#slc_year_sign").val((new Date).getFullYear());
			        }
			    });
				$('.optionsYear1').on('click',function(){
					$('.optionsYear1').closest('li').attr("class", "current");
					$('.optionsYear2').closest('li').attr("class", "");
					$("#slc_year_sign").disableSelection();
					$("#slc_year").enableSelection();
				});
				$('.optionsYear2').on('click',function(){
					$('.optionsYear1').closest('li').attr("class", "");
					$('.optionsYear2').closest('li').attr("class", "current");
					$("#slc_year").disableSelection();
					$("#slc_year_sign").enableSelection();
				});
				
				
				
				$.ajax({ 
					type : "POST",
					dataType: "json",
			        url: "Ajax/SV_Select_saleup.php",
		        	data: {
		        		saleid: saleid
		        	},
		        	success: function (json) {
			        	$(json).each(function() {
			        		var empno=this[0];
			        		var name=this[1];
						if(empno == saleid){
							checked = "checked";
							classli = "current";
						}else{
							checked = "";
							classli = "";
						}
		        		$("#listsaleName").append('<li class="' + classli + '"><p class="sales"><input type="checkbox" name="chkname" style="margin-top:-2px" class="chksales" value="' + empno + '" ' + checked + ' /> ' + name + '<span class=" pull-right" style="margin-top:-8px"></span></p></li>');
					});
					$('.salesAll').on('click',function(){
						if ($(".salesAll").is(':checked')) {
							$(".chksales").prop("checked", true);
							$('.chksales').closest('li').attr("class", "current");
						} else {
							$(".chksales").prop("checked", false);
							$('.chksales').closest('li').attr("class", "");
						}
			    	});
					$('.chksales').on('click',function(){
						if ( $(this).prop('checked')) {
							$(this).closest('li').attr("class", "current");
					    } else {
							$(this).closest('li').attr("class", "");
					    }
						if( $(".chksales:checked").length == $(".chksales").length){
							$(".salesAll").prop("checked", true);
						}else{
							$(".salesAll").prop("checked", false);
						}
					});
		        }
				});
			
			showReport('0');
			function showReport(showNo) {
				$("#loading").show();
				//var _saleid= '<?php echo $_COOKIE['Ses_ID']?>';
				console.log('potential: ',slcYear+' '+saleid+' '+typeYear);
				$.ajax({ 
					type : "POST",
					dataType: "json",
					url: "AJAX/SV_report_potentialPT.php",
					data: {
						year: slcYear,
						saleid: saleid,
						type: typeYear,
						option: slcOption
					},
					success: function (json) {
					if(json.length == 0){
						$('#showWarning').removeClass("hide");
						$('#showGraph').hide();
						$(".charts-data-quarter").text('');
						$("#sumResult").text('');
						$("#numResult").text('0');
						$('#detail > tbody:last').empty();
						var noresult = "<tr><td colspan='7' style='text-align:center;'><p class='lead14'>ไม่พบข้อมูล</p></td></tr>";
						$('#detail').append(noresult);
						$("#loading").hide();
					}else{
						document.getElementById('focusReport').scrollIntoView();
						$('#showGraph').show();
						var dataBar ='';
						var dataPie ='';
						var P100=0;
						var P75=0;
						var P50=0;
						var P25=0;
						var P10=0;
						$.each(json, function() {
							//var item = this;
							//console.log(this[1]);
							P100=parseFloat(this[1]);
							P75=parseFloat(this[2]);
							P50=parseFloat(this[3]);
							P25=parseFloat(this[4]);
							P10=parseFloat(this[5]);
						});
						if(showNo == '0'){
							$('#detail > tbody:last').empty();
							$(".charts-data-quarter").text('');
							$("#sumResult").text('');
							$("#numResult").text('0');
							dataBar = [P100,P75,P50,P25,P10];
							dataPie = [
							  ['100', P100],
							  ['75', P75],
							  ['50', P50],
							  ['25', P25],
							  ['10', P10]
							];
						}else{
							if(showNo == '100'){
								var p100Pie = {name: '100',y: P100,sliced: true,selected: true};
								var p100Bar = {y:P100,selected:true};
							}else{
								var p100Pie = ['100', P100];
								var p100Bar = P100;
							}
							if(showNo == '75'){
								var p75Pie = {name: '75',y: P75,sliced: true,selected: true};
								var p75Bar = {y:P75,selected:true};
							}else{
								var p75Pie = ['75', P75];
								var p75Bar = P75;
							}
							if(showNo == '50'){
								var p50Pie = {name: '50',y: P50,sliced: true,selected: true};
								var p50Bar = {y:P50,selected:true};
							}else{
								var p50Pie = ['50', P50];
								var p50Bar = P50;
							}
							if(showNo == '25'){
								var p25Pie = {name: '25',y: P25,sliced: true,selected: true};
								var p25Bar = {y:P25,selected:true};
							}else{
								var p25Pie = ['25', P25];
								var p25Bar = P25;
							}
							if(showNo == '10'){
								var p10Pie = {name: '10',y: P10,sliced: true,selected: true};
								var p10Bar = {y:P10,selected:true};
							}else{
								var p10Pie = ['10', P10];
								var p10Bar = P10;
							}
							dataBar = [p100Bar,p75Bar,p50Bar,p25Bar,p10Bar];
							dataPie = [p100Pie,p75Pie,p50Pie,p25Pie,p10Pie];
						}
						RanderBarChart('containerBar', dataBar);
						RenderPieChart('containerPie', dataPie); 
					}
					}
				});
				
			}
			
			function RenderPieChart(elementId, dataList) {
                new Highcharts.Chart({
                    chart: {
                        renderTo: elementId,
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false
                    }, 
                    credits:{
                    	enabled: false
                    },
                    exporting: {
			            enabled: false
			        },
                    title: {
                        text: "Potential " +slcYear
                    },
                    tooltip: {
                        formatter: function () {
                            return '<b>' + this.point.name + '</b> : ' + this.percentage.nformat() + ' %<br>click to detail' ;
                        }
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                color: '#000000',
                                connectorColor: '#000000',
                                formatter: function () {
                                    return '<b>' + this.point.name + '</b> : ' + this.point.y.nformat() ;
                                }
                            }
                        },
                        series: {                        	
							 events: {
			                    click: function(event) {
									document.getElementById('focusShow').scrollIntoView();
									selectNo = event.point.name;
									showdetail(event.point.name,event.point.y);
			                    }
			                }
                        }
                    },
                    series: [{
                        type: 'pie',
                        name: 'Potential',
                        data: dataList
                    }],
                    
                    colors: [
					   '#749DD1', 
					   '#D67B76', 
					   '#C5DC96', 
					   '#AE9BC8', 
					   '#66BCD5', 
					   '#F19F55', 
					   '#84B5EF', 
					   '#8AB9EF', 
					   '#F89995', 
					   '#D8F4A0', 
					   '#C5B1E6', 
					   '#94EAFB', 
					   '#FEC283', 
					   '#CADDFB', 
					   '#FECCCD', 
					   '#EAFBCE'
					]
                });
				$("#loading").hide();
            }
			function RanderBarChart(elementId,dataList){
				$('#'+elementId).highcharts({

			        chart: {
			            type: 'column'
			        },			         
                    credits:{
                    	enabled: false
                    },
                    exporting: {
			            enabled: false
			        },
                    title: {
                        text: "Potential "+slcYear
                    },
			        xAxis: {
			            categories: ['100', '75', '50', '25', '10']  
			        },
					tooltip: {
                        formatter: function () {
                            return '<b>Income</b> : ' + this.y.nformat() + '<br>click to detail' ;
                        }
                    },
			        plotOptions: {
			            series: {
			                allowPointSelect: true,
			                cursor: 'pointer',
			                point: {
			                    events: {
			                        click: function() {
										document.getElementById('focusShow').scrollIntoView();
										selectNo = this.category;
										showdetail(this.category,this.y);				
			                        }
			                    }
			                }
			            }
			        },

			        series: [{
			        	data: dataList      
			            ,name:"Income "        
			        }]
			    });
				$("#loading").hide();
			}
			function showdetail(PotentialN,potentialVal){
				$("#loading").show();
				$(".charts-data-quarter").text("Potential " + PotentialN);
				$("#sumResult").text(potentialVal.nformat() + ' บาท');
				//var _saleid= '<?php echo $_COOKIE['Ses_ID']?>';
				console.log('potential_detail: ',slcYear+' '+saleid+' '+PotentialN+' '+typeYear);
				$.ajax({ 
					type : "POST",
				dataType: "json",
				url: "AJAX/SV_report_potentialPT_detail.php",
				data: {
					year: slcYear,
					saleid: saleid,
					potential : PotentialN,
					type: typeYear,
					option: slcOption
				},
				success: function(json) {
					getDetail(json);
				}
				});			
			}
			
			function getDetail(json) {
            	    console.log("showTable:",json);	
					document.getElementById('numResult').innerHTML=json.length;
					$('#detail > tbody:last').empty();
					if(json.length== 0){
						var noresult = "<tr><td colspan='7' style='text-align:center;'><p class='lead14'>ไม่พบข้อมูล</p></td></tr>";
						$('#detail').append(noresult);
					}else{
            	    $.each(json, function() {
	            	    	var newrow="";
	            	    	var Potential=this[0];
	            	    	var PEContractNo=this[1];
	            	    	var Project=this[2];
	            	    	var SaleRepresentative=this[3];
	            	    	var TimeFrameBidingDate=this[4];
	            	    	var TimeFrameContractSigndate=this[5];
	            	    	var TargetIncome=parseFloat(this[6]);
	            	    	var ID=this[8];
            	    	if(Potential>50)
            	    	{
            	    		newrow=$('<tr>').attr('class','success');
            	    	}
            	    	else if(Potential==50)
            	    	{
            	    		newrow=$('<tr>').attr('class','warning');
            	    	}else if(Potential<50)
            	    	{
            	    		newrow=$('<tr>').attr('class','error');
            	    	}
            	    	var cols="";
            	    	cols+="<td><p class='lead14'>"+PEContractNo+"</p></td>";
            	    	cols+="<td><p class='lead14'>"+Project+"</p></td>";
            	    	cols+="<td><p class='lead14'>"+SaleRepresentative+"</p></td>";
            	    	cols+="<td><p class='lead14'>"+TimeFrameBidingDate+"</p></td>";
            	    	cols+="<td><p class='lead14'>"+TimeFrameContractSigndate+"</p></td>";
            	    	cols+="<td style='text-align:right'><p class='lead16'>"+TargetIncome.nformat()+"</p></td>";
						/*if(SaleID==userid)
            	    	{
            	    		cols+="<td style='text-align:center'><a href='javascript:edit("+ID+");' title='Edit'><i class='i_btn icon-pencil-1 iconEdit' style='color:#2ABF9E;'></i></a></td>";
            	    	}
            	    	else
            	    	{*/
            	    		cols+="<td style='text-align:center'><a href='javascript:view("+ID+");' title='View'><i class='i_btn icon-newspaper iconView' style='color:#2ABF9E;'></i></a></td>";
            	    	//}
            	    	newrow.append(cols);

            	    	$('#detail').append(newrow);
            	    });
					}
				$("#loading").hide();
           	}
			
			$('#btnSwitch').on('click',function(){
				showReport(selectNo);
				if($('#containerBar').is(':hidden')) {
					$("#containerBar").show();
					$("#containerPie").hide();
				}else{
					$("#containerBar").hide();
					$("#containerPie").show();
				}
			});
			$('#btnReport').on('click',function(){
				$('#showError').addClass("hide");
				$('#showWarning').addClass("hide");
				if ($(".chksales:checked").length == 0) {
						$('#showError').removeClass("hide");
					}else{
						var chkid = '';
						$("input[name='chkname']").each(function () {
							if ( $(this).prop('checked')) {
								if(chkid=='')
								{
									chkid= $(this).val();
								}
								else
								{
									chkid = chkid+","+$(this).val();
								}
							}
						});

						if ($(".optionsYear1").is(':checked')) {
							typeYear=$('#optionsRadios1').val();	
							slcYear=$("#slc_year").val();
						}else{
							typeYear=$('#optionsRadios2').val();			
							slcYear=$("#slc_year_sign").val();
						}
						saleid=chkid;
						if ( $('.chkSpecial').prop('checked')) {
							slcOption="0,1";
						}else{
							slcOption="0";
						}
						showReport('1');
					}
			});
		});
			function edit(id){
				window.location="newproject.php?id="+id+"&page=report_PT";
			}
			function view(id){
				
				window.open("previewforecastReport.php?id="+id);
			}
		</script>
  	</body>

<!-- Mirrored from bootstrapstyler.com/preview/_/flatdashboard/ by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 18 Oct 2013 07:00:04 GMT -->
</html>