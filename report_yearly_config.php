﻿<!DOCTYPE html>
<html lang="en">
	
<!-- Mirrored from bootstrapstyler.com/preview/_/flatdashboard/ui.html by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 18 Oct 2013 07:02:36 GMT -->
<!-- Added by HTTrack --><!-- /Added by HTTrack -->
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
		<meta charset="utf-8">
		<title>SFS:::Yearly Report</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="Izuddin Helmi">

		<!-- Stylesheets -->
		<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
		<link href="js/library/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
		<link href="css/styler/style.css" rel="stylesheet">
		<link href="js/library/syntaxHighlighter/shCore.css" rel="stylesheet" type="text/css" />
		<link href="js/library/syntaxHighlighter/shThemeDefault.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="css/styler/fontello.css">
	    <link rel="stylesheet" href="css/styler/animation.css"><!--[if IE 7]>
	    <link rel="stylesheet" href="css/styler/fontello-ie7.css"><![endif]-->
	    

		<!-- Custom Fonts -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300,500' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

		<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
		  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	</head>
<?php  $checkmenu = '4'; ?>
<body cz-shortcut-listen="true" style="margin-top:0px;">	
<div id="loading" style="height: 100%; width: 100%; position: fixed; left: 0; top: 0; z-index: 1051 !important; background-color: black; filter: alpha(opacity=9); -khtml-opacity: 0.9; -moz-opacity: 0.9; opacity: 0.9;"><img src="img/loading.gif" style="width: 150px; height: 150px; position: fixed; top: 50%; left: 50%; margin: -75px 0 0 -75px;"></div>	
<section class="page-header bg-color white">
	<div class="container">
		<div class="row">
			<div class="span12">
				<div class="section-header clearfix">
					<div class="pull-left">
						<h2><i class="icon-clipboard-1"></i>Report</h2>
					</div>
					<div id="shortcut" class="nav-collapse pull-right"><ul class="shortcuts unstyled">
						<?php include("menu.php");?>
					</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="content">
	<div class="page-tab">
		<div class="page-tab-header">
			<div class="container">
				<ul class="nav nav-tabs clearfix" id="socialTab">
					<li><a href="report_PT_all.php"><h5>Potential</h5></a></li>
					<li ><a  href="report_all.php"><h5>Progress</h5></a></li>
					<li ><a href="report_month_all.php"><h5>Month</h5></a></li>
					<li class="active"><a href="#"><h5>Yearly</h5></a></li>
					<li><a href="report_Qur_all.php"><h5>ภาพรวมรายไตรมาส</h5></a></li>
					<li><a href="report_potential_all.php"><h5>ภาพรวมรายเดือน</h5></a></li>
					<li ><a href="report_siteref_all.php"><h5>Site Reference หนังสือรับรอง</h5></a></li>
				</ul>
			</div>
		</div>

		<div class="page-tab-header" style="background: #EEEFF2!important">
			<div class="container">
				<ul class="nav nav-tabs clearfix" id="socialTab" style="margin-left:250px;">
					<li ><a href="report_yearly.php"><h5>Yearly Report</h5></a></li>
					<li class="active"><a href="#"><h5>กำหนดโครงการเพื่อออกรายงาน Yearly Report</h5></a></li>
				</ul>
			</div>
		</div>
		<div class="tab-content">
			<div class="tab-pane fade active in container" id="ReportProgress">
				<div class="widget bg-color turqoise rounded" id="focusReport">
					<div class="box-padding">
						<h1>กำหนดโครงการเพื่อออกรายงาน Yearly Report</h1>
						<p class="lead"></p>
					</div>
				</div>
				<div class="row">
					<div class="span3 widget widget-option">
						<div class="profile-head bg-color dark-blue rounded-top">
							<div class="box-padding" style="text-align:center">
									<h3 class="normal"><i class="icon-calendar"></i>Year</h3>
									<span class=" pull-left" style="margin-left:-10px"></span>
							</div>
						</div>
						<div class="bg-color white rounded-bottom">
								<ul class="menu unstyled">
									<li class="current"><p>
									  	
									  	 Signdate <span class="pull-right"  style="margin-top:-8px"><select id="slc_year_sign" name="slc_year_sign" class="span1" style="width:80px;"></select></span></p></li>
								</ul>
						</div>

						<div class="profile-head bg-color dark-blue rounded-top">
							<div class="box-padding" style="text-align:center">
									<h3 class="normal"><i class="icon-th-thumb-empty"></i>ขนาดโครงการ</h3>
							</div>
						</div>
						<div class="bg-color white rounded-bottom">
								<ul class="menu unstyled">
									<li class="current"><p class="sales">
									  	<input type="radio"  class="optionsYear1" id="optionsYear1" name="rdoYear" value="TimeFrameBidingDate" style="margin-top:-2px" checked>
									  	 ทั้งหมดทุกโครงการ</p></li>
									<li><p class="sales">
									  	<input type="radio"  class="optionsYear2" id="optionsYear2" name="rdoYear" value="TimeFrameBidingDate" style="margin-top:-2px">
									  	 ระบุโครงการ <br/><input id="contract_start" class="span1 requireField nNumber" type="text" value="5" style="width:30px"> ถึง <input id="contract_end" class="span1 requireField nNumber" type="text" value="10" style="width:30px"> ล้านบาท</p></li>
								</ul>
						</div>
						<div style="text-align:center"><a class="btn btn-blue btn-large" id="btnReport" style="width:190px">Report</a></div>
						<!--
						//icon-info-circle icon-info-circled icon-warning icon-attention-circle icon-attention-3
						<div id="showError" class="alert alert-error hide" style="margin-top:10px;margin-bottom:-10px;">
							<button type="button" class="close" data-dismiss="alert">×</button>
							<strong>Error!</strong> กรุณาเลือก Sale ด้วย
						</div>
						-->
						<form name="frmreport"  action="ExportReport.php" >
							<input type="hidden" name="Progress2" value="100,90,80,70,60,50,40,30,20,10">
						</form>
						<div id="showError" class="alert alert-error hide" style="margin-top:10px;margin-bottom:-10px;">
							<button type="button" class="close" data-dismiss="alert">×</button>
								<i class="icon-attention-3 pull-left" style="font-size:20px;margin-top:-3px;"></i>
								<div class="media-body">
									<strong>กรุณาเลือก Sale ด้วย</strong>
								</div>
						</div>
						<div id="showWarning" class="alert alert-block hide" style="margin-top:10px;margin-bottom:-10px;">
							<button type="button" class="close" data-dismiss="alert">×</button>
							<i class="icon-info-circle pull-left" style="font-size:25px;margin-top:-7px;"></i>
							<div class="media-body">
								<strong>ไม่พบข้อมูล</strong>
							</div>
						</div>
					</div>				
					
					<div class="span5">
						<div class="widget widget-profile">
							<!--<div class="widget-header clearfix">
								<span class="pull-left"><i class="icon-user-male"></i> Progress</span>
								<div class="dropdown pull-right">
								</div>
							</div>-->
							<div class="profile-head bg-color dark-blue rounded-top">
								<div class="box-padding">
									<h3 class="normal"><i class="icon-progress-3"></i>Progress</h3>
									<span class=" pull-right" style="margin-right:-10px;margin-top:-30px;"><input type="checkbox" id="progressAll" title="Select all"></span>
								</div>
							</div>

							<div class="bg-color white rounded-bottom">
								<ul class="menu unstyled">
									<li class="current"><p>100% Sign date<span class=" pull-right"><input type="checkbox" name="chkprog" value="100" class="progress" checked></span></p></li>
									<li class="current"><p>90% <b>ประกาศผลแล้วรอเซ็นสัญญา</b><span class="pull-right"><input type="checkbox" name="chkprog" value="90" class="progress" checked></span></p></li>
									<li class="current"><p>80% <b>ยื่นซองแล้ว</b><span class="pull-right"><input type="checkbox" name="chkprog" value="80" class="progress" checked ></span></p></li>
									<li class="current"><p>70% Bidding date<span class="pull-right"><input type="checkbox" name="chkprog" value="70" class="progress" checked></span></p></li>
									<li class="current"><p>60% TOR Final<span class="pull-right"><input type="checkbox" name="chkprog" value="60" class="progress" checked></span></p></li>
									<li class="current"><p>50% <b>ได้งบประมาณ</b><span class="pull-right"><input type="checkbox" name="chkprog" value="50" class="progress" checked></span></p></li>
									<li class="current"><p>40% <b>ขั้นตอนการพิจารณาอนุมัติงบประมาณ</b><span class="pull-right"><input type="checkbox" name="chkprog" value="40" class="progress" checked></span></p></li>
									<li class="current"><p>30% <b>ร่างโครงการ (ของบประมาณ)</b><span class="pull-right"><input type="checkbox" name="chkprog" value="30" class="progress" checked></span></p></li>
									<li class="current"><p>20% Presentation/Demo<span class="pull-right"><input type="checkbox" name="chkprog" value="20" class="progress" checked></span></p></li>
									<li class="current"><p>10% Build solution<span class="pull-right"><input type="checkbox" name="chkprog" value="10" class="progress"  checked></span></p></li>
									<li><p>0% <b>แพ้</b><span class="pull-right"><input type="checkbox" name="chkprog" value="0" class="progress" ></span></p></li>
									<li><p>V <b>ยกเลิกโครงการ</b><span class="pull-right"><input type="checkbox" name="chkprog" value="v" class="progress" ></span></p></li>
									<li><p>P <b>Pending</b><span class="pull-right"><input type="checkbox" name="chkprog" value="p" class="progress" ></span></p></li>
									
								</ul>
							</div>

						</div>
					</div>
					<div class="span4 widget widget-option">

						<div class="profile-head bg-color dark-blue rounded-top">
							<div class="box-padding" style="text-align:center">
									<h3 class="normal"><i class="icon-users-1"></i>Sales</h3>
									<span class=" pull-left" style="margin-left:-10px;margin-top:-20px"><input type="checkbox" class="salesAll" title="Select all" checked></span>
							</div>
						</div>
						<div class="bg-color white rounded-bottom">
								<ul class="menu unstyled" id="listsaleName"></ul>
						</div>
					</div>
				</div>
				<div class="widget widget-chart bg-color dark-blue rounded" id="Potential_Detail" style="display:none">
					<div style="background-color: #2ABF9E;">
						<div id="charts-data" class="monthly clearfix">	
							<div class="box-padding" style="padding:20px;">
								<div class="clearfix" >
									<h2 class="charts-data-quarter pull-left" style="color: #FFFFFF;" id="PotentailShowname">Potential 100</h2>
									<h2 id="sumResult" class="pull-right" style="margin: 10px 0;color: #FFFFFF;" title="Income">0</h2>
								</div>
								<div class="clearfix" >
									<span id="numResult" class="label label-inverse pull-left" style="font-size:20px;padding:10px;margin-right:10px;" title="Number of results">0</span>
									<font class="pull-left" style="color: #FFFFFF;font-size:16px;"><b>รายการ</b></font>
									<div class="box-padding pull-right" style="padding:5px;margin-right:-5px;">

										<label style="background-color: #dcdcdc; display: inline-block; width: 150px; text-align: center; float: right; cursor: default;color: #777777;margin-right:10px;font-size: 18px;"><input type="radio" value="25" style="margin-top:-2px" class="chkPotential" name="rdoPO"><i class="icon-progress-0" ></i>Potential=25</label>	
										<label style="background-color: #F2DEDE; display: inline-block; width: 150px; text-align: center; float: right; cursor: default;color: #777777;margin-right:10px;font-size: 18px;"><input type="radio" value="50" style="margin-top:-2px" class="chkPotential" name="rdoPO"><i class="icon-progress-1" ></i>Potential=50</label>
										<label style="background-color: #FCF8E3; display: inline-block; width: 150px; text-align: center; float: right; cursor: default;color: #777777;margin-right:10px;font-size: 18px;"><input type="radio" value="75" style="margin-top:-2px" class="chkPotential" name="rdoPO"><i class="icon-progress-2"></i>Potential=75</label>
										<label style="background-color: #DFF0D8; display: inline-block; width: 150px; text-align: center; float: right; cursor: default;color: #777777;margin-right:10px;font-size: 18px;"><input type="radio" value="100" style="margin-top:-2px" class="chkPotential" name="rdoPO" checked><i class="icon-progress-3" ></i>Potential=100</label>
									</div>
								</div>
							</div>
							<div class="charts-data-table">
								<div class="box-padding" style="padding:0px;" id="focusShow">
									<table class="table table-striped showdetail" id="detail" >
										<thead>
											<tr role="row">
												<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;" width="50px"><input type='checkbox' class='chkPotential' style='margin-top:-2px' id="checkAll"/></th>
												<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;" width="50px"><i class="icon-progress-3"></i></th>
												<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;" width="80px">PE Contract</th>
												<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;" width="480px">Project Name</th>
												<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;" width="120px">SalesName</th>
												<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;" width="80px">BiddingDate</th>
												<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;" width="80px">SignDate</th>
												<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align:right" width="100px">Income</th>
												<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;" width="20px"><!--<i class="icon-pencil-1" title='Edit'></i>|--><i class='icon-newspaper' title='View'></i></th>
											</tr>
										</thead>
										<tbody ></tbody>
										<tfoot>
											<tr role="row">
												<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;width:100%" colspan="9" align="center"><div style="text-align:center;"><a class="btn btn-blue btn-large" id="btnSavePotential" style="width:100px">Save</a></div></th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
		<div id="footer" class="bg-color dark-blue">
			<div class="container">
				<div class="box-padding">
					Copyright &copy; 2013 Sales Forecast
				</div>
			</div>
		</div>
		<!-- Javascript -->
		<!-- JS:jquery-->
		<!--<script src="../../../../ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>-->
  		<script src="js/library/jquery/jquery.min.js"></script>
		<script src="js/library/jquery/jquery-ui.min.js"></script>

		<!-- JS:masonry-->
		<script src="js/library/masonry/jquery.masonry.min.js"></script>

		<!-- JS:flexslider-->
		<script src="js/library/flexslider/jquery.flexslider-min.js"></script>

		<!-- JS:tables-->
		<script src="js/library/dataTables/jquery.dataTables.min.js"></script>

		<!-- JS:calendar-->
		<script src="js/library/fullcalendar/fullcalendar.min.js"></script>
		<script src="js/library/fullcalendar/gcal.js"></script>

		<!-- JS:forms-->
		<script src="js/library/forms/jquery.hotkeys.js"></script>
		<script src="js/library/forms/bootstrap-wysiwyg.js"></script>
		<script src="js/library/forms/picker.js"></script>
		<script src="js/library/forms/picker.date.js"></script>
		<script src="js/library/forms/picker.time.js"></script>
		<script src="js/library/forms/jquery.bootstrap.wizard.min.js"></script>

		<!-- JS:charts-->
		<script src="js/library/charts/jquery.flot.js"></script>
		<script src="js/library/charts/jquery.flot.resize.js"></script>
		<script src="js/library/charts/jquery.flot.stack.js"></script>
		<script src="js/library/charts/jquery.flot.pie.min.js"></script>
		<script src="js/library/charts/jquery.sparkline.min.js"></script>
		<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="excanvas.min.js"></script><![endif]-->

		<!-- JS:syntaxHighlighter-->
		<script src="js/library/syntaxHighlighter/shCore.js"></script>
		<script src="js/library/syntaxHighlighter/shBrushCss.js"></script>
		<script src="js/library/syntaxHighlighter/shBrushXml.js"></script>
		<script src="js/library/syntaxHighlighter/shBrushJScript.js"></script>

		<!-- JS:bootstrap-->
		<script src="bootstrap/js/bootstrap.min.js"></script>
		<script src="js/library/jquery.number.js"></script>
		
		<!-- JS:custom js for this template -->
		<script src="js/styler/custom.js"></script>

							<script src="js/highcharts.js"></script>
							<script src="js/exporting.js"></script>
		<script src="js/log_forecast.js"></script>
		<script type="text/javascript">
			var saleid= '<?php echo $_COOKIE['Ses_ID']?>';
			var userid= '<?php echo $_COOKIE['Ses_ID']?>';
			var typeYear= 'signdate';
			var slcYear=(new Date).getFullYear();
			var slcOption='0';
			var slcOptionstart='0';
			var slcOptionend='0';
			var selectNo = '1';
			var PotentialIncome100=0;
			var PotentialIncome75=0;
			var PotentialIncome50=0;
			var PotentialIncome25=0;
			var PotentialSum100=0;
			var PotentialSum75=0;
			var PotentialSum50=0;
			var PotentialSum25=0;
			var PotentialSelect="PotentialTag100";
			var ckSale = saleid;
			var ckYear = slcYear;
	    	$(".nNumber").number(true, 0);
			Number.prototype.nformat = function() {
			    return this.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
			};
			var _loginID = "<?php echo $_COOKIE['Ses_ID']?>";
		    Log_Forecast("Report_All","Progress",_loginID,"Start","Load","-");
		    $(document).ready(function(){   
			    $.ajax({ 
			    	cache: false,
					type: "POST",
					dataType: "json",
		        	url: "AJAX/select_yearSign.php",
		        	success: function (json) {
		        		$.each(json, function() {
							var item = this;
		        			$("#slc_year_sign").append("<option value='" + item[0] + "'>" + item[0] + "</option>");
						});
						$("#slc_year_sign").val((new Date).getFullYear());
			        },
			        error: function(err)
			        {
			        	Log_Forecast("Report_All","Progress",_loginID,"select_yearSign","error",err);
			        }
			    });
				$('.optionsYear1').on('click',function(){
					$('.optionsYear1').closest('li').attr("class", "current");
					$('.optionsYear2').closest('li').attr("class", "");
					//$("#optionsYear2").disableSelection();
					//$("#slc_year").enableSelection();
				});
				$('.optionsYear2').on('click',function(){
					$('.optionsYear1').closest('li').attr("class", "");
					$('.optionsYear2').closest('li').attr("class", "current");
					//$("#optionsYear2").disableSelection();
					//$("#slc_year").enableSelection();
				});
				$("#progressAll").click(function () {
			          $('.progress').attr('checked', this.checked);
			          if(this.checked)
			          {			          	
			          	$('.progress').closest('li').attr("class", "current");
						document.frmreport.Progress2.value="100,90,80,70,60,50,40,30,20,10,0,v,P";
			          }
			          else
			          {			          	
			          	$('.progress').closest('li').removeAttr("class");
						document.frmreport.Progress2.value="";
						// $('#showWarning').removeClass("hide");
			          }
			    });
			    $("#checkAll").click(function () {
			        //$('#checkAll').attr('checked', false);

			        if(PotentialSelect=="PotentialTag100")
					{
						if(this.checked)
						{
							$("input.PotentialCheck100").each(function ()
							{
								$(this).attr('checked',true);
							});
						}
						else
						{
							$("input.PotentialCheck100").each(function ()
							{
								$(this).attr('checked',false);
							});
						}
						

					}
					else if(PotentialSelect=="PotentialTag75")
					{
						if(this.checked)
						{
							$("input.PotentialCheck75").each(function ()
							{
								$(this).attr('checked',true);
							});
						}
						else
						{
							$("input.PotentialCheck75").each(function ()
							{
								$(this).attr('checked',false);
							});
						}
						
					}
					else if(PotentialSelect=="PotentialTag50")
					{
						if(this.checked)
						{
							$("input.PotentialCheck50").each(function ()
							{
								$(this).attr('checked',true);
							});
						}
						else
						{
							$("input.PotentialCheck50").each(function ()
							{
								$(this).attr('checked',false);
							});
						}
						
					}
					else if(PotentialSelect=="PotentialTag25")
					{
						if(this.checked)
						{
							$("input.PotentialCheck25").each(function ()
							{
								$(this).attr('checked',true);
							});
						}
						else
						{
							$("input.PotentialCheck25").each(function ()
							{
								$(this).attr('checked',false);
							});
						}
					}
			    });
			    $(".progress").click(function(){
			 
			        if($(".progress").length == $(".progress:checked").length) {
			            $("#progressAll").attr("checked", "checked");
			        } else {
			            $("#progressAll").removeAttr("checked");
			        }
			        if(this.checked)
			        {			          	
			          	$(this).closest('li').attr("class", "current");
			        }
			        else
			        {			          	
			         	$(this).closest('li').removeAttr("class");
			        }
					getProgress();					
			    });
			    function getProgress(){
						var chkidlist = '';
						$("input[name='chkprog']").each(function () {
								if ( $(this).prop('checked')) {
									if(chkidlist=='')
									{
										chkidlist= $(this).val();
									}
									else
									{
										chkidlist = chkidlist+","+$(this).val();
									}
								}
						});
						document.frmreport.Progress2.value=chkidlist;
				}
				$('#btnSavePotential').on('click',function(){
					var checked = [];
					var checkedNo = [];
					if(PotentialSelect=="PotentialTag100")
					{
						$("input.PotentialCheck100").each(function ()
						{
							if($(this).is(':checked'))
							{
						    	checked.push(parseInt($(this).val()));
						    }
						    else
						    {
						    	checkedNo.push(parseInt($(this).val()));
						    }
						});

					}
					else if(PotentialSelect=="PotentialTag75")
					{
						$("input.PotentialCheck75").each(function ()
						{
							if($(this).is(':checked'))
							{
						    	checked.push(parseInt($(this).val()));
						    }
						    else
						    {
						    	checkedNo.push(parseInt($(this).val()));
						    }
						});
						
					}
					else if(PotentialSelect=="PotentialTag50")
					{
						$("input.PotentialCheck50").each(function ()
						{
							if($(this).is(':checked'))
							{
						    	checked.push(parseInt($(this).val()));
						    }
						    else
						    {
						    	checkedNo.push(parseInt($(this).val()));
						    }
						});
						
					}
					else if(PotentialSelect=="PotentialTag25")
					{
						$("input.PotentialCheck25").each(function ()
						{
							if($(this).is(':checked'))
							{
						    	checked.push(parseInt($(this).val()));
						    }
						    else
						    {
						    	checkedNo.push(parseInt($(this).val()));
						    }
						});
					}
					console.log("checked:"+checked);
					console.log("checkedNo:"+checkedNo);
					$.ajax({ 
				    	cache: false,
						type: "POST",
						dataType: "json",
						data:{
							checked:checked,
							checkedNo:checkedNo
						},
			        	url: "AJAX/SV_report_yearly_save.php",
			        	success: function (json) {
			        		console.log(json);
			        		alert("บันทึกข้อมูลเรียบร้อย");
			        		$('html,body').animate({
						        scrollTop: $("#Potential_Detail").offset().top
						    }, 'slow');
				        },
				        error: function(err)
				        {
				        	//Log_Forecast("Report_All","Progress",_loginID,"select_yearSign","error",err);
				        }
				    });
				});
				$('.chkPotential').on('click',function(){
					var Potential=$(this).val();
					var rows = $('table.showdetail>tbody tr');
					if(Potential=="100")
					{
						$("#PotentailShowname").text("Potential 100");
						$("#sumResult").text(PotentialIncome100.nformat() + ' บาท');
						var PotentialTag100 = rows.filter('.PotentialTag100').show();
    					rows.not( PotentialTag100 ).hide();
						PotentialSelect="PotentialTag100";
						document.getElementById('numResult').innerHTML=PotentialSum100;
						$('#checkAll').attr('checked', false);
					}
					else if(Potential=="75")
					{
						$("#PotentailShowname").text("Potential 75");
						$("#sumResult").text(PotentialIncome75.nformat() + ' บาท');
						var PotentialTag75 = rows.filter('.PotentialTag75').show();
    					rows.not( PotentialTag75 ).hide();
						PotentialSelect="PotentialTag75";
						document.getElementById('numResult').innerHTML=PotentialSum75;
						$('#checkAll').attr('checked', false);
					}
					else if(Potential=="50")
					{
						$("#PotentailShowname").text("Potential 50");
						$("#sumResult").text(PotentialIncome50.nformat() + ' บาท');
						var PotentialTag50 = rows.filter('.PotentialTag50').show();
    					rows.not( PotentialTag50 ).hide();
						PotentialSelect="PotentialTag50";
						document.getElementById('numResult').innerHTML=PotentialSum50;
						$('#checkAll').attr('checked', false);
					}
					else if(Potential=="25")
					{
						$("#PotentailShowname").text("Potential 25");
						$("#sumResult").text(PotentialIncome25.nformat() + ' บาท');
						var PotentialTag25 = rows.filter('.PotentialTag25').show();
    					rows.not( PotentialTag25 ).hide();
						PotentialSelect="PotentialTag25";
						document.getElementById('numResult').innerHTML=PotentialSum25;
						$('#checkAll').attr('checked', false);
					}
					//$("#optionsYear2").disableSelection();
					//$("#slc_year").enableSelection();
				});
				
				if (_loginID == "0095") {
						depMng = "Sales TM";
					} else if (_loginID == "4371") {
						depMng = "Sales 1";
					} else if (_loginID == "0416") {
						depMng = "Sales 2";
					} else if (_loginID == "3581") {
						depMng = "Sales 3";
					} else if (_loginID == "3882") {
						depMng = "Sales 4";
					} else if (_loginID == "4583") {
						depMng = "Sales CSD";
					} else if (_loginID == "1190") {
						depMng = "Sales GEO";
					} else if (_loginID == "1994") {
						depMng = "Sales CLMV";
					} else if (_loginID == "3784") {
						depMng = "Sales CSD";
					}else {
						depMng = "";
					}
					$.ajax({ 
						type:"POST",
						dataType: "json",
						url: "Ajax/SV_Select_Sale.php",
						data: {
							saledept: depMng,
							loginid : _loginID
						},
						success: function (json) {
							if(json[0].length>1)
								{
									checked = "checked";
									classli = "current";
									for(var ii = 0;ii<json[0].length;ii++){
										$("#listsaleName").append('<li class="' + classli + '"><p class="sales"><input type="checkbox" name="selectSales_allsale" style="margin-top:-2px" class="selectSales_all'+ii+'"  ' + checked + ' onclick="SelectSale('+ii+')" />All '+json[0][ii]+'<span class=" pull-right" style="margin-top:-8px"></span></p></li>');
										// $("#listsaleName").append('<label class="checkbox"><input name ="selectSales_allsale" class="selectSales_all'+ii+'" type="checkbox" onclick="SelectSale('+ii+')"/><h4>All '+json[0][ii]+'</h4></label>');
										$(json).each(function() {
				        		var empno=this[0];
				        		var name=this[1];
								
								
								if(this[2] == json[0][ii]){
				        		$("#listsaleName").append('<li class="' + classli + '"><p class="sales"><input type="checkbox" name="chkname'+ii+'" style="margin-top:-2px" class="chksales" value="' + empno + '" ' + checked + ' /> ' + name + '<span class=" pull-right" style="margin-top:-8px"></span></p></li>');
								}
							});
										}
								}else{
							$(json).each(function() {
				        		var empno=this[0];
				        		var name=this[1];
								
								checked = "checked";
								classli = "current";
								if(this[2] == json[0][0]){
				        		$("#listsaleName").append('<li class="' + classli + '"><p class="sales"><input type="checkbox" name="chkname" style="margin-top:-2px" class="chksales" value="' + empno + '" ' + checked + ' /> ' + name + '<span class=" pull-right" style="margin-top:-8px"></span></p></li>');
								}
							});
								}
							$('.salesAll').on('click',function(){
								if ($(".salesAll").is(':checked')) {
									$(".chksales").prop("checked", true);
									$('[name="selectSales_allsale"]').prop("checked", true);
									$('[name="selectSales_allsale"]').closest('li').attr("class", "current");
									$('.chksales').closest('li').attr("class", "current");
								} else {
									$(".chksales").prop("checked", false);
									$('[name="selectSales_allsale"]').prop("checked", false);
									$('[name="selectSales_allsale"]').closest('li').attr("class", "");
									$('.chksales').closest('li').attr("class", "");
								}
					    	});
							$('.chksales').on('click',function(){
								if ( $(this).prop('checked')) {
									$(this).closest('li').attr("class", "current");
							    } else {
									$(this).closest('li').attr("class", "");
							    }
								if( $(".chksales:checked").length == $(".chksales").length){
									$(".salesAll").prop("checked", true);
								}else{
									$(".salesAll").prop("checked", false);
								}
								for(var index = 0 ; index < json[0].length;index++){
									if( $('[name="chkname'+index+'"]:checked').length == $('[name="chkname'+index+'"]').length){
									$(".selectSales_all"+index).prop("checked", true);
									$('.selectSales_all'+index).closest('li').attr("class", "current");
								}else{
									$(".selectSales_all"+index).prop("checked", false);
									$('.selectSales_all'+index).closest('li').attr("class", "");
								}
								}
							});							
				        },
						error: function() {
							alert("เกิดปัญหาในการโหลดข้อมูลกรุณารีเฟรชหน้าเว็บอีกครั้ง");
							$("#loading").hide();
						}
				    });
			//showReport('1');
			$("#loading").hide();
			//$('#showGraph').hide();
			function showReport(showNo) {
				$("#loading").show();
				/*console.log('progress: ',slcYear+' '+saleid+' '+typeYear);
				$.ajax({ 
					dataType: "json",
					url: "AJAX/SV_report_yearly.php",
					data: {
						year: slcYear,
						saleid: saleid,
						type: typeYear,
						option: slcOption,
						option_start: slcOptionstart,
						option_end: slcOptionend
					},
					success: function (json) {
					if(json.length == 0){
						$('#showWarning').removeClass("hide");
						//$('#showGraph').hide();
						$(".charts-data-quarter").text('');
						$("#sumResult").text('');
						$("#numResult").text('0');
						$('#detail > tbody:last').empty();
						var noresult = "<tr><td colspan='7' style='text-align:center;'><p class='lead14'>ไม่พบข้อมูล</p></td></tr>";
						$('#detail').append(noresult);
						$("#loading").hide();
					}else{
						document.getElementById('focusReport').scrollIntoView();
						//$('#showGraph').show();
						var dataBar ='';
						var dataPie ='';
						var P100=0;
						var P90=0;
						var P80=0;
						var P70=0;
						var P60=0;
						var P50=0;
						var P40=0;
						var P30=0;
						var P20=0;
						var P10=0;
						var P0=0;
						var Pv=0;
						var Pp=0;
						$.each(json, function() {
							//var item = this;
							//console.log(this[1]);
							P100=parseFloat(this[1]);
							P90=parseFloat(this[2]);
							P80=parseFloat(this[3]);
							P70=parseFloat(this[4]);
							P60=parseFloat(this[5]);
							P50=parseFloat(this[6]);
							P40=parseFloat(this[7]);
							P30=parseFloat(this[8]);
							P20=parseFloat(this[9]);
							P10=parseFloat(this[10]);
							P0=parseFloat(this[11]);
							Pv=parseFloat(this[12]);
							Pp=parseFloat(this[13]);
						});
						if(showNo == '1'){
							$('#detail > tbody:last').empty();
							$(".charts-data-quarter").text('');
							$("#sumResult").text('');
							$("#numResult").text('0');
							dataBar = [P100,P90,P80,P70,P60,P50,P40,P30,P20,P10,P0,Pp,Pv];
							dataPie = [
							  ['100', P100],
							  ['90', P90],
							  ['80', P80],
							  ['70', P70],
							  ['60', P60],
							  ['50', P50],
							  ['40', P40],
							  ['30', P30],
							  ['20', P20],
							  ['10', P10],
							  ['0', P0],
							  ['P', Pp],
							  ['v', Pv]
							];
						}else{
							if(showNo == '100'){
								var p100Pie = {name: '100',y: P100,sliced: true,selected: true};
								var p100Bar = {y:P100,selected:true};
							}else{
								var p100Pie = ['100', P100];
								var p100Bar = P100;
							}
							if(showNo == '90'){
								var p90Pie = {name: '90',y: P90,sliced: true,selected: true};
								var p90Bar = {y:P90,selected:true};
							}else{
								var p90Pie = ['90', P90];
								var p90Bar = P90;
							}
							if(showNo == '80'){
								var p80Pie = {name: '80',y: P80,sliced: true,selected: true};
								var p80Bar = {y:P80,selected:true};
							}else{
								var p80Pie = ['80', P80];
								var p80Bar = P80;
							}
							if(showNo == '70'){
								var p70Pie = {name: '70',y: P70,sliced: true,selected: true};
								var p70Bar = {y:P70,selected:true};
							}else{
								var p70Pie = ['70', P70];
								var p70Bar = P70;
							}
							if(showNo == '60'){
								var p60Pie = {name: '60',y: P60,sliced: true,selected: true};
								var p60Bar = {y:P60,selected:true};
							}else{
								var p60Pie = ['60', P60];
								var p60Bar = P60;
							}
							if(showNo == '50'){
								var p50Pie = {name: '50',y: P50,sliced: true,selected: true};
								var p50Bar = {y:P50,selected:true};
							}else{
								var p50Pie = ['50', P50];
								var p50Bar = P50;
							}
							if(showNo == '40'){
								var p40Pie = {name: '40',y: P40,sliced: true,selected: true};
								var p40Bar = {y:P40,selected:true};
							}else{
								var p40Pie = ['40', P40];
								var p40Bar = P40;
							}
							if(showNo == '30'){
								var p30Pie = {name: '30',y: P30,sliced: true,selected: true};
								var p30Bar = {y:P30,selected:true};
							}else{
								var p30Pie = ['30', P30];
								var p30Bar = P30;
							}
							if(showNo == '20'){
								var p20Pie = {name: '20',y: P20,sliced: true,selected: true};
								var p20Bar = {y:P20,selected:true};
							}else{
								var p20Pie = ['20', P20];
								var p20Bar = P20;
							}
							if(showNo == '10'){
								var p10Pie = {name: '10',y: P10,sliced: true,selected: true};
								var p10Bar = {y:P10,selected:true};
							}else{
								var p10Pie = ['10', P10];
								var p10Bar = P10;
							}
							if(showNo == '0'){
								var p0Pie = {name: '0',y: P0,sliced: true,selected: true};
								var p0Bar = {y:P0,selected:true};
							}else{
								var p0Pie = ['0', P0];
								var p0Bar = P0;
							}
							if(showNo == 'P'){
								var ppPie = {name: 'P',y: Pp,sliced: true,selected: true};
								var ppBar = {y:Pp,selected:true};
							}else{
								var ppPie = ['P', Pp];
								var ppBar = Pp;
							}
							if(showNo == 'v'){
								var pvPie = {name: 'v',y: Pv,sliced: true,selected: true};
								var pvBar = {y:Pv,selected:true};
							}else{
								var pvPie = ['v', Pv];
								var pvBar = Pv;
							}
							
							dataBar = [p100Bar,p90Bar,p80Bar,p70Bar,p60Bar,p50Bar,p40Bar,p30Bar,p20Bar,p10Bar,p0Bar,ppBar,pvBar];
							dataPie = [p100Pie,p90Pie,p80Pie,p70Pie,p60Pie,p50Pie,p40Pie,p30Pie,p20Pie,p10Pie,p0Pie,ppPie,pvPie];
						}
						RanderBarChart('containerBar', dataBar);
						RenderPieChart('containerPie', dataPie); 
					}
					}
				});*/

				
			}
			
			function RenderPieChart(elementId, dataList) {
                new Highcharts.Chart({
                    chart: {
                        renderTo: elementId,
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false
                    }, 
                    credits:{
                    	enabled: false
                    },
                    exporting: {
			            enabled: false
			        },
                    title: {
                        text: "Progress "+slcYear
                    },
                    tooltip: {
                        formatter: function () {
                            return '<b>' + this.point.name + '</b> : ' + this.percentage.nformat() + ' %<br>click to detail' ;
                        }
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                color: '#000000',
                                connectorColor: '#000000',
                                formatter: function () {
                                    return '<b>' + this.point.name + '</b> : ' + this.point.y.nformat() ;
                                }
                            }
                        },
                        series: {                        	
							 events: {
			                    click: function(event) {
									document.getElementById('focusShow').scrollIntoView();
									selectNo = event.point.name;
									showdetail(event.point.name,event.point.y);
			                    }
			                }
                        }
                    },
                    series: [{
                        type: 'pie',
                        name: 'Progress',
                        data: dataList
                    }],
                    
                    colors: [
					   '#749DD1', 
					   '#D67B76', 
					   '#C5DC96', 
					   '#AE9BC8', 
					   '#66BCD5', 
					   '#F19F55', 
					   '#84B5EF', 
					   '#8AB9EF', 
					   '#F89995', 
					   '#D8F4A0', 
					   '#C5B1E6', 
					   '#94EAFB', 
					   '#FEC283', 
					   '#CADDFB', 
					   '#FECCCD', 
					   '#EAFBCE'
					]
                });
				$("#loading").hide();
            }
			function RanderBarChart(elementId,dataList){
				$('#'+elementId).highcharts({

			        chart: {
			            type: 'column'
			        },			         
                    credits:{
                    	enabled: false
                    },
                    exporting: {
			            enabled: false
			        },
                    title: {
                        text: "Progress "+slcYear
                    },
			        xAxis: {
			            categories: ['100', '90', '80', '70', '60', '50', '40' , '30' , '20' , '10', '0','P', 'v']  
			        },
					tooltip: {
                        formatter: function () {
                            return '<b>Income</b> : ' + this.y.nformat() + '<br>click to detail' ;
                        }
                    },
			        plotOptions: {
			            series: {
			                allowPointSelect: true,
			                cursor: 'pointer',
			                point: {
			                    events: {
			                        click: function() {
										document.getElementById('focusShow').scrollIntoView();
										selectNo = this.category;
										showdetail(this.category,this.y);				
			                        }
			                    }
			                }
			            }
			        },

			        series: [{
			        	data: dataList      
			            ,name:"Income "       
			        }]
			    });
				$("#loading").hide();
			}
								
			function showdetail(){
				$("#loading").show();
				//$(".charts-data-quarter").text("Progress " + progressN);
				//$("#sumResult").text(progressVal.nformat()+ ' บาท');
				console.log('progress_detail: ',slcYear+' '+saleid+' '+typeYear);
				$.ajax({ 
					type:"POST",
					dataType: "json",
					url: "AJAX/SV_report_yearly.php",
					data: {
						year: slcYear,
						saleid: saleid,
						type: typeYear,
						option: slcOption,
						option_start: slcOptionstart,
						option_end: slcOptionend,
						ProgressObj:document.frmreport.Progress2.value
					},
					success: getDetail
				});			
			}

			function getDetail(json) {
					$("#Potential_Detail").css("display","block");
            	    //console.log("showTable:",json);	
					document.getElementById('numResult').innerHTML=json.length;
					$('#detail > tbody:last').empty();
					if(json.length== 0){
						var noresult = "<tr><td colspan='7' style='text-align:center;'><p class='lead14'>ไม่พบข้อมูล</p></td></tr>";
						$('#detail').append(noresult);
					}else{
						PotentialIncome25=0;
						PotentialIncome50=0;
						PotentialIncome75=0;
						PotentialIncome100=0;
						PotentialSum100=0;
						PotentialSum75=0;
						PotentialSum50=0;
						PotentialSum25=0;
            	    	$.each(json, function() {
	            	    	var newrow="";
	            	    	var Potential=this[0];
	            	    	var PEContractNo=this[1];
	            	    	var Project=this[2];
	            	    	var SaleRepresentative=this[3];
	            	    	var TimeFrameBidingDate=this[4];
	            	    	var TimeFrameContractSigndate=this[5];
	            	    	var TargetIncome=parseFloat(this[6]);
	            	    	var ID=this[8];
	            	    	var check_report=this[9];
	            	    	var chkPo="";
	            	    	if(Potential==100)
	            	    	{
	            	    		newrow=$('<tr >').attr('class','success PotentialTag100');
	            	    		PotentialIcon="<i class='icon-progress-3' ></i>";
								PotentialIncome100=PotentialIncome100+TargetIncome;
								chkPo="PotentialCheck100";
								PotentialSum100++;
	            	    	}
	            	    	else if(Potential==75)
	            	    	{
	            	    		newrow=$('<tr >').attr('class','warning PotentialTag75');
	            	    		PotentialIcon="<i class='icon-progress-2'></i>";
								PotentialIncome75=PotentialIncome75+TargetIncome;
								chkPo="PotentialCheck75";
								PotentialSum75++;
	            	    	}
	            	    	else if(Potential==50)
	            	    	{
	            	    		newrow=$('<tr >').attr('class','error PotentialTag50');
	            	    		PotentialIcon="<i class='icon-progress-1'></i>";
								PotentialIncome50=PotentialIncome50+TargetIncome;
								chkPo="PotentialCheck50";
								PotentialSum50++;
	            	    	}
	            	    	else if(Potential==25)
	            	    	{
	            	    		newrow=$('<tr >').attr('class','info PotentialTag25');
	            	    		PotentialIcon="<i class='icon-progress-0'></i>";
								PotentialIncome25=PotentialIncome25+TargetIncome;
								chkPo="PotentialCheck25";
								PotentialSum25++;
	            	    	}
	            	    	var cols="";
	            	    	var report_check="";
	            	    	if(check_report=='1')
	            	    	{
	            	    		report_check="checked";
	            	    	}
	            	    	cols+="<td><p class='lead14'><input type='checkbox' class='"+chkPo+"' style='margin-top:-2px' name='chkpo' value='" + ID + "' "+report_check+"/></p></td>";
	            	    	cols+="<td><p class='lead14'>"+PotentialIcon+"</p></td>";
	            	    	cols+="<td><p class='lead14'>"+PEContractNo+"</p></td>";
	            	    	cols+="<td><p class='lead14'>"+Project+"</p></td>";
	            	    	cols+="<td><p class='lead14'>"+SaleRepresentative+"</p></td>";
	            	    	cols+="<td><p class='lead14'>"+TimeFrameBidingDate+"</p></td>";
	            	    	cols+="<td><p class='lead14'>"+TimeFrameContractSigndate+"</p></td>";
	            	    	cols+="<td style='text-align:right'><p class='lead16'>"+TargetIncome.nformat()+"</p></td>";
	            	    	/*if(json.items[i].SaleID==userid)
	            	    	{
	            	    		cols+="<td style='text-align:center'><a href='javascript:edit("+json.items[i].ID+");' title='Edit'><i class='i_btn icon-pencil-1 iconEdit' style='color:#2ABF9E;'></i></a></td>";
	            	    	}
	            	    	else
	            	    	{*/
	            	    		cols+="<td style='text-align:center'><a href='javascript:view("+ID+");' title='View'><i class='i_btn icon-newspaper iconView' style='color:#2ABF9E;'></i></a></td>";
	            	    	//}
							newrow.append(cols);

	            	    	$('#detail').append(newrow);
	            	    	$('#charts-data')[0].scrollIntoView(true);
							$("#sumResult").text(PotentialIncome100.nformat() + ' บาท');
							var rows = $('table.showdetail>tbody tr');
	            	    	var black = rows.filter('.PotentialTag100').show();
    						rows.not( black ).hide();
							document.getElementById('numResult').innerHTML=PotentialSum100;
	            	    });
					}
				$("#loading").hide();
           	}
			
			$('#btnSwitch').on('click',function(){
				showReport(selectNo);
				if($('#containerBar').is(':hidden')) {
					$("#containerBar").show();
					$("#containerPie").hide();
				}else{
					$("#containerBar").hide();
					$("#containerPie").show();
				}
			});
			$('#btnReport').on('click',function(){
				$('#showError').addClass("hide");
				$('#showWarning').addClass("hide");
					
						checked = false;
						if ($(".chksales:checked").length == 0) {
							checked = false;
						}
						else
						{
							checked = true;
						}
						var chkid = '';
						$(".chksales").each(function () {
							if ( $(this).prop('checked')) {
								if(chkid=='')
								{
									chkid= $(this).val();
								}
								else
								{
									chkid = chkid+","+$(this).val();
								}
							}
						});

			
						slcYear=$("#slc_year_sign").val();

						saleid=chkid;
						if ( $('.optionsYear1').is(':checked')) {
							slcOption="0";
						}else{
							slcOption="1";
							slcOptionstart=$("#contract_start").val();
							slcOptionend=$("#contract_end").val();
						}
						//console.log("slcOption:"+$('#optionsYear1').is(':checked'));

						//showReport('1');
						if(document.frmreport.Progress2.value!="")
						{
							if($("#contract_start").val()!="")
							{
								if($("#contract_end").val()!="")
								{
									if (checked) {
										$("#PotentailShowname").text("Potential 100");
										$("input[name='rdoPO'][value='100']").prop('checked', true);
										showdetail();
									} else {
										alert("กรุณาเลือก Sales อย่างน้อย 1 รายการ");
									}
								}
								else
								{
									alert("กรุณาระบุมูลค่าโครงการ สิ้นสุด ");
								}
							}
							else
							{
								alert("กรุณาระบุมูลค่าโครงการ เริ่มต้น ");
							}
						}
						else
						{
							alert("กรุณาเลือก Progress อย่างน้อย 1 รายการ");
						}
							

					
				});
			});
			function edit(id){
				window.location="newproject.php?id="+id+"&page=report_all";
			}
			function view(id){
				//window.location="previewforecast.php?id="+id+"&page=report_all";			
				window.open("previewforecastReport.php?id="+id);
			}
			function SelectSale(i) {
			if ($(".selectSales_all"+i).is(':checked')) {
									$('[name="chkname'+i+'"]').prop("checked", true);
									$('[name="chkname'+i+'"]').closest('li').attr("class", "current");
									$(".selectSales_all"+i).closest('li').attr("class", "current");
								} else {
									$('[name="chkname'+i+'"]').prop("checked", false);
									$('[name="chkname'+i+'"]').closest('li').attr("class", "");
									$(".selectSales_all"+i).closest('li').attr("class", "");
								}
								if( $(".chksales:checked").length == $(".chksales").length){
									$(".salesAll").prop("checked", true);
								}else{
									$(".salesAll").prop("checked", false);
								}
}
		</script>
  	</body>

<!-- Mirrored from bootstrapstyler.com/preview/_/flatdashboard/ by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 18 Oct 2013 07:00:04 GMT -->
</html>