<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="Izuddin Helmi">
	<title>SFS:::Home</title>
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css">
	<link href="js/library/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
	<link href="css/styler/style.css" rel="stylesheet" type="text/css">
	<link href="css/main.css" rel="stylesheet" type="text/css">
	<link href="js/library/syntaxHighlighter/shCore.css" rel="stylesheet" type="text/css"/>
	<link href="js/library/syntaxHighlighter/shThemeDefault.css" rel="stylesheet" type="text/css"/>
	<link href="css/styler/fontello.css" rel="stylesheet" type="text/css">
    <link href="css/styler/animation.css" rel="stylesheet" type="text/css">
    <link href="css/project.css" rel="stylesheet" type="text/css">
</head>
<?php  $checkmenu = '1'; ?>
<body cz-shortcut-listen="true" style="margin-top:0px;">
<div id="loading" style="height: 100%; width: 100%; position: fixed; left: 0; top: 0; z-index: 1051 !important; background-color: black; filter: alpha(opacity=9); -khtml-opacity: 0.9; -moz-opacity: 0.9; opacity: 0.9;"><img src="img/loading.gif" style="width: 150px; height: 150px; position: fixed; top: 50%; left: 50%; margin: -75px 0 0 -75px;"></div>
<section class="page-header bg-color white">
	<div class="container">
		<div class="row">
			<div class="span12">
				<div class="section-header clearfix">
					<div class="pull-left">
						<h2><i class="icon-warehouse"></i>Home</h2>
					</div>
					<div id="shortcut" class="nav-collapse pull-right">
						<ul class="shortcuts unstyled">
							<?php include("menu.php");?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="dashboard content">
	<div class="container">
			<div class="row">
				<div class="span3">
					<div class="widget widget-year">
						<div class="widget-header clearfix">
							<span class="pull-left" style="margin-top: 10px;"><i class="icon-calendar"></i> Year</span>
							<div class="dropdown pull-right">
								<select id="slc_year" class="span1"></select>
							</div>
						</div>
						<div class="bg-color turqoise rounded-top rounded-bottom">
							<div class="box-padding aligncenter">
								<h1 id="selected_year" class="no-margin normal"></h1>
							</div>
						</div>
					</div>
					<div class="widget widget-sales">
						<div class="widget-header clearfix">
							<span class="pull-left"><i class="icon-users-1"></i> Sales</span>
							<div class="dropdown pull-right">
								<h3 style="margin:-3px 0px 0px 0px;"><i id="btn_selectSales_modal" title="Select Sales" class="i_btn icon-list-add"></i></h3>
							</div>
						</div>
						<div class="bg-color turqoise  rounded-top rounded-bottom">
							<div class="accordion" id="accordionsales">
				                <div class="">
				                  <div class="accordion-body collapse">
				                    <div class="accordion-inner">
				                    	<ul id="selected_salesName" class="unstyled"></ul>
				                    </div>
				                  </div>
				                </div>
				            </div>
						</div>						
					</div>
					<div class="widget widget-sort">
						<div class="widget-header clearfix">
							<span class="pull-left"><i class="icon-list-numbered"></i> Sort By</span>
							<div class="dropdown pull-right">
								<h3 style="margin:-3px 0px 0px 0px;"><i id="btn_selectSort_modal" title="Select Sort" class="i_btn icon-list-add"></i></h3>
							</div>
						</div>
						<div class="bg-color turqoise  rounded-top rounded-bottom">
							<div class="accordion" id="accordionsales">
				                <div class="">
				                  <div class="accordion-body collapse">
				                    <div class="accordion-inner">
				                    	<ul id="selected_sortBy" class="unstyled"></ul>
				                    </div>
				                  </div>
				                </div>
				            </div>
						</div>						
					</div>
					<!--Modal Select Sales-->
					<div class="modal hide fade" id="selectSales_modal">
						<div class="modal-header bg-color light-green aligncenter">
						    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						    <h1>Sales</h1>
						    </div>
						<div id="selectSales_ul_sales" class="modal-body" style="max-height:300px"></div>
						<div class="modal-footer alignright">
						    <a id="selectSales_btn_ok" href="#" class="btn btn-blue">OK</a>
						    <a href="#" class="btn btn-red" data-dismiss="modal">CLOSE</a>
						</div>
					</div>
					<!--Modal Select Sort-->
					<div class="modal hide fade" id="selectSort_modal">
						<div class="modal-header bg-color light-green aligncenter">
						    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						    <h1>Sort By</h1>
						    </div>
						<div class="modal-body">
							<table>
								<tbody>
									<tr>
										<td style="width: 150px;">
											<label class="checkbox"><input class="selectSort_inp_by" type="checkbox" value="Project" checked/> <h4>Project Name</h4></label>
										</td>
										<td style="width: 150px;">
											<label class="radio"><input name="inp_sortProject" type="radio" value="ASC" checked/> <h4>A > Z</h4></label>
										</td>
										<td style="width: 150px;">
											<label class="radio"><input name="inp_sortProject" type="radio" value="DESC"/> <h4>Z > A</h4></label>
										</td>
									</tr>
									<tr>
										<td>
											<label class="checkbox"><input class="selectSort_inp_by" type="checkbox" value="TimeFrameBidingDate"/> <h4>Bidding Date</h4></label>
										</td>
										<td>
											<label class="radio"><input name="inp_sortBidding" type="radio" value="ASC" checked/> <h4>Old > New</h4></label>
										</td>
										<td>
											<label class="radio"><input name="inp_sortBidding" type="radio" value="DESC"/> <h4>New > Old</h4></label>
										</td>
									</tr>
									<tr>
										<td>
											<label class="checkbox"><input class="selectSort_inp_by" type="checkbox" value="TimeFrameContractSigndate" checked/> <h4>Sign Date</h4></label>
										</td>
										<td>
											<label class="radio"><input name="inp_sortSign" type="radio" value="ASC" checked/> <h4>Old > New</h4></label>
										</td>
										<td>
											<label class="radio"><input name="inp_sortSign" type="radio" value="DESC"/> <h4>New > Old</h4></label>
										</td>
									</tr>
									<tr>
										<td>
											<label class="checkbox"><input class="selectSort_inp_by" type="checkbox" value="TargetIncome"/> <h4>Income</h4></label>
										</td>
										<td>
											<label class="radio"><input name="inp_sortIncome" type="radio" value="ASC" checked/> <h4>0 > 9</h4></label>
										</td>
										<td>
											<label class="radio"><input name="inp_sortIncome" type="radio" value="DESC"/> <h4>9 > 0</h4></label>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="modal-footer alignright">
						    <a id="selectSort_btn_ok" href="#" class="btn btn-blue">OK</a>
						    <a href="#" class="btn btn-red" data-dismiss="modal">CLOSE</a>
						</div>
					</div>
				</div>
				<div class="span9 widget">					
					<ul class="nav nav-pills tabbed">
					    <li><a id="btn_NEdu" href="#Non_Education">Non-Education</a></li>
					    <li class="active"><a id="btn_Edu" href="#Education">Education</a></li>
					</ul>
					<div class="tab-content">
					    <div class="tab-pane fade active in" id="Non_Education">
					    	<div class="widget-header clearfix">
								<span class="pull-left"><i class="icon-progress-3"></i> Progress</span>
								<label style="background-color: #DFF0D8; display: inline-block; width: 150px; text-align: center; float: right; cursor: default;">Potential>50</label>
								<label style="background-color: #FCF8E3; display: inline-block; width: 150px; text-align: center; float: right; cursor: default;">Potential=50</label>
								<label style="background-color: #F2DEDE; display: inline-block; width: 150px; text-align: center; float: right; cursor: default;">Potential&lt;50</label>
							</div>
					        <div class="bg-color white rounded-top rounded-bottom">
								<div class="accordion" id="accordion3">
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapse100')" style="cursor:hand" class="accordion-toggle collapsed clearfix" data-toggle="collapse" data-parent="#accordion3" >
					                      <h4 class="pull-left" style="margin-bottom:0px">100% Sign date</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px;color:#008A17"></p>
					                    </a>
					                  </div>
					                  <div id="collapse100" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapse90')" style="cursor:hand" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion3" >
					                      <h4 class="pull-left" style="margin-bottom:0px">90% ประกาศผลแล้วรอเซ็นสัญญา</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px"></p>
					                    </a>
					                  </div>
					                  <div id="collapse90" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapse80')" style="cursor:hand" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion3" >
					                      <h4 class="pull-left" style="margin-bottom:0px">80% ยื่นซองแล้ว</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px"></p>
					                    </a>
					                  </div>
					                  <div id="collapse80" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapse70')" style="cursor:hand" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion3" >
					                      <h4 class="pull-left" style="margin-bottom:0px">70% Bidding date</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px"></p>
					                    </a>
					                  </div>
					                  <div id="collapse70" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapse60')" style="cursor:hand" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion3" >
					                      <h4 class="pull-left" style="margin-bottom:0px">60% TOR Final</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px"></p>
					                    </a>
					                  </div>
					                  <div id="collapse60" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapse50')" style="cursor:hand" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion3" >
					                      <h4 class="pull-left" style="margin-bottom:0px">50% ได้งบประมาณ</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px"></p>
					                    </a>
					                  </div>
					                  <div id="collapse50" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapse40')" style="cursor:hand" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion3" >
					                      <h4 class="pull-left" style="margin-bottom:0px">40% ขั้นตอนการพิจารณาอนุมัติงบประมาณ</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px"></p>
					                    </a>
					                  </div>
					                  <div id="collapse40" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapse30')" style="cursor:hand" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion3" >
					                      <h4 class="pull-left" style="margin-bottom:0px">30% ร่างโครงการ (ของบประมาณ)</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px"></p>
					                    </a>
					                  </div>
					                  <div id="collapse30" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapse20')" style="cursor:hand" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion3" >
					                      <h4 class="pull-left" style="margin-bottom:0px">20% Presentation/Demo</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px"></p>
					                    </a>
					                  </div>
					                  <div id="collapse20" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapse10')" style="cursor:hand" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion3" >
					                      <h4 class="pull-left" style="margin-bottom:0px">10% Build solution</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px"></p>
					                    </a>
					                  </div>
					                  <div id="collapse10" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapse0')" style="cursor:hand" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion3" >
					                      <h4 class="pull-left" style="margin-bottom:0px">0% แพ้</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px;color:#AC193D"></p>
					                    </a>
					                  </div>
					                  <div id="collapse0" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapseP')" style="cursor:hand" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion3" >
					                      <h4 class="pull-left" style="margin-bottom:0px">P Pending</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px;color:#AC193D"></p>
					                    </a>
					                  </div>
					                  <div id="collapseP" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapseV')" style="cursor:hand" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion3" >
					                      <h4 class="pull-left" style="margin-bottom:0px">v ยกเลิกโครงการ</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px;color:#AC193D"></p>
					                    </a>
					                  </div>
					                  <div id="collapseV" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading" style="border-bottom:3px #0072C6 solid;">
					                    <a class=" accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion3" style="cursor: default;">
					                      <h4 class="pull-left">Summary</h4>
					                      <span id="tot_nedu" class="label label-inverse pull-right" style="margin-top:5px" title="ยกเว้น 0,V"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p id="sum_nedu" class="lead text-success pull-right" style="margin-bottom:0px;color:#0072C6;"></p>
					                    </a>
					                  </div>
					                </div>
					            </div>						
							</div>
						</div>
					    <div class="tab-pane fade active in" id="Education">
					    	<div class="widget-header clearfix">
								<span class="pull-left"><i class="icon-progress-3"></i> Progress</span>
								<label style="background-color: #DFF0D8; display: inline-block; width: 150px; text-align: center; float: right; cursor: default;">Potential>50</label>
								<label style="background-color: #FCF8E3; display: inline-block; width: 150px; text-align: center; float: right; cursor: default;">Potential=50</label>
								<label style="background-color: #F2DEDE; display: inline-block; width: 150px; text-align: center; float: right; cursor: default;">Potential&lt;50</label>
							</div>
					        <div class="bg-color white rounded-top rounded-bottom">
								<div class="accordion" id="accordion2">
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapse100e')" style="cursor:hand" class="accordion-toggle collapsed clearfix" data-toggle="collapse" data-parent="#accordion2" >
					                      <h4 class="pull-left" style="margin-bottom:0px">100% Sign date</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px;color:#008A17"></p>
					                    </a>
					                  </div>
					                  <div id="collapse100e" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapse90e')" style="cursor:hand" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion2" >
					                      <h4 class="pull-left" style="margin-bottom:0px">90% ประกาศผลแล้วรอเซ็นสัญญา</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px"></p>
					                    </a>
					                  </div>
					                  <div id="collapse90e" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapse80e')" style="cursor:hand" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion2" >
					                      <h4 class="pull-left" style="margin-bottom:0px">80% ยื่นซองแล้ว</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px"></p>
					                    </a>
					                  </div>
					                  <div id="collapse80e" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapse70e')" style="cursor:hand" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion2" >
					                      <h4 class="pull-left" style="margin-bottom:0px">70% Bidding date</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px"></p>
					                    </a>
					                  </div>
					                  <div id="collapse70e" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapse60e')" style="cursor:hand"  class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion2" >
					                      <h4 class="pull-left" style="margin-bottom:0px">60% TOR Final</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px"></p>
					                    </a>
					                  </div>
					                  <div id="collapse60e" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapse50e')" style="cursor:hand"  class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion2" >
					                      <h4 class="pull-left" style="margin-bottom:0px">50% ได้งบประมาณ</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px"></p>
					                    </a>
					                  </div>
					                  <div id="collapse50e" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapse40e')" style="cursor:hand"  class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion2" >
					                      <h4 class="pull-left" style="margin-bottom:0px">40% ขั้นตอนการพิจารณาอนุมัติงบประมาณ</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px"></p>
					                    </a>
					                  </div>
					                  <div id="collapse40e" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapse30e')" style="cursor:hand"  class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion2" >
					                      <h4 class="pull-left" style="margin-bottom:0px">30% ร่างโครงการ (ของบประมาณ)</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px"></p>
					                    </a>
					                  </div>
					                  <div id="collapse30e" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapse20e')" style="cursor:hand"  class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion2" >
					                      <h4 class="pull-left" style="margin-bottom:0px">20% Presentation/Demo</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px"></p>
					                    </a>
					                  </div>
					                  <div id="collapse20e" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapse10e')" style="cursor:hand"  class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion2" >
					                      <h4 class="pull-left" style="margin-bottom:0px">10% Build solution</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px"></p>
					                    </a>
					                  </div>
					                  <div id="collapse10e" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapse0e')" style="cursor:hand"  class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion2" >
					                      <h4 class="pull-left" style="margin-bottom:0px">0% แพ้</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px;color:#AC193D"></p>
					                    </a>
					                  </div>
					                  <div id="collapse0e" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapsePe')" style="cursor:hand"  class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion2" >
					                      <h4 class="pull-left" style="margin-bottom:0px">P Pending</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px;color:#AC193D"></p>
					                    </a>
					                  </div>
					                  <div id="collapsePe" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapseVe')" style="cursor:hand"  class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion2" >
					                      <h4 class="pull-left" style="margin-bottom:0px">v ยกเลิกโครงการ</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px;color:#AC193D"></p>
					                    </a>
					                  </div>
					                  <div id="collapseVe" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading" style="border-bottom:3px #0072C6 solid;">
					                    <a class=" accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion2" style="cursor: default;">
					                      <h4 class="pull-left">Summary</h4>
					                      <span id="tot_edu" class="label label-inverse pull-right" style="margin-top:5px" title="ยกเว้น 0,V"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p id="sum_edu" class="lead text-success pull-right" style="margin-bottom:0px;color:#0072C6;"></p>
					                    </a>
					                  </div>
					                </div>
					            </div>						
							</div>
						</div>
					</div>				
				</div>
			</div>

			<div class="row" style="display: none;">
				<div class="span6">
					<div class="widget widget-events">
						<div class="widget-header clearfix">
							<span class="pull-left"><i class="icon-calendar-1"></i> Bidding Date</span>
							<div class="dropdown pull-right">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-cog-2"></i> <b class="caret"></b></a>
								<ul class="dropdown-menu">
				                    <li><a href="typography.html">Typography</a></li>
									<li><a href="forms.html">Forms</a></li>
									<li><a href="ui.html">UI Elements</a></li>
									<li><a href="tables.html">Tables</a></li>
									<li><a href="charts.html">Charts</a></li>
									<li><a href="calendar.html">Calendars</a></li>
				                </ul>
							</div>
						</div>
						<div class="bg-color white rounded event-item">
							<div class="box-padding narrow">
								<div class="media">
					              <a class="pull-left" href="#">
					                <img class="media-object" src="http://placehold.it/80x80/43484D/fff">
					              </a>
					              	<div class="pull-right event-date aligncenter" href="#">
					              		<h1>21</h1>
					              		<h5 class="uppercase">Dec 13</h5>
					              	</div>
					              <div class="media-body">
					                <h4 class="media-heading">Rooftop Bawlin' - Part III</h4>
					                <ul class="unstyled event-details">
					                	<li><i class="icon-clock-5 pull-left"></i> <span class="light">9:00am - 11:00am</span></li>
					                	<li><i class="icon-location-3 pull-left"></i> <span class="light">795 Folsom Ave, Suite 600, San Francisco, CA 94107</span></li>
					                	<li><i class="icon-users-2 pull-left"></i> <span class="light">270 Attendees</span></li>
					                </ul>
					                <a class="btn btn-flat btn-wide btn-turqoise">Attend</a>
					              </div>
					            </div>
							</div>
						</div>
						<div class="bg-color white rounded event-item">
							<div class="box-padding narrow">
								<div class="media">
					              <a class="pull-left" href="#">
					                <img class="media-object" src="http://placehold.it/80x80/43484D/fff">
					              </a>
					              	<div class="pull-right event-date aligncenter" href="#">
					              		<h1>30</h1>
					              		<h5 class="uppercase">June 13</h5>
					              	</div>
					              <div class="media-body">
					                <h4 class="media-heading">S&amp;D Company Retreat</h4>
					                <ul class="unstyled event-details">
					                	<li><i class="icon-clock-5 pull-left"></i> <span class="light">All day</span></li>
					                	<li><i class="icon-location-3 pull-left"></i> <span class="light">795 Folsom Ave, Suite 600, San Francisco, CA 94107</span></li>
					                	<li><i class="icon-users-2 pull-left"></i> <span class="light">18 Attendees</span></li>
					                </ul>
					                <a class="btn btn-flat btn-wide btn-turqoise">Attend</a>
					              </div>
					            </div>
							</div>
						</div>
					</div>
				</div>
				<div class="span6">
					<div class="widget widget-events">
						<div class="widget-header clearfix">
							<span class="pull-left"><i class="icon-calendar-6"></i> Contract Sign Date</span>
							<div class="dropdown pull-right">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-cog-2"></i> <b class="caret"></b></a>
								<ul class="dropdown-menu">
				                    <li><a href="typography.html">Typography</a></li>
									<li><a href="forms.html">Forms</a></li>
									<li><a href="ui.html">UI Elements</a></li>
									<li><a href="tables.html">Tables</a></li>
									<li><a href="charts.html">Charts</a></li>
									<li><a href="calendar.html">Calendars</a></li>
				                </ul>
							</div>
						</div>
						<div class="bg-color white rounded event-item">
							<div class="box-padding narrow">
								<div class="media">
					              <a class="pull-left" href="#">
					                <img class="media-object" src="http://placehold.it/80x80/43484D/fff">
					              </a>
					              	<div class="pull-right event-date aligncenter" href="#">
					              		<h1>21</h1>
					              		<h5 class="uppercase">Dec 13</h5>
					              	</div>
					              <div class="media-body">
					                <h4 class="media-heading">Rooftop Bawlin' - Part III</h4>
					                <ul class="unstyled event-details">
					                	<li><i class="icon-clock-5 pull-left"></i> <span class="light">9:00am - 11:00am</span></li>
					                	<li><i class="icon-location-3 pull-left"></i> <span class="light">795 Folsom Ave, Suite 600, San Francisco, CA 94107</span></li>
					                	<li><i class="icon-users-2 pull-left"></i> <span class="light">270 Attendees</span></li>
					                </ul>
					                <a class="btn btn-flat btn-wide btn-turqoise">Attend</a>
					              </div>
					            </div>
							</div>
						</div>
						<div class="bg-color white rounded event-item">
							<div class="box-padding narrow">
								<div class="media">
					              <a class="pull-left" href="#">
					                <img class="media-object" src="http://placehold.it/80x80/43484D/fff">
					              </a>
					              	<div class="pull-right event-date aligncenter" href="#">
					              		<h1>30</h1>
					              		<h5 class="uppercase">June 13</h5>
					              	</div>
					              <div class="media-body">
					                <h4 class="media-heading">S&amp;D Company Retreat</h4>
					                <ul class="unstyled event-details">
					                	<li><i class="icon-clock-5 pull-left"></i> <span class="light">All day</span></li>
					                	<li><i class="icon-location-3 pull-left"></i> <span class="light">795 Folsom Ave, Suite 600, San Francisco, CA 94107</span></li>
					                	<li><i class="icon-users-2 pull-left"></i> <span class="light">18 Attendees</span></li>
					                </ul>
					                <a class="btn btn-flat btn-wide btn-turqoise">Attend</a>
					              </div>
					            </div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
	</div>
</section>

<div id="alerterrorID" class="alert alert-error" style="width:600px;z-index:9999">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <div class="media">
        <i class="icon-cancel-circled-1 pull-left" style="font-size:60px"></i>
        <div class="media-body">
            <h2 class="media-heading">Error Cookie ID</h2>
            <p class="lead">มีปัญหาในการดึงข้อมูล ID กรุณาลอง refresh ใหม่อีกครั้ง 
            หรือ ปิดแล้วเข้าใช้งานระบบใหม่ หรือ ติดต่อผู้รับผิดชอบ</p>
        </div>
    </div>
</div>
<div id="footer" class="bg-color dark-blue">
	<div class="container">
		<div class="box-padding">
			Copyright &copy; 2013 Sales Forecast
		</div>
	</div>
</div>

	<script src="js/library/jquery/jquery.min.js"></script>
	<script src="js/library/jquery/jquery-ui.min.js"></script>
	<script src="js/library/masonry/jquery.masonry.min.js"></script>
	<script src="js/library/flexslider/jquery.flexslider-min.js"></script>
	<script src="js/library/dataTables/jquery.dataTables.min.js"></script>
	<script src="js/library/fullcalendar/fullcalendar.min.js"></script>
	<script src="js/library/fullcalendar/gcal.js"></script>
	<script src="js/library/forms/jquery.hotkeys.js"></script>
	<script src="js/library/forms/bootstrap-wysiwyg.js"></script>
	<script src="js/library/forms/picker.js"></script>
	<script src="js/library/forms/picker.date.js"></script>
	<script src="js/library/forms/picker.time.js"></script>
	<script src="js/library/forms/jquery.bootstrap.wizard.min.js"></script>
	<script src="js/library/charts/jquery.flot.js"></script>
	<script src="js/library/charts/jquery.flot.resize.js"></script>
	<script src="js/library/charts/jquery.flot.stack.js"></script>
	<script src="js/library/charts/jquery.flot.pie.min.js"></script>
	<script src="js/library/charts/jquery.sparkline.min.js"></script>
	<script src="js/library/syntaxHighlighter/shCore.js"></script>
	<script src="js/library/syntaxHighlighter/shBrushCss.js"></script>
	<script src="js/library/syntaxHighlighter/shBrushXml.js"></script>
	<script src="js/library/syntaxHighlighter/shBrushJScript.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="js/styler/custom.js"></script>
	<script src="js/set_cookie.js"></script>
	<script type="text/javascript" src="js/bouncebox-plugin/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="js/bouncebox-plugin/jquery.bouncebox.1.0.js"></script>
	<script type="text/javascript">
		function doMenu(item) 
		{ 
			var ids="#"+item;
			var checkDisplay=$(ids).css('display');
			$(ids).slideToggle(100);

			console.log(item +" : " + checkDisplay );
			if(checkDisplay == "none") 
	        {                           
	            //$('#'+item).css('display','block');
	            $(ids).removeClass( "displayNone" ).addClass( "displayBlock" );
	        }
	        else
	        {
	            //$('#'+item).css('display','none');	
	            $(ids).removeClass( "displayBlock" ).addClass( "displayNone" );                        	
	        }
		} 
		var check_sort = "";
		var slc_sales = "";
	    $(document).ready(function() {
	    	$('#alerterrorID').bounceBox();

		    $('#alerterrorID').click(function(){		    	
				$("#loadingBlank").hide();
		        $('#alerterrorID').bounceBoxHide();
		    });
//------------------------------------------------- Function ------------------------------------------------

			function checkid()
			{
				var _loginID = "<?php echo $_COOKIE['Ses_ID']?>";
				//alert(_loginID );
				if(_loginID=="" || _loginID==" ")
				{					
					$("#loadingBlank").show();
		        	$('#alerterrorID').bounceBoxShow();
				}
				else
				{

					$("#loadingBlank").hide();
		        }	//$('#alerterrorID').bounceBoxShow();
			}
			function numFormat(x) {
				var parts = x.toString().split(".");
			    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			    return parts.join(".");
			}
			function listProjects() {
				$("#loading").show();
				slc_sales = "";
				slc_sort = "";
				$("#selected_salesName").empty();
				$("#selected_sortBy").empty();
				$("#selected_year").text($("#slc_year").val());
				$($(".selectSales_inp_saleslist")).each(function() {
					if ($(this).prop("checked")) {
						slc_sales += $(this).val() + ",";
						$("#selected_salesName").append('<li class="clearfix"><p class="text-left"><h4>' + $(this).parent().find("h4")[0].innerHTML + '</h4></p></li>');
					}
				});
				check_order = '';
				check_sort = "";
				$($(".selectSort_inp_by")).each(function() {
					if ($(this).prop("checked")) {
						slc_order = " DESC";
						check_order = "2";
						slc_orderTxt = "(" + $(this).closest("tr").find("h4")[2].innerHTML + ")";
						if ($(this).closest("tr").find("input")[1].checked) {
							slc_order = " ASC";
							check_order = "1";
							slc_orderTxt = "(" + $(this).closest("tr").find("h4")[1].innerHTML + ")";
						}
						if ($(this).val() == "TargetIncome" || $(this).val() == "Project") {
							slc_sort += $(this).val() + slc_order + ", ";
						} else {
							slc_sort += "YEAR(" + $(this).val() + ")" + slc_order + ", " + "MONTH(" + $(this).val() + ")" + slc_order + ", " + "DAY(" + $(this).val() + ")" + slc_order + ", ";
						}
						check_sort += check_order + $(this).val() + ",";
						$("#selected_sortBy").append('<li ><p ><h4 style="display: inline-block">' + $(this).parent().find("h4")[0].innerHTML + '</h4> <span class="pull-right"><h4 style="display: inline-block">' + slc_orderTxt + '</h4></span></p></li>');
					}
				});
				slc_sales = slc_sales.substr(0, (slc_sales.length - 1));
				slc_sort = slc_sort.substr(0, (slc_sort.length - 2));
				check_sort = check_sort.substr(0, (check_sort.length - 1));
				
				console.log('saleid ',slc_sales);
				$.ajax({ 
					type:"POST",
					dataType: "json",
			        url: "Ajax/SV_Select_Project_sql.php",
			        data: {
			        	year: $("#slc_year").val(),
			        	// saleid: "0563",
			        	saleid: slc_sales,
			        	orderby: slc_sort
			        },
			        success: function (item) {
		        		//console.log(json);
		        		//item = json;
			        	progList = ["100", "90", "80", "70", "60", "50", "40", "30", "20", "10", "0", "P", "v"];
			        	i = jsi = sum = sumi = 0;
			        	$($("#Non_Education").find("tbody")).each(function() {
			        		$(this).empty();
			        		total = totalItems = 0
			        		while (jsi < item.length) 
			        		{
			        			var ID=item[jsi][0];
			        			var PEContractNo=item[jsi][1];
			        			var Project=item[jsi][2];
			        			var Education=item[jsi][3];
			        			var Progress=item[jsi][4];
			        			var Potential=item[jsi][5];
			        			var TargetIncome=item[jsi][6];
			        			var TargetBudget=item[jsi][7];
			        			var TimeFrameBidingDate=item[jsi][8];
			        			var TimeFrameContractSigndate=item[jsi][9];
			        			var SaleID=item[jsi][10];
			        			var SaleRepresentative=item[jsi][11];
								//console.log('items[jsi].Education ',items[jsi].Education);
			        			if (Education == "1") {
									//console.log('items[jsi].Education ',items[jsi].Education);
			        				jsi++;
			        				continue;
		        				}
			        			if (Progress != progList[i]) break;
										var val_Progress = Progress;
										if(Progress == 'v'){
											val_Progress = 111;
										}
										else if(Progress == 'P'){
											val_Progress = 444;
										}
			        			parseInt(Potential) < 50 ? trCls = "error" : parseInt(Potential) == 50 ? trCls = "warning" : trCls = "success";
			        			btn_edit = _loginID == SaleID ? '<a href="javascript:editProject('+ parseInt(ID) +',1,'+ val_Progress +');" title="Edit"><i class="i_btn icon-pencil-1" style="color: #000000;"></i></a>' : '<a href="javascript:previewForecast('+ parseInt(ID) +',1,'+ val_Progress +');" title="Preview"><i class="i_btn icon-newspaper" style="color: #000000;"></i></a>';
			        			$(this).append('<tr class="' + trCls + '" title="Sales: ' + SaleRepresentative + ', Potential: ' + Potential + '"><td>' + PEContractNo + '</td><td>' + Project + '</td><td>' + TimeFrameBidingDate + '</td><td>' + TimeFrameContractSigndate + '</td><td class="td_money">' + numFormat(parseFloat(TargetIncome).toFixed(2)) + '</td><td style="text-align: center;">' + btn_edit + '</td></tr>');
			        			total += parseFloat(TargetIncome);
			        			totalItems++;
			        			jsi++;
			        		}
			        		$(this).closest(".accordion-group").find(".text-success")[0].innerHTML = numFormat(total.toFixed(2));
			        		$(this).closest(".accordion-group").find(".label-inverse")[0].innerHTML = totalItems;
			        		if (i <= 9) {
				        		sum += total;
				        		sumi += totalItems;
		        			}
			        		i++;
			        	});
								$("#sum_nedu").text(numFormat(sum.toFixed(2)));
								$("#tot_nedu").text(sumi);
			        	i = jsi = sum = sumi = 0;
			        	$($("#Education").find("tbody")).each(function() {
			        		$(this).empty();
			        		total = totalItems = 0
			        		while (jsi < item.length) {
			        			var ID=item[jsi][0];
			        			var PEContractNo=item[jsi][1];
			        			var Project=item[jsi][2];
			        			var Education=item[jsi][3];
			        			var Progress=item[jsi][4];
			        			var Potential=item[jsi][5];
			        			var TargetIncome=item[jsi][6];
			        			var TargetBudget=item[jsi][7];
			        			var TimeFrameBidingDate=item[jsi][8];
			        			var TimeFrameContractSigndate=item[jsi][9];
			        			var SaleID=item[jsi][10];
			        			var SaleRepresentative=item[jsi][11];
			        			if (Education == "0") {
			        				jsi++;
			        				continue;
		        				}
			        			if (Progress != progList[i]) break;
										var val_Progress = Progress;
										if(Progress == 'v'){
											val_Progress = 111;
										}
										else if(Progress == 'P'){
											val_Progress = 444;
										}
			        			parseInt(Potential) < 50 ? trCls = "error" : parseInt(Potential) == 50 ? trCls = "warning" : trCls = "success";
			        			btn_edit = _loginID == SaleID ? '<a href="javascript:editProject('+ parseInt(ID) +',2,'+ val_Progress +');" title="Edit"><i class="i_btn icon-pencil-1" style="color: #000000;"></i></a>' : '<a href="javascript:previewForecast('+ parseInt(ID) +',2,'+ val_Progress +');" title="Preview"><i class="i_btn icon-newspaper" style="color: #000000;"></i></a>';
			        			$(this).append('<tr class="' + trCls + '" title="Sales: ' + SaleRepresentative + ', Potential: ' + Potential + '"><td>' + PEContractNo + '</td><td>' + Project + '</td><td>' + TimeFrameBidingDate + '</td><td>' + TimeFrameContractSigndate + '</td><td class="td_money">' + numFormat(parseFloat(TargetIncome).toFixed(2)) + '</td><td style="text-align: center;">' + btn_edit + '</td></tr>');
			        			total += parseFloat(TargetIncome);
			        			totalItems++;
			        			jsi++;
			        		}
			        		$(this).closest(".accordion-group").find(".text-success")[0].innerHTML = numFormat(total.toFixed(2));
			        		$(this).closest(".accordion-group").find(".label-inverse")[0].innerHTML = totalItems;
		        			if (i <= 9) {
				        		sum += total;
				        		sumi += totalItems;
		        			}
			        		i++;
			        	});
						$("#sum_edu").text(numFormat(sum.toFixed(2)));
						$("#tot_edu").text(sumi);
						funcDisplayProgress_Load();
						$("#loading").hide();
			        },
					error: function() {
						alert("เกิดปัญหาในการค้นหากรุณาลองใหม่อีกครั้ง");
						funcDisplayProgress_Load();
						$("#loading").hide();
					}
			    });
			}
			function funcDisplayProgress_Init()
			{
				console.log("funcDisplayProgress_Init");
				//$(".displayBlock").removeClass("displayBlock").addClass("displayNone");
			}
			function funcDisplayProgress_Load()
			{
				console.log("funcDisplayProgress_Load");
				//$(".displayNone").removeClass("displayNone").addClass("displayBlock");
				$(".displayBlock").removeClass("displayBlock").addClass("displayNone");

			}
			//----------------------
			//------- Initial ------
			//----------------------
			funcDisplayProgress_Init();
			var ckYear = getCookie("Ses_year");
			var ckSale = getCookie("Ses_saleid");
			var ckSort = getCookie("Ses_sort");
			var ckTab = getCookie("Ses_tab");
			var ckProgress = getCookie("Ses_progress");
			//console.log('ckYear ',ckYear);
			//console.log('ckSale ',ckSale);
			//console.log('ckSort ',ckSort);
			//console.log('ckTab ',ckTab);
			//console.log('ckProgress ',ckProgress);
			deleteCookie("Ses_year");
			deleteCookie("Ses_saleid");
			deleteCookie("Ses_sort");
			deleteCookie("Ses_tab");
			deleteCookie("Ses_progress");
			var _loginID = "<?php echo $_COOKIE['Ses_ID']?>";
			$("#btn_NEdu").click();
			if (ckTab!=null && ckTab!="" && ckTab == "2"){
				$("#btn_Edu").click();
			}
			if (ckProgress!=null && ckProgress!=""){
				if(ckTab == "2"){
					switch(ckProgress)
					{
					case '100':
						doMenu("collapse100e");
						break;
					case '90':						
						doMenu("collapse90e");
						break;
					case '80':	
						doMenu("collapse80e");
						break;
					case '70':	
						doMenu("collapse70e");
						break;
					case '60':	
						doMenu("collapse60e");
						break;
					case '50':	
						doMenu("collapse50e");
						break;
					case '40':	
						doMenu("collapse40e");
						break;
					case '30':	
						doMenu("collapse30e");
						break;
					case '20':	
						doMenu("collapse20e");
						break;
					case '10':	
						doMenu("collapse10e");
						break;
					case '0':	
						doMenu("collapse0e");
						break;
					case 'P':	
						doMenu("collapsePe");
						break;
					default:	
						doMenu("collapseVe");
					}
				}else{
					switch(ckProgress)
					{
					case '100':	
						doMenu("collapse100");
						break;
					case '90':
						doMenu("collapse90");
						break;
					case '80':
						doMenu("collapse80");
						break;
					case '70':
						doMenu("collapse70");
						break;
					case '60':
						doMenu("collapse60");
						break;
					case '50':
						doMenu("collapse50");
						break;
					case '40':
						doMenu("collapse40");
						break;
					case '30':
						doMenu("collapse30");
						break;
					case '20':
						doMenu("collapse20");
						break;
					case '10':
						doMenu("collapse10");
						break;
					case '0':
						doMenu("collapse0");
						break;
					case 'P':	
						doMenu("collapseP");
						break;
					default:
						doMenu("collapseV");
					}
				}
			}
			if (ckSort!=null && ckSort!=""){
				var arr_ckSort = new Array();
				arr_ckSort = ckSort.split(",");
				$($(".selectSort_inp_by")).each(function() {
					$(this).prop('checked',false);
					for (var i=0;i<arr_ckSort.length;i++){
						str_sort = arr_ckSort[i].substr(1, (arr_ckSort[i].length - 1));
						str_order = arr_ckSort[i].substr(0, 1);
						if ($(this).val() == str_sort){
							$(this).prop('checked',true); 
							if(str_order == '2'){
								$(this).closest("tr").find("input")[1].checked = false;
								$(this).closest("tr").find("input")[2].checked = true;
							}else{
								$(this).closest("tr").find("input")[1].checked = true;
								$(this).closest("tr").find("input")[2].checked = false;
							}
						}	
					}
				
				});
			}
			
			$.ajax({ 
				type: "POST",
				dataType: "json",
		        url: "AJAX/select_year.php",
		        success: function (json) {
		        	 $.each(json, function() {
						var item = this;
		        		$("#slc_year").append("<option value='" + item[0] + "'>" + item[0] + "</option>");
					});
					$("#slc_year").val((new Date).getFullYear());
					if (_loginID == "0095") {
						depMng = "Sales TM";
					} else if (_loginID == "4371") {
						depMng = "Sales 1";
					} else if (_loginID == "0416") {
						depMng = "Sales 2";
					} else if (_loginID == "3581") {
						depMng = "Sales 3";
					} else if (_loginID == "3882") {
						depMng = "Sales 4";
					} else if (_loginID == "3784") {
						depMng = "Sales CSD";
					} else if (_loginID == "1190") {
						depMng = "Sales GEO";
					} else if (_loginID == "1994") {
						depMng = "Sales CLMV";
					} else if (_loginID == "4371") {
						depMng = "Sales CSD";
					}else {
						depMng = "";
					}
					$.ajax({ 
						type:"POST",
						dataType: "json",
						url: "Ajax/SV_Select_Sale.php",
						data: {
							saledept: depMng,
							loginid : _loginID
						},
						success: function (json) {
							console.log(json);
							console.log(depMng);
							$("#selectSales_ul_sales").append('<label class="checkbox"><input class="selectSales_all" type="checkbox"/> <h4>All</h4></label>');
							if(json[0].length>1)
								{
									for(var ii = 0;ii<json[0].length;ii++){
										$("#selectSales_ul_sales").append('<label class="checkbox"><input name ="selectSales_allsale" class="selectSales_all'+ii+'" type="checkbox" onclick="SelectSale('+ii+')"/><h4>All '+json[0][ii]+'</h4></label>');

									$(json).each(function() {
								var item = this;
								if (ckSale!=null && ckSale!=""){
											checked = "";
											for (var i=0;i<arr_ckSale.length;i++)
											{ 
												if(item[0] == arr_ckSale[i]){
													checked = "checked";
												}
											}
								}else{
									item[0] == _loginID ? checked = "checked" : checked = "";
								}
									
								 	if(item[2] == json[0][ii]){
									$("#selectSales_ul_sales").append('<label class="checkbox"><input name="selectSales_inp_saleslist'+ii+'"class="selectSales_inp_saleslist" type="checkbox"  value="' + item[0] + '" ' + checked + '/> <h4>' + item[1] + '</h4></label>');
								}
							});
							$("#selectSales_ul_sales").append('</br>');
								}
							
								}else{
							
							if (ckSale!=null && ckSale!=""){
										var arr_ckSale = new Array();
										arr_ckSale = ckSale.split(",");
									}
							$(json).each(function() {
								var item = this;
								if (ckSale!=null && ckSale!=""){
											checked = "";
											for (var i=0;i<arr_ckSale.length;i++)
											{ 
												if(item[0] == arr_ckSale[i]){
													checked = "checked";
												}
											}
								}else{
									item[0] == _loginID ? checked = "checked" : checked = "";
								}
								
								
									if(item[2] == json[0][0]){
										console.log(_loginID);
									$("#selectSales_ul_sales").append('<label class="checkbox"><input class="selectSales_inp_saleslist" type="checkbox" value="' + item[0] + '" ' + checked + '/> <h4>' + item[1] + '</h4></label>');
									}
								
								
							});
							}
							if (ckYear!=null && ckYear!=""){
								$("#slc_year").val(ckYear);
							}
							listProjects();
							$('.selectSales_all').on('click',function(){
								if ($(".selectSales_all").is(':checked')) {
									$(".selectSales_inp_saleslist").prop("checked", true);
									$('[name="selectSales_allsale"]').prop("checked", true);
								} else {
									$(".selectSales_inp_saleslist").prop("checked", false);
									$('[name="selectSales_allsale"]').prop("checked", false);
								}
							});
							$('.selectSales_inp_saleslist').on('click',function(){
								if( $(".selectSales_inp_saleslist:checked").length == $(".selectSales_inp_saleslist").length){
									$(".selectSales_all").prop("checked", true);
								}else{
									$(".selectSales_all").prop("checked", false);
								}
								for(var index = 0 ; index < json[0].length;index++){
									if( $('[name="selectSales_inp_saleslist'+index+'"]:checked').length == $('[name="selectSales_inp_saleslist'+index+'"]').length){
									$(".selectSales_all"+index).prop("checked", true);
								}else{
									$(".selectSales_all"+index).prop("checked", false);
								}
								}
							});
				        },
						error: function() {
							alert("เกิดปัญหาในการโหลดข้อมูลกรุณารีเฟรชหน้าเว็บอีกครั้ง");
							$("#loading").hide();
						}
				    });
			},
			error: function() {
				alert("เกิดปัญหาในการโหลดข้อมูลกรุณารีเฟรชหน้าเว็บอีกครั้ง");
				$("#loading").hide();
			}
		    });
		    
			checkid();
			//------------------------------------------

			//--------------------- Event --------------
			//education
			/*$("#edu100").on("click", function() {
				if($("#collapse100e").height() == 0){	
					$("#edu100").addClass('collapsed');
					$("#collapse100e").removeClass('in');
					$("#collapse100e").css("height","auto");
					}else{
					$("#edu100").removeClass('collapsed');
					$("#collapse100e").addClass('in');
					$("#collapse100e").css("height","0px");
					}
			});
			$("#edu20").on("click", function() {
				if($("#collapse20e").height() == 0){	
					$("#edu20").addClass('collapsed');
					$("#collapse20e").removeClass('in');
					$("#collapse20e").css("height","auto");
					}else{
					$("#edu20").removeClass('collapsed');
					$("#collapse20e").addClass('in');
					$("#collapse20e").css("height","0px");
					}
			});
			$("#edu30").on("click", function() {
				if($("#collapse30e").height() == 0){	
					$("#edu30").addClass('collapsed');
					$("#collapse30e").removeClass('in');
					$("#collapse30e").css("height","auto");
					}else{
					$("#edu30").removeClass('collapsed');
					$("#collapse30e").addClass('in');
					$("#collapse30e").css("height","0px");
					}
			});
			$("#edu40").on("click", function() {
				if($("#collapse40e").height() == 0){
					$("#edu40").addClass('collapsed');
					$("#collapse40e").removeClass('in');
					$("#collapse40e").css("height","auto");
					}else{
					$("#edu40").removeClass('collapsed');
					$("#collapse40e").addClass('in');
					$("#collapse40e").css("height","0px");
					}
			});
			$("#edu50").on("click", function() {
				if($("#collapse50e").height() == 0){
					$("#edu50").addClass('collapsed');
					$("#collapse50e").removeClass('in');
					$("#collapse50e").css("height","auto");
					}else{
					$("#edu50").removeClass('collapsed');
					$("#collapse50e").addClass('in');
					$("#collapse50e").css("height","0px");
					}
			});
			$("#edu60").on("click", function() {
				if($("#collapse60e").height() == 0){
					$("#edu60").addClass('collapsed');
					$("#collapse60e").removeClass('in');
					$("#collapse60e").css("height","auto");
					}else{
					$("#edu60").removeClass('collapsed');
					$("#collapse60e").addClass('in');
					$("#collapse60e").css("height","0px");
					}
			});
			$("#edu70").on("click", function() {
				if($("#collapse70e").height() == 0){
					$("#edu70").addClass('collapsed');
					$("#collapse70e").removeClass('in');
					$("#collapse70e").css("height","auto");
					}else{
					$("#edu70").removeClass('collapsed');
					$("#collapse70e").addClass('in');
					$("#collapse70e").css("height","0px");
					}
			});
			$("#edu80").on("click", function() {
				if($("#collapse80e").height() == 0){
					$("#edu80").addClass('collapsed');
					$("#collapse80e").removeClass('in');
					$("#collapse80e").css("height","auto");
					}else{
					$("#edu80").removeClass('collapsed');
					$("#collapse80e").addClass('in');
					$("#collapse80e").css("height","0px");
					}
			});
			$("#edu90").on("click", function() {
				if($("#collapse90e").height() == 0){
					$("#edu90").addClass('collapsed');
					$("#collapse90e").removeClass('in');
					$("#collapse90e").css("height","auto");
					}else{
					$("#edu90").removeClass('collapsed');
					$("#collapse90e").addClass('in');
					$("#collapse90e").css("height","0px");
					}
			});
			$("#edu10").on("click", function() {
				if($("#collapse10e").height() == 0){	
					$("#edu10").addClass('collapsed');
					$("#collapse10e").removeClass('in');
					$("#collapse10e").css("height","auto");
					}else{
					$("#edu10").removeClass('collapsed');
					$("#collapse10e").addClass('in');
					$("#collapse10e").css("height","0px");
					}
			});
			$("#edu0").on("click", function() {
				if($("#collapse0e").height() == 0){
					$("#edu0").addClass('collapsed');
					$("#collapse0e").removeClass('in');
					$("#collapse0e").css("height","auto");
					}else{
					$("#edu0").removeClass('collapsed');
					$("#collapse0e").addClass('in');
					$("#collapse0e").css("height","0px");
					}
			});
			$("#eduv").on("click", function() {
				if($("#collapseVe").height() == 0){
					$("#eduv").addClass('collapsed');
					$("#collapseVe").removeClass('in');
					$("#collapseVe").css("height","auto");
					}else{
					$("#eduv").removeClass('collapsed');
					$("#collapseVe").addClass('in');
					$("#collapseVe").css("height","0px");
					}
			});
			//non-edu
			$("#n100").on("click", function() {
				if($("#collapse100").height() == 0){	
					$("#n100").addClass('collapsed');
					$("#collapse100").removeClass('in');
					$("#collapse100").css("height","auto");
					}else{
					$("#n100").removeClass('collapsed');
					$("#collapse100").addClass('in');
					$("#collapse100").css("height","0px");
					}
			});
			$("#n20").on("click", function() {
				if($("#collapse20").height() == 0){	
					$("#n20").addClass('collapsed');
					$("#collapse20").removeClass('in');
					$("#collapse20").css("height","auto");
					}else{
					$("#n20").removeClass('collapsed');
					$("#collapse20").addClass('in');
					$("#collapse20").css("height","0px");
					}
			});
			$("#n30").on("click", function() {
				if($("#collapse30").height() == 0){	
					$("#n30").addClass('collapsed');
					$("#collapse30").removeClass('in');
					$("#collapse30").css("height","auto");
					}else{
					$("#n30").removeClass('collapsed');
					$("#collapse30").addClass('in');
					$("#collapse30").css("height","0px");
					}
			});
			$("#n40").on("click", function() {
				if($("#collapse40").height() == 0){
					$("#n40").addClass('collapsed');
					$("#collapse40").removeClass('in');
					$("#collapse40").css("height","auto");
					}else{
					$("#n40").removeClass('collapsed');
					$("#collapse40").addClass('in');
					$("#collapse40").css("height","0px");
					}
			});
			$("#n50").on("click", function() {
				if($("#collapse50").height() == 0){
					$("#n50").addClass('collapsed');
					$("#collapse50").removeClass('in');
					$("#collapse50").css("height","auto");
					}else{
					$("#n50").removeClass('collapsed');
					$("#collapse50").addClass('in');
					$("#collapse50").css("height","0px");
					}
			});
			$("#n60").on("click", function() {
				if($("#collapse60").height() == 0){
					$("#n60").addClass('collapsed');
					$("#collapse60").removeClass('in');
					$("#collapse60").css("height","auto");
					}else{
					$("#n60").removeClass('collapsed');
					$("#collapse60").addClass('in');
					$("#collapse60").css("height","0px");
					}
			});
			$("#n70").on("click", function() {
				if($("#collapse70").height() == 0){
					$("#n70").addClass('collapsed');
					$("#collapse70").removeClass('in');
					$("#collapse70").css("height","auto");
					}else{
					$("#n70").removeClass('collapsed');
					$("#collapse70").addClass('in');
					$("#collapse70").css("height","0px");
					}
			});
			$("#n80").on("click", function() {
				if($("#collapse80").height() == 0){
					$("#n80").addClass('collapsed');
					$("#collapse80").removeClass('in');
					$("#collapse80").css("height","auto");
					}else{
					$("#n80").removeClass('collapsed');
					$("#collapse80").addClass('in');
					$("#collapse80").css("height","0px");
					}
			});
			$("#n90").on("click", function() {
				if($("#collapse90").height() == 0){
					$("#n90").addClass('collapsed');
					$("#collapse90").removeClass('in');
					$("#collapse90").css("height","auto");
					}else{
					$("#n90").removeClass('collapsed');
					$("#collapse90").addClass('in');
					$("#collapse90").css("height","0px");
					}
			});
			$("#n10").on("click", function() {
				if($("#collapse10").height() == 0){	
					$("#n10").addClass('collapsed');
					$("#collapse10").removeClass('in');
					$("#collapse10").css("height","auto");
					}else{
					$("#n10").removeClass('collapsed');
					$("#collapse10").addClass('in');
					$("#collapse10").css("height","0px");
					}
			});
			$("#n0").on("click", function() {
				if($("#collapse0").height() == 0){
					$("#n0").addClass('collapsed');
					$("#collapse0").removeClass('in');
					$("#collapse0").css("height","auto");
					}else{
					$("#n0").removeClass('collapsed');
					$("#collapse0").addClass('in');
					$("#collapse0").css("height","0px");
					}
			});
			$("#nv").on("click", function() {
				if($("#collapseV").height() == 0){
					$("#nv").addClass('collapsed');
					$("#collapseV").removeClass('in');
					$("#collapseV").css("height","auto");
					}else{
					$("#nv").removeClass('collapsed');
					$("#collapseV").addClass('in');
					$("#collapseV").css("height","0px");
					}
			});*/
			
			$("#slc_year").on("change", function() {
				listProjects();
			});
			$("#selectSales_btn_ok").on("click", function() {
				checked = false;
				$($(".selectSales_inp_saleslist")).each(function() {
					if ($(this).prop("checked")) {
						checked = true;
					}
				});
				if (checked) {
					listProjects();
					$("#selectSales_modal").modal("hide");
				} else {
					alert("กรุณาเลือกอย่างน้อย 1 รายการ")
				}
			});
			$("#selectSort_btn_ok").on("click", function() {
				checked = false;
				$($(".selectSort_inp_by")).each(function() {
					if ($(this).prop("checked")) {
						checked = true;
					}
				});
				if (checked) {
					listProjects();
					$("#selectSort_modal").modal("hide");
				} else {
					alert("กรุณาเลือกอย่างน้อย 1 รายการ")
				}
			});
			document.getElementById("selectSales_modal").addEventListener ('DOMAttrModified', function() {
				if ($("#selectSales_modal").css('display') == "none") {
					$($(".selectSales_inp_saleslist")).each(function() {
						$(this).prop("checked", false);
						names = $("#selected_salesName").find("h4");
						for (i = 0; i < names.length; i++) {
							if (names[i].innerHTML == $(this).parent().find("h4")[0].innerHTML) {
								$(this).prop("checked", true);
							}
						}
					});
				}
			}, false);
			document.getElementById("selectSort_modal").addEventListener ('DOMAttrModified', function() {
				if ($("#selectSort_modal").css('display') == "none") {
					$($(".selectSort_inp_by")).each(function() {
						$(this).prop("checked", false);
						order = $("#selected_sortBy").find("h4");
						type = $("#selected_sortBy").find("h5");
						for (i = 0; i < order.length; i++) {
							if (order[i].innerHTML == $(this).parent().find("h4")[0].innerHTML) {
								$(this).prop("checked", true);
								if (type[i].innerHTML == "(A > Z)" || type[i].innerHTML == "(0 > 9)" || type[i].innerHTML == "(Old > New)") {
									$(this).closest("tr").find("input")[1].checked = true;
								} else {
									$(this).closest("tr").find("input")[2].checked = true;
								}
							}
						}
					});
				}
			}, false);
	    	$("#btn_selectSales_modal").on("click", function() {
	    		$("#selectSales_modal").modal("show");
	    	});
	    	$("#btn_selectSort_modal").on("click", function() {
	    		$("#selectSort_modal").modal("show");
	    	});
			//-----------------------------------------
	    });
		function editProject(ID,tab,slc_progress){
				setCookie("Ses_year",$("#slc_year").val());
				setCookie("Ses_saleid",slc_sales);
				setCookie("Ses_sort",check_sort);
				setCookie("Ses_tab",tab);
				setCookie("Ses_progress",slc_progress);
				window.location="newproject.php?id="+ID+"&page=index_all";
			}
		function previewForecast(ID,tab,slc_progress){
				setCookie("Ses_year",$("#slc_year").val());
				setCookie("Ses_saleid",slc_sales);
				setCookie("Ses_sort",check_sort);
				setCookie("Ses_tab",tab);
				setCookie("Ses_progress",slc_progress);
				window.location="previewforecast.php?id="+ID+"&page=index_all";
		}
		function SelectSale(i) {
			if ($(".selectSales_all"+i).is(':checked')) {
									$('[name="selectSales_inp_saleslist'+i+'"]').prop("checked", true);
								} else {
									$('[name="selectSales_inp_saleslist'+i+'"]').prop("checked", false);
								}
								if( $(".selectSales_inp_saleslist:checked").length == $(".selectSales_inp_saleslist").length){
									$(".selectSales_all").prop("checked", true);
								}else{
									$(".selectSales_all").prop("checked", false);
								}
}


	</script>

</body>
</html>