<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="Izuddin Helmi">
	<title>Site Reference:::KeyIn</title>
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css">
	<link href="js/library/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
	<link href="css/styler/style.css" rel="stylesheet" type="text/css">
	<link href="css/main.css" rel="stylesheet" type="text/css">
	<link href="js/library/syntaxHighlighter/shCore.css" rel="stylesheet" type="text/css"/>
	<link href="js/library/syntaxHighlighter/shThemeDefault.css" rel="stylesheet" type="text/css"/>
	<link href="css/styler/fontello.css" rel="stylesheet" type="text/css">
    <link href="css/styler/animation.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="js/library/jquery/jquery-ui-1.10.3.custom.min.css" />

</head>
<body cz-shortcut-listen="true" style="margin-top: 0px;">
<div id="loading" style="height: 100%; width: 100%; position: fixed; left: 0; top: 0; z-index: 1051 !important; background-color: black; filter: alpha(opacity=9); -khtml-opacity: 0.9; -moz-opacity: 0.9; opacity: 0.9;"><img src="img/loading.gif" style="width: 150px; height: 150px; position: fixed; top: 50%; left: 50%; margin: -75px 0 0 -75px;"></div>
<section class="page-header bg-color white">
	<div class="container">
		<div class="row">
			<div class="span12">
				<div class="section-header clearfix">
					<div class="pull-left">
						<h2><i class="icon-keyboard"></i>KeyIn:Site Reference</h2>
					</div>
					<div id="shortcut" class="nav-collapse pull-right">
						<ul class="shortcuts unstyled">
							<li class="current">
								<a href="index.php" class="symbol" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Home">
									<i class="icon-keyboard"></i>
								</a>
							</li>
							<li >
								<a href="search.php" class="symbol" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Search">
									<i class="icon-search-6"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="dashboard content" >
	<div class="container">
		<div class="row">			
			<div class="span12">
			  <div class="tab-content">
				<div id="site0" class="tab-pane fade active in"><!--mysite1-->
					<div class="widget-header clearfix bg-color red rounded-top">
						<div class="box-padding" style="margin-bottom:10px;text-align:right;">
							<h1 class="pull-left">Quick Update </h1>
						</div>
					</div>
					<div class="bg-color white rounded-bottom">
						<div class="box-padding" style="padding:0px 0px;">
							<table class="table table-striped" id="detail1">
								<thead>
									<tr role="row">
										<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;" width="100px">Contract</th>
										<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;"width="80px">Start</th>
										<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;"width="80px">End</th>
										<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;"width="100px">Accept Date</th>
										<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;"width="100px">วันที่หนังสือ</th>
										<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;"width="100px">Price</th>
										<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;"width="100px">Site</th>
										<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;"width="160px">Woding</th>
										<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;"width="160px">เนื้องาน</th>
										<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;"width="20px"><i class="icon-pencil-1" title="Edit"></i></th>
									</tr>
								</thead>
								<tbody></tbody>
								<tfoot>

									<tr role="row">
										<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;" width="100px"></th>
										<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;"width="80px"></th>
										<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;"width="80px"></th>
										<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;"width="100px"></th>
										<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;"width="100px"></th>
										<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;"width="100px"></th>
										<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;"width="100px"></th>
										<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;"width="160px"></th>
										<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;"width="160px"></th>
										<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;"width="20px"></th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			  </div>
			</div>
		</div>
	</div>
</section>

<div id="footer" class="bg-color dark-blue">
	<div class="container">
		<div class="box-padding">
			Copyright &copy; 2014 Site Reference
		</div>
	</div>
</div>
	<script src="js/library/jquery/jquery-1.9.1"></script>
	<script src="js/library/jquery/jquery-ui-1.10.3.custom.js"></script>
	<script src="js/library/masonry/jquery.masonry.min.js"></script>
	<script src="js/library/flexslider/jquery.flexslider-min.js"></script>
	<script src="js/library/dataTables/jquery.dataTables.min.js"></script>
	<script src="js/library/fullcalendar/fullcalendar.min.js"></script>
	<script src="js/library/fullcalendar/gcal.js"></script>
	<script src="js/library/forms/jquery.hotkeys.js"></script>
	<script src="js/library/forms/bootstrap-wysiwyg.js"></script>
	<script src="js/library/forms/picker.js"></script>
	<script src="js/library/forms/picker.date.js"></script>
	<script src="js/library/forms/picker.time.js"></script>
	<script src="js/library/forms/jquery.bootstrap.wizard.min.js"></script>
	<script src="js/library/charts/jquery.flot.js"></script>
	<script src="js/library/charts/jquery.flot.resize.js"></script>
	<script src="js/library/charts/jquery.flot.stack.js"></script>
	<script src="js/library/charts/jquery.flot.pie.min.js"></script>
	<script src="js/library/charts/jquery.sparkline.min.js"></script>
	<script src="js/library/syntaxHighlighter/shCore.js"></script>
	<script src="js/library/syntaxHighlighter/shBrushCss.js"></script>
	<script src="js/library/syntaxHighlighter/shBrushXml.js"></script>
	<script src="js/library/syntaxHighlighter/shBrushJScript.js"></script>
	<script src="js/library/jquery.number.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="js/styler/custom.js"></script>
	<script src="js/set_cookie.js"></script>


	<!--<link href="libs/bootstrap/css/bootstrap.css" rel="stylesheet">
        <script src="libs/jquery/jquery-1.8.2.min.js"></script>
        <script src="libs/bootstrap/js/bootstrap.min.js"></script>-->
        <link href="libs/prettify/prettify-bootstrap.css" rel="stylesheet">
        <!--Bootstrap-editable-->
        <link href="bootstrap-editable/css/bootstrap-editable.css" rel="stylesheet">
        <script src="bootstrap-editable/js/bootstrap-editable.min.js"></script>
        <script src="libs/prettify/prettify.js"></script>
        <script src="libs/mockjax/jquery.mockjax.js"></script>

        <script src="main.js"></script>

	<script type="text/javascript">
		var GlobalSaleID = "<?php echo $_COOKIE['Ses_ID']?>";
		var keywordHome = "";
	    $(document).ready(function() {
	    	$("#loading").hide();
	    	$('.dashboard-stat').click(function (e) {
				//
				//reset all
				$('.dashboard-stat').children("i").attr('class','icon-check-empty more');
				$('.tab-pane').hide();				

				//set this selected
				$(this).children("i").attr('class','icon-check-1 more');
				var href = $(this).attr('href').replace('#','');
				$('#'+href).fadeIn();
				$('#'+href).attr('class','tab-pane fade active in');
				e.preventDefault();
			});
			

		});
				function search(){
					//$("#loading").show();
					var Keyword = document.getElementById("txtKeyword").value;

				}
				function getSearch(json) {
            	    console.log("json:",json);	
					document.getElementById('sumResult1').innerHTML=json.items.length;
					$('#detail1 > tbody:last').empty();
					if(json.items.length== 0){
						var noresult = "<tr><td colspan='6' style='text-align:center;'><p class='lead14'>ไม่พบข้อมูล</p></td></tr>";
						$('#detail1').append(noresult);
					}else{
						for(i=0;i<json.items.length;i++)
						{
							var organizationid = '"'+json.items[i].organizationid+'"';
							var thaioganization = (json.items[i].thaioganization!= null)?json.items[i].thaioganization:"";
							var organization = (json.items[i].organization!= null)?json.items[i].organization:" ";
							var thaidepartment = (json.items[i].thaidepartment!= null)?json.items[i].thaidepartment:"";
							var department = (json.items[i].department!= null)?json.items[i].department:"";
							var thaidivision = (json.items[i].thaidivision!= null)?json.items[i].thaidivision:"";
							var division = (json.items[i].division!= null)?json.items[i].division:" ";
							
							var cols="";
							cols+="<tr><td><p class='lead14'>"+json.items[i].organizationinitial+"</p></td>";
							cols+="<td><p class='lead14'>"+thaioganization+"<br>"+organization+"</p></td>";
							cols+="<td><p class='lead14'>"+thaidepartment+"<br>"+department+"</p></td>";
							cols+="<td><p class='lead14'>"+thaidivision+"<br>"+division+"</p></td>";
							if(division.replace(" ","") != "" || thaidivision.replace(" ","") != ""){
								cols+="<td style='text-align:center'></td>";
									
							}else if(department.replace(" ","") != "" || thaidepartment.replace(" ","") != ""){
								cols+="<td style='text-align:center'><a href='javascript:addDiv("+organizationid+",1);' ><i class='i_btn icon-plus-circle iconAddDiv' ></i></a></td>";
								
							}else{
								cols+="<td style='text-align:center'><a href='javascript:addDep("+organizationid+",1);' ><i class='i_btn icon-plus-circle iconAddDep'></i></a></td>";
							}
							cols+="<td style='text-align:center'><a href='javascript:edit("+organizationid+",1);' ><i class='i_btn icon-pencil-1 iconEdit' ></i></a></td></tr>";
							$('#detail1').append(cols);
							
						}
					$('.iconAddDep').tooltip({
						placement: 'bottom',
						title : 'Add Department'
					});
					$('.iconAddDiv').tooltip({
						placement: 'bottom',
						title : 'Add Division'
					});
					$('.iconEdit').tooltip({
						placement: 'bottom',
						title : 'Edit'
					});
					}
					$("#loading").hide();
				}
								
				function edit(id,site){
					setCookie("Ses_Keyword_index",keywordHome);
					setCookie("Ses_Site",site);
					window.location = "new.php?id=" + id + "&page=home";
				}
				function addDep(id,site){
					setCookie("Ses_Keyword_index",keywordHome);
					setCookie("Ses_Site",site);
					window.location = "new.php?org=" + parseInt(id.substr(0, 4)) + "&page=home";
				}
				function addDiv(id,site){
					setCookie("Ses_Keyword_index",keywordHome);
					setCookie("Ses_Site",site);
					window.location = "new.php?org=" + parseInt(id.substr(0, 4)) + "&dep=" + parseInt(id.substr(4, 3)) + "&page=home";
				}
				
		

//------------------------------------------------- Function ------------------------------------------------
		function numFormat(x) {
				var parts = x.toString().split(".");
			    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			    return parts.join(".");
		}
//-----------------------------------------------------------------------------------------------------------
	</script>

</body>
</html>