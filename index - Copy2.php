﻿<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="Izuddin Helmi">
	<title>SFS:::Home</title>
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css">
	<link href="js/library/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
	<link href="css/styler/style.css" rel="stylesheet" type="text/css">
	<link href="css/main.css" rel="stylesheet" type="text/css">
	<link href="js/library/syntaxHighlighter/shCore.css" rel="stylesheet" type="text/css"/>
	<link href="js/library/syntaxHighlighter/shThemeDefault.css" rel="stylesheet" type="text/css"/>
	<link href="css/styler/fontello.css" rel="stylesheet" type="text/css">
    <link href="css/styler/animation.css" rel="stylesheet" type="text/css">
    <link href="css/project.css" rel="stylesheet" type="text/css">
</head>
<?php  $checkmenu = '1'; ?>
<body cz-shortcut-listen="true" style="margin-top:0px;">
<div id="loading" style="height: 100%; width: 100%; position: fixed; left: 0; top: 0; z-index: 1051 !important; background-color: black; filter: alpha(opacity=9); -khtml-opacity: 0.9; -moz-opacity: 0.9; opacity: 0.9;"><img src="img/loading.gif" style="width: 150px; height: 150px; position: fixed; top: 50%; left: 50%; margin: -75px 0 0 -75px;"></div>

<div id="loadingBlank" style="height: 100%; width: 100%; position: fixed; left: 0; top: 0; z-index: 1051 !important; background-color: black; filter: alpha(opacity=9); -khtml-opacity: 0.9; -moz-opacity: 0.9; opacity: 0.9;"></div>
<section class="page-header bg-color white">
	<div class="container">
		<div class="row">
			<div class="span12">
				<div class="section-header clearfix">
					<div class="pull-left">
						<h2><i class="icon-warehouse"></i>Home</h2>
					</div>
					<div id="shortcut" class="nav-collapse pull-right">
						<ul class="shortcuts unstyled">
							<?php include("menu.php");?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="dashboard content">
	<div class="container">
			<div class="row">
				<div class="span3">
					<div class="widget widget-year">
						<div class="widget-header clearfix">
							<span class="pull-left" style="margin-top: 10px;"><i class="icon-calendar"></i> Year</span>
							<div class="dropdown pull-right">
								<select id="slc_year" class="span1"></select>
							</div>
						</div>
						<div class="bg-color turqoise rounded-top rounded-bottom">
							<div class="box-padding aligncenter">
								<h1 id="selected_year" class="no-margin normal"></h1>
							</div>
						</div>
					</div>
					<div class="widget widget-sales">
						<div class="widget-header clearfix">
							<span class="pull-left"><i class="icon-users-1"></i> Sales</span>
							<div class="dropdown pull-right">
								<h3 style="margin:-3px 0px 0px 0px;"><i id="btn_selectSales_modal" title="Select Sales" class="i_btn icon-list-add"></i></h3>
							</div>
						</div>
						<div class="bg-color turqoise  rounded-top rounded-bottom">
							<div class="accordion" id="accordionsales">
				                <div class="">
				                  <div class="accordion-body collapse">
				                    <div class="accordion-inner">
				                    	<ul id="selected_salesName" class="unstyled"></ul>
				                    </div>
				                  </div>
				                </div>
				            </div>
						</div>						
					</div>
					<div class="widget widget-sort">
						<div class="widget-header clearfix">
							<span class="pull-left"><i class="icon-list-numbered"></i> Sort By</span>
							<div class="dropdown pull-right">
								<h3 style="margin:-3px 0px 0px 0px;"><i id="btn_selectSort_modal" title="Select Sort" class="i_btn icon-list-add"></i></h3>
							</div>
						</div>
						<div class="bg-color turqoise  rounded-top rounded-bottom">
							<div class="accordion" id="accordionsales">
				                <div class="">
				                  <div class="accordion-body collapse">
				                    <div class="accordion-inner">
				                    	<ul id="selected_sortBy" class="unstyled"></ul>
				                    </div>
				                  </div>
				                </div>
				            </div>
						</div>						
					</div>
					<!--Modal Select Sales-->
					<div class="modal hide fade" id="selectSales_modal">
						<div class="modal-header bg-color light-green aligncenter">
						    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						    <h1>Sales</h1>
						    </div>
						<div id="selectSales_ul_sales" class="modal-body"style="max-height:300px"></div>
						<div class="modal-footer alignright">
						    <a id="selectSales_btn_ok" href="#" class="btn btn-blue">OK</a>
						    <a href="#" class="btn btn-red" data-dismiss="modal">CLOSE</a>
						</div>
					</div>
					<!--Modal Select Sort-->
					<div class="modal hide fade" id="selectSort_modal">
						<div class="modal-header bg-color light-green aligncenter">
						    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						    <h1>Sort By</h1>
						    </div>
						<div class="modal-body">
							<table>
								<tbody>
									<tr>
										<td style="width: 150px;">
											<label class="checkbox"><input class="selectSort_inp_by" type="checkbox" value="Project" checked/> <h4>Project Name</h4></label>
										</td>
										<td style="width: 150px;">
											<label class="radio"><input name="inp_sortProject" type="radio" value="ASC" checked/> <h4>A > Z</h4></label>
										</td>
										<td style="width: 150px;">
											<label class="radio"><input name="inp_sortProject" type="radio" value="DESC"/> <h4>Z > A</h4></label>
										</td>
									</tr>
									<tr>
										<td>
											<label class="checkbox"><input class="selectSort_inp_by" type="checkbox" value="TimeFrameBidingDate"/> <h4>Bidding Date</h4></label>
										</td>
										<td>
											<label class="radio"><input name="inp_sortBidding" type="radio" value="ASC" checked/> <h4>Old > New</h4></label>
										</td>
										<td>
											<label class="radio"><input name="inp_sortBidding" type="radio" value="DESC"/> <h4>New > Old</h4></label>
										</td>
									</tr>
									<tr>
										<td>
											<label class="checkbox"><input class="selectSort_inp_by" type="checkbox" value="TimeFrameContractSigndate" checked/> <h4>Sign Date</h4></label>
										</td>
										<td>
											<label class="radio"><input name="inp_sortSign" type="radio" value="ASC" checked/> <h4>Old > New</h4></label>
										</td>
										<td>
											<label class="radio"><input name="inp_sortSign" type="radio" value="DESC"/> <h4>New > Old</h4></label>
										</td>
									</tr>
									<tr>
										<td>
											<label class="checkbox"><input class="selectSort_inp_by" type="checkbox" value="TargetIncome"/> <h4>Income</h4></label>
										</td>
										<td>
											<label class="radio"><input name="inp_sortIncome" type="radio" value="ASC" checked/> <h4>0 > 9</h4></label>
										</td>
										<td>
											<label class="radio"><input name="inp_sortIncome" type="radio" value="DESC"/> <h4>9 > 0</h4></label>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="modal-footer alignright">
						    <a id="selectSort_btn_ok" href="#" class="btn btn-blue">OK</a>
						    <a href="#" class="btn btn-red" data-dismiss="modal">CLOSE</a>
						</div>
					</div>
				</div>
				<div class="span9 widget">					
					<ul class="nav nav-pills tabbed">
					    <li><a id="btn_NEdu" href="#Non_Education">Non-Education</a></li>
					    <li class="active"><a id="btn_Edu" href="#Education">Education</a></li>
					</ul>
					<div class="tab-content">
					    <div class="tab-pane fade active in" id="Non_Education">
					    	<div class="widget-header clearfix">
								<span class="pull-left"><i class="icon-progress-3"></i> Progress</span>
								<label style="background-color: #DFF0D8; display: inline-block; width: 150px; text-align: center; float: right; cursor: default;">Potential>50</label>
								<label style="background-color: #FCF8E3; display: inline-block; width: 150px; text-align: center; float: right; cursor: default;">Potential=50</label>
								<label style="background-color: #F2DEDE; display: inline-block; width: 150px; text-align: center; float: right; cursor: default;">Potential&lt;50</label>
							</div>
					        <div class="bg-color white rounded-top rounded-bottom">
								<div class="accordion" id="accordion3">
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapse100')" style="cursor:hand" class="accordion-toggle collapsed clearfix" data-toggle="collapse" data-parent="#accordion3" >
					                      <h4 class="pull-left" style="margin-bottom:0px">100% Sign date</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px;color:#008A17"></p>
					                    </a>
					                  </div>
					                  <div id="collapse100" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapse90')" style="cursor:hand" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion3" >
					                      <h4 class="pull-left" style="margin-bottom:0px">90% ประกาศผลแล้วรอเซ็นสัญญา</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px"></p>
					                    </a>
					                  </div>
					                  <div id="collapse90" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapse80')" style="cursor:hand" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion3" >
					                      <h4 class="pull-left" style="margin-bottom:0px">80% ยื่นซองแล้ว</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px"></p>
					                    </a>
					                  </div>
					                  <div id="collapse80" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapse70')" style="cursor:hand" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion3" >
					                      <h4 class="pull-left" style="margin-bottom:0px">70% Bidding date</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px"></p>
					                    </a>
					                  </div>
					                  <div id="collapse70" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapse60')" style="cursor:hand" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion3" >
					                      <h4 class="pull-left" style="margin-bottom:0px">60% TOR Final</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px"></p>
					                    </a>
					                  </div>
					                  <div id="collapse60" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapse50')" style="cursor:hand" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion3" >
					                      <h4 class="pull-left" style="margin-bottom:0px">50% ได้งบประมาณ</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px"></p>
					                    </a>
					                  </div>
					                  <div id="collapse50" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapse40')" style="cursor:hand" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion3" >
					                      <h4 class="pull-left" style="margin-bottom:0px">40% ขั้นตอนการพิจารณาอนุมัติงบประมาณ</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px"></p>
					                    </a>
					                  </div>
					                  <div id="collapse40" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapse30')" style="cursor:hand" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion3" >
					                      <h4 class="pull-left" style="margin-bottom:0px">30% ร่างโครงการ (ของบประมาณ)</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px"></p>
					                    </a>
					                  </div>
					                  <div id="collapse30" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapse20')" style="cursor:hand" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion3" >
					                      <h4 class="pull-left" style="margin-bottom:0px">20% Presentation/Demo</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px"></p>
					                    </a>
					                  </div>
					                  <div id="collapse20" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapse10')" style="cursor:hand" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion3" >
					                      <h4 class="pull-left" style="margin-bottom:0px">10% Build solution</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px"></p>
					                    </a>
					                  </div>
					                  <div id="collapse10" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapse0')" style="cursor:hand" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion3" >
					                      <h4 class="pull-left" style="margin-bottom:0px">0% แพ้</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px;color:#AC193D"></p>
					                    </a>
					                  </div>
					                  <div id="collapse0" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>					                
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapseP')" style="cursor:hand" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion3" >
					                      <h4 class="pull-left" style="margin-bottom:0px">P Pending</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px;color:#f1c40f"></p>
					                    </a>
					                  </div>
					                  <div id="collapseP" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapseV')" style="cursor:hand" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion3" >
					                      <h4 class="pull-left" style="margin-bottom:0px">v ยกเลิกโครงการ</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px;color:#AC193D"></p>
					                    </a>
					                  </div>
					                  <div id="collapseV" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading" style="border-bottom:3px #0072C6 solid;">
					                    <a class=" accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion3" style="cursor: default;">
					                      <h4 class="pull-left">Summary</h4>
					                      <span id="tot_nedu" class="label label-inverse pull-right" style="margin-top:5px" title="ยกเว้น 0,V"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p id="sum_nedu" class="lead text-success pull-right" style="margin-bottom:0px;color:#0072C6;"></p>
					                    </a>
					                  </div>
					                </div>
					            </div>						
							</div>
						</div>
					    <div class="tab-pane fade active in" id="Education">
					    	<div class="widget-header clearfix">
								<span class="pull-left"><i class="icon-progress-3"></i> Progress</span>
								<label style="background-color: #DFF0D8; display: inline-block; width: 150px; text-align: center; float: right; cursor: default;">Potential>50</label>
								<label style="background-color: #FCF8E3; display: inline-block; width: 150px; text-align: center; float: right; cursor: default;">Potential=50</label>
								<label style="background-color: #F2DEDE; display: inline-block; width: 150px; text-align: center; float: right; cursor: default;">Potential&lt;50</label>
							</div>
					        <div class="bg-color white rounded-top rounded-bottom">
								<div class="accordion" id="accordion2">
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapse100e')" style="cursor:hand" class="accordion-toggle collapsed clearfix" data-toggle="collapse" data-parent="#accordion2" >
					                      <h4 class="pull-left" style="margin-bottom:0px">100% Sign date</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px;color:#008A17"></p>
					                    </a>
					                  </div>
					                  <div id="collapse100e" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapse90e')" style="cursor:hand" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion2" >
					                      <h4 class="pull-left" style="margin-bottom:0px">90% ประกาศผลแล้วรอเซ็นสัญญา</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px"></p>
					                    </a>
					                  </div>
					                  <div id="collapse90e" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapse80e')" style="cursor:hand" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion2" >
					                      <h4 class="pull-left" style="margin-bottom:0px">80% ยื่นซองแล้ว</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px"></p>
					                    </a>
					                  </div>
					                  <div id="collapse80e" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapse70e')" style="cursor:hand" class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion2" >
					                      <h4 class="pull-left" style="margin-bottom:0px">70% Bidding date</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px"></p>
					                    </a>
					                  </div>
					                  <div id="collapse70e" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapse60e')" style="cursor:hand"  class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion2" >
					                      <h4 class="pull-left" style="margin-bottom:0px">60% TOR Final</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px"></p>
					                    </a>
					                  </div>
					                  <div id="collapse60e" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapse50e')" style="cursor:hand"  class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion2" >
					                      <h4 class="pull-left" style="margin-bottom:0px">50% ได้งบประมาณ</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px"></p>
					                    </a>
					                  </div>
					                  <div id="collapse50e" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapse40e')" style="cursor:hand"  class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion2" >
					                      <h4 class="pull-left" style="margin-bottom:0px">40% ขั้นตอนการพิจารณาอนุมัติงบประมาณ</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px"></p>
					                    </a>
					                  </div>
					                  <div id="collapse40e" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapse30e')" style="cursor:hand"  class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion2" >
					                      <h4 class="pull-left" style="margin-bottom:0px">30% ร่างโครงการ (ของบประมาณ)</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px"></p>
					                    </a>
					                  </div>
					                  <div id="collapse30e" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapse20e')" style="cursor:hand"  class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion2" >
					                      <h4 class="pull-left" style="margin-bottom:0px">20% Presentation/Demo</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px"></p>
					                    </a>
					                  </div>
					                  <div id="collapse20e" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapse10e')" style="cursor:hand"  class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion2" >
					                      <h4 class="pull-left" style="margin-bottom:0px">10% Build solution</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px"></p>
					                    </a>
					                  </div>
					                  <div id="collapse10e" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapse0e')" style="cursor:hand"  class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion2" >
					                      <h4 class="pull-left" style="margin-bottom:0px">0% แพ้</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px;color:#AC193D"></p>
					                    </a>
					                  </div>
					                  <div id="collapse0e" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapsePe')" style="cursor:hand"  class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion2" >
					                      <h4 class="pull-left" style="margin-bottom:0px">P Pending</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px;color:#f1c40f"></p>
					                    </a>
					                  </div>
					                  <div id="collapsePe" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading">
					                    <a onclick="doMenu('collapseVe')" style="cursor:hand"  class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion2" >
					                      <h4 class="pull-left" style="margin-bottom:0px">v ยกเลิกโครงการ</h4>
					                      <span class="label label-inverse pull-right" style="margin-top:5px" title="Amount of projects"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p class="lead text-success pull-right" style="margin-bottom:0px;color:#AC193D"></p>
					                    </a>
					                  </div>
					                  <div id="collapseVe" class="accordion-body collapse displayBlock" style="border-left:3px #2ABF9E solid;border-right:3px #2ABF9E solid;border-bottom:2px #2ABF9E solid;">
					                    <table class="table" style="margin-bottom:0px;table-layout: fixed;">
							              <thead>
							                <tr style=" background-color: #2ABF9E; color: #FFFFFF;">
												<th style="width: 10%;">PEContract</th>
												<th style="width: 45%;">Project Name</th>
												<th style="width: 12%;">Bidding Date</th>
												<th style="width: 12%;">Sign Date</th>
												<th style="width: 12%; text-align: right;">Income</th>
												<th style="width: 8%; text-align: center;"><i class="icon-pencil-1"></i>|<i class="icon-newspaper"></i></th>
							                </tr>
							              </thead>
							              <tbody></tbody>
							            </table>
					                  </div>
					                </div>
					                <div class="accordion-group">
					                  <div class="accordion-heading" style="border-bottom:3px #0072C6 solid;">
					                    <a class=" accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion2" style="cursor: default;">
					                      <h4 class="pull-left">Summary</h4>
					                      <span id="tot_edu" class="label label-inverse pull-right" style="margin-top:5px" title="ยกเว้น 0,V"></span>
					                      <p class="lead pull-right" style="margin-bottom:0px">&nbsp;&nbsp;&nbsp;</p>
					                      <p id="sum_edu" class="lead text-success pull-right" style="margin-bottom:0px;color:#0072C6;"></p>
					                    </a>
					                  </div>
					                </div>
					            </div>						
							</div>
						</div>
					</div>				
				</div>
			</div>

			<div class="row" style="display: none;">
				<div class="span6">
					<div class="widget widget-events">
						<div class="widget-header clearfix">
							<span class="pull-left"><i class="icon-calendar-1"></i> Bidding Date</span>
							<div class="dropdown pull-right">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-cog-2"></i> <b class="caret"></b></a>
								<ul class="dropdown-menu">
				                    <li><a href="typography.html">Typography</a></li>
									<li><a href="forms.html">Forms</a></li>
									<li><a href="ui.html">UI Elements</a></li>
									<li><a href="tables.html">Tables</a></li>
									<li><a href="charts.html">Charts</a></li>
									<li><a href="calendar.html">Calendars</a></li>
				                </ul>
							</div>
						</div>
						<div class="bg-color white rounded event-item">
							<div class="box-padding narrow">
								<div class="media">
					              <a class="pull-left" href="#">
					                <img class="media-object" src="http://placehold.it/80x80/43484D/fff">
					              </a>
					              	<div class="pull-right event-date aligncenter" href="#">
					              		<h1>21</h1>
					              		<h5 class="uppercase">Dec 13</h5>
					              	</div>
					              <div class="media-body">
					                <h4 class="media-heading">Rooftop Bawlin' - Part III</h4>
					                <ul class="unstyled event-details">
					                	<li><i class="icon-clock-5 pull-left"></i> <span class="light">9:00am - 11:00am</span></li>
					                	<li><i class="icon-location-3 pull-left"></i> <span class="light">795 Folsom Ave, Suite 600, San Francisco, CA 94107</span></li>
					                	<li><i class="icon-users-2 pull-left"></i> <span class="light">270 Attendees</span></li>
					                </ul>
					                <a class="btn btn-flat btn-wide btn-turqoise">Attend</a>
					              </div>
					            </div>
							</div>
						</div>
						<div class="bg-color white rounded event-item">
							<div class="box-padding narrow">
								<div class="media">
					              <a class="pull-left" href="#">
					                <img class="media-object" src="http://placehold.it/80x80/43484D/fff">
					              </a>
					              	<div class="pull-right event-date aligncenter" href="#">
					              		<h1>30</h1>
					              		<h5 class="uppercase">June 13</h5>
					              	</div>
					              <div class="media-body">
					                <h4 class="media-heading">S&amp;D Company Retreat</h4>
					                <ul class="unstyled event-details">
					                	<li><i class="icon-clock-5 pull-left"></i> <span class="light">All day</span></li>
					                	<li><i class="icon-location-3 pull-left"></i> <span class="light">795 Folsom Ave, Suite 600, San Francisco, CA 94107</span></li>
					                	<li><i class="icon-users-2 pull-left"></i> <span class="light">18 Attendees</span></li>
					                </ul>
					                <a class="btn btn-flat btn-wide btn-turqoise">Attend</a>
					              </div>
					            </div>
							</div>
						</div>
					</div>
				</div>
				<div class="span6">
					<div class="widget widget-events">
						<div class="widget-header clearfix">
							<span class="pull-left"><i class="icon-calendar-6"></i> Contract Sign Date</span>
							<div class="dropdown pull-right">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-cog-2"></i> <b class="caret"></b></a>
								<ul class="dropdown-menu">
				                    <li><a href="typography.html">Typography</a></li>
									<li><a href="forms.html">Forms</a></li>
									<li><a href="ui.html">UI Elements</a></li>
									<li><a href="tables.html">Tables</a></li>
									<li><a href="charts.html">Charts</a></li>
									<li><a href="calendar.html">Calendars</a></li>
				                </ul>
							</div>
						</div>
						<div class="bg-color white rounded event-item">
							<div class="box-padding narrow">
								<div class="media">
					              <a class="pull-left" href="#">
					                <img class="media-object" src="http://placehold.it/80x80/43484D/fff">
					              </a>
					              	<div class="pull-right event-date aligncenter" href="#">
					              		<h1>21</h1>
					              		<h5 class="uppercase">Dec 13</h5>
					              	</div>
					              <div class="media-body">
					                <h4 class="media-heading">Rooftop Bawlin' - Part III</h4>
					                <ul class="unstyled event-details">
					                	<li><i class="icon-clock-5 pull-left"></i> <span class="light">9:00am - 11:00am</span></li>
					                	<li><i class="icon-location-3 pull-left"></i> <span class="light">795 Folsom Ave, Suite 600, San Francisco, CA 94107</span></li>
					                	<li><i class="icon-users-2 pull-left"></i> <span class="light">270 Attendees</span></li>
					                </ul>
					                <a class="btn btn-flat btn-wide btn-turqoise">Attend</a>
					              </div>
					            </div>
							</div>
						</div>
						<div class="bg-color white rounded event-item">
							<div class="box-padding narrow">
								<div class="media">
					              <a class="pull-left" href="#">
					                <img class="media-object" src="http://placehold.it/80x80/43484D/fff">
					              </a>
					              	<div class="pull-right event-date aligncenter" href="#">
					              		<h1>30</h1>
					              		<h5 class="uppercase">June 13</h5>
					              	</div>
					              <div class="media-body">
					                <h4 class="media-heading">S&amp;D Company Retreat</h4>
					                <ul class="unstyled event-details">
					                	<li><i class="icon-clock-5 pull-left"></i> <span class="light">All day</span></li>
					                	<li><i class="icon-location-3 pull-left"></i> <span class="light">795 Folsom Ave, Suite 600, San Francisco, CA 94107</span></li>
					                	<li><i class="icon-users-2 pull-left"></i> <span class="light">18 Attendees</span></li>
					                </ul>
					                <a class="btn btn-flat btn-wide btn-turqoise">Attend</a>
					              </div>
					            </div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
	</div>
</section>
<div class="modal hide fade" id="generate_modal" data-backdrop="static" style="width:700px">
	<div class="modal-header bg-color light-green aligncenter">
	    <h1>รายการที่ยังไม่ได้ E-Contract</h1>
	</div>
	<div class="modal-body" style="max-height:70px;margin-bottom:0px">
		<b>รายการที่รอ Generate E-Contract</b>
		<table class="table" style="margin-bottom:0px;table-layout: fixed;">
			<thead>
				<tr style=" background-color: #2ABF9E; color: #FFFFFF;">
					<th style="width: 50px;">No</th>
					<th style="width: 100px;">PEContract</th>
					<th style="width: 270px;">Project</th>
					<th style="width: 100px;">SignDate</th>
				</tr>
			</thead>
		</table>
	</div>

	<div class="modal-body" style="max-height:120px;padding-top:0px;">
		<table class="table" style="margin-bottom:0px;table-layout: fixed;">
			<tbody id="generate_tbody_list"></tbody>
		</table>
	</div>
	<div class="modal-body errorlog" style="max-height:70px;margin-bottom:0px">
		<b>รายการที่ Service Error</b>
		<table class="table" >
			<thead>
				<tr style=" background-color: #ac193d; color: #FFFFFF;">
					<th style="width: 50px;">No</th>
					<th style="width: 100px;">PEContract</th>
					<th style="width: 270px;">Project</th>
					<th style="width: 100px;">SignDate</th>
				</tr>
			</thead>
		</table>
	</div>

	<div class="modal-body errorlog" style="max-height:100px;padding-top:0px;">
		<table class="table" >
			<tbody id="generateerror_tbody_list"></tbody>
		</table>
	</div>
	<div class="modal-footer alignright">
	    <!--<a id="reason_btn_ok" class="btn btn-blue">OK</a>-->
	    <a class="btn btn-red" data-dismiss="modal">CLOSE</a>
	</div>
</div>
<div id="alertsuccess" class="alert alert-success" style="width:600px">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <div class="media">
        <i class="icon-ok-circled pull-left" style="font-size:60px"></i>
        <div class="media-body">
            <h2 class="media-heading">Success!</h2>
            <p class="lead">ระบบสามารถเชื่อมต่อกับ Service ของ IS ได้</p>
        </div>
    </div>
</div>
<div id="alerterror" class="alert alert-error" style="width:600px">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <div class="media">
        <i class="icon-cancel-circled-1 pull-left" style="font-size:60px"></i>
        <div class="media-body">
            <h2 class="media-heading">Error</h2>
            <p class="lead">มีปัญหาในการติดต่อกับ Service ของ IS กรุณาติดต่อผู้รับผิดชอบ</p>
        </div>
    </div>
</div>
<div id="alerterrorID" class="alert alert-error" style="width:600px;z-index:9999">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <div class="media">
        <i class="icon-cancel-circled-1 pull-left" style="font-size:60px"></i>
        <div class="media-body">
            <h2 class="media-heading">Error Cookie ID</h2>
            <p class="lead">มีปัญหาในการดึงข้อมูล ID กรุณาลอง refresh ใหม่อีกครั้ง 
            หรือ ปิดแล้วเข้าใช้งานระบบใหม่ หรือ ติดต่อผู้รับผิดชอบ</p>
            <!--<br/>
	    	<a id="refresh_page" class="btn btn-blue">Refresh</a>-->

        </div>
    </div>
</div>
<div id="footer" class="bg-color dark-blue">
	<div class="container">
		<div class="box-padding">
			Copyright &copy; 2013 Sales Forecast
		</div>
	</div>
</div>

	<script src="js/library/jquery/jquery.min.js"></script>
	<script src="js/library/jquery/jquery-ui.min.js"></script>
	<script src="js/library/masonry/jquery.masonry.min.js"></script>
	<script src="js/library/flexslider/jquery.flexslider-min.js"></script>
	<script src="js/library/dataTables/jquery.dataTables.min.js"></script>
	<script src="js/library/fullcalendar/fullcalendar.min.js"></script>
	<script src="js/library/fullcalendar/gcal.js"></script>
	<script src="js/library/forms/jquery.hotkeys.js"></script>
	<script src="js/library/forms/bootstrap-wysiwyg.js"></script>
	<script src="js/library/forms/picker.js"></script>
	<script src="js/library/forms/picker.date.js"></script>
	<script src="js/library/forms/picker.time.js"></script>
	<script src="js/library/forms/jquery.bootstrap.wizard.min.js"></script>
	<script src="js/library/charts/jquery.flot.js"></script>
	<script src="js/library/charts/jquery.flot.resize.js"></script>
	<script src="js/library/charts/jquery.flot.stack.js"></script>
	<script src="js/library/charts/jquery.flot.pie.min.js"></script>
	<script src="js/library/charts/jquery.sparkline.min.js"></script>
	<script src="js/library/syntaxHighlighter/shCore.js"></script>
	<script src="js/library/syntaxHighlighter/shBrushCss.js"></script>
	<script src="js/library/syntaxHighlighter/shBrushXml.js"></script>
	<script src="js/library/syntaxHighlighter/shBrushJScript.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="js/styler/custom.js"></script>
	<script src="js/set_cookie.js"></script>
	<script type="text/javascript" src="js/bouncebox-plugin/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="js/bouncebox-plugin/jquery.bouncebox.1.0.js"></script>
	<script type="text/javascript">
		var _flagOpen=0;
		function doMenu(item) 
		{ 
			//console.log(item +" : " + $('#'+item).css('display') );
			var ids="#"+item;
			var checkDisplay=$(ids).css('display');
			$(ids).slideToggle(100);
			if(checkDisplay == "none") 
	        {                           
	            //$('#'+item).css('display','block');
	            $(ids).removeClass( "displayNone" ).addClass( "displayBlock" );
	        }
	        else
	        {
	            //$('#'+item).css('display','none');	
	            $(ids).removeClass( "displayBlock" ).addClass( "displayNone" );                        	
	        }
		} 
		var check_sort = "";
		var slc_sales = "";
		//console.log("event 1");
	    $(document).ready(function() {
	    	$('#alertsuccess').bounceBox();
	    	$('#alerterror').bounceBox();
	    	$('#alerterrorID').bounceBox();

		    /* Listening for the click event and toggling the box: */
		    /* When the box is clicked, hide it: */
		    $('#alertsuccess').click(function(){
		        $('#alertsuccess').bounceBoxHide();
		    });
		    $('#alerterror').click(function(){
		        $('#alerterror').bounceBoxHide();
		    });
		    $('#alerterrorID').click(function(){		    	
				$("#loadingBlank").hide();
		        $('#alerterrorID').bounceBoxHide();
		        window.location.reload();
		    });
			//-------------------------------- Function ------------------------------------------------
			function checkService()
			{
				$.ajax({
					cache: false,
						type: "POST",
						dataType: "json",
						url: "AJAX/IS_checkservice.php",
						success: function(jsonObject) {

            	//var jsonString = /{.+}/.exec(jsonPTS)[0];        						
						//var jsonObject = jQuery.parseJSON(jsonString);
							//console.log("jsonObject :",jsonObject);
							if (jsonObject.strReturn.ErrMsg == "" ) 
							{

								//console.log("jsonObject.strReturn");
		        		//$('#alertsuccess').bounceBoxShow();
							}
							else
							{
		        		$('#alerterror').bounceBoxShow();
							}
							//$("#loading").hide();
						},
						error: function() {
		        	$('#alerterror').bounceBoxShow();
							//$("#loading").hide();
						}
					});
			}
			function checkid()
			{
				var _loginID = "<?php echo $_COOKIE['Ses_ID']?>";
				//alert(_loginID );
				if(_loginID=="" || _loginID==" ")
				{					
					$("#loadingBlank").show();
		        	$('#alerterrorID').bounceBoxShow();
				}
				else
				{

					$("#loadingBlank").hide();					
		        }	//$('#alerterrorID').bounceBoxShow();
			}
			function flagGenerate()
			{
				var _loginID = "<?php echo $_COOKIE['Ses_ID']?>";
				$.ajax({
						cache: false,
						type: "POST",
						dataType: "json",
						url: "AJAX/LoadFlagGenerate.php",
						data:{saleid: _loginID},
						success: function(jsonObject) {
							console.log("flagGenerate ",jsonObject.length);
							var i=1;
							if(jsonObject.length>0)
							{
								$.each(jsonObject, function() {
					    		var item = this;
					    		tr = $('<tr style="cursor:pointer" ><td width="50px">' + i + '</td><td width="100px">' + item[2] + '</td><td width="370px">' + item[1] + '</td><td width="100px">' + item[4] + '</td></tr>');
					    		tr.on("click", function() {
						    			//console.log("progress on tr ",);
						    		var ID=item[7];
						    		var tab=1;
						    		var slc_progress=item[3];
						    		if(item[6]=='Education')
						    		{
						    			tab=2;
						    		}
					    			editProject(ID,tab,slc_progress);
					    		});
					    		$("#generate_tbody_list").append(tr);
					    		i++;
	    					});
	    					$("#generate_modal").modal('show');
	    					_flagOpen=1;
							}
							else
							{
								$("#generate_modal").modal('hide');
								_flagOpen=0;
							}

							flagGenerateError();
            
						},
						error: function() {
								
						}
					});
			}

			function flagGenerateError()
			{
				var _loginID = "<?php echo $_COOKIE['Ses_ID']?>";
				$.ajax({
						cache: false,
						type: "POST",
						dataType: "json",
						url: "AJAX/LoadFlagGenerateError.php",
						data:{saleid: _loginID},
						success: function(jsonObject) {
							console.log("flagGenerateError ",jsonObject.length);
							var i=1;
							if(jsonObject.length>0)
							{
								$.each(jsonObject, function() {
					    		var item = this;
					    		tr = $('<tr style="cursor:pointer" ><td width="50px">' + i + '</td><td width="100px">' + item[2] + '</td><td width="370px">' + item[1] + '</td><td width="100px">' + item[4] + '</td></tr>');
					    		tr.on("click", function() {
						    			//console.log("progress on tr ",);
						    		var ID=item[7];
						    		var tab=1;
						    		var slc_progress=item[3];
						    		if(item[6]=='Education')
						    		{
						    			tab=2;
						    		}
					    			editProject(ID,tab,slc_progress);
					    		});
					    		$("#generateerror_tbody_list").append(tr);
					    		i++;
	    					});
	    					$("#generate_modal").modal('show');
							}
							else
							{
								$(".errorlog").css("display","none");
								
								if(_flagOpen==0)
								{	
									$("#generate_modal").modal('hide');
								}
							}
            
						},
						error: function() {
						
						}
					});
			}
			function numFormat(x) {
				var parts = x.toString().split(".");
			    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			    return parts.join(".");
			}
			function listProjects() {

				console.log("event 3");
				$("#loading").show();
				slc_sales = ""
				slc_sort = "";
				$("#selected_salesName").empty();
				$("#selected_sortBy").empty();
				$("#selected_year").text($("#slc_year").val());
				$($(".selectSales_inp_saleslist")).each(function() {
					if ($(this).prop("checked")) {
						slc_sales += $(this).val() + ",";
						$("#selected_salesName").append('<li class="clearfix"><p class="text-left"><h4>' + $(this).parent().find("h4")[0].innerHTML + '</h4></p></li>');
					}
				});
				
				check_order = '';
				check_sort = "";
				$($(".selectSort_inp_by")).each(function() {
					if ($(this).prop("checked")) {
						slc_order = " DESC";
						check_order = "2";
						slc_orderTxt = "(" + $(this).closest("tr").find("h4")[2].innerHTML + ")";
						if ($(this).closest("tr").find("input")[1].checked) {
							slc_order = " ASC";
							check_order = "1";
							slc_orderTxt = "(" + $(this).closest("tr").find("h4")[1].innerHTML + ")";
						}
						if ($(this).val() == "TargetIncome" || $(this).val() == "Project") {
							slc_sort += $(this).val() + slc_order + ", ";
						} else {
							slc_sort += "YEAR(" + $(this).val() + ")" + slc_order + ", " + "MONTH(" + $(this).val() + ")" + slc_order + ", " + "DAY(" + $(this).val() + ")" + slc_order + ", ";
						}
						check_sort += check_order + $(this).val() + ",";
						$("#selected_sortBy").append('<li ><p ><h4 style="display: inline-block">' + $(this).parent().find("h4")[0].innerHTML + '</h4> <span class="pull-right"><h4 style="display: inline-block">' + slc_orderTxt + '</h4></span></p></li>');
					}
				});
				slc_sales = slc_sales.substr(0, (slc_sales.length - 1));
				slc_sort = slc_sort.substr(0, (slc_sort.length - 2));
				check_sort = check_sort.substr(0, (check_sort.length - 1));
				
				//console.log('year ',$("#slc_year").val());
				console.log('saleid ',slc_sales);
				//console.log('slc_sort ',slc_sort);
				
				$.ajax({ 
					cache: false,
					type: 'POST',
					dataType: "json",
			        url: "Ajax/SV_Select_Project_sql.php",
			        data: {
			        	year: $("#slc_year").val(),
			        	saleid: slc_sales,
			        	orderby: slc_sort
			        },
			        success: function (item) {
		        		//console.log(json);
		        		//item = json;
			        	progList = ["100", "90", "80", "70", "60", "50", "40", "30", "20", "10", "0", "P", "v"];
			        	i = jsi = sum = sumi = 0;
			        	$($("#Non_Education").find("tbody")).each(function() {
			        		$(this).empty();
			        		total = totalItems = 0
			        		while (jsi < item.length) 
			        		{
			        			var ID=item[jsi][0];
			        			var PEContractNo=item[jsi][1];
			        			var Project=item[jsi][2];
			        			var Education=item[jsi][3];
			        			var Progress=item[jsi][4];
			        			var Potential=item[jsi][5];
			        			var TargetIncome=item[jsi][6];
			        			var TargetBudget=item[jsi][7];
			        			var TimeFrameBidingDate=item[jsi][8];
			        			var TimeFrameContractSigndate=item[jsi][9];
			        			var SaleID=item[jsi][10];
			        			var SaleRepresentative=item[jsi][11];
								//console.log('items[jsi].Education ',items[jsi].Education);
			        			if (Education == "1") {
									//console.log('items[jsi].Education ',items[jsi].Education);
			        				jsi++;
			        				continue;
		        				}
			        			if (Progress != progList[i]) break;
										var val_Progress = Progress;
										if(Progress == 'v'){
											val_Progress = 111;
										}
										else if(Progress == 'P'){
											val_Progress = 444;
										}
			        			parseInt(Potential) < 50 ? trCls = "error" : parseInt(Potential) == 50 ? trCls = "warning" : trCls = "success";
			        			btn_edit = _loginID == SaleID ? '<a href="javascript:editProject('+ parseInt(ID) +',1,'+ val_Progress +');" title="Edit"><i class="i_btn icon-pencil-1" style="color: #000000;"></i></a>' : '<a href="javascript:previewForecast('+ parseInt(ID) +',1,'+ val_Progress +');" title="Preview"><i class="i_btn icon-newspaper" style="color: #000000;"></i></a>';
			        			$(this).append('<tr class="' + trCls + '" title="Sales: ' + SaleRepresentative + ', Potential: ' + Potential + '"><td>' + PEContractNo + '</td><td>' + Project + '</td><td>' + TimeFrameBidingDate + '</td><td>' + TimeFrameContractSigndate + '</td><td class="td_money">' + numFormat(parseFloat(TargetIncome).toFixed(2)) + '</td><td style="text-align: center;">' + btn_edit + '</td></tr>');
			        			total += parseFloat(TargetIncome);
			        			totalItems++;
			        			jsi++;
			        		}
			        		$(this).closest(".accordion-group").find(".text-success")[0].innerHTML = numFormat(total.toFixed(2));
			        		$(this).closest(".accordion-group").find(".label-inverse")[0].innerHTML = totalItems;
			        		if (i <= 9) {
				        		sum += total;
				        		sumi += totalItems;
		        			}
			        		i++;
			        	});
								$("#sum_nedu").text(numFormat(sum.toFixed(2)));
								$("#tot_nedu").text(sumi);
			        	i = jsi = sum = sumi = 0;
			        	$($("#Education").find("tbody")).each(function() {
			        		$(this).empty();
			        		total = totalItems = 0
			        		while (jsi < item.length) {
			        			var ID=item[jsi][0];
			        			var PEContractNo=item[jsi][1];
			        			var Project=item[jsi][2];
			        			var Education=item[jsi][3];
			        			var Progress=item[jsi][4];
			        			var Potential=item[jsi][5];
			        			var TargetIncome=item[jsi][6];
			        			var TargetBudget=item[jsi][7];
			        			var TimeFrameBidingDate=item[jsi][8];
			        			var TimeFrameContractSigndate=item[jsi][9];
			        			var SaleID=item[jsi][10];
			        			var SaleRepresentative=item[jsi][11];
			        			if (Education == "0") {
			        				jsi++;
			        				continue;
		        				}
			        			if (Progress != progList[i]) break;
										var val_Progress = Progress;
										if(Progress == 'v'){
											val_Progress = 111;
										}
										else if(Progress == 'P'){
											val_Progress = 444;
										}
			        			parseInt(Potential) < 50 ? trCls = "error" : parseInt(Potential) == 50 ? trCls = "warning" : trCls = "success";
			        			btn_edit = _loginID == SaleID ? '<a href="javascript:editProject('+ parseInt(ID) +',2,'+ val_Progress +');" title="Edit"><i class="i_btn icon-pencil-1" style="color: #000000;"></i></a>' : '<a href="javascript:previewForecast('+ parseInt(ID) +',2,'+ val_Progress +');" title="Preview"><i class="i_btn icon-newspaper" style="color: #000000;"></i></a>';
			        			$(this).append('<tr class="' + trCls + '" title="Sales: ' + SaleRepresentative + ', Potential: ' + Potential + '"><td>' + PEContractNo + '</td><td>' + Project + '</td><td>' + TimeFrameBidingDate + '</td><td>' + TimeFrameContractSigndate + '</td><td class="td_money">' + numFormat(parseFloat(TargetIncome).toFixed(2)) + '</td><td style="text-align: center;">' + btn_edit + '</td></tr>');
			        			total += parseFloat(TargetIncome);
			        			totalItems++;
			        			jsi++;
			        		}
			        		$(this).closest(".accordion-group").find(".text-success")[0].innerHTML = numFormat(total.toFixed(2));
			        		$(this).closest(".accordion-group").find(".label-inverse")[0].innerHTML = totalItems;
		        			if (i <= 9) {
				        		sum += total;
				        		sumi += totalItems;
		        			}
			        		i++;
			        	});
						$("#sum_edu").text(numFormat(sum.toFixed(2)));
						$("#tot_edu").text(sumi);
						funcDisplayProgress_Load();
						$("#loading").hide();
			        },
					error: function() {
						alert("เกิดปัญหาในการค้นหากรุณาลองใหม่อีกครั้ง");
						funcDisplayProgress_Load();
						$("#loading").hide();
					}
			    });
			}
			
			function funcDisplayProgress_Init()
			{
				console.log("funcDisplayProgress_Init");
				//$(".displayBlock").removeClass("displayBlock").addClass("displayNone");
			}
			function funcDisplayProgress_Load()
			{
				console.log("funcDisplayProgress_Load");
				//$(".displayNone").removeClass("displayNone").addClass("displayBlock");
				$(".displayBlock").removeClass("displayBlock").addClass("displayNone");

			}
			//----------------------
			//------- Initial ------
			//----------------------
			funcDisplayProgress_Init();
			var ckYear = getCookie("Ses_year");
			var ckSale = getCookie("Ses_saleid");
			var ckSort = getCookie("Ses_sort");
			var ckTab = getCookie("Ses_tab");
			var ckProgress = getCookie("Ses_progress");
			//console.log('ckYear ',ckYear);
			//console.log('ckSale ',ckSale);
			//console.log('ckSort ',ckSort);
			//console.log('ckTab ',ckTab);
			//console.log('ckProgress ',ckProgress);
			deleteCookie("Ses_year");
			deleteCookie("Ses_saleid");
			deleteCookie("Ses_sort");
			deleteCookie("Ses_tab");
			deleteCookie("Ses_progress");
			var _loginID = "<?php echo $_COOKIE['Ses_ID']?>";
			
			$("#btn_Edu").click();
			$("#btn_NEdu").click();
			if (ckTab!=null && ckTab!="" && ckTab == "2"){
				$("#btn_Edu").click();
			}
			if (ckProgress!=null && ckProgress!=""){
				if(ckTab == "2"){
					switch(ckProgress)
					{
					case '100':
						doMenu("collapse100e");
						break;
					case '90':						
						doMenu("collapse90e");
						break;
					case '80':	
						doMenu("collapse80e");
						break;
					case '70':	
						doMenu("collapse70e");
						break;
					case '60':	
						doMenu("collapse60e");
						break;
					case '50':	
						doMenu("collapse50e");
						break;
					case '40':	
						doMenu("collapse40e");
						break;
					case '30':	
						doMenu("collapse30e");
						break;
					case '20':	
						doMenu("collapse20e");
						break;
					case '10':	
						doMenu("collapse10e");
						break;
					case '0':	
						doMenu("collapse0e");
						break;
					case 'P':	
						doMenu("collapsePe");
						break;
					default:	
						doMenu("collapseVe");
					}
				}else{
					switch(ckProgress)
					{
					case '100':	
						doMenu("collapse100");
						break;
					case '90':
						doMenu("collapse90");
						break;
					case '80':
						doMenu("collapse80");
						break;
					case '70':
						doMenu("collapse70");
						break;
					case '60':
						doMenu("collapse60");
						break;
					case '50':
						doMenu("collapse50");
						break;
					case '40':
						doMenu("collapse40");
						break;
					case '30':
						doMenu("collapse30");
						break;
					case '20':
						doMenu("collapse20");
						break;
					case '10':
						doMenu("collapse10");
						break;
					case '0':
						doMenu("collapse0");
						break;
					case 'P':	
						doMenu("collapseP");
						break;
					default:
						doMenu("collapseV");
					}
				}
			}
			
			if (ckSort!=null && ckSort!=""){
				var arr_ckSort = new Array();
				arr_ckSort = ckSort.split(",");
				$($(".selectSort_inp_by")).each(function() {
					$(this).prop('checked',false);
					for (var i=0;i<arr_ckSort.length;i++){
						str_sort = arr_ckSort[i].substr(1, (arr_ckSort[i].length - 1));
						str_order = arr_ckSort[i].substr(0, 1);
						if ($(this).val() == str_sort){
							$(this).prop('checked',true); 
							if(str_order == '2'){
								$(this).closest("tr").find("input")[1].checked = false;
								$(this).closest("tr").find("input")[2].checked = true;
							}else{
								$(this).closest("tr").find("input")[1].checked = true;
								$(this).closest("tr").find("input")[2].checked = false;
							}
						}	
					}
				
				});
			}
			$.ajax({ cache: false,
				type: "POST",
				dataType: "json",
				url: "AJAX/select_year.php",
		        success: function (json) {
					
		        	 $.each(json, function() {
						var item = this;
		        		$("#slc_year").append("<option value='" + item[0] + "'>" + item[0] + "</option>");
					});
					$("#slc_year").val((new Date).getFullYear());
					///////////////////select_saleup
					
					$.ajax({
						type: 'POST',
						cache: false,
						dataType: "json",
						url: "Ajax/SV_Select_saleup.php",
						data: {
							saleid: _loginID
						},
						success: function (json) {	
													
						if(json.length > 1){
							$("#selectSales_ul_sales").append('<label class="checkbox"><input class="selectSales_all" type="checkbox"/><h4>All</h4></label>');	
						}
							if (ckSale!=null && ckSale!=""){
								var arr_ckSale = new Array();
								arr_ckSale = ckSale.split(",");
							}
							$(json).each(function() {
								var item = this;
								if (ckSale!=null && ckSale!=""){
									checked = "";
									for (var i=0;i<arr_ckSale.length;i++)
									{ 
										if(item[0] == arr_ckSale[i]){
											checked = "checked";
										}
									}
								}else{
									item[0] == _loginID ? checked = "checked" : checked = "";
								}
								$("#selectSales_ul_sales").append('<label class="checkbox"><input class="selectSales_inp_saleslist" type="checkbox" value="' + item[0] + '" ' + checked + '/> <h4>' + item[1] + '</h4></label>');
							});
							
							$('.selectSales_all').on('click',function(){
								if ($(".selectSales_all").is(':checked')) {
									$(".selectSales_inp_saleslist").prop("checked", true);
								} else {
									$(".selectSales_inp_saleslist").prop("checked", false);
								}
							});
							$('.selectSales_inp_saleslist').on('click',function(){
								if( $(".selectSales_inp_saleslist:checked").length == $(".selectSales_inp_saleslist").length){
									$(".selectSales_all").prop("checked", true);
								}else{
									$(".selectSales_all").prop("checked", false);
								}
							});
							if (ckYear!=null && ckYear!=""){
								$("#slc_year").val(ckYear);
							}
							listProjects();
						},
						error: function() {
        				
							alert("เกิดปัญหาในการโหลดข้อมูลกรุณารีเฟรชหน้าเว็บอีกครั้ง");
							$("#loading").hide();
						}
					});
		        },
				error: function() {
					alert("เกิดปัญหาในการโหลดข้อมูลกรุณารีเฟรชหน้าเว็บอีกครั้ง");
					$("#loading").hide();
				}
		    });
			
			//---------------------------------------------------------------------------------------
			//----------------------------------- Event ---------------------------------------------
			//---------------------------------------------------------------------------------------
			console.log("Load 1");
			checkService();
			checkid();
			flagGenerate();
			
			$("#slc_year").on("change", function() {
				listProjects();
			});
			$("#selectSales_btn_ok").on("click", function() {
				checked = false;
				$($(".selectSales_inp_saleslist")).each(function() {
					if ($(this).prop("checked")) {
						checked = true;
					}
				});
				if (checked) {
					listProjects();
					$("#selectSales_modal").modal("hide");
				} else {
					alert("กรุณาเลือกอย่างน้อย 1 รายการ")
				}
			});
			$("#selectSort_btn_ok").on("click", function() {
				checked = false;
				$($(".selectSort_inp_by")).each(function() {
					if ($(this).prop("checked")) {
						checked = true;
					}
				});
				if (checked) {
					listProjects();
					$("#selectSort_modal").modal("hide");
				} else {
					alert("กรุณาเลือกอย่างน้อย 1 รายการ")
				}
			});
			$("#btn_NEdu").on("click", function() {
				console.log("btn_NEdu");
				$("#Non_Education").addClass("tab-pane fade active in");
				$("#Education").addClass("tab-pane fade");
			});
			$("#btn_Edu").on("click", function() {
				console.log("btn_Edu");
				$("#Education").addClass("tab-pane fade active in");
				$("#Non_Education").addClass("tab-pane fade");
			});
			document.getElementById("selectSales_modal").addEventListener ('DOMAttrModified', function() {
				if ($("#selectSales_modal").css('display') == "none") {
					$($(".selectSales_inp_saleslist")).each(function() {
						$(this).prop("checked", false);
						names = $("#selected_salesName").find("h4");
						for (i = 0; i < names.length; i++) {
							if (names[i].innerHTML == $(this).parent().find("h4")[0].innerHTML) {
								$(this).prop("checked", true);
							}
						}
					});
				}
			}, false);
			document.getElementById("selectSort_modal").addEventListener ('DOMAttrModified', function() {
				if ($("#selectSort_modal").css('display') == "none") {
					$($(".selectSort_inp_by")).each(function() {
						$(this).prop("checked", false);
						order = $("#selected_sortBy").find("h4");
						type = $("#selected_sortBy").find("h5");
						for (i = 0; i < order.length; i++) {
							if (order[i].innerHTML == $(this).parent().find("h4")[0].innerHTML) {
								$(this).prop("checked", true);
								if (type[i].innerHTML == "(A > Z)" || type[i].innerHTML == "(0 > 9)" || type[i].innerHTML == "(Old > New)") {
									$(this).closest("tr").find("input")[1].checked = true;
								} else {
									$(this).closest("tr").find("input")[2].checked = true;
								}
							}
						}
					});
				}
			}, false);
	    	$("#btn_selectSales_modal").on("click", function() {
	    		$("#selectSales_modal").modal("show");
	    	});
	    	$("#btn_selectSort_modal").on("click", function() {
	    		$("#selectSort_modal").modal("show");
	    	});
			//----------------------------------------------------------------------------------------
	    });
		function editProject(ID,tab,slc_progress){
				setCookie("Ses_year",$("#slc_year").val());
				setCookie("Ses_saleid",slc_sales);
				setCookie("Ses_sort",check_sort);
				setCookie("Ses_tab",tab);
				setCookie("Ses_progress",slc_progress);
				window.location="newproject.php?id="+ID+"&page=index";
			}
		function previewForecast(ID,tab,slc_progress){
				setCookie("Ses_year",$("#slc_year").val());
				setCookie("Ses_saleid",slc_sales);
				setCookie("Ses_sort",check_sort);
				setCookie("Ses_tab",tab);
				setCookie("Ses_progress",slc_progress);
				window.location="previewforecast.php?id="+ID+"&page=index";
		}
	</script>

</body>
</html>