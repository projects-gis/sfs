﻿<!DOCTYPE html>
<html lang="en">
	
<!-- Mirrored from bootstrapstyler.com/preview/_/flatdashboard/ui.html by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 18 Oct 2013 07:02:36 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
		<meta charset="utf-8">
		<title>SFS:::Export->Monthly</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="Izuddin Helmi">

		<!-- Stylesheets -->
		<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
		<link href="js/library/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
		<link href="css/styler/style.css" rel="stylesheet">
		<link href="js/library/syntaxHighlighter/shCore.css" rel="stylesheet" type="text/css" />
		<link href="js/library/syntaxHighlighter/shThemeDefault.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="css/styler/fontello.css">
	    <link rel="stylesheet" href="css/styler/animation.css"><!--[if IE 7]>
	    <link rel="stylesheet" href="css/styler/fontello-ie7.css"><![endif]-->
	    

		<!-- Custom Fonts -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300,500' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

		<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
		  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	</head>
<?php 
$checkmenu = '5';
$menuExport = '1';
?>
<body cz-shortcut-listen="true" style="margin-top:0px;">
<div id="loading" style="height: 100%; width: 100%; position: fixed; left: 0; top: 0; z-index: 1051 !important; background-color: black; filter: alpha(opacity=9); -khtml-opacity: 0.9; -moz-opacity: 0.9; opacity: 0.9;"><img src="img/loading.gif" style="width: 150px; height: 150px; position: fixed; top: 50%; left: 50%; margin: -75px 0 0 -75px;"></div>
<form name="frmexport"  action="ExportReport.php" method="POST">	
<input type="hidden" name="Progress2" value="100,90,80,70,60,50,40,30,20,10">
<input type="hidden" name="saleid" value="<?php echo $_COOKIE['Ses_ID']?>">
<input type="hidden" name="SpecialProject" value="0">
<input type="hidden" name="BidingYear" value="<?php echo date('Y')?>">
<input type="hidden" name="where" value="TimeFrameContractSigndate">
<section class="page-header bg-color white">
	<div class="container">
		<div class="row">
			<div class="span12">
				<div class="section-header clearfix">
					<div class="pull-left">
						<h2><i class="icon-export-4"></i>Export</h2>
					</div>
					<div id="shortcut" class="nav-collapse pull-right"><ul class="shortcuts unstyled">
						<?php include("menu.php");?>
					</ul>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="content">
	<div class="page-tab">
		<div class="page-tab-header">
			<div class="container">
				<ul class="nav nav-tabs clearfix" id="socialTab">
					<?php include("menu_export.php");?>	
				</ul>
			</div>
		</div>
		<div class="tab-content">
			<div class="tab-pane fade active in container" id="monthly">
				<div class="widget bg-color turqoise rounded">
					<div class="box-padding">
						<h1>Monthly</h1>
						<p class="lead">All Project </p>
					</div>
				</div>
				<div class="row">
					<div class="span3 widget widget-option">
						<div class="profile-head bg-color dark-blue rounded-top">
							<div class="box-padding" style="text-align:center">
									<h3 class="normal"><i class="icon-calendar"></i>Year</h3>
									<span class=" pull-left" style="margin-left:-10px"></span>
							</div>
						</div>
						<div class="bg-color white rounded-bottom">
								<ul class="menu unstyled">
									<li ><p>
									  	<input type="radio"  class="optionsYear1" id="optionsRadios1" name="rdoYear1" value="TimeFrameBidingDate" style="margin-top:-2px">
									  	 Bidding Date <span class=" pull-right" style="margin-top:-8px"><select id="slc_year" name="slc_year" class="span1" style="width:80px;"></select></span></p></li>
									<li class="current"><p>
									  	<input type="radio" checked class="optionsYear2" id="optionsRadios1" name="rdoYear1" style="margin-top:-2px" value="TimeFrameContractSigndate">
									  	 Signdate <span class="pull-right"style="margin-top:-8px"><select id="slc_year_sign" name="slc_year_sign" class="span1" style="width:80px;"></select></span></p></li>
								</ul>
						</div>
						<div class="profile-head bg-color dark-blue rounded-top">
							<div class="box-padding" style="text-align:center">
									<h3 class="normal"><i class="icon-th-thumb-empty"></i>Option</h3>
							</div>
						</div>
						<div class="bg-color white rounded-bottom">
								<ul class="menu unstyled">
									<li><p class="sales">
									  	<input type="checkbox" value="1" style="margin-top:-2px" class="chkSpecial"> Mega Project<span class="pull-right"style="margin-top:-8px"></span></p></li>
								</ul>
						</div>
						<div style="text-align:center"><a class="btn btn-blue btn-large" id="btnExport" style="width:190px" >Export</a></div>
						<div id="showWarning" class="alert alert-error hide" style="margin-top:10px;margin-bottom:-10px;">
							<button type="button" class="close" data-dismiss="alert">×</button>
								<i class="icon-attention-3 pull-left" style="font-size:20px;margin-top:-3px;"></i>
								<div class="media-body">
									<strong>กรุณาเลือก Progress และ Sale ด้วย</strong>
								</div>
						</div>
						<div id="warningGraph" class="alert alert-block hide" style="margin-top:10px;margin-bottom:-10px;">
							<button type="button" class="close" data-dismiss="alert">×</button>
							<i class="icon-info-circle pull-left" style="font-size:25px;margin-top:-7px;"></i>
							<div class="media-body">
								<strong>ไม่พบข้อมูล</strong>
							</div>
						</div>
						
						<div class="tab-content" id="Graph2" style="margin-top:10px;margin-bottom:-10px;">
								<div id="containerPie2" style="min-width: 270px; height: 200px; margin: 0 auto;display:none" ></div>
								<div id="containerBar2" style="min-width: 27px; height: 200px; margin: 0 auto"></div>
						</div>
					</div>
					<div class="span5">
						<div class="widget widget-profile">
							<div class="profile-head bg-color dark-blue rounded-top">
								<div class="box-padding">
									<h3 class="normal"><i class="icon-progress-3"></i>Progress</h3>
									<span class=" pull-right" style="margin-right:-10px;margin-top:-20px"><input type="checkbox" class="progressAll" title="Select all"></span>
								</div>
							</div>

							<div class="bg-color white rounded-bottom">
								<ul class="menu unstyled">
									<li class="current"><p>100% Sign date<span class=" pull-right"><input type="checkbox" name="chkprog" value="100" class="progress" checked></span></p></li>
									<li class="current"><p>90% <b>ประกาศผลแล้วรอเซ็นสัญญา</b><span class="pull-right"><input type="checkbox" name="chkprog" value="90" class="progress" checked></span></p></li>
									<li class="current"><p>80% <b>ยื่นซองแล้ว</b><span class="pull-right"><input type="checkbox" name="chkprog" value="80" class="progress" checked ></span></p></li>
									<li class="current"><p>70% Bidding date<span class="pull-right"><input type="checkbox" name="chkprog" value="70" class="progress" checked></span></p></li>
									<li class="current"><p>60% TOR Final<span class="pull-right"><input type="checkbox" name="chkprog" value="60" class="progress" checked></span></p></li>
									<li class="current"><p>50% <b>ได้งบประมาณ</b><span class="pull-right"><input type="checkbox" name="chkprog" value="50" class="progress" checked></span></p></li>
									<li class="current"><p>40% <b>ขั้นตอนการพิจารณาอนุมัติงบประมาณ</b><span class="pull-right"><input type="checkbox" name="chkprog" value="40" class="progress" checked></span></p></li>
									<li class="current"><p>30% <b>ร่างโครงการ (ของบประมาณ)</b><span class="pull-right"><input type="checkbox" name="chkprog" value="30" class="progress" checked></span></p></li>
									<li class="current"><p>20% Presentation/Demo<span class="pull-right"><input type="checkbox" name="chkprog" value="20" class="progress" checked></span></p></li>
									<li class="current"><p>10% Build solution<span class="pull-right"><input type="checkbox" name="chkprog" value="10" class="progress"  checked></span></p></li>
									<li><p>0% <b>แพ้</b><span class="pull-right"><input type="checkbox" name="chkprog" value="0" class="progress" ></span></p></li>
									<li><p>V <b>ยกเลิกโครงการ</b><span class="pull-right"><input type="checkbox" name="chkprog" value="v" class="progress" ></span></p></li>
									<li><p>P <b>Pendding</b><span class="pull-right"><input type="checkbox" name="chkprog" value="p" class="progress" ></span></p></li>
									
								</ul>
							</div>

						</div>
					</div>

					<div class="span4 widget widget-option">
						<div class="profile-head bg-color dark-blue rounded-top">
							<div class="box-padding" style="text-align:center">
									<h3 class="normal"><i class="icon-users-1"></i>Sales</h3>
									<span class=" pull-left" style="margin-left:-10px;margin-top:-20px"><input type="checkbox" class="salesAll" title="Select all"></span>
							</div>
						</div>

						<div class="bg-color white rounded-bottom">
								<ul class="menu unstyled" id="listsaleName"></ul>
						</div>
						<div class="tab-content" id="Graph">
								<div id="containerPie" style="min-width: 370px; height: 300px; margin: 0 auto;display:none" ></div>
								<div id="containerBar" style="min-width: 370px; height: 300px; margin: 0 auto"></div>
								<!--<div style="text-align:right" ><a style="margin:10px 0px;" class="btn btn-blue" id="btnSwitch" >Switch Graph</a></div>-->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
		<div id="footer" class="bg-color dark-blue">
			<div class="container">
				<div class="box-padding">Copyright &copy; 2013 Sales Forecast</div>
			</div>
		</div>
		<!-- Javascript -->
		<!-- JS:jquery-->
		<!--<script src="../../../../ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>-->
  		<script src="js/library/jquery/jquery.min.js"></script>
		<script src="js/library/jquery/jquery-ui.min.js"></script>

		<!-- JS:masonry-->
		<script src="js/library/masonry/jquery.masonry.min.js"></script>

		<!-- JS:flexslider-->
		<script src="js/library/flexslider/jquery.flexslider-min.js"></script>

		<!-- JS:tables-->
		<script src="js/library/dataTables/jquery.dataTables.min.js"></script>

		<!-- JS:calendar-->
		<script src="js/library/fullcalendar/fullcalendar.min.js"></script>
		<script src="js/library/fullcalendar/gcal.js"></script>

		<!-- JS:forms-->
		<script src="js/library/forms/jquery.hotkeys.js"></script>
		<script src="js/library/forms/bootstrap-wysiwyg.js"></script>
		<script src="js/library/forms/picker.js"></script>
		<script src="js/library/forms/picker.date.js"></script>
		<script src="js/library/forms/picker.time.js"></script>
		<script src="js/library/forms/jquery.bootstrap.wizard.min.js"></script>

		<!-- JS:charts-->
		<script src="js/library/charts/jquery.flot.js"></script>
		<script src="js/library/charts/jquery.flot.resize.js"></script>
		<script src="js/library/charts/jquery.flot.stack.js"></script>
		<script src="js/library/charts/jquery.flot.pie.min.js"></script>
		<script src="js/library/charts/jquery.sparkline.min.js"></script>
		<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="excanvas.min.js"></script><![endif]-->

		<!-- JS:syntaxHighlighter-->
		<script src="js/library/syntaxHighlighter/shCore.js"></script>
		<script src="js/library/syntaxHighlighter/shBrushCss.js"></script>
		<script src="js/library/syntaxHighlighter/shBrushXml.js"></script>
		<script src="js/library/syntaxHighlighter/shBrushJScript.js"></script>

		<!-- JS:bootstrap-->
		<script src="bootstrap/js/bootstrap.min.js"></script>
		
		<!-- JS:custom js for this template -->
		<script src="js/styler/custom.js"></script>
		<script src="js/highcharts.js"></script>
		<script src="js/exporting.js"></script>
		<script type="text/javascript">
			var titleYear =(new Date).getFullYear();
			Number.prototype.nformat = function() {
			    return this.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
			};
		    $(document).ready(function(){
				$("#slc_year").disableSelection();
		    	$('.widget-sales').on('click','#loadsales',function(){
		        	$('#myModal').modal('show');
		    	});
		    	$.ajax({ 
		    		cache: false,
					type: "POST",
					dataType: "json",
		        	url: "AJAX/select_year.php",
		        	success: function (json) {
		        		$.each(json, function() {
							var item = this;
		        			$("#slc_year").append("<option value='" + item[0] + "'>" + item[0] + "</option>");
						});
						$("#slc_year").val((new Date).getFullYear());
			        }
			    });
			    $.ajax({ 
			    	cache: false,
					type: "POST",
					dataType: "json",
		        	url: "AJAX/select_yearSign.php",
		        	success: function (json) {
		        		$.each(json, function() {
							var item = this;
		        			$("#slc_year_sign").append("<option value='" + item[0] + "'>" + item[0] + "</option>");
						});
						$("#slc_year_sign").val((new Date).getFullYear());
			        }
			    });
				var _loginID = "<?php echo $_COOKIE['Ses_ID']?>";
				$.ajax({ 
					type:"POST",
					dataType: "json",
			        url: "Ajax/SV_Select_saleup.php",
			        data: {
			        	saleid: _loginID
			        },
			        success: function (json) {
						
			        	$(json).each(function() {
			        		var empno=this[0];
			        		var name=this[1];
							if(empno == _loginID){
								checked = "checked";
								classli = "current";
							}else{
								checked = "";
								classli = "";
							}
			        		$("#listsaleName").append('<li class="' + classli + '"><p class="sales"><input type="checkbox" name="chkname" style="margin-top:-2px" class="chksales" value="' + empno + '" ' + checked + ' /> ' + name + '<span class=" pull-right" style="margin-top:-8px"></span></p></li>');
							
						});
						$('.salesAll').on('click',function(){
							if ($(".salesAll").is(':checked')) {
								$(".chksales").prop("checked", true);
								$('.chksales').closest('li').attr("class", "current");
								getSaleID();
							} else {
								$(".chksales").prop("checked", false);
								$('.chksales').closest('li').attr("class", "");
								showGraph();
							}
						});
						$('.chksales').on('click',function(){
							if ( $(this).prop('checked')) {
								$(this).closest('li').attr("class", "current");
							} else {
								$(this).closest('li').attr("class", "");
							}
							if( $(".chksales:checked").length == $(".chksales").length){
								$(".salesAll").prop("checked", true);
							}else{
								$(".salesAll").prop("checked", false);
							}
							getSaleID();
						});
						showGraph();//start
			        }
				});
		    	$('.progressAll').on('click',function(){
					if ($(".progressAll").is(':checked')) {
						$(".progress").prop("checked", true);
						$('.progress').closest('li').attr("class", "current");
						document.frmexport.Progress2.value="100,90,80,70,60,50,40,30,20,10,0,v";
						showGraph();
					} else {
						$(".progress").prop("checked", false);
						$('.progress').closest('li').attr("class", "");
						showGraph();
					}
		    	});
				$('.progress').on('click',function(){
					if ( $(this).prop('checked')) {
						$(this).closest('li').attr("class", "current");
				    } else {
						$(this).closest('li').attr("class", "");
				    }
					if( $(".progress:checked").length == $(".progress").length){
						$(".progressAll").prop("checked", true);
					}else{
						$(".progressAll").prop("checked", false);
					}
					getProgress();
				});
		    	
				$('.optionsYear1').on('click',function(){
					$('.optionsYear1').closest('li').attr("class", "current");
					$('.optionsYear2').closest('li').attr("class", "");
					$("#slc_year_sign").disableSelection();
					$("#slc_year").enableSelection();
					document.frmexport.BidingYear.value=document.frmexport.slc_year.value;
					document.frmexport.where.value=document.frmexport.rdoYear1[0].value;
					showGraph();
				});
				$('.optionsYear2').on('click',function(){
					$('.optionsYear1').closest('li').attr("class", "");
					$('.optionsYear2').closest('li').attr("class", "current");
					$("#slc_year").disableSelection();
					$("#slc_year_sign").enableSelection();
					document.frmexport.BidingYear.value=document.frmexport.slc_year_sign.value;
					document.frmexport.where.value=document.frmexport.rdoYear1[1].value;
					showGraph();
				});
				$('.chkSpecial').on('click',function(){
					if ( $(this).prop('checked')) {
						$(this).closest('li').attr("class", "current");
						document.frmexport.SpecialProject.value="0,1";
					}else{
						$(this).closest('li').attr("class", "");
						document.frmexport.SpecialProject.value="0";
					}
					showGraph();
				});
				$("#slc_year").change(function(){
					document.frmexport.BidingYear.value=document.frmexport.slc_year.value;
					document.frmexport.where.value=document.frmexport.rdoYear1[0].value;
					showGraph();
				});
				$("#slc_year_sign").change(function(){
					document.frmexport.BidingYear.value=document.frmexport.slc_year_sign.value;
					document.frmexport.where.value=document.frmexport.rdoYear1[1].value;
					showGraph();
				});
				
				function getSaleID(){
					var chkid='';
					$("input[name='chkname']").each(function () {
							if ( $(this).prop('checked')) {
								if(chkid=='')
								{
									chkid= $(this).val();
								}
								else
								{
									chkid = chkid+","+$(this).val();
								}
							}
					});
					document.frmexport.saleid.value=chkid;
					showGraph();
				}
				function getProgress(){
					var chkidlist = '';
					$("input[name='chkprog']").each(function () {
							if ( $(this).prop('checked')) {
								if(chkidlist=='')
								{
									chkidlist= $(this).val();
								}
								else
								{
									chkidlist = chkidlist+","+$(this).val();
								}
							}
					});
					document.frmexport.Progress2.value=chkidlist;
					showGraph();
				}

				$('#btnExport').on('click',function(){
					if($('#warningGraph').is(':hidden')) {
						$('#showWarning').addClass("hide");
						if ( ($(".chksales:checked").length == 0) || ($(".progress:checked").length == 0)) {
							$('#showWarning').removeClass("hide");
						}else{
							console.log("Export ",document.frmexport.BidingYear.value+' '+document.frmexport.where.value+' '+document.frmexport.Progress2.value+' '+document.frmexport.saleid.value+' '+document.frmexport.SpecialProject.value);
							document.frmexport.submit();
						}
					}
				});	
//////////////////////////////show graph
			$('#btnSwitch').on('click',function(){
				if($('#containerBar').is(':hidden')) {
					$("#containerBar").show();
					$("#containerPie").hide();
				}else{
					$("#containerBar").hide();
					$("#containerPie").show();
				}
			});
			function showGraph() {
				$("#loading").hide();
				//showGraphShow();
			}
			function showGraphShow() {
				var showNo='1';
				$("#loading").show();
				$('#showWarning').addClass("hide");
				$('#warningGraph').addClass("hide");
					if($(".progress:checked").length == 0 || $("input[name='chkname']:checked").length == 0){
						$('#showWarning').removeClass("hide");
						$('#Graph').hide();
					}else{
						var typeExport = '';
						if(document.frmexport.where.value=='TimeFrameBidingDate'){
							typeExport = 'bidding';
						}else{
							typeExport = 'signdate';
						}
						console.log("change ",document.frmexport.BidingYear.value+' '+typeExport+' '+document.frmexport.Progress2.value+' '+document.frmexport.saleid.value+' '+document.frmexport.SpecialProject.value);
						
						$.ajax({ 
							typr:"POST",
							dataType: "json",
							url: "Ajax/export_monthly.php",
							data: {
								year: document.frmexport.BidingYear.value,
								saleid: document.frmexport.saleid.value,
								type: typeExport,
								special: document.frmexport.SpecialProject.value
							},
							success: function (json) {
								console.log(json);
								if(json.length == 0){
									$('#warningGraph').removeClass("hide");
									$('#Graph').hide();
								}
								else
								{

									var dataBar ='';
									var dataPie ='';
									var P100=0;
									var P90=0;
									var P80=0;
									var P70=0;
									var P60=0;
									var P50=0;
									var P40=0;
									var P30=0;
									var P20=0;
									var P10=0;
									var P0=0;
									var Pv=0;
									$.each(json, function() {
										//var item = this;
										//console.log(this[1]);
										P100=parseFloat(this[1]);
										P90=parseFloat(this[2]);
										P80=parseFloat(this[3]);
										P70=parseFloat(this[4]);
										P60=parseFloat(this[5]);
										P50=parseFloat(this[6]);
										P40=parseFloat(this[7]);
										P30=parseFloat(this[8]);
										P20=parseFloat(this[9]);
										P10=parseFloat(this[10]);
										P0=parseFloat(this[11]);
										Pv=parseFloat(this[12]);
									});
									if(showNo == '1'){
										$('#detail > tbody:last').empty();
										$(".charts-data-quarter").text('');
										$("#sumResult").text('');
										$("#numResult").text('0');
										dataBar = [P100,P90,P80,P70,P60,P50,P40,P30,P20,P10,P0,Pv];
										dataPie = [
										  ['100', P100],
										  ['90', P90],
										  ['80', P80],
										  ['70', P70],
										  ['60', P60],
										  ['50', P50],
										  ['40', P40],
										  ['30', P30],
										  ['20', P20],
										  ['10', P10],
										  ['0', P0],
										  ['v', Pv]
										];
									}else{
										if(showNo == '100'){
											var p100Pie = {name: '100',y: P100,sliced: true,selected: true};
											var p100Bar = {y:P100,selected:true};
										}else{
											var p100Pie = ['100', P100];
											var p100Bar = P100;
										}
										if(showNo == '90'){
											var p90Pie = {name: '90',y: P90,sliced: true,selected: true};
											var p90Bar = {y:P90,selected:true};
										}else{
											var p90Pie = ['90', P90];
											var p90Bar = P90;
										}
										if(showNo == '80'){
											var p80Pie = {name: '80',y: P80,sliced: true,selected: true};
											var p80Bar = {y:P80,selected:true};
										}else{
											var p80Pie = ['80', P80];
											var p80Bar = P80;
										}
										if(showNo == '70'){
											var p70Pie = {name: '70',y: P70,sliced: true,selected: true};
											var p70Bar = {y:P70,selected:true};
										}else{
											var p70Pie = ['70', P70];
											var p70Bar = P70;
										}
										if(showNo == '60'){
											var p60Pie = {name: '60',y: P60,sliced: true,selected: true};
											var p60Bar = {y:P60,selected:true};
										}else{
											var p60Pie = ['60', P60];
											var p60Bar = P60;
										}
										if(showNo == '50'){
											var p50Pie = {name: '50',y: P50,sliced: true,selected: true};
											var p50Bar = {y:P50,selected:true};
										}else{
											var p50Pie = ['50', P50];
											var p50Bar = P50;
										}
										if(showNo == '40'){
											var p40Pie = {name: '40',y: P40,sliced: true,selected: true};
											var p40Bar = {y:P40,selected:true};
										}else{
											var p40Pie = ['40', P40];
											var p40Bar = P40;
										}
										if(showNo == '30'){
											var p30Pie = {name: '30',y: P30,sliced: true,selected: true};
											var p30Bar = {y:P30,selected:true};
										}else{
											var p30Pie = ['30', P30];
											var p30Bar = P30;
										}
										if(showNo == '20'){
											var p20Pie = {name: '20',y: P20,sliced: true,selected: true};
											var p20Bar = {y:P20,selected:true};
										}else{
											var p20Pie = ['20', P20];
											var p20Bar = P20;
										}
										if(showNo == '10'){
											var p10Pie = {name: '10',y: P10,sliced: true,selected: true};
											var p10Bar = {y:P10,selected:true};
										}else{
											var p10Pie = ['10', P10];
											var p10Bar = P10;
										}
										if(showNo == '0'){
											var p0Pie = {name: '0',y: P0,sliced: true,selected: true};
											var p0Bar = {y:P0,selected:true};
										}else{
											var p0Pie = ['0', P0];
											var p0Bar = P0;
										}
										if(showNo == 'v'){
											var pvPie = {name: 'v',y: Pv,sliced: true,selected: true};
											var pvBar = {y:Pv,selected:true};
										}else{
											var pvPie = ['v', Pv];
											var pvBar = Pv;
										}
										
										dataBar = [p100Bar,p90Bar,p80Bar,p70Bar,p60Bar,p50Bar,p40Bar,p30Bar,p20Bar,p10Bar,p0Bar,pvBar];
										dataPie = [p100Pie,p90Pie,p80Pie,p70Pie,p60Pie,p50Pie,p40Pie,p30Pie,p20Pie,p10Pie,p0Pie,pvPie];
									}
									RanderBarChart('containerBar', dataBar);
									RenderPieChart('containerPie', dataPie); 
								}
							}
						});
					}
					$("#loading").hide();
				}
			function RenderPieChart(elementId, dataList) {
                new Highcharts.Chart({
                    chart: {
                        renderTo: elementId,
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false
                    }, 
                    credits:{
                    	enabled: false
                    },
                    exporting: {
			            enabled: false
			        },
                    title: {
                        text: titleYear
                    },
                    tooltip: {
                        formatter: function () {
                            return '<b>' + this.point.name + '</b> : ' + this.percentage.nformat() + ' %' ;
                        }
                    },
                    /*plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                color: '#000000',
                                connectorColor: '#000000',
                                formatter: function () {
                                    return '<b>' + this.point.name + '</b> : ' + this.point.y.nformat() ;
                                }
                            }
                        },
                        series: {                        	
							 events: {
			                    click: function(event) {
									//showdetail(event.point.name,event.point.y);
			                    }
			                }
                        }
                    },*/
                    series: [{
                        type: 'pie',
                        name: titleYear,
                        data: dataList
                    }],
                    
                    colors: [
					   '#749DD1', 
					   '#D67B76', 
					   '#C5DC96', 
					   '#AE9BC8', 
					   '#66BCD5', 
					   '#F19F55', 
					   '#84B5EF', 
					   '#8AB9EF', 
					   '#F89995', 
					   '#D8F4A0', 
					   '#C5B1E6', 
					   '#94EAFB', 
					   '#FEC283', 
					   '#CADDFB', 
					   '#FECCCD', 
					   '#EAFBCE'
					]
                });
				$("#loading").hide();
            }
			function RanderBarChart(elementId,dataList,categoriesList){
				//console.log(dataList);
				$('#'+elementId).highcharts({

			        chart: {
			            type: 'column'
			        },			         
                    credits:{
                    	enabled: false
                    },
                    exporting: {
			            enabled: false
			        },
                    title: {
                        text: titleYear
                    },
			        xAxis: {
			            //categories: ['100','90','80','70','60','50','40','30','20','10','0','v']  
						categories: categoriesList
			        },
			        /*plotOptions: {
			            series: {
			                allowPointSelect: true,
			                cursor: 'pointer',
			                point: {
			                    events: {
			                        click: function() {
										//showdetail(this.category,this.y);				
			                        }
			                    }
			                }
			            }
			        },*/

			        series: [{
			        	data: dataList      
			            ,name:"Income "        
			        }]
			    });
				$("#loading").hide();
			}
			
		    });
		</script>
</form>
</body>
<!-- Mirrored from bootstrapstyler.com/preview/_/flatdashboard/ by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 18 Oct 2013 07:00:04 GMT -->
</html>