<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<meta http-equiv="refresh" content="86400">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="Izuddin Helmi">
	<title>Update Project Status</title>
	<link href="bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="bootstrap/css/bootstrap-responsive.css" rel="stylesheet" type="text/css">
	<link href="js/library/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
	<link href="css/styler/style_edit.css" rel="stylesheet" type="text/css">
	<link href="css/main.css" rel="stylesheet" type="text/css">
	<link href="js/library/syntaxHighlighter/shCore.css" rel="stylesheet" type="text/css"/>
	<link href="js/library/syntaxHighlighter/shThemeDefault.css" rel="stylesheet" type="text/css"/>
	<link href="css/styler/fontello-edit.css" rel="stylesheet" type="text/css">
    <link href="css/styler/animation.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="js/library/jquery/jquery-ui-1.10.3.custom.min.css" />

	<link href="libs/bootstrap/css/bootstrap-edit.css" rel="stylesheet">
    <link href="libs/prettify/prettify-bootstrap.css" rel="stylesheet">
    <!--Bootstrap-editable-->
    <link href="bootstrap-editable/css/bootstrap-editable.css" rel="stylesheet">

</head>
<body cz-shortcut-listen="true" style="margin-top: 0px;">
<div id="footer" class="bg-color dark-blue">
</div>
	<script src="js/library/jquery/jquery-1.9.1.js"></script>
	<script src="js/library/jquery/jquery-ui-1.10.3.custom.js"></script>
	<script src="js/library/masonry/jquery.masonry.min.js"></script>
	<script src="js/library/flexslider/jquery.flexslider-min.js"></script>
	<script src="js/library/dataTables/jquery.dataTables.min.js"></script>
	<script src="js/library/fullcalendar/fullcalendar.min.js"></script>
	<script src="js/library/fullcalendar/gcal.js"></script>
	<script src="js/library/forms/jquery.hotkeys.js"></script>
	<script src="js/library/forms/bootstrap-wysiwyg.js"></script>
	<script src="js/library/forms/picker.js"></script>
	<script src="js/library/forms/picker.date.js"></script>
	<script src="js/library/forms/picker.time.js"></script>
	<script src="js/library/forms/jquery.bootstrap.wizard.min.js"></script>
	<script src="js/library/charts/jquery.flot.js"></script>
	<script src="js/library/charts/jquery.flot.resize.js"></script>
	<script src="js/library/charts/jquery.flot.stack.js"></script>
	<script src="js/library/charts/jquery.flot.pie.min.js"></script>
	<script src="js/library/charts/jquery.sparkline.min.js"></script>
	<script src="js/library/syntaxHighlighter/shCore.js"></script>
	<script src="js/library/syntaxHighlighter/shBrushCss.js"></script>
	<script src="js/library/syntaxHighlighter/shBrushXml.js"></script>
	<script src="js/library/syntaxHighlighter/shBrushJScript.js"></script>
	<script src="js/library/jquery.number.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="js/styler/custom.js"></script>
	<script src="js/set_cookie.js"></script>

 <!--<script src="libs/jquery/jquery-1.8.2.min.js"></script>-->
  <script src="libs/bootstrap/js/bootstrap.min.js"></script>
  <script src="bootstrap-editable/js/bootstrap-editable.js"></script>
  <script src="libs/prettify/prettify.js"></script>
  <script src="libs/mockjax/jquery.mockjax.js"></script>

	<script src="js/jquery-scrolltofixed.js"></script>
	<script src="main.js"></script>

	<script type="text/javascript">

	 $(document).ready(function() {
	 		function CurrentDate(format)
			{
				var today = new Date();
				var dd = today.getDate();
				var mm = today.getMonth()+1; //January is 0!
				var yyyy = today.getFullYear();

				if(dd<10) {
				    dd='0'+dd
				} 

				if(mm<10) {
				    mm='0'+mm
				} 
				if(format==103)
				{
					today = dd+'/'+mm+'/'+yyyy;
				}
				else if(format==104)
				{
					today = dd+'.'+mm+'.'+yyyy;					
				}
				return today;
			}
			console.log("start");
	  	$.ajax({
				type: "POST",
				dataType: "json",
				url: "AJAX/CA_Project_select_all.php",
				success: function(json) {
					$.each(json, function() {
						var item = this;
						var ForecastID=item[0];
						var inp_signDate=item[1];
						var inp_endCont=item[2];
						var progress=item[3];
						var status="";

						if(progress=="100")
						{
							var today=CurrentDate(103);//dd/mm/yyyy
							var dateComponentsfrom = today.split("/");

							//split and convert into date to be more precise

			        var fromdate = new Date(dateComponentsfrom[2], dateComponentsfrom[1] - 1, dateComponentsfrom[0]);
			        var dateComponentsto  = inp_endCont.split("/");
			        var todate = new Date(dateComponentsto [2], dateComponentsto [1] - 1, dateComponentsto [0]);
			        if(fromdate>todate){
								//Project year sign-year end:Sign,End แล้ว

			        	var signdate  = inp_signDate.split("/");
			        	if(signdate[2]==dateComponentsto[2])
			        	{
									status="Project "+signdate[2];
								}
								else
								{						
									status="Project "+signdate[2]+"-"+dateComponentsto[2];
								}
			        }
			        else
			        {
								//Project Implement year sign-year end: Sign แล้ว แต่ยังไม่ End
								var signdate  = inp_signDate.split("/");
			        	if(signdate[2]==dateComponentsto[2])
			        	{
									status="Project implement "+signdate[2];
								}
								else
								{						
									status="Project implement"+signdate[2]+"-"+dateComponentsto[2];
								}
			        }

						}
						else if(progress=="80" || progress=="90")
						{
							status="Opportunity : 80-90%";
						}
						else if(progress=="50" || progress=="60" || progress=="70")
						{
							status="Opportunity : 50-60-70%";
						}
						else if(progress=="10" || progress=="20" || progress=="30"|| progress=="40")
						{
							status="Opportunity : 10-20-30-40%";
						}
						else if(progress=="0" )
						{
							status="Cancel";
						}
						else if(progress=="v")
						{
							status="Void";
						}
						console.log(ForecastID," : ",progress," : ",inp_signDate," : ",inp_endCont," : ",status);
						$.ajax({
							type: "POST",
							dataType: "json",
							url: "AJAX/CA_project_update_projectstatus.php",
							data: {
								status: status,
								ForecastID:ForecastID
							},
							success: function(json) {
								//console.log("load ",item[0]);
								//Update Project Status
							},
							error: function(err)
							{

							}
						});
						
					});
				},
				error: function() {

				}
			});
		
	});

			
</script>

</body>
</html>