﻿<!DOCTYPE html>
<html lang="en">
	
<!-- Mirrored from bootstrapstyler.com/preview/_/flatdashboard/ui.html by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 18 Oct 2013 07:02:36 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
		<meta charset="utf-8">
		<title>SFS:::Setting</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="Izuddin Helmi">

		<!-- Stylesheets -->
		<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
		<link href="js/library/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
		<link href="css/styler/style.css" rel="stylesheet">
		<link href="js/library/syntaxHighlighter/shCore.css" rel="stylesheet" type="text/css" />
		<link href="js/library/syntaxHighlighter/shThemeDefault.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="css/styler/fontello.css">
	    <link rel="stylesheet" href="css/styler/animation.css"><!--[if IE 7]>
	    <link rel="stylesheet" href="css/styler/fontello-ie7.css"><![endif]-->
	    

		<!-- Custom Fonts -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300,500' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

		<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
		  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	</head>
<?php  $checkmenu = '6'; ?>
<body cz-shortcut-listen="true" style="margin-top:0px;">
<input type="hidden" name="Progress2" value="All">
<input type="hidden" name="saleid">
<input type="hidden" name="SpecialProject">
<input type="hidden" name="BidingYear" >
<input type="hidden" name="where" >
<section class="page-header bg-color white">
	<div class="container">
		<div class="row">
			<div class="span12">
				<div class="section-header clearfix">
					<div class="pull-left">
						<h2><i class="icon-tools"></i>Settings</h2>
					</div>
					<div id="shortcut" class="nav-collapse pull-right"><ul class="shortcuts unstyled">
						<?php include("menu.php");?>
					</ul>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="content">
	<div class="page-tab">
		<div class="page-tab-header">
			<div class="container">
				<ul class="nav nav-tabs tabbed clearfix" id="socialTab">
					<li class="active"><a  href="#mailDep"><h5>Department</h5></a></li>
				</ul>
			</div>
		</div>
		<div class="tab-content">
			<div class="tab-pane fade active in container" id="mailDep">
				<div class="widget bg-color turqoise rounded">
					<div class="box-padding">
						<h1>Department</h1>
						<p class="lead">Department Settings </p>
					</div>
				</div>
					<div class="box-padding" style="padding:0px">
						<h3 style="margin:5px;text-align:right;"><a href="javascript:AddDep();" title="Add Department"><i style="color: #333333;" class="i_btn icon-plus-circle" ></i></a></h3>
						<table class="table table-striped" id="example">
							<thead>
								<tr role="row">
									<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;" width="50px">No.</th>
									<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;"width="200px">Department</th>
									<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;" width="400px">E-mail</th>
									<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align:center;" width="100px">Edit Sequence</th>
									<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align:center;" width="50px"><i class="icon-pencil-1" title='Edit'></i></th>
									<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align:center;" width="50px"><i class="icon-trash-6" title='Delete'></i></th>
								</tr>
							</thead>
							<tbody id="tbody_searchresults"></tbody>
						</table>
					</div>
			
			</div>
			
		</div>
	<div class="modal hide fade" id="editDepartment" style="width: 800px;">
		<div class="modal-header bg-color light-green aligncenter">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<h1>Department</h1>
		</div>
		<div class="modal-body">
			<!--<table class="table table-striped table-condensed table_select">-->
			<table>					
				<tr>
					<td class="text-right"><font color="#FF0000">*</font>&nbsp;<font color="#666666">Department</font>&nbsp;</td>
					<td><input id="inpDepName" class="span1" type="text" style="width: 150px;"><input id="hidID" type="hidden"></td>
				</tr>
				<tr>
					<td class="text-right"><font color="#FF0000">*</font>&nbsp;<font color="#666666">E-mail</font>&nbsp;</td>
					<td><input id="inpMail" class="span1" type="text" style="width: 550px;"></td>
				</tr>
			</table>
			<font color="#FF0000">**</font>&nbsp;<font color="#666666">หากมีหลาย E-mail ให้คั่นด้วย comma( , ) เช่น mail1.com,mail2.com,...  </font>
		</div>
		<div class="modal-footer alignright">
			<a id="staff_btn_ok" class="btn btn-blue">Save</a>
			<a class="btn btn-red" data-dismiss="modal">Cancel</a>
		</div>
	</div>
	</div>
</section>
		<div id="footer" class="bg-color dark-blue">
			<div class="container">
				<div class="box-padding">
					Copyright &copy; 2013 Sales Forecast
				</div>
			</div>
		</div>
		<!-- Javascript -->
		<!-- JS:jquery-->
		<!--<script src="../../../../ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>-->
  		<script src="js/library/jquery/jquery.min.js"></script>
		<script src="js/library/jquery/jquery-ui.min.js"></script>

		<!-- JS:masonry-->
		<script src="js/library/masonry/jquery.masonry.min.js"></script>

		<!-- JS:flexslider-->
		<script src="js/library/flexslider/jquery.flexslider-min.js"></script>

		<!-- JS:tables-->
		<script src="js/library/dataTables/jquery.dataTables.min.js"></script>

		<!-- JS:calendar-->
		<script src="js/library/fullcalendar/fullcalendar.min.js"></script>
		<script src="js/library/fullcalendar/gcal.js"></script>

		<!-- JS:forms-->
		<script src="js/library/forms/jquery.hotkeys.js"></script>
		<script src="js/library/forms/bootstrap-wysiwyg.js"></script>
		<script src="js/library/forms/picker.js"></script>
		<script src="js/library/forms/picker.date.js"></script>
		<script src="js/library/forms/picker.time.js"></script>
		<script src="js/library/forms/jquery.bootstrap.wizard.min.js"></script>

		<!-- JS:charts-->
		<script src="js/library/charts/jquery.flot.js"></script>
		<script src="js/library/charts/jquery.flot.resize.js"></script>
		<script src="js/library/charts/jquery.flot.stack.js"></script>
		<script src="js/library/charts/jquery.flot.pie.min.js"></script>
		<script src="js/library/charts/jquery.sparkline.min.js"></script>
		<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="excanvas.min.js"></script><![endif]-->

		<!-- JS:syntaxHighlighter-->
		<script src="js/library/syntaxHighlighter/shCore.js"></script>
		<script src="js/library/syntaxHighlighter/shBrushCss.js"></script>
		<script src="js/library/syntaxHighlighter/shBrushXml.js"></script>
		<script src="js/library/syntaxHighlighter/shBrushJScript.js"></script>

		<!-- JS:bootstrap-->
		<script src="bootstrap/js/bootstrap.min.js"></script>
		
		<!-- JS:custom js for this template -->
		<script src="js/styler/custom.js"></script>
		<script type="text/javascript">
			var _loginID = "<?php echo $_COOKIE['Ses_ID']?>";
			var sumResult = 0;
			var editID = '';
		    $(document).ready(function(){
			 //------------------------------------------------- Initial -------------------------------------------------
			showDepartment();
			$("#staff_btn_ok").click(function() {
				if($("#inpDepName").val().replace(" ","") == "" || $("#inpMail").val().replace(" ","") == ""){
					alert("กรุณากรอกข้อมูลในช่องที่มีเครื่องหมาย * ให้ครบ");
				}else{
					$("#loading").show();
					$.ajax({
						type: "POST",
						dataType: "json",
						data: {
							depId: $("#hidID").val(),
							userId: _loginID,
							depName: $("#inpDepName").val(),
							mailManager: $("#inpMail").val()
						},
						url: "AJAX/saveDepartment.php",
						success: function(json) {
							alert("ทำการบันทึกเรียบร้อย");
							showDepartment();
						},
						error: function() {
							alert("เกิดปัญหากรุณาลองใหม่อีกครั้ง");
							$("#loading").hide();
						}
					});
					$("#editDepartment").modal("hide");
				}
			});
			
			});
			 //------------------------------------------------- Function -------------------------------------------------
		    	function AddDep(){
					$("#editDepartment").modal("show");
				}
				function showDepartment(){
				$.ajax({
					type: "POST",
					dataType: "json",
					url: "AJAX/showDepartment.php",
					success: function(json) {
						listResults(json);
					},
					error: function() {
						alert("เกิดปัญหาในการค้นหากรุณาลองใหม่อีกครั้ง");
						$("#loading").hide();
					}
				});
			}
				function listResults(json) {
				$("#loading").show();
				var i = 1;
				$('#tbody_searchresults').empty();
				$.each(json, function() {
					var item = this;
					sumResult = json.length;
					var tr = $('<tr><td class="lead14" >' + i + '</td><td class="lead14">' + item[2] + '</td><td class="lead14">' + item[3] + '</td><td class="lead14" style="text-align:center"><a href="javascript:editUp('+item[0]+','+item[1]+');" title="Up"><i class="i_btn icon-up-3 iconUp"></i></a>&nbsp;&nbsp;<a href="javascript:editDown('+item[0]+','+item[1]+');" title="Down"><i class="i_btn icon-down-3 iconDown"></i></a></td><td style="text-align:center"><a href="javascript:edit('+item[0]+');" title="Edit"><i class="i_btn icon-pencil-1 iconEdit"></i></a></td><td style="text-align:center"><a href="javascript:deledtDep('+item[0]+');" title="Delete"><i class="i_btn icon-trash-6 iconEdit"></i></a></td></tr>');
					$("#tbody_searchresults").append(tr);
					i++;
				});
				if (json.length == 0) {
					$("#tbody_searchresults").append($('<tr><td colspan="4" style="text-align: center;">ไม่พบข้อมูล</td></tr>'));
				}
				$("#loading").hide();
			}
			function deledtDep(id) {
				if (window.confirm("ต้องการลบรายการนี้หรือไม่")) {
		    		$("#loading").show();
					$.ajax({
					type: "POST",
					dataType: "json",
					data: {
							depId: id,
							status: 'del'
						},
					url: "AJAX/saveDepartment.php",
					success: function(json) {
						alert("ทำการลบข้อมูลเรียบร้อย");
						showDepartment();
						},
					error: function() {
						alert("เกิดปัญหากรุณาลองใหม่อีกครั้ง");
						$("#loading").hide();
						}
					});
		    	}
				
			}
			function edit(id) {
				editID = id ;
				$.ajax({
    			type: "POST",
				dataType: "json",
				data: {
						idDep: id,
					},
		        url: "AJAX/departmentDetail.php",
		        success: function(json) {
		        	$.each(json, function() {
	    				var item = this;
						$("#hidID").val(item[0]);
						$("#inpDepName").val(item[1]);
						$("#inpMail").val(item[2]);
	    			});
					$("#editDepartment").modal("show");
					}
				});
			}
		
			function editUp(id,sequence) {
				console.log('id,sequence: ',id + ', '+sequence);
				var new_sequence = 0;
				if(sequence !='1'){
					new_sequence = sequence-1;
					saveSequence(id,sequence,new_sequence);
				}
			}
			function editDown(id,sequence) {
				console.log('id,sequence: ',id + ', '+sequence);
				var new_sequence = 0;
				if(sequence !=sumResult){
					new_sequence = sequence+1;
					saveSequence(id,sequence,new_sequence);
				}
			}
			function saveSequence(id,sequence,new_sequence){
				$("#loading").show();
	    		$.ajax({
	    			type: "POST",
					dataType: "json",
					data: {
						depId: id,
						sequence: sequence,
						new_sequence:new_sequence
					},
			        url: "AJAX/saveSequenceDep.php",
			        success: function(json) {
			        	showDepartment();
			        },
					error: function() {
						alert("เกิดปัญหากรุณาลองใหม่อีกครั้ง");
						$("#loading").hide();
					}
			    });
			}
		
		</script>
  	</body>

<!-- Mirrored from bootstrapstyler.com/preview/_/flatdashboard/ by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 18 Oct 2013 07:00:04 GMT -->
</html>