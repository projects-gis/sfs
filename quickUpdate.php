<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="Izuddin Helmi">
	<title>Smart Update</title>
	<script src="js/library/jquery/jquery.min.js"></script>
	<link href="bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="bootstrap/css/bootstrap-responsive.css" rel="stylesheet" type="text/css">
	<link href="js/library/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
	<link href="css/styler/style_edit.css" rel="stylesheet" type="text/css">
	<link href="css/main.css" rel="stylesheet" type="text/css">
	<link href="js/library/syntaxHighlighter/shCore.css" rel="stylesheet" type="text/css"/>
	<link href="js/library/syntaxHighlighter/shThemeDefault.css" rel="stylesheet" type="text/css"/>
	<link href="css/styler/fontello-edit.css" rel="stylesheet" type="text/css">
    <link href="css/styler/animation.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="js/library/jquery/jquery-ui-1.10.3.custom.min.css" />

	<link href="libs/bootstrap/css/bootstrap-edit.css" rel="stylesheet">
    <link href="libs/prettify/prettify-bootstrap.css" rel="stylesheet">
    <!--Bootstrap-editable-->
    <link href="bootstrap-editable/css/bootstrap-editable.css" rel="stylesheet">
    <script>

		function OpenProject(id)
		{
			window.location="newproject.php?id="+id+"&page=quickupdate";
		}
		</script>
	<style>
	a.warning {
	    color: #DD1144;
	    
	}	
	a.current {
	    color: #D3C523;
	}
	a.success {
	    color: #008000;
	}
	</style>
</head>
<?php  $checkmenu = '7'; ?>
<body cz-shortcut-listen="true" style="margin-top: 0px;">
<div id="loading" style="height: 100%; width: 100%; position: fixed; left: 0; top: 0; z-index: 1051 !important; background-color: black; filter: alpha(opacity=9); -khtml-opacity: 0.9; -moz-opacity: 0.9; opacity: 0.9;"><img src="img/loading.gif" style="width: 150px; height: 150px; position: fixed; top: 50%; left: 50%; margin: -75px 0 0 -75px;"></div>
<section class="page-header bg-color white">
	<div class="container">
		<div class="row">
			<div class="span12">
				<div class="section-header clearfix">
					<div class="pull-left">
						<h2><i class="icon-clock-5"></i>Smart Update</h2>
					</div>
					<div id="shortcut" class="nav-collapse pull-right">
						<ul class="shortcuts unstyled">
							<?php include("menu.php");?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="dashboard content" >
	<div class="container">
		<div class="row">			
			<div class="span12">
			  <div class="tab-content">
				<div id="site0" class="tab-pane fade active in"><!--mysite1-->
					<div class="widget-header clearfix bg-color red rounded-top">
						<div class="box-padding" style="margin-bottom:0px;text-align:right;">
							<br/>
							<h1 class="pull-left">Smart Update </h1>
							<h4>
							<input type="text" id="txtKeyword" style="width:300px;margin-bottom:0px;margin-top:1px;display:none" sthle="margin-bottom: 0px;" placeholder="ค้นหา project" >&nbsp;&nbsp;
							<span id="sumResult" class="label label-inverse" style="font-size:20px;padding:10px;margin-top:10px" title="Number of results">0</span>&nbsp;&nbsp;<font color="#FFFFFF" style="font-size:14px"><b>รายการ</b></font>
							</h4><span>Sort by Signdate,Potential,Progress</span>
						</div>
					</div>
					<div class="bg-color white rounded-bottom">
						<div class="box-padding" style="padding:0px 0px;">
							<table class="table table-striped" >
								<thead id="setUp">
									<tr role="row">
										<!--<th rowspan="2" width="10px" style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align:center"></th>-->
										 <th rowspan="2" width="170px" style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align:center">Project</th>
						                <th rowspan="2" width="50px" style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align:center">Poten<br/>tial</th>
						                <th rowspan="2" width="50px" style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align:center">Pro<br/>gress</th>
						                <th rowspan="2" width="50px" style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align:center">Status Update</th>
						                <th colspan="10" width="700px" style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align:center">Timeline Progress</th>
									</tr>
									<tr>
						                <th width="70px" style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align:center" title='Build solution'>10</th>
						                <th width="70px" style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align:center" title='Presentation/Demo'>20</th>
						                <th width="70px" style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align:center" title='ร่างโครงการ (ของบประมาณ) '>30</th>
						                <th width="70px" style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align:center" title='ขั้นตอนการพิจารณาอนุมัติงบประมาณ '>40</th>
						                <th width="70px" style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align:center" title='ได้งบประมาณ '>50</th>
						                <th width="70px" style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align:center" title='TOR Final '>60</th>
						                <th width="70px" style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align:center" title='Bidding date'>70</th>
						                <th width="70px" style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align:center" title='ยื่นซองแล้ว '>80</th>
						                <th width="70px" style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align:center" title='ประกาศผลแล้วรอเซ็นสัญญา'>90</th>
						                <th width="70px" style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;text-align:center" title='signdate'>100</th>
						            </tr>
								</thead>
								<tbody id="detail">
								</tbody>
								<tfoot>

									<tr role="row">
										<th style="background: none repeat scroll 0 0 #333333;color: #FFFFFF;" colspan="14"></th>
										
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			  </div>
			</div>
		</div>
	</div>
</section>

<div id="footer" class="bg-color dark-blue">
	<div class="container">
		<div class="box-padding">
			Copyright &copy; 2014 Site Reference
		</div>
	</div>
</div>
<div class="modal hide fade" id="invoicing_modal" style="width: 800px; margin-left: -400px;" data-backdrop="static">
	<div class="modal-header bg-color light-green aligncenter">
	    <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>-->
	    <h1>Invoicing Plan</h1>
	</div>
	<div class="modal-body">
		<table id="invoicing_table_details" style="width: 100%">
			<tr>
				<td style="width: 50%;"></td>
				<td style="width: 50%;"></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
			</tr>
		</table>
		<table class="table table-striped table-condensed">
			<thead>
				<tr style=" background-color: #2ABF9E; color: #FFFFFF;">
					<th style="width: 42px;">Phase</th>
					<th style="width: 56px;">Days</th>
					<th style="width: 96px;">Date</th>
					<th style="width: 76px;text-align:right" >%</th>
					<th style="width: 126px;text-align:right" >Amount</th>
					<th style="width: 284px;">Remark</th>
					<th><i class="icon-minus-circled-1" title='Delete'></i></th>
				</tr>
			</thead>
			<tbody id="invoicing_tbody_list"></tbody>
		</table>
	</div>
	<div class="modal-footer alignright">
	    <a id="invoicing_btn_ok" class="btn btn-blue">OK</a>
	    <!--<a class="btn btn-red" data-dismiss="modal">CLOSE</a>-->
	</div>
</div>
<div style="display:none;">
	<div class="row" >
			<div class="span6">
			</div>
			<div class="span6">					
				<div class="widget widget-timeframe">	
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-calendar-6"></i> Time Frame</span>
					</div>					
					<div class="bg-color turqoise rounded">
						<div class="box-padding"><h5>
							<table>
							<tr>
								<td class="text-right">Bidding Date&nbsp;</td>
								<td><input id="inp_biddDate" class="form_date" style="width: 80px;" type="text" placeholder="choose date"></td>
								<td class="text-right">Contract Sign Date&nbsp;</td>
								<td><input id="inp_signDate" class="form_date" style="width: 80px;" type="text" placeholder="choose date"></td>
							</tr>	
							<tr>
								<td class="text-right">Project Duration&nbsp;</td>
								<td><input id="inp_pDuration" class="span1 nNumber text-right" type="text" placeholder="0"> Days</td>
								<td class="text-right">End of Contract&nbsp;</td>
								<td><input id="inp_endCont" class="span1" style="width: 80px;" type="text" readonly></td>
							</tr>
							<tr>
								<td class="text-right"><input id="chk_downPayment" type="checkbox" value="" style="margin-top: 0px;" checked> Down Payment&nbsp;</td>
								<td><input id="inp_downPayment" class="span1 nNumber text-right" type="text" placeholder="0">&nbsp;&nbsp;&nbsp;&nbsp;% =</td>
								<td colspan="2"><input id="inp_dPBaht" class="span2 number2 text-right" type="text" style="width: 150px;" placeholder="0.00" > Baht</td>
							</tr>
							<tr>
								<td class="text-right">Inv.Date&nbsp;</td>
								<td><input id="inp_dPDays" class="span1 nNumber text-right" type="text" placeholder="0"> Days =&nbsp;</td>
								<td colspan="2"><input id="inp_dPDate" class="form_date" style="width: 80px;" type="text" placeholder="choose date"></td>
							</tr>
							</table>							
							 </h5>
						</div>
					</div>
				</div>				
			</div>
	</div>
	<div class="row" >				
			<div class="span8">					
				<div class="widget widget-invoicing">	
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-list-alt"></i> Invoicing Plan</span>
						<div class="dropdown pull-right"><input id="inp_ForecastID" type="hidden">
							<h3 style="margin:0px;margin-top:-5px"><i id="btn_invoicing_modal" class="i_btn icon-edit-1" data-toggle="tooltip" data-placement="bottom" data-original-title="Manage Plan"></i></h3>
						</div>
					</div>					
					<div class="bg-color white rounded">
						<div class="box-padding" style="padding:5px">
			    			<table class="table table-striped">
								<thead>
									<tr style="background-color: #2ABF9E; color: #FFFFFF;">
										<th style="width: 50px;">Phase</th>
										<th style="width: 50px;">Days</th>
										<th style="width: 70px;">Date</th>
										<th style="width: 50px;text-align:right">%</th>
										<th style="width: 120px;text-align:right" >Amount</th>
										<th>Remark</th>
									</tr>
								</thead>
								<tbody id="tbody_invoicing"></tbody>
							</table>
				        </div>
					</div>
				</div>
			</div>
			<div class="span4">					
				<div class="widget widget-book">	
					<div class="widget-header clearfix">
						<span class="pull-left" style="padding-bottom: 3px;"><i class="icon-dollar"></i> Book/Forward</span>
					</div>					
					<div class="bg-color white rounded">
						<div class="box-padding" style="padding:5px">
			    			<table class="table table-striped">
								<thead>
									<tr style="background-color: #2ABF9E; color: #FFFFFF;">
										<th style="width: 80px;">Type</th>
										<th style="width: 80px;">Year</th>
										<th>Amount</th>
									</tr>
								</thead>
								<tbody id="tbody_bank"></tbody>
							</table>
				        </div>
					</div>
				</div>
			</div>
		</div>
	<script src="js/library/jquery/jquery-1.9.1.js"></script>
	<script src="js/library/jquery/jquery-ui-1.10.3.custom.js"></script>
	<script src="js/library/masonry/jquery.masonry.min.js"></script>
	<script src="js/library/flexslider/jquery.flexslider-min.js"></script>
	<script src="js/library/dataTables/jquery.dataTables.min.js"></script>
	<script src="js/library/fullcalendar/fullcalendar.min.js"></script>
	<script src="js/library/fullcalendar/gcal.js"></script>
	<script src="js/library/forms/jquery.hotkeys.js"></script>
	<script src="js/library/forms/bootstrap-wysiwyg.js"></script>
	<script src="js/library/forms/picker.js"></script>
	<script src="js/library/forms/picker.date.js"></script>
	<script src="js/library/forms/picker.time.js"></script>
	<script src="js/library/forms/jquery.bootstrap.wizard.min.js"></script>
	<script src="js/library/charts/jquery.flot.js"></script>
	<script src="js/library/charts/jquery.flot.resize.js"></script>
	<script src="js/library/charts/jquery.flot.stack.js"></script>
	<script src="js/library/charts/jquery.flot.pie.min.js"></script>
	<script src="js/library/charts/jquery.sparkline.min.js"></script>
	<script src="js/library/syntaxHighlighter/shCore.js"></script>
	<script src="js/library/syntaxHighlighter/shBrushCss.js"></script>
	<script src="js/library/syntaxHighlighter/shBrushXml.js"></script>
	<script src="js/library/syntaxHighlighter/shBrushJScript.js"></script>
	<script src="js/library/jquery.number.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="js/styler/custom.js"></script>
	<script src="js/set_cookie.js"></script>


	
        <!--<script src="libs/jquery/jquery-1.8.2.min.js"></script>-->
        <script src="libs/bootstrap/js/bootstrap.min.js"></script>
        <script src="bootstrap-editable/js/bootstrap-editable.js"></script>
        <script src="libs/prettify/prettify.js"></script>
        <script src="libs/mockjax/jquery.mockjax.js"></script>

	<script src="js/jquery-scrolltofixed.js"></script>
        <script src="main.js"></script>
	<!--date diff-->
    <script src="js/date/moment.min.js"></script>

	<script type="text/javascript">
		var GlobalSaleID = "<?php echo $_COOKIE['Ses_ID']?>";
		var keywordHome = "";

		function sendmailUpdate(ID)
		{
			var sendmailID=ID;
				$.ajax({
								type: "POST",
								dataType: "json",
							  url: "AJAX/sendmailUpdate.php",
							  data: {
							   	ID: sendmailID,
							   	CountlastStatus:1
							  },
							  success: function(json) {

								},
								error: function(error) {
									console.log("error : ",error);
									alert("เกิดปัญหาในการบันทึกกรุณาลองใหม่อีกครั้ง");
									$("#loading").hide();
								}
					});

		}
		function projectStatus(progress)
		{
			var status="";
			if(progress=="100")
			{
				var today=CurrentDate(103);//dd/mm/yyyy
				var dateComponentsfrom = today.split("/");

//split and convert into date to be more precise

        var fromdate = new Date(dateComponentsfrom[2], dateComponentsfrom[1] - 1, dateComponentsfrom[0]);
        var dateComponentsto  = $("#TimeFrameDeliveryDate").val().split("/");
        var todate = new Date(dateComponentsto [2], dateComponentsto [1] - 1, dateComponentsto [0]);
        if(fromdate>todate){
					//Project year sign-year end:Sign,End แล้ว

        	var signdate  = $("#TimeFrameContractSigndate").val().split("/");
        	if(signdate[2]==dateComponentsto[2])
        	{
						status="Project "+signdate[2];
					}
					else
					{						
						status="Project "+signdate[2]+"-"+dateComponentsto[2];
					}
        }
        else
        {
					//Project Implement year sign-year end: Sign แล้ว แต่ยังไม่ End
					var signdate  = $("#TimeFrameContractSigndate").val().split("/");
        	if(signdate[2]==dateComponentsto[2])
        	{
						status="Project implement "+signdate[2];
					}
					else
					{						
						status="Project implement"+signdate[2]+"-"+dateComponentsto[2];
					}
        }

			}
			else if(progress=="80" || progress=="90")
			{
				status="Opportunity : 80-90%";
			}
			else if(progress=="50" || progress=="60" || progress=="70")
			{
				status="Opportunity : 50-60-70%";
			}
			else if(progress=="10" || progress=="20" || progress=="30"|| progress=="40")
			{
				status="Opportunity : 10-20-30-40 %";
			}
			else if(progress=="0" )
			{
				status="Cancel";
			}
			else if(progress=="v")
			{
				status="Void";
			}
			else if(progress=="P")
			{
				status="Pending";
			}
			//$("#inp_projectstatus").val();
		}
		function quicksave(settings)
	    {
	        
	        //document.ifrQuicksave.location="quicksave.php?pk="+settings.data.pk+"&name="+settings.data.name+"&value="+settings.data.value;

	        $trSU=$("a[data-pk="+settings.data.pk+"]").closest('tr');
	        console.log("settings.data.name",settings.data.name);
	        $trSU.find('td').css('background-color', '');
	        $trSU.find("i.icon-alert").attr("style","display:none");
	        var SaleID = "<?php echo $_COOKIE['Ses_ID']?>";
	        //alert(settings.data.value);
	        $.ajax({
	            type: "POST",
	            dataType: "json",
	            data: {
	              name: settings.data.name,
	              pk: settings.data.pk,
	              value: settings.data.value,
	              salesid: SaleID
	            },
	            url: "AJAX/quicksave.php",
	            success: function(json) {
	            		console.log(json);
	            		var chkDP=json[0][4];
	            		var PercentAmount=json[0][5];
	            		var InvoiceAmount=json[0][6];
	            		var InvDuration=json[0][7];
	            		var Invdate=json[0][8];
	            		var TargetIncome=json[0][12];
	            		var TimeFrameProjectDuration=json[0][13];
	            		var TimeFrameContractSigndate=json[0][14];
	            		var TimeFrameDeliveryDate=json[0][15];
	                if(settings.data.name=="progress70")
	                {

	                	console.log("quicksave ",json);
	                	var $trSU=$("a[data-pk="+settings.data.pk+"]").closest('tr');

										var split70=settings.data.value.split("-");
										var date2 = new Date(split70[1]+"/"+split70[2]+"/"+split70[0]);
										date2.setDate(date2.getDate()+15);
										var DP80_day=(date2.getDate()>=10)? date2.getDate() : "0"+date2.getDate();
										var mon80=(date2.getMonth()+1);
										var DP80_mon=(mon80>=10)? mon80 : "0"+ mon80;
										var DP80 = DP80_day + "/" + DP80_mon + "/" + date2.getFullYear();	
	                	$trSU.find("a.progress80").html(DP80);


										date2.setDate(date2.getDate()+15);
										var DP90_day=(date2.getDate()>=10)? date2.getDate() : "0"+date2.getDate();
										var mon90=(date2.getMonth()+1);
										var DP90_mon=(mon90>=10)? mon90 : "0"+ mon90;
										var DP90 = DP90_day + "/" + DP90_mon + "/" + date2.getFullYear();	
	                	$trSU.find("a.progress90").html(DP90);
	                	//console.log($trSU.find("a.progress80")[0].innerHTML);

										date2.setDate(date2.getDate()+15);
										var DP100_day=(date2.getDate()>=10)? date2.getDate() : "0"+date2.getDate();
										var mon100=(date2.getMonth()+1);
										var DP100_mon=(mon100>=10)? mon100 : "0"+ mon100;
										var DP100 = DP100_day + "/" + DP100_mon + "/" + date2.getFullYear();	
	                	$trSU.find("a.progress100").html(DP100);
	                	modal_invoice(settings.data.pk,TargetIncome,TimeFrameProjectDuration,InvoiceAmount,PercentAmount,chkDP,InvDuration,Invdate,TimeFrameContractSigndate,TimeFrameDeliveryDate);

	                }
	                else if(settings.data.name=="progress100")
	                {
	                	
	                	modal_invoice(settings.data.pk,TargetIncome,TimeFrameProjectDuration,InvoiceAmount,PercentAmount,chkDP,InvDuration,Invdate,TimeFrameContractSigndate,TimeFrameDeliveryDate);
	                }
	                if(json[0][16]==1)
	                {
	                	console.log("sendmailUpdate ",settings.data.pk);
	                	//sendmailUpdate(settings.data.pk);
	                }
	            },
	            error: function() {}
	        });

	    }
			function modal_invoice(pk,inp_inco_evat,inp_pDuration,inp_dPBaht,inp_downPayment,chk_downPayment,InvDuration,Invdate,TimeFrameContractSigndate,TimeFrameDeliveryDate)
			{
				
				$("#inp_ForecastID").val(pk);
				$("#invoicing_modal").modal("show");
				$("#inp_signDate").val(TimeFrameContractSigndate).change();
				var tempend = $("#inp_signDate").val().split("/");
				var slcdateEnd=new Date(tempend[2],tempend[1]-1,tempend[0]);
				slcdateEnd.setDate(slcdateEnd.getDate() + parseInt(inp_pDuration));
				//$("#inp_signDate").datepicker('setDate', TimeFrameContractSigndate);
				$("#inp_endCont").val(("0" + slcdateEnd.getDate()).slice(-2) + "/" + ("0" + (slcdateEnd.getMonth() + 1)).slice(-2) + "/" + slcdateEnd.getFullYear());
				$("#inp_pDuration").val(inp_pDuration);
				if(chk_downPayment=="1")
				{
					$("#inp_dPDate").val(Invdate);
					$("#inp_dPBaht").val(inp_dPBaht);
					$("#chk_downPayment").click();
				}
				else
				{
					$("#chk_downPayment").attr("checked",false);
				}
				//
				console.log("TimeFrameContractSigndate : ",TimeFrameContractSigndate);
				//check invoicing plan >0
				$.ajax({
	            type: "POST",
	            dataType: "json",
	            data: {
	              idforecast: pk
	            },
	            url: "AJAX/LoadInvoice.php",
	            success: function(json) {
	              console.log("loadInvoice ",json);
	              $("#tbody_invoicing").empty();
	              $("#invoicing_tbody_list").empty();
		              $.each(json, function() {
		              	tr = '<tr class="warning"><td><p class="lead14">'+this[0]+'</p></td><td><p class="lead14">'+this[1]+'</p></td><td><p class="lead14">'+this[2]+'</p></td><td><p class="lead14 text-right">' + numFormat(parseFloat(this[3]).toFixed(2)) + '</p></td><td><p class="lead14 text-right">' + numFormat(parseFloat(this[4]).toFixed(2)) + '</p></td><td><p class="lead14">'+this[6]+'</p></td></tr>';
										$("#tbody_invoicing").append(tr);
									});
	              if($("#tbody_invoicing").children().length > 0)
	              {

		            	inp_inco_evat=parseFloat(inp_inco_evat.replace(/,/g, ""));       
					    		if ((inp_inco_evat > 0)  && inp_pDuration > 0) {
				    				$("#invoicing_tbody_list").empty();
				    				downPayment = "0.00";
				    				salesBal = parseFloat(inp_inco_evat);
				    				leftPerc = 100;
				    				if (chk_downPayment) {
				    					downPayment = numFormat(parseFloat(inp_dPBaht != "" ? inp_dPBaht : "0").toFixed(2));
				    					salesBal = parseFloat(inp_inco_evat) - parseFloat(inp_dPBaht != "" ? inp_dPBaht : "0");
				    					leftPerc = 100 - parseFloat(inp_downPayment != "" ? inp_downPayment : "0");
				    				}
					    			tempTd = $("#invoicing_table_details").find("td");
					    			tempTd[0].innerHTML = '<h5 style="display: inline-block;">Income:&nbsp;</h5>' + numFormat(parseFloat(inp_inco_evat));
						    		tempTd[1].innerHTML = '<h5 style="display: inline-block;">Sign Date:&nbsp;</h5>' + $("#inp_signDate").val();
						    		tempTd[2].innerHTML = '<h5 style="display: inline-block;">Down Payment:&nbsp;</h5>' + downPayment;
						    		tempTd[3].innerHTML = '<h5 style="display: inline-block;">End of Contract:&nbsp;</h5>' + $("#inp_endCont").val();
						    		tempTd[4].innerHTML = '<h5 style="display: inline-block;">Sales Balance:&nbsp;</h5><input type="text" class="span2 salesBal" value="' + salesBal + '" readonly>';
						    		tempTd[5].innerHTML = '<h5 style="display: inline-block;">Left (%):&nbsp;</h5><input type="text" class="span1 leftPerc" value="' + leftPerc + '" readonly>';
						    		$(".salesBal").number(true, 2);
						    		$(".leftPerc").number(true, 2);
						    		var tr = $('<tr class="invPlanAdd"><td style="text-align: center; vertical-align: middle;"></td><td></td><td style="vertical-align: middle;"></td><td></td><td></td><td><input class="invoicing_add_rema" type="text" style="width: 258px;"></td><td></td></tr>');
					    			var td0 = $('<i class="i_btn icon-plus-circle-1" title="Add"></i>');
					    			var td1 = $('<input class="invoicing_add_days" type="text" style="width: 30px;">');
					    			var td2 = $('<input class="invoicing_add_date" type="text" style="width: 70px;" readonly>');
					    			var td3 = $('<input class="invoicing_add_perc" type="text" style="text-align: center;width: 50px;">');
					    			var td4 = $('<input class="invoicing_add_amnt text-right" type="text" style="width: 100px;" readonly>');
							    	td1.number(true, 0);
							    	td3.number(true, 0);
							    	td4.number(true, 2);
							    	td0.on("click", function() {
							    		if ($(".invoicing_add_days").val() > 0 && $(".invoicing_add_perc").val() > 0 && $(".invoicing_add_amnt").val() > 0) {
							    			var tr = $('<tr><td style="text-align: center; vertical-align: middle;">' + ($("#invoicing_tbody_list").children().length) + '</td><td></td><td style="vertical-align: middle;"></td><td></td><td></td><td><input type="text" style="width: 258px;" value="' + $(".invoicing_add_rema").val() + '"></td><td style="text-align: center; vertical-align: middle;"></td></tr>');
							    			var td1 = $('<input type="text" style="width: 30px;" value="' + $(".invoicing_add_days").val() + '">');
							    			var td2 = $('<input type="text" style="width: 70px;" value="' + $(".invoicing_add_date").val() + '" readonly>');
							    			var td3 = $('<input type="text" style="width: 50px;text-align: center;" value="' + $(".invoicing_add_perc").val() + '">');
							    			var td4 = $('<input class="text-right" type="text" style="width: 100px;" value="' + $(".invoicing_add_amnt").val() + '" readonly>');
							    			var td6 = $('<i class="i_btn icon-minus-circled-1" title="Delete"></i>');
									    	td1.number(true, 0);
									    	td3.number(true, 0);
									    	td4.number(true, 2);
									    	td1.on("blur", function() {
												if ($(this).val() > 0 && parseInt($(this).val()) <= parseInt($("#inp_pDuration").val())) {
													var tempday = $("#inp_signDate").val().split("/");
													var slcdate=new Date(tempday[2],tempday[1]-1,tempday[0]);
													slcdate.setDate(slcdate.getDate() + parseInt($(this).val()));
													$($(this).closest("tr").find("input")[1]).val(("0" + slcdate.getDate()).slice(-2) + "/" + ("0" + (slcdate.getMonth() + 1)).slice(-2) + "/" + slcdate.getFullYear());
												} else {
													alert("จำนวนวันที่ระบุจะต้องมากกว่า 0 และไม่เกิน Project duration (" + $("#inp_pDuration").val() + " วัน)");
													$(this).val("1");
													$(this).blur();
												}
									    	});
									    	td3.on("blur", function() {
									    		tempTr = $("#invoicing_tbody_list").children();
									    		tempTrIndx = $(this).closest("tr").index();
									    		tempLeftPerc = tempSalesBal = 0.00;
									    		for (i = 0; i < (tempTr.length - 1); i++) {
									    			if (i != tempTrIndx) {
									    				tempLeftPerc += parseFloat($($(tempTr[i]).find("input")[2]).val() == "" ? "0.00" : $($(tempTr[i]).find("input")[2]).val());
									    				tempSalesBal += parseFloat($($(tempTr[i]).find("input")[3]).val() == "" ? "0.00" : $($(tempTr[i]).find("input")[3]).val());
									    			}
									    		}
									    		$(".leftPerc").val(leftPerc - tempLeftPerc);
									    		$(".salesBal").val(salesBal - tempSalesBal);
									    		if ($(this).val() > 0) {
									    			tempLeftPerc = parseFloat($(".leftPerc").val());
									    			if (parseFloat($(this).val()) >= tempLeftPerc) {
									    				if (parseFloat($(this).val()) > tempLeftPerc) alert("จำนวนที่ระบุเกิน % ที่เหลืออยู่ ระบบจะคำนวนค่าที่เหลือให้อัตโนมัติ");
									    				$(this).val(tempLeftPerc);
									    				$($(this).closest("tr").find("input")[3]).val($(".salesBal").val());
									    				$(".leftPerc").val("");
									    				$(".salesBal").val("");
									    			} else {
									    				$($(this).closest("tr").find("input")[3]).val((parseFloat(inp_inco_evat) / 100) * parseFloat($(this).val()));
									    				tempLeftPerc -= parseFloat($(this).val());
									    				tempSalesBal = parseFloat($(".salesBal").val()) - parseFloat($($(this).closest("tr").find("input")[3]).val());
									    				$(".leftPerc").val(tempLeftPerc);
									    				$(".salesBal").val(tempSalesBal);
									    			}
									    		} else  {
									    			alert("จำนวนที่ระบุจะต้องมากกว่า 0");
									    			$(this).val($(".leftPerc").val());
									    			$(this).blur();
									    		}
									    		$(".invoicing_add_perc").val(parseFloat($(".leftPerc").val()));
									    		$(".invoicing_add_amnt").val(parseFloat($(".salesBal").val()));
									    		//start edit by tua
									    		if($(".leftPerc").val()=="0")
									    		{
									    			console.log("leftPerc ",$(".leftPerc").val());
									    			$(".invPlanAdd").css("display", "none");
									    		}
									    		else
									    		{
									    			$(".invPlanAdd").css("display", "");
									    		}
									    		//end edit by tua
									    	});
									    	td4.on("blur", function() {
									    		tempTr = $("#invoicing_tbody_list").children();
									    		tempTrIndx = $(this).closest("tr").index();
									    		tempLeftPerc = tempSalesBal = 0.00;
									    		for (i = 0; i < (tempTr.length - 1); i++) {
									    			if (i != tempTrIndx) {
									    				tempLeftPerc += parseFloat($($(tempTr[i]).find("input")[2]).val() == "" ? "0.00" : $($(tempTr[i]).find("input")[2]).val());
									    				tempSalesBal += parseFloat($($(tempTr[i]).find("input")[3]).val() == "" ? "0.00" : $($(tempTr[i]).find("input")[3]).val());
									    			}
									    		}
									    		$(".leftPerc").val(leftPerc - tempLeftPerc);
									    		$(".salesBal").val(salesBal - tempSalesBal);
									    		if ($(this).val() > 0) {
									    			tempSalesBal = parseFloat($(".salesBal").val());
									    			if (parseFloat($(this).val()) >= tempSalesBal) {
									    				if (parseFloat($(this).val()) > tempSalesBal) alert("จำนวนที่ระบุเกินจำนวนที่เหลืออยู่ ระบบจะคำนวนค่าที่เหลือให้อัตโนมัติ");
									    				$(this).val(tempSalesBal);
									    				$($(this).closest("tr").find("input")[2]).val($(".leftPerc").val());
									    				$(".leftPerc").val("");
									    				$(".salesBal").val("");
									    			} else {
									    				$($(this).closest("tr").find("input")[2]).val((100 / parseFloat(inp_inco_evat)) * parseFloat($(this).val()));
									    				tempSalesBal -= parseFloat($(this).val());
									    				tempLeftPerc = parseFloat($(".leftPerc").val()) - parseFloat($($(this).closest("tr").find("input")[2]).val());
									    				$(".leftPerc").val(tempLeftPerc);
									    				$(".salesBal").val(tempSalesBal);
									    			}
									    		} else  {
									    			alert("จำนวนที่ระบุจะต้องมากกว่า 0");
									    			$(this).val($(".salesBal").val());
									    			$(this).blur();
									    		}
									    		$(".invoicing_add_perc").val(parseFloat($(".leftPerc").val()));
									    		$(".invoicing_add_amnt").val(parseFloat($(".salesBal").val()));
									    	});
											td6.on("click", function() {
												if (window.confirm("ต้องการลบรายการนี้หรือไม่")) {
													$(this).closest("tr").remove();
													tempTr = $("#invoicing_tbody_list").children();
										    		tempLeftPerc = tempSalesBal = 0.00;
										    		for (i = 0; i < (tempTr.length - 1); i++) {
										    			$($(tempTr[i]).find("td")[0]).text(i + 1);
									    				tempLeftPerc += parseFloat($($(tempTr[i]).find("input")[2]).val() == "" ? "0.00" : $($(tempTr[i]).find("input")[2]).val());
									    				tempSalesBal += parseFloat($($(tempTr[i]).find("input")[3]).val() == "" ? "0.00" : $($(tempTr[i]).find("input")[3]).val());
										    		}
										    		$(".leftPerc").val(leftPerc - tempLeftPerc);
										    		$(".salesBal").val(salesBal - tempSalesBal);
										    		$(".invoicing_add_perc").val(parseFloat($(".leftPerc").val()));
										    		$(".invoicing_add_amnt").val(parseFloat($(".salesBal").val()));
									    		//start edit by tua
									    		if($(".leftPerc").val()=="0")
									    		{
									    			console.log("leftPerc ",$(".leftPerc").val());
									    			$(".invPlanAdd").css("display", "none");
									    		}
									    		else
									    		{
									    			$(".invPlanAdd").css("display", "");
									    		}
									    		//end edit by tua
												}
											});
							    			$(tr.find("td")[1]).append(td1);
							    			$(tr.find("td")[2]).append(td2);
							    			$(tr.find("td")[3]).append(td3);
							    			$(tr.find("td")[4]).append(td4);
							    			$(tr.find("td")[6]).append(td6);
											$("#invoicing_tbody_list > tr:last-child").before(tr);

								    		tempTr = $("#invoicing_tbody_list").children();
								    		tempLeftPerc = tempSalesBal = 0.00;
								    		for (i = 0; i < (tempTr.length - 1); i++) {
							    				tempLeftPerc += parseFloat($($(tempTr[i]).find("input")[2]).val() == "" ? "0.00" : $($(tempTr[i]).find("input")[2]).val());
							    				tempSalesBal += parseFloat($($(tempTr[i]).find("input")[3]).val() == "" ? "0.00" : $($(tempTr[i]).find("input")[3]).val());
								    		}
								    		$(".leftPerc").val(leftPerc - tempLeftPerc);
								    		$(".salesBal").val(salesBal - tempSalesBal);
								    		$(".invoicing_add_days").val(inp_pDuration);
								    		$(".invoicing_add_days").blur();
								    		$(".invoicing_add_perc").val(parseFloat($(".leftPerc").val()));
								    		$(".invoicing_add_amnt").val(parseFloat($(".salesBal").val()));
								    		$(".invoicing_add_rema").val("");
									    		//start edit by tua
									    		if($(".leftPerc").val()=="0")
									    		{
									    			console.log("leftPerc ",$(".leftPerc").val());
									    			$(".invPlanAdd").css("display", "none");
									    		}
									    		else
									    		{
									    			$(".invPlanAdd").css("display", "");
									    		}
									    		//end edit by tua
							    		} else {
							    			alert("เพิ่มรายการไม่ได้เนื่องจาก Days, %, Amount มีค่าเท่ากับ 0 หรือไม่ได้ระบุค่า");
							    		}
							    	});
							    	td1.on("blur", function() {
										if ($(this).val() > 0 && parseInt($(this).val()) <= parseInt(inp_pDuration)) {
											var tempday = $("#inp_signDate").val().split("/");
											var slcdate=new Date(tempday[2],tempday[1]-1,tempday[0]);
											//slcdate = $("#inp_signDate").datepicker('getDate', '+1d');
											slcdate.setDate(slcdate.getDate() + parseInt($(this).val()));
											$($(this).closest("tr").find("input")[1]).val(("0" + slcdate.getDate()).slice(-2) + "/" + ("0" + (slcdate.getMonth() + 1)).slice(-2) + "/" + slcdate.getFullYear());
										} else {
											alert("จำนวนวันที่ระบุจะต้องมากกว่า 0 และไม่เกิน Project duration (" + $("#inp_pDuration").val() + " วัน)");
											$(this).val("1");
											$(this).blur();
										}
							    	});
							    	td3.on("blur", function() {
							    		if ($(this).val() > 0) {
							    			tempLeftPerc = parseFloat($(".leftPerc").val());
							    			if (parseFloat($(this).val()) >= tempLeftPerc) {
							    				$(this).val(tempLeftPerc);
							    				$($(this).closest("tr").find("input")[3]).val($(".salesBal").val());
							    			} else {
							    				$($(this).closest("tr").find("input")[3]).val((parseFloat(inp_inco_evat) / 100) * parseFloat($(this).val()));
							    			}
							    		} else  {
							    			alert("จำนวนที่ระบุจะต้องมากกว่า 0");
							    			$(this).val("");
							    			$($(this).closest("tr").find("input")[3]).val("");
							    		}
							    	});
							    	td4.on("blur", function() {
							    		if ($(this).val() > 0) {
							    			tempSalesBal = parseFloat($(".salesBal").val());
							    			if (parseFloat($(this).val()) >= tempSalesBal) {
							    				$(this).val(tempSalesBal);
							    				$($(this).closest("tr").find("input")[2]).val($(".leftPerc").val());
							    			} else {
							    				$($(this).closest("tr").find("input")[2]).val((100 / parseFloat(inp_inco_evat)) * parseFloat($(this).val()));
							    			}
							    		} else  {
							    			alert("จำนวนที่ระบุจะต้องมากกว่า 0");
							    			$(this).val("");
							    			$($(this).closest("tr").find("input")[2]).val("");
							    		}
							    	});
					    			$(tr.find("td")[0]).append(td0);
					    			$(tr.find("td")[1]).append(td1);
					    			$(tr.find("td")[2]).append(td2);
					    			$(tr.find("td")[3]).append(td3);
					    			$(tr.find("td")[4]).append(td4);
										$("#invoicing_tbody_list").append(tr);

					    			$("#tbody_invoicing").children().each(function() {
					    				if (parseFloat($(".leftPerc").val()) > 0) {
						    				tempP = $(this).find("p");
						    				if (parseInt($(tempP[1]).text()) > parseInt(inp_pDuration)) {
						    					$(".invoicing_add_days").val(inp_pDuration);
						    				} else {
						    					$(".invoicing_add_days").val($(tempP[1]).text());
						    				}
							    			$(".invoicing_add_days").blur();
						    				//$(".invoicing_add_date").val($(tempP[2]).text());
							    			$(".invoicing_add_perc").val($(tempP[3]).text());
							    			$(".invoicing_add_perc").blur();
							    			//$(".invoicing_add_amnt").val($(tempP[4]).text());
							    			$(".invoicing_add_rema").val($(tempP[5]).text());
							    			td0.click();
					    				}
				    				});

						    		$(".invoicing_add_days").val(inp_pDuration);
						    		$(".invoicing_add_days").blur();
						    		$(".invoicing_add_perc").val(parseFloat($(".leftPerc").val()));
						    		$(".invoicing_add_amnt").val(parseFloat($(".salesBal").val()));
						    		$(".invoicing_add_rema").val("");
						    		$("#invoicing_modal").modal("show");
					    		} else {
					    			alert("กรุณาระบุ Income, Sign date และ Project duration ก่อน");
					    		}
									$('.invoicing_add_days').keyup(function(event) {
										if(event.keyCode == 13) {
											$('.invoicing_add_perc').select();
										}
									});
									$('.invoicing_add_perc').keyup(function(event) {
										if(event.keyCode == 13) {
											$('.invoicing_add_rema').select();
										}
									}); 
								}   
								else
								{
									$("#invoicing_modal").modal("hide");
								}
	            },
	            error: function() {}
	        });
					
	    }
		function initQuickUpdate()
		{

			$.ajax({ 
				type: "POST",
				dataType: "json", 
				data: {			        	
			        	saleid: GlobalSaleID
			        },
		        url: "AJAX/LoadProject103_Sign.php",
		        success: function (json) {
		        	$("#sumResult").text(json.length);

						  var date = new Date();
							var today = date.getDate();
							var lastMonth = date.getMonth()+1;
							var year = date.getFullYear();
		        	console.log(json);
		        	$.each(json, function() {
								var item = this;
								var currentProgressPiority =parseInt(item[15]);
		        		$tr = $("<tr>");
		        		/*$td0 = $("<td>").appendTo($tr);	
				        if(item[16]==1)
				        {
				        	$btnSendmail=$("<i>").attr({ title:'แจ้งเตือน',style:'color:#DD1144'}).addClass("icon-alert");
				        	$btnSendmail.appendTo($td0);
				    	}*/

		        		/* project */
				        $td1 = $("<td>").appendTo($tr);			
				        	$td1.attr({ title:'',style:'width:170px'});	        
				        //$span1 = $("<a>").text(item[1]).appendTo($td1);
				        
				        	$a1=$("<a>");
									var datestatus = 0;
									
									if(item[18] != null)
									datestatus=item[18].split("/");
									if(today>=25)
									{
										if(datestatus[1]==lastMonth && datestatus[2]==year)
										{
											if(item[16]==1)
							        		{
								        	$btnSendmail=$("<i>").attr({ title:'แจ้งเตือนจาก Manager',style:'color:#DD1144;font-size:20px;'}).addClass("icon-alert");
								        	$btnSendmail.appendTo($td1);
						        			$a1.attr({ style:'color:#DD1144'});	
								    		}
										}
										else
										{
											$btnAlert=$("<i>").attr({ title:'ยังไม่ได้ Update Status',style:'color:#FF3333;font-size:20px;'}).addClass("icon-bell");
				        			$btnAlert.appendTo($td1);
					        		$a1.attr({ style:'color:#FF3333'});	
										}
									}
				        	$a1.attr("href","javascript:OpenProject('"+item[14]+"')");				        	
				        	$a1.attr("title","Income : "+numFormat(parseFloat(item[17]).toFixed(2)));
				        	//$a1.attr("title","click to edit project");
				        	$a1.text(item[1]);	
				        	$a1.appendTo($td1);	

		        		/* potential */
				        $td2 = $("<td>").appendTo($tr);	
				        	$td2.attr({ title:'',style:'width:50px'});	
					        $a2=$("<a>");
					        $a2.attr("data-original-title","Select potential");
					        $a2.attr("data-source","{'100': '100','75': '75','50': '50','25': '25','10': '10'}");	
					        $a2.attr("data-value",item[2]);	
					        $a2.attr("data-pk",item[0]);	
					        $a2.attr("data-url","post.php");	
					        $a2.attr("data-name","potential");
					        $a2.attr("data-placement","bottom");			
					        $a2.attr("data-type","select");
					        $a2.attr("data-helpblock","100% มั่นใจว่าได้แน่ <br/>75% มั่นใจ <br/>50% ไม่แน่ใจว่าจะได้โครงการหรือไม่ <br/>25% พอมีโอกาส <br/>10% โอกาสน้อยมาก");		
					        $a2.attr("href","#");	
					        $a2.attr("class","editable potential");	
					        $a2.appendTo($td2);	        
				        $td3 = $("<td>").appendTo($tr);	
				        	$td3.attr({ title:'',style:'width:50px'});
					        $a3=$("<a>");
					        $a3.attr("data-original-title","Select Progress");
					        $a3.attr("data-source","{'100': '100','90': '90','80': '80','70': '70','60': '60','50': '50','40': '40','30': '30','20': '20','10': '10','0': '0','P': 'P','v': 'v'}");	
					        $a3.attr("data-value",item[3]);	
					        $a3.attr("data-pk",item[0]);	
					        $a3.attr("data-url","post.php");	
					        $a3.attr("data-name","progress");	
					        $a3.attr("data-placement","bottom");
					        $a3.attr("data-type","select");	
					        $a3.attr("data-helpblock","100% signdate <br/>90% ประกาศผลแล้วรอเซ็นสัญญา <br/>80% ยื่นซองแล้ว <br/>70% Bidding date <br/>60% TOR Final <br/>50% ได้งบประมาณ <br/>40% ขั้นตอนการพิจารณาอนุมัติงบประมาณ <br/>30% ร่างโครงการ (ของบประมาณ) <br/>20% Presentation/Demo <br/>10% Build solution <br/>0% แพ้ <br/>P Pending<br/>v ยกเลิกโครงการ");	
					        $a3.attr("href","#");	
					        $a3.attr("class","editable progress");	
					        $a3.appendTo($td3);	
				        $td4 = $("<td>").appendTo($tr);	
				        	$td4.attr({ title:'',style:'width:80px'});
					        $a4=	$("<a>");
					        $a4.attr("data-original-title","Update status");
					        $a4.attr("data-placeholder","null");	
					        $a4.attr("data-pk",item[0]);	
					        $a4.attr("data-url","post.php");	
					        $a4.attr("data-name","status");	
					        $a4.attr("data-type","textarea");	
					        $a4.attr("href","#");	
					        $a4.attr("class","editable status");	
					        $a4.appendTo($td4);	
				        $td5 = $("<td>").appendTo($tr);	
				        	$td5.attr({ title:'',style:'width:70px'});
					        $a5=	$("<a>");
					        $a5.attr("data-original-title","Select Date");
					        $a5.attr("data-viewformat","dd/mm/yyyy");
					        //$a5.attr("data-value",item[4]);		
					        $a5.attr("data-pk",item[0]);	
					        $a5.attr("data-url","post.php");	
					        $a5.attr("data-name","progress10");	
					        $a5.attr("data-type","date");	
					        $a5.attr("href","#");	
					        $a5.attr("class","editable dob");
					        $a5.text(item[4]);	
					        $a5.appendTo($td5);	
				        $td6 = $("<td>").appendTo($tr);	
				        	$td6.attr({ title:'',style:'width:70px'});
					        $a6=	$("<a>");
					        $a6.attr("data-original-title","Select Date");
					        $a6.attr("data-viewformat","dd/mm/yyyy");
					        //$a6.attr("data-value",item[5]);		
					        $a6.attr("data-pk",item[0]);	
					        $a6.attr("data-url","post.php");	
					        $a6.attr("data-name","progress20");	
					        $a6.attr("data-type","date");	
					        $a6.attr("href","#");	
					        $a6.attr("class","editable dob");
					        $a6.text(item[5]);		
					        $a6.appendTo($td6);
				        $td7 = $("<td>").appendTo($tr);	
				        	$td7.attr({ title:'',style:'width:70px'});
					        $a7=	$("<a>");
					        $a7.attr("data-original-title","Select Date");
					        $a7.attr("data-viewformat","dd/mm/yyyy");
					        //$a7.attr("data-value",item[6]);		
					        $a7.attr("data-pk",item[0]);	
					        $a7.attr("data-url","post.php");	
					        $a7.attr("data-name","progress30");	
					        $a7.attr("data-type","date");	
					        $a7.attr("href","#");	
					        $a7.attr("class","editable dob");
					        $a7.text(item[6]);		
					        $a7.appendTo($td7);
				        $td8 = $("<td>").appendTo($tr);
				        	$td8.attr({ title:'',style:'width:70px'});
					        $a8=	$("<a>");
					        $a8.attr("data-original-title","Select Date");
					        $a8.attr("data-viewformat","dd/mm/yyyy");
					        //$a8.attr("data-value",item[7]);		
					        $a8.attr("data-pk",item[0]);	
					        $a8.attr("data-url","post.php");	
					        $a8.attr("data-name","progress40");	
					        $a8.attr("data-type","date");	
					        $a8.attr("href","#");	
					        $a8.attr("class","editable dob");	
					        $a8.text(item[7]);	
					        $a8.appendTo($td8);
				        $td9 = $("<td>").appendTo($tr);	
				        	$td9.attr({ title:'',style:'width:70px'});
					        $a9=	$("<a>");
					        $a9.attr("data-original-title","Select Date");
					        $a9.attr("data-viewformat","dd/mm/yyyy");
					        //$a9.attr("data-value",item[8]);		
					        $a9.attr("data-pk",item[0]);	
					        $a9.attr("data-url","post.php");	
					        $a9.attr("data-name","progress50");	
					        $a9.attr("data-type","date");	
					        $a9.attr("href","#");	
					        $a9.attr("class","editable dob");	
					        $a9.text(item[8]);	
					        $a9.appendTo($td9);
				        $td10 = $("<td>").appendTo($tr);
				        	$td10.attr({ title:'',style:'width:70px'});
					        $a10=	$("<a>");
					        $a10.attr("data-original-title","Select Date");
					        $a10.attr("data-viewformat","dd/mm/yyyy");
					        //$a10.attr("data-value",item[9]);		
					        $a10.attr("data-pk",item[0]);	
					        $a10.attr("data-url","post.php");	
					        $a10.attr("data-name","progress60");	
					        $a10.attr("data-type","date");	
					        $a10.attr("href","#");	
					        $a10.attr("class","editable dob");	
					        $a10.text(item[9]);	
					        $a10.appendTo($td10);	
				        $td11 = $("<td>").appendTo($tr);
				        	$td11.attr({ title:'',style:'width:70px'});
					        $a11=	$("<a>");
					        $a11.attr("data-original-title","Select Date");
					        $a11.attr("data-viewformat","dd/mm/yyyy");
					        //$a11.attr("data-value",item[10]);		
					        $a11.attr("data-pk",item[0]);	
					        $a11.attr("data-url","post.php");	
					        $a11.attr("data-name","progress70");	
					        $a11.attr("data-type","date");	
					        $a11.attr("data-income",item[17]);
					        $a11.attr("href","#");	
					        $a11.attr("class","editable dob");	
					        $a11.text(item[10]);	
					        $a11.appendTo($td11);	
				        $td12 = $("<td>").appendTo($tr);
				        	$td12.attr({ title:'',style:'width:70px'});
					        $a12=	$("<a>");
					        $a12.attr("data-original-title","Select Date");
					        $a12.attr("data-viewformat","dd/mm/yyyy");
					        //$a12.attr("data-value",item[11]);		
					        $a12.attr("data-pk",item[0]);	
					        $a12.attr("data-url","post.php");	
					        $a12.attr("data-name","progress80");	
					        $a12.attr("data-type","date");	
					        $a12.attr("href","#");	
					        $a12.attr("class","progress80 editable dob");	
					        $a12.text(item[11]);	
					        $a12.appendTo($td12);	
				        $td13 = $("<td>").appendTo($tr);
				        	$td13.attr({ title:'',style:'width:70px'});
					        $a13=	$("<a>");
					        $a13.attr("data-original-title","Select Date");
					        $a13.attr("data-viewformat","dd/mm/yyyy");
					        //$a13.attr("data-value",item[12]);		
					        $a13.attr("data-pk",item[0]);	
					        $a13.attr("data-url","post.php");	
					        $a13.attr("data-name","progress90");	
					        $a13.attr("data-type","date");	
					        $a13.attr("href","#");	
					        $a13.attr("class","progress90 editable dob");	
					        $a13.text(item[12]);	
					        $a13.appendTo($td13);	
				        $td14 = $("<td>").appendTo($tr);
				        	$td14.attr({ title:'',style:'width:70px'});
					        $a14=	$("<a>");
					        $a14.attr("data-original-title","Select Date");
					        $a14.attr("data-viewformat","dd/mm/yyyy");
					        $a14.attr("data-placement","left");		
					        $a14.attr("data-pk",item[0]);	
					        $a14.attr("data-url","post.php");	
					        $a14.attr("data-name","progress100");	
					        $a14.attr("data-type","date");	
					        $a14.attr("data-income",item[17]);
					        $a14.attr("href","#");	
					        $a14.attr("class","progress100 editable dob");
					        $a14.text(item[13]);		
					        $a14.appendTo($td14);	

				        if(item[16]!=0)
		        		{
		        			//$td0.attr({style:'background-color:#F9E5E7'});
		        			$td1.attr({style:'background-color:#F9E5E7'});
		        			$td2.attr({style:'background-color:#F9E5E7'});
		        			$td3.attr({style:'background-color:#F9E5E7'});
		        			$td4.attr({style:'background-color:#F9E5E7'});
		        			$td5.attr({style:'background-color:#F9E5E7'});
		        			$td6.attr({style:'background-color:#F9E5E7'});
		        			$td7.attr({style:'background-color:#F9E5E7'});
		        			$td8.attr({style:'background-color:#F9E5E7'});
		        			$td9.attr({style:'background-color:#F9E5E7'});
		        			$td10.attr({style:'background-color:#F9E5E7'});
		        			$td11.attr({style:'background-color:#F9E5E7'});
		        			$td12.attr({style:'background-color:#F9E5E7'});
		        			$td13.attr({style:'background-color:#F9E5E7'});
		        			$td14.attr({style:'background-color:#F9E5E7'});
		        			
		        		}	
		        		if(currentProgressPiority==10)
		        		{
		        			$td5.attr({style:'background-color:#FFFEDB'});
		        		}
		        		else if(currentProgressPiority==9)
		        		{
		        			$td6.attr({style:'background-color:#FFFEDB'});
		        			
		        		}
		        		else if(currentProgressPiority==8)
		        		{
		        			$td7.attr({style:'background-color:#FFFEDB'});
		        			
		        		}
		        		else if(currentProgressPiority==7)
		        		{
		        			$td8.attr({style:'background-color:#FFFEDB'});
		        			
		        		}
		        		else if(currentProgressPiority==6)
		        		{
		        			$td9.attr({style:'background-color:#FFFEDB'});
		        			
		        		}
		        		else if(currentProgressPiority==5)
		        		{
		        			$td10.attr({style:'background-color:#FFFEDB'});
		        			
		        		}
		        		else if(currentProgressPiority==4)
		        		{
		        			$td11.attr({style:'background-color:#FFFEDB'});
		        			
		        		}
		        		else if(currentProgressPiority==3)
		        		{
		        			$td12.attr({style:'background-color:#FFFEDB'});
		        			
		        		}
		        		else if(currentProgressPiority==2)
		        		{
		        			$td13.attr({style:'background-color:#FFFEDB'});
		        			
		        		}
		        		else if(currentProgressPiority==1)
		        		{
		        			$td14.attr({style:'background-color:#FFFEDB'});
		        			
		        		}
		        		
				        $("#detail").append($tr);	
				        $('.potential').editable({}); 
            			$('.progress').editable({}); 
				        $('.dob').editable({}); 
            			$('.status').editable({}); 		        		
					});
					$("#loading").hide();
					//main.js to update file
		        },
				error: function() {
					alert("เกิดปัญหาในการโหลดข้อมูลกรุณารีเฟรชหน้าเว็บอีกครั้ง");
					$("#loading").hide();
				}
		    });
		}
				function search(){
					//$("#loading").show();
					var Keyword = document.getElementById("txtKeyword").value;

				}
				function getSearch(json) {
            	    console.log("json:",json);	
					document.getElementById('sumResult1').innerHTML=json.items.length;
					$('#detail1 > tbody:last').empty();
					if(json.items.length== 0){
						var noresult = "<tr><td colspan='6' style='text-align:center;'><p class='lead14'>ไม่พบข้อมูล</p></td></tr>";
						$('#detail1').append(noresult);
					}else{
						for(i=0;i<json.items.length;i++)
						{
							var organizationid = '"'+json.items[i].organizationid+'"';
							var thaioganization = (json.items[i].thaioganization!= null)?json.items[i].thaioganization:"";
							var organization = (json.items[i].organization!= null)?json.items[i].organization:" ";
							var thaidepartment = (json.items[i].thaidepartment!= null)?json.items[i].thaidepartment:"";
							var department = (json.items[i].department!= null)?json.items[i].department:"";
							var thaidivision = (json.items[i].thaidivision!= null)?json.items[i].thaidivision:"";
							var division = (json.items[i].division!= null)?json.items[i].division:" ";
							
							var cols="";
							cols+="<tr><td><p class='lead14'>"+json.items[i].organizationinitial+"</p></td>";
							cols+="<td><p class='lead14'>"+thaioganization+"<br>"+organization+"</p></td>";
							cols+="<td><p class='lead14'>"+thaidepartment+"<br>"+department+"</p></td>";
							cols+="<td><p class='lead14'>"+thaidivision+"<br>"+division+"</p></td>";
							if(division.replace(" ","") != "" || thaidivision.replace(" ","") != ""){
								cols+="<td style='text-align:center'></td>";
									
							}else if(department.replace(" ","") != "" || thaidepartment.replace(" ","") != ""){
								cols+="<td style='text-align:center'><a href='javascript:addDiv("+organizationid+",1);' ><i class='i_btn icon-plus-circle iconAddDiv' ></i></a></td>";
								
							}else{
								cols+="<td style='text-align:center'><a href='javascript:addDep("+organizationid+",1);' ><i class='i_btn icon-plus-circle iconAddDep'></i></a></td>";
							}
							cols+="<td style='text-align:center'><a href='javascript:edit("+organizationid+",1);' ><i class='i_btn icon-pencil-1 iconEdit' ></i></a></td></tr>";
							$('#detail1').append(cols);
							
						}
					$('.iconAddDep').tooltip({
						placement: 'bottom',
						title : 'Add Department'
					});
					$('.iconAddDiv').tooltip({
						placement: 'bottom',
						title : 'Add Division'
					});
					$('.iconEdit').tooltip({
						placement: 'bottom',
						title : 'Edit'
					});
					}
					$("#loading").hide();
				}
								
				function edit(id,site){
					setCookie("Ses_Keyword_index",keywordHome);
					setCookie("Ses_Site",site);
					window.location = "new.php?id=" + id + "&page=home";
				}
				function addDep(id,site){
					setCookie("Ses_Keyword_index",keywordHome);
					setCookie("Ses_Site",site);
					window.location = "new.php?org=" + parseInt(id.substr(0, 4)) + "&page=home";
				}
				function addDiv(id,site){
					setCookie("Ses_Keyword_index",keywordHome);
					setCookie("Ses_Site",site);
					window.location = "new.php?org=" + parseInt(id.substr(0, 4)) + "&dep=" + parseInt(id.substr(4, 3)) + "&page=home";
				}
				
		

	  $(document).ready(function() {
	    	$("#loading").hide();        

			$(".form_date").datepicker({
		        dateFormat: 'dd/mm/yy',
		        changeYear: true,
		        beforeShow:function(input) {
			        $(input).css({
			            "position": "relative",
			            "z-index": 999
			        });
    				}
	    	});
				initQuickUpdate();
				$('#setUp').scrollToFixed();
				$("#invoicing_btn_ok").on("click", function() {
	    		if (parseFloat($(".salesBal").val()) > 0 && parseFloat($(".leftPerc").val()) > 0) {
	    			alert("ยังเหลือยอดที่ยังไม่ได้ระบุในงวดอยู่ โปรดระบุยอดที่ค้างอยู่ให้หมด");
	    		} else {
	    			$("#tbody_invoicing").empty();
	    			$("#tbody_bank").empty();
	    			var phaseNo = 1;
	    			var tempArr = {};

			    	var tempstart = $("#inp_signDate").val().split("/")[2];
			    	var tempend = $("#inp_endCont").val().split("/")[2];
			    	for(var iii=tempstart;iii<=tempend;iii++)
			    	{
			    		tempArr[iii]=0;
			    	}
	    			if ($("#chk_downPayment").prop("checked") && parseFloat($("#inp_dPBaht").val()) > 0) {
	    				if ($("#inp_dPDate").val() != "") {
	    					tempArr[$("#inp_dPDate").datepicker('getDate', '+1d').getFullYear()] = parseFloat($("#inp_dPBaht").val()).toFixed(2);
	    				} else {
	    					tempArr[$("#inp_signDate").datepicker('getDate', '+1d').getFullYear()] = parseFloat($("#inp_dPBaht").val()).toFixed(2);
	    				}
	    			}
	    			$("#invoicing_tbody_list tr:not(:last-child)").each(function() {
	    				tempInps = $(this).find("input");
	    				tempYear = $(tempInps[1]).val().split("/")[2];
	    				if (tempArr[tempYear] > 0) {
	    					tempArr[tempYear] = (parseFloat(tempArr[tempYear]) + parseFloat($(tempInps[3]).val())).toFixed(2);
	    				} else {
	    					tempArr[tempYear] = parseFloat($(tempInps[3]).val()).toFixed(2);
	    				}
	    				tr = '<tr class="warning"><td><p class="lead14">' + phaseNo++ + '</p></td><td><p class="lead14">' + $(tempInps[0]).val() + '</p></td><td><p class="lead14">' + $(tempInps[1]).val() + '</p></td><td><p class="lead14 text-right">' + numFormat(parseFloat($(tempInps[2]).val()).toFixed(2)) + '</p></td><td><p class="lead14 text-right">' + numFormat(parseFloat($(tempInps[3]).val()).toFixed(2)) + '</p></td><td><p class="lead14">' + $(tempInps[4]).val() + '</p></td></tr>';
	    				$("#tbody_invoicing").append(tr);
    				});
    				for (tempKey in tempArr) {
    					tempType = "Book";
    					if (parseInt(tempKey) > $("#inp_signDate").val().split("/")[2]) tempType = "Forward";
    					tr = '<tr class="error"><td><p class="lead14">' + tempType + '</p></td><td><p class="lead14">' + tempKey + '</p></td><td><p class="lead14">' + numFormat(parseFloat(tempArr[tempKey]).toFixed(2)) + '</p></td></tr>';
    					$("#tbody_bank").append(tr);
    				}
    				//bookforwardNo();
    				IDForecast=$("#inp_ForecastID").val();
    				jsonInvo="";
						Book = 0.00;
						Forward = 0.00;
						Forward2 = 0.00;
						Forward3 = 0.00;
	    			if ($("#tbody_invoicing").children().length > 0) {
							itemsArray = [];
							$("#tbody_invoicing").children().each(function() {
								eachitemArray = [];
								tempP = $(this).find("p");
								eachitemArray.push($(tempP[0]).text());
								eachitemArray.push($(tempP[1]).text());
								if ($(tempP[2]).text() != "") {
									tempDate = $(tempP[2]).text().split("/");
									tempDate = "'" + tempDate[2] + "-" + tempDate[1] + "-" + tempDate[0] + "'";
								} else {
									tempDate = "NULL";
								}
								eachitemArray.push(tempDate);
								eachitemArray.push($(tempP[4]).text().replace(/,/g, ""));
								eachitemArray.push($(tempP[3]).text());
								eachitemArray.push($(tempP[5]).text());
								itemsArray.push(eachitemArray);
							});
							jsonInvo = JSON.stringify(itemsArray);
						}
						tempDate = $("#inp_signDate").val().split("/")[2];//Edit Tua 18/12/57
						$("#tbody_bank").children().each(function() {
							tempYear = parseInt($($(this).find("p")[1]).text());
							tempPrice = parseFloat($($(this).find("p")[2]).text().replace(/,/g, ""));
							if (tempYear <= tempDate) {
								Book += tempPrice;
							} else if ((tempYear - tempDate) == 1) {
								Forward += tempPrice;
							} else if ((tempYear - tempDate) == 2) {
								Forward2 += tempPrice;
							} else if ((tempYear - tempDate) == 3) {
								Forward3 += tempPrice;
							}
						});
						$.ajax({
							type: "POST",
							dataType: "json",
							url: "AJAX/saveSmart.php",
							data: {
								IDForecast: IDForecast,
								Book: Book,
								Forward: Forward,
								Forward2: Forward2,
								Forward3: Forward3,
								jsonInvo:jsonInvo
							},
							success: function(json) {
								alert("บันทึกข้อมูล Invoicing Plan เรียบร้อย");
			    			$("#invoicing_modal").modal("hide");
			    		},
			    		error: function() {
								//alert("เกิดปัญหาในการบันทึกกรุณาลองใหม่อีกครั้ง 3");
								//$("#loading").hide();
			    			$("#invoicing_modal").modal("hide");
							}
						});
	    		}
	    	});
			
		});
//------------------------------------------------- Function ------------------------------------------------
		function numFormat(x) {
				var parts = x.toString().split(".");
			    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			    return parts.join(".");
		}
//-----------------------------------------------------------------------------------------------------------
	</script>

</body>
</html>