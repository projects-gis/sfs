<?php 
    $status = strtolower($_GET['status']);
    $data = json_decode(file_get_contents('php://input'), true);
    //echo "\n==================================\n";
    for ($i=0; $i < count($data); $i++) { 
        //echo "Record:" . ($i+1) . "\n ------------";
        $table = $data[$i]["table"];
        $id = $data[$i]["id"];
        $code = $data[$i]["code"];
        $name = $data[$i]["name"];
        $createdate = $data[$i]["createdate"];
        $createby = $data[$i]["createby"];
        $updatedate = $data[$i]["updatedate"];
        $updateby = $data[$i]["updateby"];
        $producttypeid = $data[$i]["producttypeid"];
        $vehiclemodelid = $data[$i]["vehiclemodelid"];
        $productcatalogid = $data[$i]["productcatalogid"];
        if($table){
            include("INC/connectSFC.php");
            if($table == 1){//CSD_ProductTypes  --1  
                if ($status == "delete") {
                    //delete
                    $sqlStrDelete = "DELETE FROM CSD_ProductTypes WHERE Id = $id";
                    sqlsrv_query($ConnectSaleForecast,$sqlStrDelete);        
                }

                //=================== Check Old Data For Insert / Update ===================
                 $sqlStrSelect = "SELECT Id FROM CSD_ProductTypes WHERE Id = $id";
                 $oldId=0;
                 $querySelect = sqlsrv_query($ConnectSaleForecast,$sqlStrSelect);
                if ($obj = sqlsrv_fetch_object($querySelect)) {
                     $oldId = $obj->Id;
                } 
                 
                if($oldId!=0){
                     $setUpData = "";
                     if($code){$setUpData .= "Code ='$code'";}
                     if($name){$setUpData .= ",Name ='$name'";}
                     if($createdate){$setUpData .= ",CreateDate ='$createdate'";}
                     if($createby){$setUpData .= ",CreateBy ='$createby'";}
                     if($updatedate){$setUpData .= ",UpdateDate ='$updatedate'";}
                     if($updateby){$setUpData .= ",UpdateBy ='$updateby'";}
                     $sqlStrUpdate = "UPDATE CSD_ProductTypes SET $setUpData WHERE Id = $id";
                     $query = sqlsrv_query($ConnectSaleForecast,$sqlStrUpdate);        
                }
                else{
                     if($id){
                         $sqlStrInsert = "INSERT INTO CSD_ProductTypes(Id,Code,Name,CreateDate,CreateBy,UpdateDate,UpdateBy) VALUES ('$id','$code','$name','$createdate','$createby','$updatedate','$updateby')";
                         sqlsrv_query($ConnectSaleForecast,$sqlStrInsert);
                     }
                }
                //============================================================================
                if($status == "view"){
                    $sqlStrSelect = "SELECT * FROM CSD_ProductTypes";
                    $querySelect = sqlsrv_query($ConnectSaleForecast,$sqlStrSelect);
                    $results = array();
                    $r = 0;
                    while ($obj = sqlsrv_fetch_object($querySelect)) {
                        $results[$r] = $obj;
                        $r++;
                    }
                    echo json_encode($results);
                }
                 
            }

            if($table == 2){//CSD_ProductCatalogs --2
                if ($status == "delete") {
                      //delete
                    $sqlStrDelete = "DELETE FROM CSD_ProductCatalogs WHERE Id = $id";
                    sqlsrv_query($ConnectSaleForecast,$sqlStrDelete);
                }

                //=================== Check Old Data For Insert / Update ===================
                $sqlStrSelect = "SELECT Id FROM CSD_ProductCatalogs WHERE Id = $id";
                $oldId=0;
                $querySelect = sqlsrv_query($ConnectSaleForecast,$sqlStrSelect);
                if ($obj = sqlsrv_fetch_object($querySelect)) {
                    $oldId = $obj->Id;
                } 
                
                if($oldId!=0){
                    $setUpData = "";
                    if($code){$setUpData .= "Code ='$code'";}
                    if($name){$setUpData .= ",Name ='$name'";}
                    if($createdate){$setUpData .= ",CreateDate ='$createdate'";}
                    if($createby){$setUpData .= ",CreateBy ='$createby'";}
                    if($updatedate){$setUpData .= ",UpdateDate ='$updatedate'";}
                    if($updateby){$setUpData .= ",UpdateBy ='$updateby'";}
                    if($producttypeid){$setUpData .= ",ProductTypeId ='$producttypeid'";}
                    if($vehiclemodelid){$setUpData .= ",VehiccleModelId ='$vehiclemodelid'";}
                    $sqlStrUpdate = "UPDATE CSD_ProductCatalogs SET $setUpData WHERE Id = $id";
                    sqlsrv_query($ConnectSaleForecast,$sqlStrUpdate);            
                }
                else{
                    if($id){
                        $sqlStrInsert = "INSERT INTO CSD_ProductCatalogs(Id,Code,Name,CreateDate,CreateBy,UpdateDate,UpdateBy,ProductTypeId,VehiccleModelId) VALUES ('$id','$code','$name','$createdate','$createby','$updatedate','$updateby','$producttypeid','$vehiclemodelid')";
                        sqlsrv_query($ConnectSaleForecast,$sqlStrInsert);
                    }
                }
                //============================================================================
                if($status == "view"){
                    $sqlStrSelect = "SELECT * FROM CSD_ProductCatalogs";
                    $querySelect = sqlsrv_query($ConnectSaleForecast,$sqlStrSelect);
                    $results = array();
                    $r = 0;
                    while ($obj = sqlsrv_fetch_object($querySelect)) {
                        $results[$r] = $obj;
                        $r++;
                    }
                    echo json_encode($results);
                }
            }

            if($table == 3){//CSD_VehicleModels --3
                if ($status == "delete") {
                    //delete
                    $sqlStrDelete = "DELETE FROM CSD_VehicleModels WHERE Id = $id";
                    sqlsrv_query($ConnectSaleForecast,$sqlStrDelete);
                }

                //=================== Check Old Data For Insert / Update ===================
                $sqlStrSelect = "SELECT Id FROM CSD_VehicleModels WHERE Id = $id";
                $oldId=0;
                $querySelect = sqlsrv_query($ConnectSaleForecast,$sqlStrSelect);
                if ($obj = sqlsrv_fetch_object($querySelect)) {
                    $oldId = $obj->Id;
                } 
                
                if($oldId!=0){
                    $setUpData = "";
                    if($code){$setUpData .= "Code ='$code'";}
                    if($name){$setUpData .= ",Name ='$name'";}
                    if($createdate){$setUpData .= ",CreateDate ='$createdate'";}
                    if($createby){$setUpData .= ",CreateBy ='$createby'";}
                    if($updatedate){$setUpData .= ",UpdateDate ='$updatedate'";}
                    if($updateby){$setUpData .= ",UpdateBy ='$updateby'";}
                    $sqlStrUpdate = "UPDATE CSD_VehicleModels SET $setUpData WHERE Id = $id";
                    sqlsrv_query($ConnectSaleForecast,$sqlStrUpdate);             
                }
                else{
                    if($id){
                        $sqlStrInsert = "INSERT INTO CSD_VehicleModels(Id,Code,Name,CreateDate,CreateBy,UpdateDate,UpdateBy) VALUES ('$id','$code','$name','$createdate','$createby','$updatedate','$updateby')";
                        sqlsrv_query($ConnectSaleForecast,$sqlStrInsert);
                    }
                }
                //============================================================================   
                if($status == "view"){
                    $sqlStrSelect = "SELECT * FROM CSD_VehicleModels";
                    $querySelect = sqlsrv_query($ConnectSaleForecast,$sqlStrSelect);
                    $results = array();
                    $r = 0;
                    while ($obj = sqlsrv_fetch_object($querySelect)) {
                        $results[$r] = $obj;
                        $r++;
                    }
                    echo json_encode($results);
                }
            }

            if($table == 4){//CSD_BoxTemplates --4
                if ($status == "delete") {
                    //delete
                    $sqlStrDelete = "DELETE FROM CSD_BoxTemplates WHERE Id = $id";
                    sqlsrv_query($ConnectSaleForecast,$sqlStrDelete);
                }
                
                //=================== Check Old Data For Insert / Update ===================
                $sqlStrSelect = "SELECT Id FROM CSD_BoxTemplates WHERE Id = $id";
                $oldId=0;
                $querySelect = sqlsrv_query($ConnectSaleForecast,$sqlStrSelect);
                if ($obj = sqlsrv_fetch_object($querySelect)) {
                    $oldId = $obj->Id;
                } 
                
                if($oldId!=0){
                    $setUpData = "";
                    if($code){$setUpData .= "Code ='$code'";}
                    if($name){$setUpData .= ",Name ='$name'";}
                    if($createdate){$setUpData .= ",CreateDate ='$createdate'";}
                    if($createby){$setUpData .= ",CreateBy ='$createby'";}
                    if($updatedate){$setUpData .= ",UpdateDate ='$updatedate'";}
                    if($updateby){$setUpData .= ",UpdateBy ='$updateby'";}
                    if($productcatalogid){$setUpData .= ",ProductCatalogId ='$productcatalogid'";}
                    $sqlStrUpdate = "UPDATE CSD_BoxTemplates SET $setUpData WHERE Id = $id";
                    sqlsrv_query($ConnectSaleForecast,$sqlStrUpdate);     
                }
                else{
                    if($id){
                        $sqlStrInsert = "INSERT INTO CSD_BoxTemplates(Id,Code,Name,CreateDate,CreateBy,UpdateDate,UpdateBy,ProductCatalogId) VALUES ('$id','$code','$name','$createdate','$createby','$updatedate','$updateby','$productcatalogid')";
                        sqlsrv_query($ConnectSaleForecast,$sqlStrInsert);
                    }
                }
                //============================================================================   

                if($status == "view"){
                    $sqlStrSelect = "SELECT * FROM CSD_BoxTemplates";
                    $querySelect = sqlsrv_query($ConnectSaleForecast,$sqlStrSelect);
                    $results = array();
                    $r = 0;
                    while ($obj = sqlsrv_fetch_object($querySelect)) {
                        $results[$r] = $obj;
                        $r++;
                    }
                    echo json_encode($results);
                }
            }           
        }

        else{
            //echo "table Is Required.";
        }
    }
?>