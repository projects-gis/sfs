<?php  
function ThaiIToUTF8($in) {
	$out = ""; 
	for ($i = 0; $i < strlen($in); $i++) 
	{
		if (ord($in[$i]) <= 126) 
		$out .= $in[$i];
	else 
		$out .= "&#" . (ord($in[$i]) - 161 + 3585) . ";"; 
	} 
	return $out; 
} 
function funcSaleIn($id)
{
	$myid=explode(",",$id);
	$myCount=count($myid);
	$mythiname="";
	for($i=0;$i<$myCount;$i++)
	{
		if($i===0)
		{
			$mythiname="'".$myid[$i]."'";
		}
		else
		{
			$mythiname=$mythiname.",'".$myid[$i]."'";
		}
	}
	return $mythiname;
}
function funcSpecialIn($id)
	{
		$myid=explode(",",$id);
		$myCount=count($myid);
		$mythiname="";
		for($i=0;$i<$myCount;$i++)
		{
			if($i==0)
			{
				$mythiname="'".$myid[$i]."'";
			}
			else
			{
				$mythiname=$mythiname.",'".$myid[$i]."'";
			}
		}
		return $mythiname;
	}
function funcProgressIn($id)
	{
		$myid=explode(",",$id);
		$myCount=count($myid);
		$mythiname="";
		if($myCount==1)
		{
			$mythiname="'".$id."'";
		}
		else
		{
			for($i=0;$i<$myCount;$i++)
			{
				if($i==0)
				{
					$mythiname="'".$myid[$i]."'";
				}
				else
				{
					$mythiname=$mythiname.",'".$myid[$i]."'";
				}
			}
		}
		return $mythiname;
	}
function myDescription($idforecast) 
{
	$mydesc="";
	include ("INC/connectSFC.php");
	$sql="SELECT     TypeDescription.TypeName, DescriptionDetail.Description FROM         DescriptionDetail INNER JOIN TypeDescription ON DescriptionDetail.IDTypeDescription = TypeDescription.IDTypeDescription WHERE (DescriptionDetail.IDForecast = '$idforecast')ORDER BY TypeDescription.TypeName";
	$result=sqlsrv_query($ConnectSaleForecast,$sql);
	$i=1;
	while ($obj=sqlsrv_fetch_object($result)) 
	{	
		$TypeName=$obj->TypeName;
		$Description=$obj->Description;
		if($i==1)
		{
			$mydesc=$TypeName." : ".$Description;
		}
		else
		{
			$mydesc=$mydesc." , ".$TypeName." : ".$Description;
		}
		$i++;
	}
	return $mydesc;
}
function myStatus($idforecast) 
{
	$mystat="";
	include ("INC/connectSFC.php");
	$sql="SELECT  *,convert(varchar,DateStatus,103) as DateStatus from statusDetail WHERE IDForecast = '$idforecast'  order by Year(DateStatus) desc,Month(DateStatus) desc ,day(DateStatus) desc ";
	$result=sqlsrv_query($ConnectSaleForecast,$sql);
	$i=1;
	while ($obj=sqlsrv_fetch_object($result)) 
	{	
		$DateStatus=$obj->DateStatus;
		$Description=$obj->Description;
		if($i==1)
		{
			$mystat=$DateStatus." : ".$Description;
		}
		else
		{
			$mystat=$mystat." , ".$DateStatus." : ".$Description;
		}
		$i++;
	}
	return $mystat;
}
function CheckDepartmentSupportNeeded($IDForecast,$DSNNameID)
{
	include("INC/connectSFC.php");
	$sql="SELECT  * from DepartmentSupportNeeded WHERE IDForecast = '$IDForecast'  and DSNNameID='$DSNNameID' ";
	$result=sqlsrv_query($ConnectSaleForecast,$sql);
	$num=sqlsrv_num_rows($result);
	if($num==0)
		$num=0;
	else
		$num=1;
	return $num;
}
function CheckApplicationSupportNeeded($IDForecast,$ASNName)
{
	include("INC/connectSFC.php");
	$sql="SELECT  * from ApplicationSupportNeeded WHERE IDForecast = '$IDForecast'  and ASNName='$ASNName' ";
	$result=sqlsrv_query($ConnectSaleForecast,$sql);
	$num=sqlsrv_num_rows($result);
	return $num;
}
function CheckESRIProduct($IDForecast,$EPName)
{
	include("INC/connectSFC.php");
	$sql="SELECT  * from ESRIProduct WHERE IDForecast = '$IDForecast'  and EPName='$EPName' ";
	$result=sqlsrv_query($ConnectSaleForecast,$sql);
	$num=0;
	while($obj=sqlsrv_fetch_object($result))
	{
		$num=$obj->qty;
	}
	return $num;
}
function CheckLeicaProduct($IDForecast,$LPName)
{
	include("INC/connectSFC.php");
	$sql="SELECT  * from LeicaProduct WHERE IDForecast = '$IDForecast'  and LPName='$LPName' ";
	$result=sqlsrv_query($ConnectSaleForecast,$sql);
	$num=0;
	while($obj=sqlsrv_fetch_object($result))
	{
		$num=$obj->qty;
	}
	return $num;
}
function CheckGEOProduct($IDForecast,$GPName)
{
	include("INC/connectSFC.php");
	$sql="SELECT  * from GEOProduct WHERE IDForecast = '$IDForecast'  and GPName='$GPName' ";
	$result=sqlsrv_query($ConnectSaleForecast,$sql);
	$num=0;
	while($obj=sqlsrv_fetch_object($result))
	{
		$num=$obj->qty;
	}
	return $num;
}
function CheckTMProduct($IDForecast,$TMPName)
{
	include("INC/connectSFC.php");
	$sql="SELECT  * from TMProduct WHERE IDForecast = '$IDForecast'  and TMPName='$TMPName' ";
	$result=sqlsrv_query($ConnectSaleForecast,$sql);
	$num=0;
	while($obj=sqlsrv_fetch_object($result))
	{
		$num=$obj->qty;
	}
	return $num;
}

//<!--Query Department Support Needed-->
	include("INC/connectSFC.php");
	$sqlDepSup ="SELECT id,depName FROM DepartmentDetail where flag <> '1' and  export = '1' order by depName";
	$result3=sqlsrv_query($ConnectSaleForecast,$sqlDepSup);
	$sumDep=sqlsrv_num_rows($result3);
	$countDep = 0;
	while($obj3=sqlsrv_fetch_object($result3))
	{
		$DepIDArr[$countDep]=$obj3->id;
		$DepNameArr[$countDep]=$obj3->depName;
		$countDep= $countDep+1;
	}

	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
	header("Content-Type: application/download");
	header("Content-Transfer-Encoding: binary ");
	header("Content-type: application/ms-excel");	
	header("Content-type: charset=UTF-8");		
	header("Content-Disposition: attachment; filename=".basename("Report.xls").";");
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<title>:Report:</title>
<!-- <style type="text/css">

td.menubb {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:14px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue; text-Align: center;background:#CCFFCC;font-weight: bold;
}
td.menubbn {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue; text-Align: center;background:#CCFFCC;font-weight: bold;
}
td.menubbE {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:14px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue; text-Align: center;background:#CDE6FF;font-weight: bold;
}
td.menubbnE {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue; text-Align: center;background:#CDE6FF;font-weight: bold;
}
td.menubbG {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:14px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue; text-Align: center;background:#B2FFFF;font-weight: bold;
}
td.menubbnG {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue; text-Align: center;background:#B2FFFF;font-weight: bold;
}
td.menubbT {
	color:red; font-family: sans-serif , Tahoma; font-size:16px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue; text-Align: left;background:#FFE6E6;font-weight: bold;
}
td.menubbTc {
	color:red; font-family: sans-serif , Tahoma; font-size:16px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue; text-Align: center;background:#FFE6E6;font-weight: bold;
}
td.menubbnT {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue; text-Align: center;background:#FFB3FE;font-weight: bold;
}
td.menubbL {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:14px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue; text-Align: center;background:#E6CDFF;font-weight: bold;
}
td.menubbnL {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue; text-Align: center;background:#E6CDFF;font-weight: bold;
}
td.menubbA {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:14px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue; text-Align: center;background:#CCFFCC;font-weight: bold;
}
td.menubbnA {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue; text-Align: center;background:#CCFFCC;font-weight: bold;
}
td.menubbO {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:14px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue; text-Align: center;background:#FFF0B3;font-weight: bold;
}
td.menubbnO {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue; text-Align: center;background:#FFF0B3;font-weight: bold;
}
td.menubbY {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:14px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue; text-Align: center;background:#FFFF99;font-weight: bold;
}
td.menubbnY {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue; text-Align: center;background:#FFFF99;font-weight: bold;
}
td.menubbY2 {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:14px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue; text-Align: center;background:#FFFD4D;font-weight: bold;
}
td.menubbnY2 {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue; text-Align: center;background:#FFFD4D;font-weight: bold;
}
td.menubbD {
	color:red; font-family: sans-serif , Tahoma; font-size:14px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue; text-Align: center;background:#ECFFFF;font-weight: bold;
}
td.menubbnD {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue; text-Align: center;background:#ECFFFF;font-weight: bold;
}
td.menubbDl {
	color:red; font-family: sans-serif , Tahoma; font-size:14px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue; text-Align: left;background:#ECFFFF;font-weight: bold;
}
td.submenubb {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue;text-Align: left;
}
td.submenubbR {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue;text-Align: right;
}
td.submenubbC {
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue;text-Align: center;
}
td.submenubbCb {
	color:red; font-family: sans-serif , Tahoma; font-size:16px;Border-BOTTOM: 1px solid blue;Border-LEFT: 1px solid blue;Border-TOP: 1px solid blue;Border-RIGHT: 1px solid blue;text-Align: center;font-weight: bold;
}
td.txt13 { 
	color:#007AF4; font-family: sans-serif , Tahoma; font-size:13px;Border-LEFT: 1px solid blue;Border-RIGHT: 1px solid blue;
}

</style> -->
</head>
<body >
<?php 
function spiltword($str)
{
	$DecimalPlace = strpos($str, ":");
	$DecimalPlace1 = strpos($str1, " ");
	If($DecimalPlace > 0)
	{
		$Mystr = substr($str,0, $DecimalPlace);
	}
	else
	{
		$DecimalPlace1 = strpos($str, " ");
		If($DecimalPlace1 > 0)
		{
			$Mystr = substr($str,0, $DecimalPlace1);
		}
		else
		{
			$Mystr = substr($str,0);
		}
	}
	return trim($Mystr);
}
function convertfloat($number,$Format=6){
	$max=strlen($number);
	$DecimalPlace = strpos($number, ".");
	If($DecimalPlace > 0)
	{
		$MyNumber = substr($number,0, $DecimalPlace);
	}
	else
	{
		$MyNumber = $number;
	}
	$mymax=strlen($MyNumber);
	//echo "<br>";
	if($mymax<=4)
	{
		$digit=0;
	}
	else if($mymax==5)
	{
		$MyNumber="0".$MyNumber;
		$digit=substr($MyNumber,0-$Format, 2);
	}
	else if($mymax>=6)
	{
		$digit=substr($MyNumber,0-$Format, 2);
	}
	if($digit!=0)
	{
		$mt=substr($MyNumber,0, strlen($MyNumber)-$Format).".".$digit;
	}
	else
	{
		$mt=substr($MyNumber,0, strlen($MyNumber)-$Format);
	}
	return $mt;
}
function convertMoney($mt)
{
	if($mt==0)
	{
		$num_return="-";
	}
	else
	{
		$num_return=number_format($mt, 2);
	}
	return $num_return;
}	
function convertMoney1($number,$Format=6)
{
	$max=strlen($number);
	$DecimalPlace = strpos($number, ".");
	If($DecimalPlace > 0)
	{
		$MyNumber = substr($number,0, $DecimalPlace);
	}
	else
	{
		$MyNumber = $number;
	}
	$digit=substr($MyNumber,0-$Format, $Format);
	if($digit!=0)
	{
		$mt=substr($MyNumber,0, strlen($MyNumber)-$Format).".".$digit;
	}
	else
	{
		$mt=substr($MyNumber,0, strlen($MyNumber)-$Format);
	}
	if($mt==0)
	{
		$num_return="-";
	}
	else
	{
		$num_return=number_format($mt, 2);
	}
	return $num_return;
}	
	$wherestrYearStartSummary=" Year(TimeFrameContractSigndate) ";
	$wherestrYearStart=" Year(TimeFrameContractSigndate) ";
	$wherestrMonthStartSummary=" Month(TimeFrameContractSigndate) ";
	$wherestrMonthStart=" TimeFrameContractSigndate ";
	//$BidingYear=$year
	$BidingYear=$_POST['PotentialYear'];
	$getYear=Date("Y");
	//$saleid="1455";
	$getMonth=(int)date("m");
	$getDay=(int)date("d");
	$getDay_num=array("","31","29","31","30","31","30","31","31","30","31","30","31");
	if($getDay==1)
	{
		$getMonthBack=$getDay-1;
		$getDayBack=$getDay_num[$getMonthBack];
		
		while(checkdate($getMonthBack, $getDayBack, $BidingYear))
		{
			$getDayBack=$getDay-1;
		}
	}
	else
	{
		$getDayBack=$getDay-1;
		$getMonthBack=$getMonth;
	}
	$getToday=$BidingYear.date("md");
	if($getDayBack<10)
	{
		if($getMonthBack<10)
		{
			$getTodayBack=$BidingYear."0".$getMonthBack."0".$getDayBack;
		}
		else
		{
			$getTodayBack=$BidingYear.$getMonthBack."0".$getDayBack;
		}		
	}
	else
	{
		if($getMonthBack<10)
		{
			$getTodayBack=$BidingYear."0".$getMonthBack.$getDayBack;
		}
		else
		{
			$getTodayBack=$BidingYear.$getMonthBack.$getDayBack;
		}		
	}
	$strYearStart=$BidingYear;
	$strYearStartSummary=$BidingYear;
	$month_names=array("","Jan","Fab","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
	$SL=array("","4371","0416","","3882");
	$pTotal_SL1=0;
	$pTotal_SL2=0;
	$pTotal_SL1_2=0;
	$mpTotal_SL1=0;
	$mpTotal_SL2=0;
	$mpTotal_SL1_2=0;
	$SaleIn=$_POST['saleid'];
	include("INC/connectDB.php");
	$sql="select * from employeeesri where empno='$SaleIn' ";
	$que=sqlsrv_query($ConnectDB,$sql);
	while($obj=sqlsrv_fetch_object($que))
	{
		$Salename=trim($obj->thiname);
	}
if($BidingYear==$getYear)
{
?>
<table cellpadding="0" cellspacing="0" Border="0"width="300%" >
	<tr Align=left><td colspan=16><FONT SIZE="5" COLOR="#0000FF"><B><?php echo "SaleName : ".$Salename?></B></FONT></td></tr>
	<tr Align=left><td colspan=16><FONT SIZE="4" COLOR="#0000FF"><B>Project List</B></FONT></td></tr>
  <tr Align="center">
    <td  class ="menubbY" rowspan="2" style="width:300px;">Description</td>
	<?php if($getMonth=="1")
		{
			$nameActual="1 - ".$getDayBack."Jan ".$strYearStart;
		}
		else
		{
			$nameActual="1 Jan - ".$getDayBack." ".$month_names[$getMonth]." ".$strYearStart;
		}
		$strYearStart1=$strYearStart+1;
		$strYearStart2=$strYearStart+2;
		$strYearStart3=$strYearStart+3;
	?>
		
    <td  class ="menubbY" colspan="5" ><?php echo $nameActual?></td>
	<?php 
	if($getMonth=="12")
		{
			$nameForecast=$getDay." - 31 Dec  ".$strYearStart;
		}
		else
		{
			$nameForecast=$getDay." ".$month_names[$getMonth]." - 31 Dec  ".$strYearStart;
		}
	?>
    <td  class ="menubbY" colspan="5" ><?php echo $nameForecast?></td>
    <td  class ="menubbY" colspan="5" ><?php echo "Total Jan-Dec ".$strYearStart;?></td>

  </tr>
   <tr Align="center">
	<!--nameActual-->
	<td class ="menubbnO" style="width:100px;">P.O(MB)</td>
	<td class ="menubbnO" style="width:100px;">Book(MB)</td>
	<td class ="menubbnO" style="width:100px;"><?php echo "FWD ".$strYearStart1."".chr(10)."(MB)"?></td>
	<td class ="menubbnO" style="width:100px;"><?php echo "FWD ".$strYearStart2."".chr(10)."(MB)"?></td>
	<td class ="menubbnO" style="width:100px;"><?php echo "FWD ".$strYearStart3."".chr(10)."(MB)"?></td>
	<!--nameForecast-->
	<td class ="menubbnO" style="width:100px;">P.O(MB)</td>
	<td class ="menubbnO" style="width:100px;">Book(MB)</td>
	<td class ="menubbnO" style="width:100px;"><?php echo "FWD ".$strYearStart1."".chr(10)."(MB)"?></td>
	<td class ="menubbnO" style="width:100px;"><?php echo "FWD ".$strYearStart2."".chr(10)."(MB)"?></td>
	<td class ="menubbnO" style="width:100px;"><?php echo "FWD ".$strYearStart3."".chr(10)."(MB)"?></td>
	<!--Total-->
	<td class ="menubbnO" style="width:100px;">P.O(MB)</td>
	<td class ="menubbnO" style="width:100px;">Book(MB)</td>
	<td class ="menubbnO" style="width:100px;"><?php echo "FWD ".$strYearStart1."".chr(10)."(MB)"?></td>
	<td class ="menubbnO" style="width:100px;"><?php echo "FWD ".$strYearStart2."".chr(10)."(MB)"?></td>
	<td class ="menubbnO" style="width:100px;"><?php echo "FWD ".$strYearStart3."".chr(10)."(MB)"?></td>
  </tr>
<?php 
			$GTotalSummaryTargetIncome=0;
			$GTotalSummaryBook=0;
			$GTotalSummaryForward=0;
			$GTotalSummaryForward2=0;
			$GTotalSummaryForward3=0;
			$GTotalTargetIncomeF=0;
			$GTotalBookF=0;
			$GTotalForwardF=0;
			$GTotalForward2F=0;
			$GTotalForward3F=0;
			$GTotalTargetIncomeA=0;
			$GTotalBookA=0;
			$GTotalForwardA=0;
			$GTotalForward2A=0;
			$GTotalForward3A=0;
			$TotalSummaryTargetIncome=0;
			$TotalSummaryBook=0;
			$TotalSummaryForward=0;
			$TotalSummaryForward2=0;
			$TotalSummaryForward3=0;
			$TotalTargetIncomeF=0;
			$TotalBookF=0;
			$TotalForwardF=0;
			$TotalForward2F=0;
			$TotalForward3F=0;
			$TotalTargetIncomeA=0;
			$TotalBookA=0;
			$TotalForwardA=0;
			$TotalForward2A=0;
			$TotalForward3A=0;
				$SaleIn=$_POST['saleid'];
				//$SaleIn="1455";
				$sumTargetIncomeA=0;
				$sumBookA=0;
				$sumForwardA=0;
				$sumForward2A=0;
				$sumForward3A=0;
				include("INC/connectSFC.php");
				$sqlP="select  * from forecast where SaleID='$SaleIn' and $wherestrYearStart='$getYear'  and StatusDel='0' and TargetSpecialProject='0' and  Progress in ('100','90','80','70','60','50','40','30','20','10')";
				$queP=sqlsrv_query($ConnectSaleForecast,$sqlP);
				while($objP=sqlsrv_fetch_object($queP))
				{
					$IDForecast=$objP->IDForecast;
					$SaleRepresentative=$objP->SaleRepresentative;
					$Project=$objP->Project;
					//$SplitProject=spiltword($Project);
					$check=0;
					$TargetIncomeA=0;
					$BookA=0;
					$ForwardA=0;
					$Forward2A=0;
					$Forward3A=0;
					$TargetIncomeF=0;
					$BookF=0;
					$ForwardF=0;
					$Forward2F=0;
					$Forward3F=0;
					if($getMonth=="1")
					{
						include("INC/connectSFC.php");
						$sqlA="select  * from forecast where IDForecast='$IDForecast'  and $wherestrYearStart='$getYear' and $wherestrMonthStart='1' and StatusDel='0' and TargetSpecialProject='0' and Progress='100'";
						$queA=sqlsrv_query($ConnectSaleForecast,$sqlA);
						while($objA=sqlsrv_fetch_object($queA))
						{
							$TargetIncomeA=$objA->TargetIncome;
							$BookA=$objA->Book;
							$ForwardA=$objA->Forward;
							$Forward2A=$objA->Forward2;							
							$Forward3A=$objA->Forward3;
							$check=1;
						}
					}
					else
					{
						include("INC/connectSFC.php");
						$sqlA="select  *  from forecast where IDForecast='$IDForecast' and $wherestrYearStart='$getYear'  and $wherestrMonthStart<'$getToday'  and StatusDel='0' and TargetSpecialProject='0' and Progress='100'";
						$queA=sqlsrv_query($sqlA,$ConnectSaleForecast);
						while($objA=sqlsrv_fetch_object($queA))
						{
							$TargetIncomeA=$objA->TargetIncome;
							$BookA=$objA->Book;
							$ForwardA=$objA->Forward;
							$Forward2A=$objA->Forward2;						
							$Forward3A=$objA->Forward3;
							$check=1;
						}
					}
					if($getMonth=="12")
					{
						include("INC/connectSFC.php");
						$sqlF="select  * from forecast where IDForecast='$IDForecast' and $wherestrYearStart='$getYear' and $wherestrMonthStart='12' and StatusDel='0' and TargetSpecialProject='0' and  Progress in ('90','80','70','60','50','40','30','20','10')";
						$queF=sqlsrv_query($ConnectSaleForecast,$sqlF);
						while($objF=sqlsrv_fetch_object($queF))
						{
							$TargetIncomeF=$objF->TargetIncome;
							$BookF=$objF->Book;
							$ForwardF=$objF->Forward;
							$Forward2F=$objF->Forward2;
							$Forward3F=$objF->Forward3;
							$check=1;
						}
					}
					else
					{
						include("INC/connectSFC.php");
						$sqlF="select  * from forecast where IDForecast='$IDForecast' and $wherestrYearStart='$getYear' and $wherestrMonthStart>='$getToday' and StatusDel='0' and TargetSpecialProject='0' and  Progress in ('90','80','70','60','50','40','30','20','10')";
						$queF=sqlsrv_query($sqlF,$ConnectSaleForecast);
						while($objF=sqlsrv_fetch_object($queF))
						{
							$TargetIncomeF=$objF->TargetIncome;
							$BookF=$objF->Book;
							$ForwardF=$objF->Forward;
							$Forward2F=$objF->Forward2;
							$Forward3F=$objF->Forward3;
							$check=1;
						}
					}
					if($check==1)
					{
						$TargetIncomeA=convertfloat($TargetIncomeA,6);
						$BookA=convertfloat($BookA,6);
						$ForwardA=convertfloat($ForwardA,6);
						$Forward2A=convertfloat($Forward2A,6);
						$Forward3A=convertfloat($Forward3A,6);
						$TargetIncomeF=convertfloat($TargetIncomeF,6);
						$BookF=convertfloat($BookF,6);
						$ForwardF=convertfloat($ForwardF,6);
						$Forward2F=convertfloat($Forward2F,6);
						$Forward3F=convertfloat($Forward3F,6);
						$SummaryTargetIncome=$TargetIncomeA+$TargetIncomeF;
						$SummaryBook=$BookA+$BookF;
						$SummaryForward=$ForwardA+$ForwardF;
						$SummaryForward2=$Forward2A+$Forward2F;
						$SummaryForward3=$Forward3A+$Forward3F;
						?>
						<tr>
							<td  class ="submenubb" vAlign=top ><?php echo $Project;?></td>
							<td  class ="submenubbC" vAlign=top ><?php echo convertMoney($TargetIncomeA);?></td>
							<td  class ="submenubbC" vAlign=top ><?php echo convertMoney($BookA);?></td>
							<td  class ="submenubbC" vAlign=top ><?php echo convertMoney($ForwardA);?></td>
							<td  class ="submenubbC" vAlign=top ><?php echo convertMoney($Forward2A);?></td>
							<td  class ="submenubbC" vAlign=top ><?php echo convertMoney($Forward3A);?></td>
							<td  class ="submenubbC" vAlign=top ><?php echo convertMoney($TargetIncomeF);?></td>
							<td  class ="submenubbC" vAlign=top ><?php echo convertMoney($BookF);?></td>
							<td  class ="submenubbC" vAlign=top ><?php echo convertMoney($ForwardF);?></td>
							<td  class ="submenubbC" vAlign=top ><?php echo convertMoney($Forward2F);?></td>
							<td  class ="submenubbC" vAlign=top ><?php echo convertMoney($Forward3F);?></td>
							<td  class ="submenubbC" vAlign=top ><?php echo convertMoney($SummaryTargetIncome);?></td>
							<td  class ="submenubbC" vAlign=top ><?php echo convertMoney($SummaryBook);?></td>
							<td  class ="submenubbC" vAlign=top ><?php echo convertMoney($SummaryForward);?></td>
							<td  class ="submenubbC" vAlign=top ><?php echo convertMoney($SummaryForward2);?></td>
							<td  class ="submenubbC" vAlign=top ><?php echo convertMoney($SummaryForward3);?></td>
						</tr>
						<?php 
						$TotalSummaryTargetIncome=$TotalSummaryTargetIncome+$SummaryTargetIncome;
						$TotalSummaryBook=$TotalSummaryBook+$SummaryBook;
						$TotalSummaryForward=$TotalSummaryForward+$SummaryForward;
						$TotalSummaryForward2=$TotalSummaryForward2+$SummaryForward2;
						$TotalSummaryForward3=$TotalSummaryForward3+$SummaryForward3;
						$TotalTargetIncomeF=$TotalTargetIncomeF+$TargetIncomeF;
						$TotalBookF=$TotalBookF+$BookF;
						$TotalForwardF=$TotalForwardF+$ForwardF;
						$TotalForward2F=$TotalForward2F+$Forward2F;
						$TotalForward3F=$TotalForward3F+$Forward3F;
						$TotalTargetIncomeA=$TotalTargetIncomeA+$TargetIncomeA;
						$TotalBookA=$TotalBookA+$BookA;
						$TotalForwardA=$TotalForwardA+$ForwardA;
						$TotalForward2A=$TotalForward2A+$Forward2A;
						$TotalForward3A=$TotalForward3A+$Forward3A;
					}
					
				}
				$GTotalSummaryTargetIncome=$TotalSummaryTargetIncome+$GTotalSummaryTargetIncome;
				$GTotalSummaryBook=$TotalSummaryBook+$GTotalSummaryBook;
				$GTotalSummaryForward=$TotalSummaryForward+$GTotalSummaryForward;
				$GTotalSummaryForward2=$TotalSummaryForward2+$GTotalSummaryForward2;
				$GTotalSummaryForward3=$TotalSummaryForward3+$GTotalSummaryForward3;
				$GTotalTargetIncomeF=$TotalTargetIncomeF+$GTotalTargetIncomeF;
				$GTotalBookF=$TotalBookF+$GTotalBookF;
				$GTotalForwardF=$TotalForwardF+$GTotalForwardF;
				$GTotalForward2F=$TotalForward2F+$GTotalForward2F;
				$GTotalForward3F=$TotalForward3F+$GTotalForward3F;
				$GTotalTargetIncomeA=$TotalTargetIncomeA+$GTotalTargetIncomeA;
				$GTotalBookA=$TotalBookA+$GTotalBookA;
				$GTotalForwardA=$TotalForwardA+$GTotalForwardA;
				$GTotalForward2A=$TotalForward2A+$GTotalForward2A;
				$GTotalForward3A=$TotalForward3A+$GTotalForward3A;			
				
				?>
			<tr>
				<td  class ="menubbTc" vAlign=top >Total</td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($GTotalTargetIncomeA);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($GTotalBookA);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($GTotalForwardA);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($GTotalForward2A);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($GTotalForward3A);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($GTotalTargetIncomeF);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($GTotalBookF);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($GTotalForwardF);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($GTotalForward2F);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($GTotalForward3F);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($GTotalSummaryTargetIncome);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($GTotalSummaryBook);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($GTotalSummaryForward);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($GTotalSummaryForward2);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($GTotalSummaryForward3);?></td>
			</tr>
			<tr><td colspan=17> <td></tr>
			<!--Special Project List-->
		<tr Align=left><td colspan=17><FONT SIZE="4" COLOR="#0000FF"><B>Special Project List</B></FONT></td></tr>
		<?php 
			$SGTotalSummaryTargetIncome=0;
			$SGTotalSummaryBook=0;
			$SGTotalSummaryForward=0;
			$SGTotalSummaryForward2=0;
			$SGTotalSummaryForward3=0;
			$SGTotalTargetIncomeF=0;
			$SGTotalBookF=0;
			$SGTotalForwardF=0;
			$SGTotalForward2F=0;
			$SGTotalForward3F=0;
			$SGTotalTargetIncomeA=0;
			$SGTotalBookA=0;
			$SGTotalForwardA=0;
			$SGTotalForward2A=0;
			$SGTotalForward3A=0;
			$STotalSummaryTargetIncome=0;
			$STotalSummaryBook=0;
			$STotalSummaryForward=0;
			$STotalSummaryForward2=0;
			$STotalSummaryForward3=0;
			$STotalTargetIncomeF=0;
			$STotalBookF=0;
			$STotalForwardF=0;
			$STotalForward2F=0;
			$STotalForward3F=0;
			$STotalTargetIncomeA=0;
			$STotalBookA=0;
			$STotalForwardA=0;
			$STotalForward2A=0;
			$STotalForward3A=0;				
			$SsumTargetIncomeA=0;
			$SsumBookA=0;
			$SsumForwardA=0;
			$SsumForward2A=0;
			$SsumForward3A=0;
			include("INC/connectSFC.php");
				$sqlSP="select  * from forecast where SaleID='$SaleIn' and $wherestrYearStart='$getYear'  and StatusDel='0' and TargetSpecialProject='1' and  Progress in ('100','90','80','70','60','50','40','30','20','10')";
				$queSP=sqlsrv_query($ConnectSaleForecast,$sqlSP);
				while($objSP=sqlsrv_fetch_object($queSP))
				{
					$IDForecast=$objSP->IDForecast;
					$SaleRepresentative=$objSP->SaleRepresentative;
					$Project=$objSP->Project;
					//$SplitProject=spiltword($Project);
					$check=0;
					$STargetIncomeA=0;
					$SBookA=0;
					$SForwardA=0;
					$SForward2A=0;
					$SForward3A=0;
					$STargetIncomeF=0;
					$SBookF=0;
					$SForwardF=0;
					$SForward2F=0;
					$SForward3F=0;
					if($getMonth=="1")
					{
						include("INC/connectSFC.php");
						$sqlA="select  * from forecast where IDForecast='$IDForecast'  and $wherestrYearStart='$getYear' and $wherestrMonthStart='1' and StatusDel='0' and TargetSpecialProject='1' and Progress='100'";
						$queA=sqlsrv_query($ConnectSaleForecast,$qlA);
						while($objA=sqlsrv_fetch_object($queA))
						{
							$STargetIncomeA=$objA->TargetIncome;
							$SBookA=$objA->Book;
							$SForwardA=$objA->Forward;
							$SForward2A=$objA->Forward2;							
							$SForward3A=$objA->Forward3;
							$check=1;
						}
					}
					else
					{
						include("INC/connectSFC.php");
						$sqlA="select  *  from forecast where IDForecast='$IDForecast' and $wherestrYearStart='$getYear'  and $wherestrMonthStart<'$getToday'  and StatusDel='0' and TargetSpecialProject='1' and Progress='100'";
						$queA=sqlsrv_query($sqlA,$ConnectSaleForecast);
						while($objA=sqlsrv_fetch_object($queA))
						{
							$STargetIncomeA=$objA->TargetIncome;
							$SBookA=$objA->Book;
							$SForwardA=$objA->Forward;
							$SForward2A=$objA->Forward2;						
							$SForward3A=$objA->Forward3;
							$check=1;
						}
					}
					if($getMonth=="12")
					{
						include("INC/connectSFC.php");
						$sqlF="select  * from forecast where IDForecast='$IDForecast' and $wherestrYearStart='$getYear' and $wherestrMonthStart='12' and StatusDel='0' and TargetSpecialProject='1' and  Progress in ('90','80','70','60','50','40','30','20','10')";
						$queF=sqlsrv_query($ConnectSaleForecast,$sqlF);
						while($objF=sqlsrv_fetch_object($queF))
						{
							$STargetIncomeF=$objF->TargetIncome;
							$SBookF=$objF->Book;
							$SForwardF=$objF->Forward;
							$SForward2F=$objF->Forward2;
							$SForward3F=$objF->Forward3;
							$check=1;
						}
					}
					else
					{
						include("INC/connectSFC.php");
						$sqlF="select  * from forecast where IDForecast='$IDForecast' and $wherestrYearStart='$getYear' and $wherestrMonthStart>='$getToday' and StatusDel='0' and TargetSpecialProject='1' and  Progress in ('90','80','70','60','50','40','30','20','10')";
						$queF=sqlsrv_query($sqlF,$ConnectSaleForecast);
						while($objF=sqlsrv_fetch_object($queF))
						{
							$STargetIncomeF=$objF->TargetIncome;
							$SBookF=$objF->Book;
							$SForwardF=$objF->Forward;
							$SForward2F=$objF->Forward2;
							$SForward3F=$objF->Forward3;
							$check=1;
						}
					}
					if($check==1)
					{
						$STargetIncomeA=convertfloat($STargetIncomeA,6);
						$SBookA=convertfloat($SBookA,6);
						$SForwardA=convertfloat($SForwardA,6);
						$SForward2A=convertfloat($SForward2A,6);
						$SForward3A=convertfloat($SForward3A,6);
						$STargetIncomeF=convertfloat($STargetIncomeF,6);
						$SBookF=convertfloat($SBookF,6);
						$SForwardF=convertfloat($SForwardF,6);
						$SForward2F=convertfloat($SForward2F,6);
						$SForward3F=convertfloat($SForward3F,6);
						$SSummaryTargetIncome=$STargetIncomeA+$STargetIncomeF;
						$SSummaryBook=$SBookA+$SBookF;
						$SSummaryForward=$SForwardA+$SForwardF;
						$SSummaryForward2=$SForward2A+$SForward2F;
						$SSummaryForward3=$SForward3A+$SForward3F;
						?>
						<tr>
							<td  class ="submenubb" vAlign=top ><?php echo $Project;?></td>
							<td  class ="submenubbC" vAlign=top ><?php echo convertMoney($STargetIncomeA);?></td>
							<td  class ="submenubbC" vAlign=top ><?php echo convertMoney($SBookA);?></td>
							<td  class ="submenubbC" vAlign=top ><?php echo convertMoney($SForwardA);?></td>
							<td  class ="submenubbC" vAlign=top ><?php echo convertMoney($SForward2A);?></td>
							<td  class ="submenubbC" vAlign=top ><?php echo convertMoney($SForward3A);?></td>
							<td  class ="submenubbC" vAlign=top ><?php echo convertMoney($STargetIncomeF);?></td>
							<td  class ="submenubbC" vAlign=top ><?php echo convertMoney($SBookF);?></td>
							<td  class ="submenubbC" vAlign=top ><?php echo convertMoney($SForwardF);?></td>
							<td  class ="submenubbC" vAlign=top ><?php echo convertMoney($SForward2F);?></td>
							<td  class ="submenubbC" vAlign=top ><?php echo convertMoney($SForward3F);?></td>
							<td  class ="submenubbC" vAlign=top ><?php echo convertMoney($SSummaryTargetIncome);?></td>
							<td  class ="submenubbC" vAlign=top ><?php echo convertMoney($SSummaryBook);?></td>
							<td  class ="submenubbC" vAlign=top ><?php echo convertMoney($SSummaryForward);?></td>
							<td  class ="submenubbC" vAlign=top ><?php echo convertMoney($SSummaryForward2);?></td>
							<td  class ="submenubbC" vAlign=top ><?php echo convertMoney($SSummaryForward3);?></td>
						</tr>
						<?php 
						$STotalSummaryTargetIncome=$STotalSummaryTargetIncome+$SSummaryTargetIncome;
						$STotalSummaryBook=$STotalSummaryBook+$SSummaryBook;
						$STotalSummaryForward=$STotalSummaryForward+$SSummaryForward;
						$STotalSummaryForward2=$STotalSummaryForward2+$SSummaryForward2;
						$STotalSummaryForward3=$STotalSummaryForward3+$SSummaryForward3;
						$STotalTargetIncomeF=$STotalTargetIncomeF+$STargetIncomeF;
						$STotalBookF=$STotalBookF+$SBookF;
						$STotalForwardF=$STotalForwardF+$SForwardF;
						$STotalForward2F=$STotalForward2F+$SForward2F;
						$STotalForward3F=$STotalForward3F+$SForward3F;
						$STotalTargetIncomeA=$STotalTargetIncomeA+$STargetIncomeA;
						$STotalBookA=$STotalBookA+$SBookA;
						$STotalForwardA=$STotalForwardA+$SForwardA;
						$STotalForward2A=$STotalForward2A+$SForward2A;
						$STotalForward3A=$STotalForward3A+$SForward3A;
					}
					
				}
				$SGTotalSummaryTargetIncome=$STotalSummaryTargetIncome+$SGTotalSummaryTargetIncome;
				$SGTotalSummaryBook=$STotalSummaryBook+$SGTotalSummaryBook;
				$SGTotalSummaryForward=$STotalSummaryForward+$SGTotalSummaryForward;
				$SGTotalSummaryForward2=$STotalSummaryForward2+$SGTotalSummaryForward2;
				$SGTotalSummaryForward3=$STotalSummaryForward3+$SGTotalSummaryForward3;
				$SGTotalTargetIncomeF=$STotalTargetIncomeF+$SGTotalTargetIncomeF;
				$SGTotalBookF=$STotalBookF+$SGTotalBookF;
				$SGTotalForwardF=$STotalForwardF+$SGTotalForwardF;
				$SGTotalForward2F=$STotalForward2F+$SGTotalForward2F;
				$SGTotalForward3F=$STotalForward3F+$SGTotalForward3F;
				$SGTotalTargetIncomeA=$STotalTargetIncomeA+$SGTotalTargetIncomeA;
				$SGTotalBookA=$STotalBookA+$SGTotalBookA;
				$SGTotalForwardA=$STotalForwardA+$SGTotalForwardA;
				$SGTotalForward2A=$STotalForward2A+$SGTotalForward2A;
				$SGTotalForward3A=$STotalForward3A+$SGTotalForward3A;			
				
				?>
			<tr>
				<td  class ="menubbTc" vAlign=top >Total</td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($SGTotalTargetIncomeA);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($SGTotalBookA);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($SGTotalForwardA);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($SGTotalForward2A);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($SGTotalForward3A);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($SGTotalTargetIncomeF);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($SGTotalBookF);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($SGTotalForwardF);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($SGTotalForward2F);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($SGTotalForward3F);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($SGTotalSummaryTargetIncome);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($SGTotalSummaryBook);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($SGTotalSummaryForward);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($SGTotalSummaryForward2);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($SGTotalSummaryForward3);?></td>
			</tr>
<?php 
				$TSGTotalSummaryTargetIncome=$GTotalSummaryTargetIncome+$SGTotalSummaryTargetIncome;
				$TSGTotalSummaryBook=$GTotalSummaryBook+$SGTotalSummaryBook;
				$TSGTotalSummaryForward=$GTotalSummaryForward+$SGTotalSummaryForward;
				$TSGTotalSummaryForward2=$GTotalSummaryForward2+$SGTotalSummaryForward2;
				$TSGTotalSummaryForward3=$GTotalSummaryForward3+$SGTotalSummaryForward3;
				$TSGTotalTargetIncomeF=$GTotalTargetIncomeF+$SGTotalTargetIncomeF;
				$TSGTotalBookF=$GTotalBookF+$SGTotalBookF;
				$TSGTotalForwardF=$GTotalForwardF+$SGTotalForwardF;
				$TSGTotalForward2F=$GTotalForward2F+$SGTotalForward2F;
				$TSGTotalForward3F=$GTotalForward3F+$SGTotalForward3F;
				$TSGTotalTargetIncomeA=$GTotalTargetIncomeA+$SGTotalTargetIncomeA;
				$TSGTotalBookA=$GTotalBookA+$SGTotalBookA;
				$TSGTotalForwardA=$GTotalForwardA+$GSTotalForwardA;
				$TSGTotalForward2A=$GTotalForward2A+$SGTotalForward2A;
				$TSGTotalForward3A=$GTotalForward3A+$SGTotalForward3A;
				
				
				?>
			<tr>
				<td  class ="menubbTc" vAlign=top >Grand Total</td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($TSGTotalTargetIncomeA);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($TSGTotalBookA);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($TSGTotalForwardA);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($TSGTotalForward2A);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($TSGTotalForward3A);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($TSGTotalTargetIncomeF);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($TSGTotalBookF);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($TSGTotalForwardF);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($TSGTotalForward2F);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($TSGTotalForward3F);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($TSGTotalSummaryTargetIncome);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($TSGTotalSummaryBook);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($TSGTotalSummaryForward);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($TSGTotalSummaryForward2);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($TSGTotalSummaryForward3);?></td>
			</tr>
	</table>
 <?php 
}
 else
 {
 
 //difference year
 ?>
 <table cellpadding="0" cellspacing="0" Border="0"width="300%" >
	<tr Align=left><td colspan=16><FONT SIZE="5" COLOR="#0000FF"><B><?php echo "SaleName : ".$Salename?></B></FONT></td></tr>
	<tr Align=left><td colspan=6><FONT SIZE="4" COLOR="#0000FF"><B>Project List</B></FONT></td></tr>
  <tr Align="center">
    <td  class ="menubbY" rowspan="2" style="width:300px;">Description</td>
	<?php 	$strYearStart1=$strYearStart+1;
		$strYearStart2=$strYearStart+2;
		$strYearStart3=$strYearStart+3;
	?>
		
    
    <td  class ="menubbY" colspan="5" ><?php echo "Total Jan-Dec ".$strYearStart;?></td>

  </tr>
   <tr Align="center">
	<!--Total-->
	<td class ="menubbnO" style="width:100px;">P.O(MB)</td>
	<td class ="menubbnO" style="width:100px;">Book(MB)</td>
	<td class ="menubbnO" style="width:100px;"><?php echo "FWD ".$strYearStart1."".chr(10)."(MB)"?></td>
	<td class ="menubbnO" style="width:100px;"><?php echo "FWD ".$strYearStart2."".chr(10)."(MB)"?></td>
	<td class ="menubbnO" style="width:100px;"><?php echo "FWD ".$strYearStart3."".chr(10)."(MB)"?></td>
  </tr>
<?php 		$i=1;
			$GTotalSummaryTargetIncome=0;
			$GTotalSummaryBook=0;
			$GTotalSummaryForward=0;
			$GTotalSummaryForward2=0;
			$GTotalSummaryForward3=0;
			$GTotalTargetIncomeF=0;
			$GTotalBookF=0;
			$GTotalForwardF=0;
			$GTotalForward2F=0;
			$GTotalForward3F=0;
			$GTotalTargetIncomeA=0;
			$GTotalBookA=0;
			$GTotalForwardA=0;
			$GTotalForward2A=0;
			$GTotalForward3A=0;
			$PGTotalTargetIncomeA=0;
			$PGTotalBookA=0;
			$PGTotalForwardA=0;
			$PGTotalForward2A=0;
			$PGTotalForward3A=0;
			$TotalSummaryTargetIncome=0;
			$TotalSummaryBook=0;
			$TotalSummaryForward=0;
			$TotalSummaryForward2=0;
			$TotalSummaryForward3=0;
			$TotalTargetIncomeF=0;
			$TotalBookF=0;
			$TotalForwardF=0;
			$TotalForward2F=0;
			$TotalForward3F=0;
			$TotalTargetIncomeA=0;
			$TotalBookA=0;
			$TotalForwardA=0;
			$TotalForward2A=0;
			$TotalForward3A=0;
				$SaleIn=$_POST['saleid'];
				$sumTargetIncomeA=0;
				$sumBookA=0;
				$sumForwardA=0;
				$sumForward2A=0;
				$sumForward3A=0;
			include("INC/connectSFC.php");
				$sqlM="select  * from forecast where SaleID='$SaleIn' and $wherestrYearStart='$strYearStart'  and StatusDel='0' and TargetSpecialProject='0' and  Progress not in ('100','0','v')";
				$queM=sqlsrv_query($ConnectSaleForecast,$sqlM);
				$iii=1;
				while($objM=sqlsrv_fetch_object($queM))
				{
					$IDForecast=$objM->IDForecast;
					$SaleRepresentative=$objM->SaleRepresentative;
					$Project=$objM->Project;
					//$SplitProject=spiltword($Project);
					?><tr><td  class ="submenubb" vAlign=top ><?php echo $Project;?></td><?php 
					$TargetIncomeAM=0;
					$BookAM=0;
					$ForwardAM=0;
					$Forward2AM=0;
					$Forward3AM=0;
						include("INC/connectSFC.php");
						$sqlA="select  *  from forecast where IDForecast='$IDForecast' and $wherestrYearStart='$strYearStart'  and StatusDel='0' and TargetSpecialProject='0' and Progress not in ('100','0','v')";
						$queA=sqlsrv_query($ConnectSaleForecast,$sqlA);
						while($objA=sqlsrv_fetch_object($queA))
						{
							$TargetIncomeAM=$objA->TargetIncome;
							$BookAM=$objA->Book;
							$ForwardAM=$objA->Forward;
							$Forward2AM=$objA->Forward2;						
							$Forward3AM=$objA->Forward3;
							
						}
					$TargetIncomeAM=convertfloat($TargetIncomeAM,6);
					$BookAM=convertfloat($BookAM,6);
					$ForwardAM=convertfloat($ForwardAM,6);
					$Forward2AM=convertfloat($Forward2AM,6);
					$Forward3AM=convertfloat($Forward3AM,6);
					?>
						<td  class ="submenubbC" vAlign=top ><?php echo convertMoney($TargetIncomeAM);?></td>
						<td  class ="submenubbC" vAlign=top ><?php echo convertMoney($BookAM);?></td>
						<td  class ="submenubbC" vAlign=top ><?php echo convertMoney($ForwardAM);?></td>
						<td  class ="submenubbC" vAlign=top ><?php echo convertMoney($Forward2AM);?></td>
						<td  class ="submenubbC" vAlign=top ><?php echo convertMoney($Forward3AM);?></td>
					</tr>
<?php 
					$TotalSummaryTargetIncome=$TotalSummaryTargetIncome+$TargetIncomeAM;
					$TotalSummaryBook=$TotalSummaryBook+$BookAM;
					$TotalSummaryForward=$TotalSummaryForward+$ForwardAM;
					$TotalSummaryForward2=$TotalSummaryForward2+$Forward2AM;
					$TotalSummaryForward3=$TotalSummaryForward3+$Forward3AM;
				}
				
			?><tr>
				<td  class ="menubbTc" vAlign=top >Total </td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($TotalSummaryTargetIncome);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($TotalSummaryBook);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($TotalSummaryForward);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($TotalSummaryForward2);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($TotalSummaryForward3);?></td>
			</tr><?php 
				$PGTotalTargetIncomeA=$TotalSummaryTargetIncome+$PGTotalTargetIncomeA;
				$PGTotalBookA=$TotalSummaryBook+$PGTotalBookA;
				$PGTotalForwardA=$TotalSummaryForward+$PGTotalForwardA;
				$PGTotalForward2A=$TotalSummaryForward2+$PGTotalForward2A;
				$PGTotalForward3A=$TotalSummaryForward3+$PGTotalForward3A;
				
?>		
			<tr><td colspan=6></td></tr>
<!--*********************************			
**************************************->		
			<!--Special Project List-->
		<tr Align=left><td colspan=6><FONT SIZE="4" COLOR="#0000FF"><B>Special Project List</B></FONT></td></tr>
		<?php 
			$SGTotalSummaryTargetIncome=0;
			$SGTotalSummaryBook=0;
			$SGTotalSummaryForward=0;
			$SGTotalSummaryForward2=0;
			$SGTotalSummaryForward3=0;
			$SGTotalTargetIncomeF=0;
			$SGTotalBookF=0;
			$SGTotalForwardF=0;
			$SGTotalForward2F=0;
			$SGTotalForward3F=0;
			$SGTotalTargetIncomeA=0;
			$SGTotalBookA=0;
			$SGTotalForwardA=0;
			$SGTotalForward2A=0;
			$SGTotalForward3A=0;
			$TotalTargetIncomeA=0;
			$TotalBookA=0;
			$TotalForwardA=0;
			$TotalForward2A=0;
			$TotalForward3A=0;
			$TotalSummaryTargetIncomeM=0;
			$TotalSummaryBookM=0;
			$TotalSummaryForwardM=0;
			$TotalSummaryForward2M=0;
			$TotalSummaryForward3M=0;
			$startmp=$i;

				include("INC/connectSFC.php");
				$sqlM="select  * from forecast where SaleID='$SaleIn' and $wherestrYearStart='$strYearStart'  and StatusDel='0' and TargetSpecialProject='1' and  Progress not in ('100','0','v')";
				$queM=sqlsrv_query($ConnectSaleForecast,$sqlM);
				$iii=1;
				while($objM=sqlsrv_fetch_object($queM))
				{
					$IDForecast=$objM->IDForecast;
					$SaleRepresentative=$objM->SaleRepresentative;
					$Project=$objM->Project;
					//$SplitProject=spiltword($Project);
					?><tr><td  class ="submenubb" vAlign=top ><?php echo $Project;?></td><?php 
					$TargetIncomeAM=0;
					$BookAM=0;
					$ForwardAM=0;
					$Forward2AM=0;
					$Forward3AM=0;
						include("INC/connectSFC.php");
						$sqlA="select  *  from forecast where IDForecast='$IDForecast' and $wherestrYearStart='$strYearStart'  and StatusDel='0' and TargetSpecialProject='1' and Progress not in ('100','0','v')";
						$queA=sqlsrv_query($ConnectSaleForecast,$sqlA);
						while($objA=sqlsrv_fetch_object($queA))
						{
							$TargetIncomeAM=$objA->TargetIncome;
							$BookAM=$objA->Book;
							$ForwardAM=$objA->Forward;
							$Forward2AM=$objA->Forward2;						
							$Forward3AM=$objA->Forward3;
							
						}
					$TargetIncomeAM=convertfloat($TargetIncomeAM,6);
					$BookAM=convertfloat($BookAM,6);
					$ForwardAM=convertfloat($ForwardAM,6);
					$Forward2AM=convertfloat($Forward2AM,6);
					$Forward3AM=convertfloat($Forward3AM,6);
					?>
						<td  class ="submenubbC" vAlign=top ><?php echo convertMoney($TargetIncomeAM);?></td>
						<td  class ="submenubbC" vAlign=top ><?php echo convertMoney($BookAM);?></td>
						<td  class ="submenubbC" vAlign=top ><?php echo convertMoney($ForwardAM);?></td>
						<td  class ="submenubbC" vAlign=top ><?php echo convertMoney($Forward2AM);?></td>
						<td  class ="submenubbC" vAlign=top ><?php echo convertMoney($Forward3AM);?></td>
					</tr>
<?php 
					$TotalSummaryTargetIncomeM=$TotalSummaryTargetIncomeM+$TargetIncomeAM;
					$TotalSummaryBookM=$TotalSummaryBookM+$BookAM;
					$TotalSummaryForwardM=$TotalSummaryForwardM+$ForwardAM;
					$TotalSummaryForward2M=$TotalSummaryForward2M+$Forward2AM;
					$TotalSummaryForward3M=$TotalSummaryForward3M+$Forward3AM;
				}
				$SGTotalTargetIncomeA=$TotalSummaryTargetIncomeM+$SGTotalTargetIncomeA;
				$SGTotalBookA=$TotalSummaryBookM+$SGTotalBookA;
				$SGTotalForwardA=$TotalSummaryForwardM+$GSTotalForwardA;
				$SGTotalForward2A=$TotalSummaryForward2M+$SGTotalForward2A;
				$SGTotalForward3A=$TotalSummaryForward3M+$SGTotalForward3A;
			?><tr>
				<td  class ="menubbTc" vAlign=top >Total </td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($TotalSummaryTargetIncomeM);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($TotalSummaryBookM);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($TotalSummaryForwardM);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($TotalSummaryForward2M);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($TotalSummaryForward3M);?></td>
			</tr><?php 
				$TSGTotalTargetIncomeA=$PGTotalTargetIncomeA+$SGTotalTargetIncomeA;
				$TSGTotalBookA=$PGTotalBookA+$SGTotalBookA;
				$TSGTotalForwardA=$PGTotalForwardA+$GSTotalForwardA;
				$TSGTotalForward2A=$PGTotalForward2A+$SGTotalForward2A;
				$TSGTotalForward3A=$PGTotalForward3A+$SGTotalForward3A;
				
				
				?>
			<tr>
				<td  class ="menubbTc" vAlign=top >Grand Total </td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($TSGTotalTargetIncomeA);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($TSGTotalBookA);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($TSGTotalForwardA);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($TSGTotalForward2A);?></td>
				<td  class ="submenubbCb" vAlign=top ><?php echo convertMoney($TSGTotalForward3A);?></td>
			</tr>
	</table>
 <?php 
 }
 ?>
</body>
</html>
