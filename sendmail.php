<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body cz-shortcut-listen="true" style="margin-top:0px;">
<?php 
$IDForecast = $_GET['IDForecast'];
$page = $_GET['page'];
if ($IDForecast) {
	
				include("INC/connectSFC.php");
				$sqlStr = "SELECT PEContractNo, Project,CONVERT(VARCHAR(10), TimeFrameBidingDate, 103) AS TimeFrameBidingDate, CONVERT(VARCHAR(10), TimeFrameContractSigndate, 103) AS TimeFrameContractSigndate,  SaleID, SaleRepresentative FROM Forecast WHERE IDForecast='$IDForecast'";
				$query = sqlsrv_query($ConnectSaleForecast, $sqlStr );
				if ($obj = sqlsrv_fetch_object($query)) {
					$PEContractNo = iconv( 'UTF-8','TIS-620',$obj->PEContractNo);
					$Project = iconv( 'UTF-8','TIS-620',$obj->Project);
					$TimeFrameBidingDate = iconv( 'UTF-8','TIS-620',$obj->TimeFrameBidingDate);
					$TimeFrameContractSigndate = iconv( 'UTF-8','TIS-620',$obj->TimeFrameContractSigndate);
					$SaleID = iconv( 'UTF-8','TIS-620',$obj->SaleID);
				}
				$Description = '';
				$sqlStr = "SELECT TOP 1(Description) FROM StatusDetail WHERE IDForecast='$IDForecast' order by DateStatus desc";
				$query = sqlsrv_query( $ConnectSaleForecast,$sqlStr);
				if ($obj = sqlsrv_fetch_object($query)) {
					$Description = iconv( 'UTF-8','TIS-620',$obj->Description);
				}
				
				$sqlStr = "SELECT DSNNameID FROM DepartmentSupportNeeded WHERE IDForecast='$IDForecast' order by DSNName";
				$query = sqlsrv_query($ConnectSaleForecast, $sqlStr);
				$DSNNameArr = array();
				$mailArr = array();
				$r = 0;
				while ($obj = sqlsrv_fetch_object($query)) {
					$DSNNameIDdata = trim(iconv( 'UTF-8','TIS-620',$obj->DSNNameID));
					$sqlStr2 = "SELECT depName,mailManager FROM DepartmentDetail WHERE id ='$DSNNameIDdata' and flag <> '1'";
						$query2 = sqlsrv_query($ConnectSaleForecast, $sqlStr2);
						while ($obj2 = sqlsrv_fetch_object($query2)) {
							//$DSNNamedata = trim($obj2->depName);
							$DSNNameArr[$r][] = trim(iconv( 'UTF-8','TIS-620',$obj2->depName));
							$mailArr[$r][] = trim(iconv( 'UTF-8','TIS-620',$obj2->mailManager));
							$r++;
						}
				}
					
				if($SaleID){
					include("INC/connectDB.php");
					$sqlStr = "SELECT thiname,email FROM employeeesri WHERE empno = '$SaleID' ";
					$query = sqlsrv_query($ConnectDB, $sqlStr);
					while ($obj = sqlsrv_fetch_object($query)) {
						$thiname = trim(iconv( 'UTF-8','TIS-620',$obj->thiname));
						$mailSale = trim(iconv( 'UTF-8','TIS-620',$obj->email));
					}
				}
			}
$titlename = iconv( 'UTF-8','TIS-620','คุณ');

require("classMail/class.mail.php");
	$mail = new phpmailer();
	$mail->Subject = "New forcast(". $PEContractNo ." / ". $Project .")";
	$mail->Body  = "<table border='0' cellpadding='5' cellspacing='0' >
						<tr>
							<td align='right' >PEContract : </td>
							<td align='left' >". $PEContractNo ."</td>
						</tr>
						<tr>
							<td align='right' >Project Name : </td>
							<td align='left' >". $Project ."</td>
						</tr>
						<tr>
							<td align='right' >Bidding Date : </td>
							<td align='left' >". $TimeFrameBidingDate ."</td>
						</tr>
						<tr>
							<td align='right' >Contract Sign Date : </td>
							<td align='left' >". $TimeFrameContractSigndate ."</td>
						</tr>
						<tr>
							<td align='right' >Sales Representative : </td>
							<td align='left' >".$titlename." ". $thiname ."</td>
						</tr>
						<tr>
							<td align='right' >Status : </td>
							<td align='left' >". $Description ."</td>
						</tr>
						<tr>
							<td align='right' >Department Support  : </td>
							<td align='left' valign='bottom'>
							<table>
";

	for ($i = 0; $i < count($DSNNameArr); $i++) {
		
		$DSNName=$DSNNameArr[$i][0];
		$mail->Body .="<tr><td>". $DSNName ."</td></tr>";
	}
	$mail->Body .="</table></td></tr></table>
				<br/>
				<br/>
				<table border='0' cellpadding='5' cellspacing='0' >
				<tr><td>Regards</td></tr>
				<tr><td>". $thiname ."</td></tr>
				</table>";
	
	$mail->From = $mailSale;
	$mail->FromName =$thiname;
	if(count($mailArr) > 0){
		for ($i = 0; $i < count($mailArr); $i++) {
			$mailManager=$mailArr[$i][0];
			$mailManagerArr = explode(",", $mailManager);
			for ($num = 0; $num < count($mailManagerArr); $num++) {
				$mailStr = str_replace(' ','',$mailManagerArr[$num]);
				if($mailStr != ''){
					$mail->AddAddress($mailStr);
				}
			}
		}
		//sendmail($mail);
	}
	
	//print_r($mail);
	
	?><script>
		alert("ทำการบันทึกข้อมูลเรียบร้อย");
	<?php if ($page) {?>
		window.location = "<?php echo $page?>.php";
	<?php } else {?>
		window.location = "index.php";
	<?php }?>
	</script>
</body>
</html>