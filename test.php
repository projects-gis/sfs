
	<script src="js/library/jquery/jquery-1.9.1"></script>
	<script src="js/library/jquery/jquery-ui-1.10.3.custom.js"></script>
	<script src="js/library/masonry/jquery.masonry.min.js"></script>
	<script src="js/library/flexslider/jquery.flexslider-min.js"></script>
	<script src="js/library/dataTables/jquery.dataTables.min.js"></script>
	<script src="js/library/fullcalendar/fullcalendar.min.js"></script>
	<script src="js/library/fullcalendar/gcal.js"></script>
	<script src="js/library/forms/jquery.hotkeys.js"></script>
	<script src="js/library/forms/bootstrap-wysiwyg.js"></script>
	<script src="js/library/forms/picker.js"></script>
	<script src="js/library/forms/picker.date.js"></script>
	<script src="js/library/forms/picker.time.js"></script>
	<script src="js/library/forms/jquery.bootstrap.wizard.min.js"></script>
	<script src="js/library/charts/jquery.flot.js"></script>
	<script src="js/library/charts/jquery.flot.resize.js"></script>
	<script src="js/library/charts/jquery.flot.stack.js"></script>
	<script src="js/library/charts/jquery.flot.pie.min.js"></script>
	<script src="js/library/charts/jquery.sparkline.min.js"></script>
	<script src="js/library/syntaxHighlighter/shCore.js"></script>
	<script src="js/library/syntaxHighlighter/shBrushCss.js"></script>
	<script src="js/library/syntaxHighlighter/shBrushXml.js"></script>
	<script src="js/library/syntaxHighlighter/shBrushJScript.js"></script>
	<script src="js/library/jquery.number.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="js/styler/custom.js"></script>

	<!--timeline-->
	<script defer src="libs/timeline/plugins.js"></script>
	<script defer src="libs/timeline/mylibs/timelinexml.js"></script>
	<script defer src="libs/timeline/script.js"></script>
	<!--date diff-->
    <script src="js/date/moment.min.js"></script><script>var step1, step2, url;

url = 'http://fiddle.jshell.net';

  step1 = $.ajax(url);

  step2 = step1.then(
    function (data) {
        var def = new $.Deferred();

        setTimeout(function () {
            console.log('Request completed');
            def.resolve();
        },2000);

      return def.promise();

  },
    function (err) {
        console.log('Step1 failed: Ajax request');
    }
  );
  step2.done(function () {
      console.log('Sequence completed')
      setTimeout("console.log('end')",1000);
  });
  </script>