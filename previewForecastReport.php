﻿<?php 
	$id = $_GET['id'];
	$page = $_GET['page'];
	$checkmenu = "0";
	function replaceSQuoteAndNl($textinp) {
		$textinp = preg_replace("/\r|\n/", "", $textinp);
		return str_replace("'", "\'", $textinp);
	}
	if ($id) {
		include("INC/connectSFC.php");
		$sqlStr = "SELECT IDForecast FROM Forecast WHERE ID='$id'";
		$query = sqlsrv_query($ConnectSaleForecast, $sqlStr );
		if ($obj = sqlsrv_fetch_object($query)) {
			$id = $obj->IDForecast;
		} else {
?>
	<script type="text/javascript">
		alert("ไม่มีข้อมูลของรายการนี้ในระบบ");
<?php 
			if ($page) {
?>
		window.location = "<?php echo $page?>.php";
<?php 
			} else {
?>
		window.location = "index.php";
<?php 
			}
?>
	</script>
<?php 
		}
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="Izuddin Helmi">
	<title>SFS:::View</title>
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css">
	<link href="js/library/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
	<link href="css/styler/style.css" rel="stylesheet" type="text/css">
	<link href="css/main.css" rel="stylesheet" type="text/css">
	<link href="js/library/syntaxHighlighter/shCore.css" rel="stylesheet" type="text/css"/>
	<link href="js/library/syntaxHighlighter/shThemeDefault.css" rel="stylesheet" type="text/css"/>
	<link href="css/styler/fontello.css" rel="stylesheet" type="text/css">
    <link href="css/styler/animation.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="js/library/jquery/jquery-ui-1.10.3.custom.min.css" />
</head>
<body cz-shortcut-listen="true" style="margin-top:0px;">	
<section class="page-header bg-color white">
	<div class="container">
		<div class="row">
			<div class="span12">
				<div class="section-header clearfix">
					<div class="pull-left">
						<h2 id="forecastid"><i class="icon-chart-2"></i><?php echo $id?></h2>
					</div>
					<div id="shortcut" class="nav-collapse pull-right">
						
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="dashboard content">
	<div class="container">
<!-- ============================================= Cont & Cus =========================================== -->
<div class="row">				
			<div class="span6">					
				<div class="widget widget-initial">
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-suitcase"></i> Contract</span>
					</div>		
					<div class="bg-color turqoise rounded">
						<div class="box-padding" ><h4><input id="inp_ForecastID" type="hidden">
							<table>
							<tr>
								<td class="text-right">PEContract&nbsp;</td>
								<td><input id="inp_contractpe" class="span3"  type="text" style="margin-bottom: 0px;" placeholder="Click Get PEContract" readonly><!--<?php if (!$id) {?><i id="btn_pEContract_modal" class="i_btn icon-search-3" data-toggle="tooltip" data-placement="bottom" data-original-title="Search PEContract"></i><i id="btn_pEContract_del" class="i_btn icon-cancel-alt" data-toggle="tooltip" data-placement="bottom" data-original-title="Delete PEContract"></i><?php  }?>--></td>
							</tr>
							<tr>
								<td class="text-right">E-Contract&nbsp;</td>
								<td><input id="inp_contractec" class="span3"  type="text" placeholder="Get E-Contract" readonly><i id="btn_eContract" class="i_btn icon-exchange" data-toggle="tooltip" data-placement="bottom" data-original-title="Get E-Contract" style="display:none"></i></td>
							</tr>							
							</table>						
						</h4></div>
					</div>

							
				</div>
			</div>
			<div class="span6">
				<div class="widget widget-customer">	
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-article-alt-1"></i> Project Description</span>
					</div>					
					<div class="bg-color turqoise rounded-top">
						<div class="box-padding" style="padding:15px;"><h4>
							<table>
								<tr>
									<td class="text-right">ปีงบประมาณ&nbsp;</td>
									<td ><input id="projDesc_Year" class="span1" type="text" placeholder="2557" style="background:#FDD752;"></td>
								</tr>
								<tr>
									<td class="text-right">Project Name&nbsp;</td>
									<td><textarea id="projDesc_Name" rows="3" class="span3" style="background:#FDD752;"></textarea><i style="vertical-align: bottom;margin-bottom:5px;" id="btn_help_modal" class="i_btn icon-newspaper" data-toggle="tooltip" data-placement="bottom" data-original-title="เอกสารหลักการตั้งชื่อโครงการ" style="display:none"></i></td>
								</tr>
							</table>							
						</h4></div>
					</div>					
					<div class="bg-color dark-grey rounded-bottom" style="padding:15px;">
						<span id="projDesc_show" title="[ปีงบประมาณ] Payer code/enduser code:project name"></span>
					</div>
				</div>				
			</div>
		</div>

		<div class="row">				
			<div class="span6">					
				<div class="widget widget-initial">
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-users-3"></i> Payer <a class="btn btn-pink" id="payer_copy" style="margin-top: -15px;font-weight:bold;display:none">Copy Enduser</a></span>
					</div>		<input id="payer_custno" type="hidden">
					<div class="bg-color turqoise rounded">
						<div class="box-padding" style="padding:15px;"><h4>
							<table>
								<tr>
									<td class="text-right">Customer Name&nbsp;</td>
									<td><input id="payer_name" class="span3" type="text" readonly><i id="btn_payercust_modal" class="i_btn icon-search-3" data-toggle="tooltip" data-placement="bottom" data-original-title="Search Customer" style="display:none"></i></td>
									<!--<?php  //if (!$id) { ?><i id="btn_customer_modal" class="i_btn icon-search-3" data-toggle="tooltip" data-placement="bottom" data-original-title="Search Customer"></i><?php  //} ?>-->
								</tr>	
								<tr>
									<td class="text-right">dep/div Code&nbsp;</td>
									<td><input id="payer_code" class="span3" type="text" placeholder="dep-div-sec-user" style="background:#FDD752;" readonly><i id="btn_payercode_modal" class="i_btn icon-search-3" data-toggle="tooltip" data-placement="bottom" data-original-title="Search Oraganization" style="display:none"></i></td>
								</tr>
								<tr>
									<td class="text-right">dep/div Name&nbsp;</td>
									<td><textarea id="payer_org" rows="3" class="span3" readonly></textarea></td>
								</tr>
								<tr>
									<td class="text-right" >Industry&nbsp;</td>
									<td  ><input id="PayerIndustry" class="span3" type="text" value="" readonly >
									</td>
								</tr>
							</table>							
						</h4></div>
					</div>
				</div>
			</div>
			<div class="span6">					
				<div class="widget widget-customer">	
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-users-3"></i> End User <a class="btn btn-pink" style="margin-top: -15px;font-weight:bold;display:none" id="enduser_copy">Copy Payer</a></span>
						<input id="enduser_custno" type="hidden">
					</div>					
					<div class="bg-color turqoise rounded">
						<div class="box-padding" style="padding:15px;"><h4>
							<table>
								<tr>
									<td class="text-right">Customer Name&nbsp;</td>
									<td><input id="enduser_name" class="span3" type="text" readonly><i id="btn_endusercust_modal" class="i_btn icon-search-3" data-toggle="tooltip" data-placement="bottom" data-original-title="Search Customer" style="display:none"></i></td>
									<!--<?php  //if (!$id) { ?><i id="btn_customer_modal" class="i_btn icon-search-3" data-toggle="tooltip" data-placement="bottom" data-original-title="Search Customer"></i><?php  //} ?>-->
								</tr>	
								<tr>
									<td class="text-right">dep/div Code&nbsp;</td>
									<td><input id="enduser_code" class="span3" type="text" placeholder="dep-div-sec-user" style="background:#FDD752;" readonly><i id="btn_endusercode_modal" class="i_btn icon-search-3" data-toggle="tooltip" data-placement="bottom" data-original-title="Search Oraganization" style="display:none"></i></td>
								</tr>
								<tr>
									<td class="text-right">dep/div Name&nbsp;</td>
									<td><textarea id="enduser_org" rows="3" class="span3" readonly></textarea></td>
								</tr>
								<tr>
									<td class="text-right" >Industry&nbsp;</td>
									<td  ><input id="EndUserIndustry" class="span3" type="text" value="" readonly >
									</td>
								</tr>
							</table>							
						</h4></div>
					</div>
				</div>				
			</div>
		</div>
<!-- ==================================================================================================== -->
<!-- ============================================= Targ & Time ========================================== -->
		<div class="row">				
			<div class="span6">					
				<div class="widget widget-target">	
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-target-2"></i> Target</span>
						<div class="dropdown pull-right"><h4 style="margin: 0px;">
							<!--<label class="checkbox inline" style="color: #0072C6; font-weight: bold; padding-top: 0px;">
								<input id="chk_esri" type="checkbox" value="1" disabled>
								ESRI's TOR
							</label>
							<label class="checkbox inline" style="color: #0072C6; font-weight: bold; padding-top: 0px;">
								<input id="chk_edu" type="checkbox" value="1" disabled>
								Education
							</label>
							<label class="checkbox inline" style="color: #0072C6; font-weight: bold; padding-top: 0px;">
								<input id="chk_proj" type="checkbox" value="1" disabled>
								Project
							</label>-->
							<label class="checkbox inline" style="color: #AC193D; font-weight: bold; padding-top: 0px;">
								<input id="chk_sproj" type="checkbox" value="1" disabled>
								Mega Project
							</label>
						</h4></div>
					</div>					
					<div class="bg-color turqoise rounded">
						<div class="box-padding"><h5>
							<table>
								<tr>
									<td class="text-right" style="width: 100px;"><h4>Potential&nbsp;</h4></td>
									<td class="text-right" style="width: 60px;"><input id="inp_potentialp" class="span1" type="text" value="10%" readonly></td>
									<td class="text-right"><input id="inp_potentialt" class="span3" type="text" value="โอกาสน้อยมาก" readonly><i id="btn_potential_modal" class="i_btn icon-th-list" title="Load Potential" style="display: none;"></i><i id="btn_hPotential_modal" class="i_btn icon-clock-8" title="History"></i></td>
									
								</tr>
								<tr>
									<td class="text-right"><h4>Progress&nbsp;</h4></td>
									<td class="text-right"><input id="inp_progressp" class="span1" type="text" value="10%" readonly></td>
									<td class="text-right"><input id="inp_progresst" class="span3" type="text" value="พิจารณาจัดตั้งโครงการใหม่อยู่" readonly><i id="btn_progress_modal" class="i_btn icon-th-list" title="Load Progress" style="display: none;"></i><i id="btn_hProgress_modal" class="i_btn icon-clock-8" title="History"></i></td>
									
								</tr>
							</table>							
							<table>
								<tbody>
									<tr>
										<td colspan="2" class="text-right" style="width: 270px;"><h4>Excluded VAT</h4></td>
										<td style="width: 50px;"></td>
										<td class="text-right"><h4>Included VAT</h4></td>
										<td style="width: 50px;"></td>
									</tr>
									<tr>
										<td class="text-right">Budget&nbsp;</td>
										<td><input id="inp_budg_evat" class="span2 number text-right" type="text" placeholder="0.00" readonly></td>
										<td>Baht</td>
										<td><input id="inp_budg_ivat" class="span2 number text-right" type="text" placeholder="0.00" readonly></td>
										<td>Baht</td>
									</tr>
									<tr>
										<td class="text-right">Income&nbsp;</td>
										<td><input id="inp_inco_evat" class="span2 number text-right" type="text" placeholder="0.00" readonly></td>
										<td>Baht</td>
										<td><input id="inp_inco_ivat" class="span2 number text-right" type="text" placeholder="0.00" readonly></td>
										<td>Baht</td>
									</tr>
									<tr>
										<td class="text-right">Cost&nbsp;</td>
										<td><input id="inp_cost_evat" class="span2 number text-right" type="text" placeholder="0.00" readonly></td>
										<td>Baht</td>
										<td><input id="inp_cost_ivat" class="span2 number text-right" type="text" placeholder="0.00" readonly></td>
										<td>Baht</td>
									</tr>
								</tbody>
							</table>
						</h5></div>
					</div>
				</div>
			</div>
			<div class="span6">					
				<div class="widget widget-timeframe">	
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-calendar-6"></i> Time Frame</span>
						<div class="dropdown pull-right"></div>
					</div>					
					<div class="bg-color turqoise rounded">
						<div class="box-padding"><h5>
							<table>
							<tr>
								<td class="text-right">Bidding Date&nbsp;</td>
								<td><input id="inp_biddDate" class="form_date" style="width: 80px;" type="text" placeholder="choose date" readonly></td>
								<td class="text-right">Contract Sign Date&nbsp;</td>
								<td><input id="inp_signDate" class="form_date" style="width: 80px;" type="text" placeholder="choose date" readonly></td>
							</tr>	
							<tr>
								<td class="text-right">Project Duration&nbsp;</td>
								<td><input id="inp_pDuration" class="span1 nNumber text-right" type="text" placeholder="0" readonly> Days</td>
								<td class="text-right">End of Contract&nbsp;</td>
								<td><input id="inp_endCont" class="span1" style="width: 80px;" type="text" readonly></td>
							</tr>
							<tr>
								<td class="text-right"><input id="chk_downPayment" type="checkbox" value="" style="margin-top: 0px;" checked disabled> Down Payment&nbsp;</td>
								<td><input id="inp_downPayment" class="span1 number text-right" type="text" placeholder="0" readonly>&nbsp;&nbsp;&nbsp;&nbsp;% =</td>
								<td colspan="2"><input id="inp_dPBaht" class="span2 number text-right" type="text" style="width: 150px;" placeholder="0.00" readonly> Baht</td>
							</tr>
							<tr>
								<td class="text-right">Inv.Date&nbsp;</td>
								<td><input id="inp_dPDays" class="span1 nNumber text-right" type="text" placeholder="0" readonly> Days =&nbsp;</td>
								<td colspan="2"><input id="inp_dPDate" class="form_date" style="width: 80px;" type="text" placeholder="choose date" readonly></td>
							</tr>
							</table>							
							 </h5>
						</div>
					</div>
					<div class="widget-header clearfix" style="margin-top:5px;">
						<span class="pull-left"><i class="icon-clock-5"></i>Timeline Progress</span>
						<section class="demo">
							<div class="demo-box">
							</div>
						</section>
					</div>
				</div>				
			</div>
		</div>

		<div class="row closer">	
			<div class="span6" id="Progress_30_div" style="display:none">					
				<div class="widget widget-target">	
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-progress-1"></i> Progress 30% ทำราคากลาง</span>
						<div class="dropdown pull-right"><h4 style="margin: 0px;"></div>
					</div>					
					<div class="bg-color blue rounded">
						<div class="box-padding"><h5>
							<table>
								<tbody>
									<tr>
										<td style="width: 350px;" class="text-right" colspan="2"><h4>Company</h4></td>
										<td style="width: 20px;"></td>
										<td class="text-right"><h4>Price</h4></td>
										<td style="width: 20px;"></td>
									</tr>
									<tr>
										<td class="text-right">1.&nbsp;</td>
										<td><input type="text" class="span3" id="P30company1_name" readonly style="background:#FFFFFF !important;" ></td>
										<td><i data-original-title="Load Company" data-placement="bottom" data-toggle="tooltip" class="i_btn icon-th-list" id="btn_P30_modal"></i></td>
										<td><input type="text" value="0.00" class="span2 number text-right" id="P30company1_price" maxlength="20"></td>
										<td>Baht</td>
									</tr>
									<tr>
										<td class="text-right">2.&nbsp;</td>
										<td><input type="text" class="span3" id="P30company2_name" readonly style="background:#FFFFFF !important;" ></td>
										<td><i data-original-title="Load Company" data-placement="bottom" data-toggle="tooltip" class="i_btn icon-th-list" id="btn2_P30_modal"></i></td>
										<td><input type="text"  value="0.00" class="span2 number text-right" id="P30company2_price" maxlength="20"></td>
										<td>Baht</td>
									</tr>
									<tr>
										<td class="text-right">3.&nbsp;</td>
										<td><input type="text" class="span3" id="P30company3_name" readonly style="background:#FFFFFF !important;" ></td>
										<td><i data-original-title="Load Company" data-placement="bottom" data-toggle="tooltip" class="i_btn icon-th-list" id="btn3_P30_modal"></i></td>
										<td><input type="text" value="0.00" class="span2 number text-right" id="P30company3_price" maxlength="20"></td>
										<td>Baht</td>
									</tr>
								</tbody>
							</table>
						</h5>
						</div>
					</div>
				</div>
			</div>
			<div class="span6" id="Progress_60_div" style="display:none">					
				<div class="widget widget-target">	
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-progress-2"></i> Progress 60% ยื่นซอง</span>
						<div class="dropdown pull-right"><h4 style="margin: 0px;"></div>
					</div>					
					<div class="bg-color blue rounded">
						<div class="box-padding"><h5>
							<table>
								<tbody>
									<tr>
										<td style="width: 350px;" class="text-right" colspan="2"><h4>Company</h4></td>
										<td style="width: 20px;"></td>
										<td class="text-right"><h4>Price</h4></td>
										<td style="width: 20px;"></td>
									</tr>
									<tr>
										<td class="text-right">1.&nbsp;</td>
										<td><input type="text" class="span3" id="P60company1_name" readonly style="background:#FFFFFF !important;"></td>
										<td><i data-original-title="Load Company" data-placement="bottom" data-toggle="tooltip" class="i_btn icon-th-list" id="btn_P60_modal"></i></td>
										<td><input type="text" value="0.00" class="span2 number text-right" id="P60company1_price" maxlength="20"></td>
										<td>Baht</td>
									</tr>
									<tr>
										<td class="text-right">2.&nbsp;</td>
										<td><input type="text" class="span3" id="P60company2_name" readonly style="background:#FFFFFF !important;"></td>
										<td><i data-original-title="Load Company" data-placement="bottom" data-toggle="tooltip" class="i_btn icon-th-list" id="btn2_P60_modal"></i></td>
										<td><input type="text"  value="0.00" class="span2 number text-right" id="P60company2_price" maxlength="20"></td>
										<td>Baht</td>
									</tr>
									<tr>
										<td class="text-right">3.&nbsp;</td>
										<td><input type="text" class="span3" id="P60company3_name" readonly style="background:#FFFFFF !important;"></td>
										<td><i data-original-title="Load Company" data-placement="bottom" data-toggle="tooltip" class="i_btn icon-th-list" id="btn3_P60_modal"></i></td>
										<td><input type="text" value="0.00" class="span2 number text-right" id="P60company3_price" maxlength="20"></td>
										<td>Baht</td>
									</tr>
								</tbody>
							</table>
						</h5>
						</div>
					</div>
				</div>
			</div>
		</div>
<!-- ==================================================================================================== -->
<!-- ============================================= Invo & Book ========================================== -->
		<div class="row">				
			<div class="span8">					
				<div class="widget widget-invoicing">	
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-list-alt"></i> Invoicing Plan</span>
					</div>					
					<div class="bg-color white rounded">
						<div class="box-padding" style="padding:5px">
			    			<table class="table table-striped">
								<thead>
									<tr style="background-color: #2ABF9E; color: #FFFFFF;">
										<th style="width: 50px;">Phase</th>
										<th style="width: 50px;">Days</th>
										<th style="width: 70px;">Date</th>
										<th style="width: 50px;">%</th>
										<th style="width: 120px;">Amount</th>
										<th>Remark</th>
									</tr>
								</thead>
								<tbody id="tbody_invoicing"></tbody>
							</table>
				        </div>
					</div>
				</div>
			</div>
			<div class="span4">					
				<div class="widget widget-book">	
					<div class="widget-header clearfix">
						<span class="pull-left" style="padding-bottom: 3px;"><i class="icon-dollar"></i> Book/Forward</span>
					</div>					
					<div class="bg-color white rounded">
						<div class="box-padding" style="padding:5px">
			    			<table class="table table-striped">
								<thead>
									<tr style="background-color: #2ABF9E; color: #FFFFFF;">
										<th style="width: 80px;">Type</th>
										<th style="width: 80px;">Year</th>
										<th>Amount</th>
									</tr>
								</thead>
								<tbody id="tbody_bank"></tbody>
							</table>
				        </div>
					</div>
				</div>
			</div>
		</div>
<!-- ==================================================================================================== -->
<!-- ============================================= Sales Rep. =========================================== -->
		<div class="row">
			<div class="span12">					
				<div class="widget widget-representative">
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-user-5"></i> sales representative</span>
						<div class="dropdown pull-right"></div>
					</div>		
					<div class="bg-color dark-blue rounded">
						<div class="box-padding">
							<h4>
							<table>
								<tr>
									<td class="text-right">Sales Representative&nbsp;</td>
									<td><input id="inp_salesname" class="span3" type="text" readonly></td>
								</tr>	
								<tr>
									<td class="text-right">Project By&nbsp;</td>
									<td>
										<select id="slc_salesproj" class="span2" style="background-color: #F2DEDE !important;" disabled>
											<option value="Sales 1">Sales 1</option>
											<option value="Sales 2">Sales 2</option>
											<option value="Sales 3">Sales 3</option>
											<option value="Sales 4">Sales 4</option>
											<option value="Sales TM">Sales TM</option>
											<option value="Sales CSD">Sales CSD</option>
											<option value="Sales GEO">Sales GEO</option>
											<option value="Sales CLMV">Sales CLMV</option>
										</select>
									</td>
								</tr>
								<tr>
									<td class="text-right">Remark&nbsp;</td>
									<td><textarea id="txa_salesremark" rows="3" style="width: 255px;" readonly></textarea></td>
								</tr>
							</table>							
							</h4>
						</div>
					</div>
				</div>
			</div>	
		</div>
<!-- ==================================================================================================== -->
<!-- ============================================= Status & Dept ======================================== -->
		<div class="row">					
			<div class="span8">					
				<div class="widget widget-status">	
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-tasks-1"></i> Status</span>
					</div>					
					<div class="bg-color white rounded">
						<div class="box-padding" style="padding:5px">
			    			<table class="table table-striped">
								<thead>
									<tr style="background-color: #2ABF9E; color: #FFFFFF;">
										<th style="width: 100px;">Date</th>
										<th>Description</th>
									</tr>
								</thead>
								<tbody id="tbody_status"></tbody>
							</table>
				        </div>
					</div>
				</div>
			</div>
			<div class="span4">					
				<div class="widget widget-Dept">	
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-sitemap"></i> Department Support</span>
					</div>					
					<div class="bg-color white rounded">
						<div class="box-padding" style="padding:5px">
			    			<table class="table table-striped">
								<thead>
									<tr style="background-color: #2ABF9E; color: #FFFFFF;">
										<th>Description</th>
									</tr>
								</thead>
								<tbody id="tbody_deptsup"></tbody>
							</table>
				        </div>
					</div>
				</div>
			</div>
		</div>
<!-- ==================================================================================================== -->
<!-- ============================================= Desc & % ============================================= -->
		<div class="row">				
			<div class="span12">					
				<div class="widget widget-description">	
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-article-alt-1"></i> Description</span>
					</div>					
					<div class="bg-color white rounded-top">
						<div class="box-padding" style="padding:5px">
			    			<table class="table table-striped">
								<thead>
									<tr style="background-color: #2ABF9E; color: #FFFFFF;">
										<th style="width: 100px;">Type</th>
										<th>Description</th>
										<th style="width: 150px;">Price</th>
									</tr>
								</thead>
								<tbody id="tbody_description"></tbody>
							</table>
				        </div>
					</div>
				</div>
			</div>
		</div>
<!-- ==================================================================================================== -->
<!-- ============================================= Product ============================================== -->
		<div class="row">
			<div class="span6">					
				<div class="widget widget-esriproduct">	
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-star-5"></i>ESRI Product</span>
					</div>					
					<div class="bg-color white rounded">
						<div class="box-padding" style="padding:5px">
			    			<table class="table table-striped">
								<thead>
									<tr style="background-color: #2ABF9E; color: #FFFFFF;">
										<th>Description</th>
										<th style="width: 50px;">Qty</th>
									</tr>
								</thead>
								<tbody id="tbody_esri"></tbody>
							</table>
				        </div>
					</div>
				</div>
			</div>
			<div class="span6">					
				<div class="widget widget-SEproduct">	
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-star-5"></i>Schneider Electric Product</span>
					</div>					
					<div class="bg-color white rounded">
						<div class="box-padding" style="padding:5px">
			    			<table class="table table-striped">
								<thead>
									<tr style="background-color: #2ABF9E; color: #FFFFFF;">
										<th>Description</th>
										<th style="width: 50px;">Qty</th>
									</tr>
								</thead>
								<tbody id="tbody_SE"></tbody>
							</table>
				        </div>
					</div>
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="span6">					
				<div class="widget widget-garmin">	
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-star-5"></i>Garmin Product</span>
					</div>					
					<div class="bg-color white rounded">
						<div class="box-padding" style="padding:5px">
			    			<table class="table table-striped">
								<thead>
									<tr style="background-color: #2ABF9E; color: #FFFFFF;">
										<th>Description</th>
										<th style="width: 50px;">Qty</th>
									</tr>
								</thead>
								<tbody id="tbody_garmin"></tbody>
							</table>
				        </div>
					</div>
				</div>
			</div>
			<div class="span6">					
				<div class="widget widget-nostra">	
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-star-5"></i>Nostra Product </span>
					</div>					
					<div class="bg-color white rounded">
						<div class="box-padding" style="padding:5px">
			    			<table class="table table-striped">
								<thead>
									<tr style="background-color: #2ABF9E; color: #FFFFFF;">
										<th>Description</th>
										<th style="width: 50px;">Qty</th>
									</tr>
								</thead>
								<tbody id="tbody_nostra"></tbody>
							</table>
				        </div>
					</div>
				</div>
			</div>				
		</div>
		<div class="row">

			<div class="span6">					
				<div class="widget widget-leicaproduct">	
					<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-star-5"></i>Geomatic's Product</span>
					</div>					
					<div class="bg-color white rounded">
						<div class="box-padding" style="padding:5px">
			    			<table class="table table-striped">
								<thead>
									<tr style="background-color: #2ABF9E; color: #FFFFFF;">
										<th>Description</th>
										<th style="width: 50px;">Qty</th>
									</tr>
								</thead>
								<tbody id="tbody_leica"></tbody>
							</table>
				        </div>
					</div>
				</div>
			</div>
			<div class="span6">	
			</div>
		</div>
<!-- ==================================================================================================== -->
		<div class="row">
			<div class="span12">
				<div class="widget widget-save">
					<div class="widget-header clearfix">
						<div class="dropdown pull-right">
							<a id="btn_cancel" class="btn btn-blue" style="color: #FFFFFF">CLOSE</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div id="footer" class="bg-color dark-blue">
	<div class="container">
		<div class="box-padding">
			Copyright &copy; 2013 Sales Forecast
		</div>
	</div>
</div>
<div class="modal hide fade" id="potential_modal">
	<div class="modal-header bg-color light-green aligncenter">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    <h1>Potential</h1>
	</div>
	<div class="modal-body">
		<table class="table table-striped table-condensed table_select">
			<thead>
				<tr style=" background-color: #2ABF9E; color: #FFFFFF;">
					<th style="width: 20%;">%</th>
					<th style="width: 80%;">Description</th>
				</tr>
			</thead>
			<tbody id="potential_tbody_list"></tbody>
		</table>
	</div>
	<div class="modal-footer alignright">
	    <a class="btn btn-red" data-dismiss="modal">CLOSE</a>
	</div>
</div>
<div class="modal hide fade" id="progress_modal">
	<div class="modal-header bg-color light-green aligncenter">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    <h1>Progress</h1>
	</div>
	<div class="modal-body">
		<table class="table table-striped table-condensed table_select">
			<thead>
				<tr style=" background-color: #2ABF9E; color: #FFFFFF;">
					<th style="width: 20%;">%</th>
					<th style="width: 80%;">Description</th>
				</tr>
			</thead>
			<tbody id="progress_tbody_list"></tbody>
		</table>
	</div>
	<div class="modal-footer alignright">
	    <a class="btn btn-red" data-dismiss="modal">CLOSE</a>
	</div>
</div>

<div class="modal hide fade" id="hPotential_modal">
	<div class="modal-header bg-color light-green aligncenter">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    <h1>Potential History</h1>
	</div>
	<div class="modal-body">
		<table class="table table-striped">
			<thead>
				<tr style=" background-color: #2ABF9E; color: #FFFFFF;">
					<th style="width: 15%;">Date</th>
					<th style="width: 15%;">Potential</th>
					<th style="width: 70%;">Description</th>
				</tr>
			</thead>
			<tbody id="hPotential_tbody_list"></tbody>
		</table>
	</div>
	<div class="modal-footer alignright">
	    <a class="btn btn-red" data-dismiss="modal">CLOSE</a>
	</div>
</div>
<div class="modal hide fade" id="hProgress_modal">
	<div class="modal-header bg-color light-green aligncenter">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    <h1>Progress History</h1>
	</div>
	<div class="modal-body">
		<table class="table table-striped">
			<thead>
				<tr style=" background-color: #2ABF9E; color: #FFFFFF;">
					<th style="width: 15%;">Date</th>
					<th style="width: 15%;">Progress</th>
					<th style="width: 70%;">Description</th>
				</tr>
			</thead>
			<tbody id="hProgress_tbody_list"></tbody>
		</table>
	</div>
	<div class="modal-footer alignright">
	    <a class="btn btn-red" data-dismiss="modal">CLOSE</a>
	</div>
</div>
<div class="modal hide fade" id="sales_modal">
	<div class="modal-header bg-color light-green aligncenter">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    <h1>Sales Representative</h1>
	</div>
	<div class="modal-body">
		<table class="table table-striped table-condensed table_select">
			<thead>
				<tr style=" background-color: #2ABF9E; color: #FFFFFF;">
					<th style="width: 15%;">Emp. No.</th>
					<th style="width: 85%;">Name</th>
				</tr>
			</thead>
			<tbody id="sales_tbody_list"></tbody>
		</table>
	</div>
	<div class="modal-footer alignright">
	    <a class="btn btn-red" data-dismiss="modal">CLOSE</a>
	</div>
</div>
	<script src="js/library/jquery/jquery-1.9.1.js"></script>
	<script src="js/library/jquery/jquery-ui-1.10.3.custom.js"></script>
	<script src="js/library/masonry/jquery.masonry.min.js"></script>
	<script src="js/library/flexslider/jquery.flexslider-min.js"></script>
	<script src="js/library/dataTables/jquery.dataTables.min.js"></script>
	<script src="js/library/fullcalendar/fullcalendar.min.js"></script>
	<script src="js/library/fullcalendar/gcal.js"></script>
	<script src="js/library/forms/jquery.hotkeys.js"></script>
	<script src="js/library/forms/bootstrap-wysiwyg.js"></script>
	<script src="js/library/forms/picker.js"></script>
	<script src="js/library/forms/picker.date.js"></script>
	<script src="js/library/forms/picker.time.js"></script>
	<script src="js/library/forms/jquery.bootstrap.wizard.min.js"></script>
	<script src="js/library/charts/jquery.flot.js"></script>
	<script src="js/library/charts/jquery.flot.resize.js"></script>
	<script src="js/library/charts/jquery.flot.stack.js"></script>
	<script src="js/library/charts/jquery.flot.pie.min.js"></script>
	<script src="js/library/charts/jquery.sparkline.min.js"></script>
	<script src="js/library/syntaxHighlighter/shCore.js"></script>
	<script src="js/library/syntaxHighlighter/shBrushCss.js"></script>
	<script src="js/library/syntaxHighlighter/shBrushXml.js"></script>
	<script src="js/library/syntaxHighlighter/shBrushJScript.js"></script>
	<script src="js/library/jquery.number.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="js/styler/custom.js"></script>
	<script type="text/javascript">
	    $(document).ready(function(){
//------------------------------------------------- cancel ----------------------------------------------------
	$("#btn_cancel").on("click", function() {
		window.close();
		
	});
//-----------------------------------------------------------------------------------------------------------
//------------------------------------------------- Function ------------------------------------------------
			function numFormat(x) {
				var parts = x.toString().split(".");
			    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			    return parts.join(".");
			}
			function calSIProj() {
				hw = sw = app = data = false;
				$("#tbody_description").children().each(function() {
					tempP = $($(this).find("p")[0]).text();
					if (tempP == "HW") {
						hw = true;
					} else if (tempP == "SW") {
						sw = true;
					} else if (tempP == "Application") {
						app = true;
					} else if (tempP == "Data") {
						data = true;
					}
				});
				(hw && sw && app && data) ? $("#siproject").text("SI Project") : $("#siproject").text("Non SI Project");
			}
			function calSignProj() {
				if (parseInt($("#inp_potentialp").val().replace(/%/g, "")) >= 50 && parseFloat($("#inp_inco_evat").val()) >= 5000000) {
    				$("#sigproject").text("Significant Project");
    			} else {
    				$("#sigproject").text("Non Significant Project");
    			}
			}
			function calEndCont() {
				if ($("#inp_signDate").val() != "") {
					slcdate = $("#inp_signDate").datepicker('getDate', '+1d');
					if ($("#inp_pDuration").val() != "") {
						slcdate.setDate(slcdate.getDate() + parseInt($("#inp_pDuration").val()));
					}
					$("#inp_endCont").val(("0" + slcdate.getDate()).slice(-2) + "/" + ("0" + (slcdate.getMonth() + 1)).slice(-2) + "/" + slcdate.getFullYear());

					slcdate = $("#inp_signDate").datepicker('getDate', '+1d');
					if ($("#inp_dPDays").val() != "") {
						slcdate.setDate(slcdate.getDate() + parseInt($("#inp_dPDays").val()));
					}
					$("#inp_dPDate").val(("0" + slcdate.getDate()).slice(-2) + "/" + ("0" + (slcdate.getMonth() + 1)).slice(-2) + "/" + slcdate.getFullYear());
				} else {
					$("#inp_endCont").val("");
					$("#inp_dPDate").val("");
				}
			}
			function load_Tb_DeptSupport(deptID,dept) {
				var tr = $('<tr class="warning"><td><p class="lead14">' + dept + '</p><input type="hidden" value ="'+ deptID +'"></td></tr>');
				//$(tr.find("td")[1]).append(td_del);
				$("#tbody_deptsup").append(tr);
			}
//-----------------------------------------------------------------------------------------------------------

//------------------------------------------------- Initial -------------------------------------------------
			var _loginID = "<?php echo $_COOKIE['Ses_ID']?>";
	    	var old_inco_ivat, old_inco_evat, old_biddDate, old_signDate, old_pDuration, old_downPayment, old_dPBaht;
	    	var description_type = ["HW", "SW", "Application", "Data", "Site Prep", "On Site", "Training", "Other"];

			$(".form_date").datepicker({
		        dateFormat: 'dd/mm/yy',
		        minDate: -1,
		        maxDate: -2
	    	});
	    	$(".nNumber").number(true, 0);
	    	$(".number").number(true, 2);
	    	$(".form_date").css("cursor", "pointer");
	    	list_potential = [["100%", "มั่นใจว่าได้แน่"], ["75%", "มั่นใจ"], ["50%", "ไม่แน่ใจว่าจะได้โครงการหรือไม่"], ["25%", "พอมีโอกาส"], ["10%", "โอกาสน้อยมาก"]];
	    	list_progress = [["100%", "เซ็นสัญญาแล้ว"], ["90%", "ประกาศผลแล้วรอเซ็นสัญญา"], ["80%", "ยื่นซองแล้ว"], ["70%", "ประกาศประกวดราคาแล้วรอยื่นซอง"], ["60%", "จัดส่งของบประมาณให้สำนักงบประมาณแล้ว"], ["50%", "จัดทำ spec. ให้หน่วยงานอยู่"], ["40%", "ขั้นตอนการพิจารณาอนุมัติงบประมาณ"], ["30%", "ร่างโครงการ (ของบประมาณ)"], ["20%", "Presentation/Demo"], ["10%", "พิจารณาจัดตั้งโครงการใหม่อยู่"], ["0%", "แพ้"],["P", "Pending"], ["v", "ยกเลิกโครงการ"]];
	    	$(list_potential).each(function() {
	    		var item = this;
	    		tr = $('<tr><td>' + item[0] + '</td><td>' + item[1] + '</td></tr>');
	    		tr.click(function() {
	    			$("#inp_potentialp").val(item[0]);
	    			$("#inp_potentialt").val(item[1]);
	    			$("#potential_modal").modal("hide");
	    		});
	    		$("#potential_tbody_list").append(tr);
    		});
	    	
	    	$(list_progress).each(function() {
	    		var item = this;
	    		tr = $('<tr><td>' + item[0] + '</td><td>' + item[1] + '</td></tr>');
	    		tr.on("click", function() {
	    			$("#inp_progressp").val(item[0]);
	    			$("#inp_progresst").val(item[1]);
	    			$("#progress_modal").modal("hide");
	    			var Sprogress=item[0].replace(/%/g, "");
	    			console.log("progress on tr ",Sprogress);
	    			//projectStatus(item[0]);
	    			if(Sprogress=='0' )
	    			{
	    				
	    				//$("#Reason").css("display", "");

	    				$("#Progress_30_div").css("display", "none");
	    				$("#Progress_60_div").css("display", "none");
	    			}
	    			else if(Sprogress=='10'||Sprogress=='20'||Sprogress=='v'||Sprogress=='P')
	    			{
	    				$("#Progress_30_div").css("display", "none");
	    				$("#Progress_60_div").css("display", "none");
	    				
	    			}
	    			else if(Sprogress=='30')
	    			{
	    				//$("#Reason").css("display", "none");
	    				//$("#Price30_modal").modal('show');
	    				$("#Progress_30_div").css("display", "block");
	    				$("#Progress_60_div").css("display", "none");
	    				$("#Progress_30_div").focus();
	    				
	    			}
	    			else if(Sprogress=='40'||Sprogress=='50')
	    			{
	    				//$("#Reason").css("display", "none");
	    				//$("#Price30_modal").modal('show');
	    				$("#Progress_30_div").css("display", "block");
	    				$("#Progress_60_div").css("display", "none");
	    				
	    			}
	    			else if(Sprogress=='60')
	    			{
	    				//$("#Reason").css("display", "none");
	    				//$("#Price60_modal").modal('show');
	    				$("#Progress_30_div").css("display", "block");
	    				$("#Progress_60_div").css("display", "block");
	    				$("#Progress_60_div").focus();
	    				
	    			}
	    			else if(Sprogress=='70'||Sprogress=='80'||Sprogress=='90')
	    			{
	    				//$("#Reason").css("display", "none");
	    				//$("#Price60_modal").modal('show');
	    				$("#Progress_30_div").css("display", "block");
	    				$("#Progress_60_div").css("display", "block");
	    				
	    			}
	    			else if(Sprogress=='100')
	    			{
	    				
	    				$("#Progress_30_div").css("display", "block");
	    				$("#Progress_60_div").css("display", "block");
	    				//$("#Reason").css("display", "");
	    			}
	    			else
	    			{

	    			}
	    		});
	    		$("#progress_tbody_list").append(tr);
    		});
    		/*$.ajax({
    			type: "POST",
				dataType: "json",
		        url: "AJAX/listSalesRepresentative.php",
		        success: function(json) {
		        	$.each(json, function() {
	    				var item = this;
	    				tr = $('<tr><td>' + item[0] + '</td><td>' + item[1] + '</td></tr>');
	    				tr.click(function() {
			    			$("#inp_salesname").val(item[1]);
			    			$.ajax({
			    				type: "POST",
								dataType: "json",
						        url: "AJAX/getSalesDepartment.php",
						        data: {
						        	emp: item[0]
						        },
						        success: function(json) {
						        	$("#slc_salesproj").val(json[0][0]);
								}
						    });
			    			$("#sales_modal").modal("hide");
			    		});
			    		if (parseInt(_loginID) == item[0]) tr.click();
			    		$("#sales_tbody_list").append(tr);
	    			});
		        }
		    });*/
//-----------------------------------------------------------------------------------------------------------

//------------------------------------------------- Event ---------------------------------------------------
	    	$("#btn_hPotential_modal").click(function() {
	    		$("#hPotential_modal").modal("show");
	    	});
	    	$("#btn_hProgress_modal").click(function() {
	    		$("#hProgress_modal").modal("show");
	    	});
	    	$("#inp_biddDate").change(function() {
	    		if ($("#inp_biddDate").val() != "" && $("#inp_signDate").val() == "") {
		    		slcdate = $("#inp_biddDate").datepicker('getDate', '+1d');
					slcdate.setDate(slcdate.getDate() + 45);
					$("#inp_signDate").datepicker('setDate', slcdate);
					calEndCont();
	    		}
	    	});
	    	$("#inp_signDate").change(function() {
    			if ($("#inp_signDate").val() != "" && $("#inp_biddDate").val() == "") {
		    		slcdate = $("#inp_signDate").datepicker('getDate', '+1d');
					slcdate.setDate(slcdate.getDate() - 45);
					$("#inp_biddDate").datepicker('setDate', slcdate);
	    		}
	    		calEndCont();
	    	});
	    	$("#inp_inco_evat").blur(function() {
	    		$("#inp_inco_ivat").val($("#inp_inco_evat").val() * 1.07);
	    		$("#inp_dPBaht").val($("#inp_inco_evat").val() * ($("#inp_downPayment").val() / 100));
				$("#inp_downPayment").val((100 / $("#inp_inco_evat").val()) * $("#inp_dPBaht").val());
				tr = '<tr class="error"><td><p class="lead14">Book</p></td><td><p class="lead14">' + new Date().getFullYear() + '</p></td><td><p class="lead14">' + numFormat(parseFloat($("#inp_inco_evat").val()).toFixed(2)) + '</p></td></tr>';
				$("#tbody_bank").empty();
				$("#tbody_bank").append(tr);
				calSignProj();
	    	});
	    	$("#inp_budg_evat").blur(function() {
	    		$("#inp_budg_ivat").val($("#inp_budg_evat").val() * 1.07);
	    	});
	    	$("#inp_cost_evat").blur(function() {
				$("#inp_cost_ivat").val($("#inp_cost_evat").val() * 1.07);
	    	});
	    	$("#chk_downPayment").change(function() {
	    		if ($(this).prop("checked")) {
	    			$("#inp_downPayment").prop("disabled", false);
	    			$("#inp_dPBaht").prop("disabled", false);
	    			$("#inp_dPDays").prop("disabled", false);
	    			$("#inp_dPDate").prop("disabled", false);
	    		} else {
	    			$("#inp_downPayment").prop("disabled", true);
	    			$("#inp_dPBaht").prop("disabled", true);
	    			$("#inp_dPDays").prop("disabled", true);
	    			$("#inp_dPDate").prop("disabled", true);
	    		}
	    	});
	    	$("#inp_pDuration").blur(function() {
    			calEndCont();
	    	});
	    	$("#inp_dPDays").blur(function() {
    			calEndCont();
	    	});
//-----------------------------------------------------------------------------------------------------------

//------------------------------------------------- Load ----------------------------------------------------
		<?php 
			if ($id) {
				include("INC/connectSFC.php");
				$sqlStr = "SELECT ID, FCYear, PEContractNo, EContract, Project, CustName, CustOrg, Potential, Progress, ESRIisTOR, Education, TargetProject, TargetSpecialProject, TargetIncome, TargetBudget, TargetCost, CONVERT(VARCHAR(10), TimeFrameBidingDate, 103) AS TimeFrameBidingDate, CONVERT(VARCHAR(10), TimeFrameContractSigndate, 103) AS TimeFrameContractSigndate, TimeFrameProjectDuration, chkDP, PercentAmount, InvoiceAmount, InvDuration, Book, Forward, Forward2, Forward3, Remark, SaleID, SaleRepresentative, VerticalMarketGroup, ProjectBy, title, custsurname, division, department, CustID, quotno,ProjectName,BudgetYear,EnduserNo,EnduserName,EnduserCode,EnduserOrg,PayerNo,PayerName,PayerCode,PayerOrg,Industry,PayerIndustry,ReasonTechnic,ReasonRelation,ReasonPrice,ProjectStatus,CONVERT(VARCHAR(10), DateUpdated, 103) AS DateUpdated,FlagGenerate, P30company1, P30price1, P30company2, P30price2, P30company3, P30price3, P60company1, P60price1,P60company2, P60price2, P60company3, P60price3  FROM Forecast WHERE IDForecast='$id'";
				$query = sqlsrv_query($ConnectSaleForecast, $sqlStr );
				if ($obj = sqlsrv_fetch_object($query)) {
		?>
			tempDescType = ["", "Site Prep", "HW", "SW", "Data", "Application", "On Site", "Training", "GPS", "SURVEY EQUIPMENT", "SYSTEMS", "Other", "CONSULT", "ESRI", "Leica", "Garmin", "NOSTRA"];
			tempFCYear = '<?php echo $obj->FCYear?>';
			$("#EndUserIndustry").val('<?php echo $obj->Industry?>');
			$("#PayerIndustry").val('<?php echo $obj->PayerIndustry?>');
			//Contract Block 
			$("#inp_contractpe").val('<?php echo $obj->PEContractNo?>');
			$("#inp_contractec").val('<?php echo trim($obj->EContract)?>');
			//Project Description Block 
			$("#projDesc_Year").val('<?php echo $obj->BudgetYear?>');
			$("#projDesc_show").text('<?php echo replaceSQuoteAndNl($obj->Project)?>');
			$("#projDesc_Name").val('<?php echo replaceSQuoteAndNl($obj->ProjectName)?>');
			//Payer Block 			
			$("#payer_custno").val('<?php echo $obj->PayerNo?>');
			$("#payer_name").val('<?php echo $obj->PayerName?>');
			$("#payer_code").val('<?php echo $obj->PayerCode?>');
			$("#payer_org").val('<?php echo $obj->PayerOrg?>');
			//End User Block 
			$("#enduser_custno").val('<?php echo $obj->EnduserNo?>');
			$("#enduser_name").val('<?php echo  $obj->EnduserName?>');
			$("#enduser_code").val('<?php echo $obj->EnduserCode?>');
			$("#enduser_org").val('<?php echo  $obj->EnduserOrg?>');
			if('<?php echo trim($obj->quotno)?>' != "") $("#importedFrom").text("Imported from quotation: " + '<?php echo replaceSQuoteAndNl($obj->quotno)?>');
			//Target Block 
			$("#potential_tbody_list").children().each(function() {
				if ($($(this).children()[0]).text() == '<?php echo $obj->Potential?>%') {
					$(this).click();
					return false;
				}
			});
			// $("#progress_tbody_list").children().each(function() {
			// 	if ($($(this).children()[0]).text() == '<?php echo $obj->Progress?>%') {
			// 		$(this).click();
			// 		return false;
			// 	}
			// });
			
			$("#progress_tbody_list").children().each(function() {
				var chkProgress = '';
				if('<?php echo $obj->Progress?>' == 'v' || '<?php echo $obj->Progress?>' == 'P'){
					chkProgress = '<?php echo $obj->Progress?>';
				}else{
					chkProgress = '<?php echo $obj->Progress?>%';
				}
				if ($($(this).children()[0]).text() == chkProgress) {
					$(this).click();
					return false;
				}
			});
			var Sprogress='<?php echo $obj->Progress?>';
			if(Sprogress=='P'||Sprogress=='v'||Sprogress=='0'||Sprogress=='10'||Sprogress=='20')
			{
			}
			else if(Sprogress=='30'||Sprogress=='40'||Sprogress=='50')
	    {
	    				//$("#Reason").css("display", "none");
	    				//$("#Price30_modal").modal('show');
	    	$("#Progress_30_div").css("display", "block");'<?php echo $obj->P30company1?>'
				$("#P30company1_name").val('<?php echo $obj->P30company1?>');
				$("#P30company2_name").val('<?php echo $obj->P30company2?>');
				$("#P30company3_name").val('<?php echo $obj->P30company3?>');
				$("#P30company1_price").val('<?php echo $obj->P30price1?>').blur();
				$("#P30company2_price").val('<?php echo $obj->P30price2?>').blur();
				$("#P30company3_price").val('<?php echo $obj->P30price3?>').blur();
	    				
	    }
	    else if(Sprogress=='60'||Sprogress=='70'||Sprogress=='80'||Sprogress=='90'||Sprogress=='100')
	    {
	    				//$("#Reason").css("display", "none");
	    				//$("#Price60_modal").modal('show');
	    	$("#Progress_30_div").css("display", "block");
	    	$("#Progress_60_div").css("display", "block");
				$("#P30company1_name").val('<?php echo $obj->P30company1?>');
				$("#P30company2_name").val('<?php echo $obj->P30company2?>');
				$("#P30company3_name").val('<?php echo $obj->P30company3?>');
				$("#P30company1_price").val('<?php echo $obj->P30price1?>').blur();
				$("#P30company2_price").val('<?php echo $obj->P30price2?>').blur();
				$("#P30company3_price").val('<?php echo $obj->P30price3?>').blur();
				$("#P60company1_name").val('<?php echo $obj->P60company1?>');
				$("#P60company2_name").val('<?php echo $obj->P60company2?>');
				$("#P60company3_name").val('<?php echo $obj->P60company3?>');
				$("#P60company1_price").val('<?php echo $obj->P60price1?>').blur();
				$("#P60company2_price").val('<?php echo $obj->P60price2?>').blur();
				$("#P60company3_price").val('<?php echo $obj->P60price3?>').blur();
	    				
	    }
			//if('<?php echo $obj->ESRIisTOR?>' == "1") $("#chk_esri").prop("checked", true);
			//if('<?php echo $obj->Education?>' == "1") $("#chk_edu").prop("checked", true);
			//if('<?php echo $obj->TargetProject?>' == "1") $("#chk_proj").prop("checked", true);
			if('<?php echo $obj->TargetSpecialProject?>' == "1") $("#chk_sproj").prop("checked", true);
			$("#inp_inco_evat").val('<?php echo $obj->TargetIncome?>').blur();
			$("#inp_budg_evat").val('<?php echo $obj->TargetBudget?>').blur();
			$("#inp_cost_evat").val('<?php echo $obj->TargetCost?>').blur();
			$("#inp_biddDate").val('<?php echo $obj->TimeFrameBidingDate?>').change();
			$("#inp_signDate").val('<?php echo $obj->TimeFrameContractSigndate?>').change();
			$("#inp_pDuration").val('<?php echo $obj->TimeFrameProjectDuration?>').blur();
			if('<?php echo $obj->chkDP?>' == "0") $("#chk_downPayment").click();
			$("#inp_downPayment").val('<?php echo $obj->PercentAmount?>');
			$("#inp_dPBaht").val('<?php echo $obj->InvoiceAmount?>');
			$("#inp_dPDays").val('<?php echo $obj->InvDuration?>').blur();
			$("#txa_salesremark").val('<?php echo replaceSQuoteAndNl( $obj->Remark)?>');
			$("#inp_salesname").val('<?php echo replaceSQuoteAndNl( $obj->SaleRepresentative)?>');
			$("#slc_verticalmarket").val('<?php echo $obj->VerticalMarketGroup?>');
			$("#slc_salesproj").val('<?php echo replaceSQuoteAndNl($obj->ProjectBy)?>');
			//$("#inp_custtname").val('<?php echo replaceSQuoteAndNl( $obj->title)?>');
			//$("#inp_custlname").val('<?php echo replaceSQuoteAndNl($obj->custsurname)?>');
			//$("#inp_custdiv").val('<?php echo replaceSQuoteAndNl($obj->division)?>');
			//$("#inp_custdep").val('<?php echo replaceSQuoteAndNl( $obj->department)?>');
		<?php 
					$sqlStr = "SELECT IDTypeDescription, Description, Price FROM DescriptionDetail WHERE IDForecast='$id'";
					$query = sqlsrv_query($ConnectSaleForecast, $sqlStr );
					while ($obj = sqlsrv_fetch_object($query)) {
		?>
			tr = $('<tr class="warning"><td><p class="lead14">' + tempDescType[<?php echo $obj->IDTypeDescription?>] + '</p></td><td><p class="lead14"><?php echo replaceSQuoteAndNl( $obj->Description)?></p></td><td><p class="lead14 text-right">' + numFormat(parseFloat('<?php echo $obj->Price?>' != "" ? '<?php echo $obj->Price?>' : "0.00").toFixed(2)) + '</p></td></tr>');
			$("#tbody_description").append(tr);
		<?php 
					}
		?>
			calSIProj();
			tempArr = {};

			
	    	tempstart = $("#inp_signDate").val().split("/")[2];
	    	tempend = $("#inp_endCont").val().split("/")[2];
	    	for(var iii=tempstart;iii<=tempend;iii++)
	    	{
	    		tempArr[iii]=0;
	    	}
	    	
			if ($("#chk_downPayment").prop("checked") && parseFloat($("#inp_dPBaht").val()) > 0) {
				if ($("#inp_dPDate").val() != "") {
					tempArr[$("#inp_dPDate").datepicker('getDate', '+1d').getFullYear()] = parseFloat($("#inp_dPBaht").val()).toFixed(2);
				} else {
					tempArr[tempFCYear] = parseFloat($("#inp_dPBaht").val()).toFixed(2);
				}
			}
		<?php 
					$sqlStr = "SELECT PlanPhase, DurationDelivery, CONVERT(VARCHAR(10), DeliveryDate, 103) AS DeliveryDate, Amount_Percent, Amount, Remark FROM InvoicingPlan WHERE IDForecast='$id' ORDER BY InvoicingPlanID";
					$query = sqlsrv_query($ConnectSaleForecast, $sqlStr , array(), array( "Scrollable" => 'static' ));
					if (sqlsrv_num_rows($query) > 0) {
		?>
			$("#tbody_bank").empty();
		<?php 
					}
					while ($obj = sqlsrv_fetch_object($query)) {
		?>
			tempYear = '<?php echo $obj->DeliveryDate?>'.split("/")[2];
			if (tempArr[tempYear] > 0) {
				tempArr[tempYear] = (parseFloat(tempArr[tempYear]) + parseFloat('<?php echo $obj->Amount?>')).toFixed(2);
			} else {
				tempArr[tempYear] = parseFloat('<?php echo $obj->Amount?>').toFixed(2);
			}
			tr = '<tr class="warning"><td><p class="lead14"><?php echo $obj->PlanPhase?></p></td><td><p class="lead14"><?php echo $obj->DurationDelivery?></p></td><td><p class="lead14"><?php echo $obj->DeliveryDate?></p></td><td><p class="lead14 text-right">' + numFormat(parseFloat('<?php echo $obj->Amount_Percent?>').toFixed(2)) + '</p></td><td><p class="lead14 text-right">' + numFormat(parseFloat('<?php echo $obj->Amount?>').toFixed(2)) + '</p></td><td><p class="lead14"><?php echo replaceSQuoteAndNl( $obj->Remark)?></p></td></tr>';
			$("#tbody_invoicing").append(tr);
		<?php 
					}
		?>
			for (tempKey in tempArr) {
				tempType = "Book";
				//if (parseInt(tempKey) > new Date().getFullYear()) tempType = "Forward";
				if (parseInt(tempKey) > $("#inp_signDate").datepicker('getDate', '+1d').getFullYear()) tempType = "Forward";//22/12/14 edit by TUA
				tr = '<tr class="error"><td><p class="lead14">' + tempType + '</p></td><td><p class="lead14">' + tempKey + '</p></td><td><p class="lead14">' + numFormat(parseFloat(tempArr[tempKey]).toFixed(2)) + '</p></td></tr>';
				$("#tbody_bank").append(tr);
			}
		<?php include("INC/connectSFC.php");
					$sqlStr = "SELECT CONVERT(VARCHAR(10), DateStatus, 103) AS DateStatus, CAST(Description as TEXT) as Description FROM StatusDetail WHERE IDForecast='$id' ORDER BY year(DateStatus) desc,month(DateStatus) desc,day(DateStatus) desc";
					$query = sqlsrv_query($ConnectSaleForecast, $sqlStr );
					while ($obj = sqlsrv_fetch_object($query)) {
		?>
			tr = $('<tr class="warning"><td><p class="lead14"><?php echo $obj->DateStatus?></p></td><td><p class="lead14"><?php echo replaceSQuoteAndNl($obj->Description)?></p></td>');
			$("#tbody_status").append(tr);
		<?php 
					}
					$sqlStr = "SELECT DSNNameID FROM DepartmentSupportNeeded WHERE IDForecast='$id' ORDER BY IDDSN";
					$query = sqlsrv_query($ConnectSaleForecast,$sqlStr );
					while ($obj = sqlsrv_fetch_object($query)) {
						$DSNNameIDdata = replaceSQuoteAndNl( $obj->DSNNameID);
						$sqlStr2 = "SELECT depName FROM DepartmentDetail WHERE id ='$DSNNameIDdata' and flag <> '1'";
						$query2 = sqlsrv_query($ConnectSaleForecast, $sqlStr2 );
						while ($obj2 = sqlsrv_fetch_object($query2)) {
							$DSNNamedata = replaceSQuoteAndNl( $obj2->depName);
							?>
								load_Tb_DeptSupport('<?php echo  $DSNNameIDdata?>','<?php echo $DSNNamedata?>');
							<?php 
						}
					}
					$sqlStr = "SELECT EPName, qty FROM ESRIProduct WHERE IDForecast='$id' ORDER BY IDEP";
					$query = sqlsrv_query($ConnectSaleForecast, $sqlStr );
					while ($obj = sqlsrv_fetch_object($query)) {
		?>
    		tr = $('<tr class="warning"><td><p class="lead14"><?php echo replaceSQuoteAndNl($obj->EPName)?></p></td><td><p class="lead14"><?php echo $obj->qty?></p></td></tr>');
			$("#tbody_esri").append(tr);
		<?php 
					}
					$sqlStr = "SELECT Name, qty FROM SEProduct WHERE IDForecast='$id' ORDER BY ID";
					$query = sqlsrv_query($ConnectSaleForecast, $sqlStr );
					while ($obj = sqlsrv_fetch_object($query)) {
		?>
    		tr = $('<tr class="warning"><td><p class="lead14"><?php echo replaceSQuoteAndNl($obj->Name)?></p></td><td><p class="lead14"><?php echo $obj->qty?></p></td></tr>');
			$("#tbody_SE").append(tr);
		<?php 
					}
					$sqlStr = "SELECT LPName, qty FROM LeicaProduct WHERE IDForecast='$id' ORDER BY IDLP";
					$query = sqlsrv_query($ConnectSaleForecast, $sqlStr );
					while ($obj = sqlsrv_fetch_object($query)) {
		?>
    		tr = $('<tr class="warning"><td><p class="lead14"><?php echo replaceSQuoteAndNl($obj->LPName)?></p></td><td><p class="lead14"><?php echo $obj->qty?></p></td></tr>');
			$("#tbody_leica").append(tr);
		<?php 
					}
					$sqlStr = "SELECT TMPName, qty FROM TMProduct WHERE IDForecast='$id' ORDER BY IDTMP";
					$query = sqlsrv_query($ConnectSaleForecast, $sqlStr );
					while ($obj = sqlsrv_fetch_object($query)) {
		?>
    		tr = $('<tr class="warning"><td><p class="lead14"><?php echo replaceSQuoteAndNl($obj->TMPName)?></p></td><td><p class="lead14"><?php echo $obj->qty?></p></td></tr>');
			$("#tbody_garmin").append(tr);
		<?php 
					}
					$sqlStr = "SELECT GPName, qty FROM GEOProduct WHERE IDForecast='$id' ORDER BY IDGP";
					$query = sqlsrv_query($ConnectSaleForecast, $sqlStr );
					while ($obj = sqlsrv_fetch_object($query)) {
		?>
    		tr = $('<tr class="warning"><td><p class="lead14"><?php echo replaceSQuoteAndNl($obj->GPName)?></p></td><td><p class="lead14"><?php echo $obj->qty?></p></td></tr>');
			$("#tbody_nostra").append(tr);
		<?php 
					}
					$sqlStr = "SELECT CONVERT(VARCHAR(10), DateChange, 103) AS DateChange, Potential, PotentialName FROM HistoryPotential WHERE IDForecast='$id' ORDER BY ID DESC";
					$query = sqlsrv_query($ConnectSaleForecast, $sqlStr );
					while ($obj = sqlsrv_fetch_object($query)) {
		?>
			$("#hPotential_tbody_list").append($('<tr><td><?php echo $obj->DateChange?></td><td><?php echo $obj->Potential?>%</td><td><?php echo replaceSQuoteAndNl($obj->PotentialName)?></td></tr>'));
		<?php 
					}
					$sqlStr = "SELECT CONVERT(VARCHAR(10), DateChange, 103) AS DateChange, Progress, ProgressName FROM HistoryProgress WHERE IDForecast='$id' ORDER BY ID DESC";
					$query = sqlsrv_query($ConnectSaleForecast, $sqlStr);
					while ($obj = sqlsrv_fetch_object($query)) {
		?>
			$("#hProgress_tbody_list").append($('<tr><td><?php echo $obj->DateChange?></td><td><?php echo $obj->Progress?>%</td><td><?php echo replaceSQuoteAndNl($obj->ProgressName)?></td></tr>'));
		<?php 
					}
				} 
			}
		?>
//-----------------------------------------------------------------------------------------------------------

	    });
	</script>

</body>
</html>