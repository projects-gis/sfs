﻿<!DOCTYPE html>
<html lang="en">
<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
		<meta charset="utf-8">
		<title>SFS:::Transfer </title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="Izuddin Helmi">

		<!-- Stylesheets -->
		<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
		<link href="js/library/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
		<link href="css/styler/style.css" rel="stylesheet">
		<link href="js/library/syntaxHighlighter/shCore.css" rel="stylesheet" type="text/css" />
		<link href="js/library/syntaxHighlighter/shThemeDefault.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="css/styler/fontello.css">
	    <link rel="stylesheet" href="css/styler/animation.css"><!--[if IE 7]>
	    <link rel="stylesheet" href="css/styler/fontello-ie7.css"><![endif]-->
		<link rel="stylesheet" href="css/main.css">
	    

		<!-- Custom Fonts -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300,500' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

		<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
		  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
<!-- Javascript -->
		<!-- JS:jquery-->
		<!--<script src="../../../../ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>-->
  		<script src="js/library/jquery/jquery.min.js"></script>
		<script src="js/library/jquery/jquery-ui.min.js"></script>

		<!-- JS:masonry-->
		<script src="js/library/masonry/jquery.masonry.min.js"></script>

		<!-- JS:flexslider-->
		<script src="js/library/flexslider/jquery.flexslider-min.js"></script>

		<!-- JS:tables-->
		<script src="js/library/dataTables/jquery.dataTables.min.js"></script>

		<!-- JS:calendar-->
		<script src="js/library/fullcalendar/fullcalendar.min.js"></script>
		<script src="js/library/fullcalendar/gcal.js"></script>

		<!-- JS:forms-->
		<script src="js/library/forms/jquery.hotkeys.js"></script>
		<script src="js/library/forms/bootstrap-wysiwyg.js"></script>
		<script src="js/library/forms/picker.js"></script>
		<script src="js/library/forms/picker.date.js"></script>
		<script src="js/library/forms/picker.time.js"></script>
		<script src="js/library/forms/jquery.bootstrap.wizard.min.js"></script>

		<!-- JS:charts-->
		<script src="js/library/charts/jquery.flot.js"></script>
		<script src="js/library/charts/jquery.flot.resize.js"></script>
		<script src="js/library/charts/jquery.flot.stack.js"></script>
		<script src="js/library/charts/jquery.flot.pie.min.js"></script>
		<script src="js/library/charts/jquery.sparkline.min.js"></script>
		<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="excanvas.min.js"></script><![endif]-->

		<!-- JS:syntaxHighlighter-->
		<script src="js/library/syntaxHighlighter/shCore.js"></script>
		<script src="js/library/syntaxHighlighter/shBrushCss.js"></script>
		<script src="js/library/syntaxHighlighter/shBrushXml.js"></script>
		<script src="js/library/syntaxHighlighter/shBrushJScript.js"></script>

		<!-- JS:bootstrap-->
		<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="js/library/jquery.number.js"></script>
		
		<!-- JS:custom js for this template -->
		<script src="js/styler/custom.js"></script>
		<script src="js/set_cookie.js"></script>
		<script src="js/jquery.price_format.2.0.js"></script>
		<script type="text/javascript">
			var GlobalSaleID = "<?php echo $_COOKIE['Ses_ID']?>";
			Number.prototype.numformat = function() {
			    return this.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
			};
		  $(document).ready(function(){
				
		    $('.widget-sales').on('click','#loadsales',function(){
		        	$('#myModal').modal('show');
		    });
				$('#example > tbody:last').empty();
				$("#loading").show();
				$.ajax({ 
						type:"POST",
						dataType: "json",
						url: "AJAX/LoadTransfer.php",
						data: {
							saleid: GlobalSaleID
						},
						success: getSearch
				});
				 
				//$( "#btnSearch" ).on( "click", search);
				// $('#txtKeyword').keyup(function(e) {
				// 	if(e.keyCode == 13) {
				// 		search();
				// 	}
				// });
				$('.chkall').on( "click",function(e) {
					console.log($(this).prop("checked"));
					if($(this).prop("checked"))
					{
							$(".chktran").prop("checked",true);
					}
					else
					{
							$(".chktran").prop("checked",false);
					}
				});
				
				$('.btn_save').on( "click",function(e) {
					var myValue="";
					$("#example").find(".chktran").each(function(){
						if($(this).prop("checked"))
						{
							if(myValue=="")
							{
								myValue=$(this).val();
							}
							else
							{
								myValue=myValue+","+$(this).val();
							}
						}

					});
					if(myValue=="")
					{
						alert("กรุณาเลือกข้อมูลที่ต้องการโอน");
					}
					else
					{						
						$("#masterValue").val(myValue);
						$("#transfer_modal").modal("show");
					}
					/*if($(this).prop("checked"))
					{
							$("#chktran").prop("checked",true);
					}
					else
					{
							$(".chktran").prop("checked",false);
					}*/
				});

				var _loginID = "<?php echo $_COOKIE['Ses_ID']?>";
				$.ajax({
    			type: "POST",
					dataType: "json",
		        url: "AJAX/listSalesRepresentative.php",
		        success: function(json) {
		        	$.each(json, function() {
	    				var item = this;

			    		if (parseInt(_loginID) != item[0])
			    		{
		    				tr = $('<tr ><td style="font-size:16px;padding:10px 0;text-align:center">' + item[0] + '</td><td style="font-size:16px;padding:10px 0;">' + item[1] + '</td></tr>');
		    				tr.on("click", function() {
		    					//console.log(item[0]);
				    			Transfer(item[0],item[1]);
				    		});
				    		$("#sales_tbody_list").append(tr);
			    		}
	    			});
		        },
				error: function() {
					alert("เกิดปัญหาในการโหลดข้อมูลกรุณารีเฟรชหน้าเว็บอีกครั้ง");
					$("#loading").hide();
				}
		    });
		  });
			function refresh()
			{
				$.ajax({ 
						dataType: "json",
						url: "AJAX/LoadTransfer.php",
						data: {
							saleid: GlobalSaleID
						},
						success: getSearch
				});
			}
			function Transfer(saleid,name)
			{
				if(confirm("ต้องการโอนข้อมูลให้ "+name +" ?"))
				{
					var myValue=$("#masterValue").val();
					$("#loading").show();
					console.log(myValue);
					$.ajax({
	    			type: "POST",
						dataType: "json",
						data: {
							saleid: saleid,
							name:name,
							transfer:myValue
						},
			      url: "AJAX/saveTransfer.php",
			      success: function(json) {
			      		if(json == "success")
			      		{
									$("#transfer_modal").modal("hide");
									sendmail(saleid,myValue)
									//refresh();
				        }

				        $("#loading").hide();
			       },
						error: function() {
							alert("เกิดปัญหาในการโหลดข้อมูลกรุณารีเฟรชหน้าเว็บอีกครั้ง");
							$("#loading").hide();
						}
			    });
				}
			}
		function sendmail(saleid,transfer)
		{
					//var myValue=$("#masterValue").val();
					$.ajax({
	    			type: "POST",
						dataType: "json",
						data: {
							saleid: saleid,
							transfer:transfer
						},
			      url: "AJAX/sendmailTransfer.php",
			      success: function(json) {
			      		//refresh();						  
			      		location.reload();
				        //$("#loading").hide();
			       },
						error: function() {
							alert("เกิดปัญหาในการโหลดข้อมูลกรุณารีเฟรชหน้าเว็บอีกครั้ง");
							//$("#loading").hide();

			      			location.reload();
						}
			    });

		}
		// function search(){
		// 	$("#loading").show();
		// 	var Keyword = document.getElementById("txtKeyword").value;
		// 	if(Keyword.replace(" ","") == ""){
		// 		$.ajax({ 
		// 			dataType: "json",
		// 			url: "http://157.179.28.116/sfs/sv.svc/forecast_select_saleid?callback=?",
		// 			data: {
		// 				saleid: GlobalSaleID
		// 			},
		// 			success: getSearch
		// 	});
		// 	}else{
		// 		$.ajax({ 
		// 			dataType: "json",
		// 			url: "http://157.179.28.116/sfs/sv.svc/Search_Select_keyword?callback=?",
		// 			data: {
		// 				saleid: GlobalSaleID,
		// 				keyword: Keyword
		// 			},
		// 			success: getSearch
		// 	});
		// 	}
		// }
		function getSearch(json) {
					document.getElementById('sumResult').innerHTML=json.length;
					$('#example > tbody:last').empty();
					if(json.length== 0){
						var noresult = "<tr><td colspan='6' style='text-align:center;'><p class='lead14'>ไม่พบข้อมูล</p></td></tr>";
						$('#example').append(noresult);
					}else{
            	 $.each(json, function() {
            	 	var newrow="";       
		    				var item = this;     	 	
            	  //newrow=$('<tr>').attr('class','success');
            	  if(item[4]>50)
            	  {
            	    newrow=$('<tr>').attr('class','success');
            	  }
            	  else if(item[4]==50)
            	  {
            	  	newrow=$('<tr>').attr('class','warning');
            	  }
            	  else if(item[4]<50)
            	  {
            	  	newrow=$('<tr>').attr('class','error');
            	  }
		    				var PEContractNo = item[2];
								var Project = item[3];
								var TimeFrameBidingDate = item[6];
								var TimeFrameContractSigndate = item[7];
								var TargetIncome = item[8];

            	  var cols="";

            	  cols+="<td><p class='lead14'><input type='checkbox' class='chktran' value='"+item[0]+"'></p></td>";
            	  cols+="<td><p class='lead14'>"+PEContractNo+"</p></td>";
            	  cols+="<td><p class='lead14'>"+Project+"</p></td>";
            	  cols+="<td><p class='lead14'>"+TimeFrameBidingDate+"</p></td>";
            	  cols+="<td><p class='lead14'>"+TimeFrameContractSigndate+"</p></td>";
            	  cols+="<td style='text-align:right'><p class='lead16'>"+TargetIncome+"</p></td>";
            	  
            	  newrow.append(cols);
								$('#example').append(newrow);
		    			});
					}
					$("#loading").hide();
					$('.lead16').number(true, 2);
					
       }
		</script>
	</head>
<?php  $checkmenu = '8'; ?>
<body cz-shortcut-listen="true" style="margin-top:0px;">	
<div id="loading" style="height: 100%; width: 100%; position: fixed; left: 0; top: 0; z-index: 1051 !important; background-color: black; filter: alpha(opacity=9); -khtml-opacity: 0.9; -moz-opacity: 0.9; opacity: 0.9;"><img src="img/loading.gif" style="width: 150px; height: 150px; position: fixed; top: 50%; left: 50%; margin: -75px 0 0 -75px;"></div>
<section class="page-header bg-color white">
	<div class="container">
		<div class="row">
			<div class="span12">
				<div class="section-header clearfix">
					<div class="pull-left">
						<h2><i class="icon-switch"></i>Transfer </h2>
					</div>
					<div id="shortcut" class="nav-collapse pull-right"><ul class="shortcuts unstyled">
						<?php include("menu.php");?>
					</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<input id="masterValue" type="hidden">
<section class="dashboard content">
	<div class="container">
			<div class="row">
				<div class="span12 widget">		
					<!--<div class="widget-header clearfix">
						<span class="pull-left"><i class="icon-info-circled"></i> Search</span>						
					</div>-->
					<div class="widget widget_search">
						<div class="bg-color white rounded">
						
							<div style="padding:10px 20px 5px 20px;text-align:left">
								<span id="sumResult" class="label label-inverse " style="font-size:20px;padding:10px;" title="Number of results">0</span>
								&nbsp;&nbsp;<font color="#777777" style="font-size:14px"><b>รายการ</b></font>
								<span class="pull-right">
									<input class="span4 " type="text" id="txtKeyword" style="width:300px;margin-bottom:0px;margin-top:0px;display:none" sthle="margin-bottom: 0px;" placeholder="ค้นหาจาก PE Contract และชื่อโครงการ">
									<img id="btnSearch" src="img/styler/icons/search.png" style="cursor: pointer;padding-left:5px;display:none" width="16" height="16" border="0" data-toggle="tooltip" data-placement="bottom" data-original-title="Search"/>
									<a class="btn btn-blue btn_save" style="color: #FFFFFF"><i class="i_btn icon-switch" data-toggle="tooltip" data-placement="bottom" data-original-title="transfer"></i> TRANSFER</a>
								</span>

							</div>
							<!--<div class="form-search pull-right">
								<input class="input-medium search-query" type="text" placeholder="Search anything here">
								<button class="btn" type="submit">Search</button>
							</div>-->
							<div class="box-padding" style="padding:5px">
				    		<table class="table table-striped" id="example">
								<thead>
									<tr role="row">
											<th style="background: none repeat scroll 0 0 #2ABF9E;color: #FFFFFF;" width="30px"><input type="checkbox" class="chkall"></th>
											<th style="background: none repeat scroll 0 0 #2ABF9E;color: #FFFFFF;" width="80px">PE Contract</th>
											<th style="background: none repeat scroll 0 0 #2ABF9E;color: #FFFFFF;" width="500px">Project Name</th>
											<th style="background: none repeat scroll 0 0 #2ABF9E;color: #FFFFFF;" width="80px">BiddingDate</th>
											<th style="background: none repeat scroll 0 0 #2ABF9E;color: #FFFFFF;" width="80px">SignDate</th>
											<th style="background: none repeat scroll 0 0 #2ABF9E;color: #FFFFFF;text-align:right" width="80px">Income</th>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
					    </div>

							<div style="padding:10px 20px 40px 20px;text-align:left">
								
								<span class="pull-right">
									<a class="btn btn-blue btn_save" style="color: #FFFFFF"><i class="i_btn icon-switch" data-toggle="tooltip" data-placement="bottom" data-original-title="transfer"></i> TRANSFER</a>
								</span>

							</div>
				    </div>
			    </div>			
				</div>
			</div>
	</div>
</section>
<div class="modal hide fade" id="transfer_modal" style="width: 600px; margin-left: -250px;">
	<div class="modal-header bg-color light-green aligncenter">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    <h1>กรุณาเลือกข้อมูล Transfer</h1>
	</div>
	<div class="modal-body" style="padding-top: 0px;max-height:300px;">
			<table class="table table-striped table-condensed table_select">
			<thead>
				<tr style=" background-color: #2ABF9E; color: #FFFFFF;">
					<th style="width: 20%;font-size:18px;font-weight:bold;padding:10px 0;">Emp. No.</th>
					<th style="width: 80%;font-size:18px;font-weight:bold;padding:10px 0;">Name</th>
				</tr>
			</thead>
			<tbody id="sales_tbody_list"></tbody>
		</table>
	</div>
	<div class="modal-footer alignright">
	    <!--<a id="import_btn_ok" class="btn btn-blue">Transfer</a>-->
	    <a class="btn btn-red" data-dismiss="modal">CLOSE</a>
	</div>
</div>

		<div id="footer" class="bg-color dark-blue">
			<div class="container">
				<div class="box-padding">
					Copyright &copy; 2013 Sales Forecast
				</div>
			</div>
		</div>
		
  	</body>

<!-- Mirrored from bootstrapstyler.com/preview/_/flatdashboard/ by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 18 Oct 2013 07:00:04 GMT -->
</html>