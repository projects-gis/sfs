<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<meta http-equiv="refresh" content="86400">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="Izuddin Helmi">
	<title>MarkFlag & Sendmail Update หนังสือรับรอง</title>
<script src="js/library/jquery/jquery-1.9.1.js"></script>
	<script src="js/library/jquery/jquery-ui-1.10.3.custom.js"></script>
	<script src="js/library/masonry/jquery.masonry.min.js"></script>
	<script src="js/library/flexslider/jquery.flexslider-min.js"></script>
	<script src="js/library/dataTables/jquery.dataTables.min.js"></script>
	<script src="js/library/fullcalendar/fullcalendar.min.js"></script>
	<script src="js/library/fullcalendar/gcal.js"></script>
	<script src="js/library/forms/jquery.hotkeys.js"></script>
	<script src="js/library/forms/bootstrap-wysiwyg.js"></script>
	<script src="js/library/forms/picker.js"></script>
	<script src="js/library/forms/picker.date.js"></script>
	<script src="js/library/forms/picker.time.js"></script>
	<script src="js/library/forms/jquery.bootstrap.wizard.min.js"></script>
	<script src="js/library/charts/jquery.flot.js"></script>
	<script src="js/library/charts/jquery.flot.resize.js"></script>
	<script src="js/library/charts/jquery.flot.stack.js"></script>
	<script src="js/library/charts/jquery.flot.pie.min.js"></script>
	<script src="js/library/charts/jquery.sparkline.min.js"></script>
	<script src="js/library/syntaxHighlighter/shCore.js"></script>
	<script src="js/library/syntaxHighlighter/shBrushCss.js"></script>
	<script src="js/library/syntaxHighlighter/shBrushXml.js"></script>
	<script src="js/library/syntaxHighlighter/shBrushJScript.js"></script>
	<script src="js/library/jquery.number.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="js/styler/custom.js"></script>
	<script src="js/set_cookie.js"></script>

 <!--<script src="libs/jquery/jquery-1.8.2.min.js"></script>-->
  <script src="libs/bootstrap/js/bootstrap.min.js"></script>
  <script src="bootstrap-editable/js/bootstrap-editable.js"></script>
  <script src="libs/prettify/prettify.js"></script>
  <script src="libs/mockjax/jquery.mockjax.js"></script>

	<script src="js/jquery-scrolltofixed.js"></script>
	<script src="main.js"></script>

	<script type="text/javascript">
function sendmailSiteRef(saleid,contract)
	{
		console.log(saleid," : ",contract);
		/*$.ajax({
			type: "POST",
			dataType: "json",
			url: "AJAX/sendmailSiteRef.php",
			data: {
				SaleID: saleid,
				ContractNo: contract
			},
			success: function(json) {
				console.log("sendmail complete ",saleid);
			},
			error: function() {

			}
		});*/
	}
</script>
</head>
<?php 
function dateDiffDMY($dformat, $endDate, $beginDate)//$endDate="7/7/2003";//D/M/Y
 {
    $date_parts1=explode($dformat, $beginDate);
    $date_parts2=explode($dformat, $endDate);
    $start_date=gregoriantojd($date_parts1[1],$date_parts1[0], $date_parts1[2]);
    $end_date=gregoriantojd($date_parts2[1], $date_parts2[0], $date_parts2[2]);
    return $end_date - $start_date;
}
	//echo "test";
	include("INC/connectSFC.php");
	$sqlStr = "SELECT  SaleManager, SaleIn FROM  SaleManager order by SaleIn desc";
	$query=sqlsrv_query($ConnectSaleForecast,$sqlStr);
	//List Sale
	$ContractAll= array();
	$r=0;
	while ($obj = sqlsrv_fetch_object($query)) {
		$SaleIn= iconv("TIS-620", "UTF-8", $obj->SaleIn);
		$SaleManager= iconv("TIS-620", "UTF-8", $obj->SaleManager);
		$sqlStr2 = "select ID, IDForecast, PEContractNo,EContract, Project, Progress,SaleRepresentative,CONVERT(varchar(10), TimeFrameDeliveryDate, 103) as TimeFrameDeliveryDate,FlagSiteRef,CONVERT(varchar(10), SendmailSiteRefUpdate, 103) as SendmailSiteRefUpdate,CONVERT(varchar(10), Getdate(), 103) as TodayDate,SendmailSiteRefCount  FROM Forecast WHERE SaleID = '$SaleIn' and FlagSiteRef!='2' AND Progress ='100' and (convert(datetime,TimeFrameDeliveryDate,103) < convert(datetime,Getdate(),103)) AND (EContract <> '')";
		$query2 = sqlsrv_query($ConnectSaleForecast,$sqlStr2);
		//echo sqlsrv_num_rows($query2);
		//List E-Contract
		$i=0;
		$contract="";
		while ($obj2 = sqlsrv_fetch_object($query2)) {
			$EContract = iconv("TIS-620", "UTF-8", $obj2->EContract);
			$FlagSiteRef = iconv("TIS-620", "UTF-8", $obj2->FlagSiteRef);
			$TodayDate = iconv("TIS-620", "UTF-8", $obj2->TodayDate);
			$TimeFrameDeliveryDate = iconv("TIS-620", "UTF-8", $obj2->TimeFrameDeliveryDate);
			$SendmailSiteRefUpdate = iconv("TIS-620", "UTF-8", $obj2->SendmailSiteRefUpdate);
			$SendmailSiteRefCount = iconv("TIS-620", "UTF-8", $obj2->SendmailSiteRefCount);
			if($SendmailSiteRefUpdate=="")
			{
				if($i==0)
				{
					$contract=$EContract;
				}else
				{
					$contract.=",".$EContract;
				}
				$i++;
			}
			else
			{
				$interval = dateDiffDMY("/",$TodayDate,$SendmailSiteRefUpdate);
				if($interval>=15)
				{
					if($i==0)
					{
						$contract=$EContract;
					}else
					{
						$contract.=",".$EContract;
					}
					$i++;
				}
			}
			
			
		}
		$ContractAll[$r][]=iconv("TIS-620", "UTF-8", $SaleIn);
		$ContractAll[$r][]=iconv("TIS-620", "UTF-8", $contract);
		//echo $contractAll."<br/>";
		$r++;
	}

	for($l=0;$l<count($ContractAll);$l++)
	{
		if($ContractAll[$l][1]!="")
		{
			$results="";
			$mycontract="";
			//$ContractAll[$l][0]."-".$ContractAll[$l][1]."<br/>";
			$EContract=split(',', $ContractAll[$l][1]);
			for ($ii = 0; $ii < count($EContract); $ii++) {

				include("INC/connectSiteRef.php");
				$sqlStr3 = "select  CONVERT(varchar(10), dateDoc, 103) as dateDoc  FROM ProjectImport WHERE ContractNo ='".$EContract[$ii]."'";
				$query3 = sqlsrv_query($ConnectSiteRef,$sqlStr3);
				$num=sqlsrv_num_rows($query3);
				if($num!=0)
				{
					while ($obj3 = sqlsrv_fetch_object($query3)) {
						//echo $obj3->dateDoc;
						//$results[$r][] = iconv("TIS-620", "UTF-8", $obj->dateDoc);
						if($obj3->dateDoc=="")//||is_null($obj3->dateDoc)
						{
							$results++;
							$mycontract.=$EContract[$ii].",";
							include("INC/connectSFC.php");
							$sqlStrU = "Update Forecast set FlagSiteRef='1',SendmailSiteRefUpdate=Getdate() WHERE EContract = '".$EContract[$ii]."'";
							$queryU = sqlsrv_query($ConnectSaleForecast,$sqlStrU);

						}
						else
						{
							include("INC/connectSFC.php");
							$sqlStrU = "Update Forecast set FlagSiteRef='2' WHERE EContract = '".$EContract[$ii]."'";
							$queryU = sqlsrv_query($ConnectSaleForecast,$sqlStrU);
						}
					}
				}
				else
				{
					$results++;
					$mycontract.=$EContract[$ii].",";
					include("INC/connectSFC.php");
					$sqlStrU = "Update Forecast set FlagSiteRef='1',SendmailSiteRefUpdate=Getdate() WHERE EContract = '".$EContract[$ii]."'";
					$queryU = sqlsrv_query($ConnectSaleForecast,$sqlStrU);
				}
				
			}
			echo $mycontract."<br/>";
			sendmailSiteRef($ContractAll[$l][0],$mycontract);
			?><script>//sendmailSiteRef("<?php echo $ContractAll[$l][0]?>","<?php echo $mycontract?>")</script><?php 
		}

	}
	
			/**/
			//echo "test";
	function sendmailSiteref($SaleID,$ContractNo)
	{
		//echo "sendmailSiteref ".$SaleID;
		$mailto="";
		include("INC/connectDB.php");
		$sqlStrDB = "SELECT email FROM employeeesrith WHERE empno = '$SaleID'";
		$queryDB = sqlsrv_query($ConnectDB, $sqlStrDB);
		while($objDB=sqlsrv_fetch_object($queryDB))
		{
			$mailto=$objDB->email;
		}
		require("classMail/class.mail.php");
		$mail = new phpmailer();
		$mail->Subject = "[SFS:Site Reference] ".iconv("UTF-8", "TIS-620","กรุณากรอกข้อมูลหนังสือรับรอง");
		//$mail->Subject = "[SFS:SmartUpdate]";
		$mail->Body = "<b>Site Reference </b><br/><br/>";
		$mail->Body .="ContractNo : ".$ContractNo;
		//$mail->Body .=iconv("UTF-8", "TIS-620", $mailto);
		//$mail->Body .=iconv("UTF-8", "TIS-620", $mailmana);
	
		$mail->From = "SFSAdmin@cdg.co.th";
		$mail->FromName ="SFSAdmin";
		$mail->AddAddress($mailto);
		$mail->AddCC($mailmana);
		//$mail->AddAddress('surachai.t@cdg.co.th');
		//$mail->AddBCC('surachai.t@cdg.co.th');
		//sendmail($mail);
	}
?>
</html>